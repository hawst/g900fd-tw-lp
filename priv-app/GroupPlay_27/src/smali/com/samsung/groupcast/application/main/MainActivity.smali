.class public Lcom/samsung/groupcast/application/main/MainActivity;
.super Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
.source "MainActivity.java"


# static fields
.field public static final ACTION_BLE_REQEUST_TO_RESTART:Ljava/lang/String; = "ACTION_BLE_REQEUST_TO_RESTART"

.field public static final ACTION_REMOTE_DISCONNECTED:Ljava/lang/String; = "GP_REMOTE_DISCONNECT"

.field public static final CAMERA_PHOTO_PATH:Ljava/lang/String; = "CameraPhotoPath"

.field public static final DOC_TOO_LARGE_TAG:Ljava/lang/String; = "DOC_TOO_LARGE_TAG"

.field public static final EXIT_CONFIRM_FRAGMENT_TAG:Ljava/lang/String; = "EXIT_CONFIRM_FRAGMENT"

.field public static final EXIT_WIFICLOSE_FRAGMENT_TAG:Ljava/lang/String; = "EXIT_WIFICLOSE_FRAGMENT"

.field public static final GAME_ACTION_EXTRA_GAME_NAME:Ljava/lang/String; = "GAME NAME"

.field public static final GAME_ACTION_GAMER_JOINED:Ljava/lang/String; = "GROUPPLAY GAMER JOINED"

.field public static final GAME_ACTION_GAMER_LEFT:Ljava/lang/String; = "GROUPPLAY GAMER LEFT"

.field public static final GROUP_CAMCODER_PARTICIPANT_ID:Ljava/lang/String; = "GroupCamcoder"

.field public static final INVITER_SCREEN_ID:Ljava/lang/String; = "INVITER_SCREEN_ID"

.field public static final IS_DJ_MODE_VIA_GROUP_PLAY:Ljava/lang/String; = "DJ_MODE_VIA_GROUP_PLAY_FROM_MUSIC_PLAYER"

.field public static final IS_SHARE_CAMCODER_VIA_GROUP_PLAY:Ljava/lang/String; = "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_CAMCODER_PLAYER"

.field public static final IS_SHARE_VIDEO_VIA_GROUP_PLAY:Ljava/lang/String; = "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_VIDEO_PLAYER"

.field private static final KEY_IS_SAVEINSTANCE_CALLED:Ljava/lang/String; = "KEY_IS_SAVEINSTANCE_CALLED"

.field private static final KEY_SAVEINSTANCE_CALLED_TIME:Ljava/lang/String; = "KEY_SAVEINSTANCE_CALLED_TIME"

.field public static final MLS_APPLICATION_INTENT:Ljava/lang/String; = "com.sec.musicliveshare.serverhelper"

.field public static final MORE_GAMELIST_UPDATED:Ljava/lang/String; = "MORE GAMELIST UPDATED"

.field public static final MULTI_VISION_APPLICATION_INTENT:Ljava/lang/String; = "com.samsung.sharevideo.ACTION_START_MV_HELP"

.field public static final MUSIC_LIVE_SHARE_PARTICIPANT_ID:Ljava/lang/String; = "MusicLiveShare"

.field private static final POPUP_X_POSITION:I = 0x0

.field private static final POPUP_Y_POSITION:I = -0x5

.field private static final PROGRESS_DIALOG_TYPE_ADD_FILES:Ljava/lang/String; = "addFile"

.field private static final PROGRESS_DIALOG_TYPE_LAUNCH_MARKET:Ljava/lang/String; = "launchMarket"

.field public static final PreLoadedApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SELECTED_CONTENT_TYPE:Ljava/lang/String; = "ContentShareSelection"

.field public static final SESSION_CREATOR_FRAGMENT_TAG:Ljava/lang/String; = "SESSION_CREATOR_FRAGMENT"

.field public static final SESSION_EXTRA:Ljava/lang/String; = "SessionExtra"

.field public static final SHARE_VIDEO_PARTICIPANT_ID:Ljava/lang/String; = "ShareVideo"

.field private static final TAG:Ljava/lang/String;

.field public static final TAG_CREATE_SESSION_FAILURE:Ljava/lang/String; = "CREATE_SESSION_FAILURE"

.field public static final TAG_MENU_TYPE:Ljava/lang/String; = "TAG_MENU_TYPE"

.field public static final TAG_NFC_MESSAGE:Ljava/lang/String; = "TAG_NFC_MESSAGE"

.field public static final TAG_OBJECT_STORE_HASH:Ljava/lang/String; = "ObjectStoreHash"

.field public static final TAG_PROGRESS_DIALOG_FRAGMENT:Ljava/lang/String; = "PROGRESS_DIALOG_FRAGMENT"

.field public static final TAG_SERVICE_TYPE:Ljava/lang/String; = "TAG_SERVICE_TYPE"

.field public static final TAG_SESSION_CRASH_DIALOG_FRAGMENT:Ljava/lang/String; = "SESSION_DIALOG_FRAGMENT"

.field public static final URIS_TO_SHARE_FROM_EXTERNAL:Ljava/lang/String; = "URIS_TO_SHARE_FROM_EXTERNAL"


# instance fields
.field private fragmentExit:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

.field private final mActivitiesStateReceiver:Landroid/content/BroadcastReceiver;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mContentShareSelection:I

.field private mExternalContentType:I

.field private mFromStartActivityForExternalApp:Z

.field private mGPSDKNextAction:Ljava/lang/String;

.field private mGPSDKType:I

.field private mGroupCastHotspotTurnedOn:Z

.field private final mHowManyDisclaimerShowing:I

.field private mIsShownShareItem:Z

.field private final mIsStackPeerChangeInfo:Z

.field private mIsStartFromGPSDK:Z

.field private mIsStartedByCamcoderApp:Z

.field private mIsStartedByMusicApp:Z

.field private mIsStartedByVideoApp:Z

.field private mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

.field private mOnSaveInstanceStateCalled:Z

.field private mOnSaveInstanceStateCalledTime:J

.field private mService:Lcom/samsung/groupcast/service/GroupPlayService;

.field private mSessionPin:Ljava/lang/String;

.field private mTotalNumberParticipants:I

.field protected mWifiAlertShown:Z

.field private final preventAPStateListen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    const-class v0, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->TAG:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    .line 123
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "MyAllFiles"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "MyDocumentPages"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "MyImages"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "MusicLiveShare"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "ShareVideo"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    sget-object v0, Lcom/samsung/groupcast/application/main/MainActivity;->PreLoadedApps:Ljava/util/ArrayList;

    const-string v1, "GroupCamcoder"

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;-><init>()V

    .line 142
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mFromStartActivityForExternalApp:Z

    .line 143
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStackPeerChangeInfo:Z

    .line 144
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiAlertShown:Z

    .line 147
    iput v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mHowManyDisclaimerShowing:I

    .line 148
    iput v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mContentShareSelection:I

    .line 149
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mExternalContentType:I

    .line 159
    new-instance v0, Lcom/samsung/groupcast/application/main/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/main/MainActivity$1;-><init>(Lcom/samsung/groupcast/application/main/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mActivitiesStateReceiver:Landroid/content/BroadcastReceiver;

    .line 687
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->preventAPStateListen:Z

    .line 727
    new-instance v0, Lcom/samsung/groupcast/application/main/MainActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/application/main/MainActivity$3;-><init>(Lcom/samsung/groupcast/application/main/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method private IntentMusicLiveShare(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "playFrom"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1038
    const-string v2, ""

    .line 1040
    .local v2, "userName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1042
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1047
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mSessionName:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    invoke-static {v2, v3, p1, p2, v4}, Lcom/samsung/groupcast/application/IntentTools;->getMusicLiveShareIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/groupcast/core/messaging/NfcMsg;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1063
    .local v1, "intent":Landroid/content/Intent;
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    :try_start_1
    const-string v3, "com.sec.android.app.musicplayer"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1065
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "launchedAlready"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1068
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "launchedAlready"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1069
    const v3, 0x10008

    invoke-virtual {p0, v1, v3}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1070
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "launchedAlready"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1082
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_1
    iput-boolean v6, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 1083
    return-void

    .line 1044
    :cond_2
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1073
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 1074
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    :try_start_2
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 1077
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v0

    .line 1078
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "#### is not MusicServerActivity.apk install ####"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1079
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getToastContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0800af

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/main/MainActivity;)Lcom/samsung/groupcast/net/wifi/WifiUtils;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/main/MainActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/groupcast/application/main/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/main/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/application/main/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/application/main/MainActivity;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGroupCastHotspotTurnedOn:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/groupcast/application/main/MainActivity;Lcom/samsung/groupcast/service/GroupPlayService;)Lcom/samsung/groupcast/service/GroupPlayService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/main/MainActivity;
    .param p1, "x1"    # Lcom/samsung/groupcast/service/GroupPlayService;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mService:Lcom/samsung/groupcast/service/GroupPlayService;

    return-object p1
.end method

.method private checkAndJumpToOthers()V
    .locals 2

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v0, :cond_1

    .line 919
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v0, :cond_0

    .line 920
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    if-eqz v0, :cond_0

    .line 921
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkMusicLiveShareProcess()V

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 924
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    if-eqz v0, :cond_2

    .line 925
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkMusicLiveShareProcess()V

    goto :goto_0

    .line 926
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    if-eqz v0, :cond_3

    .line 927
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkShareVideoProcess()V

    goto :goto_0

    .line 928
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    if-eqz v0, :cond_4

    .line 929
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkShareCamcoderProcess()V

    goto :goto_0

    .line 930
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    if-eqz v0, :cond_0

    .line 931
    iget v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGPSDKType:I

    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGPSDKNextAction:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->startAction(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private checkMusicLiveShareProcess()V
    .locals 3

    .prologue
    .line 1025
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "URIS_TO_SHARE_FROM_EXTERNAL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1028
    .local v0, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    .line 1029
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsShownShareItem:Z

    .line 1030
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1031
    const-string v1, "com.sec.android.app.musicplayer"

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentMusicLiveShare(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1035
    :goto_0
    return-void

    .line 1033
    :cond_0
    const-string v1, "com.sec.android.app.musicplayer"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentMusicLiveShare(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private checkShareCamcoderProcess()V
    .locals 4

    .prologue
    .line 1017
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "filePath"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1018
    .local v0, "mPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "uri"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1019
    .local v1, "mUri":Ljava/lang/String;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    .line 1020
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsShownShareItem:Z

    .line 1021
    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentGroupCamcoder(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    return-void
.end method

.method private checkShareVideoProcess()V
    .locals 4

    .prologue
    .line 1009
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "filePath"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1010
    .local v0, "mPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "uri"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1011
    .local v1, "mUri":Ljava/lang/String;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    .line 1012
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsShownShareItem:Z

    .line 1013
    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentShareVideo(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 3

    .prologue
    .line 936
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 940
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    if-eqz v0, :cond_0

    .line 942
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 943
    const/4 v0, 0x0

    .line 946
    :cond_0
    :goto_0
    return-void

    .line 944
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getToastContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1086
    if-eqz p0, :cond_0

    .end local p0    # "this":Lcom/samsung/groupcast/application/main/MainActivity;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/samsung/groupcast/application/main/MainActivity;
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    goto :goto_0
.end method

.method private newManifestGeneration()V
    .locals 9

    .prologue
    .line 790
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 791
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "startManifestGeneration"

    const-string v8, "manifest already generated"

    invoke-static {v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 830
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isManifestGenerated()Z

    move-result v6

    if-nez v6, :cond_0

    .line 796
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 798
    .local v5, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/groupcast/misc/utility/FileTools;->getTempDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "unsupported_file.UNSUPPORTED_FILE"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 801
    .local v3, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 803
    .local v4, "unsupportedFile":Ljava/io/File;
    if-eqz v4, :cond_3

    .line 804
    const/4 v1, 0x0

    .line 806
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    if-eqz v2, :cond_2

    .line 808
    :try_start_1
    const-string v6, "GP20"

    invoke-virtual {v2, v6}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 815
    :cond_2
    if-eqz v2, :cond_5

    .line 817
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 818
    const/4 v1, 0x0

    .line 825
    .end local v2    # "fw":Ljava/io/FileWriter;
    :cond_3
    :goto_1
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    const-string v6, "GroupPlay"

    invoke-virtual {p0, v6, v5}, Lcom/samsung/groupcast/application/main/MainActivity;->startNewManifestGeneration(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 820
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 811
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :catch_1
    move-exception v0

    .line 812
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 815
    if-eqz v1, :cond_3

    .line 817
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 818
    const/4 v1, 0x0

    goto :goto_1

    .line 820
    :catch_2
    move-exception v0

    .line 821
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 814
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 815
    :goto_3
    if-eqz v1, :cond_4

    .line 817
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 818
    const/4 v1, 0x0

    .line 822
    :cond_4
    :goto_4
    throw v6

    .line 820
    :catch_3
    move-exception v0

    .line 821
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 814
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_3

    .line 811
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_5
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method private onSessionFromNetwork(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V
    .locals 1
    .param p1, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    const/4 v0, 0x0

    .line 683
    invoke-virtual {p0, p1, v0, v0}, Lcom/samsung/groupcast/application/main/MainActivity;->setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    .line 684
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/main/MainActivity;->storeNewManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 685
    return-void
.end method

.method private prepareSession(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 8
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    const/4 v2, 0x0

    .line 763
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "prepareSession"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 765
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 766
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "prepareSession"

    const-string v7, "session already created, ignoring"

    invoke-static {v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 785
    :goto_0
    return-object v2

    .line 770
    :cond_0
    const/4 v4, 0x1

    .line 771
    .local v4, "version":I
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    .line 775
    .local v3, "sourceName":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mSessionPin:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mSessionName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    invoke-static {v4, v5, v3, v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->createFreshSession(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .local v2, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    goto :goto_0

    .line 778
    .end local v2    # "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Ljava/lang/Exception;
    const v5, 0x7f080114

    invoke-static {v5}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    move-result-object v1

    .line 781
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "SESSION_DIALOG_FRAGMENT"

    invoke-virtual {v1, v5, v6}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showCreateSessionFailureDialog()V
    .locals 3

    .prologue
    .line 996
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "CREATE_SESSION_FAILURE"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1003
    const v1, 0x7f080018

    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    move-result-object v0

    .line 1005
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "CREATE_SESSION_FAILURE"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startGroupPlayService()V
    .locals 2

    .prologue
    .line 742
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 744
    const/4 v0, 0x0

    .line 745
    .local v0, "typeFromExternalApp":I
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v1, :cond_1

    .line 746
    const/4 v0, 0x2

    .line 750
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/service/GroupPlayService;->StartNBindGroupPlayService(ILandroid/content/ServiceConnection;)V

    .line 752
    return-void

    .line 747
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    if-eqz v1, :cond_0

    .line 748
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method BandCacheRelease()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 674
    iput-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    .line 675
    iput-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mMenuType:Ljava/lang/String;

    .line 676
    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->setRegister_Extra_App(Z)V

    .line 677
    invoke-static {v1}, Lcom/samsung/groupcast/application/App;->SET_EXTERNAL_APP(Z)V

    .line 678
    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setScreenId(Ljava/lang/String;)V

    .line 679
    invoke-static {v0}, Lcom/samsung/groupcast/application/App;->setPeerScreenId(Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method protected BandInterfaceRelease()V
    .locals 3

    .prologue
    .line 659
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BandInterfaceRelease [MainActivity]  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    if-eqz v0, :cond_0

    .line 662
    const-string v0, "GroupPlayBand"

    const-string v1, "unregister()"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v0}, Lcom/sec/android/band/BandInterface;->unregister()Z

    .line 670
    :cond_0
    return-void
.end method

.method public endGroupPlay()V
    .locals 1

    .prologue
    .line 1090
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/StatLog;->saveData()V

    .line 1092
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->clearConfirms()V

    .line 1093
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayService;->StopNUnbindGroupPlayService(Landroid/content/ServiceConnection;)V

    .line 1094
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 1096
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->finish()V

    .line 1097
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "EXIT_CONFIRM_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 648
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    move-result-object v0

    .line 650
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "EXIT_CONFIRM_FRAGMENT"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 652
    iput-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->fragmentExit:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    .line 654
    .end local v0    # "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x1

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 217
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 219
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "ObjectStoreHash"

    invoke-virtual {v7, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 220
    .local v3, "objectStoreHash":I
    invoke-static {v3}, Lcom/samsung/groupcast/application/ObjectStore;->isValid(I)Z

    move-result v7

    if-nez v7, :cond_0

    .line 221
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->finish()V

    .line 395
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "GROUP_NAME"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mSessionName:Ljava/lang/String;

    .line 226
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "PASSWORD"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mSessionPin:Ljava/lang/String;

    .line 228
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "AP_NAME"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 229
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "GC_HOTSPOT_TURNED_ON"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGroupCastHotspotTurnedOn:Z

    .line 232
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "TAG_NFC_MESSAGE"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/samsung/groupcast/core/messaging/NfcMsg;

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mNfcMsg:Lcom/samsung/groupcast/core/messaging/NfcMsg;

    .line 233
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "TAG_SERVICE_TYPE"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    .line 234
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "TAG_MENU_TYPE"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mMenuType:Ljava/lang/String;

    .line 235
    const-string v7, "GroupPlayBand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MainActivity get ServiceType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v7, "GroupPlayBand"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MainActivity get MenuType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mMenuType:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "URIS_TO_SHARE_FROM_EXTERNAL"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 239
    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v7, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "ExternalAppContentType"

    invoke-virtual {v7, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mExternalContentType:I

    .line 244
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "DJ_MODE_VIA_GROUP_PLAY_FROM_MUSIC_PLAYER"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    .line 245
    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v7, :cond_2

    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    if-eqz v7, :cond_2

    .line 246
    iput-boolean v10, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 248
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_VIDEO_PLAYER"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    .line 249
    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    if-eqz v7, :cond_3

    .line 250
    iput-boolean v10, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 252
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_CAMCODER_PLAYER"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    .line 254
    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v7, :cond_4

    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    if-eqz v7, :cond_4

    .line 255
    iput-boolean v10, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 258
    :cond_4
    iget-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    if-eqz v7, :cond_5

    iget-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v7, :cond_5

    .line 259
    iput-boolean v12, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mFromStartActivityForExternalApp:Z

    .line 260
    iget-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    invoke-virtual {v7}, Lcom/sec/android/band/BandInterface;->register()Z

    .line 265
    :cond_5
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 267
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getUsersCount()I

    move-result v7

    iput v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mTotalNumberParticipants:I

    .line 270
    new-instance v7, Lcom/samsung/groupcast/application/main/MainActivity$2;

    invoke-direct {v7, p0}, Lcom/samsung/groupcast/application/main/MainActivity$2;-><init>(Lcom/samsung/groupcast/application/main/MainActivity;)V

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 329
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "SessionExtra"

    invoke-virtual {v7, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 330
    .local v5, "sessionId":I
    if-lez v5, :cond_6

    .line 331
    invoke-static {v5}, Lcom/samsung/groupcast/application/ObjectStore;->release(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 332
    .local v4, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    if-eqz v4, :cond_6

    .line 333
    invoke-direct {p0, v4}, Lcom/samsung/groupcast/application/main/MainActivity;->onSessionFromNetwork(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    .line 337
    .end local v4    # "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    :cond_6
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->startGroupPlayService()V

    .line 340
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 341
    .local v2, "intentFilter":Landroid/content/IntentFilter;
    const-string v7, "GROUPPLAY GAMER JOINED"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 342
    const-string v7, "GROUPPLAY GAMER LEFT"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 343
    const-string v7, "ACTION_BLE_REQEUST_TO_RESTART"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 344
    const-string v7, "GP_REMOTE_DISCONNECT"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 345
    const-string v7, "com.samsung.groupcast.action.APP_CLOSE_NOTIFICATION_TO_GP2"

    invoke-virtual {v2, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 346
    iget-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mActivitiesStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v2}, Lcom/samsung/groupcast/application/main/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 348
    iget-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->setActivity(Landroid/app/Activity;)V

    .line 349
    iget-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->setSessionStarted(Z)V

    .line 352
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    .line 355
    if-eqz p1, :cond_8

    .line 356
    const-string v7, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 357
    const-string v7, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    .line 361
    :cond_7
    const-string v7, "KEY_IS_SAVEINSTANCE_CALLED"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 362
    const-string v7, "KEY_IS_SAVEINSTANCE_CALLED"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalled:Z

    .line 364
    const-string v7, "KEY_IS_SAVEINSTANCE_CALLED"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 365
    const-string v7, "KEY_SAVEINSTANCE_CALLED_TIME"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalledTime:J

    .line 369
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "APPLICATION_INTERFACE_NEXT_ACTION"

    invoke-virtual {v7, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGPSDKNextAction:Ljava/lang/String;

    .line 370
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "APPLICATION_INTERFACE_TYPE"

    invoke-virtual {v7, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mGPSDKType:I

    .line 372
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 376
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v7

    if-nez v7, :cond_9

    .line 378
    const-string v7, "force to make SharedExperienceManager"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 379
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    invoke-direct {v1, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 380
    .local v1, "contentMap":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v0, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 382
    .local v0, "contentBroker":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v6, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 385
    .local v6, "sharedExperienceManager":Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v7

    invoke-virtual {p0, v7, v0, v6}, Lcom/samsung/groupcast/application/main/MainActivity;->setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    .line 389
    .end local v0    # "contentBroker":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .end local v1    # "contentMap":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    .end local v6    # "sharedExperienceManager":Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    :cond_9
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkAndJumpToOthers()V

    .line 394
    :goto_1
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->createGpRunningStateCheckFile()V

    goto/16 :goto_0

    .line 391
    :cond_a
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->newManifestGeneration()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 545
    sget-object v2, Lcom/samsung/groupcast/application/main/MainActivity;->TAG:Ljava/lang/String;

    const-string v3, "onDestroy"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->setActivity(Landroid/app/Activity;)V

    .line 548
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->setSessionStarted(Z)V

    .line 551
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isChangingConfigurations()Z

    move-result v2

    if-nez v2, :cond_0

    .line 552
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 553
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    .line 554
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 563
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isChangingConfigurations()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 564
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->disableShareExperienceObjects()V

    .line 566
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const/4 v2, 0x0

    check-cast v2, Landroid/os/Bundle;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->replaceExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 567
    const-string v2, "clears old intent"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 569
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mActivitiesStateReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    .line 571
    :try_start_1
    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mActivitiesStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/application/main/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 578
    :cond_2
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 581
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 582
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->disable()V

    .line 586
    :cond_3
    sget-object v2, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 598
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getHelpState()I

    move-result v2

    if-nez v2, :cond_5

    .line 599
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->stopGroupPlayService()V

    .line 602
    :cond_5
    invoke-super {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onDestroy()V

    .line 603
    const-string v2, "--"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 604
    return-void

    .line 555
    :cond_6
    :try_start_3
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GroupPlay-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 558
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->disconnectFromCurrentAP()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 594
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 559
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_4
    iget-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiAlertShown:Z

    if-eqz v2, :cond_0

    .line 560
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->disconnectFromCurrentAP()V

    goto/16 :goto_0

    .line 572
    :catch_1
    move-exception v1

    .line 573
    .local v1, "ie":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 588
    .end local v1    # "ie":Ljava/lang/Exception;
    :pswitch_0
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->setWifiMotherFatherOxygenDelegate(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;)V

    .line 589
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 586
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "previousManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p4, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 978
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 980
    return-void
.end method

.method protected onManifestCreationFailed()V
    .locals 3

    .prologue
    .line 984
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Manifest storing failed!"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 985
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 988
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    if-eqz v0, :cond_0

    .line 989
    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V

    .line 991
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->showCreateSessionFailureDialog()V

    .line 992
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->finish()V

    .line 993
    return-void
.end method

.method public onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 7
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    const/4 v6, 0x0

    .line 834
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onCreateManifestComplete"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "manifest"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v6, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 835
    const-string v1, "SHE"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 836
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    if-nez v1, :cond_0

    .line 837
    const-string v1, "SHE"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 838
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/application/main/MainActivity;->prepareSession(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v0

    .line 839
    .local v0, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-virtual {p0, v0, v6, v6}, Lcom/samsung/groupcast/application/main/MainActivity;->setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    .line 840
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 843
    .end local v0    # "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    :goto_0
    return-void

    .line 842
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    goto :goto_0
.end method

.method public onManifestGenerationProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 4
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 873
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 876
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onManifestGenerationProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 878
    if-eqz v0, :cond_0

    .line 879
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getProgressRatio()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 880
    .local v1, "percentage":I
    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setProgressPercentage(I)V

    .line 882
    .end local v1    # "percentage":I
    :cond_0
    return-void
.end method

.method protected onManifestUpdated(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 4
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    const/4 v3, 0x0

    .line 886
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 887
    const-string v1, "MyImages"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 888
    const/4 v1, 0x3

    iput v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mContentShareSelection:I

    .line 909
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    .line 910
    iput-object v3, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mMenuType:Ljava/lang/String;

    .line 913
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->checkAndJumpToOthers()V

    .line 914
    invoke-direct {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->dismissProgressDialog()V

    .line 915
    return-void

    .line 889
    :cond_2
    const-string v1, "MyDocumentPages"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 890
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mContentShareSelection:I

    goto :goto_0

    .line 891
    :cond_3
    const-string v1, "MUSIC_LIVE_SHARE_SCREEN"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 892
    const-string v1, "com.samsung.groupcast"

    invoke-virtual {p0, v1, v3}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentMusicLiveShareReceiver(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 893
    :cond_4
    const-string v1, "ShareVideo"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 894
    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentShareVideo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 902
    :cond_5
    const-string v1, "GroupCamcoder"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 903
    const-string v1, ""

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lcom/samsung/groupcast/application/main/MainActivity;->IntentGroupCamcoder(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 904
    :cond_6
    const-string v1, "External_App"

    iget-object v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 905
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/App;

    .line 906
    .local v0, "app":Lcom/samsung/groupcast/application/App;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getPeerScreenId()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->startLaunchActivity(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 608
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 610
    const-string v0, ""

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 612
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/application/main/MainActivity;->setIntent(Landroid/content/Intent;)V

    .line 614
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "URIS_TO_SHARE_FROM_EXTERNAL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 620
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->fragmentExit:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->fragmentExit:Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->dismiss()V

    .line 624
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DJ_MODE_VIA_GROUP_PLAY_FROM_MUSIC_PLAYER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    .line 625
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByMusicApp:Z

    if-eqz v0, :cond_1

    .line 626
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 627
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_VIDEO_PLAYER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    .line 628
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByVideoApp:Z

    if-eqz v0, :cond_2

    .line 629
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 630
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "SHARE_VIDEO_VIA_GROUP_PLAY_FROM_CAMCODER_PLAYER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    .line 632
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByCamcoderApp:Z

    if-eqz v0, :cond_3

    .line 633
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartedByExternalApp:Z

    .line 638
    :cond_3
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 495
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 496
    const-string v0, "GroupPlayBand"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    invoke-super {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onPause()V

    .line 503
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mBandInterface:Lcom/sec/android/band/BandInterface;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mFromStartActivityForExternalApp:Z

    if-eqz v0, :cond_0

    .line 505
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->BandInterfaceRelease()V

    .line 506
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->BandCacheRelease()V

    .line 507
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mFromStartActivityForExternalApp:Z

    .line 511
    :cond_0
    return-void
.end method

.method public onPeerChanged()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method protected onRefreshSharedExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V
    .locals 9
    .param p1, "generator"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 847
    if-nez p1, :cond_1

    .line 848
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    invoke-direct {v1, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 849
    .local v1, "contentMap":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v0, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 851
    .local v0, "contentBroker":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v6, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 853
    .local v6, "sharedExperienceManager":Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v7

    invoke-virtual {p0, v7, v0, v6}, Lcom/samsung/groupcast/application/main/MainActivity;->setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    .line 869
    .end local v0    # "contentBroker":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .end local v1    # "contentMap":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    .end local v6    # "sharedExperienceManager":Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    :cond_0
    :goto_0
    return-void

    .line 854
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSharedExperienceManager()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    move-result-object v7

    if-nez v7, :cond_0

    .line 855
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    invoke-direct {v1, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 856
    .restart local v1    # "contentMap":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->getMappedPaths()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 857
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 858
    .local v4, "id":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 859
    .local v5, "path":Ljava/lang/String;
    invoke-virtual {v1, v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->addItemMapping(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 861
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "path":Ljava/lang/String;
    :cond_2
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v0, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 863
    .restart local v0    # "contentBroker":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getMessagingClient()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v8

    invoke-direct {v6, v7, v8, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V

    .line 866
    .restart local v6    # "sharedExperienceManager":Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v7

    invoke-virtual {p0, v7, v0, v6}, Lcom/samsung/groupcast/application/main/MainActivity;->setShareExperienceObjects(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 424
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "onResume"

    invoke-static {v0, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 425
    const-string v0, "GroupPlayBand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onResume"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalledTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalled:Z

    .line 431
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalled:Z

    if-nez v0, :cond_1

    .line 432
    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsShownShareItem:Z

    if-eqz v0, :cond_1

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 455
    :cond_1
    :goto_1
    iput-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mOnSaveInstanceStateCalled:Z

    .line 456
    invoke-super {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onResume()V

    .line 491
    return-void

    :cond_2
    move v0, v1

    .line 427
    goto :goto_0

    .line 448
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->finish()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1102
    const-string v0, "onSaveInstanceState()"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1103
    const-string v0, "KEY_IS_SAVEINSTANCE_CALLED"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1104
    const-string v0, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    iget-boolean v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mIsStartFromGPSDK:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1105
    const-string v0, "KEY_SAVEINSTANCE_CALLED_TIME"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1106
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1107
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 399
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onStart"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 400
    invoke-super {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStart()V

    .line 402
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 404
    .local v0, "filter":Landroid/content/IntentFilter;
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 417
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/groupcast/application/main/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 420
    :cond_0
    return-void

    .line 406
    :pswitch_0
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    .line 410
    :pswitch_1
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 411
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto :goto_0

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 515
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 516
    invoke-super {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->onStop()V

    .line 518
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getHelpState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    const-string v0, "return!!!"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 532
    :goto_0
    :pswitch_0
    return-void

    .line 523
    :cond_0
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 528
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->unregisterWifiReceiver()V

    goto :goto_0

    .line 523
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onViewerPeerChanged()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method protected showDisconnectAlert()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 690
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->isFinishing()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 722
    :cond_0
    :goto_0
    return v3

    .line 696
    :cond_1
    iget-boolean v5, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiAlertShown:Z

    if-nez v5, :cond_0

    .line 699
    iput-boolean v4, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mWifiAlertShown:Z

    .line 700
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 702
    invoke-static {}, Lcom/samsung/groupplay/sdk/GroupPlaySessionClosingHandler;->notifyTermination()V

    .line 705
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "EXIT_WIFICLOSE_FRAGMENT"

    invoke-virtual {v3, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_2

    .line 707
    invoke-static {v4}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    move-result-object v1

    .line 710
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->setCancelable(Z)V

    .line 711
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "EXIT_WIFICLOSE_FRAGMENT"

    invoke-virtual {v1, v3, v5}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    :cond_2
    :goto_1
    move v3, v4

    .line 722
    goto :goto_0

    .line 712
    .restart local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 715
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->createAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 716
    :catch_1
    move-exception v2

    .line 717
    .local v2, "ie":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public startAction(ILjava/lang/String;)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 949
    packed-switch p1, :pswitch_data_0

    .line 973
    :goto_0
    return-void

    .line 953
    :pswitch_0
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/groupcast/application/main/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 954
    .local v0, "am":Landroid/app/ActivityManager;
    const v5, 0x7fffffff

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 956
    .local v4, "recentTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 957
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 958
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 959
    .local v3, "rap":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 960
    iget v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Landroid/app/ActivityManager;->moveTaskToFront(II)V

    goto :goto_0

    .line 966
    .end local v3    # "rap":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/main/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, p2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 967
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 949
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public stopGroupPlayService()V
    .locals 1

    .prologue
    .line 755
    const-string v0, "stopsvc"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 756
    iget-object v0, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnection:Landroid/content/ServiceConnection;

    invoke-static {v0}, Lcom/samsung/groupcast/service/GroupPlayService;->StopNUnbindGroupPlayService(Landroid/content/ServiceConnection;)V

    .line 758
    return-void
.end method

.method public unregisterWifiReceiver()V
    .locals 2

    .prologue
    .line 536
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 537
    iget-object v1, p0, Lcom/samsung/groupcast/application/main/MainActivity;->mConnectivityChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/application/main/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 538
    :catch_0
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
