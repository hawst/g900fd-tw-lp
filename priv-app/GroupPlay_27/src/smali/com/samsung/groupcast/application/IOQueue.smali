.class public Lcom/samsung/groupcast/application/IOQueue;
.super Landroid/os/HandlerThread;
.source "IOQueue.java"


# static fields
.field private static final THREAD_NAME:Ljava/lang/String; = "IOThread"

.field private static final THREAD_PRIORITY:I = 0x1

.field private static final sHandler:Landroid/os/Handler;

.field private static final sInstance:Lcom/samsung/groupcast/application/IOQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    new-instance v0, Lcom/samsung/groupcast/application/IOQueue;

    const-string v1, "IOThread"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/application/IOQueue;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/application/IOQueue;->sInstance:Lcom/samsung/groupcast/application/IOQueue;

    .line 16
    sget-object v0, Lcom/samsung/groupcast/application/IOQueue;->sInstance:Lcom/samsung/groupcast/application/IOQueue;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/IOQueue;->start()V

    .line 17
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/samsung/groupcast/application/IOQueue;->sInstance:Lcom/samsung/groupcast/application/IOQueue;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/IOQueue;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/groupcast/application/IOQueue;->sHandler:Landroid/os/Handler;

    .line 18
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "priority"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 22
    return-void
.end method

.method public static cancel(Ljava/lang/Runnable;)V
    .locals 4
    .param p0, "runnable"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, 0x1

    .line 40
    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, v2, v2}, Ljava/util/concurrent/Semaphore;-><init>(IZ)V

    .line 42
    .local v1, "waitLock":Ljava/util/concurrent/Semaphore;
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    sget-object v2, Lcom/samsung/groupcast/application/IOQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v2, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 48
    sget-object v2, Lcom/samsung/groupcast/application/IOQueue;->sHandler:Landroid/os/Handler;

    new-instance v3, Lcom/samsung/groupcast/application/IOQueue$1;

    invoke-direct {v3, v1}, Lcom/samsung/groupcast/application/IOQueue$1;-><init>(Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 55
    :try_start_1
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 60
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 56
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 57
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public static post(Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/groupcast/application/IOQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 26
    return-void
.end method

.method public static postDelayed(Ljava/lang/Runnable;J)V
    .locals 1
    .param p0, "runnable"    # Ljava/lang/Runnable;
    .param p1, "delayMilliseconds"    # J

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/groupcast/application/IOQueue;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 30
    return-void
.end method
