.class public Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;
.super Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
.source "WifiConnectDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$1;,
        Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;
    }
.end annotation


# static fields
.field private static final KEY_BSSID:Ljava/lang/String; = "KEY_BSSID"

.field private static final KEY_PASS:Ljava/lang/String; = "KEY_PASS"

.field private static final KEY_SSID:Ljava/lang/String; = "KEY_SSID"

.field private static final TAG_POOR_CONNECTION_DIALOG_FRAGMENT:Ljava/lang/String; = "TAG_POOR_CONNECTION_DIALOG_FRAGMENT"

.field private static mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBSSID:Ljava/lang/String;

.field private final mFinished:Z

.field private mPASS:Ljava/lang/String;

.field private mSSID:Ljava/lang/String;

.field private mSavedInstanceStateCalled:Z

.field private final mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    .line 28
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    .line 29
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .line 30
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mFinished:Z

    .line 31
    iput-boolean v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    .line 33
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;
    .locals 2

    .prologue
    .line 262
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    const-class v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 263
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_0
    return-object v0

    .line 264
    :catch_0
    move-exception v0

    .line 266
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private safe_dismiss()V
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 277
    :cond_0
    return-void
.end method


# virtual methods
.method public connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "SSID"    # Ljava/lang/String;
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "PASS"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 227
    const-string v0, "Requesting connect to %s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 229
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    .line 230
    iput-object p2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    .line 231
    iput-object p3, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    .line 233
    sget-object v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->setDelegate(Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;)V

    .line 235
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 258
    :goto_0
    return-void

    .line 237
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v1, v2, p1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 238
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isOxygenConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V

    .line 240
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->turnOxygenOnOff(Z)V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1, p2, p1, p3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectToOxygen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 246
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v1, v2, p1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 247
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1, p2, p1, p3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 251
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    .line 252
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v1, v2, p2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 254
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->joinSession(Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public dismiss()V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->safe_dismiss()V

    .line 272
    return-void
.end method

.method public handleMessage(Landroid/os/Handler;Landroid/os/Message;)V
    .locals 9
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    const v8, 0x7f08001e

    const v7, 0x7f080009

    const/4 v6, 0x1

    .line 166
    iget-boolean v4, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    if-eqz v4, :cond_0

    .line 167
    const-string v4, "Ignoring due to savedInstanceState"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 224
    :goto_0
    return-void

    .line 171
    :cond_0
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 172
    .local v0, "SSID":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->values()[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    move-result-object v4

    iget v5, p2, Landroid/os/Message;->what:I

    aget-object v3, v4, v5

    .line 173
    .local v3, "status":Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->values()[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    move-result-object v4

    iget v5, p2, Landroid/os/Message;->arg1:I

    aget-object v2, v4, v5

    .line 175
    .local v2, "reason":Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Connect status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    if-nez v4, :cond_1

    .line 178
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->unregisterListener(Landroid/os/Handler;)V

    goto :goto_0

    .line 182
    :cond_1
    sget-object v4, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$1;->$SwitchMap$com$samsung$groupcast$net$wifi$WifiUtils$WifiConnectStatus:[I

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 184
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 185
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v6, v0, v5}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;->onWifiConnectionFinished(ZLjava/lang/String;Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    .line 186
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->dismiss()V

    goto :goto_0

    .line 191
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/groupcast/application/App;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 195
    :pswitch_2
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/samsung/groupcast/application/App;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 199
    :pswitch_3
    invoke-static {v8}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :pswitch_4
    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->POOR_CONNECTION:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    if-ne v2, v4, :cond_3

    .line 204
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "TAG_POOR_CONNECTION_DIALOG_FRAGMENT"

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_3

    .line 205
    const v4, 0x7f08008a

    invoke-static {v4}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->newInstance(I)Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;

    move-result-object v1

    .line 207
    .local v1, "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    iget-boolean v4, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    if-nez v4, :cond_3

    .line 208
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "TAG_POOR_CONNECTION_DIALOG_FRAGMENT"

    invoke-virtual {v1, v4, v5}, Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 211
    .end local v1    # "fragment":Lcom/samsung/groupcast/application/fragment/MessageDialogFragment;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 212
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5, v0, v2}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;->onWifiConnectionFinished(ZLjava/lang/String;Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    .line 213
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->dismiss()V

    goto/16 :goto_0

    .line 217
    :pswitch_5
    invoke-virtual {p0, v6}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setCancelable(Z)V

    .line 218
    invoke-static {v8}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x0

    .line 91
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 93
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    .line 95
    sget-object v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->setDelegate(Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;)V

    .line 98
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const v1, 0x7f0800a1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    .line 118
    const-string v0, "WifiDF:onAttached"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 121
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 136
    :goto_0
    return-void

    .line 101
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ssid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 108
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bssid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 123
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectToOxygen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 127
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 131
    :pswitch_4
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScan()V

    .line 132
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->joinSession(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 121
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    .line 65
    if-nez p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 68
    :cond_0
    const-string v0, "savedInstanceState not null"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 70
    const-string v0, "KEY_SSID"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    .line 71
    const-string v0, "KEY_PASS"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    .line 72
    const-string v0, "KEY_BSSID"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    if-nez p3, :cond_0

    .line 85
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const v1, 0x7f080009

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->setText(Ljava/lang/String;)V

    .line 86
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v3, 0x0

    .line 140
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 144
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 155
    :goto_0
    sget-object v0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;->setDelegate(Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;)V

    .line 157
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 163
    :cond_0
    :goto_1
    return-void

    .line 147
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->unregisterListener(Landroid/os/Handler;)V

    goto :goto_0

    .line 150
    :pswitch_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mStaticHandler:Lcom/samsung/groupcast/application/start/WifiConnectDialogStaticHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->unregisterListener(Landroid/os/Handler;)V

    goto :goto_0

    .line 160
    :cond_1
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    invoke-direct {p0}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->getDelegate()Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->USER_REQUEST:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-interface {v0, v1, v3, v2}, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment$WifiConnectDialogFragmentDelegate;->onWifiConnectionFinished(ZLjava/lang/String;Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    goto :goto_1

    .line 144
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onResume()V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    .line 80
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 52
    const-string v0, "KEY_SSID"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSSID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "KEY_PASS"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mPASS:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "KEY_BSSID"

    iget-object v1, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mBSSID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/application/start/WifiConnectDialogFragment;->mSavedInstanceStateCalled:Z

    .line 57
    return-void
.end method
