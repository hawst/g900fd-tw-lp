.class public Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
.super Landroid/app/DialogFragment;
.source "ExitConfirmDialogFragment.java"


# static fields
.field public static final MODE_EXIT_CONFIRM_ALL:I = 0x0

.field public static final MODE_EXIT_CONFIRM_ON_WIFI:I = 0x1

.field private static myType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->exitApplicationClearingTask()V

    return-void
.end method

.method private exitApplicationClearingTask()V
    .locals 4

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 155
    :goto_0
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    .line 146
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "APPLICATION_INTERFACE_IS_START_FROM_GPSDK"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivityForGame;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .restart local v0    # "intent":Landroid/content/Intent;
    :goto_1
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 153
    const-string v1, "EXIT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 154
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 149
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/groupcast/application/start/StartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_1
.end method

.method public static newInstance(I)Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 34
    sput p0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->myType:I

    .line 35
    new-instance v0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;

    invoke-direct {v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;-><init>()V

    .line 36
    .local v0, "fragment":Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;
    return-object v0
.end method


# virtual methods
.method public createAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const v4, 0x7f080001

    .line 57
    move-object v0, p1

    .line 58
    .local v0, "act":Landroid/app/Activity;
    const/4 v1, 0x0

    .line 62
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 65
    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    sget v2, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->myType:I

    if-nez v2, :cond_1

    .line 66
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 67
    const v2, 0x7f08003f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 68
    const v2, 0x7f08000e

    new-instance v3, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$1;-><init>(Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    const v2, 0x7f08000f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 107
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 91
    :cond_1
    sget v2, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->myType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 92
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 93
    const v2, 0x7f080057

    invoke-static {v2}, Lcom/samsung/groupcast/application/App;->getSanitizedString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 94
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 95
    const v2, 0x7f080014

    new-instance v3, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;

    invoke-direct {v3, p0, v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment$2;-><init>(Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 49
    :try_start_0
    const-class v1, Lcom/samsung/groupcast/application/main/MainActivity;

    invoke-static {p1, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->instanceOf(Ljava/lang/Object;Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->setRetainInstance(Z)V

    .line 43
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->createAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 135
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getRetainInstance()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 137
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 138
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 121
    sget v0, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->myType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 122
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/StatLog;->saveData()V

    .line 123
    invoke-static {}, Lcom/samsung/groupcast/application/fragment/DisclaimerDialogFragment;->clearConfirms()V

    .line 124
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->ClearLastSession()V

    .line 125
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/fragment/ExitConfirmDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 129
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 130
    return-void
.end method
