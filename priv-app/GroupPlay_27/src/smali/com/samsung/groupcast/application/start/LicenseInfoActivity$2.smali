.class Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;
.super Landroid/webkit/WebViewClient;
.source "LicenseInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 101
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    # setter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1, v0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$002(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 104
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v0}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->dismiss()V

    .line 107
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    new-instance v2, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    invoke-direct {v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;-><init>()V

    # setter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$002(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    .line 87
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setTitle(Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    const v3, 0x7f080022

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setText(Ljava/lang/String;)V

    .line 89
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->setCancelable(Z)V

    .line 90
    iget-object v1, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    # getter for: Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->mProgressDialogFragment:Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;
    invoke-static {v1}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->access$000(Lcom/samsung/groupcast/application/start/LicenseInfoActivity;)Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/application/start/LicenseInfoActivity$2;->this$0:Lcom/samsung/groupcast/application/start/LicenseInfoActivity;

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/start/LicenseInfoActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "LICENSE_PROGRESS_DIALOG_FRAGMENT"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/application/fragment/ProgressDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 96
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
