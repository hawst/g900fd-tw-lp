.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
.super Ljava/lang/Object;
.source "ContentBroker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;
    }
.end annotation


# static fields
.field private static final MAX_CONCURRENT_DOWNLOADS:I = 0x3


# instance fields
.field private final mContentFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

.field private final mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

.field private final mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/groupcast/misc/utility/MultiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

.field private mEnabled:Z

.field private mFetchLock:Ljava/lang/Object;

.field private final mRenderPageRequestRecords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningFetchRequests:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

.field private mWaitingFetchRequests:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mWaitingFetchRequestsHP:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V
    .locals 6
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "contentMap"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-direct {v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRenderPageRequestRecords:Ljava/util/HashMap;

    .line 48
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    .line 50
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequests:Ljava/util/LinkedHashMap;

    .line 52
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequestsHP:Ljava/util/LinkedHashMap;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    .line 59
    const-string v0, "messagingClient"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    const-string v0, "session"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    const-string v0, "contentMap"

    invoke-static {v0, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "ContentBroker"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "sessionId"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "contentMap"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 64
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 65
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    .line 66
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

    invoke-direct {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->handleContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->cleanupContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Lcom/samsung/groupcast/misc/utility/MultiMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->reprioritizeContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->completeContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->progressContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->processNextDownloadFetchRequest()V

    return-void
.end method

.method private cleanupContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 385
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 388
    .local v0, "contentId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRenderPageRequestRecords:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;

    .line 390
    .local v1, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;
    if-eqz v1, :cond_0

    .line 391
    iget-object v2, v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;->dependentContentRequests:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 393
    iget-object v2, v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;->dependentContentRequests:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 397
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRenderPageRequestRecords:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    :cond_0
    return-void
.end method

.method private completeContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 359
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v2, p1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 360
    .local v1, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    invoke-virtual {v1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 361
    invoke-direct {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->cleanupContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    .line 372
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

    if-eqz v2, :cond_0

    if-eqz p3, :cond_0

    .line 373
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

    invoke-interface {v2, p0, p1, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;->onContentAvailable(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 375
    .end local v1    # "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    :cond_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v2, p1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;)Ljava/util/Collection;

    .line 376
    return-void
.end method

.method private handleContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 162
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "contentId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v2, v0, p1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 164
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v1

    .line 166
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    if-nez v1, :cond_0

    .line 168
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$4;

    invoke-direct {v2, p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$4;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 192
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    monitor-enter v3

    .line 182
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->handleContentRequestViaContentMap(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    monitor-exit v3

    goto :goto_0

    .line 191
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 186
    :cond_1
    :try_start_1
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->handleContentRequestViaLocalProcessing(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 187
    monitor-exit v3

    goto :goto_0

    .line 190
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->handleContentRequestViaNetworkRetrieval(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z

    .line 191
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private handleContentRequestViaContentMap(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z
    .locals 10
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 195
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "contentId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v1

    .line 197
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {v5, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->getContentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "path":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 212
    :goto_0
    return v3

    .line 203
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "handleContentRequestViaContentMap"

    const-string v7, "success"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "item"

    aput-object v9, v8, v3

    aput-object v1, v8, v4

    const/4 v3, 0x2

    const-string v9, "path"

    aput-object v9, v8, v3

    const/4 v3, 0x3

    aput-object v2, v8, v3

    invoke-static {v5, v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 205
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$5;

    invoke-direct {v3, p0, v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$5;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    move v3, v4

    .line 212
    goto :goto_0
.end method

.method private handleContentRequestViaLocalProcessing(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z
    .locals 1
    .param p1, "contentRequest"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 268
    const/4 v0, 0x1

    return v0
.end method

.method private handleContentRequestViaNetworkRetrieval(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z
    .locals 11
    .param p1, "contentRequest"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 272
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v2

    .line 273
    .local v2, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "handleContentRequestViaNetworkRetrieval"

    const/4 v5, 0x0

    new-array v6, v10, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "item"

    aput-object v8, v6, v7

    aput-object v2, v6, v9

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 274
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 277
    .local v0, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    monitor-enter v4

    .line 279
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    .line 280
    .local v1, "fetchContentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    if-eqz v1, :cond_0

    .line 281
    monitor-exit v4

    .line 324
    :goto_0
    return v9

    .line 282
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fetchContentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    .line 283
    .restart local v1    # "fetchContentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    if-eqz v1, :cond_1

    .line 284
    monitor-exit v4

    goto :goto_0

    .line 322
    .end local v1    # "fetchContentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 287
    .restart local v1    # "fetchContentRequest":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;->createFetchContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    move-result-object v1

    .line 288
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;

    invoke-direct {v3, p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 315
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getPriority()I

    move-result v3

    if-ne v10, v3, :cond_2

    .line 316
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequestsHP:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    :goto_1
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->processNextDownloadFetchRequest()V

    .line 322
    monitor-exit v4

    goto :goto_0

    .line 318
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private processNextDownloadFetchRequest()V
    .locals 6

    .prologue
    .line 328
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    monitor-enter v3

    .line 330
    :goto_0
    const/4 v2, 0x3

    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v4

    if-le v2, v4, :cond_2

    .line 332
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequestsHP:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 333
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 335
    .local v1, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->start()V

    .line 337
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 354
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 341
    .restart local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 342
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 344
    .restart local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->start()V

    .line 347
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 354
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 355
    return-void
.end method

.method private progressContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 379
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v2, p1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 380
    .local v1, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    invoke-virtual {v1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V

    goto :goto_0

    .line 382
    .end local v1    # "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    :cond_0
    return-void
.end method

.method private reprioritizeContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 403
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 404
    .local v0, "contentId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRenderPageRequestRecords:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;

    .line 430
    .local v1, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$RenderPageRequestRecord;
    return-void
.end method


# virtual methods
.method public clearContentRequests()V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->clear()V

    .line 434
    return-void
.end method

.method public createContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 133
    const-string v1, "contentId"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$2;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)V

    invoke-direct {v0, p1, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;-><init>(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 142
    .local v0, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)V

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 157
    return-object v0
.end method

.method public deleteCachedContent()V
    .locals 3

    .prologue
    .line 121
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "deleteCachedContent"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "sessionId":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$1;

    invoke-direct {v1, p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/IOQueue;->post(Ljava/lang/Runnable;)V

    .line 130
    return-void
.end method

.method public disable()V
    .locals 4

    .prologue
    .line 88
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mEnabled:Z

    if-nez v2, :cond_0

    .line 118
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;

    monitor-enter v3

    .line 94
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 96
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 98
    .local v1, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 99
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->invalidate()V

    goto :goto_1

    .line 113
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 101
    .restart local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;>;"
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mWaitingFetchRequestsHP:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 102
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 104
    .restart local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 105
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->invalidate()V

    goto :goto_2

    .line 107
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 108
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 110
    .restart local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 111
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->invalidate()V

    goto :goto_3

    .line 113
    .end local v1    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;>;"
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 115
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "disable"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 116
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mEnabled:Z

    .line 117
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;->disable()V

    goto/16 :goto_0
.end method

.method public enable()V
    .locals 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "enable"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mEnabled:Z

    .line 84
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;->enable()V

    goto :goto_0
.end method

.method public getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    return-object v0
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$ContentBrokerDelegate;

    .line 71
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 438
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "sessionId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
