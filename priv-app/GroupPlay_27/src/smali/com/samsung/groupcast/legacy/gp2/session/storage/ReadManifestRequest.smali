.class public Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
.super Lcom/samsung/groupcast/misc/requests/BaseRequest;
.source "ReadManifestRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/groupcast/misc/requests/BaseRequest",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mManifestId:Ljava/lang/String;

.field private final mManifestTimestamp:J

.field private final mSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 0
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "manifestId"    # Ljava/lang/String;
    .param p3, "manifestTimestamp"    # J
    .param p5, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 20
    invoke-direct {p0, p5}, Lcom/samsung/groupcast/misc/requests/BaseRequest;-><init>(Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mSessionId:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mManifestId:Ljava/lang/String;

    .line 23
    iput-wide p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mManifestTimestamp:J

    .line 24
    return-void
.end method


# virtual methods
.method public complete(Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 1
    .param p1, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p2, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 43
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 51
    return-void
.end method

.method public getManifestAge()J
    .locals 4

    .prologue
    .line 39
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mManifestTimestamp:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getManifestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mManifestId:Ljava/lang/String;

    return-object v0
.end method

.method public getManifestTimestamp()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mManifestTimestamp:J

    return-wide v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->mSessionId:Ljava/lang/String;

    return-object v0
.end method
