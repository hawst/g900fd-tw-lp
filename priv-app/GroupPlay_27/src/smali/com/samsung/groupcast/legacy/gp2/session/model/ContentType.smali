.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;
.super Ljava/lang/Object;
.source "ContentType.java"


# static fields
.field public static final COLLAGE:I = 0x5

.field public static final COLLAGE_PAGE:I = 0x8

.field public static final CONTENTS:I = 0x6

.field public static final DOCUMENT:I = 0x1

.field public static final DOCUMENT_PAGE:I = 0x2

.field public static final GENERIC:I = 0x0

.field public static final IMAGE:I = 0x3

.field public static final PHOTOS:I = 0x7

.field public static final SUPPORTED_COLLAGE_FILE_EXTENSIONS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    .line 22
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "doc"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 23
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "docx"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 24
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "ppt"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 25
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "pptx"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 26
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "pdf"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    .line 31
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 32
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "png"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "jpg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 34
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "jpeg"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "webp"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "bmp"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "wbmp"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_COLLAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    .line 42
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_COLLAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    const-string v1, "3ic"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 43
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
