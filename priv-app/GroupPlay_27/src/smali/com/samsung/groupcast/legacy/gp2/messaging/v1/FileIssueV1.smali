.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
.super Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;
.source "FileIssueV1.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/FileIssue;


# instance fields
.field private final mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/DataKey;Ljava/lang/String;)V
    .locals 1
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;)V

    .line 37
    const-string v0, "filePath"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 38
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->mPath:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static createFileIssueForContent(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    .locals 4
    .param p0, "contentId"    # Ljava/lang/String;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 25
    const-string v3, "contentId"

    invoke-static {v3, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 26
    const-string v3, "filePath"

    invoke-static {v3, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v3, "CONTENT"

    invoke-direct {v0, v3, p0}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;

    invoke-direct {v2, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;Ljava/lang/String;)V

    .line 30
    .local v2, "issue":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    invoke-static {p1}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "filename":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/groupcast/core/messaging/FileInfo;->appendValue(Ljava/lang/String;)V

    .line 32
    return-object v2
.end method

.method public static createFileIssueForManifest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    .locals 3
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "manifestId"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 14
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 15
    const-string v2, "manifestId"

    invoke-static {v2, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 16
    const-string v2, "filePath"

    invoke-static {v2, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 17
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v2, "MANIFEST"

    invoke-direct {v0, v2, p1}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;

    invoke-direct {v1, v0, p2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;Ljava/lang/String;)V

    .line 20
    .local v1, "issue":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/samsung/groupcast/core/messaging/FileInfo;->appendValue(Ljava/lang/String;)V

    .line 21
    return-object v1
.end method


# virtual methods
.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->getType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "info"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "filePath"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->mPath:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
