.class public Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mName:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;


# instance fields
.field private final mActivitiesJoined:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mContentsResetEnabled:Z

.field private final mEmail:Ljava/lang/String;

.field private mExtraInfo:Lorg/json/JSONObject;

.field private final mJoinedGame:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mWifiIp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->makeUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->sInstance:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    .line 86
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "email"    # Ljava/lang/String;
    .param p3, "wifiIp"    # Ljava/lang/String;
    .param p4, "contentsResetEnabled"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    .line 33
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    .line 37
    sput-object p1, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mName:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mEmail:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mWifiIp:Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 41
    iput-boolean p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mContentsResetEnabled:Z

    .line 42
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    return-object p1
.end method

.method public static getInstance()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->sInstance:Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    return-object v0
.end method

.method public static getNewInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "email"    # Ljava/lang/String;
    .param p2, "wifiIp"    # Ljava/lang/String;
    .param p3, "contentsResetEnabled"    # Z

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private static makeUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 5

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static setNewUserName(Ljava/lang/String;)V
    .locals 0
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    sput-object p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mName:Ljava/lang/String;

    .line 64
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public getContentsResetEnabled()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mContentsResetEnabled:Z

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    if-eqz v1, :cond_0

    .line 235
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :goto_0
    return-object v1

    .line 236
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 240
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExtraInfoJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getJoinedActivities()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 143
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    .line 144
    .local v3, "joinedActivities":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 146
    .local v0, "i":I
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 147
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 148
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 149
    :cond_0
    return-object v3
.end method

.method public getJoinedActivitiesHS()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    return-object v0
.end method

.method public getJoinedGames()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 161
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    .line 162
    .local v3, "joinedGames":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 164
    .local v0, "i":I
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 165
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 166
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v3, v0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 167
    :cond_0
    return-object v3
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getWifiIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mWifiIp:Ljava/lang/String;

    return-object v0
.end method

.method public joinActivity(Ljava/lang/String;)V
    .locals 6
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 175
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 176
    const-string v3, "GAME_"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 180
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "GAME_"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 184
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 186
    .local v2, "gameName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v2    # "gameName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public leaveActivity(Ljava/lang/String;)V
    .locals 5
    .param p1, "activityName"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v2, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 195
    const-string v2, "GAME_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "GAME_"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 202
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "gameName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 205
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public setContentsResetEnabled(Z)V
    .locals 0
    .param p1, "contentsResetEnabled"    # Z

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mContentsResetEnabled:Z

    .line 158
    return-void
.end method

.method public setExtraInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 224
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    invoke-virtual {v1, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "email"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mEmail:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "wifiIp"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mWifiIp:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "joinedActivities"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getJoinedActivities()[Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updatePhoneNumberAtExtra()V
    .locals 4

    .prologue
    .line 251
    const-string v2, "EKPN"

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getExtraInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 252
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 254
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "phoneNumber":Ljava/lang/String;
    const-string v2, "EKPN"

    invoke-virtual {p0, v2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setExtraInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    .end local v0    # "phoneNumber":Ljava/lang/String;
    .end local v1    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :cond_0
    return-void
.end method

.method public updateVoipGroupHash(Ljava/lang/String;)V
    .locals 1
    .param p1, "hash"    # Ljava/lang/String;

    .prologue
    .line 260
    const-string v0, "EKVG"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setExtraInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public updateVoipGroupPeerId(Ljava/lang/String;)V
    .locals 1
    .param p1, "peerId"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string v0, "EKVP"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setExtraInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 68
    sget-object v2, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mEmail:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mWifiIp:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 71
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 73
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    .local v0, "activityName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    .end local v0    # "activityName":Ljava/lang/String;
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mContentsResetEnabled:Z

    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/BooleanTool;->booleanToInt(Z)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 79
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 81
    .restart local v0    # "activityName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 83
    .end local v0    # "activityName":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    return-void
.end method
