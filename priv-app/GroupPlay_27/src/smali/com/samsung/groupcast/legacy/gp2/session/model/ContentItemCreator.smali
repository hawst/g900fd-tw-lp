.class public abstract Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
.super Ljava/lang/Object;
.source "ContentItemCreator.java"


# instance fields
.field private final mType:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->mType:I

    .line 12
    return-void
.end method


# virtual methods
.method public abstract createContentItemForLocalPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.end method

.method public abstract createContentItemFromJson(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.end method

.method public abstract getSupportedFileExtensions()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->mType:I

    return v0
.end method
