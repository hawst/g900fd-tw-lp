.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "DrawingSyncNotificationV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mDrawing:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1$1;

    const-string v1, "DS_NOTI_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v0, "DS_NOTI_V1"

    invoke-direct {p0, v0, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 32
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->mDrawing:[B

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 24
    const-string v0, "DS_NOTI_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->mDrawing:[B

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->writeByteArray([B)V

    .line 28
    return-void
.end method


# virtual methods
.method public getBytes()[B
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->mDrawing:[B

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V

    .line 43
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "object:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;->mDrawing:[B

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
