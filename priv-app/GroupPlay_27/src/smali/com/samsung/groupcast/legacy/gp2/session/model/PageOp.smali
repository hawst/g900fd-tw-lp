.class Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
.super Ljava/lang/Object;
.source "PageOp.java"


# static fields
.field private static final OP_APPEND_DOUBLE:I = 0xb

.field private static final OP_APPEND_INT:I = 0x9

.field private static final OP_APPEND_LONG:I = 0xa

.field private static final OP_APPEND_STRING:I = 0x8

.field private static final OP_SET_DOUBLE:I = 0x3

.field private static final OP_SET_DOUBLE_ARRAY:I = 0x7

.field private static final OP_SET_INT:I = 0x1

.field private static final OP_SET_INT_ARRAY:I = 0x5

.field private static final OP_SET_LONG:I = 0x2

.field private static final OP_SET_LONG_ARRAY:I = 0x6

.field private static final OP_SET_STRING:I = 0x0

.field private static final OP_SET_STRING_ARRAY:I = 0x4


# instance fields
.field private final mName:Ljava/lang/String;

.field private final mOp:I

.field private final mValue:Ljava/lang/Object;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p1, "op"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    .line 79
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    .line 81
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONArray;)V
    .locals 9
    .param p1, "json"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    iput v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    .line 85
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    .line 87
    iget v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    packed-switch v7, :pswitch_data_0

    .line 181
    new-instance v7, Lorg/json/JSONException;

    const-string v8, "unknown op code"

    invoke-direct {v7, v8}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 89
    :pswitch_0
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    .line 184
    :goto_0
    return-void

    .line 94
    :pswitch_1
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 99
    :pswitch_2
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 104
    :pswitch_3
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 109
    :pswitch_4
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 110
    .local v2, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 111
    .local v0, "count":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 114
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 117
    :cond_0
    iput-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 122
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_5
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 123
    .restart local v2    # "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 124
    .restart local v0    # "count":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v0, :cond_1

    .line 127
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 130
    :cond_1
    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 135
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v4    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :pswitch_6
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 136
    .restart local v2    # "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 137
    .restart local v0    # "count":I
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    if-ge v1, v0, :cond_2

    .line 140
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 143
    :cond_2
    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto :goto_0

    .line 148
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :pswitch_7
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 149
    .restart local v2    # "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 150
    .restart local v0    # "count":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 152
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    if-ge v1, v0, :cond_3

    .line 153
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 156
    :cond_3
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto/16 :goto_0

    .line 161
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "jsonArray":Lorg/json/JSONArray;
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Double;>;"
    :pswitch_8
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto/16 :goto_0

    .line 166
    :pswitch_9
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto/16 :goto_0

    .line 171
    :pswitch_a
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto/16 :goto_0

    .line 176
    :pswitch_b
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    goto/16 :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static append(Ljava/lang/String;D)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # D

    .prologue
    .line 74
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/16 v1, 0xb

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static append(Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 66
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/16 v1, 0x9

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static append(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 70
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/16 v1, 0xa

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static append(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/16 v1, 0x8

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static set(Ljava/lang/String;D)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # D

    .prologue
    .line 42
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x3

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static set(Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static set(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 38
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static set(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static setDoubleArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Double;>;"
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static setIntArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x5

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static setLongArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static setStringArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(ILjava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    .prologue
    .line 187
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    packed-switch v0, :pswitch_data_0

    .line 208
    :goto_0
    return-void

    .line 196
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    invoke-virtual {p1, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;->set(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 204
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    invoke-virtual {p1, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;->append(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public getJsonRepresentation()Lorg/json/JSONArray;
    .locals 3

    .prologue
    .line 211
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 212
    .local v1, "json":Lorg/json/JSONArray;
    iget v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(I)Lorg/json/JSONArray;

    .line 213
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 215
    iget v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    packed-switch v2, :pswitch_data_0

    .line 243
    :goto_0
    return-object v1

    .line 220
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 229
    :pswitch_1
    new-instance v0, Lorg/json/JSONArray;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    invoke-direct {v0, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 230
    .local v0, "array":Lorg/json/JSONArray;
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 238
    .end local v0    # "array":Lorg/json/JSONArray;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 248
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "op"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mOp:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "value"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->mValue:Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
