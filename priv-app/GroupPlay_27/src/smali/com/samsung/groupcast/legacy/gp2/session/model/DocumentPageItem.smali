.class public Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;
.super Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.source "DocumentPageItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_DOCUMENT_ID:Ljava/lang/String; = "DOCUMENT_ID"

.field private static final KEY_INDEX:Ljava/lang/String; = "INDEX"


# instance fields
.field private final mDocumentId:Ljava/lang/String;

.field private final mIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;-><init>(I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 93
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$2;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$2;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;I)V
    .locals 2
    .param p1, "parent"    # Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;
    .param p2, "index"    # I

    .prologue
    .line 40
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    .line 42
    iput p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    .line 43
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "documentId"    # Ljava/lang/String;
    .param p4, "index"    # I

    .prologue
    .line 58
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 59
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    .line 60
    iput p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    .line 61
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 46
    const/4 v1, 0x2

    invoke-direct {p0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILorg/json/JSONObject;)V

    .line 49
    :try_start_0
    const-string v1, "DOCUMENT_ID"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    .line 50
    const-string v1, "INDEX"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid json used to create DocumentPageItem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getDocumentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    return v0
.end method

.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 74
    :try_start_0
    invoke-super {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v1

    .line 75
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "DOCUMENT_ID"

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 76
    const-string v2, "INDEX"

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    return-object v1

    .line 78
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception encountered while getting Json representation - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->getContentId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "documentId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "index"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mDocumentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->mIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    return-void
.end method
