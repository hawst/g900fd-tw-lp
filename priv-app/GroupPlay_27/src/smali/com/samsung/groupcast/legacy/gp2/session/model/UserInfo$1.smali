.class final Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 13
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "email":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "wifiIp":Ljava/lang/String;
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;)V

    .line 95
    .local v0, "userInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 96
    .local v6, "activitiesJoinedCount":I
    :goto_0
    if-lez v6, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 98
    .local v7, "activityName":Ljava/lang/String;
    if-nez v7, :cond_2

    .line 103
    .end local v7    # "activityName":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 104
    .local v11, "intContentsResetEnabled":I
    invoke-static {v11}, Lcom/samsung/groupcast/misc/utility/BooleanTool;->intToBoolean(I)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->setContentsResetEnabled(Z)V

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 106
    .local v12, "joinedGameCount":I
    :goto_1
    if-lez v12, :cond_1

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 108
    .local v10, "gameName":Ljava/lang/String;
    if-nez v10, :cond_3

    .line 115
    .end local v10    # "gameName":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 116
    .local v9, "extra":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mExtraInfo:Lorg/json/JSONObject;
    invoke-static {v0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->access$302(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v9    # "extra":Ljava/lang/String;
    :goto_2
    return-object v0

    .line 100
    .end local v11    # "intContentsResetEnabled":I
    .end local v12    # "joinedGameCount":I
    .restart local v7    # "activityName":Ljava/lang/String;
    :cond_2
    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mActivitiesJoined:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 101
    add-int/lit8 v6, v6, -0x1

    .line 102
    goto :goto_0

    .line 110
    .end local v7    # "activityName":Ljava/lang/String;
    .restart local v10    # "gameName":Ljava/lang/String;
    .restart local v11    # "intContentsResetEnabled":I
    .restart local v12    # "joinedGameCount":I
    :cond_3
    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->mJoinedGame:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v12, v12, -0x1

    .line 112
    goto :goto_1

    .line 117
    .end local v10    # "gameName":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 118
    .local v8, "e":Ljava/lang/Exception;
    const-string v4, "UserInfo"

    const-string v5, "no extra info!!!"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 126
    new-array v0, p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo$1;->newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    return-object v0
.end method
