.class final Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$2;
.super Ljava/lang/Object;
.source "DocumentPageItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 98
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "documentId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 100
    .local v4, "index":I
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$2;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 105
    new-array v0, p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$2;->newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;

    move-result-object v0

    return-object v0
.end method
