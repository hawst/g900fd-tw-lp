.class public abstract Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;
.super Ljava/lang/Object;
.source "SessionStorage.java"


# static fields
.field private static final CONTAINER_DIRECTORY_NAME:Ljava/lang/String; = "sessions"

.field private static final CONTENT_DIRECTORY_NAME:Ljava/lang/String; = "content"

.field private static final MANIFEST_DIRECTORY_NAME:Ljava/lang/String; = "manifests"

.field private static final MANIFEST_FILE_EXTENSION:Ljava/lang/String; = "manifest"

.field private static final TEMP_DIRECTORY_NAME:Ljava/lang/String; = "temp"

.field private static final THUMBNAILS_DIRECTORY_NAME:Ljava/lang/String; = "thumbs"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createContainerDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 20
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "sessions"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static createSessionContentDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 106
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 110
    const-class v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v3, "getSessionContentDirectory"

    const-string v4, "created session content directory"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "path"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 112
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 115
    :cond_0
    return-object v0
.end method

.method public static createSessionDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 77
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 82
    const-class v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v3, "getSessionDirectory"

    const-string v4, "created session directory"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "path"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 84
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 87
    :cond_0
    return-object v0
.end method

.method public static createSessionManifestDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 91
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    const-class v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v3, "getSessionManifestDirectory"

    const-string v4, "created session manifest directory"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "path"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 98
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 101
    :cond_0
    return-object v0
.end method

.method public static createSessionTempDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 119
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 120
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionTempDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    const-class v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v3, "getSessionTempDirectory"

    const-string v4, "created session temp directory"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "path"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 126
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 129
    :cond_0
    return-object v0
.end method

.method public static createSessionThumbnailsDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 133
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionThumbnailsDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    const-class v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v3, "getSessionThumbnailsDirectory"

    const-string v4, "created session thumbnails directory"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "path"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 140
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 143
    :cond_0
    return-object v0
.end method

.method public static getSessionContentDirectoryPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 46
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "sessionDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method public static getSessionContentFilePath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "extension"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    const-string v2, "contentId"

    invoke-static {v2, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    const-string v2, "extension"

    invoke-static {v2, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionContentDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "contentDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "path":Ljava/lang/String;
    return-object v1
.end method

.method public static getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 24
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->createContainerDirectory()Ljava/io/File;

    move-result-object v0

    .line 26
    .local v0, "containerDirectory":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "path":Ljava/lang/String;
    return-object v1
.end method

.method public static getSessionManifestDirectoryPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 31
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 32
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, "sessionDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "manifests"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method public static getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "manifestId"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    const-string v2, "manifestId"

    invoke-static {v2, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "manifestDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "manifest"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "path":Ljava/lang/String;
    return-object v1
.end method

.method public static getSessionTempDirectoryPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "sessionDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method public static getSessionThumbnailsDirectoryPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionDirectoryPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "sessionDirectoryPath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "thumbs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method
