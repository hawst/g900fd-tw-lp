.class final Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary$1;
.super Ljava/lang/Object;
.source "SessionSummary.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 13
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 151
    .local v1, "version":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 152
    .local v2, "sessionId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 153
    .local v3, "manifestId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 154
    .local v4, "manifestAge":J
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 155
    .local v6, "pinHash":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 156
    .local v7, "sourceName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 157
    .local v8, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 158
    .local v9, "firstPageTitle":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 159
    .local v10, "documentCount":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 160
    .local v11, "imageCount":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v12

    .line 161
    .local v12, "audioCount":I
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-direct/range {v0 .. v12}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 167
    new-array v0, p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary$1;->newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v0

    return-object v0
.end method
