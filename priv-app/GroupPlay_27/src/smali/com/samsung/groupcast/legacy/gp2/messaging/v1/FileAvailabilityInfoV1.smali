.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "FileAvailabilityInfoV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

.field private final mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1$1;

    const-string v1, "CONTENT_AVAILABILITY_INFO_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/DataKey;I)V
    .locals 1
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;
    .param p2, "status"    # I

    .prologue
    .line 27
    const-string v0, "CONTENT_AVAILABILITY_INFO_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 28
    const-string v0, "dataKey"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    .line 30
    iput p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->writeString(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/DataKey;->getDataId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->writeString(Ljava/lang/String;)V

    .line 33
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->writeInt(I)V

    .line 34
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v2, "CONTENT_AVAILABILITY_INFO_V1"

    invoke-direct {p0, v2, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 38
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->readString()Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "dataType":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->readString()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "dataId":Ljava/lang/String;
    new-instance v2, Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-direct {v2, v1, v0}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    .line 41
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->readInt()I

    move-result v2

    iput v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    return v0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoading()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 57
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUnavailable()Z
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V

    .line 67
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dataKey"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "status"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->mStatus:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
