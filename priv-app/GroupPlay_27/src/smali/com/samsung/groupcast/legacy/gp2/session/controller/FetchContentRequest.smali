.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
.source "FetchContentRequest.java"


# instance fields
.field private final mContentId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 12
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 13
    const-string v0, "contentId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 14
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->mContentId:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->mContentId:Ljava/lang/String;

    return-object v0
.end method
