.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
.source "FetchManifestRequest.java"


# instance fields
.field private final mManifestId:Ljava/lang/String;

.field private final mSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 2
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "manifestId"    # Ljava/lang/String;
    .param p3, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 13
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v1, "MANIFEST"

    invoke-direct {v0, v1, p2}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 14
    const-string v0, "sessionId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 15
    const-string v0, "manifestId"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->mSessionId:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->mManifestId:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getManifestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->mManifestId:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->mSessionId:Ljava/lang/String;

    return-object v0
.end method
