.class Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;
.super Ljava/lang/Object;
.source "SessionScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->startPeriodicSessionSummaryRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 105
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)Ljava/lang/Runnable;

    move-result-object v1

    if-eq v1, p0, :cond_0

    .line 115
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->isChordStarted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->enable()V

    .line 112
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;-><init>()V

    .line 113
    .local v0, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 114
    const-wide/16 v1, 0x5dc

    invoke-static {p0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
