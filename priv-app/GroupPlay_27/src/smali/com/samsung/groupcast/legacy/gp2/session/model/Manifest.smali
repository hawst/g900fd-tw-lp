.class public Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
.super Ljava/lang/Object;
.source "Manifest.java"


# static fields
.field public static final KEY_COLLECTIONS:Ljava/lang/String; = "COLLECTIONS"

.field public static final KEY_ID:Ljava/lang/String; = "ID"


# instance fields
.field private isValid:Z

.field private final mCollections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;",
            ">;"
        }
    .end annotation
.end field

.field private final mDocumentItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mManifestId:Ljava/lang/String;

.field private final mTimestamp:J


# direct methods
.method public constructor <init>(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p1, "collections":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid:Z

    .line 26
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    .line 27
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    .line 30
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    .line 34
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    .line 35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mTimestamp:J

    .line 37
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 39
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->lockCollection()V

    .line 40
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getDocumentItems()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 44
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getImageItems()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 45
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->unlockCollection()V

    goto :goto_0

    .line 47
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;J)V
    .locals 6
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "timestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid:Z

    .line 26
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    .line 27
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    .line 30
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    .line 50
    const-string v4, "ID"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    .line 51
    iput-wide p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mTimestamp:J

    .line 54
    const-string v4, "COLLECTIONS"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 55
    .local v2, "collectionsJson":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 56
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 57
    .local v1, "collectionJson":Lorg/json/JSONObject;
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;-><init>(Lorg/json/JSONObject;)V

    .line 58
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getDocumentItems()Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 60
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getImageItems()Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 55
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 62
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .end local v1    # "collectionJson":Lorg/json/JSONObject;
    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 132
    if-ne p0, p1, :cond_0

    .line 133
    const/4 v1, 0x1

    .line 141
    :goto_0
    return v1

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    if-nez v1, :cond_1

    .line 137
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 140
    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 141
    .local v0, "other":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAge()J
    .locals 4

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid:Z

    if-nez v0, :cond_0

    .line 93
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 95
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mTimestamp:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    return-object v0
.end method

.method public getCollections()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    return-object v0
.end method

.method public getDocumentCount()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    return v0
.end method

.method public getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 4
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 111
    const-string v3, "contentId"

    invoke-static {v3, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    const/4 v2, 0x0

    .line 114
    .local v2, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 115
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v2

    .line 116
    if-eqz v2, :cond_0

    .line 119
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    :cond_1
    return-object v2
.end method

.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 7

    .prologue
    .line 65
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 67
    .local v4, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v5, "ID"

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 69
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 70
    .local v1, "collectionsJson":Lorg/json/JSONArray;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 71
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 73
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .end local v1    # "collectionsJson":Lorg/json/JSONArray;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 74
    .local v2, "e":Lorg/json/JSONException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5}, Ljava/lang/RuntimeException;-><init>()V

    throw v5

    .line 72
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "collectionsJson":Lorg/json/JSONArray;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    const-string v5, "COLLECTIONS"

    invoke-virtual {v4, v5, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 76
    return-object v4
.end method

.method public getManifestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mTimestamp:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid:Z

    .line 85
    return-void
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mManifestId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "collections"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mCollections:Ljava/util/HashMap;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "documentCount"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "imageCount"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
