.class public Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;
.super Ljava/lang/Object;
.source "StorageAgent.java"


# instance fields
.field private final mSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "sessionId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->mSessionId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->handleStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->handleReadManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V

    return-void
.end method

.method private static handleReadManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V
    .locals 6
    .param p0, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v4

    .line 93
    .local v4, "sessionId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "manifestId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->getManifestTimestamp()J

    move-result-wide v1

    .line 95
    .local v1, "manifestTimestamp":J
    invoke-static {v4, v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "path":Ljava/lang/String;
    new-instance v5, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;

    invoke-direct {v5, v3, v1, v2, p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;-><init>(Ljava/lang/String;JLcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V

    invoke-static {v5}, Lcom/samsung/groupcast/application/IOQueue;->post(Ljava/lang/Runnable;)V

    .line 130
    return-void
.end method

.method private static handleStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V
    .locals 1
    .param p0, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    .prologue
    .line 49
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V

    invoke-static {v0}, Lcom/samsung/groupcast/application/IOQueue;->post(Ljava/lang/Runnable;)V

    .line 89
    return-void
.end method


# virtual methods
.method public createReadManifestRequest(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    .locals 6
    .param p1, "manifestId"    # Ljava/lang/String;
    .param p2, "manifestTimestamp"    # J

    .prologue
    .line 38
    const-string v0, "manifestId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 39
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->mSessionId:Ljava/lang/String;

    new-instance v5, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$2;

    invoke-direct {v5, p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;)V

    move-object v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/samsung/groupcast/misc/requests/Starter;)V

    return-object v0
.end method

.method public createStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    .locals 3
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 27
    const-string v0, "manifest"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->mSessionId:Ljava/lang/String;

    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$1;

    invoke-direct {v2, p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;)V

    invoke-direct {v0, v1, p1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;-><init>(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/misc/requests/Starter;)V

    return-object v0
.end method
