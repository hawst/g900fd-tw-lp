.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ImageItem;
.super Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.source "ImageItem.java"


# static fields
.field public static final CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ImageItem$1;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ImageItem$1;-><init>(I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ImageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 29
    const/4 v0, 0x3

    invoke-static {p1}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 33
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILorg/json/JSONObject;)V

    .line 34
    return-void
.end method
