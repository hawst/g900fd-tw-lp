.class Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;
.super Ljava/lang/Object;
.source "ReadManifestRequest.java"

# interfaces
.implements Lcom/samsung/groupcast/misc/requests/Notifier;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

.field final synthetic val$manifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field final synthetic val$result:Lcom/samsung/groupcast/misc/requests/Result;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;->val$result:Lcom/samsung/groupcast/misc/requests/Result;

    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;->val$manifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public notifyListener(Lcom/samsung/groupcast/misc/requests/Request;Lcom/samsung/groupcast/misc/requests/RequestListener;)V
    .locals 4
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;
    .param p2, "l"    # Lcom/samsung/groupcast/misc/requests/RequestListener;

    .prologue
    .line 46
    move-object v1, p1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .local v1, "request":Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    move-object v0, p2

    .line 47
    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestListener;

    .line 48
    .local v0, "listener":Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestListener;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;->val$result:Lcom/samsung/groupcast/misc/requests/Result;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest$1;->val$manifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestListener;->onComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 49
    return-void
.end method
