.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
.super Ljava/lang/Object;
.source "ManifestBuilder.java"


# instance fields
.field final mCollections:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 6
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    .line 17
    if-eqz p1, :cond_0

    .line 18
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v1

    .line 19
    .local v1, "collections":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;>;"
    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 20
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    invoke-direct {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;)V

    .line 21
    .local v3, "newCollection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 24
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .end local v1    # "collections":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "newCollection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    :cond_0
    return-void
.end method

.method private verifyContentIdFree(Ljava/lang/String;)Z
    .locals 4
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 31
    const/4 v2, 0x0

    .line 32
    .local v2, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 33
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    const/4 v3, 0x0

    .line 38
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addItemAsPage(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 84
    const-string v1, "item"

    invoke-static {v1, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->verifyContentIdFree(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    const-string v1, "registerItem contentID issue"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 88
    :cond_0
    const-string v1, "collectionName"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 90
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    if-nez v0, :cond_1

    .line 91
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;-><init>(Ljava/lang/String;)V

    .line 92
    .restart local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :cond_1
    invoke-virtual {v0, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->addItem(ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 95
    return-void
.end method

.method public addItemAsPage(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 70
    const-string v1, "item"

    invoke-static {v1, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->verifyContentIdFree(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    const-string v1, "registerItem contentID issue"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 74
    :cond_0
    const-string v1, "collectionName"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 76
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    if-nez v0, :cond_1

    .line 77
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;-><init>(Ljava/lang/String;)V

    .line 78
    .restart local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_1
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->addItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 81
    return-void
.end method

.method public addItemsAsPages(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;>;"
    const-string v2, "items"

    invoke-static {v2, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 65
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {p0, p1, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->addItemAsPage(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    goto :goto_0

    .line 67
    .end local v1    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :cond_0
    return-void
.end method

.method public getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    return-object v0
.end method

.method public registerItem(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 49
    const-string v1, "item"

    invoke-static {v1, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->verifyContentIdFree(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    const-string v1, "registerItem contentID issue"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 53
    :cond_0
    const-string v1, "collectionName"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 54
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 55
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    if-nez v0, :cond_1

    .line 56
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;-><init>(Ljava/lang/String;)V

    .line 57
    .restart local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_1
    invoke-virtual {v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->registerItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 60
    return-void
.end method

.method public registerItems(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;>;"
    const-string v2, "items"

    invoke-static {v2, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 44
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {p0, p1, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->registerItem(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    goto :goto_0

    .line 46
    .end local v1    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :cond_0
    return-void
.end method

.method public removeCollection(Ljava/lang/String;)V
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_0
    return-void
.end method

.method public toManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->mCollections:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;-><init>(Ljava/util/HashMap;)V

    return-object v0
.end method
