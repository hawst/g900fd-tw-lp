.class final Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;
.super Ljava/lang/Object;
.source "StorageAgent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->handleReadManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$manifestTimestamp:J

.field final synthetic val$path:Ljava/lang/String;

.field final synthetic val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;


# direct methods
.method constructor <init>(Ljava/lang/String;JLcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$path:Ljava/lang/String;

    iput-wide p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$manifestTimestamp:J

    iput-object p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 101
    const/4 v5, 0x0

    .line 102
    .local v5, "tempManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$path:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->readFileAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "fileContents":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 105
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "readSessionManifest"

    const-string v9, "failure occured while reading file"

    new-array v10, v14, [Ljava/lang/Object;

    const-string v11, "path"

    aput-object v11, v10, v12

    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$path:Ljava/lang/String;

    aput-object v11, v10, v13

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 118
    :goto_0
    move-object v3, v5

    .line 121
    .local v3, "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    if-eqz v3, :cond_1

    .line 122
    sget-object v4, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    .line 127
    .local v4, "result":Lcom/samsung/groupcast/misc/requests/Result;
    :goto_1
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-virtual {v7, v4, v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 128
    return-void

    .line 109
    .end local v3    # "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .end local v4    # "result":Lcom/samsung/groupcast/misc/requests/Result;
    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 110
    .local v2, "json":Lorg/json/JSONObject;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    iget-wide v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$manifestTimestamp:J

    invoke-direct {v6, v2, v7, v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;-><init>(Lorg/json/JSONObject;J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "tempManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .local v6, "tempManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    move-object v5, v6

    .line 115
    .end local v6    # "tempManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .restart local v5    # "tempManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    goto :goto_0

    .line 111
    .end local v2    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "readSessionManifest"

    const-string v9, "could not create manifest from Json"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const-string v11, "path"

    aput-object v11, v10, v12

    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$4;->val$path:Ljava/lang/String;

    aput-object v11, v10, v13

    const-string v11, "exception"

    aput-object v11, v10, v14

    const/4 v11, 0x3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v3    # "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    :cond_1
    sget-object v4, Lcom/samsung/groupcast/misc/requests/Result;->IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .restart local v4    # "result":Lcom/samsung/groupcast/misc/requests/Result;
    goto :goto_1
.end method
