.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;
.super Lcom/samsung/groupcast/core/messaging/BaseChannelListener;
.source "FileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChannelListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;


# direct methods
.method private constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/BaseChannelListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)V

    return-void
.end method


# virtual methods
.method public onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V

    .line 305
    return-void
.end method

.method public onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    .line 310
    return-void
.end method

.method public onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    .line 327
    return-void
.end method

.method public onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    .locals 8
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "offset"    # J
    .param p6, "size"    # J

    .prologue
    .line 315
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-wide v6, p6

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    invoke-static/range {v0 .. v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V

    .line 316
    return-void
.end method

.method public onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V

    .line 322
    return-void
.end method
