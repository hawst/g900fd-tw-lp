.class Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;
.super Landroid/os/AsyncTask;
.source "CollagePageManifestGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->convertManifestBuilderToManifest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;",
        "Ljava/lang/Void;",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)V
    .locals 0

    .prologue
    .line 269
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;)Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 3
    .param p1, "params"    # [Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .prologue
    .line 272
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "CLG convertManifest"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 273
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 274
    .local v0, "builder":Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->toManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 269
    check-cast p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->doInBackground([Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;)Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 2
    .param p1, "result"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$802(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 280
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # setter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    invoke-static {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$902(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 282
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$1000(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$1100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$1100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    invoke-interface {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;->onManifestGenerationComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 269
    check-cast p1, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;->onPostExecute(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    return-void
.end method
