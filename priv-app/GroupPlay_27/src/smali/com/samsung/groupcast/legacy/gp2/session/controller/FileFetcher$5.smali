.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;
.super Ljava/lang/Object;
.source "FileFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

.field final synthetic val$destinationPaths:Ljava/util/List;

.field final synthetic val$requests:Ljava/util/List;

.field final synthetic val$tempPath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$requests:Ljava/util/List;

    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$destinationPaths:Ljava/util/List;

    iput-object p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$tempPath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 244
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 287
    :goto_0
    return-void

    .line 246
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 247
    .local v4, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/misc/requests/Result;>;"
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 248
    .local v5, "resultsByPath":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;>;"
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$requests:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 251
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_3

    .line 252
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$destinationPaths:Ljava/util/List;

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 254
    .local v1, "destinationPath":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 256
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$tempPath:Ljava/lang/String;

    const/high16 v8, 0x80000

    invoke-static {v7, v1, v8}, Lcom/samsung/groupcast/misc/utility/FileTools;->copyFile(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v6

    .line 258
    .local v6, "success":Z
    if-eqz v6, :cond_1

    sget-object v3, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    .line 259
    .local v3, "result":Lcom/samsung/groupcast/misc/requests/Result;
    :goto_2
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-virtual {v5, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    .end local v6    # "success":Z
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 258
    .end local v3    # "result":Lcom/samsung/groupcast/misc/requests/Result;
    .restart local v6    # "success":Z
    :cond_1
    sget-object v3, Lcom/samsung/groupcast/misc/requests/Result;->IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    goto :goto_2

    .line 263
    .end local v6    # "success":Z
    :cond_2
    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/misc/requests/Result;

    .line 264
    .restart local v3    # "result":Lcom/samsung/groupcast/misc/requests/Result;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 269
    .end local v1    # "destinationPath":Ljava/lang/String;
    .end local v3    # "result":Lcom/samsung/groupcast/misc/requests/Result;
    :cond_3
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$tempPath:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->deleteFile(Ljava/lang/String;)Z

    .line 272
    new-instance v7, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;

    invoke-direct {v7, p0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;Ljava/util/ArrayList;)V

    invoke-static {v7}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
