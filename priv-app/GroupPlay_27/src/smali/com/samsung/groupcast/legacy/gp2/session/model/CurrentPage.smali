.class public Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
.super Ljava/lang/Object;
.source "CurrentPage.java"


# instance fields
.field private final mCollectionName:Ljava/lang/String;

.field private final mContentId:Ljava/lang/String;

.field private final mTimestamp:J


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 25
    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 26
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "contentId"    # Ljava/lang/String;

    .prologue
    .line 29
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 30
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p3, "age"    # J

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mCollectionName:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    .line 35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, p3

    iput-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mTimestamp:J

    .line 36
    return-void
.end method

.method public static existingPage(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .locals 1
    .param p0, "collectionName"    # Ljava/lang/String;
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "age"    # J

    .prologue
    .line 21
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method

.method public static freshPage(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .locals 1
    .param p0, "collectionName"    # Ljava/lang/String;
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 17
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static unknownPage()Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .locals 2

    .prologue
    .line 13
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    const-string v1, "DEAD COLLECTION"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getAge()J
    .locals 4

    .prologue
    .line 47
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mTimestamp:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public isContent(Ljava/lang/String;)Z
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 56
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 58
    :goto_0
    return v0

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isFresher(Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)Z
    .locals 6
    .param p1, "other"    # Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    iget-object v2, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    if-nez v2, :cond_2

    move v1, v0

    .line 67
    goto :goto_0

    .line 70
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 74
    iget-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mTimestamp:J

    iget-wide v4, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mTimestamp:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public isKnown()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "contentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->mContentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "age"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->getAge()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
