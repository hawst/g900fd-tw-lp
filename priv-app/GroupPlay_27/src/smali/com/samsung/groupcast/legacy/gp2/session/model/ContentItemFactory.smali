.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;
.super Ljava/lang/Object;
.source "ContentItemFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    }
.end annotation


# static fields
.field public static final FAILED_TO_LOAD:Ljava/lang/String; = "failed_to_load.FAILED_TO_LOAD"

.field public static final UNSUPPORTED_FILE:Ljava/lang/String; = "unsupported_file.UNSUPPORTED_FILE"

.field private static final mItemCreatorsByExtension:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;",
            ">;"
        }
    .end annotation
.end field

.field private static final mItemCreatorsByType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByType:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByExtension:Ljava/util/HashMap;

    .line 60
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V

    .line 61
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V

    .line 62
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ImageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V

    .line 63
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V

    .line 64
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V

    .line 65
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static createContentItem(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 12
    .param p0, "itemJson"    # Lorg/json/JSONObject;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 68
    const/4 v3, 0x0

    .line 71
    .local v3, "type":I
    :try_start_0
    const-string v4, "TYPE"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 78
    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByType:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 80
    .local v1, "creator":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    if-nez v1, :cond_0

    .line 81
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 82
    const-class v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v5, "createContentItemForLocalContentAtPath"

    const-string v6, "no creator found, using generic creator"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "type"

    aput-object v8, v7, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 86
    :cond_0
    const/4 v0, 0x0

    .line 89
    .local v0, "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :try_start_1
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->createContentItemFromJson(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 95
    .end local v0    # "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .end local v1    # "creator":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    :goto_0
    return-object v0

    .line 72
    :catch_0
    move-exception v2

    .line 73
    .local v2, "e":Lorg/json/JSONException;
    const-class v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    const-string v5, "createContentItem"

    const-string v6, "json does not contain type information"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "json"

    aput-object v8, v7, v9

    aput-object p0, v7, v10

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 75
    const/4 v0, 0x0

    goto :goto_0

    .line 90
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v0    # "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .restart local v1    # "creator":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    :catch_1
    move-exception v2

    .line 91
    .local v2, "e":Ljava/lang/Exception;
    const-class v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    const-string v5, "createContentItem"

    const-string v6, "exception while creating content item"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "type"

    aput-object v8, v7, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    const-string v8, "json"

    aput-object v8, v7, v11

    const/4 v8, 0x3

    aput-object p0, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static createContentItemForLocalContent(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    .locals 12
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 100
    const-string v1, "resolver"

    invoke-static {v1, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 101
    const-string v1, "uri"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 102
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 104
    .local v9, "scheme":Ljava/lang/String;
    if-nez v9, :cond_0

    .line 105
    const-class v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v2, "createContentItem"

    const-string v3, "scheme is null"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "uri"

    aput-object v5, v4, v10

    aput-object p1, v4, v11

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 149
    :goto_0
    return-object v8

    .line 109
    :cond_0
    const/4 v0, 0x0

    .line 111
    .local v0, "path":Ljava/lang/String;
    const-string v1, "content"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 112
    invoke-static {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->resolveLocalContentPath(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 121
    :goto_1
    if-nez v0, :cond_1

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "unsupported_file.UNSUPPORTED_FILE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    :cond_1
    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "unsupported_file.UNSUPPORTED_FILE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 133
    :cond_2
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByExtension:Ljava/util/HashMap;

    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 135
    .local v6, "cc":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->getType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 137
    const-wide/32 v1, 0x100000

    const/16 v3, 0x5a

    const/16 v4, 0x780

    const/16 v5, 0x438

    invoke-static/range {v0 .. v5}, Lcom/samsung/groupcast/misc/utility/FileTools;->getResizedImage(Ljava/lang/String;JIII)Ljava/lang/String;

    move-result-object v0

    .line 140
    if-nez v0, :cond_3

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "unsupported_file.UNSUPPORTED_FILE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    :cond_3
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->createContentItemForLocalContentAtPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v7

    .line 148
    .local v7, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    new-instance v8, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;

    invoke-direct {v8, v7, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;Ljava/lang/String;)V

    .line 149
    .local v8, "result":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    goto/16 :goto_0

    .line 113
    .end local v6    # "cc":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    .end local v7    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .end local v8    # "result":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    :cond_4
    const-string v1, "file"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 114
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 116
    :cond_5
    const-class v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v2, "createContentItem"

    const-string v3, "unrecognized Uri scheme"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "uri"

    aput-object v5, v4, v10

    aput-object p1, v4, v11

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private static createContentItemForLocalContentAtPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 12
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 186
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 187
    .local v3, "extension":Ljava/lang/String;
    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByExtension:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 189
    .local v1, "creator":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
    if-nez v1, :cond_0

    .line 190
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 191
    const-class v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v5, "createContentItemForLocalContentAtPath"

    const-string v6, "no creator found, using generic creator"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "extension"

    aput-object v8, v7, v9

    aput-object v3, v7, v10

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 195
    :cond_0
    const/4 v0, 0x0

    .line 198
    .local v0, "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :try_start_0
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->createContentItemForLocalPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 204
    :goto_0
    if-nez v0, :cond_1

    .line 206
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .end local v0    # "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v9, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;)V

    .line 209
    .restart local v0    # "contentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :cond_1
    return-object v0

    .line 199
    :catch_0
    move-exception v2

    .line 200
    .local v2, "e":Ljava/lang/Exception;
    const-class v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v5, "createContentItemForLocalContentAtPath"

    const-string v6, "exception while creating content item"

    new-array v7, v11, [Ljava/lang/Object;

    const-string v8, "path"

    aput-object v8, v7, v9

    aput-object p0, v7, v10

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static registerContentItemCreator(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;)V
    .locals 6
    .param p0, "creator"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->getType()I

    move-result v3

    .line 215
    .local v3, "type":I
    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByType:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;->getSupportedFileExtensions()Ljava/util/Collection;

    move-result-object v1

    .line 220
    .local v1, "extensions":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 227
    :cond_0
    return-void

    .line 224
    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    .local v0, "extension":Ljava/lang/String;
    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->mItemCreatorsByExtension:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static resolveLocalContentPath(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .locals 12
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 153
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v9

    .line 154
    .local v2, "columns":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 156
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 161
    if-nez v6, :cond_0

    .line 162
    const-class v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v1, "resolveContentPath"

    const-string v3, "query failed "

    new-array v4, v11, [Ljava/lang/Object;

    const-string v5, "uri"

    aput-object v5, v4, v9

    aput-object p1, v4, v10

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 181
    :goto_0
    return-object v8

    .line 157
    :catch_0
    move-exception v7

    .line 158
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 166
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    const-class v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v1, "resolveContentPath"

    const-string v3, "cursor is empty"

    new-array v4, v11, [Ljava/lang/Object;

    const-string v5, "uri"

    aput-object v5, v4, v9

    aput-object p1, v4, v10

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 168
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 173
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 181
    .local v8, "path":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 175
    .end local v8    # "path":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 176
    .restart local v7    # "e":Ljava/lang/Exception;
    :try_start_2
    const-class v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;

    const-string v1, "resolveContentPath"

    const-string v3, "exception while retrieving path from cursor"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v9, "uri"

    aput-object v9, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x2

    const-string v9, "exception"

    aput-object v9, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method
