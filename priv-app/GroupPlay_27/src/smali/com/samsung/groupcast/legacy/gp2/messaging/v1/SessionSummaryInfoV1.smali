.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "SessionSummaryInfoV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1$1;

    const-string v1, "SESSION_SUMMARY_INFO_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V
    .locals 2
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .prologue
    .line 28
    const-string v0, "SESSION_SUMMARY_INFO_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 29
    const-string v0, "summary"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 31
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeInt(I)V

    .line 32
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getManifestId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getManifestAge()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeLong(J)V

    .line 35
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getPinHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSourceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getFirstPageTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeString(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getDocumentCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeInt(I)V

    .line 40
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getImageCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeInt(I)V

    .line 41
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getAudioCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->writeInt(I)V

    .line 42
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v2, "SESSION_SUMMARY_INFO_V1"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readInt()I

    move-result v3

    .line 47
    .local v3, "version":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "sessionId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v5

    .line 49
    .local v5, "manifestId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readLong()J

    move-result-wide v15

    .line 50
    .local v15, "manifestAge":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v8

    .line 51
    .local v8, "pinHash":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v9

    .line 52
    .local v9, "sourceName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v10

    .line 53
    .local v10, "title":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readString()Ljava/lang/String;

    move-result-object v11

    .line 55
    .local v11, "firstPageTitle":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readInt()I

    move-result v12

    .line 56
    .local v12, "documentCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readInt()I

    move-result v13

    .line 57
    .local v13, "imageCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->readInt()I

    move-result v14

    .line 58
    .local v14, "audioCount":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v17

    sub-long v6, v17, v15

    .line 59
    .local v6, "manifestTimestamp":J
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    invoke-direct/range {v2 .. v14}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getSummary()Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "summary"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
