.class Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;
.super Landroid/os/AsyncTask;
.source "CollagePageManifestGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->processTasks(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 231
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 235
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CLG CreateContentItm:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I
    invoke-static {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 236
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    move-result v1

    .line 237
    .local v1, "iStart":I
    sget v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->MAX_IMAGES_PER_COLLAGE_PAGE:I

    add-int v0, v1, v4

    .line 238
    .local v0, "iEnd":I
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    move-result v4

    if-le v0, v4, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    move-result v0

    .line 240
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .local v3, "srcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    if-ge v1, v0, :cond_1

    .line 243
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # operator++ for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$008(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    .line 245
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 248
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    const/4 v4, 0x2

    if-le v0, v4, :cond_2

    .line 249
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    add-int/lit8 v5, v0, -0x2

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    :cond_2
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    invoke-direct {v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;-><init>(Ljava/util/ArrayList;)V

    .line 253
    .local v2, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCollectionName:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I
    invoke-static {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    move-result v6

    invoke-virtual {v4, v5, v6, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->addItemAsPage(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 254
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # ++operator for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$404(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I

    .line 255
    const/4 v4, 0x0

    return-object v4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 231
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mShowProgress:Z
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Z

    move-result v1

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->processTasks(Z)V
    invoke-static {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Z)V

    .line 261
    return-void
.end method
