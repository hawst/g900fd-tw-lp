.class public interface abstract Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
.super Ljava/lang/Object;
.source "SharedExperienceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SharedExperienceManagerDelegate"
.end annotation


# virtual methods
.method public abstract onCurrentPageChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V
.end method

.method public abstract onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
.end method

.method public abstract onPageCommand(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;)V
.end method

.method public abstract onPageEdited(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)Z
.end method

.method public abstract onPeerChanged()V
.end method

.method public abstract onRegisterForContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V
.end method

.method public abstract onUnregisterFromContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V
.end method

.method public abstract onUserJoined(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;)V
.end method

.method public abstract onViewerPeerChanged()V
.end method
