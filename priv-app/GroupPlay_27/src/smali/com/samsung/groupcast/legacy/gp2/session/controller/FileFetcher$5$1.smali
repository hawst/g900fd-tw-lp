.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;
.super Ljava/lang/Object;
.source "FileFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

.field final synthetic val$results:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->val$results:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 275
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    iget-object v5, v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 285
    :cond_0
    return-void

    .line 277
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    iget-object v5, v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$requests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 279
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 280
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    iget-object v5, v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$requests:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 281
    .local v3, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->val$results:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/misc/requests/Result;

    .line 282
    .local v4, "result":Lcom/samsung/groupcast/misc/requests/Result;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5$1;->this$1:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    iget-object v5, v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;->val$destinationPaths:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 283
    .local v1, "destinationPath":Ljava/lang/String;
    invoke-virtual {v3, v4, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 279
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
