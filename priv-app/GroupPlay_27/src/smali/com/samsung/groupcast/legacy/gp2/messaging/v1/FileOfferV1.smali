.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
.super Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;
.source "FileOfferV1.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/FileOffer;


# instance fields
.field private final mToken:Lcom/samsung/groupcast/core/messaging/FileOfferToken;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/FileInfo;Lcom/samsung/groupcast/core/messaging/FileOfferToken;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/groupcast/core/messaging/FileInfo;
    .param p2, "token"    # Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;-><init>(Lcom/samsung/groupcast/core/messaging/FileInfo;)V

    .line 16
    const-string v0, "token"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 17
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->mToken:Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    .line 18
    return-void
.end method


# virtual methods
.method public getToken()Lcom/samsung/groupcast/core/messaging/FileOfferToken;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->mToken:Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    return-object v0
.end method

.method public notifyListenerOfFailure(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    .line 45
    return-void
.end method

.method public notifyListenerOfOffer(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    .line 28
    return-void
.end method

.method public notifyListenerOfProgress(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;JJ)V
    .locals 8
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;
    .param p4, "offset"    # J
    .param p6, "size"    # J

    .prologue
    .line 39
    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p0

    move-wide v4, p4

    move-wide v6, p6

    invoke-interface/range {v0 .. v7}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V

    .line 40
    return-void
.end method

.method public notifyListenerOfSuccess(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;
    .param p4, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-interface {p1, p2, p3, p0, p4}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "info"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "token"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->mToken:Lcom/samsung/groupcast/core/messaging/FileOfferToken;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
