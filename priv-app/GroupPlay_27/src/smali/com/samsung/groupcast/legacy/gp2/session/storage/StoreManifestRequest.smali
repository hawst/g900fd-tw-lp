.class public Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
.super Lcom/samsung/groupcast/misc/requests/BaseRequest;
.source "StoreManifestRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/groupcast/misc/requests/BaseRequest",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field private final mSessionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 0
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p3, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 16
    invoke-direct {p0, p3}, Lcom/samsung/groupcast/misc/requests/BaseRequest;-><init>(Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 17
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->mSessionId:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 19
    return-void
.end method


# virtual methods
.method public complete(Lcom/samsung/groupcast/misc/requests/Result;)V
    .locals 1
    .param p1, "result"    # Lcom/samsung/groupcast/misc/requests/Result;

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 38
    return-void
.end method

.method public getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->mSessionId:Ljava/lang/String;

    return-object v0
.end method
