.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "ManifestInfoV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mAge:J

.field private final mManifestId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1$1;

    const-string v1, "MANIFEST_INFO_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 1
    .param p1, "manifestId"    # Ljava/lang/String;
    .param p2, "age"    # J

    .prologue
    .line 26
    const-string v0, "MANIFEST_INFO_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 27
    const-string v0, "manifestId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 28
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mManifestId:Ljava/lang/String;

    .line 29
    iput-wide p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mAge:J

    .line 30
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->writeString(Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->writeLong(J)V

    .line 32
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v0, "MANIFEST_INFO_V1"

    invoke-direct {p0, v0, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mManifestId:Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mAge:J

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getAge()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mAge:J

    return-wide v0
.end method

.method public getManifestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mManifestId:Ljava/lang/String;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 55
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "manifestId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mManifestId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "age"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->mAge:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
