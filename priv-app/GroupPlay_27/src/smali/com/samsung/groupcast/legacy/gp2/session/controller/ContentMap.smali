.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
.super Ljava/lang/Object;
.source "ContentMap.java"


# instance fields
.field private mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field private final mPaths:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mUnmapedContendId:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 5
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    .line 15
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mPaths:Ljava/util/HashMap;

    .line 18
    const-string v4, "manifest"

    invoke-static {v4, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 19
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 22
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 23
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItems()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 24
    .local v1, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 25
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .end local v1    # "contentId":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method


# virtual methods
.method public addItemMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 46
    const-string v0, "contentId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    const-string v0, "path"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "addItemMapping"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "contentId"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "path"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mPaths:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 51
    return-void
.end method

.method public getContentPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 41
    const-string v0, "contentId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mPaths:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasUnmappedPages()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 5
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 28
    const-string v4, "manifest"

    invoke-static {v4, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 31
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    .line 34
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 35
    .local v0, "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItems()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 36
    .local v1, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mPaths:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 37
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 38
    .end local v0    # "collection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .end local v1    # "contentId":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "manifestId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "mappedPaths"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mPaths:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "unmappedPages"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->mUnmapedContendId:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
