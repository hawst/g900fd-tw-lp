.class public abstract Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
.super Ljava/lang/Object;
.source "PageCommandHandler.java"


# instance fields
.field protected mContentDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

.field protected mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p1, "sharedExperienceManager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->mSharedExperienceManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .line 12
    return-void
.end method


# virtual methods
.method public applyPageCommand(Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;)Z
    .locals 1
    .param p1, "command"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;
    .param p2, "state"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    .prologue
    .line 21
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->applyPageCommand(Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;Z)Z

    move-result v0

    return v0
.end method

.method public abstract applyPageCommand(Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;Z)Z
.end method

.method public setContentDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->mContentDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    .line 18
    return-void
.end method
