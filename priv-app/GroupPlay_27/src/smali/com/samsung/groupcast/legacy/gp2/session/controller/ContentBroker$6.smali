.class Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;
.source "ContentBroker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->handleContentRequestViaNetworkRetrieval(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

.field final synthetic val$contentId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->val$contentId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 6
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 300
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onComplete"

    const/4 v2, 0x0

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "request"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "result"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    const/4 v4, 0x4

    const-string v5, "path"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object p3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 303
    :try_start_0
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->val$contentId:Ljava/lang/String;

    invoke-virtual {v0, v2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->addItemMapping(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->val$contentId:Ljava/lang/String;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->completeContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    invoke-static {v0, v2, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mRunningFetchRequests:Ljava/util/LinkedHashMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Ljava/util/LinkedHashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->val$contentId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->processNextDownloadFetchRequest()V
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)V

    .line 310
    monitor-exit v1

    .line 311
    return-void

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 6
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 291
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onProgress"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "request"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "progress"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mFetchLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$6;->val$contentId:Ljava/lang/String;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->progressContentRequests(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V
    invoke-static {v0, v2, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V

    .line 295
    monitor-exit v1

    .line 296
    return-void

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
