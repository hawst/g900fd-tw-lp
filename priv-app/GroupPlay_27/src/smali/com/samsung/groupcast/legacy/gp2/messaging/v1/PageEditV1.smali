.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "PageEditV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mContentId:Ljava/lang/String;

.field private final mEncoding:I

.field private final mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1$1;

    const-string v1, "PAGE_EDIT_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)V
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "encoding"    # I
    .param p3, "pageEdit"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .prologue
    .line 31
    const-string v1, "PAGE_EDIT_V1"

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 32
    const-string v1, "pageEdit"

    invoke-static {v1, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mContentId:Ljava/lang/String;

    .line 34
    iput p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mEncoding:I

    .line 35
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .line 36
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mContentId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->writeString(Ljava/lang/String;)V

    .line 37
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mEncoding:I

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->writeInt(I)V

    .line 39
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "jsonString":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->writeString(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v3, "PAGE_EDIT_V1"

    invoke-direct {p0, v3, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mContentId:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mEncoding:I

    .line 47
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->readString()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "json":Lorg/json/JSONObject;
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    invoke-direct {v3, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;-><init>(Lorg/json/JSONObject;)V

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    return-void

    .line 52
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid json in payload: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public getEncoding()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mEncoding:I

    return v0
.end method

.method public getPageEdit()Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "pageEdit"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->mPageEdit:Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
