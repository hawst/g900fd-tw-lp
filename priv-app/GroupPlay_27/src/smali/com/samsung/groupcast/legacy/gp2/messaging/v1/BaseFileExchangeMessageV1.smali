.class public abstract Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;
.super Ljava/lang/Object;
.source "BaseFileExchangeMessageV1.java"

# interfaces
.implements Lcom/samsung/groupcast/core/messaging/FileExchangeMessage;


# instance fields
.field private final mInfo:Lcom/samsung/groupcast/core/messaging/FileInfo;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/DataKey;)V
    .locals 2
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "fileIssueId":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/core/messaging/FileInfo;

    invoke-direct {v1, p1, v0}, Lcom/samsung/groupcast/core/messaging/FileInfo;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;->mInfo:Lcom/samsung/groupcast/core/messaging/FileInfo;

    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/FileInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/groupcast/core/messaging/FileInfo;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "info"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;->mInfo:Lcom/samsung/groupcast/core/messaging/FileInfo;

    .line 22
    return-void
.end method


# virtual methods
.method public getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;->mInfo:Lcom/samsung/groupcast/core/messaging/FileInfo;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "FILE_EXCHANGE_V1"

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 36
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "info"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/BaseFileExchangeMessageV1;->mInfo:Lcom/samsung/groupcast/core/messaging/FileInfo;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
