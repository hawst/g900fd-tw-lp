.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;
.super Ljava/lang/Object;
.source "ProtocolV1.java"


# static fields
.field public static final CHANNEL_ID_LOBBY:Ljava/lang/String; = "com.samsung.groupcast.Lobby_V1"

.field public static final CONTENT_AVAILABILITY_STATUS_AVAILABLE:I = 0x0

.field public static final CONTENT_AVAILABILITY_STATUS_LOADING:I = 0x1

.field public static final CONTENT_AVAILABILITY_STATUS_NOT_AVAILABLE:I = 0x2

.field public static final DATA_TYPE_CONTENT:Ljava/lang/String; = "CONTENT"

.field public static final DATA_TYPE_MANIFEST:Ljava/lang/String; = "MANIFEST"

.field public static final EndOfProtocol:Ljava/lang/String; = "&"

.field public static final PAGE_EDIT_ENCODING_JSON:I = 0x0

.field public static final PAYLOAD_CONTENT_AVAILABILITY_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_CONTENT_FILE_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_CURRENT_PAGE_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_CURRENT_PAGE_REQUEST:[Ljava/lang/String;

.field public static final PAYLOAD_DRAWING_SYNC_JSON_REQ:[Ljava/lang/String;

.field public static final PAYLOAD_DRAWING_SYNC_JSON_RSP:[Ljava/lang/String;

.field public static final PAYLOAD_DRAWING_SYNC_NOTI:[Ljava/lang/String;

.field public static final PAYLOAD_FILE_REQUEST:[Ljava/lang/String;

.field public static final PAYLOAD_MANIFEST_FILE_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_MANIFEST_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_MANIFEST_INFO_REQUEST:[Ljava/lang/String;

.field public static final PAYLOAD_PAGE_COMMAND:[Ljava/lang/String;

.field public static final PAYLOAD_PAGE_EDIT:[Ljava/lang/String;

.field public static final PAYLOAD_PING:[Ljava/lang/String;

.field public static final PAYLOAD_PONG:[Ljava/lang/String;

.field public static final PAYLOAD_REMOTE_CONTROL_PEER:[Ljava/lang/String;

.field public static final PAYLOAD_REMOTE_FILE_AVAILABILITY_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_REMOTE_NETWORK_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_SESSION_INFO_INFO:[Ljava/lang/String;

.field public static final PAYLOAD_SESSION_SUMMARY_REQUEST:[Ljava/lang/String;

.field public static final TYPE_CURRENT_PAGE_INFO:Ljava/lang/String; = "CURRENT_PAGE_INFO_V1"

.field public static final TYPE_CURRENT_PAGE_REQUEST:Ljava/lang/String; = "CURRENT_PAGE_REQUEST_V1"

.field public static final TYPE_DRAWING_SYNC_JSON_REQ:Ljava/lang/String; = "DS_REQ_V1"

.field public static final TYPE_DRAWING_SYNC_JSON_RSP:Ljava/lang/String; = "DS_RSP_V1"

.field public static final TYPE_DRAWING_SYNC_NOTI:Ljava/lang/String; = "DS_NOTI_V1"

.field public static final TYPE_FILE_AVAILABILITY_INFO:Ljava/lang/String; = "CONTENT_AVAILABILITY_INFO_V1"

.field public static final TYPE_FILE_EXCHANGE:Ljava/lang/String; = "FILE_EXCHANGE_V1"

.field public static final TYPE_FILE_REQUEST:Ljava/lang/String; = "FILE_REQUEST_V1"

.field public static final TYPE_MANIFEST_INFO:Ljava/lang/String; = "MANIFEST_INFO_V1"

.field public static final TYPE_MANIFEST_INFO_REQUEST:Ljava/lang/String; = "MANIFEST_INFO_REQUEST_V1"

.field public static final TYPE_PAGE_COMMAND:Ljava/lang/String; = "PAGE_COMMAND_V1"

.field public static final TYPE_PAGE_EDIT:Ljava/lang/String; = "PAGE_EDIT_V1"

.field public static final TYPE_PING:Ljava/lang/String; = "PING_V1"

.field public static final TYPE_PONG:Ljava/lang/String; = "PONG_V1"

.field public static final TYPE_REMOTE_CONTROL_PEER:Ljava/lang/String; = "REMOTE_CONTROL_PEER_V1"

.field public static final TYPE_REMOTE_FILE_AVAILABILITY_INFO:Ljava/lang/String; = "REMOTE_FILE_AVAILABILITY_INFO_V1"

.field public static final TYPE_REMOTE_NETWORK_INFO:Ljava/lang/String; = "REMOTE_NETWORK_INFO_V1"

.field public static final TYPE_SESSION_SUMMARY_INFO:Ljava/lang/String; = "SESSION_SUMMARY_INFO_V1"

.field public static final TYPE_SESSION_SUMMARY_REQUEST:Ljava/lang/String; = "SESSION_SUMMARY_REQUEST_V1"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "INT#ID"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_PING:[Ljava/lang/String;

    .line 26
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "INT#ID"

    aput-object v1, v0, v3

    const-string v1, "STRING#NAME"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_PONG:[Ljava/lang/String;

    .line 33
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_SESSION_SUMMARY_REQUEST:[Ljava/lang/String;

    .line 38
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "INT#VERSION"

    aput-object v1, v0, v3

    const-string v1, "STRING#SESSION_ID"

    aput-object v1, v0, v4

    const-string v1, "STRING#MANIFEST_ID"

    aput-object v1, v0, v5

    const-string v1, "LONG#MANIFEST_AGE"

    aput-object v1, v0, v6

    const-string v1, "STRING#PIN_HASH"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "STRING#TITLE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "STRING#SOURCE_NAME"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "STRING#FIRST_PAGE_TITLE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "INT#PAGE_COUNT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "INT#DOCUMENT_COUNT"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "INT#IMAGE_COUNT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "INT#AUDIO_COUNT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_SESSION_INFO_INFO:[Ljava/lang/String;

    .line 55
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_CURRENT_PAGE_REQUEST:[Ljava/lang/String;

    .line 60
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "STRING#CONTENT_ID"

    aput-object v1, v0, v3

    const-string v1, "LONG#AGE"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_CURRENT_PAGE_INFO:[Ljava/lang/String;

    .line 67
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "STRING#CONTENT_ID"

    aput-object v1, v0, v3

    const-string v1, "STRING#TYPE"

    aput-object v1, v0, v4

    const-string v1, "STRING#COMMAND_NAME"

    aput-object v1, v0, v5

    const-string v1, "INT#NUM_VALUES"

    aput-object v1, v0, v6

    const-string v1, "INT#KEY[0]"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "INT#TYPE[0]"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<VARIABLE>#VALUE[0]"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "..."

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "INT#KEY[NUM_VALUES - 1]"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "INT#TYPE[NUM_VALUES - 1]"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "<VARIABLE>#VALUE[NUM_VALUES - 1]"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_PAGE_COMMAND:[Ljava/lang/String;

    .line 83
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_MANIFEST_INFO_REQUEST:[Ljava/lang/String;

    .line 88
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "STRING#MANIFEST_ID"

    aput-object v1, v0, v3

    const-string v1, "LONG#AGE"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_MANIFEST_INFO:[Ljava/lang/String;

    .line 98
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "STRING#CONTENT_ID"

    aput-object v1, v0, v3

    const-string v1, "INT#STATUS"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_CONTENT_AVAILABILITY_INFO:[Ljava/lang/String;

    .line 106
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "STRING#CONTENT_ID"

    aput-object v1, v0, v3

    const-string v1, "INT#ENCODING"

    aput-object v1, v0, v4

    const-string v1, "<VARIABLE>#PAGE_EDIT"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_PAGE_EDIT:[Ljava/lang/String;

    .line 115
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "OBJECT#DRAWING_SYNC_RSP"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_DRAWING_SYNC_NOTI:[Ljava/lang/String;

    .line 121
    new-array v0, v3, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_DRAWING_SYNC_JSON_REQ:[Ljava/lang/String;

    .line 126
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "OBJECT#DRAWING_SYNC_JSON_RSP"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_DRAWING_SYNC_JSON_RSP:[Ljava/lang/String;

    .line 133
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "STRING#NETID"

    aput-object v1, v0, v3

    const-string v1, "STRING#PEERID"

    aput-object v1, v0, v4

    const-string v1, "STRING#ACCOUNT"

    aput-object v1, v0, v5

    const-string v1, "STRING#TOKEN"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_REMOTE_NETWORK_INFO:[Ljava/lang/String;

    .line 143
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "STRING#FILETYPE"

    aput-object v1, v0, v3

    const-string v1, "STRING#URL"

    aput-object v1, v0, v4

    const-string v1, "STRING#JSON"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_REMOTE_FILE_AVAILABILITY_INFO:[Ljava/lang/String;

    .line 151
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "STRING#DATA_TYPE"

    aput-object v1, v0, v3

    const-string v1, "STRING#DATA_ID"

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_FILE_REQUEST:[Ljava/lang/String;

    .line 161
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "STRING#DATA_TYPE"

    aput-object v1, v0, v3

    const-string v1, "STRING#DATA_ID"

    aput-object v1, v0, v4

    const-string v1, "STRING#FILE_ISSUE_ID"

    aput-object v1, v0, v5

    const-string v1, "STRING#SESSION_ID"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_MANIFEST_FILE_INFO:[Ljava/lang/String;

    .line 170
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "STRING#DATA_TYPE"

    aput-object v1, v0, v3

    const-string v1, "STRING#DATA_ID"

    aput-object v1, v0, v4

    const-string v1, "STRING#FILE_ISSUE_ID"

    aput-object v1, v0, v5

    const-string v1, "STRING#FILENAME"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_CONTENT_FILE_INFO:[Ljava/lang/String;

    .line 179
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "STRING#CMD"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ProtocolV1;->PAYLOAD_REMOTE_CONTROL_PEER:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
