.class public Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
.super Ljava/lang/Object;
.source "PageEditNode.java"


# static fields
.field private static final KEY_NODES:Ljava/lang/String; = "N"

.field private static final KEY_OPS:Ljava/lang/String; = "O"


# instance fields
.field private final mNodeByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;",
            ">;"
        }
    .end annotation
.end field

.field private final mOps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    .line 23
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 11
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    iput-object v10, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    .line 21
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    .line 26
    const-string v10, "json"

    invoke-static {v10, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    const-string v10, "O"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 30
    const-string v10, "O"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 31
    .local v9, "opsJson":Lorg/json/JSONArray;
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 33
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 34
    invoke-virtual {v9, v3}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    .line 35
    .local v2, "editJson":Lorg/json/JSONArray;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    invoke-direct {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;-><init>(Lorg/json/JSONArray;)V

    .line 36
    .local v1, "edit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    iget-object v10, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 41
    .end local v0    # "count":I
    .end local v1    # "edit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    .end local v2    # "editJson":Lorg/json/JSONArray;
    .end local v3    # "i":I
    .end local v9    # "opsJson":Lorg/json/JSONArray;
    :cond_0
    const-string v10, "N"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 42
    const-string v10, "N"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 43
    .local v8, "nodesJson":Lorg/json/JSONObject;
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v4

    .line 45
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 46
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 47
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v8, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 48
    .local v7, "nodeJson":Lorg/json/JSONObject;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    invoke-direct {v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;-><init>(Lorg/json/JSONObject;)V

    .line 49
    .local v6, "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    iget-object v10, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v10, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 52
    .end local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    .end local v7    # "nodeJson":Lorg/json/JSONObject;
    .end local v8    # "nodesJson":Lorg/json/JSONObject;
    :cond_1
    return-void
.end method


# virtual methods
.method public appendDouble(Ljava/lang/String;D)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # D

    .prologue
    .line 127
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->append(Ljava/lang/String;D)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    return-void
.end method

.method public appendInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 117
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->append(Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public appendLong(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 122
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 123
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->append(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    return-void
.end method

.method public appendString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->append(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    return-void
.end method

.method public apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V
    .locals 7
    .param p1, "pageState"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    .prologue
    .line 133
    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    .line 134
    .local v0, "edit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V

    goto :goto_0

    .line 138
    .end local v0    # "edit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    :cond_0
    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 139
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 140
    .local v3, "name":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .line 141
    .local v4, "subEditNode":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    invoke-virtual {p1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;->getNode(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    move-result-object v5

    .line 142
    .local v5, "subStateNode":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V

    goto :goto_1

    .line 144
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;>;"
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "subEditNode":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    .end local v5    # "subStateNode":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    :cond_1
    return-void
.end method

.method public containsNode(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 14

    .prologue
    .line 148
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 151
    .local v4, "json":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_1

    .line 152
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 154
    .local v8, "nodesJson":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v11}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 155
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 156
    .local v5, "name":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .line 157
    .local v6, "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v7

    .line 158
    .local v7, "nodeJson":Lorg/json/JSONObject;
    invoke-virtual {v8, v5, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "json":Lorg/json/JSONObject;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    .end local v7    # "nodeJson":Lorg/json/JSONObject;
    .end local v8    # "nodesJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Lorg/json/JSONException;
    new-instance v11, Ljava/lang/RuntimeException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "exception while getting Json representation: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 161
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "json":Lorg/json/JSONObject;
    .restart local v8    # "nodesJson":Lorg/json/JSONObject;
    :cond_0
    :try_start_1
    const-string v11, "N"

    invoke-virtual {v4, v11, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 165
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v8    # "nodesJson":Lorg/json/JSONObject;
    :cond_1
    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_3

    .line 166
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 168
    .local v10, "opsJson":Lorg/json/JSONArray;
    iget-object v11, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    .line 169
    .local v9, "op":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    invoke-virtual {v9}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->getJsonRepresentation()Lorg/json/JSONArray;

    move-result-object v1

    .line 170
    .local v1, "editJson":Lorg/json/JSONArray;
    invoke-virtual {v10, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    .line 173
    .end local v1    # "editJson":Lorg/json/JSONArray;
    .end local v9    # "op":Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;
    :cond_2
    const-string v11, "O"

    invoke-virtual {v4, v11, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 176
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v10    # "opsJson":Lorg/json/JSONArray;
    :cond_3
    return-object v4
.end method

.method public getNode(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .line 63
    .local v0, "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .end local v0    # "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;-><init>()V

    .line 65
    .restart local v0    # "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_0
    return-object v0
.end method

.method public setDouble(Ljava/lang/String;D)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # D

    .prologue
    .line 87
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->set(Ljava/lang/String;D)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    return-void
.end method

.method public setDoubleArray(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p2, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Double;>;"
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->setDoubleArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public setInt(Ljava/lang/String;I)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 77
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->set(Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public setIntArray(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->setIntArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public setLong(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 82
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->set(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    return-void
.end method

.method public setLongArray(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p2, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->setLongArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 72
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public setStringArray(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p2, "value":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    const-string v0, "name"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;->setStringArray(Ljava/lang/String;Ljava/util/Collection;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageOp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 186
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "ops"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mOps:Ljava/util/ArrayList;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "nodes"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->mNodeByName:Ljava/util/HashMap;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
