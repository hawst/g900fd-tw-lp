.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;
.super Ljava/lang/Object;
.source "ContentPrefetcher.java"

# interfaces
.implements Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;


# instance fields
.field mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

.field mContentList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mCurrentContent:Ljava/lang/String;

.field mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

.field mInvalidated:Z

.field mLock:Ljava/lang/Object;

.field mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;)V
    .locals 2
    .param p1, "contentBroker"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;
    .param p2, "viewerDelegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mLock:Ljava/lang/Object;

    .line 11
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mCurrentContent:Ljava/lang/String;

    .line 15
    iput-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z

    .line 16
    iput-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mInvalidated:Z

    .line 20
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    .line 21
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    .line 22
    return-void
.end method


# virtual methods
.method public addRequest(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 25
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mInvalidated:Z

    if-eqz v0, :cond_0

    .line 27
    monitor-exit v1

    .line 44
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentBroker:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->getContentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 31
    monitor-exit v1

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 34
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 35
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 38
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z

    if-nez v0, :cond_3

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z

    .line 40
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mCurrentContent:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    invoke-interface {v0, p1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;->onRegisterForContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 43
    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public invalidate()V
    .locals 3

    .prologue
    .line 47
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mCurrentContent:Ljava/lang/String;

    invoke-interface {v0, v2, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;->onUnregisterFromContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mInvalidated:Z

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z

    .line 51
    monitor-exit v1

    .line 52
    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onContentDeliveryAbort(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mInvalidated:Z

    if-eqz v0, :cond_0

    .line 90
    monitor-exit v1

    .line 98
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z

    .line 97
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onContentDeliveryComplete(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 3
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 56
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 57
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mInvalidated:Z

    if-eqz v1, :cond_0

    .line 58
    monitor-exit v2

    .line 76
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 61
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    invoke-interface {v1, p1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;->onUnregisterFromContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 64
    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-eq v1, p2, :cond_1

    .line 65
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mContentList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    .local v0, "nextContentId":Ljava/lang/String;
    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mCurrentContent:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mDeliveryDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    invoke-interface {v1, v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;->onRegisterForContentDelivery(Ljava/lang/String;Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;)V

    .line 75
    .end local v0    # "nextContentId":Ljava/lang/String;
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 73
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentPrefetcher;->mWorking:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public onContentDeliveryProgress(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 0
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 84
    return-void
.end method
