.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
.super Lcom/samsung/groupcast/misc/requests/BaseRequest;
.source "FetchFileRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/groupcast/misc/requests/BaseRequest",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

.field private mProgress:Lcom/samsung/groupcast/misc/requests/Progress;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/DataKey;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 1
    .param p1, "dataKey"    # Lcom/samsung/groupcast/core/messaging/DataKey;
    .param p2, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 19
    invoke-direct {p0, p2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;-><init>(Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 16
    invoke-static {}, Lcom/samsung/groupcast/misc/requests/Progress;->none()Lcom/samsung/groupcast/misc/requests/Progress;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    .line 20
    const-string v0, "dataKey"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    return-object v0
.end method


# virtual methods
.method public complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 54
    return-void
.end method

.method public getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    return-object v0
.end method

.method public getProgress()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    return-object v0
.end method

.method public setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 1
    .param p1, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 33
    const-string v0, "progress"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 34
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    .line 35
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->updateListeners(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 43
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "dataKey"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->mDataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
