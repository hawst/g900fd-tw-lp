.class public Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
.super Ljava/lang/Object;
.source "Session.java"


# static fields
.field private static GP_RUNNING_STATE_CHECK_FILE:Ljava/lang/String; = null

.field public static final PAGE_STATE_SCHEMA_CLEAR_ALL:Ljava/lang/String; = "CLEAR_ALL"

.field public static final PAGE_STATE_SCHEMA_CLEAR_ALL_VALUE:Ljava/lang/String; = "VALUE"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM:Ljava/lang/String; = "POINT_STREAM"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_CLOSING:Ljava/lang/String; = "CLOSING"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_COLOR:Ljava/lang/String; = "COLOR"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_ERASER:Ljava/lang/String; = "ERASER"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_PATH_ID:Ljava/lang/String; = "PATH_ID"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_RELATIVE_SIZE:Ljava/lang/String; = "RELATIVE_SIZE"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_SIZE:Ljava/lang/String; = "SIZE"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_X:Ljava/lang/String; = "X"

.field public static final PAGE_STATE_SCHEMA_POINT_STREAM_Y:Ljava/lang/String; = "Y"

.field private static lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;


# instance fields
.field private final mCurrentPageInCollection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;",
            ">;"
        }
    .end annotation
.end field

.field private mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field private final mPageStateByContentId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageStateByContentIdNEW:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;",
            ">;"
        }
    .end annotation
.end field

.field private final mPin:Ljava/lang/String;

.field private final mSessionId:Ljava/lang/String;

.field private final mSourceName:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 46
    const-string v0, ".gp_running_check"

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->GP_RUNNING_STATE_CHECK_FILE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 1
    .param p1, "version"    # I
    .param p2, "pin"    # Ljava/lang/String;
    .param p3, "sourceName"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mCurrentPageInCollection:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentId:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentIdNEW:Ljava/util/HashMap;

    .line 99
    const-string v0, "manifest"

    invoke-static {v0, p5}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 100
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mVersion:I

    .line 101
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSessionId:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSourceName:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mTitle:Ljava/lang/String;

    .line 106
    iput-object p5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 107
    return-void
.end method

.method private constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 1
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p2, "pin"    # Ljava/lang/String;
    .param p3, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mCurrentPageInCollection:Ljava/util/HashMap;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentId:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentIdNEW:Ljava/util/HashMap;

    .line 110
    const-string v0, "summary"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    const-string v0, "manifest"

    invoke-static {v0, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getVersion()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mVersion:I

    .line 113
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSessionId:Ljava/lang/String;

    .line 114
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSourceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSourceName:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mTitle:Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 119
    return-void
.end method

.method public static ClearLastSession()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 90
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->deleteGpRunningStateCheckFile()V

    .line 91
    return-void
.end method

.method public static HasSession()Z
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createFreshSession(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 6
    .param p0, "version"    # I
    .param p1, "pin"    # Ljava/lang/String;
    .param p2, "sourceName"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 75
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 76
    .local v0, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 77
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->createGpRunningStateCheckFile()V

    .line 78
    return-object v0
.end method

.method public static createGpRunningStateCheckFile()V
    .locals 6

    .prologue
    .line 50
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->GP_RUNNING_STATE_CHECK_FILE:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/groupcast/application/App;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 51
    .local v2, "fos":Ljava/io/FileOutputStream;
    if-eqz v2, :cond_0

    .line 52
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 56
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 57
    .local v1, "e2":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static createOngoingSession(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 1
    .param p0, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .param p1, "pin"    # Ljava/lang/String;
    .param p2, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 82
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 83
    .local v0, "session":Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->lastSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 84
    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->createGpRunningStateCheckFile()V

    .line 85
    return-object v0
.end method

.method public static deleteGpRunningStateCheckFile()V
    .locals 5

    .prologue
    .line 63
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->GP_RUNNING_STATE_CHECK_FILE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/application/App;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    .line 64
    .local v2, "fis":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 65
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->GP_RUNNING_STATE_CHECK_FILE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/application/App;->deleteFile(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 71
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 68
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 69
    .local v1, "e2":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private getPageStateSchemaForItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    .locals 4
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 206
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;

    invoke-direct {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;-><init>()V

    .line 208
    .local v2, "schema":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    const-string v3, "CLEAR_ALL"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createNode(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;

    move-result-object v0

    .line 209
    .local v0, "clearAllSchema":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    const-string v3, "VALUE"

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createIntProperty(Ljava/lang/String;)V

    .line 210
    const-string v3, "POINT_STREAM"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createNode(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;

    move-result-object v1

    .line 211
    .local v1, "pointStreamSchema":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    const-string v3, "PATH_ID"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createStringProperty(Ljava/lang/String;)V

    .line 212
    const-string v3, "COLOR"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createIntProperty(Ljava/lang/String;)V

    .line 213
    const-string v3, "SIZE"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createDoubleProperty(Ljava/lang/String;)V

    .line 214
    const-string v3, "X"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createDoubleProperty(Ljava/lang/String;)V

    .line 215
    const-string v3, "Y"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createDoubleProperty(Ljava/lang/String;)V

    .line 216
    const-string v3, "ERASER"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createIntProperty(Ljava/lang/String;)V

    .line 217
    const-string v3, "CLOSING"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createIntProperty(Ljava/lang/String;)V

    .line 218
    const-string v3, "RELATIVE_SIZE"

    invoke-virtual {v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->createDoubleProperty(Ljava/lang/String;)V

    .line 223
    return-object v2
.end method

.method public static getPinHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    .line 19
    const-string v2, "sessionId"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    :cond_0
    const/4 v1, 0x0

    .line 27
    :goto_0
    return-object v1

    .line 25
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "input":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/StringTools;->getSHA1Digest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 27
    .local v1, "pinHash":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public clearPageState(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentId:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    return-void
.end method

.method public getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .locals 2
    .param p1, "collectionName"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mCurrentPageInCollection:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .line 160
    .local v0, "page":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    return-object v0
.end method

.method public getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object v0
.end method

.method public getPageState(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    .locals 4
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentId:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    .line 171
    .local v1, "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    if-nez v1, :cond_0

    .line 172
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    invoke-virtual {v3, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v0

    .line 173
    .local v0, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPageStateSchemaForItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;

    move-result-object v2

    .line 174
    .local v2, "schema":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    .end local v1    # "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    invoke-direct {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;)V

    .line 175
    .restart local v1    # "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentId:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    .end local v0    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .end local v2    # "schema":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    :cond_0
    return-object v1
.end method

.method public getPageStateNEW(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 183
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentIdNEW:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    .line 185
    .local v0, "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    .end local v0    # "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;-><init>()V

    .line 187
    .restart local v0    # "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPageStateByContentIdNEW:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_0
    return-object v0
.end method

.method public getPin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    return-object v0
.end method

.method public getPinHash()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSessionId:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPinHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSourceName:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mVersion:I

    return v0
.end method

.method public isPinProtected()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mPin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCurrentPage(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "page"    # Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .prologue
    .line 164
    const-string v0, "info"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 165
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mCurrentPageInCollection:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    return-void
.end method

.method public setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 1
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 154
    const-string v0, "manifest"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 155
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .line 156
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 232
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mSessionId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "manifest"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "currentPagesInCollection"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->mCurrentPageInCollection:Ljava/util/HashMap;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
