.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "PongV1.java"


# static fields
.field public static final EXTRAINFO_KEY_PHONE_NO:Ljava/lang/String; = "EKPN"

.field public static final EXTRAINFO_KEY_VOIP_GROUP_HASH:Ljava/lang/String; = "EKVG"

.field public static final EXTRAINFO_KEY_VOIP_GROUP_PEERID:Ljava/lang/String; = "EKVP"

.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private mContentsResetEnabled:Z

.field private mExtraJsonString:Ljava/lang/String;

.field private final mId:Ljava/lang/Integer;

.field private final mJoinedActivities:[Ljava/lang/String;

.field private mJoinedGames:[Ljava/lang/String;

.field private final mName:Ljava/lang/String;

.field private final mWifiIp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1$1;

    const-string v1, "PONG_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/Integer;
    .param p2, "userInfo"    # Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    .prologue
    .line 45
    const-string v1, "PONG_V1"

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mId:Ljava/lang/Integer;

    .line 47
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mName:Ljava/lang/String;

    .line 48
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getWifiIp()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mWifiIp:Ljava/lang/String;

    .line 49
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getJoinedActivities()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    .line 50
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getContentsResetEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z

    .line 51
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getJoinedGames()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    .line 52
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getExtraInfoJsonString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    .line 54
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeInt(I)V

    .line 55
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mWifiIp:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeString(Ljava/lang/String;)V

    .line 57
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeInt(I)V

    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeString(Ljava/lang/String;)V

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z

    invoke-static {v1}, Lcom/samsung/groupcast/misc/utility/BooleanTool;->booleanToInt(Z)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeInt(I)V

    .line 61
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    array-length v1, v1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeInt(I)V

    .line 62
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 63
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeString(Ljava/lang/String;)V

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->writeString(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v5, 0x0

    .line 68
    const-string v2, "PONG_V1"

    invoke-direct {p0, v2, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readInt()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mId:Ljava/lang/Integer;

    .line 70
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mName:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mWifiIp:Ljava/lang/String;

    .line 72
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readInt()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    .line 73
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 73
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/BooleanTool;->intToBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readInt()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    .line 85
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 86
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 85
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "payloadCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iput-boolean v5, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z

    goto :goto_1

    .line 87
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 88
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "payloadCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-array v2, v5, [Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    .line 92
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 96
    :goto_3
    return-void

    .line 93
    :catch_2
    move-exception v0

    .line 94
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public getContentsResetEnabled()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z

    return v0
.end method

.method public getExtraJson()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 123
    const/4 v1, 0x0

    .line 125
    .local v1, "ret":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "ret":Lorg/json/JSONObject;
    .local v2, "ret":Lorg/json/JSONObject;
    move-object v1, v2

    .line 129
    .end local v2    # "ret":Lorg/json/JSONObject;
    .restart local v1    # "ret":Lorg/json/JSONObject;
    :goto_0
    return-object v1

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getExtraJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getJoinedActivities()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedActivities:[Ljava/lang/String;

    return-object v0
.end method

.method public getJoinedGames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mJoinedGames:[Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getWifi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mWifiIp:Ljava/lang/String;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPongV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;)V

    .line 140
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 144
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mId:Ljava/lang/Integer;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "wifiIp"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mWifiIp:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "contentsResetEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mContentsResetEnabled:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "mExtraJsonString"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PongV1;->mExtraJsonString:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
