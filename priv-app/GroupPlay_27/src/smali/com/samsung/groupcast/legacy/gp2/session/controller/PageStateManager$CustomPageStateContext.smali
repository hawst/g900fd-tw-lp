.class Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
.super Ljava/lang/Object;
.source "PageStateManager.java"

# interfaces
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomPageStateContext"
.end annotation


# instance fields
.field private final mContentId:Ljava/lang/String;

.field private mPageState:Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Ljava/lang/String;)V
    .locals 1
    .param p2, "contentId"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const-string v0, "contentId"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->mContentId:Ljava/lang/String;

    .line 110
    return-void
.end method


# virtual methods
.method public editPage(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V
    .locals 1
    .param p1, "block"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->editPage(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V
    invoke-static {v0, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V

    .line 121
    return-void
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 125
    const-class v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "contentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->mContentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "pageState"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->mPageState:Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
