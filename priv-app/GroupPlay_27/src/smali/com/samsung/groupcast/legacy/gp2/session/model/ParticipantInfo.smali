.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
.super Ljava/lang/Object;
.source "ParticipantInfo.java"


# instance fields
.field private mContentsResetEnabled:Z

.field private mJoinedActivities:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mJoinedGame:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mPingId:Ljava/lang/Integer;

.field private mWifiIp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pingId"    # Ljava/lang/Integer;
    .param p3, "wifiIp"    # Ljava/lang/String;
    .param p4, "joinedActivity"    # [Ljava/lang/String;
    .param p5, "contentsResetEnabled"    # Z
    .param p6, "joinedGames"    # [Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    .line 15
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    .line 19
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mName:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mPingId:Ljava/lang/Integer;

    .line 21
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mWifiIp:Ljava/lang/String;

    .line 22
    if-eqz p4, :cond_4

    .line 24
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 25
    .local v0, "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p4

    if-ge v2, v4, :cond_0

    .line 26
    aget-object v4, p4, v2

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 25
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 27
    :cond_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 28
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 29
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 30
    .local v1, "gameName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 31
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 43
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    :goto_2
    array-length v4, p4

    if-ge v2, v4, :cond_4

    .line 44
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    aget-object v5, p4, v2

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 45
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    aget-object v5, p4, v2

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 43
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 58
    .end local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    iput-boolean p5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mContentsResetEnabled:Z

    .line 61
    if-eqz p6, :cond_9

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 64
    .restart local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    array-length v4, p6

    if-ge v2, v4, :cond_5

    .line 65
    aget-object v4, p6, v2

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 66
    :cond_5
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 67
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_6
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 68
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 69
    .restart local v1    # "gameName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 70
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 75
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_7
    const/4 v2, 0x0

    :goto_5
    array-length v4, p6

    if-ge v2, v4, :cond_9

    .line 76
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    aget-object v5, p6, v2

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 77
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    aget-object v5, p6, v2

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 82
    .end local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_9
    return-void
.end method


# virtual methods
.method public getContentsResetEnabled()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mContentsResetEnabled:Z

    return v0
.end method

.method public getJoinedActivities()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    return-object v0
.end method

.method public getJoinedGames()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPingId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mPingId:Ljava/lang/Integer;

    return-object v0
.end method

.method public getWifiIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mWifiIp:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 204
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "pingId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mPingId:Ljava/lang/Integer;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "wifiIp"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mWifiIp:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)Z
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pingId"    # Ljava/lang/Integer;
    .param p3, "wifiIp"    # Ljava/lang/String;
    .param p4, "joinedActivities"    # [Ljava/lang/String;
    .param p5, "contentsResetEnabled"    # Z
    .param p6, "joinedGames"    # [Ljava/lang/String;

    .prologue
    .line 86
    const/4 v4, 0x0

    .line 87
    .local v4, "update":Z
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mName:Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mPingId:Ljava/lang/Integer;

    .line 89
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mWifiIp:Ljava/lang/String;

    .line 90
    if-eqz p4, :cond_4

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 93
    .local v0, "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p4

    if-ge v2, v5, :cond_0

    .line 94
    aget-object v5, p4, v2

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    :cond_0
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 96
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 98
    .local v1, "gameName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 99
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 107
    const/4 v4, 0x1

    goto :goto_1

    .line 112
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    :goto_2
    array-length v5, p4

    if-ge v2, v5, :cond_5

    .line 113
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    aget-object v6, p4, v2

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 114
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    aget-object v6, p4, v2

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 122
    const/4 v4, 0x1

    .line 112
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 126
    .end local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "i":I
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_4
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedActivities:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 127
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 129
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 130
    .restart local v1    # "gameName":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 137
    const/4 v4, 0x1

    .line 138
    goto :goto_3

    .line 141
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_5
    iput-boolean p5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mContentsResetEnabled:Z

    .line 143
    if-eqz p6, :cond_a

    .line 145
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 146
    .restart local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    array-length v5, p6

    if-ge v2, v5, :cond_6

    .line 147
    aget-object v5, p6, v2

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 146
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 148
    :cond_6
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 149
    :cond_7
    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 150
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 151
    .restart local v1    # "gameName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 152
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 153
    const/4 v4, 0x1

    goto :goto_5

    .line 158
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_8
    const/4 v2, 0x0

    :goto_6
    array-length v5, p6

    if-ge v2, v5, :cond_b

    .line 159
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    aget-object v6, p6, v2

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 160
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    aget-object v6, p6, v2

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 161
    const/4 v4, 0x1

    .line 158
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 165
    .end local v0    # "findJoinedGames":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v2    # "i":I
    :cond_a
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->mJoinedGame:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 166
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 168
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 169
    .restart local v1    # "gameName":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 170
    const/4 v4, 0x1

    .line 171
    goto :goto_7

    .line 173
    .end local v1    # "gameName":Ljava/lang/String;
    :cond_b
    return v4
.end method
