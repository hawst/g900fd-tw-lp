.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
.super Lcom/samsung/groupcast/misc/requests/BaseRequest;
.source "ContentRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/groupcast/misc/requests/BaseRequest",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContentId:Ljava/lang/String;

.field private mProgress:Lcom/samsung/groupcast/misc/requests/Progress;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 18
    invoke-direct {p0, p2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;-><init>(Lcom/samsung/groupcast/misc/requests/Starter;)V

    .line 15
    invoke-static {}, Lcom/samsung/groupcast/misc/requests/Progress;->none()Lcom/samsung/groupcast/misc/requests/Progress;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    .line 19
    const-string v0, "contentId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mContentId:Ljava/lang/String;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    return-object v0
.end method


# virtual methods
.method public complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 1
    .param p1, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 45
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 53
    return-void
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    return-object v0
.end method

.method public setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 1
    .param p1, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 32
    const-string v0, "progress"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mProgress:Lcom/samsung/groupcast/misc/requests/Progress;

    .line 34
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->updateListeners(Lcom/samsung/groupcast/misc/requests/Notifier;)V

    .line 42
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 57
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "contentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->mContentId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
