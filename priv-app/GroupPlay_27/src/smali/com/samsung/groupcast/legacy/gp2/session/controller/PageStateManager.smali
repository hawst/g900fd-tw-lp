.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;
.super Ljava/lang/Object;
.source "PageStateManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;
    }
.end annotation


# instance fields
.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;

.field private final mPageStateContextByContentId:Lcom/samsung/groupcast/misc/utility/MultiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/groupcast/misc/utility/MultiMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;",
            ">;"
        }
    .end annotation
.end field

.field private final mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V
    .locals 1
    .param p1, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-direct {v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mPageStateContextByContentId:Lcom/samsung/groupcast/misc/utility/MultiMap;

    .line 26
    const-string v0, "session"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    .param p2, "x2"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->editPage(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V

    return-void
.end method

.method private editPage(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;)V
    .locals 4
    .param p1, "context"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    .param p2, "block"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "contentId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPageState(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    move-result-object v2

    .line 74
    .local v2, "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    invoke-direct {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;-><init>()V

    .line 75
    .local v1, "pageEdit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    invoke-interface {p2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext$EditBlock;->onEditPage(Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)V

    .line 76
    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V

    .line 78
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;

    if-eqz v3, :cond_0

    .line 79
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;

    invoke-interface {v3, p0, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;->onPageStateEdited(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)V

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method public acquirePageStateContext(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 35
    const-string v1, "contentId"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Ljava/lang/String;)V

    .line 37
    .local v0, "context":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mPageStateContextByContentId:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    return-object v0
.end method

.method public releasePageStateContext(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;)V
    .locals 11
    .param p1, "c"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;

    .prologue
    .line 42
    const-string v5, "context"

    invoke-static {v5, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    const/4 v2, 0x0

    .line 47
    .local v2, "context":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;->getContentId()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "contentId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mPageStateContextByContentId:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v5, v1, v2}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "context":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;

    .line 56
    .restart local v2    # "context":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;
    if-nez v2, :cond_0

    .line 57
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "context was not previously acquired or is already released"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 48
    .end local v1    # "contentId":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 49
    .local v4, "e":Ljava/lang/ClassCastException;
    new-instance v5, Ljava/lang/RuntimeException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "context was acquired from this "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 60
    .end local v4    # "e":Ljava/lang/ClassCastException;
    .restart local v1    # "contentId":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "releasePageStateContext"

    const/4 v7, 0x0

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "contentId"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 61
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mPageStateContextByContentId:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v5, v1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    .line 63
    .local v3, "contexts":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$CustomPageStateContext;>;"
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 68
    :cond_1
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;

    .line 32
    return-void
.end method
