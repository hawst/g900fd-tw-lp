.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "PageCommandV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final TYPE_FLOAT:I

.field private final TYPE_INT:I

.field private final TYPE_STRING:I

.field private final mCommandName:Ljava/lang/String;

.field private final mContentId:Ljava/lang/String;

.field private final mType:Ljava/lang/String;

.field private mValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1$1;

    const-string v1, "PAGE_COMMAND_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "commandName"    # Ljava/lang/String;

    .prologue
    .line 39
    const-string v0, "PAGE_COMMAND_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_INT:I

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_FLOAT:I

    .line 29
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_STRING:I

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    .line 40
    const-string v0, "contentId"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    const-string v0, "type"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    const-string v0, "contentId"

    invoke-static {v0, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mContentId:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mType:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mCommandName:Ljava/lang/String;

    .line 48
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeString(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0, p2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeString(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0, p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeString(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v4, "PAGE_COMMAND_V1"

    invoke-direct {p0, v4, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 27
    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_INT:I

    .line 28
    const/4 v4, 0x1

    iput v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_FLOAT:I

    .line 29
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->TYPE_STRING:I

    .line 31
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    .line 55
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mContentId:Ljava/lang/String;

    .line 56
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mType:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mCommandName:Ljava/lang/String;

    .line 59
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readInt()I

    move-result v0

    .line 60
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readInt()I

    move-result v2

    .line 62
    .local v2, "key":I
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readInt()I

    move-result v3

    .line 63
    .local v3, "type":I
    packed-switch v3, :pswitch_data_0

    .line 60
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readInt()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 68
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readFloat()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 71
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 75
    .end local v2    # "key":I
    .end local v3    # "type":I
    :cond_0
    return-void

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getCommandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mCommandName:Ljava/lang/String;

    return-object v0
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMessageType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getPayload()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 123
    .local v0, "count":I
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    .line 124
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 125
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    .line 126
    .local v3, "value":Ljava/lang/Object;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    .line 127
    instance-of v4, v3, Ljava/lang/Integer;

    if-eqz v4, :cond_0

    .line 128
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    .line 129
    check-cast v3, Ljava/lang/Integer;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    goto :goto_0

    .line 130
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v4, v3, Ljava/lang/Float;

    if-eqz v4, :cond_1

    .line 131
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    .line 132
    check-cast v3, Ljava/lang/Float;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeFloat(F)V

    goto :goto_0

    .line 133
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 134
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeInt(I)V

    .line 135
    check-cast v3, Ljava/lang/String;

    .end local v3    # "value":Ljava/lang/Object;
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .restart local v3    # "value":Ljava/lang/Object;
    :cond_2
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Invalid value for PageCommand message"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 140
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Object;>;"
    .end local v3    # "value":Ljava/lang/Object;
    :cond_3
    invoke-super {p0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;->getPayload()Ljava/util/List;

    move-result-object v4

    return-object v4
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 145
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V

    .line 146
    return-void
.end method

.method public putFloat(IF)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # F

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method public putInt(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public putString(ILjava/lang/String;)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mValues:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 150
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "contentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mContentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "commandName"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->mCommandName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
