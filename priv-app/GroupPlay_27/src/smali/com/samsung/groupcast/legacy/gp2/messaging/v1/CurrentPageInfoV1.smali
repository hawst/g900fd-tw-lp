.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "CurrentPageInfoV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mAge:J

.field private final mCollectionName:Ljava/lang/String;

.field private final mContentId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1$1;

    const-string v1, "CURRENT_PAGE_INFO_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p3, "age"    # J

    .prologue
    .line 27
    const-string v0, "CURRENT_PAGE_INFO_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 28
    const-string v0, "contentId"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 29
    const-string v0, "collectionName"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mCollectionName:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mContentId:Ljava/lang/String;

    .line 32
    iput-wide p3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mAge:J

    .line 33
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->writeString(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0, p2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->writeString(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->writeLong(J)V

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v0, "CURRENT_PAGE_INFO_V1"

    invoke-direct {p0, v0, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 40
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mCollectionName:Ljava/lang/String;

    .line 41
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mContentId:Ljava/lang/String;

    .line 42
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mAge:J

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getAge()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mAge:J

    return-wide v0
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mContentId:Ljava/lang/String;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V

    .line 60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 64
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "contentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mContentId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "age"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->mAge:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
