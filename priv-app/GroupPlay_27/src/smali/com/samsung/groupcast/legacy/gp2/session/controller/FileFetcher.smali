.class public abstract Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
.super Ljava/lang/Object;
.source "FileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;
    }
.end annotation


# instance fields
.field private mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

.field private final mChannelId:Ljava/lang/String;

.field private final mChannelListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;

.field private mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

.field private mEnabled:Z

.field private final mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

.field private final mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/groupcast/misc/utility/MultiMap",
            "<",
            "Lcom/samsung/groupcast/core/messaging/DataKey;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V
    .locals 2
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p2, "channelId"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannelListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;

    .line 38
    new-instance v0, Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-direct {v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    .line 40
    new-instance v0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    invoke-direct {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    .line 43
    const-string v0, "messagingClient"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .line 45
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannelId:Ljava/lang/String;

    .line 46
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/requests/RequestScheduler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/utility/MultiMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/core/messaging/HybridChannel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "x4"    # J
    .param p6, "x5"    # J

    .prologue
    .line 31
    invoke-direct/range {p0 .. p7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V

    return-void
.end method

.method private onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
    .locals 8
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    .prologue
    .line 128
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v0

    .line 129
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    .line 131
    .local v3, "requests":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;>;"
    iget-boolean v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-nez v4, :cond_1

    .line 157
    :cond_0
    return-void

    .line 134
    :cond_1
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 138
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;->isLoading()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 140
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 145
    .local v2, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    const-wide/32 v5, 0x2255100

    new-instance v7, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;

    invoke-direct {v7, p0, v2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/DataKey;)V

    invoke-virtual {v4, v2, v5, v6, v7}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->startAttempt(Lcom/samsung/groupcast/misc/requests/Request;JLcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V

    goto :goto_0
.end method

.method private onFileReceiveFailureV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 9
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    const/4 v8, 0x0

    .line 292
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onFileReceiveFailureV1"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "offer"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p3, v5, v6

    invoke-static {v3, v4, v8, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 293
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v0

    .line 295
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 296
    .local v2, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    sget-object v3, Lcom/samsung/groupcast/misc/requests/Result;->FETCH_FAILURE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    invoke-virtual {v2, v3, v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    .end local v2    # "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    :cond_0
    return-void
.end method

.method private onFileReceiveProgressV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;JJ)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "offset"    # J
    .param p6, "size"    # J

    .prologue
    .line 201
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v0

    .line 203
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-boolean v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-nez v3, :cond_1

    .line 210
    :cond_0
    return-void

    .line 206
    :cond_1
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 208
    .local v2, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    invoke-static {p4, p5, p6, p7}, Lcom/samsung/groupcast/misc/requests/Progress;->downloading(JJ)Lcom/samsung/groupcast/misc/requests/Progress;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V

    goto :goto_0
.end method

.method private onFileReceiveSuccessV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;Ljava/lang/String;)V
    .locals 13
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;
    .param p4, "tempPath"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v1

    .line 216
    .local v1, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-boolean v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-nez v7, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v7, v1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 222
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "onFileReceiveSuccessV1"

    const/4 v9, 0x0

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "node"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object p2, v10, v11

    const/4 v11, 0x2

    const-string v12, "offer"

    aput-object v12, v10, v11

    const/4 v11, 0x3

    aput-object p3, v10, v11

    const/4 v11, 0x4

    const-string v12, "tempPath"

    aput-object v12, v10, v11

    const/4 v11, 0x5

    aput-object p4, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 225
    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v8, v1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v7}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 228
    .local v6, "requests":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v7}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 230
    .local v3, "destinationPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 232
    .local v5, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    invoke-virtual {v7, v5}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->cancelAttempts(Lcom/samsung/groupcast/misc/requests/Request;)V

    .line 235
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v7

    invoke-virtual {p0, v5, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onGetDestinationPath(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/FileInfo;)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "destinationPath":Ljava/lang/String;
    const-string v7, "destinationPath"

    invoke-static {v7, v2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 237
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 241
    .end local v2    # "destinationPath":Ljava/lang/String;
    .end local v5    # "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    :cond_2
    new-instance v7, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;

    move-object/from16 v0, p4

    invoke-direct {v7, p0, v6, v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$5;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    invoke-static {v7}, Lcom/samsung/groupcast/application/IOQueue;->post(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-nez v0, :cond_0

    .line 81
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "disable"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->cancelAllAttempts()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    .line 75
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannelListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseHybridChannel(Lcom/samsung/groupcast/core/messaging/HybridChannel;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseConnection(Lcom/samsung/groupcast/core/messaging/ConnectionHandle;)V

    .line 79
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    .line 80
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    goto :goto_0
.end method

.method public enable()V
    .locals 2

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 62
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "enable"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    .line 55
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireConnection(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 59
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireHybridChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannelListener:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    goto :goto_0
.end method

.method protected onFileOfferV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;)V
    .locals 10
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "offer"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;

    .prologue
    .line 160
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "onFileOfferV1"

    const/4 v6, 0x0

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "node"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    const-string v9, "offer"

    aput-object v9, v7, v8

    const/4 v8, 0x3

    aput-object p3, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 161
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileOfferV1;->getFileInfo()Lcom/samsung/groupcast/core/messaging/FileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/core/messaging/FileInfo;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v0

    .line 162
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->get(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v3

    .line 164
    .local v3, "requests":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;>;"
    iget-boolean v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z

    if-nez v4, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 173
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .line 177
    .local v2, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    const-wide/32 v5, 0x2255100

    new-instance v7, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$4;

    invoke-direct {v7, p0, v2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$4;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/DataKey;)V

    invoke-virtual {v4, v2, v5, v6, v7}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->startAttempt(Lcom/samsung/groupcast/misc/requests/Request;JLcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V

    .line 188
    invoke-static {}, Lcom/samsung/groupcast/misc/requests/Progress;->downloading()Lcom/samsung/groupcast/misc/requests/Progress;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V

    goto :goto_1

    .line 194
    .end local v2    # "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    :cond_2
    invoke-interface {p1, p3}, Lcom/samsung/groupcast/core/messaging/Channel;->acceptFileOffer(Lcom/samsung/groupcast/core/messaging/FileOffer;)V

    goto :goto_0
.end method

.method protected abstract onGetDestinationPath(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/FileInfo;)Ljava/lang/String;
.end method

.method protected startRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V
    .locals 7
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    .prologue
    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onStart"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->getDataKey()Lcom/samsung/groupcast/core/messaging/DataKey;

    move-result-object v6

    .line 88
    .local v6, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;

    invoke-virtual {v0, v6, p1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 90
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;

    invoke-direct {v0, p0, p1, v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/DataKey;)V

    invoke-virtual {p1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 104
    invoke-static {}, Lcom/samsung/groupcast/misc/requests/Progress;->searching()Lcom/samsung/groupcast/misc/requests/Progress;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->setProgress(Lcom/samsung/groupcast/misc/requests/Progress;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    const-wide/16 v2, 0xfa0

    const/4 v4, 0x3

    new-instance v5, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;

    invoke-direct {v5, p0, v6, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/DataKey;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->startAttempts(Lcom/samsung/groupcast/misc/requests/Request;JILcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V

    .line 124
    return-void
.end method
