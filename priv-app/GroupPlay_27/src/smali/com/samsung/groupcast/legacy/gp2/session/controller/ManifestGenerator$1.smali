.class Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;
.super Landroid/os/AsyncTask;
.source "ManifestGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->processTasks(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->val$uri:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/net/Uri;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    .locals 4
    .param p1, "params"    # [Landroid/net/Uri;

    .prologue
    .line 222
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const-string v3, "createContent"

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 223
    const/4 v2, 0x0

    aget-object v1, p1, v2

    .line 224
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->createContentItemForLocalContent(Landroid/content/ContentResolver;Landroid/net/Uri;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;

    move-result-object v0

    .line 226
    .local v0, "result":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, [Landroid/net/Uri;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->doInBackground([Landroid/net/Uri;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;)V
    .locals 11
    .param p1, "result"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v10, 0x1

    .line 231
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Landroid/os/AsyncTask;

    move-result-object v3

    if-eq v3, p0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    if-eqz p1, :cond_0

    .line 239
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;
    invoke-static {v3, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$102(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 241
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 245
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->getItem()Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v1

    .line 246
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 248
    .local v2, "path":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getType()I

    move-result v3

    if-eq v3, v10, :cond_2

    .line 249
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I
    invoke-static {v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I

    move-result v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->addItemAsPage(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 250
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # ++operator for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$704(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I

    .line 252
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # ++operator for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$404(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I

    .line 253
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onPostExecute"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I
    invoke-static {v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "uri"

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->val$uri:Landroid/net/Uri;

    aput-object v7, v6, v10

    const-string v7, "remaining"

    aput-object v7, v6, v9

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I
    invoke-static {v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I

    move-result v9

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 255
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->processTasks(Z)V
    invoke-static {v3, v10}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Z)V

    goto/16 :goto_0

    .line 259
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onPostExecute"

    const-string v5, "content is document, decomposing"

    new-array v6, v9, [Ljava/lang/Object;

    const-string v7, "uri"

    aput-object v7, v6, v8

    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->val$uri:Landroid/net/Uri;

    aput-object v7, v6, v10

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 261
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->registerItem(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    move-object v0, v1

    .line 262
    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;

    .line 299
    .local v0, "documentItem":Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;
    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 219
    check-cast p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;->onPostExecute(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;)V

    return-void
.end method
