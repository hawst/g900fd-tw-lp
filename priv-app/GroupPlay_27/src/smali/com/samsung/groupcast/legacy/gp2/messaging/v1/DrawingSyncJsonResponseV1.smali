.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "DrawingSyncJsonResponseV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private mJsonA:Lorg/json/JSONArray;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1$1;

    const-string v1, "DS_RSP_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v2, "DS_RSP_V1"

    invoke-direct {p0, v2, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 32
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;

    .line 36
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->readString()Ljava/lang/String;

    move-result-object v1

    .line 38
    .local v1, "jsonString":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONArray;)V
    .locals 1
    .param p1, "jsonA"    # Lorg/json/JSONArray;

    .prologue
    .line 26
    const-string v0, "DS_RSP_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;

    .line 27
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;

    .line 28
    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->writeString(Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getJson()Lorg/json/JSONArray;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V

    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "json:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;->mJsonA:Lorg/json/JSONArray;

    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
