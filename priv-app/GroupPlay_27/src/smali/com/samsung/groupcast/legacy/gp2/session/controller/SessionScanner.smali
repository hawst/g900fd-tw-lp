.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;
.super Ljava/lang/Object;
.source "SessionScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$LobbyChannelListener;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;
    }
.end annotation


# static fields
.field private static final INITIAL_SCAN_DELAY_MILLISECONDS:J = 0x64L

.field private static final SCAN_INTERVAL_MILLISECONDS:J = 0x5dcL

.field private static final SESSION_TTL_MILLISECONDS:J = 0xfa0L


# instance fields
.field private mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

.field private mEnabled:Z

.field private mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field private final mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

.field private final mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

.field private mPeriodicRunnable:Ljava/lang/Runnable;

.field private final mSessionSummaryRecords:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;)V
    .locals 2
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    .line 41
    const-string v0, "messagingClient"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .line 43
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$LobbyChannelListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$LobbyChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 44
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->onSessionSummaryRecordExpired(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;)V

    return-void
.end method

.method private onSessionSummaryRecordExpired(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;)V
    .locals 3
    .param p1, "record"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->getSessionSummary()Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v1

    .line 133
    .local v1, "summary":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "sessionId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    if-eqz v2, :cond_0

    .line 137
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    invoke-interface {v2, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;->onDiscoveredSessionsChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V

    .line 139
    :cond_0
    return-void
.end method

.method private startPeriodicSessionSummaryRequest()V
    .locals 3

    .prologue
    .line 102
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    .line 119
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x64

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 120
    return-void
.end method

.method private stopPeriodicSessionSummaryRequest()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mPeriodicRunnable:Ljava/lang/Runnable;

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 67
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mEnabled:Z

    if-nez v2, :cond_1

    .line 89
    :cond_0
    return-void

    .line 71
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mEnabled:Z

    .line 72
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    if-eqz v2, :cond_2

    .line 73
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 76
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    if-eqz v2, :cond_3

    .line 77
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 78
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseConnection(Lcom/samsung/groupcast/core/messaging/ConnectionHandle;)V

    .line 81
    :cond_3
    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 82
    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 83
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->stopPeriodicSessionSummaryRequest()V

    .line 84
    invoke-virtual {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;)V

    .line 86
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .line 87
    .local v1, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->stopExpiryTrigger()V

    goto :goto_0
.end method

.method public enable()V
    .locals 4

    .prologue
    .line 51
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mEnabled:Z

    if-eqz v2, :cond_1

    .line 64
    :cond_0
    return-void

    .line 55
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mEnabled:Z

    .line 56
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireConnection(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 57
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    const-string v3, "com.samsung.groupcast.Lobby_V1"

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 58
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 59
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->startPeriodicSessionSummaryRequest()V

    .line 61
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .line 62
    .local v1, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->startExpiryTrigger()V

    goto :goto_0
.end method

.method public getDiscoveredSessions()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v2, "sessions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .line 95
    .local v1, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->getSessionSummary()Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    .end local v1    # "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    :cond_0
    return-object v2
.end method

.method public onSessionSummaryInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;)V
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;

    .prologue
    .line 142
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;->getSummary()Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    move-result-object v2

    .line 143
    .local v2, "summary":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "sessionId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .line 146
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    if-nez v0, :cond_1

    .line 147
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;

    .end local v0    # "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    invoke-direct {v0, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V

    .line 148
    .restart local v0    # "record":Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mSessionSummaryRecords:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :goto_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    if-eqz v3, :cond_0

    .line 154
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    invoke-interface {v3, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;->onDiscoveredSessionsChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V

    .line 156
    :cond_0
    return-void

    .line 150
    :cond_1
    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->update(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionScannerDelegate;

    .line 48
    return-void
.end method
