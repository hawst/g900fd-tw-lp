.class Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;
.source "ContentBroker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->createContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onInvalidated(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 3
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 145
    move-object v0, p1

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 146
    .local v0, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->cleanupContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    invoke-static {v1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    .line 147
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->mContentRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;)Lcom/samsung/groupcast/misc/utility/MultiMap;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method

.method public onPriorityChanged(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 2
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 152
    move-object v0, p1

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;

    .line 153
    .local v0, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->reprioritizeContentRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V
    invoke-static {v1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentBroker;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentRequest;)V

    .line 154
    return-void
.end method
