.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.super Ljava/lang/Object;
.source "ContentItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_ID:Ljava/lang/String; = "ID"

.field private static final KEY_TITLE:Ljava/lang/String; = "TITLE"

.field public static final KEY_TYPE:Ljava/lang/String; = "TYPE"


# instance fields
.field private final mId:Ljava/lang/String;

.field protected mObject:Ljava/lang/Object;

.field private final mTitle:Ljava/lang/String;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$1;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$1;-><init>(I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 114
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$2;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$2;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mObject:Ljava/lang/Object;

    .line 55
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    .line 56
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;

    .line 58
    return-void
.end method

.method protected constructor <init>(ILorg/json/JSONObject;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mObject:Ljava/lang/Object;

    .line 61
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    .line 64
    :try_start_0
    const-string v1, "ID"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    .line 65
    const-string v1, "TITLE"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    return-void

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid json used to create ContentItem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 95
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 96
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "TYPE"

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 97
    const-string v2, "ID"

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    const-string v2, "TITLE"

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    return-object v1

    .line 100
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception encountered while getting Json representation - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getPrivateObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    return v0
.end method

.method public setPrivateObject(Ljava/lang/Object;)V
    .locals 0
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mObject:Ljava/lang/Object;

    .line 145
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 112
    return-void
.end method
