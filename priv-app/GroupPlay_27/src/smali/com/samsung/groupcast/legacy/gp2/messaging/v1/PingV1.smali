.class public Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;
.super Lcom/samsung/groupcast/core/messaging/BaseMessage;
.source "PingV1.java"


# static fields
.field public static final MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;


# instance fields
.field private final mId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 12
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1$1;

    const-string v1, "PING_V1"

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1$1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->MESSAGE_CREATOR:Lcom/samsung/groupcast/core/messaging/MessageCreator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/Integer;

    .prologue
    .line 22
    const-string v0, "PING_V1"

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->mId:Ljava/lang/Integer;

    .line 24
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->mId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->writeInt(I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "payload":Ljava/util/List;, "Ljava/util/List<[B>;"
    const-string v0, "PING_V1"

    invoke-direct {p0, v0, p1}, Lcom/samsung/groupcast/core/messaging/BaseMessage;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 29
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->mId:Ljava/lang/Integer;

    .line 30
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;->mId:Ljava/lang/Integer;

    return-object v0
.end method

.method public notifyListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .param p2, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p3, "node"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-interface {p1, p2, p3, p0}, Lcom/samsung/groupcast/core/messaging/ChannelListener;->onPingV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;)V

    .line 39
    return-void
.end method
