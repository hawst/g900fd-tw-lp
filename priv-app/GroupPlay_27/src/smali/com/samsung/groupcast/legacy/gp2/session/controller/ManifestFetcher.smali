.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
.source "ManifestFetcher.java"


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V
    .locals 0
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p2, "channelId"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V

    .line 12
    return-void
.end method


# virtual methods
.method public createFetchManifestRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;
    .locals 2
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "manifestId"    # Ljava/lang/String;

    .prologue
    .line 15
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;)V

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V

    return-object v0
.end method

.method protected onGetDestinationPath(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/FileInfo;)Ljava/lang/String;
    .locals 4
    .param p1, "r"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "info"    # Lcom/samsung/groupcast/core/messaging/FileInfo;

    .prologue
    .line 26
    move-object v2, p1

    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    .line 27
    .local v2, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "sessionId":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "manifestId":Ljava/lang/String;
    invoke-static {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    .local v1, "path":Ljava/lang/String;
    return-object v1
.end method
