.class public Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
.super Ljava/lang/Object;
.source "PageSchemaNode.java"


# static fields
.field public static final TYPE_ARRAY_DOUBLE:I = 0x8

.field public static final TYPE_ARRAY_INT:I = 0x6

.field public static final TYPE_ARRAY_LONG:I = 0x7

.field public static final TYPE_ARRAY_STRING:I = 0x5

.field public static final TYPE_DOUBLE:I = 0x4

.field public static final TYPE_INT:I = 0x2

.field public static final TYPE_LONG:I = 0x3

.field public static final TYPE_NODE:I = 0x0

.field public static final TYPE_STRING:I = 0x1


# instance fields
.field private final mNodeByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;",
            ">;"
        }
    .end annotation
.end field

.field private final mPropertyByName:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mNodeByName:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public createDoubleArrayProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/16 v1, 0x8

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 95
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method public createDoubleProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x4

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 71
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method public createIntArrayProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 82
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x6

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 83
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method

.method public createIntProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 59
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method

.method public createLongArrayProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x7

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 89
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    return-void
.end method

.method public createLongProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 65
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method

.method public createNode(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 37
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a node already exists for name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "a property already exists for name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;-><init>()V

    .line 46
    .local v0, "node":Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mNodeByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-object v0
.end method

.method public createStringArrayProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 75
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 76
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x5

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 77
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public createStringProperty(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-string v1, "name"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;-><init>(ILjava/lang/String;)V

    .line 53
    .local v0, "record":Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public getNodes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mNodeByName:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageProperty;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "properties"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mPropertyByName:Ljava/util/HashMap;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "nodes"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/PageSchemaNode;->mNodeByName:Ljava/util/HashMap;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
