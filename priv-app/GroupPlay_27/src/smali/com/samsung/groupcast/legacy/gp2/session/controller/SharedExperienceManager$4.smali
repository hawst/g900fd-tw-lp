.class Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;
.super Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestListener;
.source "SharedExperienceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->clearCollectionFromManifest(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0

    .prologue
    .line 754
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;)V
    .locals 5
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;

    .prologue
    .line 757
    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-ne v1, p2, :cond_1

    .line 758
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    .line 759
    .local v0, "previousManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 761
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 762
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 765
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1000(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 766
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1000(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    invoke-static {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 770
    .end local v0    # "previousManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    :cond_1
    return-void
.end method
