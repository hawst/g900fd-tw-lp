.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;
.super Ljava/lang/Object;
.source "ContentItemFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreateContentItemResult"
.end annotation


# instance fields
.field private final mItem:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

.field private final mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;Ljava/lang/String;)V
    .locals 0
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mItem:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 34
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mPath:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public getItem()Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mItem:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "item"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mItem:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "path"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;->mPath:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
