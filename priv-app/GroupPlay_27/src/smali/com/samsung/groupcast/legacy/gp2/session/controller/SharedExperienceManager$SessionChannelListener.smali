.class Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;
.super Lcom/samsung/groupcast/core/messaging/BaseChannelListener;
.source "SharedExperienceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionChannelListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;


# direct methods
.method private constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0

    .prologue
    .line 1090
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-direct {p0}, Lcom/samsung/groupcast/core/messaging/BaseChannelListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p2, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;

    .prologue
    .line 1090
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    return-void
.end method


# virtual methods
.method protected onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "contentId"    # Ljava/lang/String;

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    .line 1142
    return-void
.end method

.method public onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1500(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V

    .line 1115
    return-void
.end method

.method public onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V

    .line 1110
    return-void
.end method

.method public onDrawingSyncJSONResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onDrawingSyncJsonResponseV1Real(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V

    .line 1161
    return-void
.end method

.method public onDrawingSyncJsonRequest(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onDrawingSyncJsonRequestReal(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V

    .line 1149
    return-void
.end method

.method public onDrawingSyncResponseV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onDrawingSyncResponseV1Real(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V

    .line 1155
    return-void
.end method

.method public onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1600(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V

    .line 1125
    return-void
.end method

.method protected onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "manifestId"    # Ljava/lang/String;

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1400(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    .line 1136
    return-void
.end method

.method public onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;

    .prologue
    .line 1119
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V

    .line 1120
    return-void
.end method

.method public onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    # invokes: Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->access$1700(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V

    .line 1130
    return-void
.end method

.method public onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    .line 1104
    return-void
.end method

.method public onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    .line 1094
    return-void
.end method

.method public onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    .line 1099
    return-void
.end method
