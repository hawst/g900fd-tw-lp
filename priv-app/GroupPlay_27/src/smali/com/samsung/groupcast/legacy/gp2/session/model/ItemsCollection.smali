.class public Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
.super Ljava/lang/Object;
.source "ItemsCollection.java"


# static fields
.field private static final ADD_LAST_PAGE:I = -0x1

.field private static final ADD_NO_PAGE:I = -0x2

.field private static final KEY_COLLECTION_ID:Ljava/lang/String; = "ID"

.field private static final KEY_COLLECTION_ITEMS:Ljava/lang/String; = "ITEMS"

.field private static final KEY_COLLECTION_NAME:Ljava/lang/String; = "NAME"

.field private static final KEY_COLLECTION_PAGE_CONTENT_IDS:Ljava/lang/String; = "PAGE_CONTENT_IDS"


# instance fields
.field private final mCollagePageItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mCollectionId:Ljava/lang/String;

.field private final mCollectionName:Ljava/lang/String;

.field private final mDocumentItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mExtraPageItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mImageItems:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsLocked:Z

.field private final mItemByContentId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageContentIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPageIndexByContentId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;)V
    .locals 2
    .param p1, "collection"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 58
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    .line 63
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    .line 65
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    .line 66
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 46
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    .line 52
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p2, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;>;"
    .local p3, "pageContentIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 70
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    .line 71
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    .line 72
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    .line 73
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    .line 74
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    .line 75
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    .line 76
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    .line 77
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    .line 78
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    .line 80
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 83
    .local v4, "pageCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 84
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    .local v0, "contentId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 89
    .end local v0    # "contentId":Ljava/lang/String;
    :cond_0
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 90
    .local v3, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-direct {p0, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->itemAddByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    goto :goto_1

    .line 92
    .end local v3    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 13
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-boolean v12, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 95
    const-string v7, "ID"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    .line 96
    const-string v7, "NAME"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    .line 98
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    .line 99
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    .line 100
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    .line 101
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    .line 102
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    .line 103
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    .line 104
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    .line 107
    const-string v7, "ITEMS"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 108
    .local v5, "itemsJson":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 109
    invoke-virtual {v5, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 110
    .local v4, "itemJson":Lorg/json/JSONObject;
    invoke-static {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory;->createContentItem(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v3

    .line 111
    .local v3, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    if-nez v3, :cond_0

    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v8, "ItemsCollection"

    const-string v9, "could not parse Json into item, ignoring"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const-string v11, "item"

    aput-object v11, v10, v12

    const/4 v11, 0x1

    aput-object v3, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 108
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "contentId":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v7, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 121
    .end local v0    # "contentId":Ljava/lang/String;
    .end local v3    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .end local v4    # "itemJson":Lorg/json/JSONObject;
    :cond_1
    const-string v7, "PAGE_CONTENT_IDS"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 122
    .local v6, "pagesJson":Lorg/json/JSONArray;
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_2

    .line 123
    invoke-virtual {v6, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    .restart local v0    # "contentId":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 129
    .end local v0    # "contentId":Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 130
    .restart local v3    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-direct {p0, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->itemAddByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    goto :goto_3

    .line 132
    .end local v3    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    :cond_3
    return-void
.end method

.method private itemAddByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 296
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 310
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 313
    :goto_0
    return-void

    .line 298
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 306
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 296
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method private itemRemoveByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 317
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 331
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 334
    :goto_0
    return-void

    .line 319
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 323
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 327
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 317
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public addItem(ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 5
    .param p1, "itemIndex"    # I
    .param p2, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 239
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    if-eqz v2, :cond_0

    .line 240
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    throw v2

    .line 243
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "contentId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ne v2, p1, :cond_1

    .line 246
    const/4 p1, -0x1

    .line 248
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 255
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 259
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 260
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 261
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 252
    .end local v1    # "i":I
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    .restart local v1    # "i":I
    :cond_2
    invoke-direct {p0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->itemAddByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 265
    return-void

    .line 248
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public addItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    if-eqz v0, :cond_0

    .line 232
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 235
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->addItem(ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 236
    return-void
.end method

.method public getCollagePageItems()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollagePageItems:Ljava/util/HashSet;

    return-object v0
.end method

.method public getDocumentItems()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mDocumentItems:Ljava/util/HashSet;

    return-object v0
.end method

.method public getExtraPageItems()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mExtraPageItems:Ljava/util/HashSet;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    return-object v0
.end method

.method public getImageItems()Ljava/util/HashSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mImageItems:Ljava/util/HashSet;

    return-object v0
.end method

.method public getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 184
    if-ltz p1, :cond_0

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v2, p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v1

    .line 187
    :cond_1
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 188
    .local v0, "contentId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 191
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    goto :goto_0
.end method

.method public getItem(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    return-object v0
.end method

.method public getItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 137
    .local v4, "json":Lorg/json/JSONObject;
    const-string v6, "ID"

    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionId:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 138
    const-string v6, "NAME"

    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 141
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 142
    .local v3, "itemsJson":Lorg/json/JSONArray;
    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .line 143
    .local v1, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v2

    .line 144
    .local v2, "itemJson":Lorg/json/JSONObject;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 146
    .end local v1    # "item":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .end local v2    # "itemJson":Lorg/json/JSONObject;
    :cond_0
    const-string v6, "ITEMS"

    invoke-virtual {v4, v6, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 149
    new-instance v5, Lorg/json/JSONArray;

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 150
    .local v5, "pageContentIdsJson":Lorg/json/JSONArray;
    const-string v6, "PAGE_CONTENT_IDS"

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 152
    return-object v4
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getPageContentId(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getPageIndex(Ljava/lang/String;)I
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 203
    const-string v1, "contentId"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 204
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 205
    .local v0, "result":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 206
    const/4 v1, -0x1

    .line 208
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public lockCollection()V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 217
    return-void
.end method

.method public registerItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    if-eqz v0, :cond_0

    .line 225
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 227
    :cond_0
    const/4 v0, -0x2

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->addItem(ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 228
    return-void
.end method

.method public removeItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 276
    iget-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    if-eqz v2, :cond_0

    .line 277
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2}, Ljava/lang/RuntimeException;-><init>()V

    throw v2

    .line 280
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 281
    .local v0, "contentId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mItemByContentId:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 287
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 288
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 289
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageIndexByContentId:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mPageContentIds:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 292
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->itemRemoveByType(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 293
    return-void
.end method

.method public unlockCollection()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    .line 221
    return-void
.end method

.method public unregisterItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->mIsLocked:Z

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 271
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->removeItem(Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 272
    return-void
.end method
