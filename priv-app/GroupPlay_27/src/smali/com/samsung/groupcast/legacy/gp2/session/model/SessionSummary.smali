.class public Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
.super Ljava/lang/Object;
.source "SessionSummary.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static final NO_PAGES_TITLE:Ljava/lang/String; = "NO_PAGES"

.field private static final SESSION_SUMMARY_LENGTH:I = 0xb

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final mAudioCount:I

.field private final mDocumentCount:I

.field private final mFirstPageTitle:Ljava/lang/String;

.field private final mImageCount:I

.field private final mManifestId:Ljava/lang/String;

.field private final mManifestTimestamp:J

.field private final mPinHash:Ljava/lang/String;

.field private final mSessionId:Ljava/lang/String;

.field private final mSourceName:Ljava/lang/String;

.field private final mTitle:Ljava/lang/String;

.field private final mVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary$1;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary$1;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 0
    .param p1, "version"    # I
    .param p2, "sessionId"    # Ljava/lang/String;
    .param p3, "manifestId"    # Ljava/lang/String;
    .param p4, "manifestTimestamp"    # J
    .param p6, "pinHash"    # Ljava/lang/String;
    .param p7, "sourceName"    # Ljava/lang/String;
    .param p8, "title"    # Ljava/lang/String;
    .param p9, "firstPageTitle"    # Ljava/lang/String;
    .param p10, "documentCount"    # I
    .param p11, "imageCount"    # I
    .param p12, "audioCount"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    .line 55
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    .line 57
    iput-wide p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    .line 58
    iput-object p6, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    .line 59
    iput-object p7, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    .line 60
    iput-object p8, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    .line 61
    iput p10, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    .line 62
    iput p11, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    .line 63
    iput p12, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    .line 64
    iput-object p9, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V
    .locals 4
    .param p1, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "session"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 37
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getVersion()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    .line 38
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    .line 40
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getAge()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    .line 41
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPinHash()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSourceName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getDocumentCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    .line 45
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getImageCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    .line 47
    const-string v0, "NO_PAGES"

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static BandStringtoSessionSummary(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 14
    .param p0, "band_string"    # Ljava/lang/String;

    .prologue
    .line 206
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 208
    .local v13, "band_stirng_splited":[Ljava/lang/String;
    array-length v0, v13

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    const/4 v1, 0x0

    aget-object v1, v13, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    aget-object v2, v13, v2

    const/4 v3, 0x2

    aget-object v3, v13, v3

    const/4 v4, 0x3

    aget-object v4, v13, v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v6, 0x4

    aget-object v6, v13, v6

    const/4 v7, 0x5

    aget-object v7, v13, v7

    const/4 v8, 0x6

    aget-object v8, v13, v8

    const/4 v9, 0x7

    aget-object v9, v13, v9

    const/16 v10, 0x8

    aget-object v10, v13, v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    const/16 v11, 0x9

    aget-object v11, v13, v11

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    const/16 v12, 0xa

    aget-object v12, v13, v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-direct/range {v0 .. v12}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public getAudioCount()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    return v0
.end method

.method public getDocumentCount()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    return v0
.end method

.method public getFirstPageTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getImageCount()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    return v0
.end method

.method public getManifestAge()J
    .locals 4

    .prologue
    .line 84
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getManifestId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    return-object v0
.end method

.method public getManifestTimestamp()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    return-wide v0
.end method

.method public getPinHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinProtected()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinValid(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->isPinProtected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    const/4 v1, 0x0

    .line 97
    :goto_0
    return v1

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPinHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "pinHash":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public toBandString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    iget-wide v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x14

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "version"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "sessionId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "manifestId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "manifestAge"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->getManifestAge()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "sourceName"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "title"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "firstPageTitle"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "documentCount"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "imageCount"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "audioCount"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 134
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 135
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSessionId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mManifestTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mPinHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mSourceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mFirstPageTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mDocumentCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mImageCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;->mAudioCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 145
    return-void
.end method
