.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;
.source "ContentFetcher.java"


# instance fields
.field private final mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V
    .locals 1
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .prologue
    .line 17
    invoke-static {p2}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelIdForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V

    .line 18
    const-string v0, "session"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 19
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 20
    return-void
.end method


# virtual methods
.method public createFetchContentRequest(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    .locals 2
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 23
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;)V

    invoke-direct {v0, p1, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;-><init>(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Starter;)V

    return-object v0
.end method

.method protected onGetDestinationPath(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/FileInfo;)Ljava/lang/String;
    .locals 7
    .param p1, "r"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "info"    # Lcom/samsung/groupcast/core/messaging/FileInfo;

    .prologue
    .line 34
    move-object v4, p1

    check-cast v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;

    .line 35
    .local v4, "request":Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;
    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentFetcher;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v5

    .line 36
    .local v5, "sessionId":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchContentRequest;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "contentId":Ljava/lang/String;
    const/4 v6, 0x2

    invoke-virtual {p2, v6}, Lcom/samsung/groupcast/core/messaging/FileInfo;->get(I)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "filename":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/FileTools;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "extension":Ljava/lang/String;
    invoke-static {v5, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionContentFilePath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 41
    .local v3, "path":Ljava/lang/String;
    return-object v3
.end method
