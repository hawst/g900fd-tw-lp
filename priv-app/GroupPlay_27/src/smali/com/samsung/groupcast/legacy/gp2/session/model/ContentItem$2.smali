.class final Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$2;
.super Ljava/lang/Object;
.source "ContentItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 118
    .local v2, "type":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "title":Ljava/lang/String;
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    invoke-direct {v3, v2, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$2;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 125
    new-array v0, p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem$2;->newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v0

    return-object v0
.end method
