.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;
.super Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;
.source "FileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->onFileAvailabilityInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

.field final synthetic val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

.field final synthetic val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/DataKey;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-direct {p0}, Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeout(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 3
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onTimeout"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/utility/MultiMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
