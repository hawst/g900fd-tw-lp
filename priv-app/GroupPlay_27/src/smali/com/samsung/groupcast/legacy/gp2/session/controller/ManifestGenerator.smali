.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
.super Ljava/lang/Object;
.source "ManifestGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
    }
.end annotation


# instance fields
.field private final mCollectionName:Ljava/lang/String;

.field private mConvertTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;",
            "Ljava/lang/Void;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;",
            ">;"
        }
    .end annotation
.end field

.field private mCreateContentItemTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Void;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemFactory$CreateContentItemResult;",
            ">;"
        }
    .end annotation
.end field

.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

.field private mInsertionIndex:I

.field private mInternalInsertionIndex:I

.field private final mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

.field private final mMappedPaths:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mResolver:Landroid/content/ContentResolver;

.field private mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field private mStarted:Z

.field protected final mTaskType:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

.field private mUriIndex:I

.field private final mUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;

    .line 102
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResolver:Landroid/content/ContentResolver;

    .line 103
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    .line 104
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInsertionIndex:I

    .line 106
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;

    .line 107
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;->TASK_TYPE_ADD:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mTaskType:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    .line 108
    return-void
.end method

.method private constructor <init>(Landroid/content/ContentResolver;Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;ILjava/lang/String;)V
    .locals 1
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p3, "existingManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p4, "insertionIndex"    # I
    .param p5, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;

    .line 82
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResolver:Landroid/content/ContentResolver;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    .line 84
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    invoke-direct {v0, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .line 85
    iput p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInsertionIndex:I

    .line 86
    iput-object p5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;

    .line 87
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;->TASK_TYPE_ADD:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mTaskType:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    .line 88
    return-void
.end method

.method private constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;)V
    .locals 2
    .param p1, "existingManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p2, "collectionName"    # Ljava/lang/String;
    .param p3, "CurrentIndex"    # I
    .param p4, "taskType"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;

    .line 92
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResolver:Landroid/content/ContentResolver;

    .line 93
    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .line 95
    iput p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInsertionIndex:I

    .line 96
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;

    .line 97
    iput-object p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mTaskType:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    .line 98
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Landroid/os/AsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$102(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I

    return v0
.end method

.method static synthetic access$404(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    return v0
.end method

.method static synthetic access$704(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->processTasks(Z)V

    return-void
.end method

.method private convertManifestBuilderToManifest()V
    .locals 5

    .prologue
    .line 306
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    .line 329
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 330
    return-void
.end method

.method public static manifestGeneratorForDelete(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Ljava/lang/String;I)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 2
    .param p0, "existingManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "iCurrentPage"    # I

    .prologue
    .line 73
    const-string v0, "existingManifest"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 75
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;->TASK_TYPE_REMOVE:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;)V

    return-object v0
.end method

.method public static manifestGeneratorForFreshManifest(Landroid/content/ContentResolver;Ljava/util/List;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    const-string v0, "resolver"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 52
    const-string v0, "uris"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 53
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "uris cannot be empty"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->isFalse(ZLjava/lang/String;)V

    .line 54
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;-><init>(Landroid/content/ContentResolver;Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;ILjava/lang/String;)V

    return-object v0
.end method

.method public static manifestGeneratorForInsertingPages(Landroid/content/ContentResolver;Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;ILjava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
    .locals 6
    .param p0, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "existingManifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .param p3, "insertionIndex"    # I
    .param p4, "collectionName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;",
            "I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    const-string v0, "resolver"

    invoke-static {v0, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 60
    const-string v0, "uris"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "uris cannot be empty"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->isFalse(ZLjava/lang/String;)V

    .line 62
    const-string v0, "existingManifest"

    invoke-static {v0, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 63
    invoke-virtual {p2, p4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p2, p4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v0

    if-gt p3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insertionIndex ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is out of bounds"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/Verify;->isTrue(ZLjava/lang/String;)V

    .line 67
    :cond_0
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;-><init>(Landroid/content/ContentResolver;Ljava/util/List;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;ILjava/lang/String;)V

    return-object v0

    .line 64
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processTasks(Z)V
    .locals 5
    .param p1, "reportProgress"    # Z

    .prologue
    .line 190
    iget-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    if-nez v1, :cond_0

    .line 191
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "processTasks"

    const-string v3, "cancelled, returning"

    invoke-static {v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 303
    :goto_0
    return-void

    .line 195
    :cond_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    if-eqz v1, :cond_1

    .line 196
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    invoke-interface {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;->onManifestGenerationProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 199
    :cond_1
    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 200
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "processTasks"

    const-string v3, "all Uris processed"

    invoke-static {v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 213
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->convertManifestBuilderToManifest()V

    goto :goto_0

    .line 217
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 219
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;

    invoke-direct {v1, p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;Landroid/net/Uri;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    .line 302
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/net/Uri;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getInsertionIndex()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInsertionIndex:I

    return v0
.end method

.method public getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object v0
.end method

.method public getMappedPaths()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mMappedPaths:Ljava/util/HashMap;

    return-object v0
.end method

.method public getProgressCount()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    return v0
.end method

.method public getProgressRatio()F
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 139
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUriIndex:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 141
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    .line 116
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "start"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    .line 153
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInsertionIndex:I

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mInternalInsertionIndex:I

    .line 154
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;->TASK_TYPE_REMOVE:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mTaskType:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$MANIFEST_GEN_TASK_TYPE;

    if-ne v0, v1, :cond_1

    .line 155
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->convertManifestBuilderToManifest()V

    goto :goto_0

    .line 157
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->processTasks(Z)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 162
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "stop"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 170
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 175
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    .line 178
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;->mStarted:Z

    goto :goto_0
.end method
