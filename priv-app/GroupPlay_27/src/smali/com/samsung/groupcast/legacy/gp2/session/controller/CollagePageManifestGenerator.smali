.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;
.source "CollagePageManifestGenerator.java"


# static fields
.field public static MAX_IMAGES_PER_COLLAGE_PAGE:I


# instance fields
.field private final mCollectionName:Ljava/lang/String;

.field private mConvertTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;",
            "Ljava/lang/Void;",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;",
            ">;"
        }
    .end annotation
.end field

.field mCreateContentItemTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentIdx:I

.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

.field private final mInsertionIndex:I

.field private mInternalInsertionIndex:I

.field private final mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

.field private mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

.field private final mSharedExperienceActivity:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

.field private final mShowProgress:Z

.field private final mSourceCollectionName:Ljava/lang/String;

.field private final mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

.field private mStarted:Z

.field private final mTotal:I

.field private final mbClear:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x6

    sput v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->MAX_IMAGES_PER_COLLAGE_PAGE:I

    return-void
.end method

.method private constructor <init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 2
    .param p1, "se"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .param p2, "srcCollection"    # Ljava/lang/String;
    .param p3, "collection"    # Ljava/lang/String;
    .param p4, "bClean"    # Z
    .param p5, "bShowProgress"    # Z
    .param p6, "bClear"    # Z

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;-><init>()V

    .line 23
    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I

    .line 71
    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInsertionIndex:I

    .line 72
    iput-boolean p5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mShowProgress:Z

    .line 73
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSourceCollectionName:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCollectionName:Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSharedExperienceActivity:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    .line 76
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSharedExperienceActivity:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    .line 77
    if-eqz p4, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    invoke-virtual {v0, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->removeCollection(Ljava/lang/String;)V

    .line 80
    :cond_0
    iput-boolean p6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mbClear:Z

    .line 81
    invoke-virtual {p1}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSourceCollectionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    .line 82
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I

    .line 83
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    .line 84
    return-void
.end method

.method public static AddCollageItemToManifestBuilder(Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "manifestBuilder"    # Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "srcCollectionName"    # Ljava/lang/String;

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "iCurrentIdx":I
    const/4 v1, 0x0

    .line 181
    .local v1, "iCurrentPageIdx":I
    const/4 v4, 0x0

    .line 183
    .local v4, "iTotal":I
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->removeCollection(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v6

    .line 186
    .local v6, "srcCollection":Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    if-nez v6, :cond_1

    .line 213
    :cond_0
    return-void

    .line 189
    :cond_1
    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v4

    .line 191
    :goto_0
    if-eq v0, v4, :cond_0

    .line 193
    move v3, v0

    .line 194
    .local v3, "iStart":I
    sget v8, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->MAX_IMAGES_PER_COLLAGE_PAGE:I

    add-int v2, v3, v8

    .line 195
    .local v2, "iEnd":I
    if-le v2, v4, :cond_2

    move v2, v4

    .line 197
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v7, "srcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    if-ge v3, v2, :cond_3

    .line 200
    invoke-virtual {v6, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    add-int/lit8 v0, v0, 0x1

    .line 202
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 205
    :cond_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    const/4 v8, 0x2

    if-le v2, v8, :cond_4

    .line 206
    add-int/lit8 v8, v2, -0x2

    invoke-virtual {v6, v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_4
    new-instance v5, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    invoke-direct {v5, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;-><init>(Ljava/util/ArrayList;)V

    .line 210
    .local v5, "item":Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
    invoke-virtual {p0, p1, v1, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->addItemAsPage(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;)V

    .line 211
    add-int/lit8 v1, v1, 0x1

    .line 212
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    return v0
.end method

.method static synthetic access$008(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSrcList:Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I

    return v0
.end method

.method static synthetic access$404(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mShowProgress:Z

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->processTasks(Z)V

    return-void
.end method

.method static synthetic access$802(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$902(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object p1
.end method

.method public static collagePageManifestGeneratorAsCleared(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
    .locals 7
    .param p0, "se"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .param p1, "srcCollection"    # Ljava/lang/String;
    .param p2, "collection"    # Ljava/lang/String;
    .param p3, "bShowProgress"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 54
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0
.end method

.method public static collagePageManifestGeneratorAsNew(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;
    .locals 7
    .param p0, "se"    # Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .param p1, "srcCollection"    # Ljava/lang/String;
    .param p2, "collection"    # Ljava/lang/String;
    .param p3, "bShowProgress"    # Z

    .prologue
    const/4 v0, 0x0

    .line 38
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 47
    :cond_0
    :goto_0
    return-object v0

    .line 42
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 47
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;

    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;-><init>(Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0
.end method

.method private convertManifestBuilderToManifest()V
    .locals 5

    .prologue
    .line 269
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    .line 292
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mManifestBuilder:Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 293
    return-void
.end method

.method private processTasks(Z)V
    .locals 3
    .param p1, "reportProgress"    # Z

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    if-nez v0, :cond_0

    .line 217
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "processTasks"

    const-string v2, "cancelled, returning"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 266
    :goto_0
    return-void

    .line 221
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    invoke-interface {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;->onManifestGenerationProgress(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator;)V

    .line 225
    :cond_1
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I

    if-ne v0, v1, :cond_2

    .line 226
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "processTasks"

    const-string v2, "all Uris processed"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 227
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->convertManifestBuilderToManifest()V

    goto :goto_0

    .line 231
    :cond_2
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    .line 265
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public getActivity()Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSharedExperienceActivity:Lcom/samsung/groupcast/application/viewer/SharedExperienceActivity;

    return-object v0
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getInsertionIndex()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInsertionIndex:I

    return v0
.end method

.method public getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    return-object v0
.end method

.method public getProgressCount()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    return v0
.end method

.method public getProgressRatio()F
    .locals 2

    .prologue
    .line 122
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCurrentIdx:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public getSourceCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mSourceCollectionName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mTotal:I

    return v0
.end method

.method public isCompleted()Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mResultManifest:Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;)V
    .locals 0
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestGenerator$ManifestGeneratorDelegate;

    .line 98
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "start"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    .line 144
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInsertionIndex:I

    iput v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mInternalInsertionIndex:I

    .line 146
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mbClear:Z

    if-eqz v0, :cond_1

    .line 147
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "starts"

    const-string v2, "clear~"

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    .line 148
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->convertManifestBuilderToManifest()V

    goto :goto_0

    .line 150
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mShowProgress:Z

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->processTasks(Z)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 156
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    if-nez v0, :cond_0

    .line 173
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "stop"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 164
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mCreateContentItemTask:Landroid/os/AsyncTask;

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 169
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mConvertTask:Landroid/os/AsyncTask;

    .line 172
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/CollagePageManifestGenerator;->mStarted:Z

    goto :goto_0
.end method
