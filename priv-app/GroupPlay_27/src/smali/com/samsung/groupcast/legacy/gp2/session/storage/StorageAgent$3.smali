.class final Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;
.super Ljava/lang/Object;
.source "StorageAgent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->handleStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 52
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-virtual {v11}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v5

    .line 53
    .local v5, "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v7

    .line 54
    .local v7, "manifestJson":Lorg/json/JSONObject;
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "jsonString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    invoke-virtual {v11}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v9

    .line 56
    .local v9, "sessionId":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v6

    .line 57
    .local v6, "manifestId":Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->createSessionManifestDirectory(Ljava/lang/String;)Ljava/io/File;

    .line 58
    invoke-static {v9, v6}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 60
    .local v8, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 61
    .local v1, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 63
    .local v10, "wasError":Ljava/lang/Boolean;
    :try_start_0
    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/high16 v12, 0x20000

    invoke-direct {v2, v11, v12}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .local v2, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 75
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 80
    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :goto_0
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 87
    :goto_1
    return-void

    .line 76
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catch_0
    move-exception v3

    .line 77
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    move-object v1, v2

    .line 79
    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_0

    .line 67
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 68
    .restart local v3    # "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-class v11, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v12, "handleStoreManifestRequest"

    const-string v13, "exception while writing file"

    const/4 v14, 0x4

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "path"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    const/4 v15, 0x2

    const-string v16, "exception"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 71
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    sget-object v12, Lcom/samsung/groupcast/misc/requests/Result;->IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    invoke-virtual {v11, v12}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;)V

    .line 72
    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v10

    .line 75
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 76
    :catch_2
    move-exception v3

    .line 77
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 74
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    .line 75
    :goto_3
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 78
    :goto_4
    throw v11

    .line 76
    :catch_3
    move-exception v3

    .line 77
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 84
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_0
    const-class v11, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    const-string v12, "handleStoreManifestRequest"

    const-string v13, "manifest succesfully written"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "path"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 86
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent$3;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    sget-object v12, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    invoke-virtual {v11, v12}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;)V

    goto :goto_1

    .line 74
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catchall_1
    move-exception v11

    move-object v1, v2

    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_3

    .line 67
    .end local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v3

    move-object v1, v2

    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v1    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    goto :goto_2
.end method
