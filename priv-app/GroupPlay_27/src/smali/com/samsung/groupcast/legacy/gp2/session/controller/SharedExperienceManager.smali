.class public Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
.super Ljava/lang/Object;
.source "SharedExperienceManager.java"

# interfaces
.implements Lcom/samsung/groupcast/application/viewer/ContentDeliveryListener;
.implements Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$7;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$LobbyChannelListener;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;,
        Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    }
.end annotation


# static fields
.field public static final CLEAR_IMGDOC_SESSION_RESULT_DISABLED:I = 0x1

.field public static final CLEAR_IMGDOC_SESSION_RESULT_FAILED:I = 0x0

.field public static final CLEAR_IMGDOC_SESSION_RESULT_HAS_ACTIVITY:I = 0x3

.field public static final CLEAR_IMGDOC_SESSION_RESULT_NOT_HOST:I = 0x2

.field public static final CLEAR_IMGDOC_SESSION_RESULT_OK:I = -0x1

.field public static final CLEAR_IMGDOC_SESSION_RESULT_PARTICIPANT_HAS_ACTIVITY:I = 0x4

.field private static final FILE_OFFER_TIMEOUT_MILLISECONDS:I = 0xea60

.field private static final PROPAGATE_CURRENT_PAGE_JITTER_WINDOW_MILLISECONDS:J = 0x3e8L

.field private static final PROPAGATE_CURRENT_PAGE_MIN_INTERVAL_MILLISECONDS:J = 0x5dcL

.field private static final PROPAGATE_MANIFEST_INFO_JITTER_WINDOW_MILLISECONDS:J = 0x3e8L

.field private static final PROPAGATE_MANIFEST_INFO_MIN_INTERVAL_MILLISECONDS:J = 0x7d0L

.field private static final PROPAGATE_PING_REQUEST_JITTER_WINDOW_MILLISECONDS:J = 0x3e8L

.field private static final PROPAGATE_PING_REQUEST_MIN_INTERVAL_MILLISECONDS:J = 0xbb8L

.field private static final TAG:Ljava/lang/String; = "SHExp"

.field private static sLastesttInstance:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;


# instance fields
.field private mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

.field private mContentDeliveries:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Lcom/samsung/groupcast/core/messaging/Channel;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mContentDeliveryLock:Ljava/lang/Object;

.field private mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

.field private mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

.field private mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

.field private mEnabled:Z

.field private mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

.field private final mHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

.field mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field private mLastPg_collectionName:Ljava/lang/String;

.field private mLastPg_page:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

.field private mLastPg_previousPage:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

.field private mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

.field private final mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

.field private final mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

.field private final mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

.field private final mPageStateManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

.field private mPingId:Ljava/lang/Integer;

.field private mPrivateMode:Z

.field private final mPropagateCurrentPageRunnable:Ljava/lang/Runnable;

.field private final mPropagateManifestInfoRunnable:Ljava/lang/Runnable;

.field private final mPropagatePingRequestRunnable:Ljava/lang/Runnable;

.field private final mRandom:Ljava/util/Random;

.field private mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

.field private final mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

.field private final mSessionChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->sLastesttInstance:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;)V
    .locals 4
    .param p1, "messagingClient"    # Lcom/samsung/groupcast/core/messaging/MessagingClient;
    .param p2, "session"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .param p3, "contentMap"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$LobbyChannelListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$LobbyChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 124
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SessionChannelListener;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSessionChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    .line 132
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mRandom:Ljava/util/Random;

    .line 136
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveryLock:Ljava/lang/Object;

    .line 137
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    .line 139
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHandlers:Ljava/util/HashMap;

    .line 143
    iput-boolean v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    .line 144
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_collectionName:Ljava/lang/String;

    .line 145
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_previousPage:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .line 146
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_page:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .line 147
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    .line 559
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_UNKOWN:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 153
    const-string v1, "messagingClient"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 154
    const-string v1, "session"

    invoke-static {v1, p2}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 155
    const-string v1, "contentMap"

    invoke-static {v1, p3}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 156
    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    .line 157
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    .line 158
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-direct {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPageStateManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    .line 159
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPageStateManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager$PageStateManagerDelegate;)V

    .line 160
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelIdForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "channelId":Ljava/lang/String;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-direct {v1, v2, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;-><init>(Lcom/samsung/groupcast/core/messaging/MessagingClient;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    .line 162
    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    .line 165
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPingId:Ljava/lang/Integer;

    .line 168
    sput-object p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->sLastesttInstance:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .line 170
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateCurrentPageRunnable:Ljava/lang/Runnable;

    .line 178
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$2;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$2;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateManifestInfoRunnable:Ljava/lang/Runnable;

    .line 186
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$3;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$3;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    iput-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagatePingRequestRunnable:Ljava/lang/Runnable;

    .line 198
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "x2"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J

    .prologue
    .line 72
    invoke-direct/range {p0 .. p5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onFetchManifestComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    .param p2, "x2"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onReadManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->propagateCurrentPage()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleCurrentPagePropagation()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->propagateManifestInfo()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleManifestInfoPropagation(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->propagatePingRequest()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePingRequest()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    return-object v0
.end method

.method private clearCollectionFromManifest(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 745
    .local p1, "collections":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "clearCollectionFromManifest :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 746
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;

    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 747
    .local v3, "manifestBuilder":Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 748
    .local v0, "collection":Ljava/lang/String;
    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->removeCollection(Ljava/lang/String;)V

    goto :goto_0

    .line 751
    .end local v0    # "collection":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ManifestBuilder;->toManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    .line 752
    .local v2, "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;-><init>(Ljava/lang/String;)V

    .line 753
    .local v4, "storage":Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;
    invoke-virtual {v4, v2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->createStoreManifestRequest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;

    move-result-object v5

    .line 754
    .local v5, "storeManifestRequest":Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;
    new-instance v6, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;

    invoke-direct {v6, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$4;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    invoke-virtual {v5, v6}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 772
    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StoreManifestRequest;->start()V

    .line 773
    return-void
.end method

.method private clearImgDocSession(Lcom/samsung/groupcast/core/messaging/Channel;Z)I
    .locals 11
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "checkIsHost"    # Z

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x1

    .line 785
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->isClearImgDocSessionEnabled(Lcom/samsung/groupcast/core/messaging/Channel;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 786
    const-string v8, "result : CLEAR_IMGDOC_SESSION_RESULT_DISABLED"

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 866
    :goto_0
    return v7

    .line 790
    :cond_0
    if-eqz p2, :cond_1

    .line 791
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->isThisHostTheContentManager()Z

    move-result v8

    if-nez v8, :cond_1

    .line 792
    const-string v7, "not host return"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 793
    const-string v7, "result : CLEAR_IMGDOC_SESSION_RESULT_NOT_HOST"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 794
    const/4 v7, 0x2

    goto :goto_0

    .line 798
    :cond_1
    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUsersName()Ljava/util/Collection;

    move-result-object v6

    .line 799
    .local v6, "usersName":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 801
    .local v5, "userNamesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "clearImgDocSession :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 803
    const/4 v1, 0x0

    .line 804
    .local v1, "imgCnt":I
    const/4 v0, 0x0

    .line 806
    .local v0, "docCnt":I
    new-array v8, v7, [Ljava/lang/String;

    const-string v9, "MyImages"

    aput-object v9, v8, v10

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getJoinedActivitiesHS()Ljava/util/HashSet;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->hasActivity([Ljava/lang/String;Ljava/util/HashSet;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 808
    add-int/lit8 v1, v1, 0x1

    .line 810
    :cond_2
    new-array v8, v7, [Ljava/lang/String;

    const-string v9, "MyDocumentPages"

    aput-object v9, v8, v10

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getJoinedActivitiesHS()Ljava/util/HashSet;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->hasActivity([Ljava/lang/String;Ljava/util/HashSet;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 812
    add-int/lit8 v0, v0, 0x1

    .line 815
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "imgCnt:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",docCnt:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 816
    if-lez v1, :cond_4

    if-lez v0, :cond_4

    .line 817
    const-string v7, "result : CLEAR_IMGDOC_SESSION_RESULT_HAS_ACTIVITY"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 818
    const/4 v7, 0x3

    goto/16 :goto_0

    .line 821
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 822
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 823
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p1, v2}, Lcom/samsung/groupcast/core/messaging/Channel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v3

    .line 824
    .local v3, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-eqz v3, :cond_4

    .line 825
    if-nez v1, :cond_5

    new-array v8, v7, [Ljava/lang/String;

    const-string v9, "MyImages"

    aput-object v9, v8, v10

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getJoinedActivities()Ljava/util/HashSet;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->hasActivity([Ljava/lang/String;Ljava/util/HashSet;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 828
    add-int/lit8 v1, v1, 0x1

    .line 830
    :cond_5
    if-nez v0, :cond_6

    new-array v8, v7, [Ljava/lang/String;

    const-string v9, "MyDocumentPages"

    aput-object v9, v8, v10

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getJoinedActivities()Ljava/util/HashSet;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->hasActivity([Ljava/lang/String;Ljava/util/HashSet;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 833
    add-int/lit8 v0, v0, 0x1

    .line 835
    :cond_6
    if-lez v1, :cond_4

    if-lez v0, :cond_4

    .line 840
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    :cond_7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 842
    .local v4, "toDel":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v1, :cond_8

    .line 844
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    const-string v8, "MyImages"

    invoke-virtual {v7, v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 845
    const-string v7, "MyImages"

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    :cond_8
    if-nez v0, :cond_9

    .line 850
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v7

    const-string v8, "MyDocumentPages"

    invoke-virtual {v7, v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 852
    const-string v7, "MyDocumentPages"

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 856
    :cond_9
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "imgCnt:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",docCnt:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 857
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "toDel:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 859
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_a

    .line 860
    invoke-direct {p0, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->clearCollectionFromManifest(Ljava/util/ArrayList;)V

    .line 861
    const-string v7, "result : CLEAR_IMGDOC_SESSION_RESULT_OK"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 862
    const/4 v7, -0x1

    goto/16 :goto_0

    .line 865
    :cond_a
    const-string v7, "result : CLEAR_IMGDOC_SESSION_RESULT_PARTICIPANT_HAS_ACTIVITY"

    invoke-static {v7}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 866
    const/4 v7, 0x4

    goto/16 :goto_0
.end method

.method public static getLatestInstance()Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->sLastesttInstance:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;

    return-object v0
.end method

.method private isClearImgDocSessionEnabled(Lcom/samsung/groupcast/core/messaging/Channel;)Z
    .locals 4
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;

    .prologue
    .line 1292
    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getPeers()Ljava/util/Collection;

    move-result-object v1

    .line 1293
    .local v1, "usersPeer":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1295
    .local v2, "usersPeerIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1296
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p1, v3}, Lcom/samsung/groupcast/core/messaging/Channel;->getParticipantInfo(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;

    move-result-object v0

    .line 1297
    .local v0, "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    if-eqz v0, :cond_0

    .line 1298
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;->getContentsResetEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1299
    const/4 v3, 0x0

    .line 1303
    .end local v0    # "participantInfo":Lcom/samsung/groupcast/legacy/gp2/session/model/ParticipantInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isThisHostTheContentManager()Z
    .locals 5

    .prologue
    .line 563
    const/4 v1, 0x0

    .line 565
    .local v1, "res":Z
    sget-object v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$7;->$SwitchMap$com$samsung$groupcast$legacy$gp2$session$controller$SharedExperienceManager$IsThisHostTheContentManagerType:[I

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 593
    :goto_0
    :pswitch_0
    const-string v2, "SHExp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "res:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",mType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    return v1

    .line 568
    :pswitch_1
    const/4 v1, 0x1

    .line 569
    goto :goto_0

    .line 574
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getUserInfo()Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;

    move-result-object v0

    .line 576
    .local v0, "ip":Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;
    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/UserInfo;->getWifiIp()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->IsHostOrAP(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 578
    sget-object v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_MANAGER_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 580
    const/4 v1, 0x1

    goto :goto_0

    .line 582
    :cond_0
    sget-object v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_GUEST_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mIsThisHostTheContentManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    goto :goto_0

    .line 565
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private onContentRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 9
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "contentId"    # Ljava/lang/String;

    .prologue
    .line 527
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onContentRequestV1"

    const/4 v5, 0x0

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "node"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    const-string v8, "contentId"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p4, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 529
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-nez v3, :cond_0

    .line 549
    :goto_0
    return-void

    .line 532
    :cond_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v4

    .line 534
    const/4 v0, 0x1

    .line 535
    .local v0, "isRegistered":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    invoke-virtual {v3, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    .line 536
    .local v1, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    if-nez v1, :cond_1

    .line 537
    new-instance v1, Ljava/util/LinkedList;

    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 538
    .restart local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    invoke-virtual {v3, p4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    const/4 v0, 0x0

    .line 541
    :cond_1
    new-instance v2, Ljava/util/AbstractMap$SimpleEntry;

    invoke-direct {v2, p1, p2}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 543
    .local v2, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 546
    if-nez v0, :cond_2

    .line 547
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v3, p0, p4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onRegisterForContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V

    .line 548
    :cond_2
    monitor-exit v4

    goto :goto_0

    .end local v1    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .end local v2    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private onCurrentPageInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;)V
    .locals 9
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    .prologue
    .line 885
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 886
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->getCollectionName()Ljava/lang/String;

    move-result-object v3

    .line 887
    .local v3, "remoteCollectionName":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->getContentId()Ljava/lang/String;

    move-result-object v8

    .line 888
    .local v8, "remoteContentId":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->getAge()J

    move-result-wide v6

    .line 889
    .local v6, "remoteAge":J
    invoke-static {v3, v8, v6, v7}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->existingPage(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v5

    .line 891
    .local v5, "remoteCurrentPage":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;->getCollectionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v4

    .line 893
    .local v4, "localCurrentPage":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    if-eqz v4, :cond_1

    invoke-virtual {v5, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->isFresher(Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 918
    :cond_0
    :goto_0
    return-void

    .line 904
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v0, v3, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->setCurrentPage(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V

    .line 905
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleCurrentPagePropagation()V

    .line 907
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    if-eqz v0, :cond_2

    .line 908
    const-string v0, "SHExp"

    const-string v1, "private mode ignore current page"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_collectionName:Ljava/lang/String;

    .line 910
    iput-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_previousPage:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    .line 911
    iput-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_page:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    goto :goto_0

    .line 913
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v0, :cond_0

    .line 914
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onCurrentPageChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V

    goto :goto_0
.end method

.method private onFetchManifestComplete(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;J)V
    .locals 9
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "manifestTimestamp"    # J

    .prologue
    .line 1000
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1002
    iget-boolean v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mEnabled:Z

    if-nez v3, :cond_0

    .line 1030
    :goto_0
    return-void

    .line 1004
    :cond_0
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1006
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 1007
    .local v1, "sessionId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    .line 1008
    .local v0, "manifestId":Ljava/lang/String;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    .line 1010
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->isError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1011
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onFetchManifestComplete"

    const-string v5, "manifest fetch failed"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "request"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    const-string v8, "result"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p2, v6, v7

    const/4 v7, 0x4

    const-string v8, "path"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    aput-object p3, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1016
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onFetchManifestComplete"

    const-string v5, "manifest fetch succeeded"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "request"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    const-string v8, "result"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p2, v6, v7

    const/4 v7, 0x4

    const-string v8, "path"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    aput-object p3, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1019
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;

    invoke-direct {v2, v1}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;-><init>(Ljava/lang/String;)V

    .line 1020
    .local v2, "storageAgent":Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;
    invoke-virtual {v2, v0, p4, p5}, Lcom/samsung/groupcast/legacy/gp2/session/storage/StorageAgent;->createReadManifestRequest(Ljava/lang/String;J)Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .line 1022
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$6;

    invoke-direct {v4, p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$6;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;)V

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 1029
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->start()V

    goto/16 :goto_0
.end method

.method private onManifestInfoV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;)V
    .locals 14
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    .prologue
    .line 955
    const-string v8, "---"

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 956
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    if-eqz v8, :cond_0

    .line 996
    :goto_0
    return-void

    .line 960
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->getManifestId()Ljava/lang/String;

    move-result-object v7

    .line 961
    .local v7, "remoteManifestId":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;->getAge()J

    move-result-wide v5

    .line 962
    .local v5, "remoteManifestAge":J
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v2

    .line 963
    .local v2, "localManifestId":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getAge()J

    move-result-wide v0

    .line 965
    .local v0, "localManifestAge":J
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 966
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    cmp-long v8, v0, v5

    if-gtz v8, :cond_3

    .line 968
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "onManifestInfoV1"

    const-string v10, "remote manifest is stale, ignoring"

    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "remoteManifestId"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v7, v11, v12

    const/4 v12, 0x2

    const-string v13, "localManifestId"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    aput-object v2, v11, v12

    const/4 v12, 0x4

    const-string v13, "localManifestAge"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x6

    const-string v13, "remoteManifestAge"

    aput-object v13, v11, v12

    const/4 v12, 0x7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0

    .line 975
    :cond_2
    const-string v8, "SHExp"

    const-string v9, "onManifestInfoV1 local manifest is invalid.."

    invoke-static {v8, v9}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "onManifestInfoV1"

    const-string v10, "remote manifest is fresh"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "localManifestId"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v2, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 981
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "onManifestInfoV1"

    const-string v10, "requesting remote manifest..."

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "remoteManifestId"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v7, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 984
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    sub-long v3, v8, v5

    .line 985
    .local v3, "manifestTimestamp":J
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    iget-object v9, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v9}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v7}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->createFetchManifestRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    .line 988
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    new-instance v9, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$5;

    invoke-direct {v9, p0, v3, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$5;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;J)V

    invoke-virtual {v8, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    .line 995
    iget-object v8, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v8}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->start()V

    goto/16 :goto_0
.end method

.method private onManifestRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;Ljava/lang/String;)V
    .locals 9
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    .param p4, "manifestId"    # Ljava/lang/String;

    .prologue
    .line 497
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onManifestRequestV1"

    const/4 v5, 0x0

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "node"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    const-string v8, "manifestId"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p4, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 499
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 508
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "sessionId":Ljava/lang/String;
    invoke-static {v2, p4}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 506
    .local v1, "path":Ljava/lang/String;
    invoke-static {v2, p4, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->createFileIssueForManifest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;

    move-result-object v0

    .line 507
    .local v0, "issue":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    const-wide/32 v3, 0xea60

    invoke-interface {p1, p2, v0, v3, v4}, Lcom/samsung/groupcast/core/messaging/Channel;->sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V

    goto :goto_0
.end method

.method private onPageEditV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;)V
    .locals 11
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    .prologue
    .line 472
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPageEditV1"

    const/4 v2, 0x0

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "node"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    aput-object p2, v8, v9

    const/4 v9, 0x2

    const-string v10, "message"

    aput-object v10, v8, v9

    const/4 v9, 0x3

    aput-object p3, v8, v9

    invoke-static {v0, v1, v2, v8}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 473
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->getContentId()Ljava/lang/String;

    move-result-object v3

    .line 474
    .local v3, "contentId":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;->getPageEdit()Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    move-result-object v5

    .line 477
    .local v5, "pageEdit":Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPageState(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;

    move-result-object v4

    .line 479
    .local v4, "pageState":Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;
    :try_start_0
    invoke-virtual {v5, v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;->apply(Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :goto_0
    const/4 v6, 0x0

    .line 486
    .local v6, "bDrew":Z
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onPageEdited(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageStateNode;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)Z

    move-result v6

    .line 493
    :cond_0
    return-void

    .line 480
    .end local v6    # "bDrew":Z
    :catch_0
    move-exception v7

    .line 481
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 482
    const-string v0, "GP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onReadManifestRequestComplete(Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;Lcom/samsung/groupcast/misc/requests/Result;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 10
    .param p1, "request"    # Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    const/4 v9, 0x1

    .line 1034
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1035
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1036
    .local v2, "sessionId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;->getManifestId()Ljava/lang/String;

    move-result-object v0

    .line 1037
    .local v0, "manifestId":Ljava/lang/String;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mReadManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/storage/ReadManifestRequest;

    .line 1039
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->isError()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1040
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onReadManifestRequestComplete"

    const-string v5, "manifest read failed"

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "request"

    aput-object v8, v6, v7

    aput-object p1, v6, v9

    const/4 v7, 0x2

    const-string v8, "result"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object p2, v6, v7

    const/4 v7, 0x4

    const-string v8, "sessionId"

    aput-object v8, v6, v7

    const/4 v7, 0x5

    aput-object v2, v6, v7

    const/4 v7, 0x6

    const-string v8, "manifestId"

    aput-object v8, v6, v7

    const/4 v7, 0x7

    aput-object v0, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1046
    :cond_1
    const-string v3, "---"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1048
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    .line 1049
    .local v1, "previousManifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3, p3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 1050
    invoke-direct {p0, v9}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleManifestInfoPropagation(Z)V

    .line 1052
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v3, :cond_2

    .line 1053
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-interface {v3, p0, v4, v1, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 1055
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v3, :cond_0

    .line 1056
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-interface {v3, p0, v4, v1, p3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onManifestChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    goto :goto_0
.end method

.method private onSessionSummaryRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;)V
    .locals 3
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryRequestV1;

    .prologue
    .line 466
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-direct {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)V

    .line 467
    .local v1, "summary":Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V

    .line 468
    .local v0, "info":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/SessionSummaryInfoV1;
    const/4 v2, 0x0

    invoke-interface {p1, p2, v0, v2}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 469
    return-void
.end method

.method private propagateCurrentPage()V
    .locals 7

    .prologue
    .line 421
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    .local v0, "collectionName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v1

    .line 425
    .local v1, "currentPage":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    if-eqz v1, :cond_0

    .line 427
    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->isKnown()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 430
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->getContentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->getAge()J

    move-result-wide v5

    invoke-direct {v3, v0, v4, v5, v6}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 433
    .local v3, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    goto :goto_0

    .line 435
    .end local v0    # "collectionName":Ljava/lang/String;
    .end local v1    # "currentPage":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    .end local v3    # "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;
    :cond_1
    return-void
.end method

.method private propagateManifestInfo()V
    .locals 7

    .prologue
    .line 438
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    .line 439
    .local v2, "manifest":Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v3

    .line 440
    .local v3, "manifestId":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getAge()J

    move-result-wide v0

    .line 441
    .local v0, "age":J
    new-instance v4, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    invoke-direct {v4, v3, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;-><init>(Ljava/lang/String;J)V

    .line 442
    .local v4, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 443
    const-string v5, "manifest set!"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 445
    return-void
.end method

.method private propagatePingRequest()V
    .locals 3

    .prologue
    .line 448
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPingId:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPingId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPingId:Ljava/lang/Integer;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;-><init>(Ljava/lang/Integer;)V

    .line 449
    .local v0, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PingV1;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 450
    return-void
.end method

.method private rescheduleCurrentPagePropagation()V
    .locals 6

    .prologue
    .line 379
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateCurrentPageRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x5dc

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePropagationRunnable(Ljava/lang/Runnable;JJ)V

    .line 382
    return-void
.end method

.method private rescheduleManifestInfoPropagation(Z)V
    .locals 6
    .param p1, "isInitial"    # Z

    .prologue
    .line 385
    if-eqz p1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateManifestInfoRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    const-wide/16 v4, 0xfa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePropagationRunnable(Ljava/lang/Runnable;JJ)V

    .line 391
    :goto_0
    return-void

    .line 388
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateManifestInfoRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePropagationRunnable(Ljava/lang/Runnable;JJ)V

    goto :goto_0
.end method

.method private reschedulePingRequest()V
    .locals 6

    .prologue
    .line 394
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagatePingRequestRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePropagationRunnable(Ljava/lang/Runnable;JJ)V

    .line 397
    return-void
.end method

.method private reschedulePropagationRunnable(Ljava/lang/Runnable;JJ)V
    .locals 6
    .param p1, "runnable"    # Ljava/lang/Runnable;
    .param p2, "minIntervalMilliseconds"    # J
    .param p4, "jitterWindowMilliseconds"    # J

    .prologue
    .line 402
    invoke-static {p1}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 403
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mRandom:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    rem-long/2addr v4, p4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 404
    .local v2, "jitter":J
    add-long v0, p2, v2

    .line 405
    .local v0, "delay":J
    invoke-static {p1, v0, v1}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 406
    return-void
.end method

.method private sendRemoteNetworkInfo(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 0
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 640
    return-void
.end method

.method private unscheduleCurrentPagePropagation()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateCurrentPageRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 410
    return-void
.end method

.method private unscheduleManifestInfoPropagation()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagateManifestInfoRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 414
    return-void
.end method

.method private unschedulePingRequestPropagation()V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPropagatePingRequestRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 418
    return-void
.end method


# virtual methods
.method public acquirePageStateContext(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPageStateManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->acquirePageStateContext(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;

    move-result-object v0

    return-object v0
.end method

.method public clearImgDocSession()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 776
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v1, :cond_0

    .line 777
    const-string v1, "not null : mHybridChannel"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 778
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-direct {p0, v1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->clearImgDocSession(Lcom/samsung/groupcast/core/messaging/Channel;Z)I

    move-result v0

    .line 781
    :goto_0
    return v0

    .line 780
    :cond_0
    const-string v1, "result : CLEAR_IMGDOC_SESSION_RESULT_FAILED"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 245
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    .line 247
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mEnabled:Z

    if-nez v0, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "disable"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mEnabled:Z

    .line 253
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 254
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSessionChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->removeListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 255
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseLocalChannel(Lcom/samsung/groupcast/core/messaging/LocalChannel;)V

    .line 256
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseHybridChannel(Lcom/samsung/groupcast/core/messaging/HybridChannel;)V

    .line 257
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->releaseConnection(Lcom/samsung/groupcast/core/messaging/ConnectionHandle;)V

    .line 258
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 260
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 261
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->unscheduleCurrentPagePropagation()V

    .line 262
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->unscheduleManifestInfoPropagation()V

    .line 263
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->unschedulePingRequestPropagation()V

    .line 264
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->disable()V

    .line 266
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;->invalidate()V

    .line 268
    iput-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mFetchManifestRequest:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchManifestRequest;

    goto :goto_0
.end method

.method public enable()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 226
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mEnabled:Z

    if-eqz v0, :cond_0

    .line 242
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "enable"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 231
    iput-boolean v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mEnabled:Z

    .line 232
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireConnection(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mConnection:Lcom/samsung/groupcast/core/messaging/ConnectionHandle;

    .line 233
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    const-string v1, "com.samsung.groupcast.Lobby_V1"

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    .line 234
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mMessagingClient:Lcom/samsung/groupcast/core/messaging/MessagingClient;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelIdForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->acquireHybridChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    .line 236
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannel:Lcom/samsung/groupcast/core/messaging/LocalChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLobbyChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSessionChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->addListener(Lcom/samsung/groupcast/core/messaging/ChannelListener;)V

    .line 238
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleCurrentPagePropagation()V

    .line 239
    invoke-direct {p0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleManifestInfoPropagation(Z)V

    .line 240
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->reschedulePingRequest()V

    .line 241
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mManifestFetcher:Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ManifestFetcher;->enable()V

    goto :goto_0
.end method

.method public getContentMap()Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;
    .locals 6

    .prologue
    .line 205
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getContentMap"

    const-string v2, "debug"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "mContentMap"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    return-object v0
.end method

.method public getCurrentManifestPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 511
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 513
    :cond_0
    const-string v1, "SHExp"

    const-string v2, "something was null"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v0, ""

    .line 521
    :goto_0
    return-object v0

    .line 518
    :cond_1
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getSessionId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/storage/SessionStorage;->getSessionManifestFilePath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 521
    .local v0, "path":Ljava/lang/String;
    goto :goto_0
.end method

.method public getHybridChannel()Lcom/samsung/groupcast/core/messaging/HybridChannel;
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    return-object v0
.end method

.method public getLocalSessionChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;
    .locals 3

    .prologue
    .line 1061
    const-string v0, "SHExp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mHybridChannel:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getLocalChannel()Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    .line 1066
    :goto_0
    return-object v0

    .line 1065
    :cond_0
    const-string v0, "SHExp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mHybridChannel was null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    invoke-static {}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->GetInstance()Lcom/samsung/groupcast/core/messaging/MessagingClient;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-static {v1}, Lcom/samsung/groupcast/core/messaging/LocalChannel;->getChannelIdForSession(Lcom/samsung/groupcast/legacy/gp2/session/model/Session;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient;->getLocalChannel(Ljava/lang/String;)Lcom/samsung/groupcast/core/messaging/LocalChannel;

    move-result-object v0

    goto :goto_0
.end method

.method public getSession()Lcom/samsung/groupcast/legacy/gp2/session/model/Session;
    .locals 1

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    return-object v0
.end method

.method public getSessionChannelListener()Lcom/samsung/groupcast/core/messaging/ChannelListener;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSessionChannelListener:Lcom/samsung/groupcast/core/messaging/ChannelListener;

    return-object v0
.end method

.method public hasActivity([Ljava/lang/String;Ljava/util/HashSet;)Z
    .locals 9
    .param p1, "target"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "joinedActivities":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 723
    if-nez p2, :cond_1

    .line 724
    const-string v8, "joinedActivities is null"

    invoke-static {v8}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 741
    :cond_0
    :goto_0
    return v7

    .line 729
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 731
    .local v3, "joinedActivitiesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 732
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 733
    .local v4, "joinedActivityName":Ljava/lang/String;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v0, v2

    .line 734
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    if-eqz v8, :cond_3

    .line 735
    const/4 v7, 0x1

    goto :goto_0

    .line 733
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 738
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "joinedActivitiesIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v4    # "joinedActivityName":Ljava/lang/String;
    .end local v5    # "len$":I
    .end local v6    # "s":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 739
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isClearImgDocSessionEnabled()Z
    .locals 1

    .prologue
    .line 1274
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v0, :cond_0

    .line 1275
    const-string v0, "not null : mLocalSessionChannel"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1276
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->isClearImgDocSessionEnabled(Lcom/samsung/groupcast/core/messaging/Channel;)Z

    move-result v0

    .line 1278
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrivateMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1263
    iget-boolean v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContentAvailable(Ljava/lang/String;)V
    .locals 5
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 362
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v2, "CONTENT"

    invoke-direct {v0, v2, p1}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;I)V

    .line 366
    .local v1, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 368
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {v2, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->getContentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 370
    :cond_0
    const-string v2, "SHExp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onContentAvailable mContentMap:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " or contentId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentMap:Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentMap;->getContentPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_1
    return-void
.end method

.method public onContentDeliveryAbort(Ljava/lang/String;)V
    .locals 1
    .param p1, "contentId"    # Ljava/lang/String;

    .prologue
    .line 1244
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1245
    return-void
.end method

.method public onContentDeliveryComplete(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V
    .locals 8
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "result"    # Lcom/samsung/groupcast/misc/requests/Result;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 1167
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Result;->isSuccess()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1191
    :cond_0
    :goto_0
    return-void

    .line 1172
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v5

    .line 1173
    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 1174
    .local v2, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1175
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v4, :cond_2

    .line 1176
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v4, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onUnregisterFromContentDelivery(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;)V

    .line 1177
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1178
    if-eqz v2, :cond_0

    .line 1182
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1183
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1184
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1185
    .local v3, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    invoke-static {p1, p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;->createFileIssueForContent(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;

    move-result-object v0

    .line 1186
    .local v0, "issue":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/groupcast/core/messaging/Channel;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-wide/32 v6, 0xea60

    invoke-interface {v4, v5, v0, v6, v7}, Lcom/samsung/groupcast/core/messaging/Channel;->sendFileIssueToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/FileIssue;J)V

    .line 1188
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1177
    .end local v0    # "issue":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileIssueV1;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .end local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .end local v3    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 1190
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .restart local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    :cond_3
    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    goto :goto_0
.end method

.method public onContentDeliveryProgress(Ljava/lang/String;Lcom/samsung/groupcast/misc/requests/Progress;)V
    .locals 8
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "progress"    # Lcom/samsung/groupcast/misc/requests/Progress;

    .prologue
    .line 1219
    invoke-virtual {p2}, Lcom/samsung/groupcast/misc/requests/Progress;->isLoading()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1222
    iget-object v6, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveryLock:Ljava/lang/Object;

    monitor-enter v6

    .line 1223
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mContentDeliveries:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedList;

    .line 1224
    .local v2, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    if-nez v2, :cond_1

    .line 1225
    monitor-exit v6

    .line 1240
    .end local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    :cond_0
    :goto_0
    return-void

    .line 1227
    .restart local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    :cond_1
    new-instance v0, Lcom/samsung/groupcast/core/messaging/DataKey;

    const-string v5, "CONTENT"

    invoke-direct {v0, v5, p1}, Lcom/samsung/groupcast/core/messaging/DataKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    .local v0, "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    new-instance v3, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;

    const/4 v5, 0x1

    invoke-direct {v3, v0, v5}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;I)V

    .line 1233
    .local v3, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1234
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1235
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1236
    .local v4, "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v5, v3}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;)V

    goto :goto_1

    .line 1238
    .end local v0    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .end local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .end local v3    # "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;
    .end local v4    # "pair":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v0    # "dataKey":Lcom/samsung/groupcast/core/messaging/DataKey;
    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .restart local v2    # "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/util/Map$Entry<Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;>;>;"
    .restart local v3    # "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileAvailabilityInfoV1;
    :cond_2
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onCurrentPageRequestV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;)V
    .locals 6
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;

    .prologue
    .line 870
    const-string v2, "---"

    invoke-static {v2}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 871
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;->getCollectionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v0

    .line 873
    .local v0, "currentPage":Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->isKnown()Z

    move-result v2

    if-nez v2, :cond_1

    .line 882
    :cond_0
    :goto_0
    return-void

    .line 878
    :cond_1
    new-instance v1, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;->getCollectionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->getContentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->getAge()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 881
    .local v1, "info":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v2, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    goto :goto_0
.end method

.method public onDrawingSyncJsonRequestReal(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;)V
    .locals 6
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonRequestV1;

    .prologue
    .line 1204
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onDrawingSyncRequestV1"

    const-string v2, "debug"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    const-string v5, "message"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1207
    return-void
.end method

.method public onDrawingSyncJsonResponseV1Real(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;)V
    .locals 6
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncJsonResponseV1;

    .prologue
    .line 1211
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onDrawingSyncJSONResponseV1"

    const-string v2, "debug"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    const-string v5, "message"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1215
    return-void
.end method

.method public onDrawingSyncResponseV1Real(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;)V
    .locals 6
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/DrawingSyncNotificationV1;

    .prologue
    .line 1196
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onDrawingSyncResponseV1"

    const-string v2, "debug"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    const-string v5, "message"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 1200
    return-void
.end method

.method public onPageCommandV1(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)V
    .locals 8
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;
    .param p3, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;

    .prologue
    .line 921
    const-string v4, "---"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 922
    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 923
    .local v0, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getContentId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPageStateNEW(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    move-result-object v3

    .line 924
    .local v3, "state":Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getMessageType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;

    .line 925
    .local v1, "h":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
    const/4 v2, 0x0

    .line 926
    .local v2, "isCmdApplied":Z
    if-eqz v1, :cond_1

    .line 927
    invoke-virtual {v1, p3, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->applyPageCommand(Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;)Z

    move-result v2

    .line 933
    :goto_0
    if-eqz v2, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v4, :cond_0

    .line 934
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v4, p0, v0, p3, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onPageCommand(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;)V

    .line 936
    :cond_0
    return-void

    .line 929
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "onPageCommandV1"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Getting PageCommandHandler for type \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getMessageType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' failed"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onPageStateEdited(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)V
    .locals 3
    .param p1, "manager"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p3, "pageEdit"    # Lcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;

    .prologue
    const/4 v2, 0x0

    .line 455
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;

    invoke-direct {v0, p2, v2, p3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;-><init>(Ljava/lang/String;ILcom/samsung/groupcast/legacy/gp2/session/model/PageEditNode;)V

    .line 457
    .local v0, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageEditV1;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v1, :cond_0

    .line 458
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;Z)V

    .line 460
    :cond_0
    return-void
.end method

.method public onParticipantInfoChanged(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 1
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 708
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onPeerChanged()V

    .line 710
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onViewerPeerChanged()V

    .line 714
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->clearImgDocSession(Lcom/samsung/groupcast/core/messaging/Channel;Z)I

    .line 718
    return-void
.end method

.method public onPeerJoined(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 13
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 598
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onPeerJoined"

    const/4 v5, 0x0

    new-array v6, v12, [Ljava/lang/Object;

    const-string v7, "channel"

    aput-object v7, v6, v8

    aput-object p1, v6, v9

    const-string v7, "node"

    aput-object v7, v6, v10

    aput-object p2, v6, v11

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 601
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollections()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 602
    .local v0, "collectionName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v3, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->isKnown()Z

    move-result v3

    if-nez v3, :cond_0

    .line 604
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "onPeerJoined"

    const-string v5, "collection "

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    const-string v7, " requesting current page from peer"

    aput-object v7, v6, v9

    const-string v7, "channel"

    aput-object v7, v6, v10

    aput-object p1, v6, v11

    const-string v7, "node"

    aput-object v7, v6, v12

    const/4 v7, 0x5

    aput-object p2, v6, v7

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 606
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;

    invoke-direct {v2, v0}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;-><init>(Ljava/lang/String;)V

    .line 607
    .local v2, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;
    invoke-interface {p1, p2, v2}, Lcom/samsung/groupcast/core/messaging/Channel;->sendMessageToNode(Ljava/lang/String;Lcom/samsung/groupcast/core/messaging/Message;)V

    goto :goto_0

    .line 611
    .end local v0    # "collectionName":Ljava/lang/String;
    .end local v2    # "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageRequestV1;
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v3, :cond_3

    .line 612
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onPeerChanged()V

    .line 613
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onViewerPeerChanged()V

    .line 616
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->isThisHostTheContentManager()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 618
    invoke-direct {p0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->sendRemoteNetworkInfo(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V

    .line 621
    :cond_4
    invoke-static {}, Lcom/samsung/groupcast/application/StatLog;->getInstance()Lcom/samsung/groupcast/application/StatLog;

    move-result-object v3

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getPeerCount()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/application/StatLog;->updateMaxUser(I)V

    .line 622
    return-void
.end method

.method public onPeerLeft(Lcom/samsung/groupcast/core/messaging/Channel;Ljava/lang/String;)V
    .locals 6
    .param p1, "channel"    # Lcom/samsung/groupcast/core/messaging/Channel;
    .param p2, "node"    # Ljava/lang/String;

    .prologue
    .line 694
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPeerLeft"

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "channel"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    const-string v5, "node"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aput-object p2, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 696
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/samsung/groupcast/core/messaging/Channel;->getChannelId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->getChannelId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v0, :cond_1

    .line 702
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onPeerChanged()V

    .line 703
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    invoke-interface {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onViewerPeerChanged()V

    .line 705
    :cond_1
    return-void
.end method

.method public onSendPageCommand(Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    .locals 3
    .param p1, "message"    # Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;

    .prologue
    .line 939
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 940
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/PageCommandV1;->getContentId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getPageStateNEW(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;

    move-result-object v0

    .line 948
    .local v0, "state":Lcom/samsung/groupcast/legacy/gp2/session/model/PageState;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v1, :cond_0

    .line 949
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 951
    :cond_0
    return-object v0
.end method

.method public releasePageStateContext(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;)V
    .locals 1
    .param p1, "context"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPageStateManager:Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateManager;->releasePageStateContext(Lcom/samsung/groupcast/legacy/gp2/session/controller/PageStateContext;)V

    .line 359
    return-void
.end method

.method public setCurrentPage(Ljava/lang/String;I)V
    .locals 6
    .param p1, "collectionName"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 312
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    if-nez v4, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v4

    if-lt p2, v4, :cond_2

    .line 317
    const-string v4, "ERROR: index is outOfBound. fixed to inbound."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 318
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getPageCount()I

    move-result v4

    add-int/lit8 p2, v4, -0x1

    .line 321
    :cond_2
    if-ltz p2, :cond_0

    .line 326
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getCollection(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ItemsCollection;->getItem(I)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;

    move-result-object v3

    .line 327
    .local v3, "page":Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    invoke-virtual {v3}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    .line 329
    .local v0, "contentId":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 330
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getCurrentPage(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->isContent(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 336
    :cond_3
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-static {p1, v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;->freshPage(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-result-object v5

    invoke-virtual {v4, p1, v5}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->setCurrentPage(Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V

    .line 338
    iget-boolean v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    if-eqz v4, :cond_4

    .line 339
    const-string v4, "SHExp"

    const-string v5, "NOT send current page by private mode"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :cond_4
    new-instance v2, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;

    const-wide/16 v4, 0x0

    invoke-direct {v2, p1, v0, v4, v5}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 344
    .local v2, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/CurrentPageInfoV1;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v4, v2}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 346
    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleCurrentPagePropagation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 347
    :catch_0
    move-exception v1

    .line 348
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;)V
    .locals 3
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    .param p2, "caller"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    .line 212
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;

    .local v0, "handler":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
    move-object v2, p1

    .line 213
    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->setContentDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;)V

    goto :goto_0

    .line 215
    .end local v0    # "handler":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
    :cond_0
    return-void
.end method

.method public setDelegatePrivateMode(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;)V
    .locals 3
    .param p1, "delegate"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;
    .param p2, "caller"    # Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegatePrivateMode:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    .line 220
    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;

    .local v0, "handler":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
    move-object v2, p1

    .line 221
    check-cast v2, Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;

    invoke-virtual {v0, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;->setContentDelegate(Lcom/samsung/groupcast/legacy/gp2/session/controller/ContentDeliveryManagerDelegate;)V

    goto :goto_0

    .line 223
    .end local v0    # "handler":Lcom/samsung/groupcast/legacy/gp2/session/controller/PageCommandHandler;
    :cond_0
    return-void
.end method

.method public setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V
    .locals 4
    .param p1, "manifest"    # Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    .prologue
    .line 278
    const-string v1, "manifest"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    if-nez v1, :cond_0

    .line 282
    const-string v1, "SHExp"

    const-string v2, "setManifest session is null"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->getManifest()Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    const-string v1, "SHExp"

    const-string v2, "setManifest is same with session"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 292
    const-string v1, "SHExp"

    const-string v2, "setManifest manifest is invalid"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :cond_2
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    invoke-virtual {v1, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->setManifest(Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;)V

    .line 300
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;

    invoke-virtual {p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/Manifest;->getManifestId()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;-><init>(Ljava/lang/String;J)V

    .line 301
    .local v0, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/ManifestInfoV1;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    if-eqz v1, :cond_3

    .line 302
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mHybridChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    .line 308
    :cond_3
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->rescheduleManifestInfoPropagation(Z)V

    goto :goto_0
.end method

.method public setPrivateMode(Z)V
    .locals 6
    .param p1, "pmode"    # Z

    .prologue
    .line 1252
    iput-boolean p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    .line 1253
    iget-boolean v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mPrivateMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_collectionName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_previousPage:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_page:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    if-eqz v0, :cond_0

    .line 1256
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mDelegate:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mSession:Lcom/samsung/groupcast/legacy/gp2/session/model/Session;

    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_collectionName:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_previousPage:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    iget-object v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;->mLastPg_page:Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$SharedExperienceManagerDelegate;->onCurrentPageChanged(Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;Lcom/samsung/groupcast/legacy/gp2/session/model/Session;Ljava/lang/String;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;Lcom/samsung/groupcast/legacy/gp2/session/model/CurrentPage;)V

    .line 1260
    :cond_0
    return-void
.end method
