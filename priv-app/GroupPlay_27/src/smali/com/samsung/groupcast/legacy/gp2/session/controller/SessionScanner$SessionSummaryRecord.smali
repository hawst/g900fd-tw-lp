.class Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;
.super Ljava/lang/Object;
.source "SessionScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionSummaryRecord"
.end annotation


# instance fields
.field private final mExpiryRunnable:Ljava/lang/Runnable;

.field private mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

.field private mTimestamp:J

.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V
    .locals 1
    .param p2, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord$1;-><init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner;)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mExpiryRunnable:Ljava/lang/Runnable;

    .line 180
    invoke-virtual {p0, p2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->update(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V

    .line 181
    return-void
.end method


# virtual methods
.method public getSessionSummary()Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    return-object v0
.end method

.method public startExpiryTrigger()V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mExpiryRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0xfa0

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 196
    return-void
.end method

.method public stopExpiryTrigger()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mExpiryRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 200
    return-void
.end method

.method public update(Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;)V
    .locals 2
    .param p1, "summary"    # Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mSummary:Lcom/samsung/groupcast/legacy/gp2/session/model/SessionSummary;

    .line 189
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->mTimestamp:J

    .line 190
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->stopExpiryTrigger()V

    .line 191
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SessionScanner$SessionSummaryRecord;->startExpiryTrigger()V

    .line 192
    return-void
.end method
