.class final Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$2;
.super Ljava/lang/Object;
.source "CollagePageItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
    .locals 7
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 112
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 113
    .local v3, "age":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 115
    .local v4, "style":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 116
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$2;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 121
    new-array v0, p1, [Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$2;->newArray(I)[Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;

    move-result-object v0

    return-object v0
.end method
