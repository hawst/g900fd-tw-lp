.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;
.super Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;
.source "FileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->startRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

.field final synthetic val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

.field final synthetic val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/core/messaging/DataKey;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    invoke-direct {p0}, Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttempt(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 3
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 108
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "onAttempt"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 111
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;-><init>(Lcom/samsung/groupcast/core/messaging/DataKey;)V

    .line 112
    .local v0, "message":Lcom/samsung/groupcast/legacy/gp2/messaging/v1/FileRequestV1;
    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;
    invoke-static {v1}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->sendMessageToAll(Lcom/samsung/groupcast/core/messaging/Message;)V

    goto :goto_0
.end method

.method public onTimeout(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 3
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 119
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onTimeout"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;->complete(Lcom/samsung/groupcast/misc/requests/Result;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/utility/MultiMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$2;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
