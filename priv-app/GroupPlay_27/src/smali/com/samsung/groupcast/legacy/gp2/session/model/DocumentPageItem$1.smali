.class final Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem$1;
.super Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
.source "DocumentPageItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public createContentItemForLocalPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 23
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "document page items cannot be created from paths"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createContentItemFromJson(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 29
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentPageItem;-><init>(Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public getSupportedFileExtensions()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method
