.class Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;
.super Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;
.source "FileFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->startRequest(Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

.field final synthetic val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

.field final synthetic val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;Lcom/samsung/groupcast/core/messaging/DataKey;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    iput-object p2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    iput-object p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-direct {p0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onInvalidated(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 3
    .param p1, "r"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mEnabled:Z
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$100(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mScheduler:Lcom/samsung/groupcast/misc/requests/RequestScheduler;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$200(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->cancelAttempts(Lcom/samsung/groupcast/misc/requests/Request;)V

    .line 96
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/utility/MultiMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    iget-object v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$request:Lcom/samsung/groupcast/legacy/gp2/session/controller/FetchFileRequest;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/misc/utility/MultiMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mRequests:Lcom/samsung/groupcast/misc/utility/MultiMap;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$300(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/misc/utility/MultiMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/utility/MultiMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->this$0:Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;

    # getter for: Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->mChannel:Lcom/samsung/groupcast/core/messaging/HybridChannel;
    invoke-static {v0}, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;->access$400(Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher;)Lcom/samsung/groupcast/core/messaging/HybridChannel;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/legacy/gp2/session/controller/FileFetcher$1;->val$dataKey:Lcom/samsung/groupcast/core/messaging/DataKey;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/core/messaging/HybridChannel;->cancelAcceptedFile(Lcom/samsung/groupcast/core/messaging/DataKey;)V

    goto :goto_0
.end method
