.class public Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;
.super Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
.source "CollagePageItem.java"


# static fields
.field public static final CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_AGE:Ljava/lang/String; = "KEY_AGE"

.field private static final KEY_SOURCE:Ljava/lang/String; = "KEY_SOURCE"

.field private static final KEY_STYLE:Ljava/lang/String; = "KEY_STYLE"


# instance fields
.field private mAge:I

.field private mSourceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStyle:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$1;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$1;-><init>(I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->CONTENT_ITEM_CREATOR:Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;

    .line 107
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$2;

    invoke-direct {v0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$2;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V
    .locals 3
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "age"    # I
    .param p4, "style"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p5, "src":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 74
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    .line 38
    iput v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    .line 39
    iput v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    .line 75
    iput p3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    .line 76
    iput p4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    .line 77
    if-eqz p5, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 79
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # Ljava/util/ArrayList;
    .param p6, "x5"    # Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem$1;

    .prologue
    .line 14
    invoke-direct/range {p0 .. p5}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/ArrayList;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "src":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 47
    const/16 v0, 0x8

    const-string v1, "collagepage"

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILjava/lang/String;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    .line 38
    iput v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    .line 39
    iput v2, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    .line 48
    if-eqz p1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 50
    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 6
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    const/4 v5, 0x0

    .line 53
    const/16 v3, 0x8

    invoke-direct {p0, v3, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;-><init>(ILorg/json/JSONObject;)V

    .line 37
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x6

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    .line 38
    iput v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    .line 39
    iput v5, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    .line 55
    :try_start_0
    const-string v3, "KEY_AGE"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    .line 56
    const-string v3, "KEY_STYLE"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    .line 58
    const-string v3, "KEY_SOURCE"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 59
    .local v2, "tmpList":Lorg/json/JSONArray;
    if-eqz v2, :cond_0

    .line 60
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 61
    iget-object v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 65
    .end local v1    # "i":I
    .end local v2    # "tmpList":Lorg/json/JSONArray;
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Lorg/json/JSONException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "invalid json used to create CollagePageItem: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 71
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v2    # "tmpList":Lorg/json/JSONArray;
    :cond_0
    return-void
.end method


# virtual methods
.method public getJsonRepresentation()Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 84
    :try_start_0
    invoke-super {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;->getJsonRepresentation()Lorg/json/JSONObject;

    move-result-object v1

    .line 85
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "KEY_AGE"

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 86
    const-string v2, "KEY_STYLE"

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 87
    const-string v2, "KEY_SOURCE"

    new-instance v3, Lorg/json/JSONArray;

    iget-object v4, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    return-object v1

    .line 90
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Lorg/json/JSONException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception encountered while getting Json representation - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getSourcList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 127
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->getContentId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "mStyle"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "mAge"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget v3, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->getContentId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mAge:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mStyle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-object v0, p0, Lcom/samsung/groupcast/legacy/gp2/session/model/CollagePageItem;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 105
    return-void
.end method
