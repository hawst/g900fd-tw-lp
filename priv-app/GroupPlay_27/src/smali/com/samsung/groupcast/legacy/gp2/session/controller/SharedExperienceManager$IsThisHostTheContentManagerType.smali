.class final enum Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;
.super Ljava/lang/Enum;
.source "SharedExperienceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "IsThisHostTheContentManagerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field public static final enum HOST_TYPE_GUEST_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field public static final enum HOST_TYPE_GUEST_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field public static final enum HOST_TYPE_MANAGER_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field public static final enum HOST_TYPE_MANAGER_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

.field public static final enum HOST_TYPE_UNKOWN:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 552
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    const-string v1, "HOST_TYPE_MANAGER_AP"

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_MANAGER_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 553
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    const-string v1, "HOST_TYPE_MANAGER_P2P"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_MANAGER_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 554
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    const-string v1, "HOST_TYPE_GUEST_AP"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_GUEST_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 555
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    const-string v1, "HOST_TYPE_GUEST_P2P"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_GUEST_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 556
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    const-string v1, "HOST_TYPE_UNKOWN"

    invoke-direct {v0, v1, v6}, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_UNKOWN:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    .line 551
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_MANAGER_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_MANAGER_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_GUEST_AP:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_GUEST_P2P:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->HOST_TYPE_UNKOWN:Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->$VALUES:[Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 551
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 551
    const-class v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;
    .locals 1

    .prologue
    .line 551
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->$VALUES:[Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/legacy/gp2/session/controller/SharedExperienceManager$IsThisHostTheContentManagerType;

    return-object v0
.end method
