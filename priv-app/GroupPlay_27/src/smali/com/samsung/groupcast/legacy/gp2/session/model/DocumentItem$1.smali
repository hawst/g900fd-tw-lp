.class final Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem$1;
.super Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;
.source "DocumentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItemCreator;-><init>(I)V

    return-void
.end method


# virtual methods
.method public createContentItemForLocalPath(Ljava/lang/String;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public createContentItemFromJson(Lorg/json/JSONObject;)Lcom/samsung/groupcast/legacy/gp2/session/model/ContentItem;
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;

    invoke-direct {v0, p1}, Lcom/samsung/groupcast/legacy/gp2/session/model/DocumentItem;-><init>(Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public getSupportedFileExtensions()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    sget-object v0, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    return-object v0
.end method
