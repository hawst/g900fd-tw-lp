.class public final Lcom/samsung/groupcast/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_file_images:I = 0x7f080118

.field public static final agree:I = 0x7f080024

.field public static final alert:I = 0x7f080074

.field public static final ap_list:I = 0x7f08002e

.field public static final app_name:I = 0x7f080000

.field public static final app_size:I = 0x7f08006a

.field public static final connect_to_wlan_continue:I = 0x7f080069

.field public static final connect_via_mobile_network:I = 0x7f080065

.field public static final connect_via_mobile_network_wifi:I = 0x7f080067

.field public static final connect_via_mobile_network_wlan:I = 0x7f080066

.field public static final connect_via_wlan:I = 0x7f080068

.field public static final connection_to_ap_failed:I = 0x7f08003a

.field public static final dialog_forced_upgrade_message:I = 0x7f080013

.field public static final disabling_mobile_ap:I = 0x7f080032

.field public static final disagree:I = 0x7f080025

.field public static final disconnected_from_the_network:I = 0x7f080057

.field public static final download_information:I = 0x7f080071

.field public static final enabling_mobile_ap:I = 0x7f080031

.field public static final exit_confirm_message:I = 0x7f080099

.field public static final file_too_large:I = 0x7f080128

.field public static final group_password_is_enabled_you_need_to_enter_a_password:I = 0x7f080051

.field public static final group_password_not_set:I = 0x7f080056

.field public static final group_play_depending_on_network_connection:I = 0x7f08004d

.field public static final group_play_introduction:I = 0x7f080049

.field public static final group_play_master_can_change_screen:I = 0x7f080050

.field public static final group_play_master_selects_music:I = 0x7f08004e

.field public static final group_play_privacy_policy_msg_1:I = 0x7f080085

.field public static final group_play_privacy_policy_msg_2:I = 0x7f080086

.field public static final group_play_privacy_policy_msg_3:I = 0x7f080087

.field public static final group_play_privacy_policy_msg_4:I = 0x7f080088

.field public static final group_play_privacy_policy_msg_5:I = 0x7f080089

.field public static final group_play_set_password:I = 0x7f08004c

.field public static final group_play_set_speaker_automatically:I = 0x7f08004f

.field public static final group_play_terms_and_conditions_msg_1:I = 0x7f08007b

.field public static final group_play_terms_and_conditions_msg_10:I = 0x7f080084

.field public static final group_play_terms_and_conditions_msg_2:I = 0x7f08007c

.field public static final group_play_terms_and_conditions_msg_3:I = 0x7f08007d

.field public static final group_play_terms_and_conditions_msg_4:I = 0x7f08007e

.field public static final group_play_terms_and_conditions_msg_5:I = 0x7f08007f

.field public static final group_play_terms_and_conditions_msg_6:I = 0x7f080080

.field public static final group_play_terms_and_conditions_msg_7:I = 0x7f080081

.field public static final group_play_terms_and_conditions_msg_8:I = 0x7f080082

.field public static final group_play_terms_and_conditions_msg_9:I = 0x7f080083

.field public static final group_play_to_start_new_group_press_button:I = 0x7f08004a

.field public static final group_play_your_device_set_as_ap:I = 0x7f08004b

.field public static final groupplay_desc_permission:I = 0x7f0800b3

.field public static final groupplay_label_permission:I = 0x7f0800b2

.field public static final hardcoded_developer:I = 0x7f0800ad

.field public static final hardcoded_game_title:I = 0x7f0800ae

.field public static final hardcoded_no_title:I = 0x7f0800aa

.field public static final hardcoded_not_implemented:I = 0x7f0800af

.field public static final hardcoded_page_index:I = 0x7f0800ac

.field public static final hardcoded_status:I = 0x7f0800ab

.field public static final help_access_point:I = 0x7f0800a0

.field public static final help_create_group_category:I = 0x7f08012f

.field public static final help_create_group_popup:I = 0x7f08012e

.field public static final help_join_group_category:I = 0x7f080132

.field public static final help_join_group_popup:I = 0x7f080130

.field public static final help_main_screen_invalid_action:I = 0x7f08003b

.field public static final help_main_screen_mls_error:I = 0x7f0800b0

.field public static final help_main_screen_share_video_error:I = 0x7f0800b1

.field public static final help_select_mobile_ap:I = 0x7f080131

.field public static final install:I = 0x7f08006d

.field public static final install_warning:I = 0x7f080076

.field public static final install_warning_kor:I = 0x7f080077

.field public static final installation:I = 0x7f08006e

.field public static final launching_app_failed:I = 0x7f08008f

.field public static final main_banner_share_video:I = 0x7f0800d8

.field public static final main_gallery_not_found_msg:I = 0x7f0800ff

.field public static final main_image:I = 0x7f08008b

.field public static final main_join_timeout:I = 0x7f080015

.field public static final main_joining:I = 0x7f08001b

.field public static final main_myfiles_not_found_msg:I = 0x7f080100

.field public static final main_screen_alphabet_order:I = 0x7f0800d5

.field public static final main_screen_new_arrival:I = 0x7f0800d4

.field public static final main_screen_now_playing:I = 0x7f0800d0

.field public static final main_screen_samsung_apps:I = 0x7f0800d6

.field public static final main_screen_samsung_apps_no_item:I = 0x7f0800d7

.field public static final main_screen_sort_by:I = 0x7f0800d2

.field public static final main_screen_store:I = 0x7f0800d1

.field public static final main_screen_top_rated:I = 0x7f0800d3

.field public static final makecollage:I = 0x7f080098

.field public static final mb:I = 0x7f080070

.field public static final menu_add_files:I = 0x7f080112

.field public static final menu_group_mode:I = 0x7f080054

.field public static final menu_members:I = 0x7f0800bb

.field public static final menu_save_file:I = 0x7f080113

.field public static final message_mobile_data_is_disabled:I = 0x7f080072

.field public static final mini_music_player_show_hide:I = 0x7f080097

.field public static final mobile_ap:I = 0x7f080059

.field public static final mobile_ap_info_dialog_body:I = 0x7f080030

.field public static final near_by_friends_dialog_do_not_show_again:I = 0x7f080104

.field public static final near_by_friends_dialog_dont_you_ever_show_again:I = 0x7f080105

.field public static final near_by_friends_dialog_message_1:I = 0x7f080102

.field public static final near_by_friends_dialog_message_3:I = 0x7f080103

.field public static final near_by_friends_dialog_title:I = 0x7f080101

.field public static final network_is_currently_unavailable:I = 0x7f080075

.field public static final nfc_guide_info_dialog_body:I = 0x7f08002f

.field public static final nfc_network_incompatibility:I = 0x7f080017

.field public static final nfc_not_connected_to_wifi:I = 0x7f080016

.field public static final nickname_input_dialog_msg:I = 0x7f080094

.field public static final no_game:I = 0x7f080127

.field public static final no_market_app:I = 0x7f08008e

.field public static final no_mobile_ap_for_group_play_found:I = 0x7f08002c

.field public static final no_network_connection:I = 0x7f08006c

.field public static final paricipants_info_unknown:I = 0x7f080053

.field public static final password_check_option_on_join:I = 0x7f080041

.field public static final password_input_description:I = 0x7f080042

.field public static final password_input_title:I = 0x7f080040

.field public static final password_input_title_on_join:I = 0x7f080043

.field public static final password_inputfield_title_on_join:I = 0x7f080044

.field public static final password_invalid_on_join:I = 0x7f080045

.field public static final play_games_after_you_download_games_via_download_tab:I = 0x7f080060

.field public static final play_games_after_you_download_games_via_more_tab:I = 0x7f080061

.field public static final play_games_go_to_download:I = 0x7f08005f

.field public static final play_games_updating:I = 0x7f08001a

.field public static final play_games_you_can_download:I = 0x7f0800ba

.field public static final poor_connection_ap_guide_description:I = 0x7f08008a

.field public static final private_mode:I = 0x7f080129

.field public static final profile_current_wifi_info_header:I = 0x7f08008c

.field public static final profile_group_name_should_be_presented:I = 0x7f08008d

.field public static final quick_panel_device_connected:I = 0x7f08003d

.field public static final quick_panel_devices_connected:I = 0x7f08003e

.field public static final quick_panel_join_group_play:I = 0x7f080052

.field public static final quick_panel_set_mobile_ap:I = 0x7f08003c

.field public static final remote_connection_lost:I = 0x7f0800c9

.field public static final remote_disconnect_all:I = 0x7f0800c0

.field public static final remote_do_you_want_disconnect_all:I = 0x7f0800c7

.field public static final remote_do_you_want_disconnect_with:I = 0x7f0800c6

.field public static final remote_do_you_want_to_join:I = 0x7f0800c5

.field public static final remote_friends_invited:I = 0x7f0800be

.field public static final remote_friends_invited_and_joined:I = 0x7f0800bf

.field public static final remote_group_play_invitation:I = 0x7f0800c4

.field public static final remote_group_play_is_closed:I = 0x7f0800cb

.field public static final remote_invitation_title:I = 0x7f0800bd

.field public static final remote_is_disconnected:I = 0x7f0800c8

.field public static final remote_voice_chat_connected:I = 0x7f0800c2

.field public static final remote_voice_chat_disconnected:I = 0x7f0800c3

.field public static final remote_waiting_or_acceptance:I = 0x7f0800c1

.field public static final remote_your_device_has_been_disconnected:I = 0x7f0800ca

.field public static final samsung_account:I = 0x7f080092

.field public static final samsungapps_url:I = 0x7f08006b

.field public static final save_all_collages:I = 0x7f08010b

.field public static final save_completed_toast_message:I = 0x7f08010e

.field public static final save_document_page_file:I = 0x7f08009c

.field public static final save_file_all:I = 0x7f08010c

.field public static final save_file_all_images:I = 0x7f080111

.field public static final save_file_collage_with_drawing:I = 0x7f0800e9

.field public static final save_file_current_images:I = 0x7f08010f

.field public static final save_file_dir:I = 0x7f08009a

.field public static final save_file_image_with_drawing:I = 0x7f080110

.field public static final save_image_file:I = 0x7f08009b

.field public static final save_this_collage_only:I = 0x7f08010a

.field public static final saving:I = 0x7f08010d

.field public static final searching_session:I = 0x7f080033

.field public static final selecting_error:I = 0x7f080096

.field public static final selecting_information:I = 0x7f080095

.field public static final session_creator_create_session_failure:I = 0x7f080018

.field public static final session_found:I = 0x7f080034

.field public static final set_your_nickname:I = 0x7f080093

.field public static final setting_open_source_license_title:I = 0x7f080079

.field public static final settings_about_groupplay:I = 0x7f0800e5

.field public static final settings_about_groupplay_faq:I = 0x7f0800e6

.field public static final settings_about_groupplay_notice:I = 0x7f0800e7

.field public static final settings_about_groupplay_version:I = 0x7f0800e8

.field public static final settings_allow_invitation:I = 0x7f0800dd

.field public static final settings_edit:I = 0x7f0800e4

.field public static final settings_edit_nickname:I = 0x7f0800db

.field public static final settings_enter_your_nickname:I = 0x7f0800dc

.field public static final settings_nearby_friend:I = 0x7f0800de

.field public static final settings_nearby_friend_disabled:I = 0x7f0800e0

.field public static final settings_nearby_friend_enabled:I = 0x7f0800df

.field public static final settings_nickname:I = 0x7f0800da

.field public static final settings_remote_friend:I = 0x7f0800e1

.field public static final settings_remote_friend_disabled:I = 0x7f0800e3

.field public static final settings_remote_friend_enabled:I = 0x7f0800e2

.field public static final settings_title:I = 0x7f0800d9

.field public static final size:I = 0x7f08006f

.field public static final skip:I = 0x7f080091

.field public static final start_create_button:I = 0x7f080078

.field public static final start_make_collage:I = 0x7f080120

.field public static final start_play_games:I = 0x7f080121

.field public static final start_play_games_and_more:I = 0x7f080122

.field public static final start_scan:I = 0x7f08001c

.field public static final start_screen_create_group:I = 0x7f080029

.field public static final start_screen_description_target:I = 0x7f080028

.field public static final start_screen_join:I = 0x7f08002a

.field public static final start_screen_notify_nearby:I = 0x7f0800cc

.field public static final start_text_welcome:I = 0x7f080055

.field public static final starting_share_contents_doc:I = 0x7f080126

.field public static final starting_share_contents_snote:I = 0x7f080117

.field public static final stop_scan:I = 0x7f08001d

.field public static final tablet_enter:I = 0x7f0800a5

.field public static final tablet_gototab_documents:I = 0x7f0800a8

.field public static final tablet_gototab_music:I = 0x7f0800a6

.field public static final tablet_gototab_pictures:I = 0x7f0800a7

.field public static final tablet_gototab_playgames:I = 0x7f0800a9

.field public static final tablet_main_description:I = 0x7f080064

.field public static final tablet_no_device:I = 0x7f0800b4

.field public static final tablet_no_documents:I = 0x7f0800b7

.field public static final tablet_no_games:I = 0x7f0800b8

.field public static final tablet_no_music:I = 0x7f0800b5

.field public static final tablet_no_pictures:I = 0x7f0800b6

.field public static final tag_action_bar_icon_add_files:I = 0x7f08011d

.field public static final tag_action_bar_icon_eraser_settings:I = 0x7f08011f

.field public static final tag_action_bar_icon_pen_mode:I = 0x7f08011b

.field public static final tag_action_bar_icon_view_mode:I = 0x7f08011a

.field public static final tag_app_name_gc:I = 0x7f080002

.field public static final tag_app_name_groupplay_englishOnly:I = 0x7f080001

.field public static final tag_can_not_enable_mobile_ap_airplane_mode:I = 0x7f080046

.field public static final tag_cancel:I = 0x7f08000f

.field public static final tag_close:I = 0x7f080014

.field public static final tag_confirm:I = 0x7f080026

.field public static final tag_dialog_question_delete:I = 0x7f080125

.field public static final tag_disclaimer_cant_access_internet:I = 0x7f080036

.field public static final tag_disclaimer_data_collected:I = 0x7f0800b9

.field public static final tag_disclaimer_do_not_show_for_serveral_days:I = 0x7f08000d

.field public static final tag_disclaimer_hotspot_expend_more_battery_message:I = 0x7f08002b

.field public static final tag_disclaimer_legal_resonsibility_message:I = 0x7f080038

.field public static final tag_disclaimer_title:I = 0x7f080027

.field public static final tag_document_processing:I = 0x7f080115

.field public static final tag_done:I = 0x7f080012

.field public static final tag_download:I = 0x7f08005a

.field public static final tag_download_help_description:I = 0x7f080062

.field public static final tag_drm_document:I = 0x7f080114

.field public static final tag_exit_close:I = 0x7f08003f

.field public static final tag_failed_to_join_share_documents:I = 0x7f08012d

.field public static final tag_failed_to_join_share_images:I = 0x7f08012c

.field public static final tag_flight_mode_create_group:I = 0x7f080048

.field public static final tag_flight_mode_join_group:I = 0x7f080047

.field public static final tag_join_incompatible_session:I = 0x7f080005

.field public static final tag_main_new_app_download_msg:I = 0x7f0800ce

.field public static final tag_main_new_app_download_title:I = 0x7f0800cd

.field public static final tag_may_result_in_additional_charges:I = 0x7f0800cf

.field public static final tag_menu_icon_help_title:I = 0x7f080023

.field public static final tag_menu_icon_remove_title:I = 0x7f080119

.field public static final tag_menu_item_info:I = 0x7f080003

.field public static final tag_menu_item_thumb:I = 0x7f080004

.field public static final tag_menu_pen_settings:I = 0x7f08011e

.field public static final tag_more:I = 0x7f08005b

.field public static final tag_new_participant:I = 0x7f08002d

.field public static final tag_new_participant_joined_to:I = 0x7f08009f

.field public static final tag_next:I = 0x7f08005d

.field public static final tag_no:I = 0x7f080011

.field public static final tag_no_session_available:I = 0x7f080039

.field public static final tag_ok:I = 0x7f08000e

.field public static final tag_participant_left:I = 0x7f08009d

.field public static final tag_participant_left_from:I = 0x7f08009e

.field public static final tag_participants_connected_devices:I = 0x7f080058

.field public static final tag_play_game_installed_games:I = 0x7f080123

.field public static final tag_play_game_more_games:I = 0x7f080124

.field public static final tag_presentation_security_advice:I = 0x7f08000b

.field public static final tag_presentation_security_advice_title:I = 0x7f08000a

.field public static final tag_presentation_security_dont_show_again:I = 0x7f08000c

.field public static final tag_presentation_status_connected_to:I = 0x7f080006

.field public static final tag_presentation_status_connecting:I = 0x7f080009

.field public static final tag_presentation_status_connecting_failed:I = 0x7f080008

.field public static final tag_presentation_status_download_progress:I = 0x7f080007

.field public static final tag_previous:I = 0x7f08005c

.field public static final tag_remote_connection_failed_toast:I = 0x7f0800fd

.field public static final tag_remote_connection_prepare:I = 0x7f0800fc

.field public static final tag_remote_connection_title:I = 0x7f0800fb

.field public static final tag_remote_connection_voip_failed_toast:I = 0x7f0800fe

.field public static final tag_remote_disconnect_all_confirm:I = 0x7f0800f4

.field public static final tag_remote_disconnect_all_toast:I = 0x7f0800f5

.field public static final tag_remote_invitation_dlg_text:I = 0x7f0800ec

.field public static final tag_remote_invitation_dlg_title:I = 0x7f0800eb

.field public static final tag_remote_invitation_need_mobile_network:I = 0x7f0800f3

.field public static final tag_remote_invitation_progress_dlg_title:I = 0x7f0800ed

.field public static final tag_remote_invitation_sa_request_dlg_text:I = 0x7f0800ef

.field public static final tag_remote_invitation_sa_request_dlg_title:I = 0x7f0800ee

.field public static final tag_remote_invitation_sms_not_sent:I = 0x7f0800f1

.field public static final tag_remote_invitation_sms_sent:I = 0x7f0800f0

.field public static final tag_remote_invitation_sms_sent_not_sent:I = 0x7f0800f2

.field public static final tag_remote_voip_declined_toast:I = 0x7f0800f8

.field public static final tag_remote_voip_end_toast:I = 0x7f0800f7

.field public static final tag_remote_voip_invitation_message:I = 0x7f0800fa

.field public static final tag_remote_voip_invitation_title:I = 0x7f0800f9

.field public static final tag_remote_voip_start_toast:I = 0x7f0800f6

.field public static final tag_room_not_destroyed:I = 0x7f0800ea

.field public static final tag_scanning_ing:I = 0x7f08001e

.field public static final tag_start_share_separator:I = 0x7f08005e

.field public static final tag_starting_group_camcorder:I = 0x7f0800bc

.field public static final tag_starting_share_doc:I = 0x7f080108

.field public static final tag_starting_share_image:I = 0x7f080107

.field public static final tag_starting_share_music:I = 0x7f080106

.field public static final tag_starting_share_snote:I = 0x7f080116

.field public static final tag_starting_share_video:I = 0x7f080109

.field public static final tag_stop_being_shared_documents:I = 0x7f08012b

.field public static final tag_stop_being_shared_images:I = 0x7f08012a

.field public static final tag_unable_to_stop_session:I = 0x7f080063

.field public static final tag_viewer_pen_toggle_title:I = 0x7f08011c

.field public static final tag_yes:I = 0x7f080010

.field public static final tap_here_to_download_group_play:I = 0x7f08007a

.field public static final to_start_new_press_button:I = 0x7f080090

.field public static final unavailable_update:I = 0x7f080073

.field public static final viewer_content_downloading_determinate:I = 0x7f080021

.field public static final viewer_content_downloading_indeterminate:I = 0x7f080020

.field public static final viewer_content_loading:I = 0x7f080022

.field public static final viewer_content_searching:I = 0x7f08001f

.field public static final viewer_session_no_longer_available:I = 0x7f080019

.field public static final wifi_authenticating:I = 0x7f080037

.field public static final wifi_connecting:I = 0x7f0800a1

.field public static final wifi_connecting_dots:I = 0x7f0800a2

.field public static final wifi_enabling:I = 0x7f0800a3

.field public static final wifi_enabling_waiting:I = 0x7f0800a4

.field public static final wifi_lost:I = 0x7f080035


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
