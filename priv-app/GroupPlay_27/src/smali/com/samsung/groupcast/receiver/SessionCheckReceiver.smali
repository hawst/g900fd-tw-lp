.class public Lcom/samsung/groupcast/receiver/SessionCheckReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SessionCheckReceiver.java"


# instance fields
.field private final IS_GROUP_PLAY_RUNNING:Ljava/lang/String;

.field private final SESSION_CHECK_ACTION:Ljava/lang/String;

.field private final SESSION_CHECK_REPONSE_ACTION:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    const-string v0, "IS_GROUP_PLAY_RUNNING"

    iput-object v0, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->IS_GROUP_PLAY_RUNNING:Ljava/lang/String;

    .line 13
    const-string v0, "com.samsung.groupcast.action.GROUP_PLAY_RUNNING_CHECK"

    iput-object v0, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->SESSION_CHECK_ACTION:Ljava/lang/String;

    .line 15
    const-string v0, "com.samsung.screenmirroring.action.GROUP_PLAY_RUNNING_CHECK_RESPONSE"

    iput-object v0, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->SESSION_CHECK_REPONSE_ACTION:Ljava/lang/String;

    .line 17
    const-class v0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 22
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->TAG:Ljava/lang/String;

    const-string v2, "no intent or not have action"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    :cond_1
    :goto_0
    return-void

    .line 26
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.samsung.groupcast.action.GROUP_PLAY_RUNNING_CHECK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    iget-object v1, p0, Lcom/samsung/groupcast/receiver/SessionCheckReceiver;->TAG:Ljava/lang/String;

    const-string v2, "received session check action"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 30
    .local v0, "newIntent":Landroid/content/Intent;
    const-string v1, "com.samsung.screenmirroring.action.GROUP_PLAY_RUNNING_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    const-string v1, "IS_GROUP_PLAY_RUNNING"

    invoke-static {}, Lcom/samsung/groupcast/legacy/gp2/session/model/Session;->HasSession()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
