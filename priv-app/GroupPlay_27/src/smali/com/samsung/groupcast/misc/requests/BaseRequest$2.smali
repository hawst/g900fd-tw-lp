.class Lcom/samsung/groupcast/misc/requests/BaseRequest$2;
.super Ljava/lang/Object;
.source "BaseRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/misc/requests/BaseRequest;->complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

.field final synthetic val$notifier:Lcom/samsung/groupcast/misc/requests/Notifier;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/misc/requests/BaseRequest;Lcom/samsung/groupcast/misc/requests/Notifier;)V
    .locals 0

    .prologue
    .line 166
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest$2;, "Lcom/samsung/groupcast/misc/requests/BaseRequest.2;"
    iput-object p1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    iput-object p2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->val$notifier:Lcom/samsung/groupcast/misc/requests/Notifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 169
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest$2;, "Lcom/samsung/groupcast/misc/requests/BaseRequest.2;"
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$000(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 170
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z
    invoke-static {v2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$100(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    monitor-exit v3

    .line 180
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$200(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/misc/requests/RequestListener;

    .line 175
    .local v1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->val$notifier:Lcom/samsung/groupcast/misc/requests/Notifier;

    iget-object v4, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    invoke-interface {v2, v4, v1}, Lcom/samsung/groupcast/misc/requests/Notifier;->notifyListener(Lcom/samsung/groupcast/misc/requests/Request;Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    goto :goto_1

    .line 179
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 178
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$200(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 179
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
