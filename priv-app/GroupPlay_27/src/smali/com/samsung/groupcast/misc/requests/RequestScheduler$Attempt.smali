.class Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
.super Ljava/lang/Object;
.source "RequestScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/misc/requests/RequestScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Attempt"
.end annotation


# instance fields
.field private mAttemptCount:I

.field private mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

.field private final mIntervalMilliseconds:J

.field private mInvalidated:Z

.field private final mMaxAttemptCount:I

.field private mRequest:Lcom/samsung/groupcast/misc/requests/Request;

.field final synthetic this$0:Lcom/samsung/groupcast/misc/requests/RequestScheduler;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/misc/requests/RequestScheduler;Lcom/samsung/groupcast/misc/requests/Request;JILcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V
    .locals 0
    .param p2, "request"    # Lcom/samsung/groupcast/misc/requests/Request;
    .param p3, "intervalMilliseconds"    # J
    .param p5, "maxAttemptCount"    # I
    .param p6, "delegate"    # Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->this$0:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p2, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    .line 59
    iput-object p6, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    .line 60
    iput-wide p3, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mIntervalMilliseconds:J

    .line 61
    iput p5, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mMaxAttemptCount:I

    .line 62
    return-void
.end method


# virtual methods
.method public invalidate()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mInvalidated:Z

    if-nez v0, :cond_0

    .line 66
    iput-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    .line 67
    iput-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    .line 70
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mInvalidated:Z

    .line 71
    invoke-static {p0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 72
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 76
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mInvalidated:Z

    if-eqz v0, :cond_0

    .line 104
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-interface {v0}, Lcom/samsung/groupcast/misc/requests/Request;->isCompleted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-interface {v0}, Lcom/samsung/groupcast/misc/requests/Request;->isInvalidated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "run"

    const-string v2, "request completed or invalidated"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "attemptCount"

    aput-object v4, v3, v6

    iget v4, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const-string v4, "request"

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    aput-object v4, v3, v9

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->this$0:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    # getter for: Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->access$000(Lcom/samsung/groupcast/misc/requests/RequestScheduler;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    iput-object v5, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    .line 85
    iput-object v5, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    goto :goto_0

    .line 89
    :cond_2
    iget v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    iget v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mMaxAttemptCount:I

    if-ne v0, v1, :cond_3

    .line 90
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "run"

    const-string v2, "timeout"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "attemptCount"

    aput-object v4, v3, v6

    iget v4, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const-string v4, "request"

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    aput-object v4, v3, v9

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;->onTimeout(Lcom/samsung/groupcast/misc/requests/Request;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->this$0:Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    # getter for: Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->access$000(Lcom/samsung/groupcast/misc/requests/RequestScheduler;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iput-object v5, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    .line 95
    iput-object v5, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    goto/16 :goto_0

    .line 99
    :cond_3
    iget v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    .line 100
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mDelegate:Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;->onAttempt(Lcom/samsung/groupcast/misc/requests/Request;)V

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "run"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "attemptCount"

    aput-object v3, v2, v6

    iget v3, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mAttemptCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "maxAttemptCount"

    aput-object v3, v2, v8

    iget v3, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mMaxAttemptCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x4

    const-string v4, "request"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mRequest:Lcom/samsung/groupcast/misc/requests/Request;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v5, v2}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 103
    iget-wide v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->mIntervalMilliseconds:J

    invoke-static {p0, v0, v1}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    goto/16 :goto_0
.end method
