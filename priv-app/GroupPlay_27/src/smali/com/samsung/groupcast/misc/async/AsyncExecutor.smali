.class public Lcom/samsung/groupcast/misc/async/AsyncExecutor;
.super Landroid/os/AsyncTask;
.source "AsyncExecutor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AsyncExecutor"


# instance fields
.field private callable:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<TT;>;"
        }
    .end annotation
.end field

.field private callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/groupcast/misc/async/AsyncCallBack",
            "<TT;>;"
        }
    .end annotation
.end field

.field private occuredException:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private isExceptionOccured()Z
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->occuredException:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private notifyCancled()V
    .locals 1

    .prologue
    .line 65
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callable:Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;

    invoke-interface {v0}, Lcom/samsung/groupcast/misc/async/AsyncCallBack;->canclled()V

    .line 67
    :cond_0
    return-void
.end method

.method private notifyException()V
    .locals 2

    .prologue
    .line 59
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callable:Ljava/util/concurrent/Callable;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->occuredException:Ljava/lang/Exception;

    invoke-interface {v0, v1}, Lcom/samsung/groupcast/misc/async/AsyncCallBack;->exceptionOccured(Ljava/lang/Exception;)V

    .line 62
    :cond_0
    return-void
.end method

.method private notifyResult(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    .local p1, "result":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;

    invoke-interface {v0, p1}, Lcom/samsung/groupcast/misc/async/AsyncCallBack;->onResult(Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void
.end method

.method private processAsyncExecutorAware(Lcom/samsung/groupcast/misc/async/AsyncCallBack;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/groupcast/misc/async/AsyncCallBack",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    .local p1, "callback":Lcom/samsung/groupcast/misc/async/AsyncCallBack;, "Lcom/samsung/groupcast/misc/async/AsyncCallBack<TT;>;"
    instance-of v0, p1, Lcom/samsung/groupcast/misc/async/AsyncExecutorAware;

    if-eqz v0, :cond_0

    .line 30
    check-cast p1, Lcom/samsung/groupcast/misc/async/AsyncExecutorAware;

    .end local p1    # "callback":Lcom/samsung/groupcast/misc/async/AsyncCallBack;, "Lcom/samsung/groupcast/misc/async/AsyncCallBack<TT;>;"
    invoke-interface {p1, p0}, Lcom/samsung/groupcast/misc/async/AsyncExecutorAware;->setAsyncExecutor(Lcom/samsung/groupcast/misc/async/AsyncExecutor;)V

    .line 32
    :cond_0
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 9
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->doInBackground([Ljava/lang/Void;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Object;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "AsyncExecutor"

    invoke-virtual {v1, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 38
    iget-object v1, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callable:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 42
    :goto_0
    return-object v1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "AsyncExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception occured with doing in background :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iput-object v0, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->occuredException:Ljava/lang/Exception;

    .line 42
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    .local p1, "result":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->notifyCancled()V

    .line 51
    :cond_0
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->isExceptionOccured()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->notifyException()V

    .line 56
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->notifyResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setCallable(Ljava/util/concurrent/Callable;)Lcom/samsung/groupcast/misc/async/AsyncExecutor;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Lcom/samsung/groupcast/misc/async/AsyncExecutor",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    iput-object p1, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callable:Ljava/util/concurrent/Callable;

    .line 18
    return-object p0
.end method

.method public setCallback(Lcom/samsung/groupcast/misc/async/AsyncCallBack;)Lcom/samsung/groupcast/misc/async/AsyncExecutor;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/groupcast/misc/async/AsyncCallBack",
            "<TT;>;)",
            "Lcom/samsung/groupcast/misc/async/AsyncExecutor",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/samsung/groupcast/misc/async/AsyncExecutor;, "Lcom/samsung/groupcast/misc/async/AsyncExecutor<TT;>;"
    .local p1, "callback":Lcom/samsung/groupcast/misc/async/AsyncCallBack;, "Lcom/samsung/groupcast/misc/async/AsyncCallBack<TT;>;"
    iput-object p1, p0, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->callback:Lcom/samsung/groupcast/misc/async/AsyncCallBack;

    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/misc/async/AsyncExecutor;->processAsyncExecutorAware(Lcom/samsung/groupcast/misc/async/AsyncCallBack;)V

    .line 24
    return-object p0
.end method
