.class Lcom/samsung/groupcast/misc/utility/DecodedGif$1;
.super Ljava/lang/Object;
.source "DecodedGif.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/misc/utility/DecodedGif;->runInnerThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/misc/utility/DecodedGif;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    # getter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$000(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Landroid/graphics/Movie;

    move-result-object v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->load()Z

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    # getter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$100(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    # getter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$100(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    invoke-interface {v0, v1}, Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;->onNextFrame(Lcom/samsung/groupcast/misc/utility/DecodedGif;)V

    .line 59
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    # getter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$200(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    # getter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$200(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Ljava/lang/Runnable;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    invoke-virtual {v1}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->getDelay()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 66
    :cond_1
    :goto_0
    return-void

    .line 63
    :cond_2
    const-string v0, "nulling mGifAnimatorRunnable"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;->this$0:Lcom/samsung/groupcast/misc/utility/DecodedGif;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/DecodedGif;->access$202(Lcom/samsung/groupcast/misc/utility/DecodedGif;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    goto :goto_0
.end method
