.class public Lcom/samsung/groupcast/misc/utility/BooleanTool;
.super Ljava/lang/Object;
.source "BooleanTool.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static booleanToInt(Z)I
    .locals 1
    .param p0, "val"    # Z

    .prologue
    .line 8
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static intToBoolean(I)Z
    .locals 1
    .param p0, "val"    # I

    .prologue
    .line 17
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
