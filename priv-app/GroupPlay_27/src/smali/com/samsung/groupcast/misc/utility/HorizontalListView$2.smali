.class Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;
.super Landroid/database/DataSetObserver;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/misc/utility/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 124
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    monitor-enter v1

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataChanged:Z
    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$002(Lcom/samsung/groupcast/misc/utility/HorizontalListView;Z)Z

    .line 126
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getEmptyView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setEmptyView(Landroid/view/View;)V

    .line 128
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->invalidate()V

    .line 129
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 130
    return-void

    .line 126
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # invokes: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->reset()V
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$100(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V

    .line 135
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->invalidate()V

    .line 136
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 137
    return-void
.end method
