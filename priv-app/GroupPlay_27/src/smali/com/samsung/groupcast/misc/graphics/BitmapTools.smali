.class public Lcom/samsung/groupcast/misc/graphics/BitmapTools;
.super Ljava/lang/Object;
.source "BitmapTools.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 5
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "targetWidth"    # I
    .param p2, "targetHeight"    # I

    .prologue
    .line 86
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 87
    .local v0, "height":I
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 88
    .local v2, "width":I
    const/4 v1, 0x1

    .line 90
    .local v1, "inSampleSize":I
    if-gt v0, p2, :cond_0

    if-le v2, p1, :cond_1

    .line 91
    :cond_0
    if-le v2, v0, :cond_2

    .line 92
    int-to-float v3, v2

    int-to-float v4, p1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 98
    :cond_1
    :goto_0
    return v1

    .line 94
    :cond_2
    int-to-float v3, v0

    int-to-float v4, p2

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0
.end method

.method public static decodeBitmapFromBitmap(Landroid/graphics/Bitmap;II)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .locals 10
    .param p0, "source"    # Landroid/graphics/Bitmap;
    .param p1, "targetWidth"    # I
    .param p2, "targetHeight"    # I

    .prologue
    const/4 v9, 0x1

    .line 69
    const/4 v0, 0x0

    .line 71
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, p1, p2, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 73
    if-nez v0, :cond_0

    .line 74
    const-class v3, Lcom/samsung/groupcast/misc/graphics/BitmapTools;

    const-string v4, "decodeBitmapFromBitmap"

    const-string v5, "decoding failed"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "bitmap"

    aput-object v8, v6, v7

    aput-object p0, v6, v9

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 76
    const/4 v2, 0x0

    .line 81
    :goto_0
    return-object v2

    .line 79
    :cond_0
    new-instance v1, Lcom/samsung/groupcast/misc/graphics/ExifData;

    invoke-direct {v1, v9}, Lcom/samsung/groupcast/misc/graphics/ExifData;-><init>(I)V

    .line 80
    .local v1, "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    new-instance v2, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;

    invoke-direct {v2, v0, v1}, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;-><init>(Landroid/graphics/Bitmap;Lcom/samsung/groupcast/misc/graphics/ExifData;)V

    .line 81
    .local v2, "result":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    goto :goto_0
.end method

.method public static decodeBitmapFromPath(Ljava/lang/String;II)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .locals 17
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "targetWidth"    # I
    .param p2, "targetHeight"    # I

    .prologue
    .line 19
    const/4 v2, 0x0

    .line 21
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .line 22
    .local v6, "fis":Ljava/io/FileInputStream;
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 23
    .local v9, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v11, 0x1

    iput-boolean v11, v9, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 25
    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 26
    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v9, v0, v1}, Lcom/samsung/groupcast/misc/graphics/BitmapTools;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v11

    iput v11, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 27
    const/4 v11, 0x0

    iput-boolean v11, v9, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 28
    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v11, v9, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 29
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 30
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    .line 31
    .local v5, "fd":Ljava/io/FileDescriptor;
    const/4 v8, 0x0

    .local v8, "iTry":I
    :goto_0
    const/4 v11, 0x4

    if-ge v8, v11, :cond_0

    .line 32
    const/4 v11, 0x0

    invoke-static {v5, v11, v9}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 33
    if-eqz v2, :cond_3

    .line 45
    :cond_0
    if-eqz v7, :cond_1

    .line 47
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 54
    :cond_1
    :goto_1
    const-class v11, Lcom/samsung/groupcast/misc/graphics/BitmapTools;

    const-string v12, "decodeBitmapFromPath"

    const-string v13, "decoding end"

    const/16 v14, 0x8

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "path"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object p0, v14, v15

    const/4 v15, 0x2

    const-string v16, "options.inSampleSize"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    iget v0, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    const-string v16, "height"

    aput-object v16, v14, v15

    const/4 v15, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x6

    const-string v16, "width"

    aput-object v16, v14, v15

    const/4 v15, 0x7

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 58
    if-nez v2, :cond_5

    .line 59
    const-class v11, Lcom/samsung/groupcast/misc/graphics/BitmapTools;

    const-string v12, "decodeBitmapFromPath"

    const-string v13, "decoding failed"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "path"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object p0, v14, v15

    invoke-static {v11, v12, v13, v14}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 60
    const/4 v10, 0x0

    move-object v6, v7

    .line 65
    .end local v5    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "iTry":I
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :goto_2
    return-object v10

    .line 36
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fd":Ljava/io/FileDescriptor;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "iTry":I
    :cond_3
    :try_start_3
    iget v11, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    mul-int/lit8 v11, v11, 0x2

    iput v11, v9, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 31
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 48
    :catch_0
    move-exception v3

    .line 49
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 38
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "iTry":I
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 39
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :goto_3
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 40
    const/4 v10, 0x0

    .line 45
    if-eqz v6, :cond_2

    .line 47
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 48
    :catch_2
    move-exception v3

    .line 49
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 41
    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 42
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_6
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 43
    const/4 v10, 0x0

    .line 45
    if-eqz v6, :cond_2

    .line 47
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 48
    :catch_4
    move-exception v3

    .line 49
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 45
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_5
    if-eqz v6, :cond_4

    .line 47
    :try_start_8
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 50
    :cond_4
    :goto_6
    throw v11

    .line 48
    :catch_5
    move-exception v3

    .line 49
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 63
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fd":Ljava/io/FileDescriptor;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "iTry":I
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/BitmapTools;->getExifData(Ljava/lang/String;)Lcom/samsung/groupcast/misc/graphics/ExifData;

    move-result-object v4

    .line 64
    .local v4, "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    new-instance v10, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;

    invoke-direct {v10, v2, v4}, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;-><init>(Landroid/graphics/Bitmap;Lcom/samsung/groupcast/misc/graphics/ExifData;)V

    .local v10, "result":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    move-object v6, v7

    .line 65
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 45
    .end local v4    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .end local v5    # "fd":Ljava/io/FileDescriptor;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "iTry":I
    .end local v10    # "result":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v11

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 41
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 38
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v3

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private static getExifData(Ljava/lang/String;)Lcom/samsung/groupcast/misc/graphics/ExifData;
    .locals 11
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 102
    const/4 v1, 0x0

    .line 105
    .local v1, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    .end local v1    # "exif":Landroid/media/ExifInterface;
    .local v2, "exif":Landroid/media/ExifInterface;
    const-string v4, "Orientation"

    invoke-virtual {v2, v4, v10}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    .line 116
    .local v3, "orientation":I
    new-instance v4, Lcom/samsung/groupcast/misc/graphics/ExifData;

    invoke-direct {v4, v3}, Lcom/samsung/groupcast/misc/graphics/ExifData;-><init>(I)V

    move-object v1, v2

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .end local v3    # "orientation":I
    .restart local v1    # "exif":Landroid/media/ExifInterface;
    :goto_0
    return-object v4

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    const-class v4, Lcom/samsung/groupcast/misc/graphics/BitmapTools;

    const-string v5, "getImageFileRotation"

    const-string v6, "exception while getting EXIF data"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "path"

    aput-object v9, v7, v8

    aput-object p0, v7, v10

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 109
    const/4 v4, 0x0

    goto :goto_0
.end method
