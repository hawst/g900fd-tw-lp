.class public Lcom/samsung/groupcast/misc/utility/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"


# instance fields
.field private mHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/Handler;",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getListenersCount()I
    .locals 2

    .prologue
    .line 133
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    monitor-enter v1

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMyObject(Landroid/os/Handler;)Ljava/lang/Object;
    .locals 4
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    monitor-enter v3

    .line 51
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/util/Pair;

    move-object v1, v0

    .line 52
    monitor-exit v3

    .line 54
    if-nez v1, :cond_0

    .line 55
    const/4 v2, 0x0

    .line 58
    :goto_0
    return-object v2

    .line 52
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 58
    :cond_0
    iget-object v2, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    goto :goto_0
.end method

.method public registerListener(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 39
    const/4 v0, 0x0

    check-cast v0, Landroid/util/Pair;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Landroid/util/Pair;)V

    .line 40
    return-void
.end method

.method public registerListener(Landroid/os/Handler;Landroid/util/Pair;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    monitor-enter v1

    .line 22
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    monitor-exit v1

    .line 24
    return-void

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;
    .param p3, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Landroid/util/Pair;)V

    .line 28
    return-void
.end method

.method public varargs registerListener(Landroid/os/Handler;[I)V
    .locals 6
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "what"    # [I

    .prologue
    .line 31
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 32
    .local v4, "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, v0, v1

    .line 33
    .local v3, "w":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    .end local v3    # "w":I
    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;Ljava/util/Set;Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public sendMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(II)V

    .line 130
    return-void
.end method

.method public sendMessage(II)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 122
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 123
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 124
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 125
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(Landroid/os/Message;)V

    .line 126
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 114
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 115
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 116
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 118
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(Landroid/os/Message;)V

    .line 119
    return-void
.end method

.method public sendMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 62
    const/4 v4, 0x0

    .line 64
    .local v4, "messagessSent":I
    const-string v9, "---"

    invoke-static {v9}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 65
    iget-object v10, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    monitor-enter v10

    .line 66
    :try_start_0
    iget-object v9, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 69
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/Handler;Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 70
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v5

    .line 72
    .local v5, "newMessage":Landroid/os/Message;
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 73
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/Handler;Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    .line 74
    .local v2, "handler":Landroid/os/Handler;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/Pair;

    .line 75
    .local v7, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;"
    const/4 v8, 0x0

    .line 76
    .local v8, "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    .line 78
    .local v6, "obj":Ljava/lang/Object;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Sending message to: "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 80
    if-eqz v7, :cond_1

    .line 81
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    iget-object v8, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v8    # "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    check-cast v8, Ljava/util/Set;

    .line 82
    .restart local v8    # "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    iget-object v6, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 84
    .end local v6    # "obj":Ljava/lang/Object;
    :cond_1
    if-eqz v6, :cond_2

    .line 85
    iput-object v6, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 88
    :cond_2
    if-eqz v8, :cond_3

    iget v9, v5, Landroid/os/Message;->what:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    if-eqz v9, :cond_0

    .line 93
    :cond_3
    :try_start_1
    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 94
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/os/Handler;Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;>;"
    .end local v2    # "handler":Landroid/os/Handler;
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/Handler;Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;>;>;"
    .end local v5    # "newMessage":Landroid/os/Message;
    .end local v7    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;"
    .end local v8    # "whatSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v9

    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Landroid/os/Handler;Landroid/util/Pair<Ljava/util/Set<Ljava/lang/Integer;>;Ljava/lang/Object;>;>;>;"
    :cond_5
    :try_start_3
    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 104
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "=== messagessSent: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public sendMessage(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 108
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 109
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 110
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(Landroid/os/Message;)V

    .line 111
    return-void
.end method

.method public unregisterListener(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 43
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    monitor-enter v1

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/EventHandler;->mHandlers:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    monitor-exit v1

    .line 46
    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
