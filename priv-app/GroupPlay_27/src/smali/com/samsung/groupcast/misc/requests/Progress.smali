.class public Lcom/samsung/groupcast/misc/requests/Progress;
.super Ljava/lang/Object;
.source "Progress.java"


# static fields
.field public static final TYPE_COMPLETED:Ljava/lang/String; = "COMPLETED"

.field public static final TYPE_DOWNLOADING:Ljava/lang/String; = "DOWNLOADING"

.field public static final TYPE_FAILURE:Ljava/lang/String; = "FAILED"

.field public static final TYPE_LOADING:Ljava/lang/String; = "LOADING"

.field public static final TYPE_NONE:Ljava/lang/String; = "NONE"

.field public static final TYPE_SEARCHING:Ljava/lang/String; = "SEARCHING"


# instance fields
.field private final mCurrent:J

.field private final mDeterminate:Z

.field private final mTotal:J

.field private final mType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZJJ)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "determinate"    # Z
    .param p3, "completed"    # J
    .param p5, "total"    # J

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "type"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    iput-object p1, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    .line 50
    iput-boolean p2, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mDeterminate:Z

    .line 51
    iput-wide p3, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mCurrent:J

    .line 52
    iput-wide p5, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mTotal:J

    .line 53
    return-void
.end method

.method public static completed()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 40
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "COMPLETED"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static downloading()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 32
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "DOWNLOADING"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static downloading(JJ)Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7
    .param p0, "current"    # J
    .param p2, "total"    # J

    .prologue
    .line 36
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "DOWNLOADING"

    const/4 v2, 0x1

    move-wide v3, p0

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static failed()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 44
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "FAILED"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static loading()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 24
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "LOADING"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static none()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 20
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "NONE"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method

.method public static searching()Lcom/samsung/groupcast/misc/requests/Progress;
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    .line 28
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Progress;

    const-string v1, "SEARCHING"

    const/4 v2, 0x0

    move-wide v5, v3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/Progress;-><init>(Ljava/lang/String;ZJJ)V

    return-object v0
.end method


# virtual methods
.method public getCurrent()J
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mCurrent:J

    return-wide v0
.end method

.method public getPercentageInt()I
    .locals 6

    .prologue
    .line 92
    iget-wide v2, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mCurrent:J

    long-to-float v0, v2

    .line 93
    .local v0, "current":F
    const-wide/16 v2, 0x1

    iget-wide v4, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mTotal:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    long-to-float v1, v2

    .line 94
    .local v1, "total":F
    div-float v2, v0, v1

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    return v2
.end method

.method public getTotal()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mTotal:J

    return-wide v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public isCompleted()Z
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    const-string v1, "COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isDeterminate()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mDeterminate:Z

    return v0
.end method

.method public isDownloading()Z
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    const-string v1, "DOWNLOADING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isFailure()Z
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    const-string v1, "FAILED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isLoading()Z
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    const-string v1, "LOADING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isSearching()Z
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    const-string v1, "SEARCHING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mDeterminate:Z

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "type"

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    aput-object v2, v1, v4

    const-string v2, "completed"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mCurrent:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "total"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mTotal:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "type"

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/Progress;->mType:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
