.class public Lcom/samsung/groupcast/misc/utility/DecodedGif;
.super Ljava/lang/Object;
.source "DecodedGif.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;
    }
.end annotation


# static fields
.field public static final DELAY_FOR_ANIMATION:I = 0x10


# instance fields
.field private mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

.field private mDuration:I

.field private mGifAnimatorRunnable:Ljava/lang/Runnable;

.field private mMovie:Landroid/graphics/Movie;

.field private mMovieStart:J

.field private mPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mPath:Ljava/lang/String;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Landroid/graphics/Movie;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/DecodedGif;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/DecodedGif;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/misc/utility/DecodedGif;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/DecodedGif;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/groupcast/misc/utility/DecodedGif;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/DecodedGif;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    return-object p1
.end method


# virtual methods
.method public getDelay()I
    .locals 1

    .prologue
    .line 35
    const/16 v0, 0x10

    .line 40
    .local v0, "delay":I
    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDuration:I

    return v0
.end method

.method public getNextFrame()Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 131
    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    if-nez v5, :cond_1

    .line 132
    const-string v5, "null movie"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v5}, Landroid/graphics/Movie;->width()I

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v5}, Landroid/graphics/Movie;->height()I

    move-result v5

    if-nez v5, :cond_3

    .line 137
    :cond_2
    const-string v5, "size error"

    invoke-static {v5}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 141
    :cond_3
    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v5}, Landroid/graphics/Movie;->width()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v6}, Landroid/graphics/Movie;->height()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 142
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 143
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 144
    .local v2, "current":J
    const/4 v4, 0x0

    .line 145
    .local v4, "next":I
    iget v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDuration:I

    if-lez v5, :cond_4

    .line 146
    iget-wide v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovieStart:J

    sub-long v5, v2, v5

    long-to-int v5, v5

    iget v6, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDuration:I

    rem-int v4, v5, v6

    .line 147
    :cond_4
    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v5, v4}, Landroid/graphics/Movie;->setTime(I)Z

    .line 148
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 149
    .local v1, "c":Landroid/graphics/Canvas;
    if-eqz v1, :cond_0

    .line 150
    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v5, v1, v8, v8}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    goto :goto_0
.end method

.method public load()Z
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 79
    const/4 v1, 0x0

    .line 81
    .local v1, "bRes":Z
    iget-object v11, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mPath:Ljava/lang/String;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mPath:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_1

    .line 82
    :cond_0
    const-string v11, "GroupPlay"

    const-string v12, "mpath is null or empty"

    invoke-static {v11, v12}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :goto_0
    return v10

    .line 86
    :cond_1
    const/4 v7, 0x0

    .line 87
    .local v7, "fis":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 89
    .local v2, "bis":Ljava/io/InputStream;
    :try_start_0
    new-instance v6, Ljava/io/File;

    iget-object v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mPath:Ljava/lang/String;

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v6, "f":Ljava/io/File;
    if-eqz v6, :cond_2

    .line 91
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    .end local v7    # "fis":Ljava/io/InputStream;
    .local v8, "fis":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v8}, Ljava/io/InputStream;->available()I

    move-result v10

    new-array v0, v10, [B

    .line 93
    .local v0, "array":[B
    const/4 v10, 0x0

    invoke-virtual {v8}, Ljava/io/InputStream;->available()I

    move-result v11

    invoke-virtual {v8, v0, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v9

    .line 94
    .local v9, "read":I
    const-string v10, "GIF"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "size:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v2    # "bis":Ljava/io/InputStream;
    .local v3, "bis":Ljava/io/InputStream;
    move-object v2, v3

    .end local v3    # "bis":Ljava/io/InputStream;
    .restart local v2    # "bis":Ljava/io/InputStream;
    move-object v7, v8

    .line 98
    .end local v0    # "array":[B
    .end local v8    # "fis":Ljava/io/InputStream;
    .end local v9    # "read":I
    .restart local v7    # "fis":Ljava/io/InputStream;
    :cond_2
    if-eqz v2, :cond_3

    .line 99
    const/4 v10, 0x0

    :try_start_2
    invoke-virtual {v2, v10}, Ljava/io/InputStream;->mark(I)V

    .line 100
    invoke-static {v2}, Landroid/graphics/Movie;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Movie;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    .line 101
    iget-object v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v10}, Landroid/graphics/Movie;->duration()I

    move-result v10

    if-lez v10, :cond_3

    .line 102
    iget-object v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovie:Landroid/graphics/Movie;

    invoke-virtual {v10}, Landroid/graphics/Movie;->duration()I

    move-result v10

    iput v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDuration:I

    .line 103
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mMovieStart:J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    const/4 v1, 0x1

    .line 110
    :cond_3
    if-eqz v7, :cond_4

    .line 112
    :try_start_3
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 118
    :cond_4
    :goto_1
    if-eqz v2, :cond_5

    .line 120
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .end local v6    # "f":Ljava/io/File;
    :cond_5
    :goto_2
    move v10, v1

    .line 127
    goto :goto_0

    .line 113
    .restart local v6    # "f":Ljava/io/File;
    :catch_0
    move-exception v5

    .line 114
    .local v5, "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 121
    .end local v5    # "ee":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 122
    .restart local v5    # "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 107
    .end local v5    # "ee":Ljava/lang/Exception;
    .end local v6    # "f":Ljava/io/File;
    :catch_2
    move-exception v4

    .line 108
    .local v4, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 110
    if-eqz v7, :cond_6

    .line 112
    :try_start_6
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 118
    :cond_6
    :goto_4
    if-eqz v2, :cond_5

    .line 120
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 121
    :catch_3
    move-exception v5

    .line 122
    .restart local v5    # "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 113
    .end local v5    # "ee":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    .line 114
    .restart local v5    # "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 110
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "ee":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    :goto_5
    if-eqz v7, :cond_7

    .line 112
    :try_start_8
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    .line 118
    :cond_7
    :goto_6
    if-eqz v2, :cond_8

    .line 120
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 123
    :cond_8
    :goto_7
    throw v10

    .line 113
    :catch_5
    move-exception v5

    .line 114
    .restart local v5    # "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 121
    .end local v5    # "ee":Ljava/lang/Exception;
    :catch_6
    move-exception v5

    .line 122
    .restart local v5    # "ee":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 110
    .end local v5    # "ee":Ljava/lang/Exception;
    .end local v7    # "fis":Ljava/io/InputStream;
    .restart local v6    # "f":Ljava/io/File;
    .restart local v8    # "fis":Ljava/io/InputStream;
    :catchall_1
    move-exception v10

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/InputStream;
    .restart local v7    # "fis":Ljava/io/InputStream;
    goto :goto_5

    .line 107
    .end local v7    # "fis":Ljava/io/InputStream;
    .restart local v8    # "fis":Ljava/io/InputStream;
    :catch_7
    move-exception v4

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/InputStream;
    .restart local v7    # "fis":Ljava/io/InputStream;
    goto :goto_3
.end method

.method public runInnerThread()V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/misc/utility/DecodedGif$1;-><init>(Lcom/samsung/groupcast/misc/utility/DecodedGif;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    .line 68
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 72
    :cond_0
    return-void
.end method

.method public setDelegate(Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;)V
    .locals 0
    .param p1, "d"    # Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mDecodedGifDelegate:Lcom/samsung/groupcast/misc/utility/DecodedGif$DecodedGifDelegate;

    .line 45
    return-void
.end method

.method public stopInnerThread()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/DecodedGif;->mGifAnimatorRunnable:Ljava/lang/Runnable;

    .line 76
    return-void
.end method
