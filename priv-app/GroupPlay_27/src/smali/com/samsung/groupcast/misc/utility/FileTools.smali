.class public Lcom/samsung/groupcast/misc/utility/FileTools;
.super Ljava/lang/Object;
.source "FileTools.java"


# static fields
.field public static final SIZE_10M:I = 0xa00000

.field public static final SIZE_128K:I = 0x20000

.field public static final SIZE_16K:I = 0x4000

.field public static final SIZE_1K:I = 0x400

.field public static final SIZE_1M:I = 0x100000

.field public static final SIZE_256K:I = 0x40000

.field public static final SIZE_32K:I = 0x8000

.field public static final SIZE_512K:I = 0x80000

.field public static final SIZE_64K:I = 0x10000

.field public static final SIZE_8K:I = 0x2000

.field public static final TMP_DIR_IMG_NAME:Ljava/lang/String; = "gp_img_dir"

.field public static final TMP_DIR_NAME:Ljava/lang/String; = "gp_tmp_dir"

.field public static final UNKNOWN:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetFileNameForSave(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 393
    move-object v5, p0

    .line 395
    .local v5, "result":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 396
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 397
    .local v2, "fileType":Ljava/lang/String;
    move-object v1, p0

    .line 398
    .local v1, "filePath":Ljava/lang/String;
    const-string v6, "."

    invoke-virtual {p0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 399
    .local v4, "index":I
    const/4 v6, -0x1

    if-ne v4, v6, :cond_1

    move-object v1, p0

    .line 401
    :goto_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 403
    const/4 v3, 0x0

    .line 405
    .local v3, "iRetry":I
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 406
    new-instance v0, Ljava/io/File;

    .end local v0    # "f":Ljava/io/File;
    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 407
    .restart local v0    # "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    .line 414
    .end local v0    # "f":Ljava/io/File;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "fileType":Ljava/lang/String;
    .end local v3    # "iRetry":I
    .end local v4    # "index":I
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Name:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 415
    return-object v5

    .line 399
    .restart local v1    # "filePath":Ljava/lang/String;
    .restart local v2    # "fileType":Ljava/lang/String;
    .restart local v4    # "index":I
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 410
    .restart local v0    # "f":Ljava/io/File;
    .restart local v3    # "iRetry":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static closeStreamIfPossible(Ljava/io/Closeable;)Z
    .locals 8
    .param p0, "stream"    # Ljava/io/Closeable;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 544
    const-string v3, "stream"

    invoke-static {v3, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 547
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :goto_0
    return v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Ljava/lang/Exception;
    const-class v3, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v4, "closeStreamIfPossible"

    const-string v5, "exception while closing stream"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "exception"

    aput-object v7, v6, v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v1, v2

    .line 551
    goto :goto_0
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 16
    .param p0, "sourcePath"    # Ljava/lang/String;
    .param p1, "destinationPath"    # Ljava/lang/String;
    .param p2, "bufferSize"    # I

    .prologue
    .line 419
    const-string v10, "sourcePath"

    move-object/from16 v0, p0

    invoke-static {v10, v0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 420
    const-string v10, "destinationPath"

    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 421
    const/4 v5, 0x0

    .line 424
    .local v5, "inputStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 432
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .local v6, "inputStream":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 435
    .local v7, "outputStream":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 436
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    .line 438
    .local v2, "directory":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 439
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 442
    :cond_0
    new-instance v7, Ljava/io/FileOutputStream;

    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 452
    .restart local v7    # "outputStream":Ljava/io/FileOutputStream;
    const/16 v10, 0x4000

    const/high16 v11, 0xa00000

    :try_start_2
    move/from16 v0, p2

    invoke-static {v0, v10, v11}, Lcom/samsung/groupcast/misc/math/MathTools;->clamp(III)I

    move-result v9

    .line 453
    .local v9, "saneBufferSize":I
    new-array v1, v9, [B

    .line 454
    .local v1, "buffer":[B
    const/4 v8, 0x0

    .line 456
    .local v8, "readLength":I
    :goto_0
    const/4 v10, 0x0

    move/from16 v0, p2

    invoke-virtual {v6, v1, v10, v0}, Ljava/io/FileInputStream;->read([BII)I

    move-result v8

    const/4 v10, -0x1

    if-eq v8, v10, :cond_1

    .line 457
    const/4 v10, 0x0

    invoke-virtual {v7, v1, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 459
    .end local v1    # "buffer":[B
    .end local v8    # "readLength":I
    .end local v9    # "saneBufferSize":I
    :catch_0
    move-exception v3

    .line 460
    .local v3, "e":Ljava/io/IOException;
    :try_start_3
    const-class v10, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v11, "copyFile"

    const-string v12, "exception while copying bytes"

    const/16 v13, 0x8

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "source"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p0, v13, v14

    const/4 v14, 0x2

    const-string v15, "destination"

    aput-object v15, v13, v14

    const/4 v14, 0x3

    aput-object p1, v13, v14

    const/4 v14, 0x4

    const-string v15, "bufferSize"

    aput-object v15, v13, v14

    const/4 v14, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x6

    const-string v15, "exception"

    aput-object v15, v13, v14

    const/4 v14, 0x7

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v10, v11, v12, v13}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 463
    const/4 v10, 0x0

    .line 465
    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 466
    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    move-object v5, v6

    .line 469
    .end local v2    # "directory":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "file":Ljava/io/File;
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .end local v7    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :goto_1
    return v10

    .line 425
    :catch_1
    move-exception v3

    .line 426
    .local v3, "e":Ljava/io/FileNotFoundException;
    const-class v10, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v11, "copyFile"

    const-string v12, "source file not found"

    const/16 v13, 0x8

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "source"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p0, v13, v14

    const/4 v14, 0x2

    const-string v15, "destination"

    aput-object v15, v13, v14

    const/4 v14, 0x3

    aput-object p1, v13, v14

    const/4 v14, 0x4

    const-string v15, "bufferSize"

    aput-object v15, v13, v14

    const/4 v14, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x6

    const-string v15, "exception"

    aput-object v15, v13, v14

    const/4 v14, 0x7

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v10, v11, v12, v13}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 429
    const/4 v10, 0x0

    goto :goto_1

    .line 443
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v3

    .line 444
    .restart local v3    # "e":Ljava/io/FileNotFoundException;
    const-class v10, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v11, "copyFile"

    const-string v12, "output file could not be opened"

    const/16 v13, 0x8

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "source"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p0, v13, v14

    const/4 v14, 0x2

    const-string v15, "destination"

    aput-object v15, v13, v14

    const/4 v14, 0x3

    aput-object p1, v13, v14

    const/4 v14, 0x4

    const-string v15, "bufferSize"

    aput-object v15, v13, v14

    const/4 v14, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x6

    const-string v15, "exception"

    aput-object v15, v13, v14

    const/4 v14, 0x7

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v10, v11, v12, v13}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 447
    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 448
    const/4 v10, 0x0

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 465
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v1    # "buffer":[B
    .restart local v2    # "directory":Ljava/io/File;
    .restart local v4    # "file":Ljava/io/File;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v7    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "readLength":I
    .restart local v9    # "saneBufferSize":I
    :cond_1
    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 466
    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 469
    const/4 v10, 0x1

    move-object v5, v6

    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 465
    .end local v1    # "buffer":[B
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v8    # "readLength":I
    .end local v9    # "saneBufferSize":I
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v10

    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 466
    invoke-static {v7}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    throw v10
.end method

.method public static deleteAll(Ljava/io/File;)V
    .locals 5
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 531
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 534
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 535
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 536
    .local v1, "c":Ljava/io/File;
    invoke-static {v1}, Lcom/samsung/groupcast/misc/utility/FileTools;->deleteAll(Ljava/io/File;)V

    .line 535
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 540
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "c":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public static deleteAllContentsInDirectory(Ljava/io/File;)V
    .locals 5
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 518
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    .line 525
    :cond_0
    return-void

    .line 522
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 523
    .local v1, "c":Ljava/io/File;
    invoke-static {v1}, Lcom/samsung/groupcast/misc/utility/FileTools;->deleteAll(Ljava/io/File;)V

    .line 522
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static deleteDirectoryRecursively(Ljava/lang/String;)Z
    .locals 9
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 479
    const-string v8, "path"

    invoke-static {v8, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 480
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 482
    .local v3, "directory":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-nez v8, :cond_0

    .line 483
    const/4 v8, 0x0

    .line 511
    :goto_0
    return v8

    .line 486
    :cond_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 487
    .local v2, "contents":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 489
    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 490
    invoke-virtual {v2}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/io/File;

    .line 492
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 493
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    .line 495
    .local v7, "subfiles":[Ljava/io/File;
    if-eqz v7, :cond_1

    .line 496
    array-length v8, v7

    if-nez v8, :cond_2

    .line 497
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 498
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 500
    :cond_2
    move-object v0, v7

    .local v0, "arr$":[Ljava/io/File;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_2
    if-ge v5, v6, :cond_1

    aget-object v1, v0, v5

    .line 501
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 500
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 506
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "subfiles":[Ljava/io/File;
    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 507
    invoke-virtual {v2}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    .line 511
    .end local v4    # "file":Ljava/io/File;
    :cond_4
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public static deleteFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 473
    const-string v1, "path"

    invoke-static {v1, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 474
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 475
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    return v1
.end method

.method public static getBasename(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 322
    const-string v2, "path"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 323
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "filename":Ljava/lang/String;
    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 325
    .local v1, "index":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .end local v0    # "filename":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "filename":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getCheckUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    .local p0, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v2, "newUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 211
    const-string v3, "GroupCast_FileTools a"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    const-string v4, "gmail-ls"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 213
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-static {v3}, Lcom/samsung/groupcast/misc/utility/FileTools;->getGmailAttachement(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 214
    .local v0, "gmailAttachement":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 215
    const-string v3, "GroupCast_FileTools b"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    .end local v0    # "gmailAttachement":Landroid/net/Uri;
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 226
    :cond_1
    const-string v3, "GroupCast_FileTools d"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 230
    :cond_2
    return-object v2
.end method

.method public static getDirectoryForImageResized(Z)Ljava/lang/String;
    .locals 6
    .param p0, "bClean"    # Z

    .prologue
    .line 286
    const-string v1, "gp_img_dir"

    .line 288
    .local v1, "s":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/misc/utility/FileTools;->getTempImgDir()Ljava/io/File;

    move-result-object v0

    .line 289
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 290
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 293
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 294
    if-eqz p0, :cond_1

    .line 295
    const-string v3, "cleaning!!"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2

    .line 297
    const-string v3, "it is not dir!!"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 318
    :cond_1
    :goto_0
    return-object v1

    .line 299
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_1

    .line 300
    new-instance v2, Ljava/io/File;

    const-string v3, "gp_img_dir"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 301
    .local v2, "todel":Ljava/io/File;
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 302
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 305
    new-instance v3, Lcom/samsung/groupcast/misc/utility/FileTools$1;

    invoke-direct {v3}, Lcom/samsung/groupcast/misc/utility/FileTools$1;-><init>()V

    goto :goto_0
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 330
    const-string v2, "path"

    invoke-static {v2, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 331
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 332
    .local v0, "dotIndex":I
    if-gez v0, :cond_0

    .line 333
    const-string v2, ""

    .line 337
    :goto_0
    return-object v2

    .line 334
    :cond_0
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 335
    .local v1, "slashIndex":I
    if-le v1, v0, :cond_1

    .line 336
    const-string v2, ""

    goto :goto_0

    .line 337
    :cond_1
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getFileType(Ljava/lang/String;)I
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 341
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "ext":Ljava/lang/String;
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_DOCUMENT_FILE_EXTENSIONS:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    const/4 v1, 0x1

    .line 350
    :goto_0
    return v1

    .line 345
    :cond_0
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_IMAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 346
    const/4 v1, 0x3

    goto :goto_0

    .line 347
    :cond_1
    sget-object v1, Lcom/samsung/groupcast/legacy/gp2/session/model/ContentType;->SUPPORTED_COLLAGE_FILE_EXTENSIONS:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 348
    const/4 v1, 0x5

    goto :goto_0

    .line 350
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 54
    const-string v1, "path"

    invoke-static {v1, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 56
    .local v0, "separatorIndex":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .end local p0    # "path":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "path":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getGmailAttachement(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 10
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 163
    const/4 v8, 0x0

    .line 164
    .local v8, "fileName":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 165
    .local v6, "cur":Landroid/database/Cursor;
    const-string v0, "GroupCast_FileTools"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uri:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :try_start_0
    const-string v0, "GroupCast_FileTools"

    const-string v1, "start"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 170
    if-eqz v6, :cond_0

    .line 171
    const-string v0, "GroupCast_FileTools"

    const-string v1, "start cur"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 173
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 175
    .local v9, "orgFileName":Ljava/lang/String;
    :try_start_1
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, v9}, Lcom/samsung/groupcast/misc/utility/FileTools;->makeTempFileFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 182
    :goto_0
    :try_start_2
    const-string v0, "GroupCast_FileTools"

    const-string v1, "end cur"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 189
    .end local v9    # "orgFileName":Ljava/lang/String;
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 193
    :cond_1
    const-string v0, "GroupCast_FileTools"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ret :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    return-object v8

    .line 179
    .restart local v9    # "orgFileName":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 180
    .local v7, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 184
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    .end local v9    # "orgFileName":Ljava/lang/String;
    :catch_1
    move-exception v7

    .line 185
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 186
    const-string v0, "GMAIL"

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getResizedImage(Ljava/lang/String;JIII)Ljava/lang/String;
    .locals 26
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "maxSize"    # J
    .param p3, "jpgRatio"    # I
    .param p4, "maxResolutionW"    # I
    .param p5, "maxResolutionH"    # I

    .prologue
    .line 62
    move-object/from16 v19, p0

    .line 63
    .local v19, "ret":Ljava/lang/String;
    const/16 v22, 0x0

    .line 64
    .local v22, "srcf":Ljava/io/File;
    const/4 v3, 0x0

    .line 65
    .local v3, "b":Landroid/graphics/Bitmap;
    const/4 v15, 0x0

    .line 67
    .local v15, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/groupcast/misc/utility/FileTools;->getTempDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->getFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 68
    .local v17, "newpath":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 69
    const/high16 v4, 0x100000

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1, v4}, Lcom/samsung/groupcast/misc/utility/FileTools;->copyFile(Ljava/lang/String;Ljava/lang/String;I)Z

    .line 71
    :cond_0
    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v22    # "srcf":Ljava/io/File;
    .local v23, "srcf":Ljava/io/File;
    if-eqz v23, :cond_1

    :try_start_1
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_4

    .line 73
    :cond_1
    const-string v4, "file is null or notexist..."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 74
    const/16 v17, 0x0

    .line 149
    .end local v17    # "newpath":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_2
    if-eqz v15, :cond_3

    .line 153
    :try_start_2
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    :goto_0
    move-object/from16 v22, v23

    .line 159
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    :goto_1
    return-object v17

    .line 154
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_0
    move-exception v11

    .line 155
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v17    # "newpath":Ljava/lang/String;
    :cond_4
    :try_start_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ".gif"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v4

    if-eqz v4, :cond_7

    .line 149
    if-eqz v3, :cond_5

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_5
    if-eqz v15, :cond_6

    .line 153
    :try_start_4
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_6
    :goto_2
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_1

    .line 154
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_1
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 78
    .end local v11    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_5
    new-instance v18, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 79
    .local v18, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    move-object/from16 v0, v18

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 80
    invoke-static/range {v17 .. v18}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 82
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-gez v4, :cond_a

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v0, p4

    if-gt v4, v0, :cond_a

    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v0, p5

    if-gt v4, v0, :cond_a

    .line 84
    const-string v4, "size is not big skip..."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 149
    if-eqz v3, :cond_8

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_8
    if-eqz v15, :cond_9

    .line 153
    :try_start_6
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_9
    :goto_3
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_1

    .line 154
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_2
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 89
    .end local v11    # "e":Ljava/io/IOException;
    :cond_a
    :try_start_7
    move-object/from16 v0, v17

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/misc/graphics/BitmapTools;->decodeBitmapFromPath(Ljava/lang/String;II)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;

    move-result-object v10

    .line 92
    .local v10, "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    if-nez v10, :cond_d

    .line 93
    const-string v4, "decode failed."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 149
    if-eqz v3, :cond_b

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_b
    if-eqz v15, :cond_c

    .line 153
    :try_start_8
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_c
    :goto_4
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_1

    .line 154
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_3
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 97
    .end local v11    # "e":Ljava/io/IOException;
    :cond_d
    :try_start_9
    invoke-virtual {v10}, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 100
    const/16 v21, 0x0

    .line 101
    .local v21, "rotationF":F
    invoke-virtual {v10}, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->getExifData()Lcom/samsung/groupcast/misc/graphics/ExifData;

    move-result-object v12

    .line 102
    .local v12, "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    invoke-virtual {v12}, Lcom/samsung/groupcast/misc/graphics/ExifData;->getOrientation()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 119
    :goto_5
    :pswitch_0
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 120
    .local v8, "m":Landroid/graphics/Matrix;
    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 121
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 123
    .local v20, "rotatedBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v3, v20

    .line 124
    const/16 v20, 0x0

    .line 127
    if-nez v3, :cond_10

    .line 128
    const-string v4, "b is null..."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 149
    if-eqz v3, :cond_e

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_e
    if-eqz v15, :cond_f

    .line 153
    :try_start_a
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    :cond_f
    :goto_6
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto/16 :goto_1

    .line 104
    .end local v8    # "m":Landroid/graphics/Matrix;
    .end local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :pswitch_1
    const/high16 v21, 0x42b40000    # 90.0f

    .line 105
    goto :goto_5

    .line 109
    :pswitch_2
    const/high16 v21, 0x43340000    # 180.0f

    .line 110
    goto :goto_5

    .line 114
    :pswitch_3
    const/high16 v21, 0x43870000    # 270.0f

    goto :goto_5

    .line 154
    .restart local v8    # "m":Landroid/graphics/Matrix;
    .restart local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    :catch_4
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 132
    .end local v11    # "e":Ljava/io/IOException;
    :cond_10
    :try_start_b
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    move/from16 v0, p4

    if-gt v4, v0, :cond_13

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move/from16 v0, p5

    if-gt v4, v0, :cond_13

    .line 133
    const-string v4, "resolution is not big skip..."

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 149
    if-eqz v3, :cond_11

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_11
    if-eqz v15, :cond_12

    .line 153
    :try_start_c
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    :cond_12
    :goto_7
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto/16 :goto_1

    .line 154
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_5
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 137
    .end local v11    # "e":Ljava/io/IOException;
    :cond_13
    :try_start_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/FileTools;->getDirectoryForImageResized(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {v17 .. v17}, Lcom/samsung/groupcast/misc/utility/FileTools;->getBasename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_.jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "- start resize..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 139
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 140
    .local v13, "f":Ljava/io/File;
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 141
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .local v16, "fos":Ljava/io/FileOutputStream;
    :try_start_e
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v3, v4, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 142
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "- end resize..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " size:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v24, 0x400

    div-long v5, v5, v24

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "kb"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 144
    const/4 v3, 0x0

    .line 149
    if-eqz v3, :cond_14

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_14
    if-eqz v16, :cond_19

    .line 153
    :try_start_f
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .end local v8    # "m":Landroid/graphics/Matrix;
    .end local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .end local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .end local v13    # "f":Ljava/io/File;
    .end local v17    # "newpath":Ljava/lang/String;
    .end local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .end local v21    # "rotationF":F
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    :cond_15
    :goto_8
    move-object/from16 v17, v19

    .line 159
    goto/16 :goto_1

    .line 154
    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v8    # "m":Landroid/graphics/Matrix;
    .restart local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .restart local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .restart local v13    # "f":Ljava/io/File;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v17    # "newpath":Ljava/lang/String;
    .restart local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .restart local v21    # "rotationF":F
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_6
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .line 156
    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_8

    .line 145
    .end local v8    # "m":Landroid/graphics/Matrix;
    .end local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .end local v11    # "e":Ljava/io/IOException;
    .end local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .end local v13    # "f":Ljava/io/File;
    .end local v17    # "newpath":Ljava/lang/String;
    .end local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .end local v21    # "rotationF":F
    :catch_7
    move-exception v14

    .line 146
    .local v14, "fe":Ljava/lang/Exception;
    :goto_9
    :try_start_10
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 147
    move-object/from16 v19, p0

    .line 149
    if-eqz v3, :cond_16

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_16
    if-eqz v15, :cond_15

    .line 153
    :try_start_11
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_8

    goto :goto_8

    .line 154
    :catch_8
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 149
    .end local v11    # "e":Ljava/io/IOException;
    .end local v14    # "fe":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_a
    if-eqz v3, :cond_17

    .line 150
    const/4 v3, 0x0

    .line 151
    :cond_17
    if-eqz v15, :cond_18

    .line 153
    :try_start_12
    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_9

    .line 156
    :cond_18
    :goto_b
    throw v4

    .line 154
    :catch_9
    move-exception v11

    .line 155
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 149
    .end local v11    # "e":Ljava/io/IOException;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v17    # "newpath":Ljava/lang/String;
    .restart local v23    # "srcf":Ljava/io/File;
    :catchall_1
    move-exception v4

    move-object/from16 v22, v23

    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_a

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v8    # "m":Landroid/graphics/Matrix;
    .restart local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .restart local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .restart local v13    # "f":Ljava/io/File;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .restart local v21    # "rotationF":F
    .restart local v23    # "srcf":Ljava/io/File;
    :catchall_2
    move-exception v4

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_a

    .line 145
    .end local v8    # "m":Landroid/graphics/Matrix;
    .end local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .end local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .end local v13    # "f":Ljava/io/File;
    .end local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .end local v21    # "rotationF":F
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_a
    move-exception v14

    move-object/from16 v22, v23

    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_9

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v8    # "m":Landroid/graphics/Matrix;
    .restart local v10    # "bitmapResult":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .restart local v12    # "exifData":Lcom/samsung/groupcast/misc/graphics/ExifData;
    .restart local v13    # "f":Ljava/io/File;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v18    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v20    # "rotatedBitmap":Landroid/graphics/Bitmap;
    .restart local v21    # "rotationF":F
    .restart local v23    # "srcf":Ljava/io/File;
    :catch_b
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_9

    .end local v15    # "fos":Ljava/io/FileOutputStream;
    .end local v22    # "srcf":Ljava/io/File;
    .restart local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v23    # "srcf":Ljava/io/File;
    :cond_19
    move-object/from16 v15, v16

    .end local v16    # "fos":Ljava/io/FileOutputStream;
    .restart local v15    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v22, v23

    .end local v23    # "srcf":Ljava/io/File;
    .restart local v22    # "srcf":Ljava/io/File;
    goto :goto_8

    .line 102
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getTempDir()Ljava/io/File;
    .locals 3

    .prologue
    .line 278
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "gp_tmp_dir"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getTempImgDir()Ljava/io/File;
    .locals 3

    .prologue
    .line 282
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "gp_img_dir"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static isInstalledApplication(Ljava/lang/String;)Z
    .locals 5
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 559
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 561
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 562
    :catch_0
    move-exception v0

    .line 563
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-class v2, Lcom/samsung/groupcast/misc/utility/FileTools;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isInstalledApplication is not install ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;)V

    .line 565
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static makeTempFileFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/net/Uri;
    .locals 14
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 235
    const/4 v7, 0x0

    .line 236
    .local v7, "uri":Landroid/net/Uri;
    const v1, 0xa000

    .line 238
    .local v1, "bufferSize":I
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 239
    const/4 v4, 0x0

    .line 241
    .local v4, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v8, "gma"

    invoke-static {}, Lcom/samsung/groupcast/misc/utility/FileTools;->getTempDir()Ljava/io/File;

    move-result-object v9

    invoke-static {v8, p1, v9}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    .line 242
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->deleteOnExit()V

    .line 243
    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 244
    new-instance v4, Ljava/io/FileOutputStream;

    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 254
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    const/16 v8, 0x4000

    const/high16 v9, 0xa00000

    :try_start_1
    invoke-static {v1, v8, v9}, Lcom/samsung/groupcast/misc/math/MathTools;->clamp(III)I

    move-result v6

    .line 255
    .local v6, "saneBufferSize":I
    new-array v0, v6, [B

    .line 256
    .local v0, "buffer":[B
    const/4 v5, 0x0

    .line 258
    .local v5, "readLength":I
    :goto_0
    const/4 v8, 0x0

    invoke-virtual {p0, v0, v8, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    const/4 v8, -0x1

    if-eq v5, v8, :cond_0

    .line 259
    const/4 v8, 0x0

    invoke-virtual {v4, v0, v8, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 261
    .end local v0    # "buffer":[B
    .end local v5    # "readLength":I
    .end local v6    # "saneBufferSize":I
    :catch_0
    move-exception v2

    .line 262
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-class v8, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v9, "makeTempFileFromStream"

    const-string v10, "output file could not be opened"

    const/4 v11, 0x6

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "uri"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v7, v11, v12

    const/4 v12, 0x2

    const-string v13, "bufferSize"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x4

    const-string v13, "exception"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 265
    const/4 v8, 0x0

    .line 267
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 268
    invoke-static {v4}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 273
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    :goto_1
    return-object v8

    .line 245
    :catch_1
    move-exception v2

    .line 246
    .local v2, "e":Ljava/lang/Exception;
    const-class v8, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v9, "makeTempFileFromStream"

    const-string v10, "output file could not be opened"

    const/4 v11, 0x6

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "uri"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v7, v11, v12

    const/4 v12, 0x2

    const-string v13, "bufferSize"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x4

    const-string v13, "exception"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 249
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 250
    const/4 v8, 0x0

    goto :goto_1

    .line 267
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "buffer":[B
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "readLength":I
    .restart local v6    # "saneBufferSize":I
    :cond_0
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 268
    invoke-static {v4}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .end local v0    # "buffer":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "outputStream":Ljava/io/FileOutputStream;
    .end local v5    # "readLength":I
    .end local v6    # "saneBufferSize":I
    :cond_1
    move-object v8, v7

    .line 273
    goto :goto_1

    .line 267
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v8

    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 268
    invoke-static {v4}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    throw v8
.end method

.method public static readFileAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 355
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/samsung/groupcast/misc/utility/FileTools;->readFileAsString(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readFileAsString(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 14
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/nio/charset/Charset;

    .prologue
    .line 359
    const-string v8, "path"

    invoke-static {v8, p0}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 360
    const-string v8, "charset"

    invoke-static {v8, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 361
    const/4 v4, 0x0

    .line 362
    .local v4, "inputStream":Ljava/io/FileInputStream;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 365
    .local v3, "file":Ljava/io/File;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .local v5, "inputStream":Ljava/io/FileInputStream;
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v7, v8

    .line 373
    .local v7, "size":I
    new-array v0, v7, [B

    .line 374
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .line 375
    .local v1, "count":I
    const/4 v6, 0x0

    .line 379
    .local v6, "result":I
    :cond_0
    sub-int v8, v7, v1

    :try_start_1
    invoke-virtual {v5, v0, v1, v8}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    .line 380
    const/4 v8, -0x1

    if-ne v6, v8, :cond_0

    .line 386
    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    .line 389
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v0, p1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    move-object v4, v5

    .end local v0    # "buffer":[B
    .end local v1    # "count":I
    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .end local v6    # "result":I
    .end local v7    # "size":I
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    :goto_0
    return-object v8

    .line 366
    :catch_0
    move-exception v2

    .line 367
    .local v2, "e":Ljava/io/FileNotFoundException;
    const-class v8, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v9, "readFile"

    const-string v10, "file not found"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "path"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object p0, v11, v12

    const/4 v12, 0x2

    const-string v13, "exception"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 369
    const/4 v8, 0x0

    goto :goto_0

    .line 381
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "count":I
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "result":I
    .restart local v7    # "size":I
    :catch_1
    move-exception v2

    .line 382
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    const-class v8, Lcom/samsung/groupcast/misc/utility/FileTools;

    const-string v9, "readFile"

    const-string v10, "exception while copying bytes"

    const/16 v11, 0x8

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "path"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object p0, v11, v12

    const/4 v12, 0x2

    const-string v13, "size"

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x4

    const-string v13, "copied"

    aput-object v13, v11, v12

    const/4 v12, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x6

    const-string v13, "exception"

    aput-object v13, v11, v12

    const/4 v12, 0x7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 384
    const/4 v8, 0x0

    .line 386
    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    move-object v4, v5

    .end local v5    # "inputStream":Ljava/io/FileInputStream;
    .restart local v4    # "inputStream":Ljava/io/FileInputStream;
    goto :goto_0

    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "inputStream":Ljava/io/FileInputStream;
    .restart local v5    # "inputStream":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v8

    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/FileTools;->closeStreamIfPossible(Ljava/io/Closeable;)Z

    throw v8
.end method

.method public static scanMediaFile(Ljava/lang/String;)V
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 571
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1, v3, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 573
    return-void
.end method
