.class public Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;
.super Ljava/lang/Object;
.source "HelpConnectionUtil.java"


# static fields
.field public static final HELP_URI:Ljava/lang/String; = "http://help.content.samsung.com:8080/csmobile"

.field private static final SBROWSER_COMPONENT_NAME:Landroid/content/ComponentName;

.field public static final SHOW_CUSTOMER_SUPPORT_PAGE:I = 0x2

.field public static final SHOW_FAQ_PAGE:I = 0x1

.field public static final SHOW_TEMP_PAGE:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 118
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.sbrowser"

    const-string v2, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;->SBROWSER_COMPONENT_NAME:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createFaqUrl(I)Ljava/lang/String;
    .locals 9
    .param p0, "pageToShow"    # I

    .prologue
    .line 20
    const/4 v5, 0x0

    .line 21
    .local v5, "target":Ljava/lang/String;
    const/4 v6, 0x0

    .line 23
    .local v6, "target_uri":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 34
    const-string v6, "http://help.content.samsung.com:8080/csmobile"

    .line 39
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 40
    .local v3, "locale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "countryCode":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "langCode":Ljava/lang/String;
    const-string v7, "zh"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 51
    const-string v7, "HK"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 52
    const-string v2, "zh_hk"

    .line 86
    :cond_0
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "{serviceCd:\'groupplay\',targetUrl:\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\',chnlCd:\'odc\',_common_country:\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\',_common_lang:\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\'};"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 94
    .local v4, "postData":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "javascript:var to =\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\';"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "var p = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "var myForm = document.createElement(\'form\');"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myForm.method=\'post\' ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myForm.action = to;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "for (var k in p) {"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "var myInput = document.createElement(\'input\') ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myInput.setAttribute(\'type\', \'text\');"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myInput.setAttribute(\'name\', k) ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myInput.setAttribute(\'value\', p[k]);"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myForm.appendChild(myInput) ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "}"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "document.body.appendChild(myForm) ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "myForm.submit() ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "document.body.removeChild(myForm) ;"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "finalUrl":Ljava/lang/String;
    return-object v1

    .line 25
    .end local v0    # "countryCode":Ljava/lang/String;
    .end local v1    # "finalUrl":Ljava/lang/String;
    .end local v2    # "langCode":Ljava/lang/String;
    .end local v3    # "locale":Ljava/util/Locale;
    .end local v4    # "postData":Ljava/lang/String;
    :pswitch_0
    const-string v6, "http://help.content.samsung.com:8080/csmobile/faq/searchFaq.do"

    .line 26
    goto/16 :goto_0

    .line 28
    :pswitch_1
    const-string v6, "http://help.content.samsung.com:8080/csmobile/link/link.do"

    .line 29
    goto/16 :goto_0

    .line 31
    :pswitch_2
    const-string v6, "http://help.content.samsung.com:8080/csmobile/faq/searchFaq.do&category1_id=groupplay-b01&r_faq_id=110001"

    .line 32
    goto/16 :goto_0

    .line 53
    .restart local v0    # "countryCode":Ljava/lang/String;
    .restart local v2    # "langCode":Ljava/lang/String;
    .restart local v3    # "locale":Ljava/util/Locale;
    :cond_1
    const-string v7, "CN"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 54
    const-string v2, "zh_cn"

    goto/16 :goto_1

    .line 56
    :cond_2
    const-string v2, "zh_tw"

    goto/16 :goto_1

    .line 58
    :cond_3
    const-string v7, "en"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 59
    const-string v7, "GB"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 60
    const-string v2, "en_gb"

    goto/16 :goto_1

    .line 62
    :cond_4
    const-string v2, "en_us"

    goto/16 :goto_1

    .line 64
    :cond_5
    const-string v7, "fr"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 65
    const-string v7, "CA"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 66
    const-string v2, "fr_ca"

    goto/16 :goto_1

    .line 68
    :cond_6
    const-string v2, "fr_fr"

    goto/16 :goto_1

    .line 70
    :cond_7
    const-string v7, "pt"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 71
    const-string v7, "PT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 72
    const-string v2, "pt_pt"

    goto/16 :goto_1

    .line 74
    :cond_8
    const-string v2, "pt_latn"

    goto/16 :goto_1

    .line 76
    :cond_9
    const-string v7, "es"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 77
    const-string v7, "ES"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 78
    const-string v2, "es_es"

    goto/16 :goto_1

    .line 80
    :cond_a
    const-string v2, "es_latn"

    goto/16 :goto_1

    .line 82
    :cond_b
    const-string v7, "ar"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 83
    const-string v2, "ar_ae"

    goto/16 :goto_1

    .line 23
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static startInSBrowser(Landroid/app/Activity;Landroid/content/Intent;)V
    .locals 3
    .param p0, "fromActivity"    # Landroid/app/Activity;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 130
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 132
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v1, Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;->SBROWSER_COMPONENT_NAME:Landroid/content/ComponentName;

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 133
    sget-object v1, Lcom/samsung/groupcast/misc/utility/HelpConnectionUtil;->SBROWSER_COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 139
    return-void

    .line 134
    :catch_0
    move-exception v1

    goto :goto_0
.end method
