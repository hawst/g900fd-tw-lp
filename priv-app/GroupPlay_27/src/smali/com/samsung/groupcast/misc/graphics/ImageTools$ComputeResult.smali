.class public Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;
.super Ljava/lang/Object;
.source "ImageTools.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/misc/graphics/ImageTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComputeResult"
.end annotation


# instance fields
.field private final mHeight:F

.field private final mMatrix:Landroid/graphics/Matrix;

.field private final mScaleX:F

.field private final mScaleY:F

.field private final mWidth:F


# direct methods
.method public constructor <init>(Landroid/graphics/Matrix;FFFF)V
    .locals 0
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .param p2, "width"    # F
    .param p3, "height"    # F
    .param p4, "scaleX"    # F
    .param p5, "scaleY"    # F

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p2, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mWidth:F

    .line 16
    iput p3, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mHeight:F

    .line 17
    iput-object p1, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mMatrix:Landroid/graphics/Matrix;

    .line 18
    iput p4, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mScaleX:F

    .line 19
    iput p5, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mScaleY:F

    .line 20
    return-void
.end method


# virtual methods
.method public getHeight()F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mHeight:F

    return v0
.end method

.method public getMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mScaleX:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mScaleY:F

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;->mWidth:F

    return v0
.end method
