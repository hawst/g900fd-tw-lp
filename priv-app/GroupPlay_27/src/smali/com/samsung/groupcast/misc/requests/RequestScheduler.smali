.class public Lcom/samsung/groupcast/misc/requests/RequestScheduler;
.super Ljava/lang/Object;
.source "RequestScheduler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    }
.end annotation


# instance fields
.field private final mAttempts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/groupcast/misc/requests/Request;",
            "Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/misc/requests/RequestScheduler;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/requests/RequestScheduler;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public cancelAllAttempts()V
    .locals 3

    .prologue
    .line 41
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;

    .line 42
    .local v0, "attempt":Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->invalidate()V

    goto :goto_0

    .line 45
    .end local v0    # "attempt":Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    :cond_0
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 46
    return-void
.end method

.method public cancelAttempts(Lcom/samsung/groupcast/misc/requests/Request;)V
    .locals 2
    .param p1, "request"    # Lcom/samsung/groupcast/misc/requests/Request;

    .prologue
    .line 33
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;

    .line 35
    .local v0, "attempt":Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->invalidate()V

    .line 38
    :cond_0
    return-void
.end method

.method public startAttempt(Lcom/samsung/groupcast/misc/requests/Request;JLcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V
    .locals 6
    .param p1, "request"    # Lcom/samsung/groupcast/misc/requests/Request;
    .param p2, "timeoutMilliseconds"    # J
    .param p4, "delegate"    # Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    .prologue
    .line 13
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->startAttempts(Lcom/samsung/groupcast/misc/requests/Request;JILcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V

    .line 14
    return-void
.end method

.method public startAttempts(Lcom/samsung/groupcast/misc/requests/Request;JILcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V
    .locals 8
    .param p1, "request"    # Lcom/samsung/groupcast/misc/requests/Request;
    .param p2, "intervalMilliseconds"    # J
    .param p4, "maxAttempts"    # I
    .param p5, "delegate"    # Lcom/samsung/groupcast/misc/requests/SchedulerDelegate;

    .prologue
    .line 18
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "startAttempts"

    const/4 v3, 0x0

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "interval"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "maxAttempts"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "request"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object p1, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 20
    const-string v1, "request"

    invoke-static {v1, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 21
    const-string v1, "delegate"

    invoke-static {v1, p5}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 22
    new-instance v0, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;-><init>(Lcom/samsung/groupcast/misc/requests/RequestScheduler;Lcom/samsung/groupcast/misc/requests/Request;JILcom/samsung/groupcast/misc/requests/SchedulerDelegate;)V

    .line 23
    .local v0, "attempt":Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/RequestScheduler;->mAttempts:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;

    .line 25
    .local v7, "previousAttempt":Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;
    if-eqz v7, :cond_0

    .line 26
    invoke-virtual {v7}, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->invalidate()V

    .line 29
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/requests/RequestScheduler$Attempt;->run()V

    .line 30
    return-void
.end method
