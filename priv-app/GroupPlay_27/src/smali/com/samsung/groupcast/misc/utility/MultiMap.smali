.class public Lcom/samsung/groupcast/misc/utility/MultiMap;
.super Ljava/lang/Object;
.source "MultiMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mValuesByKey:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;",
            "Ljava/util/HashSet",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 20
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 21
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 26
    .local v0, "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    if-nez v0, :cond_0

    .line 27
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 30
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public getKeySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 36
    .local v0, "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 38
    .restart local v0    # "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    const/4 v1, 0x0

    .line 55
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 57
    .local v0, "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    if-nez v0, :cond_1

    move-object p2, v1

    .line 69
    .end local p2    # "value":Ljava/lang/Object;, "TV;"
    :cond_0
    :goto_0
    return-object p2

    .line 61
    .restart local p2    # "value":Ljava/lang/Object;, "TV;"
    :cond_1
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move-object p2, v1

    .line 62
    goto :goto_0

    .line 65
    :cond_2
    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 47
    .local v0, "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "values":Ljava/util/HashSet;, "Ljava/util/HashSet<TV;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 51
    :cond_0
    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 73
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/MultiMap;, "Lcom/samsung/groupcast/misc/utility/MultiMap<TK;TV;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/MultiMap;->mValuesByKey:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
