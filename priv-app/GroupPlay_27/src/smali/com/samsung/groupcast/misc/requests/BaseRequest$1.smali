.class Lcom/samsung/groupcast/misc/requests/BaseRequest$1;
.super Ljava/lang/Object;
.source "BaseRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/misc/requests/BaseRequest;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

.field final synthetic val$starter:Lcom/samsung/groupcast/misc/requests/Starter;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/misc/requests/BaseRequest;Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 0

    .prologue
    .line 53
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest$1;, "Lcom/samsung/groupcast/misc/requests/BaseRequest.1;"
    iput-object p1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    iput-object p2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->val$starter:Lcom/samsung/groupcast/misc/requests/Starter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 56
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest$1;, "Lcom/samsung/groupcast/misc/requests/BaseRequest.1;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$000(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    # getter for: Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z
    invoke-static {v0}, Lcom/samsung/groupcast/misc/requests/BaseRequest;->access$100(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    monitor-exit v1

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->val$starter:Lcom/samsung/groupcast/misc/requests/Starter;

    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;->this$0:Lcom/samsung/groupcast/misc/requests/BaseRequest;

    invoke-interface {v0, v2}, Lcom/samsung/groupcast/misc/requests/Starter;->start(Lcom/samsung/groupcast/misc/requests/Request;)V

    .line 62
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
