.class public Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
.super Ljava/lang/Object;
.source "BitmapLoadResult.java"


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private final mExifData:Lcom/samsung/groupcast/misc/graphics/ExifData;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/samsung/groupcast/misc/graphics/ExifData;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "exifData"    # Lcom/samsung/groupcast/misc/graphics/ExifData;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->mBitmap:Landroid/graphics/Bitmap;

    .line 11
    iput-object p2, p0, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->mExifData:Lcom/samsung/groupcast/misc/graphics/ExifData;

    .line 12
    return-void
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getExifData()Lcom/samsung/groupcast/misc/graphics/ExifData;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;->mExifData:Lcom/samsung/groupcast/misc/graphics/ExifData;

    return-object v0
.end method
