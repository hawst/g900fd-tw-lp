.class public Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;
.super Landroid/os/AsyncTask;
.source "LoadBitmapTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPath:Ljava/lang/String;

.field private final mTargetHeight:I

.field private final mTargetWidth:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mPath:Ljava/lang/String;

    .line 14
    iput p2, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mTargetWidth:I

    .line 15
    iput p3, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mTargetHeight:I

    .line 16
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 20
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bitmapLoad:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 21
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "doInBackground"

    const-string v4, "task cancelled, returning null"

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "path"

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mPath:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 33
    :goto_0
    return-object v1

    .line 27
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mPath:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mTargetWidth:I

    iget v4, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mTargetHeight:I

    invoke-static {v2, v3, v4}, Lcom/samsung/groupcast/misc/graphics/BitmapTools;->decodeBitmapFromPath(Ljava/lang/String;II)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 29
    .local v1, "result":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    goto :goto_0

    .line 30
    .end local v1    # "result":Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "doInBackground"

    const-string v4, "out of memory while decoding, returning null"

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "path"

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->mPath:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 7
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/misc/graphics/LoadBitmapTask;->doInBackground([Ljava/lang/Void;)Lcom/samsung/groupcast/misc/graphics/BitmapLoadResult;

    move-result-object v0

    return-object v0
.end method
