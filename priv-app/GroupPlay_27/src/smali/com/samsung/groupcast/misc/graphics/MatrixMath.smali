.class public Lcom/samsung/groupcast/misc/graphics/MatrixMath;
.super Ljava/lang/Object;
.source "MatrixMath.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computeCenterFitImageMatrix([FFFFFZ)V
    .locals 10
    .param p0, "resultMatrixComponents"    # [F
    .param p1, "viewWidth"    # F
    .param p2, "viewHeight"    # F
    .param p3, "imageWidth"    # F
    .param p4, "imageHeight"    # F
    .param p5, "performUpscaling"    # Z

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/4 v8, 0x0

    .line 151
    cmpl-float v7, p1, v8

    if-eqz v7, :cond_0

    cmpl-float v7, p2, v8

    if-eqz v7, :cond_0

    cmpl-float v7, p3, v8

    if-eqz v7, :cond_0

    cmpl-float v7, p4, v8

    if-nez v7, :cond_1

    .line 152
    :cond_0
    invoke-static {p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->setIdentity([F)V

    .line 178
    :goto_0
    return-void

    .line 157
    :cond_1
    const/high16 v6, 0x3f800000    # 1.0f

    .line 159
    .local v6, "uniformScaleToApply":F
    cmpg-float v7, p3, p1

    if-gez v7, :cond_2

    cmpg-float v7, p4, p2

    if-gez v7, :cond_2

    if-nez p5, :cond_2

    .line 160
    const/high16 v6, 0x3f800000    # 1.0f

    .line 169
    :goto_1
    mul-float v3, p3, v6

    .line 170
    .local v3, "scaledImageWidth":F
    mul-float v2, p4, v6

    .line 171
    .local v2, "scaledImageHeight":F
    sub-float v7, p1, v3

    mul-float v4, v7, v9

    .line 172
    .local v4, "translateX":F
    sub-float v7, p2, v2

    mul-float v5, v7, v9

    .line 175
    .local v5, "translateY":F
    invoke-static {p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->setIdentity([F)V

    .line 176
    invoke-static {p0, v4, v5}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->setTranslate([FFF)V

    .line 177
    invoke-static {p0, v6, v6}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->setScale([FFF)V

    goto :goto_0

    .line 162
    .end local v2    # "scaledImageHeight":F
    .end local v3    # "scaledImageWidth":F
    .end local v4    # "translateX":F
    .end local v5    # "translateY":F
    :cond_2
    div-float v0, p1, p3

    .line 163
    .local v0, "possibleXScale":F
    div-float v1, p2, p4

    .line 164
    .local v1, "possibleYScale":F
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v6

    goto :goto_1
.end method

.method public static copyMatrixComponents([F[F)V
    .locals 6
    .param p0, "resultMatrixComponents"    # [F
    .param p1, "imageMatrixComponents"    # [F

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 217
    if-ne p0, p1, :cond_0

    .line 230
    :goto_0
    return-void

    .line 221
    :cond_0
    aget v0, p1, v1

    aput v0, p0, v1

    .line 222
    aget v0, p1, v2

    aput v0, p0, v2

    .line 223
    aget v0, p1, v3

    aput v0, p0, v3

    .line 224
    aget v0, p1, v4

    aput v0, p0, v4

    .line 225
    aget v0, p1, v5

    aput v0, p0, v5

    .line 226
    const/4 v0, 0x5

    const/4 v1, 0x5

    aget v1, p1, v1

    aput v1, p0, v0

    .line 227
    const/4 v0, 0x6

    const/4 v1, 0x6

    aget v1, p1, v1

    aput v1, p0, v0

    .line 228
    const/4 v0, 0x7

    const/4 v1, 0x7

    aget v1, p1, v1

    aput v1, p0, v0

    .line 229
    const/16 v0, 0x8

    const/16 v1, 0x8

    aget v1, p1, v1

    aput v1, p0, v0

    goto :goto_0
.end method

.method public static createMatrix([F)Landroid/graphics/Matrix;
    .locals 1
    .param p0, "matrixComponents"    # [F

    .prologue
    .line 236
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 237
    .local v0, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v0, p0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 238
    return-object v0
.end method

.method public static createMatrixComponents()[F
    .locals 1

    .prologue
    .line 200
    const/16 v0, 0x9

    new-array v0, v0, [F

    return-object v0
.end method

.method public static createMatrixComponents(Landroid/graphics/Matrix;)[F
    .locals 2
    .param p0, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 207
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 208
    .local v0, "matrixComponents":[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 209
    return-object v0
.end method

.method public static getFitToBoundsImageMatrix([F[FFFFFFFFF)V
    .locals 26
    .param p0, "resultMatrixComponents"    # [F
    .param p1, "imageMatrixComponents"    # [F
    .param p2, "viewWidth"    # F
    .param p3, "viewHeight"    # F
    .param p4, "imageWidth"    # F
    .param p5, "imageHeight"    # F
    .param p6, "focusX"    # F
    .param p7, "focusY"    # F
    .param p8, "maxScale"    # F
    .param p9, "fitToViewThreshold"    # F

    .prologue
    .line 26
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-ne v0, v1, :cond_0

    .line 27
    invoke-static {}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->createMatrixComponents()[F

    move-result-object v15

    .line 28
    .local v15, "temp":[F
    move-object/from16 v0, p1

    invoke-static {v15, v0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->copyMatrixComponents([F[F)V

    .line 29
    move-object/from16 p1, v15

    .line 33
    .end local v15    # "temp":[F
    :cond_0
    invoke-static/range {p0 .. p1}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->copyMatrixComponents([F[F)V

    .line 36
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getXScale([F)F

    move-result v19

    .line 37
    .local v19, "xScale":F
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getYScale([F)F

    move-result v21

    .line 38
    .local v21, "yScale":F
    mul-float v14, p4, v19

    .line 39
    .local v14, "scaledWidth":F
    mul-float v13, p5, v21

    .line 41
    .local v13, "scaledHeight":F
    cmpg-float v22, p4, p2

    if-gtz v22, :cond_8

    cmpg-float v22, p5, p3

    if-gtz v22, :cond_8

    .line 43
    cmpg-float v22, v14, p4

    if-gtz v22, :cond_6

    cmpg-float v22, v13, p5

    if-gtz v22, :cond_6

    .line 45
    div-float v18, p4, p2

    .line 46
    .local v18, "xRatio":F
    div-float v20, p5, p3

    .line 47
    .local v20, "yRatio":F
    const/4 v4, 0x0

    .line 51
    .local v4, "factor":F
    cmpl-float v22, v18, p9

    if-gtz v22, :cond_1

    cmpl-float v22, v20, p9

    if-ltz v22, :cond_5

    .line 52
    :cond_1
    div-float v5, p2, v14

    .line 53
    .local v5, "fitViewXUpscale":F
    div-float v6, p3, v13

    .line 54
    .local v6, "fitViewYUpscale":F
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 61
    .end local v5    # "fitViewXUpscale":F
    .end local v6    # "fitViewYUpscale":F
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p6

    move/from16 v3, p7

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->scaleMatrixAroundPoint([F[FFFF)V

    .line 94
    .end local v4    # "factor":F
    .end local v18    # "xRatio":F
    .end local v20    # "yRatio":F
    :cond_2
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getXScale([F)F

    move-result v19

    .line 95
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getYScale([F)F

    move-result v21

    .line 96
    mul-float v14, p4, v19

    .line 97
    mul-float v13, p5, v21

    .line 100
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getXTranslate([F)F

    move-result v8

    .line 101
    .local v8, "imageLeft":F
    add-float v9, v8, v14

    .line 102
    .local v9, "imageRight":F
    move/from16 v16, v8

    .line 104
    .local v16, "translateX":F
    cmpg-float v22, v14, p2

    if-gtz v22, :cond_b

    .line 106
    sub-float v22, p2, v14

    const/high16 v23, 0x3f000000    # 0.5f

    mul-float v16, v22, v23

    .line 121
    :cond_3
    :goto_2
    invoke-static/range {p0 .. p0}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->getYTranslate([F)F

    move-result v10

    .line 122
    .local v10, "imageTop":F
    add-float v7, v10, v13

    .line 123
    .local v7, "imageBottom":F
    move/from16 v17, v10

    .line 125
    .local v17, "translateY":F
    cmpg-float v22, v13, p3

    if-gtz v22, :cond_d

    .line 127
    sub-float v22, p3, v13

    const/high16 v23, 0x3f000000    # 0.5f

    mul-float v17, v22, v23

    .line 142
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->setTranslate([FFF)V

    .line 143
    return-void

    .line 56
    .end local v7    # "imageBottom":F
    .end local v8    # "imageLeft":F
    .end local v9    # "imageRight":F
    .end local v10    # "imageTop":F
    .end local v16    # "translateX":F
    .end local v17    # "translateY":F
    .restart local v4    # "factor":F
    .restart local v18    # "xRatio":F
    .restart local v20    # "yRatio":F
    :cond_5
    div-float v11, p4, v14

    .line 57
    .local v11, "intrinsicSizeXUpscale":F
    div-float v12, p5, v13

    .line 58
    .local v12, "intrinsicSizeYUpscale":F
    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto :goto_0

    .line 65
    .end local v4    # "factor":F
    .end local v11    # "intrinsicSizeXUpscale":F
    .end local v12    # "intrinsicSizeYUpscale":F
    .end local v18    # "xRatio":F
    .end local v20    # "yRatio":F
    :cond_6
    cmpl-float v22, v19, p8

    if-gtz v22, :cond_7

    cmpl-float v22, v21, p8

    if-lez v22, :cond_2

    .line 66
    :cond_7
    div-float v4, p8, v19

    .line 68
    .restart local v4    # "factor":F
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p6

    move/from16 v3, p7

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->scaleMatrixAroundPoint([F[FFFF)V

    goto :goto_1

    .line 74
    .end local v4    # "factor":F
    :cond_8
    cmpg-float v22, v14, p2

    if-gtz v22, :cond_9

    cmpg-float v22, v13, p3

    if-gtz v22, :cond_9

    .line 76
    div-float v5, p2, v14

    .line 77
    .restart local v5    # "fitViewXUpscale":F
    div-float v6, p3, v13

    .line 78
    .restart local v6    # "fitViewYUpscale":F
    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 80
    .restart local v4    # "factor":F
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p6

    move/from16 v3, p7

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->scaleMatrixAroundPoint([F[FFFF)V

    goto :goto_1

    .line 84
    .end local v4    # "factor":F
    .end local v5    # "fitViewXUpscale":F
    .end local v6    # "fitViewYUpscale":F
    :cond_9
    cmpl-float v22, v19, p8

    if-gtz v22, :cond_a

    cmpl-float v22, v21, p8

    if-lez v22, :cond_2

    .line 85
    :cond_a
    div-float v4, p8, v19

    .line 87
    .restart local v4    # "factor":F
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p6

    move/from16 v3, p7

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->scaleMatrixAroundPoint([F[FFFF)V

    goto/16 :goto_1

    .line 111
    .end local v4    # "factor":F
    .restart local v8    # "imageLeft":F
    .restart local v9    # "imageRight":F
    .restart local v16    # "translateX":F
    :cond_b
    float-to-double v0, v8

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v22, v22, v24

    if-lez v22, :cond_c

    .line 113
    const/16 v16, 0x0

    goto :goto_2

    .line 114
    :cond_c
    cmpg-float v22, v9, p2

    if-gez v22, :cond_3

    .line 116
    sub-float v16, p2, v14

    goto :goto_2

    .line 132
    .restart local v7    # "imageBottom":F
    .restart local v10    # "imageTop":F
    .restart local v17    # "translateY":F
    :cond_d
    float-to-double v0, v10

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x0

    cmpl-double v22, v22, v24

    if-lez v22, :cond_e

    .line 134
    const/16 v17, 0x0

    goto :goto_3

    .line 135
    :cond_e
    cmpg-float v22, v7, p3

    if-gez v22, :cond_4

    .line 137
    sub-float v17, p3, v13

    goto/16 :goto_3
.end method

.method public static getXScale([F)F
    .locals 1
    .param p0, "matrixComponents"    # [F

    .prologue
    .line 304
    const/4 v0, 0x0

    aget v0, p0, v0

    return v0
.end method

.method public static getXTranslate([F)F
    .locals 1
    .param p0, "matrixComponents"    # [F

    .prologue
    .line 260
    const/4 v0, 0x2

    aget v0, p0, v0

    return v0
.end method

.method public static getYScale([F)F
    .locals 1
    .param p0, "matrixComponents"    # [F

    .prologue
    .line 318
    const/4 v0, 0x4

    aget v0, p0, v0

    return v0
.end method

.method public static getYTranslate([F)F
    .locals 1
    .param p0, "matrixComponents"    # [F

    .prologue
    .line 274
    const/4 v0, 0x5

    aget v0, p0, v0

    return v0
.end method

.method public static scaleMatrixAroundPoint([F[FFFF)V
    .locals 3
    .param p0, "resultMatrixComponents"    # [F
    .param p1, "matrixComponents"    # [F
    .param p2, "pointX"    # F
    .param p3, "pointY"    # F
    .param p4, "scale"    # F

    .prologue
    .line 185
    invoke-static {p1}, Lcom/samsung/groupcast/misc/graphics/MatrixMath;->createMatrix([F)Landroid/graphics/Matrix;

    move-result-object v0

    .line 187
    .local v0, "matrix":Landroid/graphics/Matrix;
    neg-float v1, p2

    neg-float v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 189
    invoke-virtual {v0, p4, p4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 191
    invoke-virtual {v0, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 193
    invoke-virtual {v0, p0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 194
    return-void
.end method

.method public static setIdentity([F)V
    .locals 3
    .param p0, "matrixComponents"    # [F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 245
    const/4 v0, 0x0

    aput v2, p0, v0

    .line 246
    const/4 v0, 0x1

    aput v1, p0, v0

    .line 247
    const/4 v0, 0x2

    aput v1, p0, v0

    .line 248
    const/4 v0, 0x3

    aput v1, p0, v0

    .line 249
    const/4 v0, 0x4

    aput v2, p0, v0

    .line 250
    const/4 v0, 0x5

    aput v1, p0, v0

    .line 251
    const/4 v0, 0x6

    aput v1, p0, v0

    .line 252
    const/4 v0, 0x7

    aput v1, p0, v0

    .line 253
    const/16 v0, 0x8

    aput v2, p0, v0

    .line 254
    return-void
.end method

.method public static setScale([FFF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F

    .prologue
    .line 332
    const/4 v0, 0x0

    aput p1, p0, v0

    .line 333
    const/4 v0, 0x4

    aput p2, p0, v0

    .line 334
    return-void
.end method

.method public static setTranslate([FFF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "translateX"    # F
    .param p2, "translateY"    # F

    .prologue
    .line 288
    const/4 v0, 0x2

    aput p1, p0, v0

    .line 289
    const/4 v0, 0x5

    aput p2, p0, v0

    .line 290
    return-void
.end method

.method public static setXScale([FF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "value"    # F

    .prologue
    .line 311
    const/4 v0, 0x1

    aput p1, p0, v0

    .line 312
    return-void
.end method

.method public static setXTranslate([FF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "value"    # F

    .prologue
    .line 267
    const/4 v0, 0x2

    aput p1, p0, v0

    .line 268
    return-void
.end method

.method public static setYScale([FF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "value"    # F

    .prologue
    .line 325
    const/4 v0, 0x4

    aput p1, p0, v0

    .line 326
    return-void
.end method

.method public static setYTranslate([FF)V
    .locals 1
    .param p0, "matrixComponents"    # [F
    .param p1, "value"    # F

    .prologue
    .line 281
    const/4 v0, 0x5

    aput p1, p0, v0

    .line 282
    return-void
.end method

.method public static translate([FFF)V
    .locals 2
    .param p0, "matrixComponents"    # [F
    .param p1, "translateX"    # F
    .param p2, "translateY"    # F

    .prologue
    .line 296
    const/4 v0, 0x2

    aget v1, p0, v0

    add-float/2addr v1, p1

    aput v1, p0, v0

    .line 297
    const/4 v0, 0x5

    aget v1, p0, v0

    add-float/2addr v1, p2

    aput v1, p0, v0

    .line 298
    return-void
.end method
