.class public Lcom/samsung/groupcast/misc/update/UpdateCheckTask;
.super Landroid/os/AsyncTask;
.source "UpdateCheckTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static LOGGER_CHECK_URL:Ljava/lang/String; = null

.field private static final PD_TEST_PATH:Ljava/lang/String; = "/mnt/sdcard/groupcast.samsungapps"

.field private static final REQUEST_CONNECTION_TIMEOUT_MILLISECONDS:I = 0xbb8

.field private static final REQUEST_SOCKET_TIMEOUT_MILLISECONDS:I = 0xbb8

.field private static final TAG:Ljava/lang/String;

.field public static UPDATE_FAILED:I

.field public static UPDATE_GOOGLE_FORCED:I

.field public static UPDATE_GOOGLE_USERSELF:I

.field public static UPDATE_NOT_NEED:I

.field public static UPDATE_SAMSUNGAPP_FORCED:I

.field public static UPDATE_SAMSUNGAPP_USERSELF:I


# instance fields
.field private final mAppVersion:Ljava/lang/String;

.field private final mBuildModel:Ljava/lang/String;

.field private final mCSC:Ljava/lang/String;

.field private final mLocalAppVerCode:I

.field private final mMCC:Ljava/lang/String;

.field private final mMNC:Ljava/lang/String;

.field private final mPD:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    .line 23
    const-string v0, "http://bss.allshareplay.com/groupcast/one.jpg?"

    sput-object v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->LOGGER_CHECK_URL:Ljava/lang/String;

    .line 30
    const/16 v0, 0xb

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_SAMSUNGAPP_USERSELF:I

    .line 31
    const/16 v0, 0xc

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_SAMSUNGAPP_FORCED:I

    .line 32
    const/16 v0, 0x15

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_GOOGLE_USERSELF:I

    .line 33
    const/16 v0, 0x16

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_GOOGLE_FORCED:I

    .line 34
    const/16 v0, 0x9

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_NOT_NEED:I

    .line 35
    const/16 v0, 0xa

    sput v0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_FAILED:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "localAppVerCode"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->getAppPackageName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPackageName:Ljava/lang/String;

    .line 50
    invoke-static {p1}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mAppVersion:Ljava/lang/String;

    .line 51
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getBuildModel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mBuildModel:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lcom/samsung/groupcast/application/Environment;->getMCC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMCC:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lcom/samsung/groupcast/application/Environment;->getMNC(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMNC:Ljava/lang/String;

    .line 54
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getCSC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mCSC:Ljava/lang/String;

    .line 55
    invoke-static {}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->getPD()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPD:Ljava/lang/String;

    .line 56
    iput p2, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mLocalAppVerCode:I

    .line 57
    return-void
.end method

.method private static getAppPackageName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getAppVersion(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    const/4 v2, 0x0

    .line 136
    .local v2, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 137
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x40

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v2, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return-object v2

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[getAppVersion] exception:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getPD()Ljava/lang/String;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/sdcard/groupcast.samsungapps"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    const-string v1, "1"

    .line 152
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getUpdateCheckUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "targetUrl"    # Ljava/lang/String;

    .prologue
    .line 95
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 96
    .local v0, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    const-string v1, "appInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPackageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPackageName:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mAppVersion:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mAppVersion:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 99
    const-string v1, "&deviceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mBuildModel:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mBuildModel:Ljava/lang/String;

    :goto_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    const-string v1, "&mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMCC:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMCC:Ljava/lang/String;

    :goto_3
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string v1, "&mnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMNC:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mMNC:Ljava/lang/String;

    :goto_4
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    const-string v1, "&csc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mCSC:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mCSC:Ljava/lang/String;

    :goto_5
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    const-string v1, "&openApi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 104
    iget-object v1, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPD:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 105
    const-string v1, "&pd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->mPD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 97
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 98
    :cond_2
    const-string v1, ""

    goto :goto_1

    .line 99
    :cond_3
    const-string v1, ""

    goto :goto_2

    .line 100
    :cond_4
    const-string v1, ""

    goto :goto_3

    .line 101
    :cond_5
    const-string v1, ""

    goto :goto_4

    .line 102
    :cond_6
    const-string v1, ""

    goto :goto_5
.end method

.method private userLogCheck(Ljava/lang/String;Lorg/apache/http/client/HttpClient;)V
    .locals 7
    .param p1, "logCheckUri"    # Ljava/lang/String;
    .param p2, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/http/client/ClientProtocolException;
        }
    .end annotation

    .prologue
    .line 112
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 113
    .local v1, "httpGetLog":Lorg/apache/http/client/methods/HttpGet;
    invoke-interface {p2, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 114
    .local v2, "httpResponseLog":Lorg/apache/http/HttpResponse;
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 115
    .local v3, "responseCodeLog":I
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_0

    .line 116
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[doInBackground] hitlog http failed - responseCodeLog :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_0
    :try_start_0
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    const-string v5, "consume it"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 7
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 61
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->LOGGER_CHECK_URL:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->getUpdateCheckUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "logCheckUri":Ljava/lang/String;
    sget v3, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_NOT_NEED:I

    .line 67
    .local v3, "updateChecker":I
    :try_start_0
    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->isNetworkAvailable()Z

    move-result v4

    if-nez v4, :cond_0

    .line 68
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    const-string v5, "internet is not available"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    sget v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_NOT_NEED:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 91
    :goto_0
    return-object v4

    .line 73
    :cond_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 74
    .local v1, "httpClient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.protocol.expect-continue"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 75
    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.connection.timeout"

    const/16 v6, 0xbb8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 77
    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    const-string v5, "http.socket.timeout"

    const/16 v6, 0xbb8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 81
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    const-string v5, " updatecheck make log.."

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0, v2, v1}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->userLogCheck(Ljava/lang/String;Lorg/apache/http/client/HttpClient;)V

    .line 84
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    const-string v5, " updatecheck end log.."

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 86
    .end local v1    # "httpClient":Lorg/apache/http/client/HttpClient;
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[doInBackground] general exception occured - exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    sget v4, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->UPDATE_FAILED:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/misc/update/UpdateCheckTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
