.class Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/misc/utility/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 465
    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    .line 466
    .local v12, "viewRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v7

    .line 467
    .local v7, "childCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v7, :cond_0

    .line 468
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0, v8}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 469
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 470
    .local v9, "left":I
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v10

    .line 471
    .local v10, "right":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v11

    .line 472
    .local v11, "top":I
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 473
    .local v6, "bottom":I
    invoke-virtual {v12, v9, v11, v10, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 474
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v12, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$600(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$600(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v8

    iget-object v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v4, v4, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v8

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 483
    .end local v2    # "child":Landroid/view/View;
    .end local v6    # "bottom":I
    .end local v9    # "left":I
    .end local v10    # "right":I
    .end local v11    # "top":I
    :cond_0
    return-void

    .line 467
    .restart local v2    # "child":Landroid/view/View;
    .restart local v6    # "bottom":I
    .restart local v9    # "left":I
    .restart local v10    # "right":I
    .restart local v11    # "top":I
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v4, 0x1

    .line 425
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 427
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    monitor-enter v1

    .line 428
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget v2, v0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    float-to-int v3, p3

    add-int/2addr v2, v3

    iput v2, v0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 429
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 432
    return v4

    .line 429
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 437
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 438
    .local v11, "viewRect":Landroid/graphics/Rect;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    invoke-virtual {v0, v7}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 440
    .local v2, "child":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v8

    .line 441
    .local v8, "left":I
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v9

    .line 442
    .local v9, "right":I
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v10

    .line 443
    .local v10, "top":I
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 444
    .local v6, "bottom":I
    invoke-virtual {v11, v8, v10, v9, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 445
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v11, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 446
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$200(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$200(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    iget-object v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v4, v4, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v7

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$400(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$400(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v7

    iget-object v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v4, v4, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v7

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 453
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # getter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v7

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setSelection(I)V

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;->this$0:Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    # setter for: Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedView:Landroid/view/View;
    invoke-static {v0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->access$502(Lcom/samsung/groupcast/misc/utility/HorizontalListView;Landroid/view/View;)Landroid/view/View;

    .line 460
    .end local v2    # "child":Landroid/view/View;
    .end local v6    # "bottom":I
    .end local v8    # "left":I
    .end local v9    # "right":I
    .end local v10    # "top":I
    :cond_2
    const/4 v0, 0x1

    return v0

    .line 438
    .restart local v2    # "child":Landroid/view/View;
    .restart local v6    # "bottom":I
    .restart local v8    # "left":I
    .restart local v9    # "right":I
    .restart local v10    # "top":I
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method
