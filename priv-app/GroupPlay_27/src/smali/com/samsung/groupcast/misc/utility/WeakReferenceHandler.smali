.class public abstract Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;
.super Landroid/os/Handler;
.source "WeakReferenceHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# instance fields
.field private mReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 11
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;, "Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler<TT;>;"
    .local p1, "reference":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 12
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;->mReference:Ljava/lang/ref/WeakReference;

    .line 13
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 17
    .local p0, "this":Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;, "Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler<TT;>;"
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;->mReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 21
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;->mReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/groupcast/misc/utility/WeakReferenceHandler;->handleMessage(Ljava/lang/Object;Landroid/os/Message;)V

    goto :goto_0
.end method

.method protected abstract handleMessage(Ljava/lang/Object;Landroid/os/Message;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/os/Message;",
            ")V"
        }
    .end annotation
.end method
