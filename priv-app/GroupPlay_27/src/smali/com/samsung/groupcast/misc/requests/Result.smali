.class public final enum Lcom/samsung/groupcast/misc/requests/Result;
.super Ljava/lang/Enum;
.source "Result.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/misc/requests/Result;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum DATA_UNAVAILABLE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum DRM:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum FETCH_FAILURE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum INVALID_ID_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum LOAD_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum NO_DRM:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

.field public static final enum TOO_MANY_PAGE:Lcom/samsung/groupcast/misc/requests/Result;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    .line 5
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "INVALID_ID_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->INVALID_ID_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 6
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "DATA_UNAVAILABLE_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->DATA_UNAVAILABLE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 7
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "LOAD_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->LOAD_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 8
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "IO_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 9
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "FETCH_FAILURE_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->FETCH_FAILURE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 10
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "TIMEOUT_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    .line 11
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "DRM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->DRM:Lcom/samsung/groupcast/misc/requests/Result;

    .line 12
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "NO_DRM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->NO_DRM:Lcom/samsung/groupcast/misc/requests/Result;

    .line 13
    new-instance v0, Lcom/samsung/groupcast/misc/requests/Result;

    const-string v1, "TOO_MANY_PAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/misc/requests/Result;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->TOO_MANY_PAGE:Lcom/samsung/groupcast/misc/requests/Result;

    .line 3
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/groupcast/misc/requests/Result;

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->INVALID_ID_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->DATA_UNAVAILABLE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->LOAD_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/misc/requests/Result;->IO_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/groupcast/misc/requests/Result;->FETCH_FAILURE_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/groupcast/misc/requests/Result;->TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/groupcast/misc/requests/Result;->DRM:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/groupcast/misc/requests/Result;->NO_DRM:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/groupcast/misc/requests/Result;->TOO_MANY_PAGE:Lcom/samsung/groupcast/misc/requests/Result;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/misc/requests/Result;->$VALUES:[Lcom/samsung/groupcast/misc/requests/Result;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/misc/requests/Result;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/samsung/groupcast/misc/requests/Result;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/misc/requests/Result;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/misc/requests/Result;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/samsung/groupcast/misc/requests/Result;->$VALUES:[Lcom/samsung/groupcast/misc/requests/Result;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/misc/requests/Result;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/misc/requests/Result;

    return-object v0
.end method


# virtual methods
.method public isError()Z
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/groupcast/misc/requests/Result;->SUCCESS:Lcom/samsung/groupcast/misc/requests/Result;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTimeout()Z
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/samsung/groupcast/misc/requests/Result;->TIMEOUT_ERROR:Lcom/samsung/groupcast/misc/requests/Result;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
