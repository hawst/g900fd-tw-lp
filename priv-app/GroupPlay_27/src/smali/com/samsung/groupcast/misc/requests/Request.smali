.class public interface abstract Lcom/samsung/groupcast/misc/requests/Request;
.super Ljava/lang/Object;
.source "Request.java"


# static fields
.field public static final PRIORITY_HIGH:I = 0x2

.field public static final PRIORITY_LOW:I = 0x0

.field public static final PRIORITY_MEDIUM:I = 0x1


# virtual methods
.method public abstract getPriority()I
.end method

.method public abstract getPriorityTimestamp()J
.end method

.method public abstract invalidate()V
.end method

.method public abstract isCompleted()Z
.end method

.method public abstract isInvalidated()Z
.end method

.method public abstract setPriority(I)V
.end method

.method public abstract start()V
.end method
