.class public Lcom/samsung/groupcast/misc/utility/StringTools;
.super Ljava/lang/Object;
.source "StringTools.java"


# static fields
.field public static final FONT_TYPE_ROBOTO_LIGHT:Ljava/lang/String; = "ROBOTO_LIGHT"

.field public static final FONT_TYPE_ROBOTO_REGULAR:Ljava/lang/String; = "ROBOTO_REGULAR"

.field public static final FONT_TYPE_SAMSUNG_SANS_BOLD:Ljava/lang/String; = "SAMSUNG_SANS_BOLD"

.field private static final HEX_DIGITS:[C

.field private static final INTERNAL_ENCRYPT_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final INTERNAL_ENCRYPT_KEYWORD:Ljava/lang/String; = "GroupPlay"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/groupcast/misc/utility/StringTools;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteArrayToHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    .line 225
    const-string v0, "0123456789ABCDEF"

    .line 226
    .local v0, "hexArray":Ljava/lang/String;
    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    new-array v1, v3, [C

    .line 227
    .local v1, "hexChars":[C
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    .line 228
    mul-int/lit8 v3, v2, 0x2

    const-string v4, "0123456789ABCDEF"

    aget-byte v5, p0, v2

    and-int/lit16 v5, v5, 0xf0

    shr-int/lit8 v5, v5, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v1, v3

    .line 229
    mul-int/lit8 v3, v2, 0x2

    add-int/lit8 v3, v3, 0x1

    const-string v4, "0123456789ABCDEF"

    aget-byte v5, p0, v2

    and-int/lit8 v5, v5, 0xf

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v1, v3

    .line 227
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 231
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([C)V

    return-object v3
.end method

.method public static decryptAsAES(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 259
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x0

    .line 260
    .local v6, "result":[B
    const/4 v2, 0x0

    .line 261
    .local v2, "key":Ljavax/crypto/spec/SecretKeySpec;
    move-object v4, p0

    .line 264
    .local v4, "res":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "SHA-1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    const-string v8, "GroupPlay"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v7

    const-string v8, "AES"

    invoke-direct {v3, v7, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    .end local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_1
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 268
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 269
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/StringTools;->hexStringToByteArray(Ljava/lang/String;)[B

    move-result-object v7

    invoke-virtual {v0, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 274
    if-eqz v0, :cond_0

    if-eqz v6, :cond_0

    .line 275
    new-instance v4, Ljava/lang/String;

    .end local v4    # "res":Ljava/lang/String;
    invoke-direct {v4, v6}, Ljava/lang/String;-><init>([B)V

    .restart local v4    # "res":Ljava/lang/String;
    :cond_0
    move-object v2, v3

    .end local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    move-object v5, v4

    .line 277
    .end local v4    # "res":Ljava/lang/String;
    .local v5, "res":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 270
    .end local v5    # "res":Ljava/lang/String;
    .restart local v4    # "res":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v5, v4

    .line 272
    .end local v4    # "res":Ljava/lang/String;
    .restart local v5    # "res":Ljava/lang/String;
    goto :goto_0

    .line 270
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v5    # "res":Ljava/lang/String;
    .restart local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v4    # "res":Ljava/lang/String;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_1
.end method

.method public static encryptAsAES(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v6, 0x0

    .line 237
    .local v6, "result":[B
    const/4 v2, 0x0

    .line 238
    .local v2, "key":Ljavax/crypto/spec/SecretKeySpec;
    move-object v4, p0

    .line 241
    .local v4, "res":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v7, "SHA-1"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    const-string v8, "GroupPlay"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v7

    const-string v8, "AES"

    invoke-direct {v3, v7, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    .end local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_1
    const-string v7, "AES"

    invoke-static {v7}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 245
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 246
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    invoke-virtual {v0, v7}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 251
    if-eqz v0, :cond_0

    if-eqz v6, :cond_0

    .line 252
    invoke-static {v6}, Lcom/samsung/groupcast/misc/utility/StringTools;->byteArrayToHexString([B)Ljava/lang/String;

    move-result-object v4

    :cond_0
    move-object v2, v3

    .end local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    move-object v5, v4

    .line 254
    .end local v4    # "res":Ljava/lang/String;
    .local v5, "res":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 247
    .end local v5    # "res":Ljava/lang/String;
    .restart local v4    # "res":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 248
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move-object v5, v4

    .line 249
    .end local v4    # "res":Ljava/lang/String;
    .restart local v5    # "res":Ljava/lang/String;
    goto :goto_0

    .line 247
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v5    # "res":Ljava/lang/String;
    .restart local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v4    # "res":Ljava/lang/String;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_1
.end method

.method public static varargs getDebugString(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .param p1, "keyValuePairs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 177
    .local p0, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-nez p1, :cond_0

    .line 178
    const/4 v6, 0x0

    .line 194
    :goto_0
    return-object v6

    .line 181
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    .local v0, "builder":Ljava/lang/StringBuilder;
    array-length v1, p1

    .line 183
    .local v1, "count":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " {"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_1

    .line 186
    aget-object v4, p1, v3

    check-cast v4, Ljava/lang/String;

    .line 187
    .local v4, "key":Ljava/lang/String;
    add-int/lit8 v6, v3, 0x1

    aget-object v5, p1, v6

    .line 188
    .local v5, "value":Ljava/lang/Object;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 191
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/Object;
    :cond_1
    const-string v6, " }"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 193
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "count":I
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 194
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "# StringTools.getDebugString(...) exception #"

    goto :goto_0
.end method

.method public static getHexString([B)Ljava/lang/String;
    .locals 5
    .param p0, "array"    # [B

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 43
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/16 v4, 0x10

    new-array v1, v4, [C

    fill-array-data v1, :array_0

    .line 46
    .local v1, "hex":[C
    array-length v3, p0

    .line 48
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 49
    aget-byte v4, p0, v2

    and-int/lit16 v4, v4, 0xf0

    ushr-int/lit8 v4, v4, 0x4

    aget-char v4, v1, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 50
    aget-byte v4, p0, v2

    and-int/lit8 v4, v4, 0xf

    aget-char v4, v1, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 48
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 43
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public static getLogHexString([BII)Ljava/lang/String;
    .locals 10
    .param p0, "array"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 102
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v7, "result":Ljava/lang/StringBuilder;
    const/16 v8, 0x10

    new-array v4, v8, [B

    .line 105
    .local v4, "line":[B
    const/4 v5, 0x0

    .line 107
    .local v5, "lineIndex":I
    const-string v8, "\n0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-static {p1}, Lcom/samsung/groupcast/misc/utility/StringTools;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    move v2, p1

    .local v2, "i":I
    :goto_0
    add-int v8, p1, p2

    if-ge v2, v8, :cond_3

    .line 111
    const/16 v8, 0x10

    if-ne v5, v8, :cond_2

    .line 112
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    const/16 v8, 0x10

    if-ge v3, v8, :cond_1

    .line 115
    aget-byte v8, v4, v3

    const/16 v9, 0x20

    if-le v8, v9, :cond_0

    aget-byte v8, v4, v3

    const/16 v9, 0x7e

    if-ge v8, v9, :cond_0

    .line 116
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x1

    invoke-direct {v8, v4, v3, v9}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 118
    :cond_0
    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 122
    :cond_1
    const-string v8, "\n0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/StringTools;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const/4 v5, 0x0

    .line 127
    .end local v3    # "j":I
    :cond_2
    aget-byte v0, p0, v2

    .line 128
    .local v0, "b":B
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    sget-object v8, Lcom/samsung/groupcast/misc/utility/StringTools;->HEX_DIGITS:[C

    ushr-int/lit8 v9, v0, 0x4

    and-int/lit8 v9, v9, 0xf

    aget-char v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    sget-object v8, Lcom/samsung/groupcast/misc/utility/StringTools;->HEX_DIGITS:[C

    and-int/lit8 v9, v0, 0xf

    aget-char v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 132
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "lineIndex":I
    .local v6, "lineIndex":I
    aput-byte v0, v4, v5

    .line 110
    add-int/lit8 v2, v2, 0x1

    move v5, v6

    .end local v6    # "lineIndex":I
    .restart local v5    # "lineIndex":I
    goto :goto_0

    .line 135
    .end local v0    # "b":B
    :cond_3
    const/16 v8, 0x10

    if-eq v5, v8, :cond_6

    .line 136
    rsub-int/lit8 v8, v5, 0x10

    mul-int/lit8 v1, v8, 0x3

    .line 137
    .local v1, "count":I
    add-int/lit8 v1, v1, 0x1

    .line 138
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_4

    .line 139
    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 142
    :cond_4
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v5, :cond_6

    .line 143
    aget-byte v8, v4, v2

    const/16 v9, 0x20

    if-le v8, v9, :cond_5

    aget-byte v8, v4, v2

    const/16 v9, 0x7e

    if-ge v8, v9, :cond_5

    .line 144
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x1

    invoke-direct {v8, v4, v2, v9}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 146
    :cond_5
    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 151
    .end local v1    # "count":I
    :cond_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static varargs getLogString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .param p0, "keyValuePairs"    # [Ljava/lang/Object;

    .prologue
    .line 156
    if-nez p0, :cond_0

    .line 157
    const/4 v6, 0x0

    .line 171
    :goto_0
    return-object v6

    .line 160
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    .local v0, "builder":Ljava/lang/StringBuilder;
    array-length v1, p0

    .line 163
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_1

    .line 164
    aget-object v4, p0, v3

    check-cast v4, Ljava/lang/String;

    .line 165
    .local v4, "key":Ljava/lang/String;
    add-int/lit8 v6, v3, 0x1

    aget-object v5, p0, v6

    .line 166
    .local v5, "value":Ljava/lang/Object;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    add-int/lit8 v3, v3, 0x2

    goto :goto_1

    .line 169
    .end local v4    # "key":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 170
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "count":I
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 171
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "# StringTools.getLogString(...) exception #"

    goto :goto_0
.end method

.method public static getSHA1Digest(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 27
    .local v0, "digest":Ljava/lang/String;
    :try_start_0
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 28
    .local v3, "messageDigest":Ljava/security/MessageDigest;
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 29
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 30
    .local v2, "messageBytes":[B
    invoke-static {v2}, Lcom/samsung/groupcast/misc/utility/StringTools;->getHexString([B)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    .line 37
    .end local v2    # "messageBytes":[B
    .end local v3    # "messageDigest":Ljava/security/MessageDigest;
    :goto_0
    return-object v4

    .line 31
    :catch_0
    move-exception v1

    .line 32
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    const-class v4, Lcom/samsung/groupcast/misc/utility/StringTools;

    const-string v5, "getSHA1Digest"

    const-string v6, "could not get instance of SHA-1 digest"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "exception"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v1, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->wx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    .line 34
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static hexStringToByteArray(Ljava/lang/String;)[B
    .locals 7
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x10

    .line 214
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 215
    .local v2, "len":I
    div-int/lit8 v3, v2, 0x2

    new-array v0, v3, [B

    .line 216
    .local v0, "data":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 217
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 216
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 221
    :cond_0
    return-object v0
.end method

.method public static isContains(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "orgStr"    # Ljava/lang/String;
    .param p1, "targetStr"    # Ljava/lang/String;
    .param p2, "delimiters"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 281
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 291
    :cond_0
    :goto_0
    return v1

    .line 285
    :cond_1
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p0, p2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    .local v0, "strToken":Ljava/util/StringTokenizer;
    :cond_2
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 288
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static parseIntSafely(Ljava/lang/String;I)I
    .locals 1
    .param p0, "content"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 199
    if-nez p0, :cond_0

    .line 205
    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .line 203
    .restart local p1    # "defaultValue":I
    :cond_0
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method public static setTypeFace(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 296
    const/4 v1, 0x0

    .line 297
    .local v1, "tf":Landroid/graphics/Typeface;
    :try_start_0
    const-string v2, "ROBOTO_LIGHT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 298
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "fonts/Roboto-Light.ttf"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/misc/graphics/Typefaces;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 307
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 308
    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 315
    :cond_1
    :goto_1
    return-void

    .line 300
    :cond_2
    const-string v2, "ROBOTO_REGULAR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 301
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "fonts/Roboto-Regular.ttf"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/misc/graphics/Typefaces;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    goto :goto_0

    .line 303
    :cond_3
    const-string v2, "SAMSUNG_SANS_BOLD"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    const-string v3, "fonts/Samsung Sans Bold.ttf"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/misc/graphics/Typefaces;->get(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    goto :goto_0

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_1

    .line 312
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static toByteArray(B)[B
    .locals 2
    .param p0, "b"    # B

    .prologue
    .line 83
    const/4 v1, 0x1

    new-array v0, v1, [B

    .line 84
    .local v0, "array":[B
    const/4 v1, 0x0

    aput-byte p0, v0, v1

    .line 85
    return-object v0
.end method

.method public static toByteArray(I)[B
    .locals 3
    .param p0, "i"    # I

    .prologue
    .line 89
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 91
    .local v0, "array":[B
    const/4 v1, 0x3

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 92
    const/4 v1, 0x2

    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 93
    const/4 v1, 0x1

    shr-int/lit8 v2, p0, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 94
    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 96
    return-object v0
.end method

.method public static toHexString(I)Ljava/lang/String;
    .locals 1
    .param p0, "i"    # I

    .prologue
    .line 79
    invoke-static {p0}, Lcom/samsung/groupcast/misc/utility/StringTools;->toByteArray(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/misc/utility/StringTools;->toHexString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHexString([B)Ljava/lang/String;
    .locals 2
    .param p0, "array"    # [B

    .prologue
    .line 62
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/samsung/groupcast/misc/utility/StringTools;->toHexString([BII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHexString([BII)Ljava/lang/String;
    .locals 7
    .param p0, "array"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 66
    mul-int/lit8 v5, p2, 0x2

    new-array v1, v5, [C

    .line 68
    .local v1, "buf":[C
    const/4 v2, 0x0

    .line 69
    .local v2, "bufIndex":I
    move v4, p1

    .local v4, "i":I
    move v3, v2

    .end local v2    # "bufIndex":I
    .local v3, "bufIndex":I
    :goto_0
    add-int v5, p1, p2

    if-ge v4, v5, :cond_0

    .line 70
    aget-byte v0, p0, v4

    .line 71
    .local v0, "b":B
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "bufIndex":I
    .restart local v2    # "bufIndex":I
    sget-object v5, Lcom/samsung/groupcast/misc/utility/StringTools;->HEX_DIGITS:[C

    ushr-int/lit8 v6, v0, 0x4

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    aput-char v5, v1, v3

    .line 72
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "bufIndex":I
    .restart local v3    # "bufIndex":I
    sget-object v5, Lcom/samsung/groupcast/misc/utility/StringTools;->HEX_DIGITS:[C

    and-int/lit8 v6, v0, 0xf

    aget-char v5, v5, v6

    aput-char v5, v1, v2

    .line 69
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 75
    .end local v0    # "b":B
    :cond_0
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v1}, Ljava/lang/String;-><init>([C)V

    return-object v5
.end method
