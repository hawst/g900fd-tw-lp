.class public abstract Lcom/samsung/groupcast/misc/requests/BaseRequest;
.super Ljava/lang/Object;
.source "BaseRequest.java"

# interfaces
.implements Lcom/samsung/groupcast/misc/requests/Request;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/samsung/groupcast/misc/requests/RequestListener;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/samsung/groupcast/misc/requests/Request;"
    }
.end annotation


# instance fields
.field private mCompleted:Z

.field private mInvalidated:Z

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<T",
            "L;",
            ">;"
        }
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;

.field private mPriority:I

.field private mPriorityTimestamp:J

.field private mStarted:Z

.field private mStarter:Lcom/samsung/groupcast/misc/requests/Starter;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/misc/requests/Starter;)V
    .locals 2
    .param p1, "starter"    # Lcom/samsung/groupcast/misc/requests/Starter;

    .prologue
    .line 20
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    .line 18
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mPriorityTimestamp:J

    .line 21
    const-string v0, "starter"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 22
    iput-object p1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarter:Lcom/samsung/groupcast/misc/requests/Starter;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/requests/BaseRequest;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/requests/BaseRequest;

    .prologue
    .line 10
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/misc/requests/BaseRequest;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/requests/BaseRequest;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    .local p1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;, "TL;"
    const-string v0, "listener"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    .line 29
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    :cond_0
    return-void
.end method

.method protected complete(Lcom/samsung/groupcast/misc/requests/Notifier;)V
    .locals 2
    .param p1, "notifier"    # Lcom/samsung/groupcast/misc/requests/Notifier;

    .prologue
    .line 156
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 157
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    if-eqz v0, :cond_0

    .line 158
    monitor-exit v1

    .line 183
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    if-eqz v0, :cond_1

    .line 162
    monitor-exit v1

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 165
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    .line 166
    new-instance v0, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;

    invoke-direct {v0, p0, p1}, Lcom/samsung/groupcast/misc/requests/BaseRequest$2;-><init>(Lcom/samsung/groupcast/misc/requests/BaseRequest;Lcom/samsung/groupcast/misc/requests/Notifier;)V

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 182
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getPriority()I
    .locals 2

    .prologue
    .line 107
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mPriority:I

    monitor-exit v1

    return v0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPriorityTimestamp()J
    .locals 4

    .prologue
    .line 134
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-wide v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mPriorityTimestamp:J

    monitor-exit v1

    return-wide v2

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public invalidate()V
    .locals 4

    .prologue
    .line 85
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 86
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    if-eqz v2, :cond_0

    .line 87
    monitor-exit v3

    .line 103
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    if-eqz v2, :cond_1

    .line 91
    monitor-exit v3

    goto :goto_0

    .line 102
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 94
    :cond_1
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    .line 95
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarter:Lcom/samsung/groupcast/misc/requests/Starter;

    .line 97
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/misc/requests/RequestListener;

    .line 98
    .local v1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/misc/requests/RequestListener;->onInvalidated(Lcom/samsung/groupcast/misc/requests/Request;)V

    goto :goto_1

    .line 101
    .end local v1    # "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    :cond_2
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 102
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public isCompleted()Z
    .locals 2

    .prologue
    .line 71
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    monitor-exit v1

    return v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isInvalidated()Z
    .locals 2

    .prologue
    .line 78
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    monitor-exit v1

    return v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeListener(Lcom/samsung/groupcast/misc/requests/RequestListener;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    .local p1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;, "TL;"
    const-string v0, "listener"

    invoke-static {v0, p1}, Lcom/samsung/groupcast/misc/utility/Verify;->notNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    .line 37
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public setPriority(I)V
    .locals 6
    .param p1, "priority"    # I

    .prologue
    .line 114
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 115
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    if-eqz v2, :cond_0

    .line 116
    monitor-exit v3

    .line 130
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    if-eqz v2, :cond_1

    .line 120
    monitor-exit v3

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 123
    :cond_1
    :try_start_1
    iput p1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mPriority:I

    .line 124
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mPriorityTimestamp:J

    .line 126
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/misc/requests/RequestListener;

    .line 127
    .local v1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    invoke-virtual {v1, p0}, Lcom/samsung/groupcast/misc/requests/RequestListener;->onPriorityChanged(Lcom/samsung/groupcast/misc/requests/Request;)V

    goto :goto_1

    .line 129
    .end local v1    # "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 42
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-boolean v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarted:Z

    if-eqz v1, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    if-nez v1, :cond_0

    .line 50
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarted:Z

    .line 51
    iget-object v0, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarter:Lcom/samsung/groupcast/misc/requests/Starter;

    .line 53
    .local v0, "starter":Lcom/samsung/groupcast/misc/requests/Starter;
    new-instance v1, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;

    invoke-direct {v1, p0, v0}, Lcom/samsung/groupcast/misc/requests/BaseRequest$1;-><init>(Lcom/samsung/groupcast/misc/requests/BaseRequest;Lcom/samsung/groupcast/misc/requests/Starter;)V

    invoke-static {v1}, Lcom/samsung/groupcast/application/MainQueue;->post(Ljava/lang/Runnable;)V

    .line 66
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mStarter:Lcom/samsung/groupcast/misc/requests/Starter;

    goto :goto_0
.end method

.method protected updateListeners(Lcom/samsung/groupcast/misc/requests/Notifier;)V
    .locals 4
    .param p1, "notifier"    # Lcom/samsung/groupcast/misc/requests/Notifier;

    .prologue
    .line 140
    .local p0, "this":Lcom/samsung/groupcast/misc/requests/BaseRequest;, "Lcom/samsung/groupcast/misc/requests/BaseRequest<TL;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 141
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mCompleted:Z

    if-eqz v2, :cond_0

    .line 142
    monitor-exit v3

    .line 153
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mInvalidated:Z

    if-eqz v2, :cond_1

    .line 146
    monitor-exit v3

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 149
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/groupcast/misc/requests/BaseRequest;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/groupcast/misc/requests/RequestListener;

    .line 150
    .local v1, "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    invoke-interface {p1, p0, v1}, Lcom/samsung/groupcast/misc/requests/Notifier;->notifyListener(Lcom/samsung/groupcast/misc/requests/Request;Lcom/samsung/groupcast/misc/requests/RequestListener;)V

    goto :goto_1

    .line 152
    .end local v1    # "listener":Lcom/samsung/groupcast/misc/requests/RequestListener;
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
