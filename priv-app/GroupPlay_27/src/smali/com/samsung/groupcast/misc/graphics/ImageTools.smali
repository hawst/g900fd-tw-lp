.class public Lcom/samsung/groupcast/misc/graphics/ImageTools;
.super Ljava/lang/Object;
.source "ImageTools.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    return-void
.end method

.method public static computeCenterImageMatrix(FFFFFF)Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;
    .locals 10
    .param p0, "imageWidth"    # F
    .param p1, "imageHeight"    # F
    .param p2, "scaleX"    # F
    .param p3, "scaleY"    # F
    .param p4, "viewWidth"    # F
    .param p5, "viewHeight"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 100
    mul-float v9, p0, p2

    .line 101
    .local v9, "scaledImageWidth":F
    mul-float v8, p1, p3

    .line 102
    .local v8, "scaledImageHeight":F
    sub-float v0, p4, v9

    mul-float v6, v0, v2

    .line 103
    .local v6, "dx":F
    sub-float v0, p5, v8

    mul-float v7, v0, v2

    .line 104
    .local v7, "dy":F
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 105
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 106
    new-instance v0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;

    move v2, p0

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;-><init>(Landroid/graphics/Matrix;FFFF)V

    return-object v0
.end method

.method public static computeRotateToNaturalOrientationMatrix(FFI)Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;
    .locals 6
    .param p0, "imageWidth"    # F
    .param p1, "imageHeight"    # F
    .param p2, "orientation"    # I

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 45
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 46
    .local v1, "matrix":Landroid/graphics/Matrix;
    move v2, p0

    .line 47
    .local v2, "outImageWidth":F
    move v3, p1

    .line 49
    .local v3, "outImageHeight":F
    packed-switch p2, :pswitch_data_0

    .line 73
    :goto_0
    :pswitch_0
    new-instance v0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;-><init>(Landroid/graphics/Matrix;FFFF)V

    return-object v0

    .line 51
    :pswitch_1
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 52
    invoke-virtual {v1, p1, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 53
    move v2, p1

    .line 54
    move v3, p0

    .line 55
    goto :goto_0

    .line 59
    :pswitch_2
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 60
    invoke-virtual {v1, p0, p1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 65
    :pswitch_3
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 66
    invoke-virtual {v1, v5, p0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 67
    move v2, p1

    .line 68
    move v3, p0

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static computeScaleToFitMatrix(FFFFFF)Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;
    .locals 9
    .param p0, "imageWidth"    # F
    .param p1, "imageHeight"    # F
    .param p2, "viewWidth"    # F
    .param p3, "viewHeight"    # F
    .param p4, "upscaleThreshold"    # F
    .param p5, "minViewFill"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 78
    const/high16 v4, 0x3f800000    # 1.0f

    .line 79
    .local v4, "scale":F
    div-float v8, p0, p2

    .line 80
    .local v8, "widthRatio":F
    div-float v6, p1, p3

    .line 81
    .local v6, "heightRatio":F
    invoke-static {v8, v6}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 83
    .local v7, "maxRatio":F
    cmpl-float v0, v8, v2

    if-gtz v0, :cond_0

    cmpl-float v0, v6, v2

    if-lez v0, :cond_1

    .line 84
    :cond_0
    div-float v4, v2, v7

    .line 93
    :goto_0
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 94
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 95
    new-instance v0, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;

    move v2, p0

    move v3, p1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/groupcast/misc/graphics/ImageTools$ComputeResult;-><init>(Landroid/graphics/Matrix;FFFF)V

    return-object v0

    .line 85
    .end local v1    # "matrix":Landroid/graphics/Matrix;
    :cond_1
    cmpl-float v0, v7, p4

    if-ltz v0, :cond_2

    .line 86
    div-float v4, v2, v7

    goto :goto_0

    .line 87
    :cond_2
    cmpg-float v0, v7, p5

    if-gez v0, :cond_3

    .line 88
    div-float v4, p5, v7

    goto :goto_0

    .line 90
    :cond_3
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_0
.end method
