.class public Lcom/samsung/groupcast/misc/utility/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field public mAlwaysOverrideTouch:Z

.field private mChildWidth:I

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private final mDataObserver:Landroid/database/DataSetObserver;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private final mLayoutRequest:Ljava/lang/Runnable;

.field private mLeftViewIndex:I

.field private mMaxX:I

.field protected mNextX:I

.field private final mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mPosition:I

.field private final mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightViewIndex:I

.field protected mScroller:Landroid/widget/Scroller;

.field private mSelectedPosition:I

.field private mSelectedView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAlwaysOverrideTouch:Z

    .line 50
    iput v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    .line 51
    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    .line 54
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    .line 55
    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    .line 58
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 62
    iput-boolean v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataChanged:Z

    .line 65
    iput v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedPosition:I

    .line 67
    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    .line 68
    iput v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mPosition:I

    .line 70
    new-instance v0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView$1;-><init>(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLayoutRequest:Ljava/lang/Runnable;

    .line 120
    new-instance v0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView$2;-><init>(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 411
    new-instance v0, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView$3;-><init>(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 81
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setHorizontalScrollBarEnabled(Z)V

    .line 85
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setScrollBarStyle(I)V

    .line 86
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setScrollbarFadingEnabled(Z)V

    .line 87
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setWillNotDraw(Z)V

    .line 89
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->initView()V

    .line 90
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/groupcast/misc/utility/HorizontalListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/groupcast/misc/utility/HorizontalListView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/misc/utility/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/misc/utility/HorizontalListView;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "viewPos"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x2

    const/high16 v5, -0x80000000

    const/4 v3, 0x0

    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 191
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 192
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 195
    .restart local v0    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 196
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getHeight()I

    move-result v4

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p1, v1, v4}, Landroid/view/View;->measure(II)V

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    .line 202
    const v1, 0x7f070001

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedPosition:I

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 205
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iget v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 206
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setHorizontalScrollBarEnabled(Z)V

    .line 207
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->awakenScrollBars()Z

    .line 212
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->centerInParentIfNeeded(IZ)V

    .line 213
    return-void

    :cond_1
    move v1, v3

    .line 202
    goto :goto_0

    .line 209
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setHorizontalScrollBarEnabled(Z)V

    goto :goto_1
.end method

.method private fillList(I)V
    .locals 3
    .param p1, "dx"    # I

    .prologue
    .line 264
    const/4 v1, 0x0

    .line 265
    .local v1, "edge":I
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 266
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 269
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->fillListRight(II)V

    .line 271
    const/4 v1, 0x0

    .line 272
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_1

    .line 274
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 276
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->fillListLeft(II)V

    .line 277
    return-void
.end method

.method private fillListLeft(II)V
    .locals 4
    .param p1, "leftEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 293
    :goto_0
    add-int v1, p1, p2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    if-ltz v1, :cond_0

    .line 294
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 295
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 296
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 297
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    .line 298
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    goto :goto_0

    .line 300
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private fillListRight(II)V
    .locals 4
    .param p1, "rightEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 280
    :goto_0
    add-int v1, p1, p2

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 283
    .local v0, "child":Landroid/view/View;
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 284
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr p1, v1

    .line 286
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setMaxX()V

    .line 287
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    goto :goto_0

    .line 290
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private declared-synchronized initView()V
    .locals 3

    .prologue
    .line 93
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 98
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    .line 99
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    .line 100
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedView:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 7
    .param p1, "dx"    # I

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 332
    iget v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    .line 333
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    .line 334
    .local v3, "left":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 335
    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 336
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 337
    .local v1, "childWidth":I
    const/4 v4, 0x0

    add-int v5, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 338
    add-int/2addr v3, v1

    .line 334
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 341
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childWidth":I
    .end local v2    # "i":I
    .end local v3    # "left":I
    :cond_0
    return-void
.end method

.method private removeNonVisibleItems(I)V
    .locals 4
    .param p1, "dx"    # I

    .prologue
    const/4 v3, 0x0

    .line 303
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 306
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 307
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setHorizontalScrollBarEnabled(Z)V

    .line 308
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->awakenScrollBars()Z

    .line 313
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_1

    .line 314
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDisplayOffset:I

    .line 315
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 316
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 317
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLeftViewIndex:I

    .line 318
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 310
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setHorizontalScrollBarEnabled(Z)V

    goto :goto_0

    .line 321
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 322
    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 323
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 324
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 325
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mRightViewIndex:I

    .line 326
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 328
    :cond_2
    return-void
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->initView()V

    .line 163
    monitor-enter p0

    .line 164
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->removeAllViewsInLayout()V

    .line 165
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 167
    return-void

    .line 165
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private setMaxX()V
    .locals 4

    .prologue
    .line 344
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    if-nez v0, :cond_1

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0032

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    .line 349
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    if-gez v0, :cond_0

    .line 350
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    goto :goto_0
.end method


# virtual methods
.method public centerInParentIfNeeded(IZ)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "isLayoutChange"    # Z

    .prologue
    const/4 v2, -0x1

    .line 515
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 516
    .local v1, "listLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v1, :cond_0

    .line 517
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    .end local v1    # "listLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 521
    .restart local v1    # "listLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    const/4 v0, 0x0

    .line 522
    .local v0, "leftPadding":I
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int/2addr v2, v3

    if-ge v2, p1, :cond_1

    .line 523
    iget-object v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int/2addr v2, v3

    sub-int v2, p1, v2

    div-int/lit8 v0, v2, 0x2

    .line 525
    :cond_1
    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 526
    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 527
    if-eqz p2, :cond_2

    .line 528
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setMaxX()V

    .line 530
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 531
    return-void
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 1

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 5

    .prologue
    .line 497
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    iget v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int v2, v3, v4

    .line 500
    .local v2, "totalWidth":I
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v4

    sub-int v4, v2, v4

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 501
    .local v1, "scrollbarOffset":F
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    mul-float v0, v1, v3

    .line 502
    .local v0, "correction":F
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    .line 503
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->postInvalidate()V

    .line 505
    sub-float v3, v1, v0

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    sub-float v3, v1, v0

    float-to-int v3, v3

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 1

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v0

    return v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 388
    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 389
    .local v0, "handled":Z
    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedPosition()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedPosition:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedView:Landroid/view/View;

    return-object v0
.end method

.method protected onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 407
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 408
    return v1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 487
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 488
    return-void
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 393
    monitor-enter p0

    .line 394
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v9

    .line 395
    .local v9, "vc":Landroid/view/ViewConfiguration;
    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    invoke-virtual {v9}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    invoke-static {p3}, Ljava/lang/Math;->signum(F)F

    move-result v1

    float-to-int v1, v1

    mul-int v3, v0, v1

    .line 399
    .local v3, "flingVelocity":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 400
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 403
    const/4 v0, 0x1

    return v0

    .line 395
    .end local v3    # "flingVelocity":I
    :cond_0
    float-to-int v0, p3

    neg-int v3, v0

    goto :goto_0

    .line 400
    .end local v9    # "vc":Landroid/view/ViewConfiguration;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v5, -0x1

    .line 217
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 219
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 261
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 223
    :cond_1
    :try_start_1
    iget-boolean v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataChanged:Z

    if-eqz v3, :cond_2

    .line 224
    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    .line 225
    .local v1, "oldCurrentX":I
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->initView()V

    .line 226
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->removeAllViewsInLayout()V

    .line 227
    iput v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 228
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataChanged:Z

    .line 231
    .end local v1    # "oldCurrentX":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 232
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 233
    .local v2, "scrollx":I
    iput v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 236
    .end local v2    # "scrollx":I
    :cond_3
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    if-gtz v3, :cond_4

    .line 237
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 238
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 240
    :cond_4
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    iget v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    if-lt v3, v4, :cond_5

    .line 241
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mMaxX:I

    iput v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    .line 242
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 245
    :cond_5
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    iget v4, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    sub-int v0, v3, v4

    .line 247
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->removeNonVisibleItems(I)V

    .line 248
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->fillList(I)V

    .line 249
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->positionItems(I)V

    .line 251
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    iput v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    .line 253
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-eqz v3, :cond_6

    if-eqz p1, :cond_7

    .line 254
    :cond_6
    iget-object v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mLayoutRequest:Ljava/lang/Runnable;

    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->post(Ljava/lang/Runnable;)Z

    .line 257
    :cond_7
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mPosition:I

    if-eq v3, v5, :cond_0

    .line 258
    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mPosition:I

    invoke-virtual {p0, v3}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->scrollToPosition(I)V

    .line 259
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mPosition:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 217
    .end local v0    # "dx":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public scrollTo(I)V
    .locals 5
    .param p1, "x"    # I

    .prologue
    .line 380
    monitor-enter p0

    .line 381
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 382
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    .line 384
    return-void

    .line 382
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public scrollToPosition(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/16 v5, 0x32

    const/4 v2, 0x0

    .line 356
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    if-nez v0, :cond_1

    .line 357
    iput p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mPosition:I

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    mul-int v7, p1, v0

    .line 362
    .local v7, "x_start":I
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    add-int v6, v7, v0

    .line 363
    .local v6, "x_end":I
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    if-le v7, v0, :cond_2

    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    if-lt v6, v0, :cond_0

    .line 366
    :cond_2
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    if-lt v6, v0, :cond_3

    .line 368
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v3, v6, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 369
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    goto :goto_0

    .line 372
    :cond_3
    iget v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    if-ge v7, v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    iget v3, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mCurrentX:I

    sub-int v3, v7, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 375
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 46
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 156
    :cond_0
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 157
    iget-object v0, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 158
    invoke-direct {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->reset()V

    .line 159
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    .line 113
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 118
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 108
    return-void
.end method

.method public setSelection(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mSelectedPosition:I

    .line 173
    iget v2, p0, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->mChildWidth:I

    if-nez v2, :cond_0

    .line 183
    :goto_0
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 179
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 180
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 180
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 182
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/misc/utility/HorizontalListView;->requestLayout()V

    goto :goto_0
.end method
