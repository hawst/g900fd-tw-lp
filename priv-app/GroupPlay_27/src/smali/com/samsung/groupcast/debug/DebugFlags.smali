.class public Lcom/samsung/groupcast/debug/DebugFlags;
.super Ljava/lang/Object;
.source "DebugFlags.java"


# static fields
.field public static DEBUG_FLAG_BOOLEAN_DOCUMENT_PERFORM:Z

.field public static DEBUG_FLAG_BOOLEAN_GPSERVERAPI_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_MEM_STATUS_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_PKGSESSIONMGR_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_REMOTE_DETAILLOG:Z

.field public static DEBUG_FLAG_BOOLEAN_REMOTE_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_REMOTE_PKT_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_REMOTE_TOAST:Z

.field public static DEBUG_FLAG_BOOLEAN_REMOTE_VOIP_TOAST:Z

.field public static DEBUG_FLAG_BOOLEAN_SMS_RECV_LOG:Z

.field public static DEBUG_FLAG_BOOLEAN_STATESVC_LOG:Z

.field public static INTERVAL_MEMWATCHDOG:I

.field private static TAG_MEM:Ljava/lang/String;

.field private static sInstance:Lcom/samsung/groupcast/debug/DebugFlags;


# instance fields
.field final MEGA:J

.field lastMemFreeSize:J

.field lastMemTotalSize:J

.field lastMemUsedSize:J

.field private memWatchDog:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 7
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_LOG:Z

    .line 8
    sput-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_TOAST:Z

    .line 9
    sput-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_PKT_LOG:Z

    .line 10
    sput-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_DETAILLOG:Z

    .line 11
    sput-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_REMOTE_VOIP_TOAST:Z

    .line 12
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_DOCUMENT_PERFORM:Z

    .line 13
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSION_LOG:Z

    .line 14
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_PKGSESSIONMGR_LOG:Z

    .line 15
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_STATESVC_LOG:Z

    .line 16
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_GPSERVERAPI_LOG:Z

    .line 17
    sput-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_SMS_RECV_LOG:Z

    .line 18
    sput-boolean v1, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_MEM_STATUS_LOG:Z

    .line 20
    new-instance v0, Lcom/samsung/groupcast/debug/DebugFlags;

    invoke-direct {v0}, Lcom/samsung/groupcast/debug/DebugFlags;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/debug/DebugFlags;->sInstance:Lcom/samsung/groupcast/debug/DebugFlags;

    .line 22
    const-string v0, "WD_MEM"

    sput-object v0, Lcom/samsung/groupcast/debug/DebugFlags;->TAG_MEM:Ljava/lang/String;

    .line 24
    const/16 v0, 0x3e8

    sput v0, Lcom/samsung/groupcast/debug/DebugFlags;->INTERVAL_MEMWATCHDOG:I

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/groupcast/debug/DebugFlags$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/debug/DebugFlags$1;-><init>(Lcom/samsung/groupcast/debug/DebugFlags;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/groupcast/debug/DebugFlags;->memWatchDog:Ljava/lang/Thread;

    .line 57
    iput-wide v2, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemFreeSize:J

    .line 58
    iput-wide v2, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemTotalSize:J

    .line 59
    iput-wide v2, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemUsedSize:J

    .line 60
    const-wide/32 v0, 0x100000

    iput-wide v0, p0, Lcom/samsung/groupcast/debug/DebugFlags;->MEGA:J

    .line 46
    sget-boolean v0, Lcom/samsung/groupcast/debug/DebugFlags;->DEBUG_FLAG_BOOLEAN_MEM_STATUS_LOG:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/samsung/groupcast/debug/DebugFlags;->memWatchDog:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 49
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/debug/DebugFlags;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/groupcast/debug/DebugFlags;->sInstance:Lcom/samsung/groupcast/debug/DebugFlags;

    return-object v0
.end method


# virtual methods
.method public logMemStatus()V
    .locals 14

    .prologue
    .line 63
    const-wide/16 v1, 0x0

    .line 64
    .local v1, "freeSize":J
    const-wide/16 v4, 0x0

    .line 65
    .local v4, "totalSize":J
    const-wide/16 v6, 0x0

    .line 67
    .local v6, "usedSize":J
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    .line 68
    .local v3, "info":Ljava/lang/Runtime;
    invoke-virtual {v3}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v1

    .line 69
    invoke-virtual {v3}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v4

    .line 70
    sub-long v6, v4, v1

    .line 71
    sget-object v8, Lcom/samsung/groupcast/debug/DebugFlags;->TAG_MEM:Ljava/lang/String;

    const-string v9, "used:%dM(%d)total:%dM(%d)free:%dM(%d)"

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/32 v12, 0x100000

    div-long v12, v6, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-wide v12, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemUsedSize:J

    sub-long v12, v6, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-wide/32 v12, 0x100000

    div-long v12, v4, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    iget-wide v12, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemTotalSize:J

    sub-long v12, v4, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    const-wide/32 v12, 0x100000

    div-long v12, v1, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    iget-wide v12, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemFreeSize:J

    sub-long v12, v1, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iput-wide v1, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemFreeSize:J

    .line 76
    iput-wide v4, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemTotalSize:J

    .line 77
    iput-wide v6, p0, Lcom/samsung/groupcast/debug/DebugFlags;->lastMemUsedSize:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .end local v3    # "info":Ljava/lang/Runtime;
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
