.class Lcom/samsung/groupcast/GroupPlayManager$1;
.super Landroid/content/BroadcastReceiver;
.source "GroupPlayManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/GroupPlayManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/GroupPlayManager;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/GroupPlayManager;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 84
    iget-object v2, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;
    invoke-static {v2}, Lcom/samsung/groupcast/GroupPlayManager;->access$000(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    move-result-object v2

    if-nez v2, :cond_1

    .line 85
    sget-object v2, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v3, "[EXT_APP]Error : GPMessageManager.mListener is NULL"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.samsung.groupcast.action.GP_SERVICE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    sget-object v2, Lcom/samsung/groupcast/GroupPlayManager;->TAG_LOG:Ljava/lang/String;

    const-string v3, "[EXT_APP]receive : GP_SERVICE"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 92
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "com.samsung.groupcast.extra.SCREEN_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "screenId":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/groupcast/GroupPlayManager$1;->this$0:Lcom/samsung/groupcast/GroupPlayManager;

    # getter for: Lcom/samsung/groupcast/GroupPlayManager;->mGroupPlayEventHandler:Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;
    invoke-static {v2}, Lcom/samsung/groupcast/GroupPlayManager;->access$000(Lcom/samsung/groupcast/GroupPlayManager;)Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/groupcast/GroupPlayManager$GroupPlayEventHandler;->onGpService(Ljava/lang/String;)V

    goto :goto_0
.end method
