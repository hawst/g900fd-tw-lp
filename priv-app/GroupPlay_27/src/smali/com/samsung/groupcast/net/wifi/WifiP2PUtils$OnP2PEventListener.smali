.class public interface abstract Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
.super Ljava/lang/Object;
.source "WifiP2PUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnP2PEventListener"
.end annotation


# virtual methods
.method public abstract onP2PChanged(IZI)V
.end method

.method public abstract onP2PConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
.end method

.method public abstract onP2PEventReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end method

.method public abstract onP2PScaned(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
.end method
