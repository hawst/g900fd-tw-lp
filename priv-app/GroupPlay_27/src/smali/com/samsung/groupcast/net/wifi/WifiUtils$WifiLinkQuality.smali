.class public final enum Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;
.super Ljava/lang/Enum;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WifiLinkQuality"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

.field public static final enum HIGH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

.field public static final enum LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

.field public static final enum MEDIUM:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

.field public static final enum NO_LINK:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

.field public static final enum VERY_LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    const-string v1, "NO_LINK"

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->NO_LINK:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    .line 90
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    const-string v1, "VERY_LOW"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->VERY_LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    .line 91
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    .line 92
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->MEDIUM:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    .line 93
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->HIGH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    .line 88
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->NO_LINK:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->VERY_LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->LOW:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->MEDIUM:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->HIGH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 88
    const-class v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;

    return-object v0
.end method
