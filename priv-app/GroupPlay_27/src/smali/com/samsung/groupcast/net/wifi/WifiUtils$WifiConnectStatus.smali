.class public final enum Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;
.super Ljava/lang/Enum;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WifiConnectStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum CONNECTING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum DISCONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum FAILED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

.field public static final enum NOT_ENABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "NOT_ENABLED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->NOT_ENABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 71
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->DISCONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 72
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "ENABLING"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 73
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 74
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "AUTHENTICATING"

    invoke-direct {v0, v1, v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 75
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "CONNECTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 76
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    const-string v1, "FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->FAILED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    .line 69
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->NOT_ENABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->DISCONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->FAILED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    return-object v0
.end method
