.class Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;
.super Ljava/lang/Object;
.source "WifiP2PUtils.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/net/wifi/p2p/WifiP2pInfo;

    .prologue
    .line 371
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    .line 372
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 374
    .local v2, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :try_start_0
    invoke-interface {v2, p1}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 375
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 379
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :cond_0
    return-void
.end method
