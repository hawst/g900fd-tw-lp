.class Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;
.super Ljava/lang/Thread;
.source "WifiAPScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiAPScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeriodicScanner"
.end annotation


# static fields
.field private static final SCAN_PERIOD_TIME:I = 0x1f40


# instance fields
.field private brk:Z

.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiAPScanner;)V
    .locals 2

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->brk:Z

    .line 205
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "+++ Starting PeriodicScanner"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 222
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->brk:Z

    if-nez v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->doStartScan()Z
    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$100(Lcom/samsung/groupcast/net/wifi/WifiAPScanner;)Z

    .line 224
    monitor-enter p0

    .line 225
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->brk:Z

    if-eqz v1, :cond_1

    .line 226
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "loop is ended by brk!"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 240
    :cond_0
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "loop is ended"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    return-void

    .line 229
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 232
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 233
    const-wide/16 v1, 0x1f40

    :try_start_3
    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V

    .line 234
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/InterruptedException;
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "stopped by interrupt"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v1
.end method

.method public stopSelf()V
    .locals 2

    .prologue
    .line 209
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "--- Stopping PeriodicScanner"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    monitor-enter p0

    .line 212
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->brk:Z

    .line 213
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->interrupt()V

    .line 215
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "--- Stopping PeriodicScanner interrupt"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "--- Stopped PeriodicScanner"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return-void

    .line 216
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
