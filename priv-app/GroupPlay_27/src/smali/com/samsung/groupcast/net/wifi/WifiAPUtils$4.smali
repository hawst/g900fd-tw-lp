.class Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;
.super Landroid/content/BroadcastReceiver;
.source "WifiAPUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiAPUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 426
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 429
    const-string v3, "WiFi Receiver - WIFI_STATE_CHANGED_ACTION"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 430
    const-string v3, "wifi_state"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 434
    .local v2, "wifiState":I
    packed-switch v2, :pswitch_data_0

    .line 544
    .end local v2    # "wifiState":I
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 545
    return-void

    .line 436
    .restart local v2    # "wifiState":I
    :pswitch_0
    const-string v3, "WiFi Receiver - WIFI_STATE_ENABLING"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 440
    :pswitch_1
    const-string v3, "WiFi Receiver - WIFI_STATE_ENABLED"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 442
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRVFMode()I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$700(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v3

    if-eq v3, v6, :cond_1

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRVFMode()I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$700(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 443
    :cond_1
    const-string v3, "WiFi Receiver - IS RVFMODE IN WIFI_STATE_ENABLED"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 448
    :pswitch_2
    const-string v3, "WiFi Receiver - WIFI_STATE_DISABLING"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 452
    :pswitch_3
    const-string v3, "WiFi Receiver - WIFI_STATE_DISABLED"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 453
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v3, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    if-eqz v3, :cond_0

    .line 454
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v4, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    monitor-enter v4

    .line 455
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v3, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 456
    const-string v3, "WiFi Receiver - Wifi checking completed"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 457
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 462
    :pswitch_4
    const-string v3, "WiFi Receiver - WIFI_STATE_UNKNOWN"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 467
    .end local v2    # "wifiState":I
    :cond_2
    const-string v3, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 468
    const-string v3, "WiFi Receiver - WIFI_AP_STATE_CHANGED_ACTION"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 469
    const-string v3, "wifi_state"

    const/16 v4, 0xb

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 473
    .local v1, "apState":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 496
    :pswitch_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WiFi Receiver - WIFI_AP_STATE_DISABLING, RvfHotspotState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 499
    sget v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    if-eq v3, v5, :cond_3

    sget v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    if-ne v3, v8, :cond_0

    .line 501
    :cond_3
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->BroadCastGroupPlayAPStartNStopForTEDC(Z)V

    .line 502
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 503
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;->onRvfHotspotDisabling()V

    .line 505
    :cond_4
    sput v9, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    goto/16 :goto_0

    .line 475
    :pswitch_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WiFi Receiver - WIFI_AP_STATE_ENABLING, RvfHotspotState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 478
    sput v7, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    goto/16 :goto_0

    .line 482
    :pswitch_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WiFi Receiver - WIFI_AP_STATE_ENABLED, RvfHotspotState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 485
    sget v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    if-ne v3, v7, :cond_5

    .line 486
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 487
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;->onRvfHotspotEnabled()V

    .line 491
    :cond_5
    sput v8, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    goto/16 :goto_0

    .line 510
    :pswitch_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WiFi Receiver - WIFI_AP_STATE_DISABLED, RvfHotspotState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 513
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v3, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    if-eqz v3, :cond_6

    .line 514
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v4, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    monitor-enter v4

    .line 515
    :try_start_1
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v3, v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 516
    const-string v3, "WiFi Receiver - Ap checking completed"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 517
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 520
    :cond_6
    sget v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    if-ne v3, v9, :cond_a

    .line 521
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mUserWifiState:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 522
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v3

    if-eq v3, v7, :cond_7

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v3

    if-ne v3, v6, :cond_8

    :cond_7
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$900(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Environment;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 525
    const-string v3, "wifi on"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 526
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 529
    :cond_8
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 530
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;->onRvfHotspotDisabled()V

    .line 532
    :cond_9
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->unregisterReceiver()V
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$1000(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    .line 534
    :cond_a
    sput v6, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    goto/16 :goto_0

    .line 517
    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 538
    :pswitch_9
    const-string v3, "WiFi Receiver - WIFI_AP_STATE_FAILED"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 539
    const/4 v3, 0x6

    sput v3, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    goto/16 :goto_0

    .line 434
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 473
    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_5
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method
