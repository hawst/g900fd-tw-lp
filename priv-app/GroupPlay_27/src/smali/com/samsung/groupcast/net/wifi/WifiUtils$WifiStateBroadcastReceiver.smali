.class public Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WifiStateBroadcastReceiver"
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mStarted:Z

.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;)V
    .locals 1

    .prologue
    .line 804
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 806
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mStarted:Z

    return-void
.end method


# virtual methods
.method public callMainActivityOnDisconnected()V
    .locals 2

    .prologue
    .line 809
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mActivity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",mStartd:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mStarted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 810
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mStarted:Z

    if-eqz v0, :cond_0

    .line 823
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 828
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 830
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 832
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DISCONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 833
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->callMainActivityOnDisconnected()V

    .line 836
    :cond_1
    return-void
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 839
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mActivity:Landroid/app/Activity;

    .line 840
    return-void
.end method

.method public setSessionStarted(Z)V
    .locals 0
    .param p1, "started"    # Z

    .prologue
    .line 843
    iput-boolean p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->mStarted:Z

    .line 844
    return-void
.end method
