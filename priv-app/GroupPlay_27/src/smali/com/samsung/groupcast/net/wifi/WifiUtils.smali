.class public Lcom/samsung/groupcast/net/wifi/WifiUtils;
.super Ljava/lang/Object;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/net/wifi/WifiUtils$2;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiLinkQuality;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;,
        Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;
    }
.end annotation


# static fields
.field public static final AP_MODE:Ljava/lang/String; = "AP_MODE"

.field public static final CONNECTED:Ljava/lang/String; = "CONNECTED"

.field public static final DISCONNECTED:Ljava/lang/String; = "DISCONNECTED"

.field public static final OXYGEN_FREQUENCY:I = 0x96c

.field public static final SCAN_RESULTS:I = 0x1

.field public static final SCAN_STARTED:I

.field protected static final TAG:Ljava/lang/String;

.field private static mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# instance fields
.field private final SUPPORT_INTERNET_MODE:Z

.field private final connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

.field private final mConnManager:Landroid/net/ConnectivityManager;

.field private final mContext:Landroid/content/Context;

.field private mIsOxygenConnected:Z

.field private mIsOxygenSupportChecked:Z

.field private mIsOxygenSupported:Z

.field private final mOxygenBR:Landroid/content/BroadcastReceiver;

.field private mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

.field private final mWifiListener:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

.field private mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-class v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v1, Lcom/samsung/groupcast/misc/utility/EventHandler;

    invoke-direct {v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    .line 61
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupportChecked:Z

    .line 62
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupported:Z

    .line 63
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenConnected:Z

    .line 67
    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    .line 968
    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    .line 1029
    new-instance v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;-><init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;)V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mOxygenBR:Landroid/content/BroadcastReceiver;

    .line 103
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    .line 105
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 106
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mConnManager:Landroid/net/ConnectivityManager;

    .line 107
    new-instance v1, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-direct {v1, v2, v3}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    .line 109
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiNewAPISupport(Landroid/net/wifi/WifiManager;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    .line 110
    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SUPPORT_INTERNET_MODE:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 113
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 114
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 115
    new-instance v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;-><init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;)V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiListener:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    .line 117
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiListener:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    sget-object v1, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v1}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 124
    :goto_0
    return-void

    .line 121
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->registerForOxygenBR()V

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public static GetWifiStateString(I)Ljava/lang/String;
    .locals 3
    .param p0, "state"    # I

    .prologue
    .line 588
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DISABLING"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DISABLED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ENABLING"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ENABLED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "UNKNOWN"

    aput-object v2, v0, v1

    .line 589
    .local v0, "t":[Ljava/lang/String;
    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 590
    aget-object v1, v0, p0

    .line 592
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public static IsHostOrAP(Ljava/lang/String;)Z
    .locals 2
    .param p0, "ip"    # Ljava/lang/String;

    .prologue
    .line 1096
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1097
    .local v0, "ipt":Ljava/lang/String;
    const-string v1, "192.168.43.1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "192.168.1.1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "192.168."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ".1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1099
    :cond_0
    const/4 v1, 0x1

    .line 1101
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static ShowToast(Ljava/lang/String;)V
    .locals 2
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 1025
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1026
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/net/wifi/WifiUtils;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiUtils;
    .param p1, "x1"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiConfigWithCheckForInternetSerivceBypass(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiUtils;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/groupcast/net/wifi/WifiUtils;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiUtils;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenConnected:Z

    return p1
.end method

.method private convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    return-object v0
.end method

.method private getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 249
    :try_start_0
    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "getWifiApConfiguration"

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 251
    .local v1, "methods":Ljava/lang/reflect/Method;
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiConfiguration;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    .end local v1    # "methods":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v2, v3

    .line 254
    goto :goto_0
.end method

.method private getWifiConfigWithCheckForInternetSerivceBypass(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    .locals 3
    .param p1, "wifiConfiguration"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    .line 949
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "samsungSpecificFlags"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 951
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->samsungSpecificFlags:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 952
    const-string v1, "wifiConfiguration.samsungSpecificFlags.set(SamsungFlag.IGNORE_INTERNET_SERVICE_CHECK)"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    .line 960
    :cond_0
    :goto_0
    return-object p1

    .line 954
    :catch_0
    move-exception v0

    .line 955
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "setByPassForCheckForInternetSerivce(WifiConfiguration wifiConfiguration) IllegalArgumentException thrown"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 956
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 957
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v1, "setByPassForCheckForInternetSerivce(WifiConfiguration wifiConfiguration) NoSuchFieldException thrown"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private intToIp(I)Ljava/lang/String;
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit16 v1, p1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    shr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isGroupPlayAPName(Ljava/lang/String;)Z
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 848
    const/4 v0, 0x0

    .line 850
    .local v0, "bRes":Z
    if-eqz p0, :cond_0

    const-string v1, "\""

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GroupPlay-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 851
    const/4 v0, 0x1

    .line 854
    :cond_0
    return v0
.end method

.method private isSecureConfig(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 4
    .param p1, "config"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 282
    if-eqz p1, :cond_0

    .line 283
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    .line 284
    .local v0, "keyMngt":Ljava/util/BitSet;
    invoke-virtual {v0}, Ljava/util/BitSet;->cardinality()I

    move-result v3

    if-ne v3, v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289
    .end local v0    # "keyMngt":Ljava/util/BitSet;
    :cond_0
    :goto_0
    return v1

    .restart local v0    # "keyMngt":Ljava/util/BitSet;
    :cond_1
    move v1, v2

    .line 287
    goto :goto_0
.end method

.method private registerForConfiguration(Landroid/net/wifi/WifiConfiguration;)V
    .locals 3
    .param p1, "wifiConfiguration"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    .line 495
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 497
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 499
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 500
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 501
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 502
    const-string v1, "android.net.wifi.supplicant.CONNECTION_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 503
    const-string v1, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 504
    const-string v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 507
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    if-eqz v1, :cond_0

    .line 509
    const-string v1, "android.net.wifi.WIFI_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 510
    const-string v1, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 513
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    if-eqz v1, :cond_1

    .line 515
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->unregisterWifiReceiver()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    :cond_1
    :goto_0
    new-instance v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-direct {v1, p0, p1}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;-><init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;Landroid/net/wifi/WifiConfiguration;)V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    .line 519
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 521
    return-void

    .line 516
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private registerForOxygenBR()V
    .locals 3

    .prologue
    .line 1087
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1088
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "---"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1089
    const-string v1, "android.net.wifi.WIFI_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1090
    const-string v1, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1091
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mOxygenBR:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1092
    return-void
.end method


# virtual methods
.method public connectTo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "C"    # Landroid/content/Context;
    .param p2, "BSSID"    # Ljava/lang/String;
    .param p3, "SSID"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 459
    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v2, "connectTo()"

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already connected to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 491
    :goto_0
    return v3

    .line 467
    :cond_0
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 468
    .local v0, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    const/4 v1, -0x1

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 469
    const/16 v1, 0x63

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->priority:I

    .line 471
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    if-eqz v1, :cond_1

    .line 473
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->samsungSpecificFlags:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 479
    :cond_1
    if-eqz p3, :cond_2

    .line 480
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 482
    :cond_2
    if-eqz p4, :cond_3

    .line 483
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 485
    :cond_3
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 486
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v1

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->NOT_ENABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 487
    const-string v1, "Unable to enable wifi!"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 490
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->registerForConfiguration(Landroid/net/wifi/WifiConfiguration;)V

    goto :goto_0
.end method

.method public connectToNetID(Landroid/content/Context;I)Z
    .locals 5
    .param p1, "C"    # Landroid/content/Context;
    .param p2, "netId"    # I

    .prologue
    const/4 v2, 0x0

    .line 305
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v3, :cond_1

    .line 306
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v4, "mWifiManager is null"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_0
    :goto_0
    return v2

    .line 310
    :cond_1
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    .line 311
    .local v1, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    if-eqz v1, :cond_2

    iget v3, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    if-ne v3, p2, :cond_2

    .line 312
    invoke-direct {p0, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->registerForConfiguration(Landroid/net/wifi/WifiConfiguration;)V

    .line 313
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public connectToOxygen(Landroid/net/wifi/ScanResult;)V
    .locals 3
    .param p1, "result"    # Landroid/net/wifi/ScanResult;

    .prologue
    .line 345
    const-string v1, "=="

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 346
    if-eqz p1, :cond_0

    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "oxy connect to:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 348
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 349
    .local v0, "conf":Landroid/net/wifi/WifiConfiguration;
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 350
    sget-object v1, Landroid/net/wifi/WifiConfiguration$OperationMode;->IBSS:Landroid/net/wifi/WifiConfiguration$OperationMode;

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->mode:Landroid/net/wifi/WifiConfiguration$OperationMode;

    .line 351
    iget-object v1, p1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    .line 352
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 353
    iget v1, p1, Landroid/net/wifi/ScanResult;->frequency:I

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->frequency:I

    .line 354
    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectToOxygen(Landroid/net/wifi/WifiConfiguration;)V

    .line 356
    .end local v0    # "conf":Landroid/net/wifi/WifiConfiguration;
    :cond_0
    return-void
.end method

.method public connectToOxygen(Landroid/net/wifi/WifiConfiguration;)V
    .locals 3
    .param p1, "conf"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    .line 332
    const-string v0, "=="

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iput v0, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 337
    iget v0, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 338
    const-string v0, "=="

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v1, p1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    .line 342
    :cond_0
    return-void
.end method

.method public connectToOxygen(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "C"    # Landroid/content/Context;
    .param p2, "BSSID"    # Ljava/lang/String;
    .param p3, "SSID"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 360
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "connectToOxygen()"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->stopScan()V

    .line 364
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "oxy connect to O:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 365
    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiSSID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isOxygenConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 366
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Already connected to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 367
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 424
    :goto_0
    return v3

    .line 371
    :cond_0
    new-instance v2, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v2}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 372
    .local v2, "wifiConfiguration":Landroid/net/wifi/WifiConfiguration;
    const/4 v5, -0x1

    iput v5, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 373
    sget-object v5, Landroid/net/wifi/WifiConfiguration$OperationMode;->IBSS:Landroid/net/wifi/WifiConfiguration$OperationMode;

    iput-object v5, v2, Landroid/net/wifi/WifiConfiguration;->mode:Landroid/net/wifi/WifiConfiguration$OperationMode;

    .line 374
    iget-object v5, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v5, v4}, Ljava/util/BitSet;->set(I)V

    .line 375
    iput-object p2, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    .line 379
    if-eqz p3, :cond_1

    .line 380
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 382
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 383
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 384
    .local v1, "s":Landroid/net/wifi/ScanResult;
    iget-object v4, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 385
    iget v4, v1, Landroid/net/wifi/ScanResult;->frequency:I

    iput v4, v2, Landroid/net/wifi/WifiConfiguration;->frequency:I

    .line 386
    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    if-nez v4, :cond_4

    .line 387
    iget-object v4, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    iput-object v4, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    .line 388
    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search bssid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/net/wifi/WifiConfiguration;->frequency:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .end local v1    # "s":Landroid/net/wifi/ScanResult;
    :cond_3
    :goto_1
    invoke-virtual {p0, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectToOxygen(Landroid/net/wifi/WifiConfiguration;)V

    goto/16 :goto_0

    .line 391
    .restart local v1    # "s":Landroid/net/wifi/ScanResult;
    :cond_4
    iget-object v4, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    iget-object v5, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 392
    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "same bssid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/net/wifi/WifiConfiguration;->frequency:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 399
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "s":Landroid/net/wifi/ScanResult;
    :cond_5
    const-string v3, "connecting Fs"

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    move v3, v4

    .line 400
    goto/16 :goto_0
.end method

.method public createOxygen(Ljava/lang/String;)V
    .locals 4
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 428
    const-string v1, "=="

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 429
    const-string v1, "creating.."

    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isOxygenSupported()Z

    move-result v1

    if-nez v1, :cond_0

    .line 454
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiIBSSState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 434
    const-string v1, "wifi ibss should be turned on."

    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    goto :goto_0

    .line 439
    :cond_1
    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 440
    .local v0, "newConfig":Landroid/net/wifi/WifiConfiguration;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 441
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 443
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->generateMACAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->BSSID:Ljava/lang/String;

    .line 444
    sget-object v1, Landroid/net/wifi/WifiConfiguration$OperationMode;->IBSS:Landroid/net/wifi/WifiConfiguration$OperationMode;

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->mode:Landroid/net/wifi/WifiConfiguration$OperationMode;

    .line 445
    const/16 v1, 0x96c

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->frequency:I

    .line 446
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v1

    iput v1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 448
    iget v1, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 449
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v2, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    .line 453
    :cond_2
    const/4 v0, 0x0

    .line 454
    goto :goto_0
.end method

.method public disconnectFromCurrentAP()V
    .locals 8

    .prologue
    .line 860
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnectFromCurrentAP: mWifiManager:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v5, :cond_7

    .line 862
    const/4 v0, 0x0

    .line 863
    .local v0, "bDeleted":Z
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 864
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnectFromCurrentAP: ssid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isGroupPlayAPName(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 868
    :cond_0
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    if-eqz v5, :cond_2

    .line 869
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnect id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->getPreviousNetId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->getPreviousNetId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 871
    const/4 v0, 0x1

    .line 890
    :goto_0
    if-eqz v0, :cond_1

    .line 891
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 892
    const/4 v0, 0x0

    .line 899
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v3

    .line 900
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v3, :cond_8

    .line 901
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiConfiguration;

    .line 902
    .local v4, "wifi":Landroid/net/wifi/WifiConfiguration;
    iget-object v5, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isGroupPlayAPName(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 903
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnect removes :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v6, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 906
    const/4 v0, 0x1

    goto :goto_1

    .line 873
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .end local v4    # "wifi":Landroid/net/wifi/WifiConfiguration;
    :cond_2
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "disconnect mWifiReceiver is null"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 876
    :cond_3
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnect:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 878
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 881
    :cond_4
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    if-eqz v5, :cond_5

    .line 882
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "disconnect info is null id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->getPreviousNetId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->getPreviousNetId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/net/wifi/WifiManager;->removeNetwork(I)Z

    .line 884
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 886
    :cond_5
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "disconnect info is null mWifiReceiver is null"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 908
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .restart local v4    # "wifi":Landroid/net/wifi/WifiConfiguration;
    :cond_6
    :try_start_1
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget v6, v4, Landroid/net/wifi/WifiConfiguration;->networkId:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 912
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .end local v4    # "wifi":Landroid/net/wifi/WifiConfiguration;
    :catch_0
    move-exception v1

    .line 913
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 915
    if-eqz v0, :cond_7

    .line 916
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    .line 919
    .end local v0    # "bDeleted":Z
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    :goto_2
    return-void

    .line 915
    .restart local v0    # "bDeleted":Z
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    :cond_8
    if-eqz v0, :cond_7

    .line 916
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    goto :goto_2

    .line 915
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_9

    .line 916
    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    :cond_9
    throw v5
.end method

.method public generateMACAddress()Ljava/lang/String;
    .locals 10

    .prologue
    .line 1007
    const-string v0, "0123456789abcdef"

    .line 1008
    .local v0, "HEX":Ljava/lang/String;
    const/16 v1, 0xa

    .line 1009
    .local v1, "digit":I
    const-string v8, "0123456789abcdef"

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    .line 1010
    .local v3, "len":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    .line 1011
    .local v6, "seed":J
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4, v6, v7}, Ljava/util/Random;-><init>(J)V

    .line 1012
    .local v4, "random":Ljava/util/Random;
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v8, "02:"

    invoke-direct {v5, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1013
    .local v5, "result":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v8, 0xa

    if-ge v2, v8, :cond_1

    .line 1014
    const-string v8, "0123456789abcdef"

    invoke-virtual {v4, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1015
    rem-int/lit8 v8, v2, 0x2

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    const/16 v8, 0x9

    if-eq v2, v8, :cond_0

    .line 1016
    const/16 v8, 0x3a

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1013
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1019
    :cond_1
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    return-object v0
.end method

.method public getIBSSStatus()I
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiIBSSState()I

    move-result v0

    return v0
.end method

.method public getScanResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 551
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWifiBSSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-nez v0, :cond_1

    .line 160
    :cond_0
    const-string v0, "ERR"

    .line 166
    :goto_0
    return-object v0

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWifiIP()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v0

    .line 173
    .local v0, "ipAddress":I
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->intToIp(I)Ljava/lang/String;

    move-result-object v1

    .line 177
    .end local v0    # "ipAddress":I
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "192.168.43.1"

    goto :goto_0
.end method

.method public getWifiIPLastNum()Ljava/lang/String;
    .locals 4

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Wifi IP address"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 187
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiListener:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    return-object v0
.end method

.method public getWifiManager()Landroid/net/wifi/WifiManager;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method public getWifiMotherFatherOxygenDelegate()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    return-object v0
.end method

.method public getWifiPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiUtils:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GroupCast"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWifiSSID()Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isProtected()Z

    move-result v1

    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRvfHotspotSSID(Z)Ljava/lang/String;

    move-result-object v1

    .line 154
    :goto_0
    return-object v1

    .line 141
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v1

    if-nez v1, :cond_4

    .line 142
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 144
    :cond_1
    const-string v1, "Unknown"

    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CONNECTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 147
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\""

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 148
    :cond_3
    const-string v1, "Unknown"

    goto :goto_0

    .line 150
    :cond_4
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    .line 151
    .local v0, "apConf":Landroid/net/wifi/WifiConfiguration;
    if-eqz v0, :cond_5

    .line 152
    iget-object v1, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0

    .line 154
    :cond_5
    const-string v1, "Unknown"

    goto :goto_0
.end method

.method public getWifiStatus()Ljava/lang/String;
    .locals 5

    .prologue
    .line 199
    const-string v1, "DISCONNECTED"

    .line 200
    .local v1, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 201
    iget-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    if-eqz v2, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiNetInfoConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "CONNECTED"

    .line 216
    :goto_0
    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wifistatus:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-object v1

    .line 202
    :cond_0
    const-string v1, "DISCONNECTED"

    goto :goto_0

    .line 204
    :cond_1
    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mConnManager:Landroid/net/ConnectivityManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 205
    .local v0, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 206
    const/4 v0, 0x0

    .line 207
    const-string v1, "CONNECTED"

    goto :goto_0

    .line 209
    :cond_2
    const/4 v0, 0x0

    .line 210
    const-string v1, "DISCONNECTED"

    goto :goto_0

    .line 214
    .end local v0    # "netInfo":Landroid/net/NetworkInfo;
    :cond_3
    const-string v1, "AP_MODE"

    goto :goto_0
.end method

.method public isOxygenConnected()Z
    .locals 2

    .prologue
    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "c:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenConnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 328
    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenConnected:Z

    return v0
.end method

.method public isOxygenSupported()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 991
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupportChecked:Z

    if-nez v1, :cond_1

    .line 992
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 993
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3e

    iput v1, v0, Landroid/os/Message;->what:I

    .line 995
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_1

    .line 996
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    move-result v1

    if-nez v1, :cond_0

    .line 997
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupported:Z

    .line 999
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupportChecked:Z

    .line 1002
    .end local v0    # "msg":Landroid/os/Message;
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenSupported:Z

    return v1
.end method

.method public isPoorConnectionDetected(Landroid/net/wifi/WifiConfiguration;)Z
    .locals 7
    .param p1, "wifiConfiguration"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 922
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v5, :cond_1

    .line 923
    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v5, "mWifiManager is null"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    :cond_0
    :goto_0
    return v3

    .line 927
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v0

    .line 928
    .local v0, "configs":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v0, :cond_0

    .line 929
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    .line 930
    .local v2, "wifiConfig":Landroid/net/wifi/WifiConfiguration;
    iget-object v5, p1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v6, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget v5, v2, Landroid/net/wifi/WifiConfiguration;->disableReason:I

    if-ne v5, v4, :cond_2

    move v3, v4

    .line 932
    goto :goto_0
.end method

.method public isSecureNetwork()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 259
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v5, :cond_1

    .line 260
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "mWifiManager is null"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_0
    :goto_0
    return v4

    .line 264
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isWifiApEnabled()Z

    move-result v5

    if-nez v5, :cond_3

    .line 265
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    .line 267
    .local v3, "wInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v3, :cond_0

    .line 268
    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "ssid":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 270
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    .line 271
    .local v0, "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v5, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 272
    invoke-direct {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isSecureConfig(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v4

    goto :goto_0

    .line 278
    .end local v0    # "config":Landroid/net/wifi/WifiConfiguration;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "ssid":Ljava/lang/String;
    .end local v3    # "wInfo":Landroid/net/wifi/WifiInfo;
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isSecureConfig(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v4

    goto :goto_0
.end method

.method public isSupportInternetWhenJoined()Z
    .locals 1

    .prologue
    .line 1149
    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->SUPPORT_INTERNET_MODE:Z

    return v0
.end method

.method public isWifiApEnabled()Z
    .locals 9

    .prologue
    .line 226
    const/4 v0, 0x0

    .line 227
    .local v0, "bRet":Z
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v4, :cond_0

    .line 228
    const-string v4, "mWifiManager is null"

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    move v1, v0

    .line 239
    .end local v0    # "bRet":Z
    .local v1, "bRet":I
    :goto_0
    return v1

    .line 233
    .end local v1    # "bRet":I
    .restart local v0    # "bRet":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "isWifiApEnabled"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 234
    .local v3, "methods":Ljava/lang/reflect/Method;
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Object;

    invoke-virtual {v3, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 235
    const/4 v3, 0x0

    move v1, v0

    .line 236
    .restart local v1    # "bRet":I
    goto :goto_0

    .line 237
    .end local v1    # "bRet":I
    .end local v3    # "methods":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v2

    .line 238
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "invoke"

    const-string v6, "isWifiApEnabled"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/groupcast/application/Logger;->dx(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)V

    move v1, v0

    .line 239
    .restart local v1    # "bRet":I
    goto :goto_0
.end method

.method public isWifiEnabled()Z
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiStatus()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWifiNetInfoConnected()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1121
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 1122
    .local v3, "wifiMgr":Landroid/net/wifi/WifiManager;
    const/4 v1, 0x0

    .line 1124
    .local v1, "m":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getNetworkInfo"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1131
    const/4 v5, 0x0

    :try_start_1
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    .line 1132
    .local v2, "netinfo":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1133
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "ConnectivityUtils.isSecFeature: connected"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 1134
    const/4 v4, 0x1

    .line 1145
    .end local v2    # "netinfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v4

    .line 1125
    :catch_0
    move-exception v0

    .line 1126
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "ConnectivityUtils : It is not supported2!"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1138
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 1139
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "ConnectivityUtils.isSecFeature2 : IllegalArgumentException"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1140
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1141
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "ConnectivityUtils.isSecFeature2 : IllegalAccessException"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1142
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 1143
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v6, "ConnectivityUtils.isSecFeature2 : InvocationTargetException"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isWifiNewAPISupport(Landroid/net/wifi/WifiManager;)Z
    .locals 5
    .param p1, "wifimgr"    # Landroid/net/wifi/WifiManager;

    .prologue
    const/4 v1, 0x0

    .line 1106
    if-nez p1, :cond_0

    .line 1107
    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v3, "mgr is null"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    :goto_0
    return v1

    .line 1112
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getNetworkInfo"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1117
    const/4 v1, 0x1

    goto :goto_0

    .line 1113
    :catch_0
    move-exception v0

    .line 1114
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v3, "ConnectivityUtils : It is not supported2!"

    invoke-static {v2, v3}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerForScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 534
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 535
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->registerForScanningResultsAvailable(Landroid/os/Handler;)V

    .line 538
    :cond_0
    return-void
.end method

.method public setWifiMotherFatherOxygenDelegate(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;)V
    .locals 0
    .param p1, "d"    # Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    .prologue
    .line 977
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    .line 978
    return-void
.end method

.method public startScan(ZZ)V
    .locals 2
    .param p1, "full"    # Z
    .param p2, "force"    # Z

    .prologue
    .line 557
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 574
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_1

    .line 575
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->kickStartScan(Z)V

    .line 577
    :cond_1
    return-void

    .line 562
    :pswitch_1
    if-eqz p2, :cond_0

    .line 563
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 564
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    .line 568
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_0

    .line 557
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public stopScan()V
    .locals 2

    .prologue
    .line 580
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v1, "stop scan+"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 582
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v1, "stop scan++"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->stopScan()V

    .line 585
    :cond_0
    return-void
.end method

.method public turnOxygenOnOff(Z)V
    .locals 3
    .param p1, "bOn"    # Z

    .prologue
    .line 985
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, p1}, Landroid/net/wifi/WifiManager;->setWifiIBSSEnabled(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 986
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to turn IBSS as:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 988
    :cond_0
    return-void
.end method

.method public unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V

    .line 544
    :cond_0
    return-void
.end method

.method public unregisterWifiReceiver()V
    .locals 2

    .prologue
    .line 525
    const-string v0, "--"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 526
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    if-eqz v0, :cond_0

    .line 527
    const-string v0, "--"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 528
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 529
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiReceiver:Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;

    .line 531
    :cond_0
    return-void
.end method
