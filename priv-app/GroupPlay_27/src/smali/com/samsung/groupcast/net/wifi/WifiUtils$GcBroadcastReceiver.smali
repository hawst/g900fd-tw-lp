.class public Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GcBroadcastReceiver"
.end annotation


# static fields
.field private static final CONNECT_RETRY_COUNT:I = 0x5

.field private static final CONNECT_TIMEOUT_MS:J = 0x7530L


# instance fields
.field private mDisconnectCount:I

.field private mNetID:I

.field private final mSyncLock:Ljava/lang/Object;

.field mTimeoutRunnable:Ljava/lang/Runnable;

.field private mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

.field private supplicantStateCompleted:Z

.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method public constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;Landroid/net/wifi/WifiConfiguration;)V
    .locals 3
    .param p2, "wifiConfiguration"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    const/4 v1, 0x0

    .line 605
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 599
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    .line 600
    iput v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mDisconnectCount:I

    .line 601
    iput-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->supplicantStateCompleted:Z

    .line 602
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mSyncLock:Ljava/lang/Object;

    .line 794
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver$1;-><init>(Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 606
    iput-object p2, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    .line 607
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mNetID:I

    .line 608
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x7530

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/MainQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 609
    return-void
.end method


# virtual methods
.method public getPreviousNetId()I
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mNetID:I

    return v0
.end method

.method public iFailed(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V
    .locals 3
    .param p1, "reason"    # Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .prologue
    .line 782
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mSyncLock:Ljava/lang/Object;

    monitor-enter v1

    .line 783
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->unregisterWifiReceiver()V

    .line 784
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 785
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 786
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 788
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 789
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "iFailed reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->FAILED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(II)V

    .line 792
    return-void

    .line 788
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 613
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 615
    .local v1, "action":Ljava/lang/String;
    const-string v12, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    const-string v12, "android.net.wifi.WIFI_OXYGEN_STATE_CHANGE"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 617
    :cond_0
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_CHANGED_ACTION"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v12, "wifi_state"

    const/4 v13, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 622
    .local v11, "wifiState":I
    packed-switch v11, :pswitch_data_0

    .line 774
    .end local v11    # "wifiState":I
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 624
    .restart local v11    # "wifiState":I
    :pswitch_1
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_DISABLED"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto :goto_0

    .line 629
    :pswitch_2
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_DISABLING"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto :goto_0

    .line 635
    :pswitch_3
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_UNKNOWN"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 640
    :pswitch_4
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_ENABLED"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 644
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    iget v8, v12, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 645
    .local v8, "netId":I
    const/4 v10, 0x0

    .line 647
    .local v10, "wifiConfig":Landroid/net/wifi/WifiConfiguration;
    const/4 v12, -0x1

    if-ne v8, v12, :cond_1

    .line 648
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    if-nez v12, :cond_2

    .line 649
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "mWifiManager is null"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 653
    :cond_2
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v3

    .line 656
    .local v3, "configuredNetworks":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    if-eqz v3, :cond_4

    .line 657
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiConfiguration;

    .line 658
    .local v2, "config":Landroid/net/wifi/WifiConfiguration;
    iget-object v12, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    iget-object v13, v13, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 659
    iget v8, v2, Landroid/net/wifi/WifiConfiguration;->networkId:I

    .line 660
    move-object v10, v2

    .line 665
    .end local v2    # "config":Landroid/net/wifi/WifiConfiguration;
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_4
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "netId = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const/4 v12, -0x1

    if-ne v8, v12, :cond_5

    .line 667
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiConfigWithCheckForInternetSerivceBypass(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    invoke-static {v12, v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiUtils;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v12

    iput-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    .line 669
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_ENABLED : mWifiManager.addNetwork(mWifiConfiguration)"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v12, v13}, Landroid/net/wifi/WifiManager;->addNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v8

    .line 680
    :goto_1
    iput v8, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mNetID:I

    .line 681
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v8, v13}, Landroid/net/wifi/WifiManager;->enableNetwork(IZ)Z

    .line 682
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    invoke-virtual {v12}, Landroid/net/wifi/WifiManager;->saveConfiguration()Z

    goto/16 :goto_0

    .line 673
    :cond_5
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - WIFI_STATE_ENABLED :  netId = mWifiManager.updateNetwork(getWifiConfigWithCheckForInternetSerivceBypass(wifiConfig));"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiConfigWithCheckForInternetSerivceBypass(Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;
    invoke-static {v13, v10}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiUtils;Landroid/net/wifi/WifiConfiguration;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/net/wifi/WifiManager;->updateNetwork(Landroid/net/wifi/WifiConfiguration;)I

    move-result v8

    goto :goto_1

    .line 686
    .end local v3    # "configuredNetworks":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiConfiguration;>;"
    .end local v8    # "netId":I
    .end local v10    # "wifiConfig":Landroid/net/wifi/WifiConfiguration;
    .end local v11    # "wifiState":I
    :cond_6
    const-string v12, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    const-string v12, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 688
    :cond_7
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - NETWORK_STATE_CHANGED_ACTION"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    const-string v12, "networkInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/net/NetworkInfo;

    .line 692
    .local v7, "info":Landroid/net/NetworkInfo;
    if-eqz v7, :cond_1

    .line 695
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils$2;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_1

    goto/16 :goto_0

    .line 697
    :pswitch_5
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - Connected"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    iget-boolean v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->supplicantStateCompleted:Z

    if-eqz v12, :cond_1

    .line 701
    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mSyncLock:Ljava/lang/Object;

    monitor-enter v13

    .line 702
    :try_start_0
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->unregisterWifiReceiver()V

    .line 704
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    if-eqz v12, :cond_8

    .line 705
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-static {v12}, Lcom/samsung/groupcast/application/MainQueue;->cancel(Ljava/lang/Runnable;)V

    .line 706
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 708
    :cond_8
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 710
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto/16 :goto_0

    .line 708
    :catchall_0
    move-exception v12

    :try_start_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v12

    .line 715
    :pswitch_6
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "WiFi Receiver - Suspended"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 719
    :pswitch_7
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "Wifi Receiver - Connecting"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 723
    :pswitch_8
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "Wifi Receiver - Disconnecting"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 727
    :pswitch_9
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "Wifi Receiver - Disconnected"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->DISCONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto/16 :goto_0

    .line 733
    :pswitch_a
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "Wifi Receiver - Unknown"

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 736
    .end local v7    # "info":Landroid/net/NetworkInfo;
    :cond_9
    const-string v12, "android.net.wifi.supplicant.STATE_CHANGE"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 737
    const-string v12, "connected"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 739
    .local v4, "connected":Z
    const-string v12, "supplicantError"

    const/4 v13, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 741
    .local v5, "error":I
    const-string v12, "newState"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/SupplicantState;

    .line 743
    .local v9, "state":Landroid/net/wifi/SupplicantState;
    if-eqz v9, :cond_c

    .line 744
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SUPPLICANT_STATE_CHANGED_ACTION: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v9}, Landroid/net/wifi/SupplicantState;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    sget-object v12, Landroid/net/wifi/SupplicantState;->COMPLETED:Landroid/net/wifi/SupplicantState;

    invoke-virtual {v9, v12}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 748
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->supplicantStateCompleted:Z

    .line 750
    :cond_a
    sget-object v12, Landroid/net/wifi/SupplicantState;->DISCONNECTED:Landroid/net/wifi/SupplicantState;

    invoke-virtual {v9, v12}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 751
    iget v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mDisconnectCount:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mDisconnectCount:I

    .line 753
    :cond_b
    sget-object v12, Landroid/net/wifi/SupplicantState;->ASSOCIATING:Landroid/net/wifi/SupplicantState;

    invoke-virtual {v9, v12}, Landroid/net/wifi/SupplicantState;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 754
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v12

    sget-object v13, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 758
    :cond_c
    if-nez v4, :cond_d

    const/4 v12, 0x1

    if-ne v5, v12, :cond_d

    .line 759
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->AUTH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-virtual {p0, v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->iFailed(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    goto/16 :goto_0

    .line 760
    :cond_d
    iget v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mDisconnectCount:I

    const/4 v13, 0x5

    if-ne v12, v13, :cond_1

    .line 761
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->NETWORK_GONE:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-virtual {p0, v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->iFailed(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    goto/16 :goto_0

    .line 763
    .end local v4    # "connected":Z
    .end local v5    # "error":I
    .end local v9    # "state":Landroid/net/wifi/SupplicantState;
    :cond_e
    const-string v12, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 766
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v13, "CONFIGURED_NETWORKS_CHANGED_ACTION: "

    invoke-static {v12, v13}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    iget-object v12, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    iget-object v13, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->mWifiConfiguration:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v12, v13}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isPoorConnectionDetected(Landroid/net/wifi/WifiConfiguration;)Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/samsung/groupcast/application/Environment;->isCheckForInternetServiceOn(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 769
    sget-object v12, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->POOR_CONNECTION:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-virtual {p0, v12}, Lcom/samsung/groupcast/net/wifi/WifiUtils$GcBroadcastReceiver;->iFailed(Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;)V

    goto/16 :goto_0

    .line 622
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch

    .line 695
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
