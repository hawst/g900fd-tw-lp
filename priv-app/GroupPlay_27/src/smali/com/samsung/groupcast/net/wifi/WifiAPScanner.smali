.class public Lcom/samsung/groupcast/net/wifi/WifiAPScanner;
.super Landroid/content/BroadcastReceiver;
.source "WifiAPScanner.java"

# interfaces
.implements Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;
    }
.end annotation


# static fields
.field private static final SCAN_EXPIRATION_TIME:I = 0xbb8

.field public static final SCAN_RESULTS:I = 0x1

.field private static final SCAN_RESULT_TIMEOUT:J = 0x1f40L

.field public static final SCAN_STARTED:I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private lastForceScanTime:J

.field private mFullScan:Z

.field private mIsScanOngoing:Ljava/lang/Boolean;

.field mLastScanResultsTime:J

.field mScanResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private scanEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

.field private scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "WFSCN"

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wifiManager"    # Landroid/net/wifi/WifiManager;

    .prologue
    const-wide/16 v2, 0x0

    .line 45
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    .line 36
    new-instance v1, Lcom/samsung/groupcast/misc/utility/EventHandler;

    invoke-direct {v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;-><init>()V

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scanEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    .line 37
    iput-wide v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->lastForceScanTime:J

    .line 39
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    .line 40
    iput-wide v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mLastScanResultsTime:J

    .line 46
    iput-object p2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.OXYGEN_SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 53
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/net/wifi/WifiAPScanner;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->doStartScan()Z

    move-result v0

    return v0
.end method

.method private doStartScan()Z
    .locals 9

    .prologue
    const-wide/16 v7, 0x1f40

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 245
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "--- "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mFullScan:Z

    if-eqz v0, :cond_1

    const-string v0, "FULL SCAN"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 264
    :cond_0
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    monitor-enter v1

    .line 265
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 266
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v3, "scan in progress..."

    invoke-static {v0, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getLastScanResultsTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    cmp-long v0, v3, v7

    if-gez v0, :cond_2

    .line 269
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v2

    .line 302
    :goto_1
    return v0

    .line 245
    :cond_1
    const-string v0, "PARTIAL SCAN"

    goto :goto_0

    .line 256
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v2, "Wifi disabled"

    invoke-static {v0, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 258
    goto :goto_1

    .line 271
    :cond_2
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->lastForceScanTime:J

    sub-long/2addr v3, v5

    cmp-long v0, v3, v7

    if-gez v0, :cond_3

    .line 272
    monitor-exit v1

    move v0, v2

    goto :goto_1

    .line 274
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->lastForceScanTime:J

    .line 276
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v3, "Forcing another scan request - we dropped results!"

    invoke-static {v0, v3}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    :cond_4
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 281
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    .line 283
    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mFullScan:Z

    if-eqz v0, :cond_5

    .line 284
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->startMobileAPScan()V

    .line 299
    :goto_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mFullScan:Z

    .line 300
    monitor-exit v1

    move v0, v2

    .line 302
    goto :goto_1

    .line 287
    :cond_5
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_2

    .line 293
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto :goto_2

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 289
    :pswitch_3
    :try_start_2
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->scanSession()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 287
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scanEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    return-object v0
.end method

.method private kickPeriodicScanner()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;-><init>(Lcom/samsung/groupcast/net/wifi/WifiAPScanner;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    .line 117
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->start()V

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 122
    monitor-exit v1

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private startMobileAPScan()V
    .locals 8

    .prologue
    .line 312
    const/4 v3, 0x1

    .line 314
    .local v3, "internalAPISuccess":Z
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v4

    .line 318
    .local v4, "msg":Landroid/os/Message;
    sget-object v5, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v5}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 372
    :goto_0
    return-void

    .line 321
    :pswitch_0
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v6, "mWifiManager.startScan() in startMobileAPScan()"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->scanSession()V

    goto :goto_0

    .line 326
    :pswitch_1
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v6, "mWifiManager.startScan() in startMobileAPScan()"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto :goto_0

    .line 333
    :pswitch_2
    :try_start_0
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->isGroupPlayAPName(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 336
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NO partial scan.  mWifiManager.getConnectionInfo():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    .line 337
    const/4 v3, 0x0

    .line 356
    :goto_1
    if-eqz v3, :cond_1

    .line 357
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v6, "mWifiManager.callSECApi(msg) in startMobileAPScan()"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const/4 v5, 0x3

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    .line 359
    .local v1, "channel":[I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 360
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "channel"

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 361
    iput-object v0, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 363
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5, v4}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    goto :goto_0

    .line 342
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "channel":[I
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "SEC_COMMAND_ID_PARTIAL_SCAN"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    iput v5, v4, Landroid/os/Message;->what:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 346
    :catch_0
    move-exception v2

    .line 347
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 348
    const/4 v3, 0x0

    .line 355
    goto :goto_1

    .line 349
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 350
    .local v2, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 351
    const/4 v3, 0x0

    .line 355
    goto :goto_1

    .line 352
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .line 353
    .local v2, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v2}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    .line 354
    const/4 v3, 0x0

    goto :goto_1

    .line 365
    .end local v2    # "e":Ljava/lang/NoSuchFieldException;
    :cond_1
    sget-object v5, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v6, "mWifiManager.startScan() in startMobileAPScan()"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->startScan()Z

    goto/16 :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 358
    :array_0
    .array-data 4
        0x96c
        0x985
        0x99e
    .end array-data
.end method


# virtual methods
.method public getLastScanResultsTime()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mLastScanResultsTime:J

    return-wide v0
.end method

.method public getScanResults()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 190
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mLastScanResultsTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 191
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v1, "Scan Results not fresh :/"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v0, 0x0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    goto :goto_0
.end method

.method public kickStartScan(Z)V
    .locals 3
    .param p1, "full"    # Z

    .prologue
    .line 126
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "full = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iput-boolean p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mFullScan:Z

    .line 129
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->kickPeriodicScanner()V

    .line 130
    return-void
.end method

.method public onP2PChanged(IZI)V
    .locals 0
    .param p1, "event"    # I
    .param p2, "result"    # Z
    .param p3, "code"    # I

    .prologue
    .line 377
    return-void
.end method

.method public onP2PConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 0
    .param p1, "info"    # Landroid/net/wifi/p2p/WifiP2pInfo;

    .prologue
    .line 419
    return-void
.end method

.method public onP2PEventReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 425
    return-void
.end method

.method public onP2PScaned(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 12
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 383
    iget-object v11, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    monitor-enter v11

    .line 384
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 385
    :cond_0
    const-string v1, "error scan result is null or 0 "

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 386
    monitor-exit v11

    .line 413
    :goto_0
    return-void

    .line 389
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 401
    .local v8, "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    new-instance v0, Landroid/net/wifi/ScanResult;

    iget-object v1, v8, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/wifi/WifiSsid;->createFromAsciiEncoded(Ljava/lang/String;)Landroid/net/wifi/WifiSsid;

    move-result-object v1

    iget-object v2, v8, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/net/wifi/ScanResult;-><init>(Landroid/net/wifi/WifiSsid;Ljava/lang/String;Ljava/lang/String;IIJ)V

    .line 404
    .local v0, "s":Landroid/net/wifi/ScanResult;
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 412
    .end local v0    # "s":Landroid/net/wifi/ScanResult;
    .end local v8    # "d":Landroid/net/wifi/p2p/WifiP2pDevice;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :catchall_0
    move-exception v1

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 407
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_2
    :try_start_1
    iput-object v10, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    .line 408
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    .line 409
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mLastScanResultsTime:J

    .line 410
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V

    .line 412
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v4, "received scan results"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    monitor-enter v4

    .line 160
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    .line 162
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mIsScanOngoing:Ljava/lang/Boolean;

    .line 164
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    if-nez v3, :cond_0

    .line 165
    monitor-exit v4

    .line 187
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mLastScanResultsTime:J

    .line 169
    const-string v3, "android.net.wifi.OXYGEN_SCAN_RESULTS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 170
    const-string v3, "WIFIAPSCAN"

    const-string v5, "oxyzen scan!"

    invoke-static {v3, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v1, "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 175
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/ScanResult;

    .line 176
    .local v2, "item":Landroid/net/wifi/ScanResult;
    iget-object v3, v2, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v5, "[IBSS]"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 177
    const-string v3, "WIFIAPSCAN"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found IBSS network : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 186
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .end local v2    # "item":Landroid/net/wifi/ScanResult;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 182
    .restart local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5, v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V

    .line 186
    .end local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :goto_2
    monitor-exit v4

    goto :goto_0

    .line 184
    :cond_3
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    invoke-virtual {v3, v5, v6}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public registerForScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 7
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 64
    monitor-enter p0

    .line 65
    :try_start_0
    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->TAG:Ljava/lang/String;

    const-string v5, "---"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->kickPeriodicScanner()V

    .line 69
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->registerListener(Landroid/os/Handler;)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 73
    .local v3, "scanResult":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v3, :cond_0

    .line 75
    sget-object v4, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v4}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 103
    :cond_0
    :goto_0
    monitor-exit p0

    .line 104
    return-void

    .line 78
    :pswitch_0
    const-string v4, "WIFIAPSCAN"

    const-string v5, "oxyzen scan!"

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v1, "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 83
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->mScanResults:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/ScanResult;

    .line 84
    .local v2, "item":Landroid/net/wifi/ScanResult;
    iget-object v4, v2, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    const-string v5, "[IBSS]"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 85
    const-string v4, "WIFIAPSCAN"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found IBSS network : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/groupcast/application/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 103
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .end local v2    # "item":Landroid/net/wifi/ScanResult;
    .end local v3    # "scanResult":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 90
    .restart local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    .restart local v3    # "scanResult":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 94
    .end local v1    # "ibssList":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 98
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setP2PListener()V
    .locals 1

    .prologue
    .line 60
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->addOnP2PEventListener(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;)V

    .line 61
    return-void
.end method

.method public stopScan()V
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->getListenersCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 136
    :cond_0
    monitor-exit p0

    .line 141
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;->stopSelf()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->scannerThread:Lcom/samsung/groupcast/net/wifi/WifiAPScanner$PeriodicScanner;

    .line 140
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 107
    monitor-enter p0

    .line 108
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/misc/utility/EventHandler;->unregisterListener(Landroid/os/Handler;)V

    .line 109
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanningEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;->getListenersCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->stopScan()V

    .line 111
    :cond_0
    monitor-exit p0

    .line 112
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
