.class public Lcom/samsung/groupcast/net/wifi/WifiAPUtils;
.super Ljava/lang/Object;
.source "WifiAPUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_WIFIAP_TIMEOUT:I = 0x258

.field public static final GROUP_PLAY_AP_IDENTIFIER:Ljava/lang/String; = "[GPCAST]"

.field public static final GROUP_PLAY_AP_PREFIX:Ljava/lang/String; = "GroupPlay-"

.field public static final GROUP_PLAY_AP_SUFFIX:Ljava/lang/String; = "P-"

.field public static final MHS_REQUEST:I = 0x0

.field protected static final RVF_HOTSPOT_DISABLED:I = 0x2

.field protected static final RVF_HOTSPOT_DISABLING:I = 0x5

.field protected static final RVF_HOTSPOT_DISABLING_STARTED:I = 0x1

.field protected static final RVF_HOTSPOT_ENABLED:I = 0x4

.field protected static final RVF_HOTSPOT_ENABLING:I = 0x3

.field protected static final RVF_HOTSPOT_ENABLING_STARTED:I = 0x0

.field protected static final RVF_HOTSPOT_FAILED:I = 0x6

.field public static final RVF_MODE_DEFAULT:I = 0x0

.field public static final RVF_MODE_NAT:I = 0x2

.field public static final RVF_MODE_NO_NAT:I = 0x1

.field public static final WIFIAP_POWER_MODE_ALARM:Ljava/lang/String; = "android.net.wifi.wifiap_power_mode_alarm"

.field public static final WIFIAP_POWER_MODE_ALARM_EXPIRE:I = 0x1

.field public static final WIFIAP_POWER_MODE_ALARM_OPTION:Ljava/lang/String; = "wifiap_power_mode_alarm_option"

.field public static final WIFIAP_POWER_MODE_ALARM_START:I = 0x0

.field public static final WIFIAP_POWER_MODE_ALARM_STOP:I = 0x2

.field public static final WIFIAP_POWER_MODE_ALARM_UNKNOWN:I = 0x3

.field public static final WIFIAP_POWER_MODE_VALUE_CHANGED:I = 0x4

.field public static final WIFI_TETHERING:I

.field protected static mRvfHotspotState:I

.field private static mRvfManager:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;


# instance fields
.field private final RVF_MODE_PROVISIONING_FEATURE_TYPE:I

.field protected mApCheckingObject:Ljava/lang/Object;

.field protected mCheckingObject:Ljava/lang/Object;

.field private final mContext:Landroid/content/Context;

.field private mIsTetherCheckCompleted:Z

.field private mNATModeForRVFMode:I

.field private mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

.field private mProtected:Z

.field private mProvisionApp:[Ljava/lang/String;

.field private mUserWifiApState:I

.field private mUserWifiState:I

.field protected mWifiCheckingObject:Ljava/lang/Object;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private final mWifiReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfManager:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .line 49
    const/4 v0, 0x2

    sput v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->RVF_MODE_PROVISIONING_FEATURE_TYPE:I

    .line 44
    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I

    .line 45
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I

    .line 47
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mNATModeForRVFMode:I

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mCheckingObject:Ljava/lang/Object;

    .line 51
    iput-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    .line 52
    iput-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    .line 53
    iput-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mIsTetherCheckCompleted:Z

    .line 63
    iput-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProtected:Z

    .line 423
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$4;-><init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    .line 74
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->registerReceiver()V

    .line 77
    return-void
.end method

.method public static BroadCastGroupPlayAPStartNStopForTEDC(Z)V
    .locals 3
    .param p0, "bIsStart"    # Z

    .prologue
    .line 568
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 569
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isStart:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 570
    if-eqz p0, :cond_0

    .line 571
    const-string v1, "com.TEDC.GROUPPLAY_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 575
    :goto_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/application/App;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 576
    return-void

    .line 573
    :cond_0
    const-string v1, "com.TEDC.GROUPPLAY_STOPPED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->keepAliveWifiAP()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->unregisterReceiver()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->disableWifiAP()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->enableWifiAP()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->disableWifi()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRVFMode()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private disableWifi()V
    .locals 3

    .prologue
    .line 355
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 356
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mCheckingObject:Ljava/lang/Object;

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    .line 357
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 358
    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    monitor-enter v2

    .line 360
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 361
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiCheckingObject:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 367
    :cond_0
    return-void

    .line 362
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 365
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private disableWifiAP()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 288
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    .line 289
    const/4 v1, 0x1

    sput v1, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    .line 290
    const-string v1, "setRvfHotspotEnable - User hotspot ap set to disable"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 291
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mCheckingObject:Ljava/lang/Object;

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    .line 292
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 293
    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    monitor-enter v2

    .line 295
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;

    const-wide/16 v3, 0x258

    invoke-virtual {v1, v3, v4}, Ljava/lang/Object;->wait(J)V

    .line 296
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mApCheckingObject:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 302
    :cond_0
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private enableWifiAP()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 336
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mNATModeForRVFMode:I

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setRVFMode(I)I

    .line 337
    const/4 v0, 0x0

    sput v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    .line 339
    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->BroadCastGroupPlayAPStartNStopForTEDC(Z)V

    .line 341
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getWifiConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mIsTetherCheckCompleted:Z

    .line 345
    :cond_0
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$2;-><init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    const-wide/32 v1, 0xea60

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/SideWorkingQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 352
    return-void
.end method

.method public static getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfManager:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    return-object v0
.end method

.method private getRVFMode()I
    .locals 4

    .prologue
    .line 206
    const/4 v1, -0x1

    .line 207
    .local v1, "mode":I
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 209
    .local v2, "msg":Landroid/os/Message;
    const/16 v3, 0x1c

    iput v3, v2, Landroid/os/Message;->what:I

    .line 212
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3, v2}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 217
    :goto_0
    return v1

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method public static getRvfHotspotSSID()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x20

    .line 221
    const-string v0, ""

    .line 222
    .local v0, "ssid":Ljava/lang/String;
    const-string v2, "%s%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "GroupPlay-"

    aput-object v4, v3, v7

    const/4 v4, 0x1

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v6, :cond_0

    .line 227
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .end local v0    # "ssid":Ljava/lang/String;
    .local v1, "ssid":Ljava/lang/String;
    move-object v0, v1

    .line 231
    .end local v1    # "ssid":Ljava/lang/String;
    .restart local v0    # "ssid":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v6, :cond_0

    .line 232
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_0
    return-object v0
.end method

.method public static getRvfHotspotSSID(Z)Ljava/lang/String;
    .locals 8
    .param p0, "isProtected"    # Z

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x20

    .line 239
    const-string v0, ""

    .line 240
    .local v0, "ssid":Ljava/lang/String;
    const-string v3, "%s%s%s"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/Object;

    const-string v2, "GroupPlay-"

    aput-object v2, v4, v7

    const/4 v5, 0x1

    if-eqz p0, :cond_0

    const-string v2, "P-"

    :goto_0
    aput-object v2, v4, v5

    const/4 v2, 0x2

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v6, :cond_1

    .line 247
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .end local v0    # "ssid":Ljava/lang/String;
    .local v1, "ssid":Ljava/lang/String;
    move-object v0, v1

    .line 251
    .end local v1    # "ssid":Ljava/lang/String;
    .restart local v0    # "ssid":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v6, :cond_1

    .line 252
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 240
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 255
    :cond_1
    return-object v0
.end method

.method private getWifiConfig()Landroid/net/wifi/WifiConfiguration;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 267
    new-instance v2, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v2}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 269
    .local v2, "wifiConfig":Landroid/net/wifi/WifiConfiguration;
    iget-boolean v3, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProtected:Z

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRvfHotspotSSID(Z)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 270
    const-string v3, "GroupCast"

    iput-object v3, v2, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 271
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 272
    iget-object v3, v2, Landroid/net/wifi/WifiConfiguration;->allowedProtocols:Ljava/util/BitSet;

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 275
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "vendorIE"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 276
    .local v1, "vsi":Ljava/lang/reflect/Field;
    const/16 v3, 0x10

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 284
    .end local v1    # "vsi":Ljava/lang/reflect/Field;
    :goto_0
    return-object v2

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v3, "getWifiConfig() NoSuchFieldException thrown"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 279
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 280
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "getWifiConfig() IllegalArgumentException thrown"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 281
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "getWifiConfig() IllegalAccessException thrown"

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private keepAliveWifiAP()V
    .locals 3

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->isRvfHotspotEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const/16 v0, 0x258

    invoke-direct {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setWifiApTimeOut(I)V

    .line 324
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$1;-><init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    const-wide/32 v1, 0x83d60

    invoke-static {v0, v1, v2}, Lcom/samsung/groupcast/application/SideWorkingQueue;->postDelayed(Ljava/lang/Runnable;J)V

    .line 331
    :cond_0
    return-void
.end method

.method private registerReceiver()V
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_AP_STA_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 98
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 101
    return-void
.end method

.method private setRVFMode(I)I
    .locals 6
    .param p1, "mode"    # I

    .prologue
    .line 186
    const/4 v3, -0x1

    .line 187
    .local v3, "ret":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 189
    .local v0, "args":Landroid/os/Bundle;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RVFMode:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 191
    const-string v4, "mode"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 192
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 193
    .local v2, "msg":Landroid/os/Message;
    const/16 v4, 0x1b

    iput v4, v2, Landroid/os/Message;->what:I

    .line 194
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 197
    :try_start_0
    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4, v2}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 202
    :goto_0
    return v3

    .line 198
    :catch_0
    move-exception v1

    .line 199
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method private setWifiApTimeOut(I)V
    .locals 4
    .param p1, "sec"    # I

    .prologue
    .line 315
    const-string v1, "WifiAPUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWifiApTimeOut, sec = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.net.wifi.wifiap_power_mode_alarm"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 317
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "wifiap_power_mode_alarm_option"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 318
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/groupcast/application/App;->sendBroadcast(Landroid/content/Intent;)V

    .line 319
    return-void
.end method

.method private unregisterReceiver()V
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getNATModeForRVFMode()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mNATModeForRVFMode:I

    return v0
.end method

.method public getOnRvfHotspotChangeListener()Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    return-object v0
.end method

.method public getProvisionApp()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProvisionApp:[Ljava/lang/String;

    return-object v0
.end method

.method public isProtected()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProtected:Z

    return v0
.end method

.method public isProvisioningNeeded()Z
    .locals 1

    .prologue
    .line 128
    const-string v0, "always NO NAT for RVF"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setNATModeForRVFMode(I)V

    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public isRvfHotspotEnabled()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 558
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRVFMode()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getRVFMode()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 560
    :cond_0
    const-string v1, "isRvfHotspotEnabled() returns true"

    invoke-static {v1}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 563
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseRvfHotspot()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 412
    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->BroadCastGroupPlayAPStartNStopForTEDC(Z)V

    .line 414
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x1

    sput v0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mRvfHotspotState:I

    .line 418
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setRVFMode(I)I

    .line 420
    iput-boolean v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mIsTetherCheckCompleted:Z

    .line 421
    return-void
.end method

.method public setNATModeForRVFMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mNATModeForRVFMode:I

    .line 179
    return-void
.end method

.method public setOnRvfHotspotChangeListener(Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    .prologue
    .line 550
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mOnRvfHotspotChangeListener:Lcom/samsung/groupcast/net/wifi/WifiAPUtils$OnRvfHotspotChangeListener;

    .line 551
    return-void
.end method

.method public setProtected(Z)V
    .locals 0
    .param p1, "isProtected"    # Z

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProtected:Z

    .line 260
    return-void
.end method

.method public setRvfHotspotEnable()Z
    .locals 2

    .prologue
    .line 370
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;

    invoke-direct {v1, p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;-><init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 401
    .local v0, "hotspotCheckingThread":Ljava/lang/Thread;
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->unregisterReceiver()V

    .line 402
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->registerReceiver()V

    .line 404
    const-string v1, "HotspotCheckingThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 405
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 407
    iget-boolean v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mIsTetherCheckCompleted:Z

    return v1
.end method

.method public startProvisioning()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProvisionApp:[Ljava/lang/String;

    aget-object v1, v1, v4

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mProvisionApp:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    const-string v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 169
    const/high16 v1, 0x800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 174
    return-void
.end method
