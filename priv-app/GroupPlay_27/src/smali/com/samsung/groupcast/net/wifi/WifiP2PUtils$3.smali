.class Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;
.super Landroid/content/BroadcastReceiver;
.source "WifiP2PUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 452
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 453
    .local v0, "action":Ljava/lang/String;
    const-string v5, "WifiP2PUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onReceive.....  :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string v5, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 457
    const-string v5, "WifiP2PUtils"

    const-string v6, "peer changed..........  "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v6}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$400(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-virtual {v5, v6, v7}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 461
    :cond_0
    const-string v5, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 463
    const-string v5, "networkInfo"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/NetworkInfo;

    .line 465
    .local v4, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_1

    .line 466
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 467
    const-string v5, "WifiP2PUtils"

    const-string v6, "Wifi P2P Receiver - Connected"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 486
    :goto_0
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # setter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;
    invoke-static {v5, v4}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$502(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;

    .line 490
    .end local v4    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_1
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    .line 491
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 493
    .local v3, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :try_start_0
    invoke-interface {v3, p1, p2}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PEventReceive(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 494
    :catch_0
    move-exception v1

    .line 495
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 471
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    .restart local v4    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_2
    const-string v5, "WifiP2PUtils"

    const-string v6, "Wifi P2P Receiver - Disconnected"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$500(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/NetworkInfo;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$500(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/NetworkInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 473
    const-string v5, "WifiP2PUtils"

    const-string v6, "Wifi P2P Receiver - Disconnected launch main"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :try_start_1
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiListener()Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiStateBroadcastReceiver;->callMainActivityOnDisconnected()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 476
    :catch_1
    move-exception v1

    .line 477
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 480
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    const-string v5, "WifiP2PUtils"

    const-string v6, "Wifi P2P Receiver - Disconnected set as failed"

    invoke-static {v5, v6}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-virtual {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v5

    sget-object v6, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->FAILED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto :goto_0

    .line 498
    .end local v4    # "networkInfo":Landroid/net/NetworkInfo;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_4
    return-void
.end method
