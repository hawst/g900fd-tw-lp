.class public final enum Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;
.super Ljava/lang/Enum;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WifiConnectToFail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum AUTH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum DISABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum NETWORK_GONE:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum NETWORK_TIMEOUT:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum POOR_CONNECTION:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

.field public static final enum USER_REQUEST:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 80
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "NETWORK_TIMEOUT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->NETWORK_TIMEOUT:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 81
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "AUTH"

    invoke-direct {v0, v1, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->AUTH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 82
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "NETWORK_GONE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->NETWORK_GONE:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 83
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v6}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->DISABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 84
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "USER_REQUEST"

    invoke-direct {v0, v1, v7}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->USER_REQUEST:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 85
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    const-string v1, "POOR_CONNECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->POOR_CONNECTION:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    .line 79
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->NETWORK_TIMEOUT:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->AUTH:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->NETWORK_GONE:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->DISABLED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->USER_REQUEST:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->POOR_CONNECTION:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-class v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    return-object v0
.end method

.method public static values()[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->$VALUES:[Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    invoke-virtual {v0}, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectToFail;

    return-object v0
.end method
