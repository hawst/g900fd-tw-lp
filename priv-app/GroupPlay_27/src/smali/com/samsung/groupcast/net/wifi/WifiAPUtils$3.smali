.class Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;
.super Ljava/lang/Object;
.source "WifiAPUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->setRvfHotspotEnable()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V
    .locals 0

    .prologue
    .line 370
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v1

    # setter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I
    invoke-static {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$102(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;I)I

    .line 376
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    # setter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v0, v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$302(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;I)I

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mUserWifiApState:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I
    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mUserWifiState:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v1}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 380
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiApState:I
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 398
    :goto_0
    return-void

    .line 383
    :pswitch_0
    const-string v0, "setRvfHotspotEnable - User hotspot has been enabled"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->disableWifiAP()V
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$400(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    .line 385
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->enableWifiAP()V
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$500(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    goto :goto_0

    .line 391
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->mUserWifiState:I
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$300(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 392
    :cond_0
    const-string v0, "setRvfHotspotEnable - User wifi enabled, set to disable"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->disableWifi()V
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$600(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    .line 395
    :cond_1
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiAPUtils$3;->this$0:Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->enableWifiAP()V
    invoke-static {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->access$500(Lcom/samsung/groupcast/net/wifi/WifiAPUtils;)V

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
