.class Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;
.super Ljava/lang/Object;
.source "WifiP2PUtils.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 6
    .param p1, "reason"    # I

    .prologue
    .line 351
    const-string v3, "WifiP2PUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "failed for:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getCmdName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",re:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "failed for:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v4

    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getCmdName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",re:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    .line 353
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    .line 354
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 356
    .local v2, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, p1}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PChanged(IZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 359
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 363
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :cond_0
    return-void
.end method

.method public onSuccess()V
    .locals 6

    .prologue
    .line 335
    const-string v3, "WifiP2PUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "success for:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v5

    invoke-static {v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getCmdName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "success for:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v4

    invoke-static {v4}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getCmdName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->showToast(Ljava/lang/String;)V

    .line 338
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # invokes: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$100(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    .line 339
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 341
    .local v2, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PChanged(IZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 344
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :cond_0
    return-void
.end method
