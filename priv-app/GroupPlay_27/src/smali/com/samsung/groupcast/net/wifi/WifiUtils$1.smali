.class Lcom/samsung/groupcast/net/wifi/WifiUtils$1;
.super Landroid/content/BroadcastReceiver;
.source "WifiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/net/wifi/WifiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;


# direct methods
.method constructor <init>(Lcom/samsung/groupcast/net/wifi/WifiUtils;)V
    .locals 0

    .prologue
    .line 1029
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1034
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1035
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.net.wifi.WIFI_OXYGEN_STATE_CHANGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1036
    const-string v3, "wifi_state"

    const/4 v4, 0x4

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1039
    .local v2, "wifiState":I
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", state:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->GetWifiStateString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", state:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->GetWifiStateString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 1042
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1043
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;->onWifiStateChanged(I)V

    .line 1046
    :cond_0
    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 1047
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v4, "WiFi Receiver - WIFI_STATE_ENABLED"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->AUTHENTICATING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    .line 1083
    .end local v2    # "wifiState":I
    :cond_1
    :goto_0
    return-void

    .line 1051
    .restart local v2    # "wifiState":I
    :cond_2
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1052
    sget-object v3, Lcom/samsung/groupcast/net/wifi/WifiUtils;->TAG:Ljava/lang/String;

    const-string v4, "WiFi Receiver - WIFI_STATE_DISABLED"

    invoke-static {v3, v4}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ENABLING:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto :goto_0

    .line 1057
    .end local v2    # "wifiState":I
    :cond_3
    const-string v3, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1058
    const-string v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 1059
    .local v1, "info":Landroid/net/NetworkInfo;
    const-string v3, "NETWORK_OXYGEN"

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 1061
    if-eqz v1, :cond_1

    .line 1062
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    # setter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mIsOxygenConnected:Z
    invoke-static {v3, v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$302(Lcom/samsung/groupcast/net/wifi/WifiUtils;Z)Z

    .line 1063
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->ShowToast(Ljava/lang/String;)V

    .line 1065
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 1067
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1068
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    # getter for: Lcom/samsung/groupcast/net/wifi/WifiUtils;->mWifiMotherFatherOxygenDelegate:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;
    invoke-static {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->access$200(Lcom/samsung/groupcast/net/wifi/WifiUtils;)Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiMotherFatherOxygenDelegate;->onNetworkStatusChanged(Landroid/net/NetworkInfo;)V

    .line 1071
    :cond_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1072
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->CONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto/16 :goto_0

    .line 1075
    :cond_5
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiUtils$1;->this$0:Lcom/samsung/groupcast/net/wifi/WifiUtils;

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;

    move-result-object v3

    sget-object v4, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->DISCONNECTED:Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;

    invoke-virtual {v4}, Lcom/samsung/groupcast/net/wifi/WifiUtils$WifiConnectStatus;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/groupcast/misc/utility/EventHandler;->sendMessage(I)V

    goto/16 :goto_0
.end method
