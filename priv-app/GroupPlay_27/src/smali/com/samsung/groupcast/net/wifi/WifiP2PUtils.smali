.class public Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
.super Ljava/lang/Object;
.source "WifiP2PUtils.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    }
.end annotation


# static fields
.field public static final GROUP_PLAY_P2P_IDENTIFIER:Ljava/lang/String; = "[GPCAST]"

.field public static final GROUP_PLAY_P2P_PREFIX:Ljava/lang/String; = "DIRECT-"

.field public static final GROUP_PLAY_P2P_TESTNAME:Ljava/lang/String; = "P2P_test"

.field public static final P2P_EVENT_CANCEL_CONNECT:I = 0x32

.field public static final P2P_EVENT_CREATE_CONNECT:I = 0xa

.field public static final P2P_EVENT_JOIN_CONNECT:I = 0x14

.field public static final P2P_EVENT_REMOVE:I = 0x28

.field public static final P2P_EVENT_SCAN:I = 0x1e

.field public static final P2P_EVENT_STOPSCAN:I = 0x23

.field private static final TAG:Ljava/lang/String; = "WifiP2PUtils"

.field public static final WifiP2pInterfaceName:Ljava/lang/String; = "p2p-wlan0-0"

.field private static mInstance:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;


# instance fields
.field private final connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

.field private mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private final mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

.field mConnectionInfoListener:Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private mLastCmd:I

.field private mLastNetInfo:Landroid/net/NetworkInfo;

.field private final mOnP2PEventListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;",
            ">;"
        }
    .end annotation
.end field

.field mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

.field private final mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    invoke-direct {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;-><init>()V

    sput-object v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mInstance:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    .line 50
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 55
    new-instance v0, Lcom/samsung/groupcast/misc/utility/EventHandler;

    invoke-direct {v0}, Lcom/samsung/groupcast/misc/utility/EventHandler;-><init>()V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    .line 332
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$1;-><init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    .line 368
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$2;-><init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConnectionInfoListener:Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;

    .line 449
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;

    invoke-direct {v0, p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$3;-><init>(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 109
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 111
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/groupcast/application/App;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 115
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_0

    .line 117
    const-string v0, "WifiP2PUtils"

    const-string v1, "Failed to set up connection with wifi p2p service"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 124
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 125
    new-instance v0, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-direct {v0, v1, v2}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;)V

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    .line 126
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->registerReceiver()V

    .line 127
    return-void

    .line 121
    :cond_1
    const-string v0, "WifiP2PUtils"

    const-string v1, "mWifiP2pManager is null !"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static IsGroupPlayP2P(Ljava/lang/String;)Z
    .locals 1
    .param p0, "ssid"    # Ljava/lang/String;

    .prologue
    .line 130
    if-eqz p0, :cond_1

    .line 131
    const-string v0, "GroupPlay-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DIRECT-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    const/4 v0, 0x1

    .line 136
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;)Landroid/net/NetworkInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
    .param p1, "x1"    # Landroid/net/NetworkInfo;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    return-object p1
.end method

.method private debugState()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 102
    const-string v0, "WifiP2PUtils"

    const-string v1, " mOnP2PEventListeners: %d, lastCmd:%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public static getCmdName(I)Ljava/lang/String;
    .locals 1
    .param p0, "cmd"    # I

    .prologue
    .line 312
    sparse-switch p0, :sswitch_data_0

    .line 327
    const-string v0, ""

    :goto_0
    return-object v0

    .line 314
    :sswitch_0
    const-string v0, "P2P_EVENT_CREATE_CONNECT"

    goto :goto_0

    .line 316
    :sswitch_1
    const-string v0, "P2P_EVENT_JOIN_CONNECT"

    goto :goto_0

    .line 318
    :sswitch_2
    const-string v0, "P2P_EVENT_SCAN"

    goto :goto_0

    .line 320
    :sswitch_3
    const-string v0, "P2P_EVENT_STOPSCAN"

    goto :goto_0

    .line 322
    :sswitch_4
    const-string v0, "P2P_EVENT_REMOVE"

    goto :goto_0

    .line 324
    :sswitch_5
    const-string v0, "P2P_EVENT_CANCEL_CONNECT"

    goto :goto_0

    .line 312
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x23 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public static getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mInstance:Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    return-object v0
.end method

.method public static getP2PSSID()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x20

    .line 204
    const-string v0, ""

    .line 205
    .local v0, "ssid":Ljava/lang/String;
    const-string v2, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Lcom/samsung/groupcast/application/Environment;->getUserName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v5, :cond_0

    .line 210
    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 214
    .end local v0    # "ssid":Ljava/lang/String;
    .local v1, "ssid":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v2, v2

    if-le v2, v5, :cond_1

    .line 215
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 217
    .end local v1    # "ssid":Ljava/lang/String;
    .restart local v0    # "ssid":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .end local v0    # "ssid":Ljava/lang/String;
    .restart local v1    # "ssid":Ljava/lang/String;
    :cond_1
    move-object v0, v1

    .end local v1    # "ssid":Ljava/lang/String;
    .restart local v0    # "ssid":Ljava/lang/String;
    goto :goto_0
.end method

.method public static showToast(Ljava/lang/String;)V
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 503
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 504
    return-void
.end method


# virtual methods
.method public addOnP2PEventListener(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V

    .line 84
    if-eqz p1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_0
    return-void
.end method

.method public cancelConnect()V
    .locals 3

    .prologue
    .line 248
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 249
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->cancelConnect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 253
    :cond_0
    return-void
.end method

.method public createSession(Ljava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x96c

    .line 222
    const/16 v1, 0xa

    iput v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 223
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    const/4 v2, 0x0

    iput v2, v1, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    .line 224
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iput v5, v1, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 225
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iput-object p1, v1, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 227
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    .line 229
    const-string v1, "WifiP2PUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createSession:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 234
    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v1, :cond_1

    .line 243
    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 245
    :cond_1
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 174
    const-string v0, "WifiP2PUtils"

    const-string v1, "finalizing.."

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->unregisterReceiver()V

    .line 176
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 177
    return-void
.end method

.method public getConnectToEventHandler()Lcom/samsung/groupcast/misc/utility/EventHandler;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->connectEventHandlers:Lcom/samsung/groupcast/misc/utility/EventHandler;

    return-object v0
.end method

.method public getLastCmd()I
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    return v0
.end method

.method public getLastNetInfo()Landroid/net/NetworkInfo;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method public getP2pName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getScanResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 520
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 521
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 522
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->getScanResults()Ljava/util/List;

    move-result-object v0

    .line 524
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInactiveState()Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pManager;->isInactiveState()Z

    move-result v0

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public joinSession(Ljava/lang/String;)V
    .locals 5
    .param p1, "bssid"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 293
    const-string v0, "WifiP2PUtils"

    const-string v1, "mWifiP2pManager join...!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 295
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iput v2, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->netId:I

    .line 297
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iput-object p1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    .line 301
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/16 v2, 0x96c

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConfig:Landroid/net/wifi/p2p/WifiP2pConfig;

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 309
    :cond_1
    return-void
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 6
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 405
    const-string v3, "WifiP2PUtils"

    const-string v4, "onPeersAvailable. start-----"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const-string v3, "WifiP2PUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onPeersAvailable. : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 409
    :cond_0
    const-string v3, "WifiP2PUtils"

    const-string v4, "peers is empty!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    :goto_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 434
    .local v2, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V

    .line 436
    :try_start_0
    invoke-interface {v2, p1}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PScaned(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 411
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :cond_1
    const-string v3, "WifiP2PUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "peers:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 442
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method public registerForScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 507
    const-string v0, "---"

    invoke-static {v0}, Lcom/samsung/groupcast/application/Logger;->a(Ljava/lang/String;)V

    .line 508
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->registerForScanningResultsAvailable(Landroid/os/Handler;)V

    .line 511
    :cond_0
    return-void
.end method

.method public registerReceiver()V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.REQUEST_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    :goto_0
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/groupcast/application/App;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    return-void

    .line 149
    :cond_0
    const-string v0, "WifiP2PUtils"

    const-string v1, "mIntentFilter is null"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeOnP2PEventListener(Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V

    .line 93
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    return-void
.end method

.method public removeSession()V
    .locals 6

    .prologue
    const/16 v5, 0x28

    .line 384
    const-string v3, "WifiP2PUtils"

    const-string v4, "mWifiP2pManager remove group...!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iput v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 386
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastNetInfo:Landroid/net/NetworkInfo;

    .line 387
    invoke-direct {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->debugState()V

    .line 388
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 389
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mOnP2PEventListeners:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;

    .line 391
    .local v2, "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    const/16 v3, 0x28

    const/4 v4, 0x0

    const/16 v5, 0x3e7

    :try_start_0
    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;->onP2PChanged(IZI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 398
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "l":Lcom/samsung/groupcast/net/wifi/WifiP2PUtils$OnP2PEventListener;
    :cond_0
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v3, :cond_1

    .line 399
    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v4, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v5, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v3, v4, v5}, Landroid/net/wifi/p2p/WifiP2pManager;->removeGroup(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 401
    :cond_1
    return-void
.end method

.method public requestConnectInfo()V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mConnectionInfoListener:Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->requestConnectionInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;)V

    .line 261
    :cond_0
    return-void
.end method

.method public scanSession()V
    .locals 4

    .prologue
    .line 265
    const/16 v0, 0x1e

    iput v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 266
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->setP2PListener()V

    .line 268
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiUtils;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;ILandroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 276
    :cond_1
    return-void
.end method

.method public startScan(ZZ)V
    .locals 2
    .param p1, "full"    # Z
    .param p2, "force"    # Z

    .prologue
    .line 529
    sget-object v0, Lcom/samsung/groupcast/core/messaging/MessagingClient;->DefaultInterface:Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;

    invoke-virtual {v0}, Lcom/samsung/groupcast/core/messaging/MessagingClient$MAGNET_CONNECTION_TYPE;->value()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 545
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->kickStartScan(Z)V

    .line 548
    :cond_1
    return-void

    .line 532
    :pswitch_0
    if-eqz p2, :cond_0

    .line 533
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiAPUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPUtils;->releaseRvfHotspot()V

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_0

    .line 529
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public stopScan()V
    .locals 2

    .prologue
    .line 551
    const-string v0, "WifiP2PUtils"

    const-string v1, "stop scan+"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 553
    const-string v0, "WifiP2PUtils"

    const-string v1, "stop scan++"

    invoke-static {v0, v1}, Lcom/samsung/groupcast/application/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->stopScan()V

    .line 556
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->stopScanSession()V

    .line 557
    return-void
.end method

.method public stopScanSession()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v2, 0x23

    .line 280
    invoke-static {}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getInstance()Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->getLastCmd()I

    move-result v0

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_1

    .line 281
    iput v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    iput v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mLastCmd:I

    .line 287
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v2, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mP2PActionListner:Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->stopPeerDiscovery(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto :goto_0
.end method

.method public unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mWifiAPScanner:Lcom/samsung/groupcast/net/wifi/WifiAPScanner;

    invoke-virtual {v0, p1}, Lcom/samsung/groupcast/net/wifi/WifiAPScanner;->unregisterFromScanningResultsAvailable(Landroid/os/Handler;)V

    .line 517
    :cond_0
    return-void
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 157
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 158
    invoke-static {}, Lcom/samsung/groupcast/application/App;->getInstance()Lcom/samsung/groupcast/application/App;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/groupcast/net/wifi/WifiP2PUtils;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/samsung/groupcast/application/App;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 161
    :cond_0
    return-void
.end method
