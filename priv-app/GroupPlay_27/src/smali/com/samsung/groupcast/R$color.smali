.class public final Lcom/samsung/groupcast/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final breadcrumb_separator:I = 0x7f0b0027

.field public static final gp_main_list_divider:I = 0x7f0b0016

.field public static final gp_main_list_header:I = 0x7f0b0015

.field public static final gp_participant_counter_text:I = 0x7f0b0017

.field public static final gp_participant_counter_text_viewer:I = 0x7f0b0018

.field public static final gp_play_game_no_game_text:I = 0x7f0b0019

.field public static final gp_standard_text_color:I = 0x7f0b0014

.field public static final indicator_circle:I = 0x7f0b0012

.field public static final indicator_circle_shadow:I = 0x7f0b0013

.field public static final list_music_subtitle_color_light:I = 0x7f0b000e

.field public static final list_music_title_color_light:I = 0x7f0b000d

.field public static final list_normal_subtitle_color_light:I = 0x7f0b000c

.field public static final list_normal_title_color_light:I = 0x7f0b000b

.field public static final popup_body_sub_text_color_dark:I = 0x7f0b0011

.field public static final popup_body_sub_text_color_light:I = 0x7f0b0010

.field public static final popup_body_text_color_light:I = 0x7f0b000f

.field public static final popup_participant_list_row_subtitle_color:I = 0x7f0b001a

.field public static final tablet_main_participants_text_color:I = 0x7f0b002e

.field public static final tag_action_bar_color:I = 0x7f0b0000

.field public static final tag_background_color:I = 0x7f0b0001

.field public static final tag_dim_01:I = 0x7f0b002d

.field public static final tag_game_action_bar_tab_text_color:I = 0x7f0b002f

.field public static final tag_generic_list_default_label_color:I = 0x7f0b0006

.field public static final tag_generic_list_item_default_color:I = 0x7f0b0007

.field public static final tag_generic_list_subtitle_item_default_color:I = 0x7f0b0009

.field public static final tag_generic_list_title_item_default_color:I = 0x7f0b0008

.field public static final tag_gray_tint_background_color:I = 0x7f0b0003

.field public static final tag_install_btn_text:I = 0x7f0b0030

.field public static final tag_main_list_session_primary_text_color:I = 0x7f0b0031

.field public static final tag_main_list_session_secondary_text_color:I = 0x7f0b0032

.field public static final tag_main_list_start_share_text_color:I = 0x7f0b0033

.field public static final tag_main_sort_text_color:I = 0x7f0b0034

.field public static final tag_main_tab_text_color:I = 0x7f0b0035

.field public static final tag_shadow_black_50:I = 0x7f0b0028

.field public static final tag_shadow_black_75:I = 0x7f0b0029

.field public static final tag_start_create_btn_text_color:I = 0x7f0b0036

.field public static final tag_status_dialog_button_text_color:I = 0x7f0b0037

.field public static final tag_tablet_background_color:I = 0x7f0b0002

.field public static final tag_text_black_1:I = 0x7f0b002c

.field public static final tag_text_white_1:I = 0x7f0b002a

.field public static final tag_text_white_2:I = 0x7f0b002b

.field public static final tag_title_list_normal_color:I = 0x7f0b0004

.field public static final tag_title_list_selected_color:I = 0x7f0b0005

.field public static final theme_category_item_text_color:I = 0x7f0b0021

.field public static final theme_category_new_features_text_color:I = 0x7f0b001f

.field public static final theme_category_new_features_text_view_more_color:I = 0x7f0b0020

.field public static final theme_help_page_item_group_text_color:I = 0x7f0b0024

.field public static final theme_help_page_item_text_color:I = 0x7f0b0023

.field public static final theme_help_page_item_title_color:I = 0x7f0b0022

.field public static final theme_item_text_color:I = 0x7f0b001b

.field public static final theme_item_text_shadow_color:I = 0x7f0b001c

.field public static final theme_main_activity_background:I = 0x7f0b0026

.field public static final theme_main_activity_item_title:I = 0x7f0b0025

.field public static final theme_section_item_summary_text_color:I = 0x7f0b001e

.field public static final theme_section_item_title_text_color:I = 0x7f0b001d

.field public static final thumbnail_page_indicator_background:I = 0x7f0b000a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
