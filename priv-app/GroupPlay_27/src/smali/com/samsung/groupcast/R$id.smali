.class public final Lcom/samsung/groupcast/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/groupcast/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AP_number:I = 0x7f07003b

.field public static final agree:I = 0x7f070032

.field public static final ap_frame:I = 0x7f070002

.field public static final ap_icon:I = 0x7f070003

.field public static final ap_list:I = 0x7f07003f

.field public static final ap_list_scanning:I = 0x7f070040

.field public static final ap_list_view_blank:I = 0x7f070007

.field public static final ap_list_view_scanning:I = 0x7f070009

.field public static final ap_name:I = 0x7f070004

.field public static final ap_participants:I = 0x7f070005

.field public static final ap_secured:I = 0x7f070006

.field public static final btnCreate:I = 0x7f070011

.field public static final btnJoin:I = 0x7f070012

.field public static final button_cancel:I = 0x7f07004e

.field public static final button_install:I = 0x7f070049

.field public static final dialog_near_by_friends_list:I = 0x7f07000f

.field public static final dialog_near_by_friends_msg:I = 0x7f07000e

.field public static final dialog_near_by_friends_off:I = 0x7f070010

.field public static final dont_show_again_checkbox:I = 0x7f07000d

.field public static final empty_mobile_ap_img:I = 0x7f070008

.field public static final grid_view:I = 0x7f070028

.field public static final group_nickname_title:I = 0x7f070033

.field public static final info_text:I = 0x7f070046

.field public static final input_group_nickname:I = 0x7f070034

.field public static final input_group_nickname_text:I = 0x7f070035

.field public static final layout_install:I = 0x7f070047

.field public static final layout_progressing:I = 0x7f07004a

.field public static final left_arrow:I = 0x7f07003e

.field public static final licenseInfoWebView:I = 0x7f070019

.field public static final license_actionbar:I = 0x7f070016

.field public static final license_dialod:I = 0x7f070052

.field public static final license_scanning:I = 0x7f070018

.field public static final main_activity:I = 0x7f070045

.field public static final main_screen_help:I = 0x7f070060

.field public static final main_screen_info:I = 0x7f07005f

.field public static final main_screen_setting:I = 0x7f07005e

.field public static final main_screen_sort:I = 0x7f07005d

.field public static final main_share_item_image_button:I = 0x7f070026

.field public static final main_share_item_text_view:I = 0x7f070027

.field public static final menu_group_mode:I = 0x7f070061

.field public static final menu_members:I = 0x7f070062

.field public static final mobile_ap_name:I = 0x7f070013

.field public static final nfc_guide_anim:I = 0x7f07000c

.field public static final nicknameDescription:I = 0x7f07001d

.field public static final nicknameInput:I = 0x7f07001e

.field public static final nickname_layout:I = 0x7f07001c

.field public static final pager:I = 0x7f070037

.field public static final participants_activity:I = 0x7f070021

.field public static final participants_item_layout:I = 0x7f07001f

.field public static final participants_list_view:I = 0x7f070056

.field public static final participants_name:I = 0x7f070020

.field public static final pinDescription:I = 0x7f070023

.field public static final pinInput:I = 0x7f070024

.field public static final pincode_layout:I = 0x7f070022

.field public static final progress_bar:I = 0x7f070054

.field public static final progress_layout:I = 0x7f070053

.field public static final progress_text_view:I = 0x7f070055

.field public static final progressbar_downloding:I = 0x7f07004d

.field public static final right_arrow:I = 0x7f07003d

.field public static final scan_stop:I = 0x7f07003c

.field public static final scanning_progress:I = 0x7f07000a

.field public static final scroll_view:I = 0x7f070031

.field public static final session_pin:I = 0x7f070015

.field public static final session_pin_title:I = 0x7f070014

.field public static final setting_about_text:I = 0x7f07002a

.field public static final setting_header:I = 0x7f070029

.field public static final setting_item_checkbox:I = 0x7f07002c

.field public static final setting_listView:I = 0x7f07002f

.field public static final setting_nickname:I = 0x7f07002d

.field public static final setting_nickname_btn:I = 0x7f07002e

.field public static final setting_title:I = 0x7f07002b

.field public static final setup_layout:I = 0x7f070036

.field public static final showPassword:I = 0x7f070025

.field public static final sort_cancel:I = 0x7f07001b

.field public static final sort_listview:I = 0x7f07001a

.field public static final start_button_layout:I = 0x7f07003a

.field public static final start_groupplay_title:I = 0x7f070039

.field public static final start_license_btn:I = 0x7f070043

.field public static final start_screen_button_create_group:I = 0x7f070041

.field public static final start_screen_help:I = 0x7f07005b

.field public static final start_screen_password_checkbox:I = 0x7f070042

.field public static final start_screen_setting:I = 0x7f07005c

.field public static final start_title_group_play:I = 0x7f070030

.field public static final start_title_license:I = 0x7f070017

.field public static final start_view_except_video:I = 0x7f070038

.field public static final stub_page_move:I = 0x7f070044

.field public static final textview_download_information:I = 0x7f070051

.field public static final textview_downloading:I = 0x7f07004b

.field public static final textview_downloading_size:I = 0x7f07004c

.field public static final textview_translator_information:I = 0x7f070048

.field public static final thumb_content_id:I = 0x7f070000

.field public static final thumb_position_id:I = 0x7f070001

.field public static final thumbnail_count:I = 0x7f07005a

.field public static final thumbnail_image:I = 0x7f070058

.field public static final thumbnail_selector:I = 0x7f070059

.field public static final thumbnail_strip_list:I = 0x7f070057

.field public static final welcome_img_1:I = 0x7f07004f

.field public static final welcome_img_2:I = 0x7f070050

.field public static final welcome_text:I = 0x7f07000b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
