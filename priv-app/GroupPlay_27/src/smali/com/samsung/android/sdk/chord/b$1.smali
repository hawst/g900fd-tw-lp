.class Lcom/samsung/android/sdk/chord/b$1;
.super Landroid/content/BroadcastReceiver;
.source "ConnectivityUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chord/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/samsung/android/sdk/chord/b;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/chord/b;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 436
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 439
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 443
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/b$1;->isInitialStickyBroadcast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 447
    const-string v0, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 448
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 450
    if-nez v0, :cond_2

    .line 451
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 455
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 456
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto :goto_0

    .line 459
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto :goto_0

    .line 462
    :cond_4
    const-string v0, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 463
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 464
    if-nez v0, :cond_5

    .line 465
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 469
    :cond_5
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v2, v1, :cond_0

    .line 470
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    .line 472
    sget-object v1, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v0, :cond_6

    .line 473
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto/16 :goto_0

    .line 475
    :cond_6
    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v1, v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto/16 :goto_0

    .line 481
    :cond_7
    const-string v0, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 482
    const-string v0, "wifi_state"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 484
    const/4 v1, 0x3

    if-eq v0, v1, :cond_8

    const/16 v1, 0xd

    if-ne v0, v1, :cond_9

    .line 485
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto/16 :goto_0

    .line 487
    :cond_9
    if-eq v0, v2, :cond_a

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 488
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto/16 :goto_0

    .line 495
    :cond_b
    const-string v0, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 496
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;)Lcom/samsung/android/sdk/chord/b$a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;)Lcom/samsung/android/sdk/chord/b$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/b$a;->a()V

    goto/16 :goto_0

    .line 499
    :cond_c
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 501
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    invoke-static {v0}, Lcom/samsung/android/sdk/chord/b;->b(Lcom/samsung/android/sdk/chord/b;)V

    goto/16 :goto_0

    .line 502
    :cond_d
    const-string v0, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 504
    if-nez v0, :cond_e

    .line 505
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to extrainfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 509
    :cond_e
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 510
    const-string v0, "ConnectivityUtils"

    const-string v1, "OXYGEN is connected!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V

    goto/16 :goto_0

    .line 514
    :cond_f
    const-string v0, "ConnectivityUtils"

    const-string v1, "OXYGEN is disconnected!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b$1;->a:Lcom/samsung/android/sdk/chord/b;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
