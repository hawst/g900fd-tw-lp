.class public Lcom/samsung/android/sdk/chord/Schord;
.super Ljava/lang/Object;
.source "Schord.java"

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a()Z
    .locals 1

    .prologue
    .line 79
    sget-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    return v0
.end method

.method static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "/home/jeongdaeun/Source/CHORD/Chord_v2"

    return-object v0
.end method


# virtual methods
.method public getVersionCode()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "/home/jeongdaeun/Source/CHORD/Chord_v2"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 55
    if-nez p1, :cond_0

    .line 64
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/SsdkVendorCheck;->isSamsungDevice()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 60
    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v1, "Vendor is not SAMSUNG!"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 63
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/chord/Schord;->a:Z

    goto :goto_0
.end method

.method public isFeatureEnabled(I)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method
