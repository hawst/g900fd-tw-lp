.class public Lcom/samsung/android/sdk/chord/SchordManager;
.super Ljava/lang/Object;
.source "SchordManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;,
        Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;
    }
.end annotation


# static fields
.field public static final INTERFACE_TYPE_WIFI:I = 0x0

.field public static final INTERFACE_TYPE_WIFI_AP:I = 0x2

.field public static final INTERFACE_TYPE_WIFI_P2P:I = 0x1

.field public static final SECURE_PREFIX:Ljava/lang/String; = "#"

.field private static c:Landroid/os/Handler;

.field private static w:I


# instance fields
.field private a:Lcom/samsung/android/sdk/chord/ChordAgent;

.field private b:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

.field private f:I

.field private g:Lcom/samsung/android/sdk/chord/b;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

.field private n:[Ljava/lang/Integer;

.field private o:J

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Lcom/samsung/android/sdk/chord/b/a;

.field private x:Lcom/samsung/android/sdk/chord/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    .line 291
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 248
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 252
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 254
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 256
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 258
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 260
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 262
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 264
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    .line 270
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 272
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    .line 274
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 276
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 278
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 280
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 282
    iput-boolean v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 285
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 287
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 289
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 1378
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chord/SchordManager$2;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    .line 310
    const-string v0, "chord_api"

    const-string v1, "SchordManager : SchordManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/content/Context;)V

    .line 312
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 248
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 252
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 254
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 256
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 258
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 260
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 262
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 264
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    .line 268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    .line 270
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 272
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    .line 274
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 276
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 278
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 280
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 282
    iput-boolean v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 285
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 287
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 289
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 1378
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chord/SchordManager$2;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    .line 331
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/content/Context;)V

    .line 332
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 333
    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chord/SchordManager;->setNetworkListener(Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V

    .line 334
    return-void
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x1

    .line 1023
    if-eqz p2, :cond_0

    const-string v0, "joinSecureChannel : "

    move-object v2, v0

    .line 1024
    :goto_0
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "<"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ">"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v5, v0, :cond_1

    .line 1030
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1023
    :cond_0
    const-string v0, "joinChannel : "

    move-object v2, v0

    goto :goto_0

    .line 1035
    :cond_1
    if-nez p3, :cond_2

    .line 1039
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must be set!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1044
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1047
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid channelName!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    if-nez v0, :cond_5

    if-eqz p2, :cond_5

    .line 1054
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid using security! You must call setUseSecureMode before this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1059
    :cond_5
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v1

    .line 1061
    if-eqz v1, :cond_9

    move-object v0, v1

    .line 1063
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 1065
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/a;->a()I

    move-result v3

    if-nez v3, :cond_7

    .line 1067
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Already joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/chord/a;->a(Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    .line 1116
    :cond_6
    :goto_1
    return-object v1

    .line 1073
    :cond_7
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/a;->a()I

    move-result v3

    if-ne v4, v3, :cond_8

    .line 1075
    const-string v3, "chord_api"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "re-joined!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/chord/a;->a(Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    goto :goto_1

    .line 1083
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1087
    :cond_9
    new-instance v1, Lcom/samsung/android/sdk/chord/a;

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/samsung/android/sdk/chord/a;-><init>(Lcom/samsung/android/sdk/chord/ChordAgent;Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)V

    .line 1089
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v5, v0, :cond_a

    move-object v0, v1

    .line 1090
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    .line 1093
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1095
    const-string v0, "Chord"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    .line 1097
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/a;->a(Ljava/util/List;)V

    goto :goto_1

    .line 1103
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->joinChannel(Ljava/lang/String;)I

    move-result v0

    .line 1105
    if-eqz v0, :cond_6

    .line 1110
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1111
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to join "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    return-object v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1131
    if-nez p1, :cond_0

    .line 1132
    const-string v0, "INTERFACE_TYPE_WIFI"

    .line 1140
    :goto_0
    return-object v0

    .line 1134
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 1135
    const-string v0, "INTERFACE_TYPE_WIFI_P2P"

    goto :goto_0

    .line 1137
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 1138
    const-string v0, "INTERFACE_TYPE_WIFI_AP"

    goto :goto_0

    .line 1140
    :cond_2
    const-string v0, "INTERFACE_TYPE_UNKNOWN"

    goto :goto_0
.end method

.method private a(IZLcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;,
            Ljava/lang/RuntimeException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 885
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v6, v0, :cond_0

    .line 888
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/chord/SchordManager;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 895
    new-instance v0, Lcom/samsung/android/sdk/chord/InvalidInterfaceException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid interface - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/InvalidInterfaceException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 901
    :cond_1
    if-nez p3, :cond_2

    .line 905
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid listener. It must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 910
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 912
    :cond_3
    const-string v0, "chord_api"

    const-string v1, "SchordManager : start : Please call setTempDirectory before start!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    if-nez v0, :cond_5

    if-eqz p2, :cond_5

    .line 919
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call setUseSecureMode before this."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 923
    :cond_5
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : start : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->getChordAgentState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 924
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 925
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordInit(Ljava/lang/String;)I

    move-result v0

    .line 927
    if-eqz v0, :cond_6

    .line 931
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to initialize core - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 936
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSecureMode(Z)Z

    .line 938
    iput-object p3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 940
    iput p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 942
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 944
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-nez v0, :cond_7

    .line 945
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->setLooper(Landroid/os/Looper;)V

    .line 948
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    sget-object v1, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 950
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b;->b()Z

    move-result v0

    if-ne v0, v4, :cond_8

    .line 951
    const-string v0, "chord_api"

    const-string v1, "SchordManager :  Oxygen Connected - Use Multihop Discovery"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUdpDiscover(Z)V

    .line 953
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/ChordAgent;->setMultiHopDiscovery(Z)V

    .line 960
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/ChordAgent;->setNodeExpiry(Z)V

    .line 961
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 962
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    .line 963
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 968
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 970
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : interface("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : smart discover("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/b;->c(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 984
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":*"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUsingInterface(Ljava/lang/String;)V

    .line 985
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 986
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStart()Z

    move-result v0

    if-nez v0, :cond_9

    .line 991
    iput-object v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 993
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 995
    iput-object v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 997
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 999
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 1001
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 1002
    iput v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1003
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Fail to start core"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 955
    :cond_8
    const-string v0, "chord_api"

    const-string v1, "SchordManager :  Oxygen Disconnected - Use UDP Discovery"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 956
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/ChordAgent;->setUdpDiscover(Z)V

    goto/16 :goto_0

    .line 1009
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1010
    const-string v0, "chord_api"

    const-string v1, "Call Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1011
    new-instance v0, Lcom/samsung/android/sdk/chord/b/a;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->start()V

    .line 1017
    :cond_a
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xbb9

    .line 848
    sget v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    if-eqz v0, :cond_0

    .line 849
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not allow multiple instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 852
    :cond_0
    if-nez p1, :cond_1

    .line 853
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid context!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 856
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/chord/Schord;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 857
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "you should initialize Schord before getting instance!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 860
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 861
    invoke-static {}, Lcom/samsung/android/sdk/chord/Schord;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    .line 862
    new-instance v0, Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 863
    new-instance v0, Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chord/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 864
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    .line 865
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 867
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 868
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 869
    const-string v0, "chord_api"

    const-string v1, "SchordManager : remove stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 874
    :cond_3
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    .line 876
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Hi Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    return-void
.end method

.method private declared-synchronized a(Landroid/os/Message;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1165
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sparse-switch v0, :sswitch_data_0

    .line 1372
    :cond_0
    :goto_0
    :sswitch_0
    monitor-exit p0

    return-void

    .line 1167
    :sswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    if-ne v0, v1, :cond_1

    .line 1171
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchordManager : Net Type :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " CUR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1175
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1177
    iget v0, p1, Landroid/os/Message;->arg2:I

    if-nez v0, :cond_2

    .line 1178
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;->onConnected(I)V

    goto :goto_0

    .line 1180
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;->onDisconnected(I)V

    goto :goto_0

    .line 1186
    :sswitch_2
    const-string v0, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SchordManager : Event.MGR_STARTED: handleMessages: state is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    const/4 v0, 0x3

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_3

    .line 1189
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 1191
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    goto/16 :goto_0

    .line 1193
    :cond_3
    const/4 v0, 0x5

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v3, :cond_4

    .line 1194
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1195
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    goto/16 :goto_0

    .line 1199
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v1, v0, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v5, v0, :cond_0

    .line 1205
    :cond_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 1206
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryIP()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1209
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v5, v0, :cond_10

    .line 1211
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    .line 1214
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 1215
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1219
    :goto_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1220
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v1, :cond_0

    .line 1221
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : ****onStarted("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStarted(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1229
    :sswitch_3
    const-string v0, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : Event.MGR_STOPPED: handleMessages : state is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1230
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v1, v0, :cond_8

    .line 1231
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1233
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_7

    .line 1234
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.ERROR_START_FAIL)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1238
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    goto/16 :goto_0

    .line 1241
    :cond_8
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_9

    .line 1242
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 1243
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 1244
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1246
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_0

    .line 1247
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.STOPPED_BY_USER)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1248
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3ea

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1251
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v4, :cond_0

    .line 1252
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    goto/16 :goto_0

    .line 1256
    :cond_9
    const/4 v0, 0x5

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_a

    .line 1262
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1263
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->close()V

    goto/16 :goto_0

    .line 1267
    :cond_a
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1268
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    if-eqz v0, :cond_c

    .line 1269
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 1270
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_3

    .line 1272
    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 1273
    const-string v0, "chord_api"

    const-string v1, "SchordManager : ****onStopped(StatusListener.NETWORK_DISCONNECTED)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1274
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    const/16 v1, 0x3eb

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    .line 1277
    :cond_c
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    if-eqz v0, :cond_0

    .line 1278
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1279
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    goto/16 :goto_0

    .line 1292
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 1293
    const/4 v1, 0x4

    iput v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 1294
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 1296
    if-eqz v0, :cond_0

    .line 1297
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_d

    .line 1298
    const-string v1, "chord_api"

    const-string v2, "SchordManager : ****onStopped(StatusListener.ERROR_START_FAIL)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1299
    const/16 v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1300
    :cond_d
    const/16 v1, 0xbbc

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    .line 1301
    const-string v1, "chord_api"

    const-string v2, "SchordManager : ****onStopped(StatusListener.ERROR_UNEXPECTED_STOP)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1302
    const/16 v1, 0x3e9

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;->onStopped(I)V

    goto/16 :goto_0

    .line 1326
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/d;

    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/a/d;->a()Ljava/lang/String;

    move-result-object v1

    .line 1329
    const-string v0, "Chord"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1330
    const-string v0, ""

    .line 1331
    const/16 v0, 0xc1d

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_f

    .line 1332
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/a;

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/a/a;->b:Ljava/lang/String;

    .line 1333
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 1334
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1341
    :cond_e
    :goto_4
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 1342
    if-eqz v0, :cond_0

    .line 1343
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1335
    :cond_f
    const/16 v0, 0xc1e

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_e

    .line 1336
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/chord/a/a;

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/a/a;->b:Ljava/lang/String;

    .line 1337
    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1350
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 1353
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    .line 1354
    if-eqz v0, :cond_0

    .line 1355
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/a;->a(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 1361
    :sswitch_7
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    if-eqz v0, :cond_0

    .line 1362
    const-string v0, "chord_api"

    const-string v1, "SchordManager : NETWORK_DHCP_IP_RENEW"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setConnectivityState(I)V

    .line 1364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_2

    .line 1165
    :sswitch_data_0
    .sparse-switch
        0xbb8 -> :sswitch_2
        0xbb9 -> :sswitch_3
        0xbba -> :sswitch_4
        0xbbb -> :sswitch_0
        0xbbc -> :sswitch_4
        0xc1c -> :sswitch_6
        0xc1d -> :sswitch_5
        0xc1e -> :sswitch_5
        0xc80 -> :sswitch_5
        0xce4 -> :sswitch_5
        0xce5 -> :sswitch_5
        0xce6 -> :sswitch_5
        0xce8 -> :sswitch_5
        0xce9 -> :sswitch_5
        0xcea -> :sswitch_5
        0xcf8 -> :sswitch_5
        0xcf9 -> :sswitch_5
        0xcfa -> :sswitch_5
        0xcfc -> :sswitch_5
        0xcfd -> :sswitch_5
        0xcfe -> :sswitch_5
        0xcff -> :sswitch_5
        0xd48 -> :sswitch_1
        0xd49 -> :sswitch_7
    .end sparse-switch
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/SchordManager;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/samsung/android/sdk/chord/SchordManager;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    return v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1144
    if-nez p1, :cond_0

    .line 1145
    const-string v0, "STATE_INIT"

    .line 1158
    :goto_0
    return-object v0

    .line 1146
    :cond_0
    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    .line 1147
    const-string v0, "STATE_REQ_START"

    goto :goto_0

    .line 1148
    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    .line 1149
    const-string v0, "STATE_STARTED"

    goto :goto_0

    .line 1150
    :cond_2
    const/4 v0, 0x3

    if-ne v0, p1, :cond_3

    .line 1151
    const-string v0, "STATE_REQ_STOP"

    goto :goto_0

    .line 1152
    :cond_3
    const/4 v0, 0x4

    if-ne v0, p1, :cond_4

    .line 1153
    const-string v0, "STATE_STOPPED"

    goto :goto_0

    .line 1154
    :cond_4
    const/4 v0, 0x5

    if-ne v0, p1, :cond_5

    .line 1155
    const-string v0, "STATE_REQ_RELEASE"

    goto :goto_0

    .line 1156
    :cond_5
    const/4 v0, 0x6

    if-ne v0, p1, :cond_6

    .line 1157
    const-string v0, "STATE_DISCONNECTED"

    goto :goto_0

    .line 1158
    :cond_6
    const-string v0, "STATE_UNKNOWN"

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/b;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    return-object v0
.end method

.method static synthetic d(Lcom/samsung/android/sdk/chord/SchordManager;)I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    return v0
.end method

.method static synthetic e(Lcom/samsung/android/sdk/chord/SchordManager;)Lcom/samsung/android/sdk/chord/ChordAgent;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    return-object v0
.end method

.method static synthetic f(Lcom/samsung/android/sdk/chord/SchordManager;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 342
    const-string v0, "chord_api"

    const-string v1, "SchordManager : close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    sput v3, Lcom/samsung/android/sdk/chord/SchordManager;->w:I

    .line 345
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->b:Landroid/content/Context;

    .line 346
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 347
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 349
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/SchordManager;->stop()V

    .line 351
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 353
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    .line 355
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 356
    iput v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 359
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v4, v0, :cond_2

    .line 369
    :goto_0
    return-void

    .line 363
    :cond_2
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    .line 364
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 366
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const-string v0, "chord_api"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "* Bye Samsung Chord "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    const-string v0, "chord_api"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableInterfaceTypes()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 382
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 383
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-nez v1, :cond_1

    .line 384
    const-string v1, "chord_api"

    const-string v2, "SchordManager : getAvailableInterfaceTypes : Invalid condition. Call this after initialize"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_0
    :goto_0
    return-object v0

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 391
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 394
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 397
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 680
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 698
    :goto_0
    monitor-exit p0

    return-object v0

    .line 686
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 688
    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/SchordChannel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/util/ConcurrentModificationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 694
    :catch_0
    move-exception v0

    .line 695
    :try_start_2
    const-string v2, "chord_api"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getJoinedChannel - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move-object v0, v1

    .line 698
    goto :goto_0

    .line 680
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getJoinedChannelList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chord/SchordChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 711
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 712
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    return-object v0
.end method

.method public isSmartDiscoveryEnabled()Z
    .locals 1

    .prologue
    .line 828
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    return v0
.end method

.method public declared-synchronized joinChannel(Ljava/lang/String;Lcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    .locals 1

    .prologue
    .line 606
    monitor-enter p0

    :try_start_0
    const-string v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 607
    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->a(Ljava/lang/String;ZLcom/samsung/android/sdk/chord/SchordChannel$StatusListener;)Lcom/samsung/android/sdk/chord/SchordChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 606
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized leaveChannel(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 621
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v2, v1, :cond_1

    const/4 v1, 0x4

    iget v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v1, v2, :cond_1

    .line 623
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : leaveChannel : ERROR_INVALID_STATE - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 629
    :cond_1
    if-eqz p1, :cond_2

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 631
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid channelName!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 635
    :cond_3
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager;->getJoinedChannel(Ljava/lang/String;)Lcom/samsung/android/sdk/chord/SchordChannel;

    move-result-object v2

    .line 637
    if-nez v2, :cond_4

    .line 639
    const-string v1, "chord_api"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SchordManager : leaveChannel : ERROR_INVALID_PARAM - Have not joined @"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    :cond_4
    move-object v0, v2

    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    move-object v1, v0

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    .line 648
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 650
    const-string v1, "Chord"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 656
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->leaveChannelEx(Ljava/lang/String;)I

    move-result v1

    .line 658
    if-eqz v1, :cond_0

    .line 663
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to leave "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public resetSmartDiscoveryPeriod()V
    .locals 2

    .prologue
    .line 838
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->resetSmartDiscoveryPeriod()V

    .line 841
    :cond_1
    return-void
.end method

.method public setLooper(Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 438
    if-nez p1, :cond_0

    .line 439
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setLooper : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :goto_0
    return-void

    .line 443
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/chord/SchordManager$1;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/chord/SchordManager$1;-><init>(Lcom/samsung/android/sdk/chord/SchordManager;Landroid/os/Looper;)V

    sput-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    goto :goto_0
.end method

.method public setNetworkListener(Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 470
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 471
    iput-object v4, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 472
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Handler. It must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 475
    :goto_0
    const/4 v0, 0x3

    if-ge v2, v0, :cond_2

    .line 476
    iget-object v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->n:[Ljava/lang/Integer;

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    .line 475
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 476
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 480
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    .line 482
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-eqz v0, :cond_4

    .line 483
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->x:Lcom/samsung/android/sdk/chord/b$a;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 491
    :cond_3
    :goto_2
    return-void

    .line 487
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_3

    .line 488
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    goto :goto_2
.end method

.method public setNodeKeepAliveTimeout(I)V
    .locals 3

    .prologue
    .line 754
    if-lez p1, :cond_2

    .line 755
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    .line 760
    :goto_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 761
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setLivenessTimeout(J)V

    .line 764
    :cond_1
    return-void

    .line 757
    :cond_2
    const-wide/32 v0, 0x84d0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->o:J

    goto :goto_0
.end method

.method public setSecureModeEnabled(Z)V
    .locals 0

    .prologue
    .line 794
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->r:Z

    .line 795
    return-void
.end method

.method public setSendMultiFilesLimitCount(I)V
    .locals 2

    .prologue
    .line 777
    iput p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->t:I

    .line 779
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 780
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSendFileLimit(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 781
    const-string v0, "chord_api"

    const-string v1, "SchordManager : fail to setSendFileLimit"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    :cond_1
    return-void
.end method

.method public setSmartDiscoveryEnabled(Z)V
    .locals 2

    .prologue
    .line 813
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->s:Z

    .line 814
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_1

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/chord/ChordAgent;->setSmartDiscovery(Z)V

    .line 817
    :cond_1
    return-void
.end method

.method public setTempDirectory(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    .line 417
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 418
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 419
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 420
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setTempDirectory : fail to create dir"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 425
    :cond_1
    const-string v0, "chord_api"

    const-string v1, "SchordManager : setTempDirectory : ERROR_INVALID_PARAM"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public start(ILcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/chord/InvalidInterfaceException;
        }
    .end annotation

    .prologue
    .line 513
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sdk/chord/SchordManager;->a(IZLcom/samsung/android/sdk/chord/SchordManager$StatusListener;)V

    .line 514
    return-void
.end method

.method public stop()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 524
    const-string v0, "chord_api"

    const-string v1, "SchordManager : stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    if-eqz v0, :cond_1

    .line 528
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    const-string v0, "chord_api"

    const-string v1, "Stop  Emulator_translator while running in AVD"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    if-eqz v0, :cond_0

    .line 532
    const-string v0, "chord_api"

    const-string v1, "UDPUnicastReceiver is closed "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->a()V

    .line 534
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/b/a;->interrupt()V

    .line 535
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->v:Lcom/samsung/android/sdk/chord/b/a;

    .line 539
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->m:Lcom/samsung/android/sdk/chord/SchordManager$NetworkListener;

    if-nez v0, :cond_1

    .line 540
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->g:Lcom/samsung/android/sdk/chord/b;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/b;->a(Lcom/samsung/android/sdk/chord/b$a;)V

    .line 544
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->h:I

    .line 545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->p:Z

    .line 547
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->j:Ljava/lang/String;

    .line 549
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 551
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chord/SchordChannel;

    .line 552
    check-cast v0, Lcom/samsung/android/sdk/chord/a;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chord/a;->a(I)V

    goto :goto_0

    .line 556
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 557
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 558
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->i:Ljava/lang/String;

    .line 560
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v4, v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->q:Z

    if-ne v0, v4, :cond_4

    .line 562
    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 563
    const-string v0, "chord_api"

    const-string v1, "SchordManager : stop : didn\'t receive started"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    sget-object v0, Lcom/samsung/android/sdk/chord/SchordManager;->c:Landroid/os/Handler;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 565
    iput v3, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 574
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v3, v0, :cond_7

    .line 575
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 576
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordStop()Z

    .line 585
    :cond_5
    :goto_1
    return-void

    .line 567
    :cond_6
    iput v5, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    .line 568
    const-string v0, "chord_api"

    const-string v1, "SchordManager : stop : STATE_REQ_STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 578
    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-eq v6, v0, :cond_8

    const/4 v0, 0x6

    iget v1, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    if-ne v0, v1, :cond_5

    .line 579
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chord/ChordAgent;->setHandler(Landroid/os/Handler;)V

    .line 580
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/SchordManager;->a:Lcom/samsung/android/sdk/chord/ChordAgent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chord/ChordAgent;->ChordRelease()Z

    .line 581
    iput-object v2, p0, Lcom/samsung/android/sdk/chord/SchordManager;->e:Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;

    .line 582
    iput v6, p0, Lcom/samsung/android/sdk/chord/SchordManager;->f:I

    goto :goto_1
.end method
