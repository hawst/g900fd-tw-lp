.class public interface abstract Lcom/samsung/android/sdk/chord/SchordManager$StatusListener;
.super Ljava/lang/Object;
.source "SchordManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chord/SchordManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StatusListener"
.end annotation


# static fields
.field public static final ERROR_START_FAIL:I = 0x3e8

.field public static final ERROR_UNEXPECTED_STOP:I = 0x3e9

.field public static final NETWORK_DISCONNECTED:I = 0x3eb

.field public static final STARTED_BY_RECONNECTION:I = 0x1

.field public static final STARTED_BY_USER:I = 0x0

.field public static final STOPPED_BY_USER:I = 0x3ea


# virtual methods
.method public abstract onStarted(Ljava/lang/String;I)V
.end method

.method public abstract onStopped(I)V
.end method
