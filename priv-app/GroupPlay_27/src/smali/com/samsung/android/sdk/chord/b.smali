.class public Lcom/samsung/android/sdk/chord/b;
.super Ljava/lang/Object;
.source "ConnectivityUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chord/b$a;
    }
.end annotation


# static fields
.field private static f:Z


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Lcom/samsung/android/sdk/chord/b$a;

.field private d:Z

.field private e:Z

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/chord/b;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    .line 74
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z

    .line 76
    iput-object v1, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    .line 78
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->d:Z

    .line 79
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->e:Z

    .line 432
    new-instance v0, Lcom/samsung/android/sdk/chord/b$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chord/b$1;-><init>(Lcom/samsung/android/sdk/chord/b;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/b;->g:Landroid/content/BroadcastReceiver;

    .line 87
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    .line 89
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/sdk/chord/b;->f:Z

    .line 90
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->d:Z

    .line 91
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->e:Z

    .line 92
    const-string v0, "ConnectivityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectivityUtils : E("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/samsung/android/sdk/chord/b;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") O("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/chord/b;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") S("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/chord/b;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/b;)Lcom/samsung/android/sdk/chord/b$a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    return-object v0
.end method

.method private declared-synchronized a(II)V
    .locals 1

    .prologue
    .line 524
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/chord/b$a;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 527
    :cond_0
    monitor-exit p0

    return-void

    .line 524
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/chord/b;II)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/chord/b;->a(II)V

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/sdk/chord/b;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->j()V

    return-void
.end method

.method private c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 262
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v0

    .line 266
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/b;->d:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 270
    :cond_2
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/b;->e:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 279
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 280
    if-nez v0, :cond_0

    .line 281
    const-string v0, "ConnectivityUtils"

    const-string v2, "Fail to get Wi-Fi info"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 290
    :goto_0
    return v0

    .line 285
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 287
    goto :goto_0

    :cond_1
    move v0, v1

    .line 290
    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 295
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 296
    if-nez v0, :cond_0

    .line 297
    const-string v0, "ConnectivityUtils"

    const-string v2, "Fail to get Wi-Fi Direct info"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 305
    :goto_0
    return v0

    .line 301
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 305
    goto :goto_0
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 309
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 312
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "isWifiApEnabled"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 319
    const/4 v1, 0x0

    :try_start_1
    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v2, v0, :cond_0

    .line 320
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isWifiApEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    move v0, v2

    .line 331
    :goto_0
    return v0

    .line 313
    :catch_0
    move-exception v0

    .line 314
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 315
    goto :goto_0

    .line 323
    :catch_1
    move-exception v0

    .line 324
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    move v0, v3

    .line 331
    goto :goto_0

    .line 325
    :catch_2
    move-exception v0

    .line 326
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 327
    :catch_3
    move-exception v0

    .line 328
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ConnectivityUtils.isWifiApEnabled : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private g()Z
    .locals 3

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 337
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "getWifiIBSSState"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 343
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils : It is not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 380
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 382
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getNetworkInfo"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 388
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 383
    :catch_0
    move-exception v0

    .line 384
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils : It is not supported2"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 385
    goto :goto_0
.end method

.method private i()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 395
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getNetworkInfo"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 402
    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 403
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature: connected"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 405
    const/4 v0, 0x1

    .line 417
    :goto_0
    return v0

    .line 396
    :catch_0
    move-exception v0

    .line 397
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils : It is not supported2!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 398
    goto :goto_0

    :cond_0
    move v0, v1

    .line 408
    goto :goto_0

    .line 409
    :catch_1
    move-exception v0

    .line 410
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : IllegalArgumentException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v0, v1

    .line 417
    goto :goto_0

    .line 411
    :catch_2
    move-exception v0

    .line 412
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : IllegalAccessException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 413
    :catch_3
    move-exception v0

    .line 414
    const-string v0, "ConnectivityUtils"

    const-string v2, "ConnectivityUtils.isSecFeature2 : InvocationTargetException"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private declared-synchronized j()V
    .locals 2

    .prologue
    .line 530
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "ConnectivityUtils"

    const-string v1, "notifyScreenOn if"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    invoke-interface {v0}, Lcom/samsung/android/sdk/chord/b$a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    :cond_0
    monitor-exit p0

    return-void

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "eth0"

    .line 145
    :goto_0
    return-object v0

    .line 136
    :cond_0
    if-nez p1, :cond_1

    .line 137
    const-string v0, "wlan0"

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x1

    if-ne v0, p1, :cond_2

    .line 139
    const-string v0, "p2p-wlan0-0"

    goto :goto_0

    .line 140
    :cond_2
    const/4 v0, 0x2

    if-ne v0, p1, :cond_3

    .line 141
    const-string v0, "wlan0"

    goto :goto_0

    .line 144
    :cond_3
    const-string v0, "ConnectivityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnectivityUtils.getInterfaceName : invalid interface type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized a(Lcom/samsung/android/sdk/chord/b$a;)V
    .locals 4

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->c:Lcom/samsung/android/sdk/chord/b$a;

    if-nez v0, :cond_2

    .line 100
    const-string v0, "ConnectivityUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setListener : null "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/chord/b;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/chord/b;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 107
    :cond_0
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    :try_start_3
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setListener :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 107
    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    :try_start_5
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chord/b;->b:Z

    throw v0

    .line 112
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 116
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 118
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 119
    const-string v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 120
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 122
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chord/b;->d:Z

    if-eqz v1, :cond_3

    .line 123
    const-string v1, "android.net.wifi.NETWORK_OXYGEN_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/chord/b;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chord/b;->b:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 233
    sget-boolean v0, Lcom/samsung/android/sdk/chord/b;->f:Z

    return v0
.end method

.method public b()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/b;->a:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 351
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getWifiIBSSState"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 358
    const/4 v4, 0x3

    const/4 v1, 0x0

    :try_start_1
    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v4, v1, :cond_1

    .line 359
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 361
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature: enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    .line 362
    const/4 v0, 0x1

    .line 374
    :goto_0
    return v0

    .line 352
    :catch_0
    move-exception v0

    .line 353
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils : It is not supported!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 354
    goto :goto_0

    :cond_0
    move v0, v2

    .line 364
    goto :goto_0

    .line 366
    :catch_1
    move-exception v0

    .line 367
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : IllegalArgumentException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    move v0, v2

    .line 374
    goto :goto_0

    .line 368
    :catch_2
    move-exception v0

    .line 369
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : IllegalAccessException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 370
    :catch_3
    move-exception v0

    .line 371
    const-string v0, "ConnectivityUtils"

    const-string v1, "ConnectivityUtils.isSecFeature : InvocationTargetException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public b(I)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/b;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    if-ltz p1, :cond_0

    if-le p1, v3, :cond_1

    .line 151
    :cond_0
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.isAvailableInterfaceType : invalid interface type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 165
    :cond_1
    :goto_0
    return v0

    .line 156
    :cond_2
    if-nez p1, :cond_3

    .line 157
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->c()Z

    move-result v0

    goto :goto_0

    .line 158
    :cond_3
    if-ne v0, p1, :cond_4

    .line 159
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->e()Z

    move-result v0

    goto :goto_0

    .line 160
    :cond_4
    if-ne v3, p1, :cond_5

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/sdk/chord/b;->f()Z

    move-result v0

    goto :goto_0

    .line 164
    :cond_5
    const-string v0, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.isAvailableInterfaceType : invalid interface type ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 165
    goto :goto_0
.end method

.method public c(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 170
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chord/b;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 174
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v2

    if-nez v2, :cond_0

    .line 176
    instance-of v2, v0, Ljava/net/Inet4Address;

    if-eqz v2, :cond_0

    .line 177
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 185
    :goto_0
    return-object v0

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const-string v1, "ConnectivityUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConnectivityUtils.getIp4Address : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
