.class Lcom/samsung/android/sdk/chord/ChordAgent;
.super Ljava/lang/Object;
.source "ChordAgent.java"


# instance fields
.field private a:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "chord-v1.5"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method native ChordInit(Ljava/lang/String;)I
.end method

.method synchronized native declared-synchronized ChordRelease()Z
.end method

.method native ChordStart()Z
.end method

.method synchronized native declared-synchronized ChordStop()Z
.end method

.method a(I)I
    .locals 1

    .prologue
    .line 694
    sparse-switch p1, :sswitch_data_0

    .line 709
    const/16 v0, 0x7d0

    :goto_0
    return v0

    .line 696
    :sswitch_0
    const/16 v0, 0x7d1

    goto :goto_0

    .line 698
    :sswitch_1
    const/16 v0, 0x7d2

    goto :goto_0

    .line 701
    :sswitch_2
    const/16 v0, 0x7d4

    goto :goto_0

    .line 703
    :sswitch_3
    const/16 v0, 0x7d3

    goto :goto_0

    .line 705
    :sswitch_4
    const/16 v0, 0x7d5

    goto :goto_0

    .line 707
    :sswitch_5
    const/16 v0, 0x7d6

    goto :goto_0

    .line 694
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x138a -> :sswitch_2
        0x138b -> :sswitch_3
        0x138e -> :sswitch_4
        0x138f -> :sswitch_2
        0x1393 -> :sswitch_0
        0x1394 -> :sswitch_1
    .end sparse-switch
.end method

.method native acceptFile(Ljava/lang/String;Ljava/lang/String;JIJ)I
.end method

.method b(I)I
    .locals 3

    .prologue
    .line 721
    packed-switch p1, :pswitch_data_0

    .line 732
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** convtNodeLeaveReason : invalid reason("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 723
    :pswitch_0
    const/16 v0, 0x834

    goto :goto_0

    .line 725
    :pswitch_1
    const/16 v0, 0x835

    goto :goto_0

    .line 727
    :pswitch_2
    const/16 v0, 0x836

    goto :goto_0

    .line 721
    :pswitch_data_0
    .packed-switch 0x1b59
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method native cancelFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method native getChordAgentState()I
.end method

.method native getDiscoveryIP()Ljava/lang/String;
.end method

.method native getDiscoveryPortNumber()I
.end method

.method native getNodeIpAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method native getUsingInterfaceName()Ljava/lang/String;
.end method

.method native getVersion()Ljava/lang/String;
.end method

.method native joinChannel(Ljava/lang/String;)I
.end method

.method native leaveChannelEx(Ljava/lang/String;)I
.end method

.method native multiacceptFiles(Ljava/lang/String;Ljava/lang/String;JIJ)I
.end method

.method native multicancelFiles(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method native multirejectFiles(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method native multishareFiles(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method onChordNoPeersCB()V
    .locals 2

    .prologue
    .line 335
    const-string v0, "chord_jni"

    const-string v1, "**** onChordNoPeersCB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    return-void
.end method

.method onChordPeersCB()V
    .locals 2

    .prologue
    .line 328
    const-string v0, "chord_jni"

    const-string v1, "**** onChordPeersCB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return-void
.end method

.method onChunkReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 19

    .prologue
    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 423
    :goto_0
    return-void

    .line 412
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xce5

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 422
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onChunkSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 19

    .prologue
    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 482
    :goto_0
    return-void

    .line 470
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xce8

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCoreListeningCB(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 279
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : Name["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreListeningCB : IP["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryIP()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getDiscoveryPortNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 288
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onCoreStoppedCB()V
    .locals 3

    .prologue
    .line 342
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onCoreStoppedCB : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chord/ChordAgent;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbb9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 345
    :cond_0
    return-void
.end method

.method onDataReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc80

    new-instance v2, Lcom/samsung/android/sdk/chord/a/b;

    invoke-direct {v2, p2, p1, p3, p4}, Lcom/samsung/android/sdk/chord/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onDiscoveryFailedCB(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 318
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onDiscoveryFailedCB ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    return-void
.end method

.method onFileFailedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 19

    .prologue
    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 462
    :goto_0
    return-void

    .line 451
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xcea

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v7, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v15

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 461
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileNotifiedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 19

    .prologue
    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 404
    :goto_0
    return-void

    .line 394
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xce4

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v9, p7

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 403
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 19

    .prologue
    .line 429
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 442
    :goto_0
    return-void

    .line 431
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xce6

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v9, p7

    move-object/from16 v16, p9

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 441
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onFileSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 19

    .prologue
    .line 488
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 500
    :goto_0
    return-void

    .line 490
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0xce9

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-wide/from16 v9, p7

    invoke-direct/range {v2 .. v16}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;)V

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 499
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onJoinedEventCB(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 356
    :goto_0
    return-void

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc1d

    new-instance v2, Lcom/samsung/android/sdk/chord/a/a;

    invoke-direct {v2, p2, p1}, Lcom/samsung/android/sdk/chord/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 355
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onLeaveEventCB(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xc1e

    new-instance v2, Lcom/samsung/android/sdk/chord/a/a;

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/chord/ChordAgent;->b(I)I

    move-result v3

    invoke-direct {v2, p2, p1, v3}, Lcom/samsung/android/sdk/chord/a/a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiChunkReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 21

    .prologue
    .line 527
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 541
    :goto_0
    return-void

    .line 529
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcf9

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move-object/from16 v17, p4

    move/from16 v18, p6

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 540
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiChunkSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 21

    .prologue
    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 608
    :goto_0
    return-void

    .line 595
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcfc

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-wide/from16 v9, p7

    move-wide/from16 v11, p9

    move-wide/from16 v13, p11

    move-object/from16 v17, p4

    move/from16 v18, p6

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 607
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileFailedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 21

    .prologue
    .line 571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 585
    :goto_0
    return-void

    .line 573
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcfe

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v15

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v17, p4

    move/from16 v18, p5

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 584
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileFinishedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 21

    .prologue
    .line 637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 651
    :goto_0
    return-void

    .line 639
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcff

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chord/ChordAgent;->a(I)I

    move-result v15

    const/16 v16, 0x0

    const/16 v18, -0x1

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v17, p3

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 650
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileNotifiedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V
    .locals 21

    .prologue
    .line 507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 520
    :goto_0
    return-void

    .line 509
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcf8

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-wide/from16 v9, p7

    move-object/from16 v17, p4

    move/from16 v18, p6

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 519
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileReceivedCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)V
    .locals 21

    .prologue
    .line 548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 562
    :goto_0
    return-void

    .line 550
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcfa

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-wide/from16 v9, p7

    move-object/from16 v16, p9

    move-object/from16 v17, p4

    move/from16 v18, p6

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 561
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method onMultiFileSentCB(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 21

    .prologue
    .line 616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-nez v2, :cond_0

    .line 629
    :goto_0
    return-void

    .line 618
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0xcfd

    new-instance v2, Lcom/samsung/android/sdk/chord/a/c;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, -0x1

    const/16 v16, 0x0

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    move-object/from16 v7, p5

    move-wide/from16 v9, p7

    move-object/from16 v17, p4

    move/from16 v18, p6

    invoke-direct/range {v2 .. v18}, Lcom/samsung/android/sdk/chord/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 628
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method native onNodeJoinedSender(Ljava/lang/String;)V
.end method

.method onNotifyChannelErrorCB(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 654
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onNotifyChannelErrorCB : channel("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") reason("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 669
    const/16 v1, 0xc1c

    iput v1, v0, Landroid/os/Message;->what:I

    .line 670
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 672
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 673
    iget-object v1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 675
    :cond_0
    return-void
.end method

.method onNotifyServiceErrorCB(I)V
    .locals 3

    .prologue
    .line 292
    const-string v0, "chord_jni"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "**** onNotifyServiceErrorCB ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 309
    const/16 v0, 0x3e9

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ea

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3eb

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3ec

    if-ne p1, v0, :cond_1

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    const/16 v1, 0xbbc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 315
    :cond_1
    return-void
.end method

.method native rejectFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method native resetSmartDiscoveryPeriod()V
.end method

.method native sendData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[[B)I
.end method

.method native setConnectivityState(I)V
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/sdk/chord/ChordAgent;->a:Landroid/os/Handler;

    .line 39
    return-void
.end method

.method native setLivenessTimeout(J)V
.end method

.method native setMultiHopDiscovery(Z)V
.end method

.method native setNodeExpiry(Z)V
.end method

.method native setSecureMode(Z)Z
.end method

.method native setSendFileLimit(I)Z
.end method

.method native setSmartDiscovery(Z)V
.end method

.method native setUdpDiscover(Z)V
.end method

.method native setUsingInterface(Ljava/lang/String;)V
.end method

.method native shareFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
