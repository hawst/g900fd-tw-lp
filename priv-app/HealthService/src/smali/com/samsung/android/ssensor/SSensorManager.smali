.class public Lcom/samsung/android/ssensor/SSensorManager;
.super Ljava/lang/Object;
.source "SSensorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/ssensor/SSensorManager$MyListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SSensorManager"

.field public static final TYPE_ECG:I

.field private static mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

.field public static manager:Lcom/samsung/android/ssensor/SSensorManager;


# instance fields
.field private mCoverlistener:Lcom/samsung/android/healthcover/HealthCoverListener;

.field private mlistnner:Lcom/samsung/android/ssensor/SSensorListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    .line 35
    new-instance v0, Lcom/samsung/android/ssensor/SSensorManager$MyListener;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/ssensor/SSensorManager$MyListener;-><init>(Lcom/samsung/android/ssensor/SSensorManager;Lcom/samsung/android/ssensor/SSensorManager$MyListener;)V

    iput-object v0, p0, Lcom/samsung/android/ssensor/SSensorManager;->mCoverlistener:Lcom/samsung/android/healthcover/HealthCoverListener;

    .line 36
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/ssensor/SSensorManager;)Lcom/samsung/android/ssensor/SSensorListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    return-object v0
.end method

.method public static getSSensorManager()Lcom/samsung/android/ssensor/SSensorManager;
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/healthcover/HealthCoverManager;->getHealthCoverManager()Lcom/samsung/android/healthcover/HealthCoverManager;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/ssensor/SSensorManager;->mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

    .line 45
    new-instance v0, Lcom/samsung/android/ssensor/SSensorManager;

    invoke-direct {v0}, Lcom/samsung/android/ssensor/SSensorManager;-><init>()V

    sput-object v0, Lcom/samsung/android/ssensor/SSensorManager;->manager:Lcom/samsung/android/ssensor/SSensorManager;

    .line 46
    const-string v0, "SSensorManager"

    const-string v1, "created SSensorManager"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    sget-object v0, Lcom/samsung/android/ssensor/SSensorManager;->manager:Lcom/samsung/android/ssensor/SSensorManager;

    return-object v0
.end method


# virtual methods
.method public getSSensorState()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public registerListener(Lcom/samsung/android/ssensor/SSensorListener;I)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/ssensor/SSensorListener;
    .param p2, "type"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "SSensorManager"

    const-string v1, "registerListener failed. already registered "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    .line 55
    sget-object v0, Lcom/samsung/android/ssensor/SSensorManager;->mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

    iget-object v1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mCoverlistener:Lcom/samsung/android/healthcover/HealthCoverListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/healthcover/HealthCoverManager;->registerListener(Lcom/samsung/android/healthcover/HealthCoverListener;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public registerListener(Lcom/samsung/android/ssensor/SSensorListener;Landroid/os/Handler;I)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/ssensor/SSensorListener;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "type"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "SSensorManager"

    const-string v1, "registerListener failed. already registered "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    .line 64
    sget-object v0, Lcom/samsung/android/ssensor/SSensorManager;->mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

    iget-object v1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mCoverlistener:Lcom/samsung/android/healthcover/HealthCoverListener;

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/healthcover/HealthCoverManager;->registerListener(Lcom/samsung/android/healthcover/HealthCoverListener;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public unregisterListener(Lcom/samsung/android/ssensor/SSensorListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/ssensor/SSensorListener;

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/ssensor/SSensorManager;->mlistnner:Lcom/samsung/android/ssensor/SSensorListener;

    .line 69
    sget-object v0, Lcom/samsung/android/ssensor/SSensorManager;->mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

    iget-object v1, p0, Lcom/samsung/android/ssensor/SSensorManager;->mCoverlistener:Lcom/samsung/android/healthcover/HealthCoverListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/healthcover/HealthCoverManager;->unregisterListener(Lcom/samsung/android/healthcover/HealthCoverListener;)V

    .line 70
    return-void
.end method

.method public writeData([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "type"    # I

    .prologue
    .line 74
    sget-object v0, Lcom/samsung/android/ssensor/SSensorManager;->mHealthcoverManager:Lcom/samsung/android/healthcover/HealthCoverManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/healthcover/HealthCoverManager;->writeData([B)V

    .line 75
    return-void
.end method
