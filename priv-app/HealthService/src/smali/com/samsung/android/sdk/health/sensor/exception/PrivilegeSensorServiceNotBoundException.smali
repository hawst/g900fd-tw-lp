.class public Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
.super Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorException;
.source "PrivilegeSensorServiceNotBoundException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorException;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorException;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method
