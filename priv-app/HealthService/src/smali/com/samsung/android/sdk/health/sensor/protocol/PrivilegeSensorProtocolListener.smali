.class public interface abstract Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;
.super Ljava/lang/Object;
.source "PrivilegeSensorProtocolListener.java"


# virtual methods
.method public abstract onDataReceived(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end method

.method public abstract onDataReceived([Landroid/os/Bundle;[Landroid/os/Bundle;)V
.end method

.method public abstract onDataStarted(I)V
.end method

.method public abstract onDataStopped(II)V
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;)V
.end method

.method public abstract onStateChanged(I)V
.end method

.method public abstract sendRawData([B)I
.end method
