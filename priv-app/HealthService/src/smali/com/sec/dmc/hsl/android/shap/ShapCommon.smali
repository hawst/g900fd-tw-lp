.class public Lcom/sec/dmc/hsl/android/shap/ShapCommon;
.super Ljava/lang/Object;
.source "ShapCommon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    }
.end annotation


# static fields
.field public static final ESCAPE_CHAR:B = 0x7et

.field public static final SHAP_CTLMSG_COMMON_CHANGE_STATUS:B = 0x21t

.field public static final SHAP_DEV_BLOOD_PRESSURE_MONITOR:S = 0x2s

.field public static final SHAP_DEV_BODY_COMPOSITION_ANALYZER:S = 0x6s

.field public static final SHAP_DEV_HEART_RATE_MONITOR:S = 0x3s

.field public static final SHAP_EXTMSG_COMMON_EXTRA_DATA:B = 0x35t

.field public static final SHAP_EXTMSG_COMMON_PRODUCT_INFO:B = 0x30t

.field public static final SHAP_EXTMSG_COMMON_RAWDATA_FORMAT:B = 0x31t

.field public static final SHAP_EXTMSG_COMMON_RAW_DATA:B = 0x32t

.field public static final SHAP_MSGFLAG_CONFIRMED:B = -0x80t

.field public static final SHAP_MSGTYPE_EXTENDED:B = 0x10t

.field public static final SHAP_MSGTYPE_STANDARD:B = 0x0t

.field public static final SHAP_MSG_COMMON_DATA_REQUEST:B = 0x0t

.field public static final SHAP_MSG_COMMON_EXTRA_LIST:B = 0xct

.field public static final SHAP_MSG_COMMON_PRODUCT_INFO:B = 0x1t

.field public static final SHAP_MSG_COMMON_RAWDATA_REQUEST:B = 0x7t

.field public static final SHAP_MSG_COMMON_RESPOND:B = 0xat

.field public static final SHAP_MSG_COMMON_SETTINGS:B = 0x3t

.field public static final SHAP_REQTYPE_EXTRAMSG:B = -0x80t

.field public static final SHAP_REQTYPE_SENDWITHRAW:B = 0x40t

.field public static final SHAP_WAVE_TYPE_BEI:S = 0x16s

.field public static final SHAP_WAVE_TYPE_ECG:S = 0x0s

.field public static final SHAP_WAVE_TYPE_GENERIC:S = 0xffs

.field public static final SHAP_WAVE_TYPE_PPG:S = 0x12s

.field public static final START_END_CHAR:B = 0x7ft


# instance fields
.field protected mReceivedBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    .line 45
    return-void
.end method

.method protected static fill16bitData([BII)V
    .locals 2
    .param p0, "arrMsg"    # [B
    .param p1, "nPos"    # I
    .param p2, "nData"    # I

    .prologue
    .line 303
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 304
    and-int/lit16 v0, p2, 0xff

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 305
    return-void
.end method

.method protected static fill32bitData([BIJ)V
    .locals 5
    .param p0, "arrMsg"    # [B
    .param p1, "nPos"    # I
    .param p2, "lData"    # J

    .prologue
    const-wide/16 v3, 0xff

    .line 309
    add-int/lit8 v0, p1, 0x3

    const/16 v1, 0x18

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 310
    add-int/lit8 v0, p1, 0x2

    const/16 v1, 0x10

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 311
    add-int/lit8 v0, p1, 0x1

    const/16 v1, 0x8

    shr-long v1, p2, v1

    and-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 312
    and-long v0, p2, v3

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 313
    return-void
.end method

.method private findStartEndCharPos(I)I
    .locals 5
    .param p1, "nIdx"    # I

    .prologue
    .line 121
    const/4 v2, -0x1

    .line 122
    .local v2, "nRtn":I
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 123
    .local v1, "nLength":I
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 125
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    const/16 v4, 0x7f

    if-ne v3, v4, :cond_1

    .line 127
    move v2, v0

    .line 132
    :cond_0
    return v2

    .line 123
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getNumberOfEscape(II)I
    .locals 4
    .param p1, "nStartIdx"    # I
    .param p2, "nEndIdx"    # I

    .prologue
    .line 137
    const/4 v1, 0x0

    .line 139
    .local v1, "nRtn":I
    move v0, p1

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_1

    .line 141
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Byte;

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v2

    const/16 v3, 0x7e

    if-ne v2, v3, :cond_0

    .line 142
    add-int/lit8 v1, v1, 0x1

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_1
    return v1
.end method

.method private static getNumberOfSpecialCharsInData([B)I
    .locals 5
    .param p0, "arrData"    # [B

    .prologue
    .line 169
    const/4 v2, 0x0

    .line 170
    .local v2, "nRtn":I
    array-length v1, p0

    .line 171
    .local v1, "nLoop":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 173
    aget-byte v3, p0, v0

    const/16 v4, 0x7f

    if-eq v3, v4, :cond_0

    aget-byte v3, p0, v0

    const/16 v4, 0x7e

    if-ne v3, v4, :cond_1

    .line 174
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 171
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    :cond_2
    return v2
.end method

.method public static getStringFromByteArray([BII)Ljava/lang/String;
    .locals 3
    .param p0, "arrByteArray"    # [B
    .param p1, "nStartIdx"    # I
    .param p2, "nLength"    # I

    .prologue
    .line 317
    const/4 v1, 0x0

    .line 318
    .local v1, "strRtn":Ljava/lang/String;
    add-int v2, p1, p2

    add-int/lit8 v2, v2, -0x1

    aget-byte v2, p0, v2

    if-nez v2, :cond_0

    .line 320
    add-int/lit8 p2, p2, -0x1

    .line 322
    :cond_0
    new-array v0, p2, [B

    .line 325
    .local v0, "arrSub":[B
    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 326
    new-instance v1, Ljava/lang/String;

    .end local v1    # "strRtn":Ljava/lang/String;
    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    .line 327
    .restart local v1    # "strRtn":Ljava/lang/String;
    return-object v1
.end method

.method public static makeDataPacket([B)[B
    .locals 11
    .param p0, "arrData"    # [B

    .prologue
    const/16 v10, 0x7e

    const/16 v9, 0x7f

    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "arrPacket":[B
    invoke-static {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getNumberOfSpecialCharsInData([B)I

    move-result v6

    .line 182
    .local v6, "nNumOfSpecialChars":I
    array-length v8, p0

    add-int/2addr v8, v6

    add-int/lit8 v7, v8, 0x2

    .line 183
    .local v7, "nPacketLength":I
    new-array v0, v7, [B

    .line 185
    const/4 v8, 0x0

    aput-byte v9, v0, v8

    .line 187
    array-length v5, p0

    .line 188
    .local v5, "nLoop":I
    const/4 v3, 0x1

    .line 190
    .local v3, "nIdxPacket":I
    const/4 v2, 0x0

    .local v2, "i":I
    move v4, v3

    .end local v3    # "nIdxPacket":I
    .local v4, "nIdxPacket":I
    :goto_0
    if-ge v2, v5, :cond_1

    .line 192
    aget-byte v1, p0, v2

    .line 193
    .local v1, "cByte":B
    if-eq v1, v9, :cond_0

    if-ne v1, v10, :cond_2

    .line 195
    :cond_0
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nIdxPacket":I
    .restart local v3    # "nIdxPacket":I
    aput-byte v10, v0, v4

    .line 196
    xor-int/lit8 v8, v1, 0x7e

    int-to-byte v1, v8

    .line 199
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nIdxPacket":I
    .restart local v4    # "nIdxPacket":I
    aput-byte v1, v0, v3

    .line 190
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 202
    .end local v1    # "cByte":B
    :cond_1
    add-int/lit8 v8, v7, -0x1

    aput-byte v9, v0, v8

    .line 204
    return-object v0

    .restart local v1    # "cByte":B
    :cond_2
    move v3, v4

    .end local v4    # "nIdxPacket":I
    .restart local v3    # "nIdxPacket":I
    goto :goto_1
.end method

.method protected static makeFloat(BBBB)F
    .locals 3
    .param p0, "cByte00"    # B
    .param p1, "cByte01"    # B
    .param p2, "cByte02"    # B
    .param p3, "cByte03"    # B

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "fRtn":F
    and-int/lit16 v1, p0, 0xff

    and-int/lit16 v2, p1, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    and-int/lit16 v2, p2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    and-int/lit16 v2, p3, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    .line 243
    return v0
.end method

.method protected static makeInt16(BB)S
    .locals 2
    .param p0, "cByte00"    # B
    .param p1, "cByte01"    # B

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 275
    .local v0, "nRtn":S
    and-int/lit16 v1, p1, 0xff

    int-to-short v0, v1

    .line 276
    shl-int/lit8 v1, v0, 0x8

    int-to-short v0, v1

    .line 277
    and-int/lit16 v1, p0, 0xff

    or-int/2addr v1, v0

    int-to-short v0, v1

    .line 279
    return v0
.end method

.method protected static makeInt32(BBBB)I
    .locals 2
    .param p0, "cByte00"    # B
    .param p1, "cByte01"    # B
    .param p2, "cByte02"    # B
    .param p3, "cByte03"    # B

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 286
    .local v0, "nRtn":I
    and-int/lit16 v0, p3, 0xff

    .line 287
    shl-int/lit8 v0, v0, 0x8

    .line 289
    and-int/lit16 v1, p2, 0xff

    or-int/2addr v0, v1

    .line 290
    shl-int/lit8 v0, v0, 0x8

    .line 292
    and-int/lit16 v1, p1, 0xff

    or-int/2addr v0, v1

    .line 293
    shl-int/lit8 v0, v0, 0x8

    .line 295
    and-int/lit16 v1, p0, 0xff

    or-int/2addr v0, v1

    .line 298
    return v0
.end method

.method protected static makeUint16(BB)I
    .locals 2
    .param p0, "cByte00"    # B
    .param p1, "cByte01"    # B

    .prologue
    .line 248
    const/4 v0, 0x0

    .line 249
    .local v0, "nRtn":I
    and-int/lit16 v0, p1, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 251
    and-int/lit16 v1, p0, 0xff

    or-int/2addr v0, v1

    .line 253
    const v1, 0xffff

    and-int/2addr v1, v0

    return v1
.end method

.method protected static makeUint32(BBBB)J
    .locals 5
    .param p0, "cByte00"    # B
    .param p1, "cByte01"    # B
    .param p2, "cByte02"    # B
    .param p3, "cByte03"    # B

    .prologue
    const/16 v4, 0x8

    .line 258
    const-wide/16 v0, 0x0

    .line 260
    .local v0, "nRtn":J
    and-int/lit16 v2, p3, 0xff

    int-to-long v0, v2

    shl-long/2addr v0, v4

    .line 262
    and-int/lit16 v2, p2, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    shl-long/2addr v0, v4

    .line 264
    and-int/lit16 v2, p1, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    shl-long/2addr v0, v4

    .line 266
    and-int/lit16 v2, p0, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 269
    const-wide/16 v2, -0x1

    and-long/2addr v2, v0

    return-wide v2
.end method

.method public static parseCommonMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    .locals 2
    .param p0, "arrMessage"    # [B

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 212
    .local v0, "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    const/4 v1, 0x2

    aget-byte v1, p0, v1

    sparse-switch v1, :sswitch_data_0

    .line 236
    :goto_0
    return-object v0

    .line 215
    :sswitch_0
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;-><init>([B)V

    .line 216
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 218
    :sswitch_1
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;-><init>([B)V

    .line 219
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 221
    :sswitch_2
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;-><init>([B)V

    .line 222
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 224
    :sswitch_3
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;-><init>([B)V

    .line 225
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 227
    :sswitch_4
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;-><init>([B)V

    .line 228
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 230
    :sswitch_5
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;-><init>([B)V

    .line 231
    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 233
    :sswitch_6
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .end local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;-><init>([B)V

    .restart local v0    # "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 212
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xa -> :sswitch_1
        0xc -> :sswitch_4
        0x30 -> :sswitch_2
        0x31 -> :sswitch_6
        0x32 -> :sswitch_3
        0x35 -> :sswitch_5
    .end sparse-switch
.end method

.method private reconstructionMessage(II)[B
    .locals 6
    .param p1, "nStartIdx"    # I
    .param p2, "nEndIdx"    # I

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getNumberOfEscape(II)I

    move-result v4

    .line 150
    .local v4, "nNumOfEscape":I
    sub-int v5, p2, p1

    sub-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x1

    new-array v0, v5, [B

    .line 151
    .local v0, "arrRtn":[B
    const/4 v3, 0x0

    .line 152
    .local v3, "nIdx":I
    move v2, p1

    .local v2, "i":I
    :goto_0
    if-gt v2, p2, :cond_1

    .line 154
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    .line 155
    .local v1, "cByte":B
    const/16 v5, 0x7e

    if-ne v1, v5, :cond_0

    .line 157
    add-int/lit8 v2, v2, 0x1

    .line 158
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    xor-int/lit8 v5, v5, 0x7e

    int-to-byte v1, v5

    .line 161
    :cond_0
    aput-byte v1, v0, v3

    .line 162
    add-int/lit8 v3, v3, 0x1

    .line 152
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 164
    .end local v1    # "cByte":B
    :cond_1
    return-object v0
.end method

.method private trimLeftInReceivedBuffer()Z
    .locals 8

    .prologue
    const/16 v7, 0x7f

    const/4 v6, 0x0

    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, "bRtn":Z
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 105
    .local v3, "nLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    add-int/lit8 v4, v3, -0x1

    if-ge v2, v4, :cond_0

    .line 107
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    .line 108
    .local v1, "cByte":B
    if-ne v1, v7, :cond_1

    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    if-eq v4, v7, :cond_1

    .line 110
    const/4 v0, 0x1

    .line 116
    .end local v1    # "cByte":B
    :cond_0
    return v0

    .line 114
    .restart local v1    # "cByte":B
    :cond_1
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addDataToBuffer([B)I
    .locals 1
    .param p1, "arrData"    # [B

    .prologue
    .line 54
    array-length v0, p1

    invoke-virtual {p0, p1, v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->addDataToBuffer([BI)I

    move-result v0

    return v0
.end method

.method public addDataToBuffer([BI)I
    .locals 4
    .param p1, "arrData"    # [B
    .param p2, "nLength"    # I

    .prologue
    .line 59
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    monitor-enter v2

    .line 61
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 62
    :try_start_0
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    aget-byte v3, p1, v0

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    return v1

    .line 63
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getMessage()[B
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 75
    const/4 v0, 0x0

    .line 79
    .local v0, "arrRtn":[B
    :cond_0
    invoke-direct {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->trimLeftInReceivedBuffer()Z

    .line 80
    invoke-direct {p0, v5}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->findStartEndCharPos(I)I

    move-result v1

    .line 81
    .local v1, "nEndPos":I
    if-le v1, v6, :cond_2

    .line 83
    add-int/lit8 v2, v1, -0x1

    invoke-direct {p0, v5, v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->reconstructionMessage(II)[B

    move-result-object v0

    .line 84
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 96
    :cond_1
    :goto_0
    return-object v0

    .line 87
    :cond_2
    if-ltz v1, :cond_1

    .line 92
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 94
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gt v2, v6, :cond_0

    goto :goto_0
.end method

.method public getReceivedBufferSize()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public initBuffer()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->mReceivedBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 50
    return-void
.end method
