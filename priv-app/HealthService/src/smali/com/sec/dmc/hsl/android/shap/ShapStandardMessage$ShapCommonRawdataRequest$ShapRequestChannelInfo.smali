.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;
.super Ljava/lang/Object;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapRequestChannelInfo"
.end annotation


# instance fields
.field public mChannelID:B

.field public mReserved:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    .line 163
    return-void

    .line 158
    :array_0
    .array-data 1
        -0x1t
        -0x1t
    .end array-data
.end method


# virtual methods
.method public setChannelInfo(BBB)V
    .locals 2
    .param p1, "chnId"    # B
    .param p2, "reserved0"    # B
    .param p3, "reserved1"    # B

    .prologue
    .line 172
    iput-byte p1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mChannelID:B

    .line 173
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    const/4 v1, 0x0

    aput-byte p2, v0, v1

    .line 174
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    const/4 v1, 0x1

    aput-byte p3, v0, v1

    .line 175
    return-void
.end method

.method public setChannelInfo([B)V
    .locals 4
    .param p1, "arrData"    # [B

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166
    aget-byte v0, p1, v2

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mChannelID:B

    .line 167
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    aget-byte v1, p1, v3

    aput-byte v1, v0, v2

    .line 168
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    const/4 v1, 0x2

    aget-byte v1, p1, v1

    aput-byte v1, v0, v3

    .line 169
    return-void
.end method
