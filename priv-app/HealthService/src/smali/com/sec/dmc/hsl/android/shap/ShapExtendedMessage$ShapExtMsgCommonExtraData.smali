.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgCommonExtraData"
.end annotation


# instance fields
.field public mDeviceType:I

.field public mExtraID:S

.field public mExtraMessageBody:[B


# direct methods
.method public constructor <init>([B)V
    .locals 9
    .param p1, "arrData"    # [B

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 77
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    .line 81
    if-eqz p1, :cond_0

    array-length v3, p1

    int-to-long v3, v3

    iget-wide v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mMessageLength:J

    const-wide/16 v7, 0x8

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-ltz v3, :cond_0

    .line 83
    const/16 v0, 0x8

    .line 84
    .local v0, "nIdx":I
    iget-wide v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mMessageLength:J

    const-wide/16 v5, 0x3

    sub-long/2addr v3, v5

    long-to-int v2, v3

    .line 85
    .local v2, "nMsgBodySize":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v3, p1, v0

    int-to-short v3, v3

    iput-short v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraID:S

    .line 86
    aget-byte v3, p1, v1

    const/16 v4, 0xa

    aget-byte v4, p1, v4

    invoke-static {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v3

    iput v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mDeviceType:I

    add-int/lit8 v0, v1, 0x2

    .line 87
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    new-array v3, v2, [B

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    .line 88
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    const/4 v4, 0x0

    invoke-static {p1, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    .end local v0    # "nIdx":I
    .end local v2    # "nMsgBodySize":I
    :cond_0
    return-void
.end method


# virtual methods
.method public parseExtraMessage()Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    .locals 2

    .prologue
    .line 94
    const/4 v0, 0x0

    .line 96
    .local v0, "shapHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    iget v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mDeviceType:I

    packed-switch v1, :pswitch_data_0

    .line 105
    :goto_0
    :pswitch_0
    return-object v0

    .line 99
    :pswitch_1
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    invoke-static {v1}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    .line 100
    goto :goto_0

    .line 102
    :pswitch_2
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    invoke-static {v1}, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
