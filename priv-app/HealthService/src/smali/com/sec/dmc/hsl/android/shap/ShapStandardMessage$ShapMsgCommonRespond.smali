.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgCommonRespond"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0xe


# instance fields
.field public mReserved:[B

.field public mRespond:S


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "arrData"    # [B

    .prologue
    const/16 v3, 0xe

    .line 186
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 184
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mReserved:[B

    .line 188
    if-eqz p1, :cond_0

    array-length v1, p1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 190
    const/4 v0, 0x4

    .line 191
    .local v0, "nIdx":I
    aget-byte v1, p1, v0

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    invoke-static {v1, v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mRespond:S

    add-int/lit8 v0, v0, 0x2

    .line 192
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mReserved:[B

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v0, 0xe

    .line 194
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method
