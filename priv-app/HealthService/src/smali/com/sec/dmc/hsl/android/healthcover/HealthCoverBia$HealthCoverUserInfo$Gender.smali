.class public final enum Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
.super Ljava/lang/Enum;
.source "HealthCoverBia.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Gender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

.field public static final enum FEMALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

.field public static final enum MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 154
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v2}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->FEMALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->FEMALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    const-class v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    return-object v0
.end method

.method public static values()[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    invoke-virtual {v0}, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    return-object v0
.end method
