.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgCommonExtraList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;
    }
.end annotation


# instance fields
.field public mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

.field public mNumberOfExtraType:S


# direct methods
.method public constructor <init>([B)V
    .locals 7
    .param p1, "arrData"    # [B

    .prologue
    const/4 v6, 0x5

    .line 202
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 199
    new-array v4, v6, [Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    iput-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    .line 203
    if-eqz p1, :cond_0

    array-length v4, p1

    const/16 v5, 0x14

    if-lt v4, v5, :cond_0

    .line 205
    const/4 v2, 0x4

    .line 206
    .local v2, "nIdx":I
    new-array v0, v6, [B

    .line 208
    .local v0, "arrTmp":[B
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    aget-byte v4, p1, v2

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    iput-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mNumberOfExtraType:S

    .line 209
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v3

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    :goto_0
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mNumberOfExtraType:S

    if-ge v1, v4, :cond_0

    .line 211
    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-static {p1, v2, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x3

    .line 212
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    new-instance v5, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    invoke-direct {v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;-><init>()V

    aput-object v5, v4, v1

    .line 213
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->setData([B)V

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    .end local v0    # "arrTmp":[B
    .end local v1    # "i":I
    .end local v2    # "nIdx":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getExtraIDFromDeviceType(S)I
    .locals 4
    .param p1, "nDeviceType"    # S

    .prologue
    .line 220
    const/4 v1, -0x1

    .line 222
    .local v1, "nRtn":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mNumberOfExtraType:S

    if-ge v0, v2, :cond_0

    .line 224
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    aget-object v2, v2, v0

    iget-short v2, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mDeviceType:S

    if-ne v2, p1, :cond_1

    .line 226
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;->mExtraType:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;

    aget-object v2, v2, v0

    iget-short v2, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mExtraID:S

    const v3, 0xffff

    and-int v1, v2, v3

    .line 231
    :cond_0
    return v1

    .line 222
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
