.class public Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;
.super Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;
.source "HealthCoverEcg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;
    }
.end annotation


# static fields
.field public static final ECG_MEASUREMENT_DURATION:I = 0x20

.field private static final SAVE_ECG_RAW:Z = false

.field private static final SKIP_HEART_RATE_COUNT:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public mEcgFilter:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;

.field public mEcgHPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

.field public mEcgLPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

.field private mHeartRateCnt:I

.field private mRRISum:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IFF)V
    .locals 4
    .param p1, "nFs"    # I
    .param p2, "fCoefA"    # F
    .param p3, "fCoefB"    # F

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 31
    mul-int/lit8 v0, p1, 0x20

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;-><init>(IIFF)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    .line 23
    iput-wide v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    .line 25
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;

    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;-><init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;)V

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgFilter:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;

    .line 26
    iput-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgHPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    .line 27
    iput-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgLPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    .line 32
    iput p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mFs:I

    .line 33
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgFilter:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;

    invoke-virtual {v0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->initEcgFilter()V

    .line 36
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_HPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    invoke-direct {v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;-><init>(Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;)V

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgHPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    .line 37
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_LPF_CHEBY2:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    invoke-direct {v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;-><init>(Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;)V

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgLPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    .line 39
    iput-wide v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    .line 40
    return-void
.end method


# virtual methods
.method public addRRI(D)I
    .locals 2
    .param p1, "dbRRI"    # D

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    .line 45
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 46
    iget-wide v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    .line 49
    :cond_0
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    return v0
.end method

.method public getMeanHR()D
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 54
    const-wide/16 v0, 0x0

    .line 56
    .local v0, "dbMeanHR":D
    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    const/4 v3, 0x2

    if-le v2, v3, :cond_0

    iget-wide v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    cmpl-double v2, v2, v6

    if-lez v2, :cond_0

    .line 57
    iget-wide v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mRRISum:D

    iget v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mHeartRateCnt:I

    add-int/lit8 v4, v4, -0x2

    int-to-double v4, v4

    div-double v0, v2, v4

    .line 58
    cmpl-double v2, v0, v6

    if-lez v2, :cond_1

    .line 59
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    div-double/2addr v2, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x404e000000000000L    # 60.0

    mul-double v0, v2, v4

    .line 64
    :cond_0
    :goto_0
    return-wide v0

    .line 61
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public processEcgData([I)I
    .locals 3
    .param p1, "arrRawData"    # [I

    .prologue
    .line 69
    const/4 v0, 0x1

    .line 74
    .local v0, "nRtn":I
    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mPacketCount:I

    array-length v2, p1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mPacketCount:I

    .line 76
    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mPacketCount:I

    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mMaxMeasurementCount:I

    if-lt v1, v2, :cond_0

    .line 78
    const/16 v0, 0xff

    .line 81
    :cond_0
    return v0
.end method
