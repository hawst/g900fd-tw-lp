.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapCtlMsgCommonChangeStatus"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0x5


# instance fields
.field public mManufacturer:[B

.field public mPowerMode:B

.field public mReserved:[B

.field public mStatus:B


# direct methods
.method public constructor <init>([B)V
    .locals 7
    .param p1, "arrData"    # [B

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x0

    .line 279
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 275
    new-array v2, v6, [B

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mManufacturer:[B

    .line 276
    new-array v2, v5, [B

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mReserved:[B

    .line 280
    if-eqz p1, :cond_0

    array-length v2, p1

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    .line 282
    const/4 v0, 0x4

    .line 283
    .local v0, "nIdx":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v2, p1, v0

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mPowerMode:B

    .line 284
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v1

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mStatus:B

    .line 285
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mManufacturer:[B

    invoke-static {p1, v0, v2, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v0, 0x8

    .line 286
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mReserved:[B

    invoke-static {p1, v0, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v0, 0x5

    .line 289
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method


# virtual methods
.method public makeMessage()[B
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 293
    const/4 v2, 0x0

    .line 294
    .local v2, "nIdx":I
    const/16 v4, 0x14

    new-array v0, v4, [B

    .line 295
    .local v0, "arrMsg":[B
    invoke-super {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->makeMessage()[B

    move-result-object v1

    .line 296
    .local v1, "arrTmp":[B
    const/4 v4, 0x4

    invoke-static {v1, v6, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x4

    .line 297
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    iget-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mPowerMode:B

    aput-byte v4, v0, v2

    .line 298
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    iget-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mStatus:B

    aput-byte v4, v0, v3

    .line 299
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mManufacturer:[B

    const/16 v5, 0x8

    invoke-static {v4, v6, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x8

    .line 300
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mReserved:[B

    const/4 v5, 0x5

    invoke-static {v4, v6, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x5

    .line 301
    return-object v0
.end method
