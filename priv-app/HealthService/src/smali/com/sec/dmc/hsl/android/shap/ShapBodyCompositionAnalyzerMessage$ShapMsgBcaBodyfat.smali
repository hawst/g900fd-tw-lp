.class public Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapBodyCompositionAnalyzerMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgBcaBodyfat"
.end annotation


# instance fields
.field public mBMIPrime:S

.field public mBodyFatMass:I

.field public mBodyWater:I

.field public mHeight:I

.field public mRMR:I

.field public mSkeletalMuscle:I

.field public mSoftLeanMass:I

.field public mStatus:B

.field public mWeight:I


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "arrData"    # [B

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 65
    if-eqz p1, :cond_0

    array-length v2, p1

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    .line 67
    const/4 v0, 0x4

    .line 68
    .local v0, "nIdx":I
    aget-byte v2, p1, v0

    const/4 v3, 0x5

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mWeight:I

    add-int/lit8 v0, v0, 0x2

    .line 69
    aget-byte v2, p1, v0

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mHeight:I

    add-int/lit8 v0, v0, 0x2

    .line 70
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mBMIPrime:S

    .line 71
    aget-byte v2, p1, v1

    const/16 v3, 0xa

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mBodyFatMass:I

    add-int/lit8 v0, v1, 0x2

    .line 72
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v0

    const/16 v3, 0xc

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mBodyWater:I

    add-int/lit8 v0, v0, 0x2

    .line 73
    aget-byte v2, p1, v0

    const/16 v3, 0xe

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mSoftLeanMass:I

    add-int/lit8 v0, v0, 0x2

    .line 74
    aget-byte v2, p1, v0

    const/16 v3, 0x10

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mSkeletalMuscle:I

    add-int/lit8 v0, v0, 0x2

    .line 75
    aget-byte v2, p1, v0

    const/16 v3, 0x12

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mRMR:I

    add-int/lit8 v0, v0, 0x2

    .line 76
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .restart local v1    # "nIdx":I
    aget-byte v2, p1, v0

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;->mStatus:B

    .line 78
    .end local v1    # "nIdx":I
    :cond_0
    return-void
.end method
