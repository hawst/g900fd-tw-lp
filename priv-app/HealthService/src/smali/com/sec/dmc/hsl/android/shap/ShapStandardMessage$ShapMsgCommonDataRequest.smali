.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgCommonDataRequest"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0x5


# instance fields
.field public mRequestFlag:B

.field public mRequestMessageID:B

.field public mRequestType:B

.field public mReserved:[B

.field public mSupplement1:S

.field public mSupplement2:S

.field public mTransmissionInterval:S

.field public mTransmissionMode:S


# direct methods
.method public constructor <init>([B)V
    .locals 5
    .param p1, "arrData"    # [B

    .prologue
    const/4 v4, 0x5

    .line 25
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 21
    new-array v2, v4, [B

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mReserved:[B

    .line 26
    if-eqz p1, :cond_0

    array-length v2, p1

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    .line 28
    const/4 v0, 0x4

    .line 29
    .local v0, "nIdx":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v2, p1, v0

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 30
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v1

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 31
    aget-byte v2, p1, v0

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    add-int/lit8 v0, v0, 0x2

    .line 32
    aget-byte v2, p1, v0

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    add-int/lit8 v0, v0, 0x2

    .line 33
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .restart local v1    # "nIdx":I
    aget-byte v2, p1, v0

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    .line 34
    aget-byte v2, p1, v1

    const/16 v3, 0xc

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    add-int/lit8 v0, v1, 0x2

    .line 35
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v0

    const/16 v3, 0xe

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    add-int/lit8 v0, v0, 0x2

    .line 36
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mReserved:[B

    const/4 v3, 0x0

    invoke-static {p1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    add-int/lit8 v0, v0, 0x5

    .line 39
    .end local v0    # "nIdx":I
    :cond_0
    return-void

    .line 21
    nop

    :array_0
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
    .end array-data
.end method


# virtual methods
.method public makeMessage()[B
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0xffff

    .line 43
    const/4 v2, 0x0

    .line 44
    .local v2, "nIdx":I
    const/16 v4, 0x14

    new-array v0, v4, [B

    .line 45
    .local v0, "arrMsg":[B
    invoke-super {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->makeMessage()[B

    move-result-object v1

    .line 47
    .local v1, "arrTmp":[B
    const/4 v4, 0x4

    invoke-static {v1, v6, v0, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x4

    .line 49
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    iget-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    aput-byte v4, v0, v2

    .line 50
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    iget-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    aput-byte v4, v0, v3

    .line 51
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    and-int/2addr v4, v5

    invoke-static {v0, v2, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->fill16bitData([BII)V

    add-int/lit8 v2, v2, 0x2

    .line 52
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    and-int/2addr v4, v5

    invoke-static {v0, v2, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->fill16bitData([BII)V

    add-int/lit8 v2, v2, 0x2

    .line 53
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .restart local v3    # "nIdx":I
    iget-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    aput-byte v4, v0, v2

    .line 54
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    and-int/2addr v4, v5

    invoke-static {v0, v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->fill16bitData([BII)V

    add-int/lit8 v2, v3, 0x2

    .line 55
    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    and-int/2addr v4, v5

    invoke-static {v0, v2, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->fill16bitData([BII)V

    add-int/lit8 v2, v2, 0x2

    .line 56
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mReserved:[B

    const/4 v5, 0x5

    invoke-static {v4, v6, v0, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x5

    .line 58
    return-object v0
.end method
