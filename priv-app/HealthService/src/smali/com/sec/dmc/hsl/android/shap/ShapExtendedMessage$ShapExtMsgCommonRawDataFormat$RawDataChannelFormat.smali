.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
.super Ljava/lang/Object;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RawDataChannelFormat"
.end annotation


# static fields
.field public static RAW_DATA_CHANNEL_FORMAT_SIZE:I

.field private static RESERVED_SIZE:I


# instance fields
.field public mChannelID:S

.field public mCoeff_a:F

.field public mCoeff_b:F

.field public mReserved:[B

.field public mSampleSize:S

.field public mSamplingFreq:F

.field public mType:S

.field public mUnit:S

.field public mUnitPrefix:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    const/16 v0, 0x20

    sput v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RAW_DATA_CHANNEL_FORMAT_SIZE:I

    .line 180
    const/16 v0, 0xf

    sput v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RESERVED_SIZE:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 6
    .param p1, "arrData"    # [B

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    sget v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RESERVED_SIZE:I

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mReserved:[B

    .line 192
    if-eqz p1, :cond_0

    array-length v2, p1

    sget v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RAW_DATA_CHANNEL_FORMAT_SIZE:I

    if-ne v2, v3, :cond_0

    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "nIdx":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mChannelID:S

    .line 196
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mType:S

    .line 197
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .restart local v1    # "nIdx":I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mSampleSize:S

    .line 198
    aget-byte v2, p1, v1

    const/4 v3, 0x4

    aget-byte v3, p1, v3

    const/4 v4, 0x5

    aget-byte v4, p1, v4

    const/4 v5, 0x6

    aget-byte v5, p1, v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeFloat(BBBB)F

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mCoeff_a:F

    add-int/lit8 v0, v1, 0x4

    .line 199
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v0

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    const/16 v4, 0x9

    aget-byte v4, p1, v4

    const/16 v5, 0xa

    aget-byte v5, p1, v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeFloat(BBBB)F

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mCoeff_b:F

    add-int/lit8 v0, v0, 0x4

    .line 200
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .restart local v1    # "nIdx":I
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mUnitPrefix:S

    .line 201
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mUnit:S

    .line 202
    aget-byte v2, p1, v0

    const/16 v3, 0xe

    aget-byte v3, p1, v3

    const/16 v4, 0xf

    aget-byte v4, p1, v4

    const/16 v5, 0x10

    aget-byte v5, p1, v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeFloat(BBBB)F

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mSamplingFreq:F

    add-int/lit8 v0, v0, 0x4

    .line 205
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method
