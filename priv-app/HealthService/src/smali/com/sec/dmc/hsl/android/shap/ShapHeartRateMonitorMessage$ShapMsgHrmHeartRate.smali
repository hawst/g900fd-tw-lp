.class public Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapHeartRateMonitorMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgHrmHeartRate"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0x6


# instance fields
.field public mCurrentBeatTick:J

.field public mMeanHR:S

.field public mPrevBeatTick:J

.field public mReserved:[B

.field public mStatus:S


# direct methods
.method public constructor <init>([B)V
    .locals 6
    .param p1, "arrData"    # [B

    .prologue
    const/4 v5, 0x6

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 33
    new-array v1, v5, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mReserved:[B

    .line 37
    if-eqz p1, :cond_0

    array-length v1, p1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 39
    const/4 v0, 0x4

    .line 40
    .local v0, "nIdx":I
    aget-byte v1, p1, v0

    int-to-short v1, v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mStatus:S

    add-int/lit8 v0, v0, 0x1

    .line 41
    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    add-int/lit8 v0, v0, 0x1

    .line 42
    aget-byte v1, p1, v0

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    const/16 v4, 0x9

    aget-byte v4, p1, v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt32(BBBB)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mPrevBeatTick:J

    add-int/lit8 v0, v0, 0x4

    .line 43
    aget-byte v1, p1, v0

    const/16 v2, 0xb

    aget-byte v2, p1, v2

    const/16 v3, 0xc

    aget-byte v3, p1, v3

    const/16 v4, 0xd

    aget-byte v4, p1, v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt32(BBBB)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mCurrentBeatTick:J

    add-int/lit8 v0, v0, 0x4

    .line 44
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mReserved:[B

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method
