.class public Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapBodyCompositionAnalyzerMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgBcaIntermediateData"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0xe


# instance fields
.field public mProgress:S

.field public mReserved:[B

.field public mStatus:B


# direct methods
.method public constructor <init>([B)V
    .locals 5
    .param p1, "arrData"    # [B

    .prologue
    const/16 v4, 0xe

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 36
    new-array v2, v4, [B

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;->mReserved:[B

    .line 41
    if-eqz p1, :cond_0

    array-length v2, p1

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    .line 43
    const/4 v0, 0x4

    .line 44
    .local v0, "nIdx":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v2, p1, v0

    iput-byte v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;->mStatus:B

    .line 45
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v2, p1, v1

    and-int/lit16 v2, v2, 0xff

    int-to-short v2, v2

    iput-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;->mProgress:S

    .line 46
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;->mReserved:[B

    const/4 v3, 0x0

    invoke-static {p1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 48
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method
