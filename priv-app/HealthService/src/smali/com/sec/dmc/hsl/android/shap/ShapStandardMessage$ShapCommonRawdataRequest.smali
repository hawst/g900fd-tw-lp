.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapCommonRawdataRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;
    }
.end annotation


# static fields
.field static final NUM_OF_REQ_CHN:I = 0x5


# instance fields
.field public mNumberOfChannel:B

.field public mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;


# direct methods
.method public constructor <init>([B)V
    .locals 8
    .param p1, "arrData"    # [B

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x5

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 103
    new-array v4, v6, [Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    iput-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    .line 109
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 111
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    new-instance v5, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    invoke-direct {v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;-><init>()V

    aput-object v5, v4, v1

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    :cond_0
    if-eqz p1, :cond_1

    array-length v4, p1

    const/16 v5, 0x14

    if-lt v4, v5, :cond_1

    .line 116
    const/4 v2, 0x4

    .line 117
    .local v2, "nIdx":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    aget-byte v4, p1, v2

    iput-byte v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    .line 118
    new-array v0, v7, [B

    .line 119
    .local v0, "arrTmp":[B
    const/4 v1, 0x0

    move v2, v3

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    :goto_1
    if-ge v1, v6, :cond_1

    .line 121
    const/4 v4, 0x0

    invoke-static {p1, v2, v0, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v2, 0x3

    .line 122
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->setChannelInfo([B)V

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 125
    .end local v0    # "arrTmp":[B
    .end local v2    # "nIdx":I
    :cond_1
    return-void
.end method


# virtual methods
.method public makeMessage()[B
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 136
    const/16 v5, 0x14

    new-array v0, v5, [B

    .line 137
    .local v0, "arrMsg":[B
    const/4 v3, 0x0

    .line 138
    .local v3, "nIdx":I
    invoke-super {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->makeMessage()[B

    move-result-object v1

    .line 140
    .local v1, "arrTmp":[B
    const/4 v5, 0x4

    invoke-static {v1, v7, v0, v7, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v3, 0x4

    .line 142
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nIdx":I
    .local v4, "nIdx":I
    iget-byte v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    aput-byte v5, v0, v3

    .line 144
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v5, 0x5

    if-ge v2, v5, :cond_0

    .line 146
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nIdx":I
    .restart local v3    # "nIdx":I
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v5, v5, v2

    iget-byte v5, v5, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mChannelID:B

    aput-byte v5, v0, v4

    .line 147
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "nIdx":I
    .restart local v4    # "nIdx":I
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    aget-byte v5, v5, v7

    aput-byte v5, v0, v3

    .line 148
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "nIdx":I
    .restart local v3    # "nIdx":I
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v5, v5, v2

    iget-object v5, v5, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->mReserved:[B

    const/4 v6, 0x1

    aget-byte v5, v5, v6

    aput-byte v5, v0, v4

    .line 144
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    .end local v3    # "nIdx":I
    .restart local v4    # "nIdx":I
    goto :goto_0

    .line 151
    :cond_0
    return-object v0
.end method

.method public setChannelInfo(IBBB)V
    .locals 1
    .param p1, "nSeq"    # I
    .param p2, "chnId"    # B
    .param p3, "reserved0"    # B
    .param p4, "reserved1"    # B

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2, p3, p4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->setChannelInfo(BBB)V

    .line 133
    return-void
.end method

.method public setChannelInfo(I[B)V
    .locals 1
    .param p1, "nSeq"    # I
    .param p2, "arrData"    # [B

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mRequestChannelInfo:[Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest$ShapRequestChannelInfo;->setChannelInfo([B)V

    .line 129
    return-void
.end method
