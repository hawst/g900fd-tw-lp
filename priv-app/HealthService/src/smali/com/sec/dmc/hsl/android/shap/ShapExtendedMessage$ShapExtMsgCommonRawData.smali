.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgCommonRawData"
.end annotation


# instance fields
.field public RawData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field public mChannelID:[B

.field public mNumberOfChannel:S


# direct methods
.method public constructor <init>([B)V
    .locals 11
    .param p1, "arrData"    # [B

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 217
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    .line 218
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->RawData:Ljava/util/ArrayList;

    .line 223
    if-eqz p1, :cond_0

    array-length v5, p1

    int-to-long v5, v5

    iget-wide v7, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mMessageLength:J

    const-wide/16 v9, 0x8

    add-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-ltz v5, :cond_0

    .line 225
    const/16 v2, 0x8

    .line 226
    .local v2, "nIdx":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    aget-byte v5, p1, v2

    int-to-short v5, v5

    iput-short v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mNumberOfChannel:S

    .line 227
    iget-short v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mNumberOfChannel:S

    new-array v5, v5, [B

    iput-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    .line 229
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v3

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    :goto_0
    iget-short v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mNumberOfChannel:S

    if-ge v1, v5, :cond_0

    .line 231
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .restart local v3    # "nIdx":I
    aget-byte v6, p1, v2

    aput-byte v6, v5, v1

    .line 232
    aget-byte v5, p1, v3

    add-int/lit8 v6, v3, 0x1

    aget-byte v6, p1, v6

    invoke-static {v5, v6}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v4

    .local v4, "nSize":I
    add-int/lit8 v2, v3, 0x2

    .line 233
    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    new-array v0, v4, [B

    .line 234
    .local v0, "arrTmp":[B
    const/4 v5, 0x0

    invoke-static {p1, v2, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 235
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->RawData:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "arrTmp":[B
    .end local v1    # "i":I
    .end local v2    # "nIdx":I
    .end local v4    # "nSize":I
    :cond_0
    return-void
.end method


# virtual methods
.method public convertRawDataToInt32(I)[I
    .locals 7
    .param p1, "nChnId"    # I

    .prologue
    .line 272
    const/4 v0, 0x0

    .line 273
    .local v0, "arrRtn":[I
    const/4 v4, -0x1

    .line 274
    .local v4, "nChnIdx":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    array-length v6, v6

    if-ge v2, v6, :cond_0

    .line 276
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    aget-byte v6, v6, v2

    if-ne p1, v6, :cond_2

    .line 278
    move v4, v2

    .line 283
    :cond_0
    if-ltz v4, :cond_1

    .line 285
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->RawData:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 286
    .local v1, "bbTmp":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 287
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    .line 288
    .local v3, "ibTmp":Ljava/nio/IntBuffer;
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->capacity()I

    move-result v5

    .line 289
    .local v5, "nSize":I
    new-array v0, v5, [I

    .line 290
    invoke-virtual {v3, v0}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    .line 293
    .end local v1    # "bbTmp":Ljava/nio/ByteBuffer;
    .end local v3    # "ibTmp":Ljava/nio/IntBuffer;
    .end local v5    # "nSize":I
    :cond_1
    return-object v0

    .line 274
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public convertRawDataToIntFloatArray(I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "nChnId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "arrRtn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v4, -0x1

    .line 245
    .local v4, "nChnIdx":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    array-length v6, v6

    if-ge v2, v6, :cond_0

    .line 247
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    aget-byte v6, v6, v2

    if-ne p1, v6, :cond_1

    .line 249
    move v4, v2

    .line 253
    :cond_0
    if-ltz v4, :cond_2

    .line 255
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->RawData:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 256
    .local v1, "bbTmp":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 257
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    .line 259
    .local v3, "ibTmp":Ljava/nio/IntBuffer;
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->capacity()I

    move-result v5

    .line 261
    .local v5, "nSize":I
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "arrRtn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 262
    .restart local v0    # "arrRtn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Float;>;"
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_2

    .line 264
    invoke-virtual {v3, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v6

    int-to-float v6, v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 245
    .end local v1    # "bbTmp":Ljava/nio/ByteBuffer;
    .end local v3    # "ibTmp":Ljava/nio/IntBuffer;
    .end local v5    # "nSize":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 267
    :cond_2
    return-object v0
.end method
