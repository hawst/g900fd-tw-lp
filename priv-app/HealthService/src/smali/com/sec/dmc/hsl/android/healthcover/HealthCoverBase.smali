.class public abstract Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;
.super Ljava/lang/Object;
.source "HealthCoverBase.java"


# static fields
.field public static final DATA_ADDED:I = 0x1

.field public static final DATA_MEASUREMENT_DONE:I = 0xff

.field public static final DATA_MEASUREMENT_ERROR:I = 0x2

.field public static final DATA_NONE:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mCoefA:F

.field protected mCoefB:F

.field protected mFs:I

.field public mMaxMeasurementCount:I

.field public mPacketCount:I

.field public mReceivedRawData:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIFF)V
    .locals 2
    .param p1, "nFs"    # I
    .param p2, "nMaxMeasurementCount"    # I
    .param p3, "fCoefA"    # F
    .param p4, "fCoefB"    # F

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mFs:I

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefA:F

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefB:F

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mReceivedRawData:[I

    .line 27
    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mMaxMeasurementCount:I

    .line 32
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->initializeRawData(IIFF)V

    .line 34
    return-void
.end method

.method private writeFloatArrayToFile(Ljava/lang/String;[FI)I
    .locals 12
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "arrData"    # [F
    .param p3, "nLength"    # I

    .prologue
    .line 210
    const/4 v9, -0x1

    .line 212
    .local v9, "nRtn":I
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 214
    .local v5, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    const/4 v6, 0x0

    .line 221
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 222
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    const/4 v2, 0x0

    .line 224
    .local v2, "dos":Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 225
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v1, Ljava/io/BufferedOutputStream;

    const/16 v11, 0x1f40

    invoke-direct {v1, v7, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c

    .line 226
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    :try_start_3
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_d

    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .local v3, "dos":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    move-object v6, v7

    .line 232
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, p3, :cond_0

    .line 233
    :try_start_4
    aget v11, p2, v8

    invoke-virtual {v2, v11}, Ljava/io/DataOutputStream;->writeFloat(F)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 232
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 215
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "i":I
    :catch_0
    move-exception v4

    .line 216
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    move v10, v9

    .line 291
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v9    # "nRtn":I
    .local v10, "nRtn":I
    :goto_2
    return v10

    .line 227
    .end local v10    # "nRtn":I
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "nRtn":I
    :catch_1
    move-exception v4

    .line 228
    .restart local v4    # "e":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 238
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v8    # "i":I
    :cond_0
    const/4 v9, 0x0

    .line 248
    if-eqz v2, :cond_1

    .line 252
    :try_start_5
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 262
    :cond_1
    :goto_4
    if-eqz v0, :cond_2

    .line 266
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 276
    :cond_2
    :goto_5
    if-eqz v6, :cond_3

    .line 280
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_3
    :goto_6
    move v10, v9

    .line 291
    .end local v9    # "nRtn":I
    .restart local v10    # "nRtn":I
    goto :goto_2

    .line 255
    .end local v10    # "nRtn":I
    .restart local v9    # "nRtn":I
    :catch_2
    move-exception v4

    .line 258
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 269
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 272
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 283
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 286
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 240
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 242
    .local v4, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 248
    if-eqz v2, :cond_4

    .line 252
    :try_start_9
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 262
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_7
    if-eqz v0, :cond_5

    .line 266
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 276
    :cond_5
    :goto_8
    if-eqz v6, :cond_3

    .line 280
    :try_start_b
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto :goto_6

    .line 283
    :catch_6
    move-exception v4

    .line 286
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 255
    .local v4, "e":Ljava/lang/Exception;
    :catch_7
    move-exception v4

    .line 258
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 269
    .end local v4    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v4

    .line 272
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 248
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    if-eqz v2, :cond_6

    .line 252
    :try_start_c
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 262
    :cond_6
    :goto_9
    if-eqz v0, :cond_7

    .line 266
    :try_start_d
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 276
    :cond_7
    :goto_a
    if-eqz v6, :cond_8

    .line 280
    :try_start_e
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 287
    :cond_8
    :goto_b
    throw v11

    .line 255
    :catch_9
    move-exception v4

    .line 258
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 269
    .end local v4    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v4

    .line 272
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 283
    .end local v4    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v4

    .line 286
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 227
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "i":I
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v4

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v4

    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method private writeIntArrayToFile(Ljava/lang/String;[II)I
    .locals 12
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "arrData"    # [I
    .param p3, "nLength"    # I

    .prologue
    .line 124
    const/4 v9, -0x1

    .line 126
    .local v9, "nRtn":I
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    .local v5, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    const/4 v6, 0x0

    .line 135
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/4 v0, 0x0

    .line 136
    .local v0, "bos":Ljava/io/BufferedOutputStream;
    const/4 v2, 0x0

    .line 138
    .local v2, "dos":Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v1, Ljava/io/BufferedOutputStream;

    const/16 v11, 0x1f40

    invoke-direct {v1, v7, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c

    .line 140
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .local v1, "bos":Ljava/io/BufferedOutputStream;
    :try_start_3
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_d

    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .local v3, "dos":Ljava/io/DataOutputStream;
    move-object v2, v3

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    move-object v6, v7

    .line 146
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, p3, :cond_0

    .line 147
    :try_start_4
    aget v11, p2, v8

    invoke-virtual {v2, v11}, Ljava/io/DataOutputStream;->writeInt(I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 146
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 129
    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v2    # "dos":Ljava/io/DataOutputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "i":I
    :catch_0
    move-exception v4

    .line 130
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    move v10, v9

    .line 205
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v9    # "nRtn":I
    .local v10, "nRtn":I
    :goto_2
    return v10

    .line 141
    .end local v10    # "nRtn":I
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v2    # "dos":Ljava/io/DataOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "nRtn":I
    :catch_1
    move-exception v4

    .line 142
    .restart local v4    # "e":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 152
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v8    # "i":I
    :cond_0
    const/4 v9, 0x0

    .line 162
    if-eqz v2, :cond_1

    .line 166
    :try_start_5
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 176
    :cond_1
    :goto_4
    if-eqz v0, :cond_2

    .line 180
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 190
    :cond_2
    :goto_5
    if-eqz v6, :cond_3

    .line 194
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_3
    :goto_6
    move v10, v9

    .line 205
    .end local v9    # "nRtn":I
    .restart local v10    # "nRtn":I
    goto :goto_2

    .line 169
    .end local v10    # "nRtn":I
    .restart local v9    # "nRtn":I
    :catch_2
    move-exception v4

    .line 172
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 183
    .end local v4    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 186
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 197
    .end local v4    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    .line 200
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 154
    .end local v4    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 156
    .local v4, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 162
    if-eqz v2, :cond_4

    .line 166
    :try_start_9
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 176
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_7
    if-eqz v0, :cond_5

    .line 180
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 190
    :cond_5
    :goto_8
    if-eqz v6, :cond_3

    .line 194
    :try_start_b
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto :goto_6

    .line 197
    :catch_6
    move-exception v4

    .line 200
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 169
    .local v4, "e":Ljava/lang/Exception;
    :catch_7
    move-exception v4

    .line 172
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 183
    .end local v4    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v4

    .line 186
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 162
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    if-eqz v2, :cond_6

    .line 166
    :try_start_c
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 176
    :cond_6
    :goto_9
    if-eqz v0, :cond_7

    .line 180
    :try_start_d
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_a

    .line 190
    :cond_7
    :goto_a
    if-eqz v6, :cond_8

    .line 194
    :try_start_e
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 201
    :cond_8
    :goto_b
    throw v11

    .line 169
    :catch_9
    move-exception v4

    .line 172
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 183
    .end local v4    # "e":Ljava/io/IOException;
    :catch_a
    move-exception v4

    .line 186
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 197
    .end local v4    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v4

    .line 200
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 141
    .end local v4    # "e":Ljava/io/IOException;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "i":I
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v4

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v0    # "bos":Ljava/io/BufferedOutputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_d
    move-exception v4

    move-object v0, v1

    .end local v1    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v0    # "bos":Ljava/io/BufferedOutputStream;
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3
.end method


# virtual methods
.method protected addRawData([II)I
    .locals 3
    .param p1, "arrData"    # [I
    .param p2, "nCount"    # I

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    add-int/2addr v0, p2

    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mMaxMeasurementCount:I

    if-le v0, v1, :cond_0

    .line 50
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mMaxMeasurementCount:I

    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    sub-int p2, v0, v1

    .line 52
    :cond_0
    if-lez p2, :cond_1

    .line 54
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mReceivedRawData:[I

    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    invoke-static {p1, v0, v1, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    .line 58
    :cond_1
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    return v0
.end method

.method public getCoefA()F
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefA:F

    return v0
.end method

.method public getCoefB()F
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefB:F

    return v0
.end method

.method public getFs()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mFs:I

    return v0
.end method

.method protected initializeRawData(IIFF)V
    .locals 1
    .param p1, "nFs"    # I
    .param p2, "nMaxMeasurementCount"    # I
    .param p3, "fCoefA"    # F
    .param p4, "fCoefB"    # F

    .prologue
    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    .line 39
    iput p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mFs:I

    .line 40
    iput p2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mMaxMeasurementCount:I

    .line 41
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mFs:I

    add-int/2addr v0, p2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mReceivedRawData:[I

    .line 43
    iput p3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefA:F

    .line 44
    iput p4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mCoefB:F

    .line 45
    return-void
.end method

.method public saveToFile(Ljava/lang/String;)I
    .locals 9
    .param p1, "strName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 99
    const/4 v2, -0x1

    .line 101
    .local v2, "nRtn":I
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v2

    .line 104
    :cond_1
    iget v6, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    if-lez v6, :cond_0

    .line 107
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 109
    .local v4, "strExternalStorage":Ljava/lang/String;
    const-string v6, "%s/Cover_DATA_RAW/"

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "strDirectory":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v1, "file":Ljava/io/File;
    const/4 v0, 0x1

    .line 112
    .local v0, "bSuccess":Z
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    .line 113
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 115
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 117
    .local v5, "strFileName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 118
    iget-object v6, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mReceivedRawData:[I

    iget v7, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->mPacketCount:I

    invoke-direct {p0, v5, v6, v7}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->writeIntArrayToFile(Ljava/lang/String;[II)I

    goto :goto_0
.end method

.method public saveToFile(Ljava/lang/String;[F)I
    .locals 9
    .param p1, "strName"    # Ljava/lang/String;
    .param p2, "arrFloat"    # [F

    .prologue
    const/4 v7, 0x1

    .line 75
    const/4 v2, -0x1

    .line 77
    .local v2, "nRtn":I
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v2

    .line 81
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "strExternalStorage":Ljava/lang/String;
    const-string v6, "%s/Cover_DATA_RAW/"

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "strDirectory":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "file":Ljava/io/File;
    const/4 v0, 0x1

    .line 86
    .local v0, "bSuccess":Z
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_2

    .line 87
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 89
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 91
    .local v5, "strFileName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 92
    array-length v6, p2

    invoke-direct {p0, v5, p2, v6}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->writeFloatArrayToFile(Ljava/lang/String;[FI)I

    goto :goto_0
.end method
