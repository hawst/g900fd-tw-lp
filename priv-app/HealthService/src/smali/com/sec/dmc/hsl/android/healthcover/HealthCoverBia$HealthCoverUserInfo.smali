.class public Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;
.super Ljava/lang/Object;
.source "HealthCoverBia.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HealthCoverUserInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    }
.end annotation


# static fields
.field public static final USER_INFO_GENDER_FEMALE:I = 0x0

.field public static final USER_INFO_GENDER_MALE:I = 0x1


# instance fields
.field public mAge:I

.field public mGender:I

.field public mHeight:F

.field public mWeight:F


# direct methods
.method public constructor <init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;IFF)V
    .locals 1
    .param p1, "gender"    # Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    .param p2, "nAge"    # I
    .param p3, "fHeight"    # F
    .param p4, "fWeight"    # F

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mGender:I

    .line 159
    iput p2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mAge:I

    .line 160
    iput p3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mHeight:F

    .line 161
    iput p4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mWeight:F

    .line 162
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
