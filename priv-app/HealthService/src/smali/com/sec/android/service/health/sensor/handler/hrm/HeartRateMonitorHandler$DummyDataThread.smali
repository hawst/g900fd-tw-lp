.class Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;
.super Ljava/lang/Thread;
.source "HeartRateMonitorHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DummyDataThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 47
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 53
    .local v2, "r":Ljava/util/Random;
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Generating dummy data"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const-wide/16 v3, 0xc8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 55
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v3, :cond_0

    .line 57
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v1, "finalData":Landroid/os/Bundle;
    const-string v3, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 59
    const-string v3, "heart_rate"

    const/16 v4, 0x64

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    const-string v3, "heart_rate_interval"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 61
    const-string v3, "heart_rate_snr"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)F

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 62
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v4, 0x1

    invoke-interface {v3, v4, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    .end local v1    # "finalData":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 68
    .local v0, "ex":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Sending of dummy data is interrupted"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 71
    return-void
.end method
