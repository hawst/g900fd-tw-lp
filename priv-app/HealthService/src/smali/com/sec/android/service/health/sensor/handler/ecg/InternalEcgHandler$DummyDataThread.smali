.class Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;
.super Ljava/lang/Thread;
.source "InternalEcgHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DummyDataThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
    .param p2, "x1"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 44
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 49
    .local v1, "r":Ljava/util/Random;
    :cond_0
    :goto_0
    const-wide/16 v3, 0x3e8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 51
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v3, :cond_0

    .line 53
    const/4 v3, 0x1

    new-array v2, v3, [Landroid/os/Bundle;

    .line 54
    .local v2, "testData":[Landroid/os/Bundle;
    const/4 v3, 0x0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    aput-object v4, v2, v3

    .line 55
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const-string v4, "ecg_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 56
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const-string v4, "ecg_electrowave"

    const/4 v5, 0x3

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 57
    const/4 v3, 0x0

    aget-object v3, v2, v3

    const-string v4, "ecg_heartrate"

    const/16 v5, 0xc8

    invoke-virtual {v1, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    iget-object v3, v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v4, 0x4

    invoke-interface {v3, v4, v2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 63
    .end local v2    # "testData":[Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 68
    return-void

    .line 56
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x40000000    # 2.0f
        0x40400000    # 3.0f
    .end array-data
.end method
