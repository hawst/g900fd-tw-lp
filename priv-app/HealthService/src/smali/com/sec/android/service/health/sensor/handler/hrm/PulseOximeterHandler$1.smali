.class Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;
.super Ljava/lang/Object;
.source "PulseOximeterHandler.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 272
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 277
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_1

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const v1, 0x1001a

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v0

    if-ge v0, v4, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    aput-object v2, v0, v1

    .line 302
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v5

    float-to-int v1, v1

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$602(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;I)I

    .line 303
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    float-to-long v1, v1

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;J)J

    .line 304
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v6

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$402(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;F)F

    .line 305
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$702(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;F)F

    .line 307
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "heart_rate"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "heart_rate_interval"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "heart_rate_snr"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "heart_rate_unit"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNRUnit:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$500(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "pulse_oximetry"

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # operator++ for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$108(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    .line 317
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v0

    if-gez v0, :cond_2

    .line 318
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Sensor] state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 321
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[Sensor] Valid H_R_S_P_O_2 is received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :cond_3
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SpO2Listener => heartrate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " interval = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SNR = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPulseOximetry = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v0, v5}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$102(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;I)I

    goto/16 :goto_0
.end method
