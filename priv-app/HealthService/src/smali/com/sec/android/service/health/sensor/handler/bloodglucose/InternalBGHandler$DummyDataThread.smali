.class Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
.super Ljava/lang/Thread;
.source "InternalBGHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DummyDataThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;
    .param p2, "x1"    # Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$1;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;-><init>(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v9, 0xa

    const/4 v8, 0x0

    .line 50
    # setter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {v8}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$002(I)I

    .line 51
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    .line 52
    .local v2, "r":Ljava/util/Random;
    const/16 v4, 0x1f4

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-float v0, v4

    .line 55
    .local v0, "BGvalue":F
    :goto_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$000()I

    move-result v4

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$100()[I

    move-result-object v5

    array-length v5, v5

    if-lt v4, v5, :cond_0

    .line 57
    # setter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {v8}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$002(I)I

    .line 101
    :goto_1
    return-void

    .line 63
    :cond_0
    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$100()[I

    move-result-object v4

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$000()I

    move-result v5

    aget v4, v4, v5

    if-ne v4, v9, :cond_2

    .line 64
    const-wide/16 v4, 0x1388

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :goto_2
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    iget-object v4, v4, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v4, :cond_1

    .line 75
    const/4 v4, 0x1

    new-array v3, v4, [Landroid/os/Bundle;

    .line 76
    .local v3, "testData":[Landroid/os/Bundle;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    aput-object v4, v3, v8

    .line 77
    aget-object v4, v3, v8

    const-string v5, "glucose_sensor_state"

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$100()[I

    move-result-object v6

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$000()I

    move-result v7

    aget v6, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    aget-object v4, v3, v8

    const-string v5, "glucose_error_detail"

    invoke-virtual {v4, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    aget-object v4, v3, v8

    const-string v5, "glucose"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 80
    aget-object v4, v3, v8

    const-string v5, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 81
    aget-object v4, v3, v8

    const-string v5, "glucose_unit"

    const v6, 0x222e1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    aget-object v4, v3, v8

    const-string v5, "sample_type"

    const v6, 0x15f91

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[TBG] State : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$100()[I

    move-result-object v6

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$000()I

    move-result v7

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " BG value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    iget-object v4, v4, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v5, 0x3

    invoke-interface {v4, v5, v3}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 89
    .end local v3    # "testData":[Landroid/os/Bundle;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;)Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    move-result-object v4

    if-eqz v4, :cond_3

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$100()[I

    move-result-object v4

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$000()I

    move-result v5

    aget v4, v4, v5

    if-ne v4, v9, :cond_3

    .line 91
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;)Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->interrupt()V

    .line 92
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    invoke-static {v4, v5}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;)Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    .line 93
    # setter for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {v8}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$002(I)I

    goto/16 :goto_1

    .line 66
    :cond_2
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 68
    :catch_0
    move-exception v1

    .line 70
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 98
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_3
    # operator++ for: Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->access$008()I

    goto/16 :goto_0
.end method
