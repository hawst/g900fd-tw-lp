.class public Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;
.super Landroid/os/RemoteCallbackList;
.source "HealthRemoteCallbackList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Landroid/os/RemoteCallbackList",
        "<TT;>;"
    }
.end annotation


# instance fields
.field device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field mHealthRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 0
    .param p2, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<TT;>;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<TT;>;"
    .local p1, "healthRemoteCallbackDied":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener<TT;>;"
    invoke-direct {p0}, Landroid/os/RemoteCallbackList;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->mHealthRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    .line 26
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 27
    return-void
.end method


# virtual methods
.method public getListener(Ljava/lang/Integer;)Landroid/os/IInterface;
    .locals 6
    .param p1, "cookie"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<TT;>;"
    const/4 v2, 0x0

    .line 39
    .local v2, "listener":Landroid/os/IInterface;, "TT;"
    sget-object v4, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    .line 41
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->beginBroadcast()I

    .line 42
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 44
    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 45
    .local v1, "integer":Ljava/lang/Integer;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v3, v5, :cond_1

    .line 47
    invoke-virtual {p0, v0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    .line 51
    .end local v1    # "integer":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->finishBroadcast()V

    .line 52
    monitor-exit v4

    .line 53
    return-object v2

    .line 42
    .restart local v1    # "integer":Ljava/lang/Integer;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 52
    .end local v0    # "i":I
    .end local v1    # "integer":Ljava/lang/Integer;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V
    .locals 2
    .param p2, "cookie"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<TT;>;"
    .local p1, "callback":Landroid/os/IInterface;, "TT;"
    invoke-super {p0, p1, p2}, Landroid/os/RemoteCallbackList;->onCallbackDied(Landroid/os/IInterface;Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->mHealthRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v0, v1, p2}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;->remoteCallbackDied(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)V

    .line 34
    return-void
.end method
