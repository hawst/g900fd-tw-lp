.class public Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;
.super Ljava/lang/Object;
.source "InternalBGHandler.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$1;,
        Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final CACHED_DATA_INTERVAL_CNT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static TEST_FLAG:Z = false

.field private static final TYPE_BASE:I = 0x10000

.field public static final TYPE_BLOOD_GLUCOSE:I = 0x1001f

.field private static nBGState:I

.field private static testBGstate:[I


# instance fields
.field private bListenerRegistered:Z

.field finalData:[Landroid/os/Bundle;

.field private mBloogGlucoseSensor:Landroid/hardware/Sensor;

.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field mbloodGlucoseDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private raw_data_cnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    .line 42
    sput-boolean v2, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TEST_FLAG:Z

    .line 43
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I

    .line 44
    sput v2, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I

    return-void

    .line 43
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x0
        0xa
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    .line 27
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    .line 28
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mBloogGlucoseSensor:Landroid/hardware/Sensor;

    .line 33
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    .line 35
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 36
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mbloodGlucoseDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Bundle;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    .line 47
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 20
    sget v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 20
    sput p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I

    return p0
.end method

.method static synthetic access$008()I
    .locals 2

    .prologue
    .line 20
    sget v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->nBGState:I

    return v0
.end method

.method static synthetic access$100()[I
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->testBGstate:[I

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;)Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;)Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;
    .param p1, "x1"    # Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    return-object p1
.end method

.method private registerBGListener()Z
    .locals 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001f

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mBloogGlucoseSensor:Landroid/hardware/Sensor;

    .line 216
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mBloogGlucoseSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    .line 217
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Internal Blood Glucose sensor was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "registered"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    return v0

    .line 217
    :cond_1
    const-string v0, "not "

    goto :goto_0
.end method

.method private unregisterBGListener()V
    .locals 3

    .prologue
    .line 227
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterBGListener : current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 231
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    const-string v1, "Internal Blood Glucose sensor is unregistered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    .line 235
    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    .prologue
    .line 119
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deinitialize is called - bListenerRegistered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->unregisterBGListener()V

    .line 122
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mbloodGlucoseDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mContext:Landroid/content/Context;

    .line 110
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mbloodGlucoseDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 111
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 112
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mSensorManager:Landroid/hardware/SensorManager;

    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 172
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 177
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const v1, 0x1001f

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    if-ge v0, v4, :cond_0

    .line 179
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 0 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 1 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 2 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 3 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 4 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IBG onSensorChanged 5 element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    aput-object v2, v0, v1

    .line 189
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "glucose_sensor_state"

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v5

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "glucose_error_detail"

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v4

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "glucose"

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "glucose_unit"

    const v2, 0x222e1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "sample_type"

    const v2, 0x15f91

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 196
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    .line 198
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    if-lt v0, v4, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->finalData:[Landroid/os/Bundle;

    invoke-interface {v0, v6, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 201
    iput v5, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    .line 205
    :cond_0
    return-void
.end method

.method public startReceivingData()I
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startReceivingData is called - bListenerRegistered : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    .line 130
    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    if-nez v2, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->registerBGListener()Z

    .line 133
    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TEST_FLAG:Z

    if-eqz v2, :cond_0

    .line 135
    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    .line 136
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    invoke-interface {v0, v5, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 137
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;-><init>(Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    .line 138
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->start()V

    .line 143
    :goto_0
    return v1

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    if-eqz v3, :cond_1

    move v0, v1

    :cond_1
    invoke-interface {v2, v5, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stopReceivingData is called - bListenerRegistered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;->interrupt()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler$DummyDataThread;

    .line 154
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->bListenerRegistered:Z

    .line 156
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->unregisterBGListener()V

    .line 157
    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->raw_data_cnt:I

    .line 158
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1, v3}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStopped(II)V

    .line 159
    return v3
.end method
