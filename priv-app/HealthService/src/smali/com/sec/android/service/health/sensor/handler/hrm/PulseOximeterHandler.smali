.class public Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
.super Ljava/lang/Object;
.source "PulseOximeterHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final CACHED_DATA_INTERVAL_CNT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TEST_FLAG:Z = false

.field private static final TYPE_BASE:I = 0x10000

.field public static final TYPE_HRM_BIO:I = 0x1001a


# instance fields
.field SpO2Listener:Landroid/hardware/SensorEventListener;

.field private bListenerRegistered:Z

.field private finalData:[Landroid/os/Bundle;

.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;

.field private mHeartRate:I

.field private mInterval:J

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mPulseOximetry:F

.field private mSNR:F

.field private mSNRUnit:I

.field private mSpO2Sensor:Landroid/hardware/Sensor;

.field private raw_data_cnt:I

.field private sensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    .line 27
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    .line 28
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSpO2Sensor:Landroid/hardware/Sensor;

    .line 29
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 30
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 33
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I

    .line 38
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J

    .line 39
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F

    .line 40
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F

    .line 41
    const v0, 0x4baf1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNRUnit:I

    .line 42
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Bundle;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;

    .line 266
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->SpO2Listener:Landroid/hardware/SensorEventListener;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I

    return p1
.end method

.method static synthetic access$108(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
    .param p1, "x1"    # J

    .prologue
    .line 21
    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
    .param p1, "x1"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNRUnit:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
    .param p1, "x1"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F

    return p1
.end method

.method private registerSpO2Listener()Z
    .locals 5

    .prologue
    .line 217
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    .line 220
    const/4 v0, 0x0

    .line 222
    .local v0, "rawHRMBio":Z
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1001a

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSpO2Sensor:Landroid/hardware/Sensor;

    .line 223
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->SpO2Listener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSpO2Sensor:Landroid/hardware/Sensor;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 224
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HRM sensor TYPE_HRM_BIO was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_1

    const-string v1, ""

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "registered"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    .line 227
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HRM sensor bListenerRegistered :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    .end local v0    # "rawHRMBio":Z
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    return v1

    .line 224
    .restart local v0    # "rawHRMBio":Z
    :cond_1
    const-string v1, "not "

    goto :goto_0
.end method

.method private unregisterSpO2Listener()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 235
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterSpO2Listener : current state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    .line 239
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    const-string v2, "unregisterSensorManager is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->SpO2Listener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 241
    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    .line 242
    const/4 v0, 0x1

    .line 246
    :cond_0
    return v0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->unregisterSpO2Listener()Z

    .line 129
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mContext:Landroid/content/Context;

    .line 254
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 255
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 256
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    .line 257
    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mContext:Landroid/content/Context;

    .line 120
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->sensorManager:Landroid/hardware/SensorManager;

    .line 121
    return-void
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;)V
    .locals 0
    .param p1, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 144
    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 0
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 134
    return-void
.end method

.method public startReceivingData()I
    .locals 4

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 156
    .local v0, "ret":I
    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->bListenerRegistered:Z

    if-nez v1, :cond_0

    .line 166
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->registerSpO2Listener()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    const/4 v0, 0x0

    .line 173
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v2, 0x2

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 178
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() is called. ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    return v0

    .line 169
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 4

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 199
    .local v0, "ret":I
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->unregisterSpO2Listener()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    const/4 v0, 0x0

    .line 205
    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v2, 0x2

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStopped(II)V

    .line 210
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopReceivingData() is called. ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    return v0

    .line 202
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
