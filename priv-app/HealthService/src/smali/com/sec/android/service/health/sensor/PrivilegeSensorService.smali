.class public Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
.super Landroid/app/Service;
.source "PrivilegeSensorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;
    }
.end annotation


# static fields
.field private static API_VERSION:I = 0x0

.field private static final DEBUGING_MODE:Z = true

.field private static final FEATURE_HEALTHCOVER:Ljava/lang/String; = "com.sec.feature.healthcover"

.field private static final HANDLER_BULK_DATA_RECEIVED:I = 0x68

.field private static final HANDLER_DATA_RECEIVED:I = 0x66

.field private static final HANDLER_DATA_STARTED:I = 0x65

.field private static final HANDLER_DATA_STOPED:I = 0x67

.field private static final TAG:Ljava/lang/String; = "[PrivHealthSensor]PrivilegeSensorService"

.field private static final TYPE_BASE:I = 0x10000

.field private static final TYPE_BLOOD_GLUCOSE:I = 0x1001f

.field private static final TYPE_BODY_TEMPERATURE:I = 0x1001e

.field private static final TYPE_HEART_RATE_MONITOR:I = 0x1001a

.field private static final VERSION_CODE:Ljava/lang/String; = "VERSION_CODE"

.field private static final VERSION_NAME:Ljava/lang/String; = "VERSION_NAME"

.field public static beginBroadcast:Ljava/lang/Object;

.field private static mCbHandler:Landroid/os/Handler;

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mAm:Landroid/app/IActivityManager;

.field private final mBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;

.field public final mForegroundToken:Landroid/os/IBinder;

.field private mSDKVersion:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSensorListener:Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->API_VERSION:I

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    .line 383
    new-instance v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$2;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$2;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 47
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mForegroundToken:Landroid/os/IBinder;

    .line 48
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mAm:Landroid/app/IActivityManager;

    .line 64
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mSDKVersion:Landroid/util/SparseArray;

    .line 65
    new-instance v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;-><init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;

    .line 290
    new-instance v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;-><init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;

    return-void
.end method

.method static synthetic access$100(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Landroid/os/RemoteCallbackList;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->isFeatureEnabled(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->API_VERSION:I

    return v0
.end method

.method private static createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/IInterface;",
            ">(",
            "Landroid/os/RemoteCallbackList",
            "<TT;>;)",
            "Landroid/os/RemoteCallbackList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 171
    .local p0, "originalCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<TT;>;"
    if-eqz p0, :cond_1

    .line 173
    new-instance v2, Landroid/os/RemoteCallbackList;

    invoke-direct {v2}, Landroid/os/RemoteCallbackList;-><init>()V

    .line 175
    .local v2, "newRemoteCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<TT;>;"
    sget-object v4, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v4

    .line 177
    :try_start_0
    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 178
    .local v0, "callbackCount":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 180
    invoke-virtual {p0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    invoke-virtual {p0, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 178
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 182
    :cond_0
    invoke-virtual {p0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 183
    monitor-exit v4

    .line 189
    .end local v0    # "callbackCount":I
    .end local v1    # "index":I
    .end local v2    # "newRemoteCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<TT;>;"
    :goto_1
    return-object v2

    .line 183
    .restart local v2    # "newRemoteCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<TT;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 188
    .end local v2    # "newRemoteCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<TT;>;"
    :cond_1
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "[SensorListener] createRemoteCallbackListCopy() originalCallbackList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 589
    const/4 v0, 0x0

    .line 591
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    packed-switch p1, :pswitch_data_0

    .line 601
    :goto_0
    return-object v0

    .line 595
    :pswitch_0
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    goto :goto_0

    .line 591
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private initializeSensorManager(Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;

    .prologue
    .line 657
    const/4 v1, 0x1

    .local v1, "index":I
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 659
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v0

    .line 660
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    sget-object v2, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    .line 657
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 662
    .end local v0    # "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    :cond_0
    return-void
.end method

.method private isFeatureEnabled(II)Z
    .locals 7
    .param p1, "type"    # I
    .param p2, "dataType"    # I

    .prologue
    const v6, 0x1001a

    .line 606
    sget-object v4, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    .line 608
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "SDK is not properly initialized"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 611
    :cond_0
    const/4 v0, 0x0

    .line 613
    .local v0, "bEnabled":Z
    packed-switch p1, :pswitch_data_0

    move v1, v0

    .line 651
    .end local v0    # "bEnabled":Z
    .local v1, "bEnabled":I
    :goto_0
    return v1

    .line 619
    .end local v1    # "bEnabled":I
    .restart local v0    # "bEnabled":Z
    :pswitch_0
    sget-object v4, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    const-string v5, "sensor"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    .line 620
    .local v2, "mSensorManager":Landroid/hardware/SensorManager;
    const/4 v4, 0x1

    if-eq p2, v4, :cond_1

    if-nez p2, :cond_3

    :cond_1
    invoke-virtual {v2, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 622
    const/4 v0, 0x1

    :cond_2
    :goto_1
    move v1, v0

    .line 644
    .restart local v1    # "bEnabled":I
    goto :goto_0

    .line 624
    .end local v1    # "bEnabled":I
    :cond_3
    const/4 v4, 0x2

    if-ne p2, v4, :cond_4

    invoke-virtual {v2, v6}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 626
    const/4 v0, 0x1

    goto :goto_1

    .line 628
    :cond_4
    const/4 v4, 0x3

    if-ne p2, v4, :cond_5

    const v4, 0x1001f

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 630
    const/4 v0, 0x1

    goto :goto_1

    .line 632
    :cond_5
    const/4 v4, 0x4

    if-ne p2, v4, :cond_6

    .line 634
    sget-object v4, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 635
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v3, :cond_2

    .line 637
    const-string v4, "com.sec.feature.healthcover"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 640
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_6
    const/4 v4, 0x7

    if-ne p2, v4, :cond_2

    const v4, 0x1001e

    invoke-virtual {v2, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 642
    const/4 v0, 0x1

    goto :goto_1

    .line 613
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setProcessSDKVersion(IILjava/lang/String;)V
    .locals 4
    .param p1, "processId"    # I
    .param p2, "versionCode"    # I
    .param p3, "versionName"    # Ljava/lang/String;

    .prologue
    .line 558
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mSDKVersion:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 585
    :goto_0
    return-void

    .line 561
    :cond_0
    const/4 v0, 0x0

    .line 563
    .local v0, "version":Ljava/lang/String;
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2

    .line 565
    const-string v0, "#"

    .line 572
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 574
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    .line 576
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 583
    :goto_2
    const-string v1, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setProcessSDKVersion version : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mSDKVersion:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 569
    :cond_2
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 580
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public deinitializeSensorManager()V
    .locals 4

    .prologue
    .line 666
    const-string v2, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v3, "deinitialize()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    const/4 v1, 0x1

    .local v1, "index":I
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 670
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v0

    .line 671
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->deinitialize()V

    .line 668
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 673
    .end local v0    # "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    const/4 v0, -0x1

    .line 137
    .local v0, "version_code":I
    const/4 v1, 0x0

    .line 139
    .local v1, "version_name":Ljava/lang/String;
    const-string v2, "VERSION_CODE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    const-string v2, "VERSION_CODE"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 144
    :cond_0
    const-string v2, "VERSION_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 146
    const-string v2, "VERSION_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    :cond_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-direct {p0, v2, v0, v1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->setProcessSDKVersion(IILjava/lang/String;)V

    .line 151
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;

    return-object v2
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 74
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "[SensorService] onCreate() DEBUGING_MODE = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    sput-object p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    .line 77
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mSensorListener:Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;

    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->initializeSensorManager(Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;)V

    .line 92
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 111
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "[SensorService] onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->deinitializeSensorManager()V

    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;

    .line 130
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 131
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 97
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SensorService] onStartCommand() - flags : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "This device is not SAMSUNG device. finish"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 105
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 157
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "onUnbind called"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
