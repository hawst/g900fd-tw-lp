.class Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;
.super Ljava/lang/Thread;
.source "PulseOximeterHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DummyDataThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 52
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 58
    .local v1, "r":Ljava/util/Random;
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Generating dummy data"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 60
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    if-ge v2, v6, :cond_0

    .line 81
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    aput-object v4, v2, v3

    .line 82
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "heart_rate"

    const/16 v4, 0x64

    invoke-virtual {v1, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "heart_rate_interval"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "heart_rate_snr"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 86
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "heart_rate_unit"

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNRUnit:I
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$500(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v3

    aget-object v2, v2, v3

    const-string v3, "pulse_oximetry"

    const/16 v4, 0x64

    invoke-virtual {v1, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # operator++ for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$108(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    .line 92
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    if-gez v2, :cond_1

    .line 93
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Sensor] state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 96
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[Sensor] Valid H_R_S_P_O_2 is received"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_2
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SpO2Listener => heartrate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mHeartRate:I
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$600(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " interval = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mInterval:J
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " SNR = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mSNR:F
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$400(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mPulseOximetry = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mPulseOximetry:F
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$700(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 102
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->finalData:[Landroid/os/Bundle;
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;)[Landroid/os/Bundle;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 103
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler$DummyDataThread;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->raw_data_cnt:I
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$102(Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;I)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 111
    .local v0, "ex":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Sending of dummy data is interrupted"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 114
    return-void
.end method
