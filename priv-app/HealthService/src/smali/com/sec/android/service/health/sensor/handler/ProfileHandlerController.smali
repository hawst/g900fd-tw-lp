.class public Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
.super Ljava/lang/Object;
.source "ProfileHandlerController.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;


# static fields
.field private static final STATE_DATA_AND_REQUEST_PROCESSING:I = 0x3

.field private static final STATE_DATA_RECEIVING:I = 0x1

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_REQUEST_PROCESSING:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

.field private listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

.field private mDataRecord:Z

.field private mDataType:I

.field private mProcessId:I

.field mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private mState:I

.field private mcontrollingDevObjId:Ljava/lang/Integer;

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PrivHealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;)V
    .locals 4
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "handler"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    .param p3, "listener"    # Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    .line 30
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 33
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    .line 34
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    .line 41
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 42
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    .line 43
    iput v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    .line 45
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    .line 341
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    .line 49
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    .line 50
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 51
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .line 52
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getRemoteEventCallBackList(Ljava/lang/String;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method private getSensorDataListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    .locals 8
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "devId"    # I

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getRemoteEventCallBackList(Ljava/lang/String;)Landroid/os/RemoteCallbackList;

    move-result-object v5

    .line 192
    .local v5, "remoteCallBackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const/4 v3, 0x0

    .line 193
    .local v3, "listener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-nez v5, :cond_0

    .line 194
    const/4 v6, 0x0

    .line 214
    :goto_0
    return-object v6

    .line 196
    :cond_0
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 198
    :try_start_0
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    .line 199
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 201
    invoke-virtual {v5, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v4

    .line 202
    .local v4, "obj":Ljava/lang/Object;
    if-nez v4, :cond_2

    .line 199
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 205
    :cond_2
    move-object v0, v4

    check-cast v0, Ljava/lang/Integer;

    move-object v2, v0

    .line 206
    .local v2, "integer":Ljava/lang/Integer;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-ne v6, p2, :cond_1

    .line 208
    invoke-virtual {v5, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-object v3, v0

    .line 212
    .end local v2    # "integer":Ljava/lang/Integer;
    .end local v4    # "obj":Ljava/lang/Object;
    :cond_3
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 213
    monitor-exit v7

    move-object v6, v3

    .line 214
    goto :goto_0

    .line 213
    .end local v1    # "i":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method


# virtual methods
.method public addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)V
    .locals 5
    .param p1, "devObjId"    # Ljava/lang/Integer;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    .line 161
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDataListener() devObjId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addingDataListener() binderId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addSensorEventListener deviceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .line 169
    .local v1, "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-nez v1, :cond_0

    .line 171
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addSensorEventListener RemotecallBackListAdded created with device : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    new-instance v1, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .end local v1    # "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;-><init>(Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V

    .line 177
    .restart local v1    # "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getSensorDataListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-result-object v0

    .line 178
    .local v0, "dataListener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-eqz v0, :cond_1

    .line 180
    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 182
    :cond_1
    invoke-virtual {v1, p2, p1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 183
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDataListener :  Registered listner Count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    return-void
.end method

.method public deinitialize()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->deinitialize()V

    .line 64
    :cond_0
    return-void
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v0, p1, p0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I

    .line 58
    :cond_0
    return-void
.end method

.method public onDataReceived(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 266
    monitor-enter p0

    .line 268
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 270
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;->onDataReceived(Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 273
    :cond_0
    monitor-exit p0

    .line 274
    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataReceived(I[Landroid/os/Bundle;)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "data"    # [Landroid/os/Bundle;

    .prologue
    .line 279
    monitor-enter p0

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 283
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v5, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;->onDataReceived([Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 286
    :cond_0
    monitor-exit p0

    .line 287
    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataStarted(II)V
    .locals 7
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 292
    monitor-enter p0

    .line 294
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 296
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStarted() dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    .line 299
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    .line 300
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v6, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    move v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;->onDataStarted(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 302
    :cond_0
    monitor-exit p0

    .line 303
    return-void

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDataStopped(II)V
    .locals 8
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 308
    monitor-enter p0

    .line 310
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    if-eqz v0, :cond_0

    .line 312
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStopped() dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 316
    .local v7, "tempControl":Ljava/lang/Integer;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 317
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 319
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    .line 320
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "onDataStopped() this.mState is changed to STATE_REQUEST_PROCESSING"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/RemoteCallbackList;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v6, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    move v1, p1

    move v2, p2

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;->onDataStopped(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 331
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    .line 332
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    .line 333
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    .line 335
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 338
    .end local v7    # "tempControl":Ljava/lang/Integer;
    :cond_0
    monitor-exit p0

    .line 339
    return-void

    .line 324
    .restart local v7    # "tempControl":Ljava/lang/Integer;
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    .line 325
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "onDataStopped() this.mState is changed to STATE_NONE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 338
    .end local v7    # "tempControl":Ljava/lang/Integer;
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)I
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x1

    .line 68
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() device Id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 72
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() listener || devId is NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :goto_0
    monitor-exit p0

    return v0

    .line 76
    :cond_1
    :try_start_1
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v0, :cond_2

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-ne v1, v4, :cond_3

    .line 78
    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() Device is BUSY, startRecivingData has already been called - this.mState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 68
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 82
    :cond_3
    :try_start_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getSensorDataListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 84
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() Alreay Present for the ProcessID"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_4
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v1, :cond_7

    .line 90
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() exerciseID Invoking Handler\'s start Receving Data"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)V

    .line 93
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 94
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->setObjectId(I)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->startReceivingData()I

    move-result v0

    .line 97
    .local v0, "error":I
    if-nez v0, :cond_6

    .line 99
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() Invoking Handler\'s Success"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-nez v1, :cond_5

    .line 102
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    .line 103
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() this.mState is changed to STATE_DATA_RECEIVING"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 107
    :cond_5
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    .line 108
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() this.mState is changed to STATE_DATA_AND_REQUEST_PROCESSING"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 113
    :cond_6
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 114
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    .line 115
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 122
    .end local v0    # "error":I
    :cond_7
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() exerciseID returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public removeDataListener(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 9
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "devId"    # Ljava/lang/Integer;

    .prologue
    .line 224
    sget-object v6, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeDataListener() processId : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    iget-object v6, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .line 227
    .local v5, "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const/4 v3, 0x0

    .line 229
    .local v3, "listener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-nez v5, :cond_1

    .line 231
    sget-object v6, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v7, "removeSensorEventListener :  remoteCallBackList instance is NULL"

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 237
    :try_start_0
    invoke-virtual {v5}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->beginBroadcast()I

    .line 238
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 240
    invoke-virtual {v5, v1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 241
    .local v2, "integer":Ljava/lang/Integer;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v6, v8, :cond_4

    .line 243
    invoke-virtual {v5, v1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-object v3, v0

    .line 247
    .end local v2    # "integer":Ljava/lang/Integer;
    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->finishBroadcast()V

    .line 248
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    const/4 v4, 0x0

    .line 250
    .local v4, "listenerCount":I
    if-eqz v3, :cond_3

    .line 252
    invoke-virtual {v5, v3}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 253
    invoke-virtual {v5}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    .line 254
    sget-object v6, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeSensorEventListener :  Count:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_3
    if-nez v4, :cond_0

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    goto :goto_0

    .line 238
    .end local v4    # "listenerCount":I
    .restart local v2    # "integer":Ljava/lang/Integer;
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 248
    .end local v1    # "i":I
    .end local v2    # "integer":Ljava/lang/Integer;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public declared-synchronized unregisterListener(Ljava/lang/Integer;)I
    .locals 4
    .param p1, "devObjectId"    # Ljava/lang/Integer;

    .prologue
    const/4 v0, 0x1

    .line 128
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopReceivingData() devObjectId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    if-nez p1, :cond_0

    .line 132
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "stopReceivingData() processId is NULL so returning "

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit p0

    return v0

    .line 136
    :cond_0
    :try_start_1
    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    if-eq v1, v0, :cond_1

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 138
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopReceivingData() is Not BUSY, so stopRecivingData should not be called - this.mState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 142
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 144
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_3

    .line 146
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "stopReceivingData() Invoking Handler\'s stop Receving Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->stopReceivingData()I

    move-result v0

    goto :goto_0

    .line 152
    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "stopReceivingData() processId not Equal to controlling processId"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    :cond_3
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "stopReceivingData() returning ERROR_FAILURE"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    const/4 v0, 0x0

    goto :goto_0
.end method
