.class Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;
.super Ljava/lang/Thread;
.source "BodyTemperatureHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->startReceivingData()I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 75
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    const/4 v2, 0x1

    new-array v1, v2, [Landroid/os/Bundle;

    .line 83
    .local v1, "testData":[Landroid/os/Bundle;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    aput-object v2, v1, v6

    .line 84
    aget-object v2, v1, v6

    const-string v3, "temperature"

    const/high16 v4, 0x42200000    # 40.0f

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 85
    aget-object v2, v1, v6

    const-string v3, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 86
    aget-object v2, v1, v6

    const-string v3, "temperature_unit"

    const v4, 0x27101

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    aget-object v2, v1, v6

    const-string v3, "temperature_type"

    const v4, 0x11171

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v3, 0x7

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 90
    return-void

    .line 77
    .end local v1    # "testData":[Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 79
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
