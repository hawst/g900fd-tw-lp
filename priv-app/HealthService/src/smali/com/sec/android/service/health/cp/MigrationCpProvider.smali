.class public Lcom/sec/android/service/health/cp/MigrationCpProvider;
.super Landroid/content/ContentProvider;
.source "MigrationCpProvider.java"


# static fields
.field private static final PLATFORM_DB_FILE_NAME:Ljava/lang/String; = "platform.db"

.field private static final PLATFORM_DB_FILE_NAME_COPY:Ljava/lang/String; = "platform_db.old"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private checkIfPlatformDbExists()Z
    .locals 5

    .prologue
    .line 176
    sget-object v2, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v3, "[+] checkIfPlatformDbExists"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const/4 v1, 0x0

    .line 178
    .local v1, "isExists":Z
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "platform.db"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 179
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 180
    sget-object v2, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[-] checkIfPlatformDbExists : isExists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    return v1
.end method

.method private getCallerPackage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 198
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 199
    .local v0, "caller":I
    if-nez v0, :cond_0

    .line 201
    const/4 v2, 0x0

    .line 204
    :goto_0
    return-object v2

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "pkgName":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    goto :goto_0
.end method

.method private getOldHealthServiceDBPassword()[B
    .locals 4

    .prologue
    .line 235
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getKeyFromTrustZone(Landroid/content/Context;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 242
    .local v1, "password":[B
    :goto_0
    return-object v1

    .line 237
    .end local v1    # "password":[B
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 239
    sget-object v2, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v3, "Error Meesage while retrieving the old TrustZone passward from HealthService..\n Trying with AES now..."

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-static {}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getKeyFromAES()[B

    move-result-object v1

    .line 242
    .restart local v1    # "password":[B
    goto :goto_0
.end method

.method private matchSharedPrefUri(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_SHARED_PREFS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x1

    .line 221
    :goto_0
    return v0

    .line 220
    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Returning false from the matchSharedPrefUri method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private matchUri(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 210
    invoke-virtual {p1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    .line 213
    :goto_0
    return v0

    .line 212
    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Returning false from the MatchUri method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private matchUserProfilePrefUri(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_USER_PROFILE_PREFS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 229
    :goto_0
    return v0

    .line 228
    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Returning false from the matchSharedPrefUri method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private renamePlatformDb()Z
    .locals 6

    .prologue
    .line 184
    sget-object v4, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v5, "[+] renamePlatformDb"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v3, 0x0

    .line 186
    .local v3, "result":Z
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "platform.db"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 187
    .local v0, "dbFile":Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "platform_db.old"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 188
    .local v1, "dbFileRename":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    .line 189
    .local v2, "isExists":Z
    if-eqz v2, :cond_0

    .line 190
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    .line 192
    :cond_0
    sget-object v4, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v5, "[-] renamePlatformDb"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    return v3
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 143
    sget-object v3, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MigrationCp Call : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 146
    .local v1, "result":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 148
    const-string v3, "get_secure_password"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 151
    const-string v3, "value_of_password"

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getOldHealthServiceDBPassword()[B

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 171
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[-] MigrationCp Call : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-object v1

    .line 153
    :cond_1
    const-string v3, "check_platform_db_exists"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 155
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->checkIfPlatformDbExists()Z

    move-result v0

    .line 156
    .local v0, "isPlatformDbExists":Z
    const-string v3, "value_of_check_if_platform_db_exists"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 157
    .end local v0    # "isPlatformDbExists":Z
    :cond_2
    const-string v3, "rename_platform_db_if_exists"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 158
    const/4 v2, 0x0

    .line 159
    .local v2, "resultStatus":Z
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->checkIfPlatformDbExists()Z

    move-result v0

    .line 160
    .restart local v0    # "isPlatformDbExists":Z
    if-eqz v0, :cond_3

    .line 161
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->renamePlatformDb()Z

    move-result v2

    .line 163
    :cond_3
    const-string v3, "value_of_rename_platform_db_if_exists"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 168
    .end local v0    # "isPlatformDbExists":Z
    .end local v2    # "resultStatus":Z
    :cond_4
    sget-object v3, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OpenFile API not Availiable for the caller\'s signature, caller package : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    new-instance v3, Ljava/lang/SecurityException;

    const-string v4, "OpenFile  API is not Availiable for your app"

    invoke-direct {v3, v4}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In Delete"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In getType"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In Insert module"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In Create module"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    const/high16 v11, 0x10000000

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->matchUri(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 73
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    const/4 v7, 0x3

    if-ge v0, v7, :cond_0

    .line 75
    new-instance v6, Lcom/sec/android/securestorage/SecureStorage;

    invoke-direct {v6}, Lcom/sec/android/securestorage/SecureStorage;-><init>()V

    .line 76
    .local v6, "storage":Lcom/sec/android/securestorage/SecureStorage;
    const/4 v4, 0x0

    .line 79
    .local v4, "passwd":[B
    :try_start_0
    sget-object v7, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Try to get K old SS key, count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const-string v7, "com.sec.android.service.health"

    invoke-virtual {v6, v7}, Lcom/sec/android/securestorage/SecureStorage;->getByteArray(Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 98
    sget-object v7, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v8, "Success to get old SS key"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :try_start_1
    const-string v7, "com.sec.android.app.shealth"

    invoke-virtual {v6, v7, v4}, Lcom/sec/android/securestorage/SecureStorage;->put(Ljava/lang/String;[B)Z

    .line 102
    sget-object v7, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v8, "Success to put old SS key to new storage"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_1 .. :try_end_1} :catch_2

    .line 114
    .end local v4    # "passwd":[B
    .end local v6    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    :cond_0
    new-instance v5, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "databases"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "platform.db"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v5, "platform_db":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    .line 116
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7

    .line 82
    .end local v5    # "platform_db":Ljava/io/File;
    .restart local v4    # "passwd":[B
    .restart local v6    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    :catch_0
    move-exception v2

    .line 84
    .local v2, "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    monitor-enter v8

    .line 88
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    const-wide/16 v9, 0xc8

    invoke-virtual {v7, v9, v10}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :goto_1
    :try_start_3
    monitor-exit v8

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 90
    :catch_1
    move-exception v3

    .line 92
    .local v3, "e1":Ljava/lang/InterruptedException;
    sget-object v9, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v7

    :goto_2
    invoke-static {v9, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 94
    .end local v3    # "e1":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7

    .line 92
    .restart local v3    # "e1":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_4
    const-string v7, " "
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 105
    .end local v2    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v3    # "e1":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v2

    .line 107
    .restart local v2    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    sget-object v8, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Failed to put old key : "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v7, ""

    :goto_3
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SecureStorage Save error : "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_3

    const-string v7, ""

    :goto_4
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 107
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 108
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    .line 117
    .end local v2    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v4    # "passwd":[B
    .end local v6    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    .restart local v5    # "platform_db":Ljava/io/File;
    :cond_4
    invoke-static {v5, v11}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    .line 131
    .end local v0    # "count":I
    .end local v5    # "platform_db":Ljava/io/File;
    :goto_5
    return-object v7

    .line 119
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->matchSharedPrefUri(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 121
    new-instance v1, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "shared_prefs"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "com.sec.android.service.health_preferences.xml"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    .local v1, "defaultPrefs":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_6

    .line 123
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7

    .line 124
    :cond_6
    invoke-static {v1, v11}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    goto :goto_5

    .line 126
    .end local v1    # "defaultPrefs":Ljava/io/File;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-direct {p0, p1}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->matchUserProfilePrefUri(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 128
    new-instance v1, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "shared_prefs"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "user_profile_prefs.xml"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .restart local v1    # "defaultPrefs":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_8

    .line 130
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7

    .line 131
    :cond_8
    invoke-static {v1, v11}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v7

    goto/16 :goto_5

    .line 135
    .end local v1    # "defaultPrefs":Ljava/io/File;
    :cond_9
    sget-object v7, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OpenFile API not Availiable for the caller\'s signature, caller package : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/MigrationCpProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v7, Ljava/lang/SecurityException;

    const-string v8, "OpenFile  API is not Availiable for your app"

    invoke-direct {v7, v8}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In Query module"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/service/health/cp/MigrationCpProvider;->TAG:Ljava/lang/String;

    const-string v1, "In Update API"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v0, 0x0

    return v0
.end method
