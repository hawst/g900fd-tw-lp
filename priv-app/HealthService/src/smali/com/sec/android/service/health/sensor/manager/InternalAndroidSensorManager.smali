.class public Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
.super Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
.source "InternalAndroidSensorManager.java"


# static fields
.field private static final FEATURE_HEALTHCOVER:Ljava/lang/String; = "com.sec.feature.healthcover"

.field private static final TAG:Ljava/lang/String;

.field private static sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;


# instance fields
.field private mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mHandlerMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PrivHealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 50
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 51
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 53
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 55
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    .line 62
    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-object v0
.end method


# virtual methods
.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 2
    .param p1, "filter"    # Lcom/sec/android/service/health/sensor/manager/util/Filter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v0, "device":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 134
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 139
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 144
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_4
    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    .prologue
    .line 71
    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    .line 72
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/hardware/SensorManager;

    .line 74
    .local v9, "mSensorManager":Landroid/hardware/SensorManager;
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    if-eqz v9, :cond_0

    const v0, 0x10019

    invoke-virtual {v9, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "android_id"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "android_id":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v2, "INTERNAL-HRM"

    const/4 v3, 0x1

    const/16 v4, 0x2718

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 82
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const/4 v2, 0x1

    const-class v3, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 85
    .end local v1    # "android_id":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_1

    if-eqz v9, :cond_1

    const v0, 0x1001a

    invoke-virtual {v9, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 87
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v3, "PULSEOXIMETER"

    const-string v4, "Samsung PulseOximeter"

    const/4 v5, 0x1

    const/16 v6, 0x272a

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mPulseOximeterDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 88
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const/4 v2, 0x2

    const-class v3, Lcom/sec/android/service/health/sensor/handler/hrm/PulseOximeterHandler;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_2

    if-eqz v9, :cond_2

    const v0, 0x1001f

    invoke-virtual {v9, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 93
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v3, "BLOODGLUCOSE"

    const-string v4, "BLOODGLUCOSE"

    const/4 v5, 0x1

    const/16 v6, 0x2714

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBGDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 94
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const/4 v2, 0x3

    const-class v3, Lcom/sec/android/service/health/sensor/handler/bloodglucose/InternalBGHandler;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_3

    .line 99
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 100
    .local v10, "pkgMnr":Landroid/content/pm/PackageManager;
    if-eqz v10, :cond_3

    .line 102
    const-string v0, "com.sec.feature.healthcover"

    invoke-virtual {v10, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 104
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v2, "ECG - Device is initialized"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v3, "ECG"

    const-string v4, "ECG"

    const/4 v5, 0x1

    const/16 v6, 0x2729

    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 106
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const/4 v2, 0x4

    const-class v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 110
    .end local v10    # "pkgMnr":Landroid/content/pm/PackageManager;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_4

    if-eqz v9, :cond_4

    const v0, 0x1001e

    invoke-virtual {v9, v0}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 112
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v3, "BODYTEMPERATURE"

    const-string v4, "BODYTEMPERATURE"

    const/4 v5, 0x1

    const/16 v6, 0x2716

    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mBodyTemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 113
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const/4 v2, 0x7

    const-class v3, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 115
    :cond_4
    return-void
.end method

.method public isDataTypeSupported(I)Z
    .locals 1
    .param p1, "dataType"    # I

    .prologue
    .line 153
    packed-switch p1, :pswitch_data_0

    .line 164
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 161
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)I
    .locals 8
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v3

    .line 173
    .local v3, "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    if-nez v3, :cond_1

    .line 175
    const/4 v4, 0x0

    .line 178
    .local v4, "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v5

    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 180
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v2, :cond_0

    .line 181
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 193
    .end local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    if-eqz v4, :cond_2

    .line 195
    new-instance v3, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .end local v3    # "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    invoke-direct {v3, p1, v4, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;)V

    .line 196
    .restart local v3    # "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    iget-object v5, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    .line 197
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v3}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    .line 207
    .end local v4    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :cond_1
    :goto_1
    if-nez v3, :cond_3

    const/4 v5, 0x1

    :goto_2
    return v5

    .line 183
    .restart local v4    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :catch_0
    move-exception v1

    .line 185
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 187
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 202
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_2
    sget-object v5, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v6, "createDeviceHandler: profileHandlerInterfaced is null !!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 207
    .end local v4    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :cond_3
    invoke-virtual {v3, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)I

    move-result v5

    goto :goto_2
.end method

.method public unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)I
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;

    .prologue
    .line 213
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    .line 215
    .local v0, "profileController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->unregisterListener(Ljava/lang/Integer;)I

    move-result v1

    .line 220
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
