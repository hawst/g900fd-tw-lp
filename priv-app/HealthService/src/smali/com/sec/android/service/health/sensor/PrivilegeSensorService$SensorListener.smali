.class final Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;
.super Ljava/lang/Object;
.source "PrivilegeSensorService.java"

# interfaces
.implements Lcom/sec/android/service/health/sensor/manager/ISensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
    .param p2, "x1"    # Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;

    .prologue
    .line 193
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$SensorListener;-><init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;)V

    return-void
.end method


# virtual methods
.method public onDataReceived(Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V
    .locals 7
    .param p1, "extra"    # Landroid/os/Bundle;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p2, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SensorListener] onDataReceived() deviceID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " processId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deviceObjId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Extra: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    if-eqz p2, :cond_0

    .line 203
    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$100(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    .line 204
    .local v2, "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x66

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v3, v6, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 206
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 207
    .local v0, "finalData":Landroid/os/Bundle;
    const-string v3, "_Extra"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 208
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 209
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 215
    .end local v0    # "finalData":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :goto_0
    return-void

    .line 213
    :cond_0
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataReceived callbackList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataReceived([Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V
    .locals 7
    .param p1, "extra"    # [Landroid/os/Bundle;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p2, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SensorListener] onDataReceived() deviceID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " processId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deviceObjId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Extra: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    if-eqz p2, :cond_0

    .line 224
    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    invoke-static {p2}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$100(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    .line 225
    .local v2, "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x68

    invoke-virtual {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v3, v6, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 227
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 228
    .local v0, "finalData":Landroid/os/Bundle;
    const-string v3, "_Extra"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 229
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 230
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 236
    .end local v0    # "finalData":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :goto_0
    return-void

    .line 234
    :cond_0
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataReceived callbackList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataStarted(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I
    .param p4, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p5, "deviceObjectId"    # I
    .param p6, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p3, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SensorListener] onDataStarted() dataType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " processId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " DevObjectId :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    if-nez p4, :cond_0

    .line 245
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataStarted device is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :goto_0
    return-void

    .line 249
    :cond_0
    if-eqz p3, :cond_1

    .line 251
    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$100(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    .line 252
    .local v2, "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x65

    invoke-virtual {v3, v4, p1, p2, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 253
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 254
    .local v0, "b":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 255
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 259
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :cond_1
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataStarted callbackList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDataStopped(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I
    .param p4, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p5, "deviceObjectId"    # I
    .param p6, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 266
    .local p3, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SensorListener] onDataStopped() dataType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " processId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " DevObjectId :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    if-nez p4, :cond_0

    .line 270
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataStopped device is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :goto_0
    return-void

    .line 274
    :cond_0
    if-eqz p3, :cond_1

    .line 276
    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->createRemoteCallbackListCopy(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;
    invoke-static {p3}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$100(Landroid/os/RemoteCallbackList;)Landroid/os/RemoteCallbackList;

    move-result-object v2

    .line 277
    .local v2, "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x67

    invoke-virtual {v3, v4, p1, p2, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 278
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 279
    .local v0, "b":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 280
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mCbHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$200()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 284
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    .end local v2    # "tempCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :cond_1
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "onDataStopped callbackList is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
