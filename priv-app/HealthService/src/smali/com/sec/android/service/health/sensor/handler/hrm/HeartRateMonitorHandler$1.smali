.class Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;
.super Ljava/lang/Object;
.source "HeartRateMonitorHandler.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 233
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v6, 0x1

    .line 238
    if-eqz p1, :cond_0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const v3, 0x1001a

    if-ne v2, v3, :cond_0

    .line 245
    const-string v2, "HRM Sensor"

    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v6, :cond_0

    .line 247
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HeartRateMonitor onSensorChanged : event.accuracy = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/hardware/SensorEvent;->accuracy:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v3, v3

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;I)I

    .line 249
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    float-to-long v3, v3

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J
    invoke-static {v2, v3, v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$102(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;J)J

    .line 250
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    # setter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$202(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;F)F

    .line 253
    const/4 v1, 0x0

    .line 254
    .local v1, "state":I
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 270
    :goto_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v2, :cond_2

    const/16 v2, 0x2710

    if-gt v2, v1, :cond_2

    const/16 v2, 0x2713

    if-gt v1, v2, :cond_2

    .line 272
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HRMListener => state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_2
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v2

    if-gez v2, :cond_3

    .line 278
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Sensor] state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_3
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v2

    if-lez v2, :cond_4

    .line 281
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "[Sensor] Valid H___R is received"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_4
    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HRMListener => heartrate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " interval = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " SNR = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F
    invoke-static {v4}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v2, :cond_0

    .line 286
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 287
    .local v0, "finalData":Landroid/os/Bundle;
    const-string v2, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 288
    const-string v2, "heart_rate"

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 289
    const-string v2, "heart_rate_interval"

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$100(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 290
    const-string v2, "heart_rate_snr"

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F
    invoke-static {v3}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 291
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;->this$0:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    invoke-interface {v2, v6, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 257
    .end local v0    # "finalData":Landroid/os/Bundle;
    :pswitch_0
    const/16 v1, 0x2710

    .line 258
    goto/16 :goto_1

    .line 260
    :pswitch_1
    const/16 v1, 0x2711

    .line 261
    goto/16 :goto_1

    .line 263
    :pswitch_2
    const/16 v1, 0x2712

    .line 264
    goto/16 :goto_1

    .line 266
    :pswitch_3
    const/16 v1, 0x2713

    goto/16 :goto_1

    .line 254
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
