.class public abstract Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
.super Ljava/lang/Object;
.source "BaseSensorManager.java"

# interfaces
.implements Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;


# static fields
.field protected static final TAG:Ljava/lang/String; = "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

.field private profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    .line 23
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mContext:Landroid/content/Context;

    .line 25
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method


# virtual methods
.method protected addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V
    .locals 3
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "profileHandler"    # Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .prologue
    .line 60
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addProfileHandlerController deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-void
.end method

.method public close(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .line 113
    .local v0, "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->removeDataListener(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_0
    return-void
.end method

.method public deinitialize()V
    .locals 4

    .prologue
    .line 44
    const-string v2, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    const-string v3, "deinitialize is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v2, :cond_1

    .line 48
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .line 50
    .local v1, "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->deinitialize()V

    goto :goto_0

    .line 53
    .end local v1    # "profileHandlerController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 56
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public abstract getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation
.end method

.method public getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    .prologue
    .line 38
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    .line 39
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public abstract isDataTypeSupported(I)Z
.end method

.method public onDataReceived(Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 3
    .param p1, "data"    # Landroid/os/Bundle;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "deviceObjectId"    # I
    .param p5, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "remoteCallBackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataReceived :  processId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v0, p1, p2, p3, p5}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataReceived(Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V

    .line 100
    :cond_0
    return-void
.end method

.method public onDataReceived([Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 3
    .param p1, "data"    # [Landroid/os/Bundle;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "deviceObjectId"    # I
    .param p5, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p2, "remoteCallBackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataReceived :  processId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    invoke-interface {v0, p1, p2, p3, p5}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataReceived([Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V

    .line 108
    :cond_0
    return-void
.end method

.method public onDataStarted(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 7
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I
    .param p4, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p5, "devId"    # I
    .param p6, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p3, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStarted dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataStarted(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 84
    :cond_0
    return-void
.end method

.method public onDataStopped(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .locals 7
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I
    .param p4, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p5, "devId"    # I
    .param p6, "processId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p3, "callbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStopped dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " errorCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " processId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->mSensorListener:Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/service/health/sensor/manager/ISensorListener;->onDataStopped(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V

    .line 92
    :cond_0
    return-void
.end method

.method public abstract registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)I
.end method

.method protected removeProfileHandlerController(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 72
    const-string v0, "[PrivHealthSensor][PrivHealthSensor]AbstractSensorManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeProfileHandlerController deviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->profileHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    return-void
.end method

.method public abstract unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)I
.end method
