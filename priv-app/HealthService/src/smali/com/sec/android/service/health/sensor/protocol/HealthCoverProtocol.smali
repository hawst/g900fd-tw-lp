.class public Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
.super Ljava/lang/Object;
.source "HealthCoverProtocol.java"


# static fields
.field private static final DATA_STATUS_STARTED:I = 0x1

.field private static final DATA_STATUS_STOPPED:I = 0x0

.field private static final DEFAULT_WAIT_MS:I = 0x64

.field public static final NOT_ASSIGNED_INT:I = 0x7fffffff

.field private static final PROFILE_NAME:Ljava/lang/String; = "shap"

.field private static final PROTOCOL_NAME:Ljava/lang/String; = "HEALTHCOVER_PROTOCOL"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBundleEcg:Landroid/os/Bundle;

.field private mDataTranceiveStatus:I

.field private mDataTranceiveType:I

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

.field private mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

.field private mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

.field private mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

.field private mReceivedHeartRate:I

.field public mRespondList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;",
            ">;"
        }
    .end annotation
.end field

.field public mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

.field public mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

.field public mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

.field public mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

.field public mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 52
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .line 53
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 54
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 55
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .line 57
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    .line 59
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    .line 60
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    .line 61
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    .line 63
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 66
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 67
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-direct {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 74
    return-void
.end method

.method private getDeviceInformation()I
    .locals 5

    .prologue
    .line 296
    const/4 v1, 0x0

    .line 299
    .local v1, "nRtn":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    if-nez v2, :cond_0

    .line 300
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 301
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendProductInfoRequest(Z)V

    .line 303
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    if-nez v2, :cond_1

    .line 304
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 305
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendProductInfoRequest(Z)V

    .line 307
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-nez v2, :cond_2

    .line 308
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 309
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendRealtimeDataFormatRequest()V

    .line 311
    :cond_2
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    if-nez v2, :cond_3

    .line 312
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 313
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendExtraTypeListRequestMessage()V

    .line 316
    :cond_3
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 318
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_5

    .line 321
    :cond_4
    const/4 v1, 0x1

    .line 327
    :cond_5
    :goto_0
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeviceInformation nRtn : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    return v1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initShapMessageVariables()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 287
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v1, "initShapMessageVariables"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .line 289
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 290
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 291
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .line 292
    return-void
.end method

.method private makeEcgBundle(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;)Landroid/os/Bundle;
    .locals 3
    .param p1, "healthcover"    # Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;

    .prologue
    .line 515
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "makeEcgBundle"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 518
    .local v0, "bundleRtn":Landroid/os/Bundle;
    const-string v1, "SampleFreq"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getFs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 519
    const-string v1, "CoefA"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getCoefA()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 520
    const-string v1, "CoefB"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getCoefB()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 521
    return-object v0
.end method

.method public static printHex([B)V
    .locals 7
    .param p0, "arrData"    # [B

    .prologue
    .line 788
    const-string v1, ""

    .line 789
    .local v1, "strT":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 791
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %02x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aget-byte v6, p0, v0

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 792
    add-int/lit8 v2, v0, 0x1

    rem-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    .line 794
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    const-string v1, ""

    .line 789
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 799
    :cond_1
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    return-void
.end method

.method private processBiaRawData([I)I
    .locals 1
    .param p1, "arrRawData"    # [I

    .prologue
    .line 586
    const/4 v0, -0x1

    .line 624
    .local v0, "nRtn":I
    return v0
.end method

.method private processBodyCompositionAnalyzerMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I
    .locals 1
    .param p1, "shapMsgHeader"    # Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    .prologue
    .line 467
    const/4 v0, 0x0

    .line 469
    .local v0, "nRtn":I
    return v0
.end method

.method private processEcgRawData([I)I
    .locals 16
    .param p1, "arrRawData"    # [I

    .prologue
    .line 527
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->processEcgData([I)I

    move-result v10

    .line 528
    .local v10, "nRtn":I
    const/4 v11, 0x1

    if-eq v10, v11, :cond_0

    const/16 v11, 0xff

    if-ne v10, v11, :cond_4

    .line 529
    :cond_0
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 531
    .local v5, "ecgData":Landroid/os/Bundle;
    const/high16 v7, 0x3f800000    # 1.0f

    .line 532
    .local v7, "fCoefA":F
    move-object/from16 v0, p1

    array-length v9, v0

    .line 533
    .local v9, "nLength":I
    const/4 v6, 0x0

    .line 534
    .local v6, "fArrFiltered":[F
    if-lez v9, :cond_1

    new-array v6, v9, [F

    .line 535
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-virtual {v11}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getCoefA()F

    move-result v11

    const/high16 v12, 0x447a0000    # 1000.0f

    mul-float v7, v11, v12

    .line 538
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v9, :cond_2

    .line 545
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    iget-object v11, v11, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgHPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    aget v12, p1, v8

    int-to-double v12, v12

    invoke-virtual {v11, v12, v13}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->filter(D)D

    move-result-wide v3

    .line 546
    .local v3, "dbVal":D
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    iget-object v11, v11, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgLPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    invoke-virtual {v11, v3, v4}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->filter(D)D

    move-result-wide v11

    float-to-double v13, v7

    mul-double/2addr v11, v13

    double-to-float v11, v11

    aput v11, v6, v8

    .line 538
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 550
    .end local v3    # "dbVal":D
    :cond_2
    const-string v11, "ecg_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v5, v11, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 551
    const-string v11, "ecg_electrowave"

    invoke-virtual {v5, v11, v6}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 553
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    iget v11, v11, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mPacketCount:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-virtual {v12}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getFs()I

    move-result v12

    if-le v11, v12, :cond_4

    .line 555
    const-string v11, "ecg_heartrate"

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    invoke-virtual {v5, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 556
    const v11, 0x7fffffff

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 558
    const/16 v11, 0xff

    if-ne v10, v11, :cond_3

    .line 559
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-virtual {v11}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getMeanHR()D

    move-result-wide v1

    .line 560
    .local v1, "dbMeanHR":D
    const-wide/high16 v11, 0x4044000000000000L    # 40.0

    cmpl-double v11, v1, v11

    if-ltz v11, :cond_3

    const-wide v11, 0x4066800000000000L    # 180.0

    cmpg-double v11, v1, v11

    if-gtz v11, :cond_3

    .line 561
    sget-object v11, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v12, "MeanHR: %.1f"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const-string v11, "ecg_heartrate"

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    long-to-int v12, v12

    invoke-virtual {v5, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 565
    .end local v1    # "dbMeanHR":D
    :cond_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    .line 566
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    invoke-interface {v11, v5, v12}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataReceived(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 572
    .end local v5    # "ecgData":Landroid/os/Bundle;
    .end local v6    # "fArrFiltered":[F
    .end local v7    # "fCoefA":F
    .end local v8    # "i":I
    .end local v9    # "nLength":I
    :cond_4
    :goto_1
    const/16 v11, 0xff

    if-ne v10, v11, :cond_7

    .line 580
    :cond_5
    :goto_2
    return v10

    .line 568
    .restart local v5    # "ecgData":Landroid/os/Bundle;
    .restart local v6    # "fArrFiltered":[F
    .restart local v7    # "fCoefA":F
    .restart local v8    # "i":I
    .restart local v9    # "nLength":I
    :cond_6
    sget-object v11, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v12, "onDataStoppped is already called"

    invoke-static {v11, v12}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 574
    .end local v5    # "ecgData":Landroid/os/Bundle;
    .end local v6    # "fArrFiltered":[F
    .end local v7    # "fCoefA":F
    .end local v8    # "i":I
    .end local v9    # "nLength":I
    :cond_7
    const/4 v11, 0x2

    if-ne v10, v11, :cond_5

    goto :goto_2
.end method

.method private processHeartRateMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I
    .locals 10
    .param p1, "shapMsgHeader"    # Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 435
    const/4 v2, 0x0

    .line 436
    .local v2, "nRtn":I
    const/16 v4, 0x50

    iget-byte v5, p1, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v4, v5, :cond_2

    move-object v3, p1

    .line 437
    check-cast v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;

    .line 438
    .local v3, "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v5, "HR: %d, status: %02x"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-short v7, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v8

    iget-short v7, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mStatus:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    iget-short v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mStatus:S

    and-int/lit8 v4, v4, 0x10

    int-to-byte v4, v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_1

    .line 441
    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v5, "Non-contact detected..."

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 443
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v4, :cond_0

    .line 444
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    iget v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v4, v5, v9}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStopped(II)V

    .line 445
    iput v8, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 461
    .end local v3    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_0
    :goto_0
    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processHeartRateMessage nRtn : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    return v2

    .line 447
    .restart local v3    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_1
    iget-short v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    const/16 v5, 0x19

    if-lt v4, v5, :cond_0

    iget-short v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    const/16 v5, 0xe1

    if-gt v4, v5, :cond_0

    .line 449
    iget-short v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    iput v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 450
    const-wide v4, 0x40ed4c0000000000L    # 60000.0

    iget-short v6, v3, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    int-to-double v6, v6

    div-double v0, v4, v6

    .line 451
    .local v0, "dbRRI":D
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-virtual {v4, v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->addRRI(D)I

    goto :goto_0

    .line 455
    .end local v0    # "dbRRI":D
    .end local v3    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_2
    const/4 v4, 0x3

    iget-byte v5, p1, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v4, v5, :cond_3

    .line 456
    sget-object v4, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v5, "Heart Rate Monitor Settings Message...."

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 458
    :cond_3
    const/4 v2, 0x2

    goto :goto_0
.end method

.method private processMessage([B)I
    .locals 13
    .param p1, "arrData"    # [B

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 360
    .local v1, "bRetryCnt":Z
    const/4 v5, 0x0

    .line 361
    .local v5, "nRtn":I
    const/4 v0, 0x0

    .line 362
    .local v0, "arrMessage":[B
    const/4 v4, 0x0

    .line 365
    .local v4, "nReceiveBufSize":I
    if-eqz p1, :cond_0

    array-length v9, p1

    if-lez v9, :cond_0

    .line 366
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->addDataToBuffer([B)I

    move-result v4

    .line 372
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 373
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getReceivedBufferSize()I

    move-result v4

    .line 374
    const/16 v9, 0x14

    if-lt v4, v9, :cond_1

    .line 375
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getMessage()[B

    move-result-object v0

    .line 377
    :cond_1
    if-nez v0, :cond_2

    move v6, v5

    .line 429
    .end local v5    # "nRtn":I
    .local v6, "nRtn":I
    :goto_1
    return v6

    .line 380
    .end local v6    # "nRtn":I
    .restart local v5    # "nRtn":I
    :cond_2
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->parseCommonMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v7

    .line 381
    .local v7, "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    if-nez v7, :cond_4

    .line 383
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v10, "Device Specific Message..."

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v5, 0x2

    .line 385
    iget v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_3

    .line 386
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v7

    .line 387
    if-eqz v7, :cond_3

    .line 388
    invoke-direct {p0, v7}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processHeartRateMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I

    move-result v5

    .line 425
    :cond_3
    :goto_2
    if-eqz v1, :cond_c

    move v6, v5

    .line 429
    .end local v5    # "nRtn":I
    .restart local v6    # "nRtn":I
    goto :goto_1

    .line 396
    .end local v6    # "nRtn":I
    .restart local v5    # "nRtn":I
    :cond_4
    const/4 v9, 0x1

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_5

    move-object v9, v7

    .line 397
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    goto :goto_2

    .line 398
    :cond_5
    const/16 v9, 0x30

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_6

    move-object v9, v7

    .line 399
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 400
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->setDeviceInformation()I

    goto :goto_2

    .line 401
    :cond_6
    const/16 v9, 0x31

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_7

    move-object v9, v7

    .line 402
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    goto :goto_2

    .line 403
    :cond_7
    const/16 v9, 0xa

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_a

    move-object v2, v7

    .line 404
    check-cast v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;

    .line 405
    .local v2, "msgCmdResp":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/16 v10, 0xa

    if-lt v9, v10, :cond_8

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 406
    :cond_8
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    const-string v10, "Invoke ID: %02x, Respond:%s(%04x)"

    const/4 v9, 0x3

    new-array v11, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-byte v12, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mInvokeID:B

    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v12

    aput-object v12, v11, v9

    const/4 v12, 0x1

    iget-short v9, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mRespond:S

    if-nez v9, :cond_9

    const-string v9, "OK"

    :goto_3
    aput-object v9, v11, v12

    const/4 v9, 0x2

    iget-short v12, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mRespond:S

    invoke-static {v12}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 409
    .local v8, "strLog":Ljava/lang/String;
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v9, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 407
    .end local v8    # "strLog":Ljava/lang/String;
    :cond_9
    const-string v9, "Failed"

    goto :goto_3

    .line 410
    .end local v2    # "msgCmdResp":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;
    :cond_a
    const/16 v9, 0xc

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_b

    move-object v9, v7

    .line 411
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    goto :goto_2

    .line 412
    :cond_b
    const/16 v9, 0x35

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-eq v9, v10, :cond_3

    .line 414
    const/16 v9, 0x32

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_3

    move-object v3, v7

    .line 415
    check-cast v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;

    .line 416
    .local v3, "msgRawData":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processRawData(Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;)I

    move-result v5

    goto/16 :goto_2

    .line 426
    .end local v3    # "msgRawData":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;
    :cond_c
    const/4 v1, 0x1

    .line 427
    goto/16 :goto_0
.end method

.method private processRawData(Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;)I
    .locals 9
    .param p1, "msgRawData"    # Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 475
    const/4 v2, 0x0

    .line 476
    .local v2, "nErrorCode":I
    const/4 v0, 0x0

    .line 477
    .local v0, "arrRawData":[I
    const/4 v4, 0x0

    .line 478
    .local v4, "nRtn":I
    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-nez v7, :cond_0

    .line 510
    :goto_0
    return v6

    .line 479
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-short v7, p1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mNumberOfChannel:S

    if-ge v1, v7, :cond_3

    .line 481
    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    iget-object v8, p1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    aget-byte v8, v8, v1

    invoke-virtual {v7, v8}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->getChannelFormatById(I)Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    move-result-object v7

    iget-short v3, v7, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mType:S

    .line 482
    .local v3, "nRawType":S
    iget-object v7, p1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    aget-byte v7, v7, v1

    invoke-virtual {p1, v7}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->convertRawDataToInt32(I)[I

    move-result-object v0

    .line 483
    const/16 v7, 0x16

    if-ne v7, v3, :cond_2

    .line 484
    if-eqz v0, :cond_1

    .line 485
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processBiaRawData([I)I

    move-result v4

    .line 479
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 486
    :cond_2
    if-nez v3, :cond_1

    .line 487
    if-eqz v0, :cond_1

    .line 488
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processEcgRawData([I)I

    move-result v4

    goto :goto_2

    .line 492
    .end local v3    # "nRawType":S
    :cond_3
    sparse-switch v4, :sswitch_data_0

    :cond_4
    :goto_3
    move v6, v2

    .line 510
    goto :goto_0

    .line 496
    :sswitch_0
    iget v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    if-ne v7, v6, :cond_4

    .line 497
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 498
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 499
    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v7, :cond_4

    .line 500
    const/16 v7, 0xff

    if-ne v4, v7, :cond_5

    move v2, v5

    .line 501
    :goto_4
    iget-object v6, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    iget v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v6, v7, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStopped(II)V

    .line 502
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    goto :goto_3

    :cond_5
    move v2, v6

    .line 500
    goto :goto_4

    .line 492
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method private sendData([B)I
    .locals 4
    .param p1, "arrMessage"    # [B

    .prologue
    .line 276
    const/4 v0, 0x1

    .line 277
    .local v0, "nRtn":I
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->sendRawData([B)I

    move-result v0

    .line 280
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendData nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    return v0
.end method

.method private sendDataRequestMessage(BBBBS)V
    .locals 6
    .param p1, "cInvokeID"    # B
    .param p2, "cReqID"    # B
    .param p3, "cFlag"    # B
    .param p4, "cReqType"    # B
    .param p5, "sSupp2"    # S

    .prologue
    const/4 v5, 0x0

    .line 712
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendDataRequestMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 715
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 716
    iput-byte p1, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 717
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 718
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 720
    iput-byte p4, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 721
    iput-byte p2, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 722
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    .line 723
    const/4 v3, -0x1

    iput-short v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 724
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 725
    iput-short p5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 727
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 728
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 729
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 730
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 731
    return-void
.end method

.method private sendExtraTypeListRequestMessage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 735
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendExtraTypeListRequestMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 739
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 740
    const/4 v3, 0x6

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 741
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 742
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 744
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 745
    const/16 v3, 0xc

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 746
    const/4 v3, 0x1

    iput-short v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 747
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 748
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 749
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 751
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 752
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 753
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 754
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 755
    return-void
.end method

.method private sendProductInfoRequest(Z)V
    .locals 8
    .param p1, "bExtended"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 630
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendProductInfoRequest bExtended : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 633
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 634
    iput-byte v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 635
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 636
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 638
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 640
    if-eqz p1, :cond_0

    .line 641
    const/16 v3, 0x30

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 645
    :goto_0
    iput-short v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 646
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 647
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    .line 648
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 649
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 651
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 652
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 653
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 654
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 655
    return-void

    .line 643
    .end local v0    # "arrMsg":[B
    .end local v1    # "arrPacket":[B
    :cond_0
    iput-byte v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    goto :goto_0
.end method

.method private sendRealtimeDataFormatRequest()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 659
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendRealtimeDataFormatRequest"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 662
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 663
    const/4 v3, 0x5

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 664
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 665
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 666
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 667
    const/16 v3, 0x31

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 668
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 669
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 670
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 671
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 673
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 674
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 675
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 676
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 677
    return-void
.end method

.method private sendStopAllAction()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 759
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "sendStopAllAction"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    new-instance v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;-><init>([B)V

    .line 762
    .local v4, "shapChangeStatus":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;
    iput-byte v7, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageType:B

    .line 763
    const/16 v5, 0xa

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mInvokeID:B

    .line 764
    const/16 v5, 0x21

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageID:B

    .line 765
    const/16 v5, -0x80

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageFlag:B

    .line 766
    const/4 v5, -0x1

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mPowerMode:B

    .line 767
    iput-byte v7, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mStatus:B

    .line 769
    invoke-virtual {v4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->makeMessage()[B

    move-result-object v0

    .line 770
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 771
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 772
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    move-result v3

    .line 773
    .local v3, "nRtn":I
    if-eqz v3, :cond_0

    .line 776
    const-wide/16 v5, 0xc8

    const/4 v7, 0x0

    :try_start_0
    invoke-static {v5, v6, v7}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 781
    :goto_0
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "Re-sending..."

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    :cond_0
    return-void

    .line 778
    :catch_0
    move-exception v2

    .line 779
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setDeviceInformation()I
    .locals 4

    .prologue
    .line 334
    const/4 v0, 0x0

    .line 336
    .local v0, "nRtn":I
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    if-eqz v1, :cond_2

    .line 338
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v1, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSN:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v1, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturer:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 346
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDeviceInformation ProductSN : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v3, v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Manufacturer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v3, v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDeviceInformation nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    return v0
.end method

.method private setUserInformation(Landroid/os/Bundle;)I
    .locals 8
    .param p1, "parameters"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 164
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "setUserInformation"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/4 v4, 0x0

    .line 168
    .local v4, "nRtn":I
    const-string v5, "IsMan"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    sget-object v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    .line 169
    .local v2, "gender":Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    :goto_0
    const-string v5, "Age"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 170
    .local v3, "nAge":I
    const-string v5, "Height"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    .line 171
    .local v0, "fHeight":F
    const-string v5, "Weight"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    .line 173
    .local v1, "fWeight":F
    cmpl-float v5, v0, v7

    if-nez v5, :cond_0

    .line 174
    const-string v5, "Height"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v0, v5

    .line 175
    :cond_0
    cmpl-float v5, v1, v7

    if-nez v5, :cond_1

    .line 176
    const-string v5, "Weight"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v1, v5

    .line 177
    :cond_1
    new-instance v5, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;-><init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;IFF)V

    iput-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    .line 178
    return v4

    .line 168
    .end local v0    # "fHeight":F
    .end local v1    # "fWeight":F
    .end local v2    # "gender":Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    .end local v3    # "nAge":I
    :cond_2
    sget-object v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->FEMALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    goto :goto_0
.end method

.method private startBodyFatMeasurement()I
    .locals 4

    .prologue
    .line 268
    const/4 v0, 0x1

    .line 269
    .local v0, "nRtn":I
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startMeasurement(S)I

    move-result v0

    .line 270
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startBodyFatMeasurement nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    return v0
.end method

.method private startMeasurement(S)I
    .locals 18
    .param p1, "nDevType"    # S

    .prologue
    .line 183
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startMeasurement nDevType : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const/4 v15, 0x1

    .line 186
    .local v15, "nRtn":I
    const/16 v13, 0xff

    .line 188
    .local v13, "nDeviceExtraId":I
    const/4 v14, 0x3

    .line 189
    .local v14, "nInvokeId":I
    const/4 v3, 0x0

    .line 190
    .local v3, "cMsgId":B
    const/4 v5, 0x0

    .line 191
    .local v5, "cReqType":B
    const/16 v6, 0xff

    .line 193
    .local v6, "sSupp2":S
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I

    .line 195
    packed-switch p1, :pswitch_data_0

    move/from16 v16, v15

    .line 255
    .end local v15    # "nRtn":I
    .local v16, "nRtn":I
    :goto_0
    return v16

    .line 197
    .end local v16    # "nRtn":I
    .restart local v15    # "nRtn":I
    :pswitch_0
    const/16 v3, 0x50

    .line 198
    const/16 v17, 0x0

    .line 199
    .local v17, "sWaveType":S
    const/16 v5, 0x40

    .line 200
    const/4 v1, 0x4

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    .line 220
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-eqz v1, :cond_1

    .line 222
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    const/4 v2, 0x0

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->getChannelIDByType(IS)I

    move-result v12

    .line 223
    .local v12, "nChnId":I
    if-ltz v12, :cond_3

    .line 225
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    invoke-virtual {v1, v12}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->getChannelFormatById(I)Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    move-result-object v1

    iget v11, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mSamplingFreq:F

    .line 226
    .local v11, "fFs":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    invoke-virtual {v1, v12}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->getChannelFormatById(I)Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    move-result-object v1

    iget v9, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mCoeff_a:F

    .line 227
    .local v9, "fCoefA":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    invoke-virtual {v1, v12}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->getChannelFormatById(I)Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    move-result-object v1

    iget v10, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mCoeff_b:F

    .line 229
    .local v10, "fCoefB":F
    const/4 v1, 0x6

    move/from16 v0, p1

    if-ne v0, v1, :cond_2

    .line 230
    new-instance v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    float-to-int v4, v11

    invoke-direct {v1, v2, v4, v9, v10}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;-><init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;IFF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    .line 237
    :cond_0
    :goto_1
    const/4 v1, 0x1

    new-array v7, v1, [B

    .line 238
    .local v7, "arrChnId":[B
    const/4 v1, 0x0

    int-to-byte v2, v12

    aput-byte v2, v7, v1

    .line 239
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendRealtimeDataRequest([B)V

    .line 240
    const-wide/16 v1, 0x64

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v1, v2, v4}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :goto_2
    int-to-byte v2, v14

    const/16 v4, -0x80

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendDataRequestMessage(BBBBS)V

    .line 247
    const/4 v15, 0x0

    .line 248
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 249
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStarted(I)V

    .end local v7    # "arrChnId":[B
    .end local v9    # "fCoefA":F
    .end local v10    # "fCoefB":F
    .end local v11    # "fFs":F
    .end local v12    # "nChnId":I
    :cond_1
    :goto_3
    move/from16 v16, v15

    .line 255
    .end local v15    # "nRtn":I
    .restart local v16    # "nRtn":I
    goto :goto_0

    .line 231
    .end local v16    # "nRtn":I
    .restart local v9    # "fCoefA":F
    .restart local v10    # "fCoefB":F
    .restart local v11    # "fFs":F
    .restart local v12    # "nChnId":I
    .restart local v15    # "nRtn":I
    :cond_2
    const/4 v1, 0x3

    move/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 233
    new-instance v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    float-to-int v2, v11

    invoke-direct {v1, v2, v9, v10}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;-><init>(IFF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    .line 234
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->makeEcgBundle(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;)Landroid/os/Bundle;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    goto :goto_1

    .line 240
    .restart local v7    # "arrChnId":[B
    :catch_0
    move-exception v8

    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 253
    .end local v7    # "arrChnId":[B
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "fCoefA":F
    .end local v10    # "fCoefB":F
    .end local v11    # "fFs":F
    :cond_3
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "onDataStarted will not be called."

    invoke-static {v1, v2}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public deinitialize()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 151
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    if-ne v0, v2, :cond_0

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 156
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    iget v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStopped(II)V

    .line 160
    :cond_0
    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 78
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    .line 79
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 1
    .param p1, "arrData"    # [B

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "nRtn":I
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processMessage([B)I

    move-result v0

    .line 88
    return v0
.end method

.method public notifyStart()I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 93
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "notifyStart"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 95
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initShapMessageVariables()V

    .line 96
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-direct {v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I

    move-result v1

    .line 98
    .local v1, "nRtn":I
    if-eqz v1, :cond_0

    .line 100
    const-wide/16 v2, 0x1f4

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 108
    :cond_0
    :goto_0
    return v5

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public notifyStop()I
    .locals 2

    .prologue
    .line 112
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v1, "notifyStop"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 114
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initShapMessageVariables()V

    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public sendRealtimeDataRequest([B)V
    .locals 9
    .param p1, "arrChnId"    # [B

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 681
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "sendRealtimeDataRequest"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    new-instance v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;-><init>([B)V

    .line 684
    .local v4, "shapRawDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;
    iput-byte v8, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageType:B

    .line 685
    const/4 v5, 0x2

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mInvokeID:B

    .line 686
    const/4 v5, 0x7

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageID:B

    .line 687
    const/16 v5, -0x80

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageFlag:B

    .line 689
    if-eqz p1, :cond_0

    array-length v5, p1

    if-gtz v5, :cond_2

    .line 691
    :cond_0
    iput-byte v8, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    .line 704
    :cond_1
    invoke-virtual {v4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->makeMessage()[B

    move-result-object v0

    .line 705
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 706
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 707
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 708
    return-void

    .line 695
    .end local v0    # "arrMsg":[B
    .end local v1    # "arrPacket":[B
    :cond_2
    array-length v3, p1

    .line 696
    .local v3, "nNumOfChn":I
    const/4 v5, 0x5

    if-le v3, v5, :cond_3

    const/4 v3, 0x5

    .line 697
    :cond_3
    int-to-byte v5, v3

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    .line 699
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 701
    aget-byte v5, p1, v2

    invoke-virtual {v4, v2, v5, v7, v7}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->setChannelInfo(IBBB)V

    .line 699
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startEcgMeasurement()I
    .locals 4

    .prologue
    .line 260
    const/4 v0, 0x1

    .line 261
    .local v0, "nRtn":I
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startMeasurement(S)I

    move-result v0

    .line 262
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startEcgMeasurement nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    return v0
.end method
