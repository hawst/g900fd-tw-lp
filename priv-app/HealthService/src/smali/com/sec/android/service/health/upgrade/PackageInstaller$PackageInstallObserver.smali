.class Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "PackageInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/PackageInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/PackageInstaller;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstaller;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstaller;)Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageInstallObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstaller;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstaller;)Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;->packageInstalled(Ljava/lang/String;I)V

    .line 112
    :cond_0
    return-void
.end method
