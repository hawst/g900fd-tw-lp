.class Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;
.super Ljava/lang/Object;
.source "InternalEcgHandler.java"

# interfaces
.implements Lcom/samsung/android/ssensor/SSensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EcgDataListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
    .param p2, "x1"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V

    return-void
.end method


# virtual methods
.method public onDataReceived([BI)V
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "size"    # I

    .prologue
    .line 235
    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataReceived :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/math/BigInteger;

    const/4 v3, 0x1

    invoke-direct {v2, v3, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyRawDataReceived([B)I

    .line 238
    :cond_0
    return-void
.end method
