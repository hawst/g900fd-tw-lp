.class Lcom/sec/android/service/health/upgrade/PackageInstallService$1;
.super Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;
.source "PackageInstallService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/PackageInstallService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/PackageInstallService;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    invoke-direct {p0}, Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public deletePackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 112
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "deletePackage()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "[PackageInstallService] deletePackage : Security exception - Caller application not privileged"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->deletePackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$200(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageDeleteFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disablePackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "disablePackage()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "[PackageInstallService] disablePackage : Security exception - Caller application not privileged"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->disablePackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public installPackage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 91
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "installPackage()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v2}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 95
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "[PackageInstallService] installPackage : Security exception - Caller application not privileged"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->installPackage(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 106
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$200(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageInstallFailed(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public registerCallback(Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 151
    const-string v0, "SHealthUpgrade(PackageInstallService)"

    const-string v1, "registerCallback()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    const-string v0, "SHealthUpgrade(PackageInstallService)"

    const-string v1, "[PackageInstallService] registerCallback : Security exception - Caller application not privileged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    if-eqz p1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # setter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    invoke-static {v0, p1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$202(Lcom/sec/android/service/health/upgrade/PackageInstallService;Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    goto :goto_0
.end method

.method public unregisterCallback()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 167
    const-string v0, "SHealthUpgrade(PackageInstallService)"

    const-string v1, "unregisterCallback()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;
    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const-string v0, "SHealthUpgrade(PackageInstallService)"

    const-string v1, "[PackageInstallService] unregisterCallback : Security exception - Caller application not privileged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/upgrade/PackageInstallService;->access$202(Lcom/sec/android/service/health/upgrade/PackageInstallService;Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    goto :goto_0
.end method
