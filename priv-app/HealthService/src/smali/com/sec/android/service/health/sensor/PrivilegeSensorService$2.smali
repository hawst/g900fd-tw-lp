.class final Lcom/sec/android/service/health/sensor/PrivilegeSensorService$2;
.super Landroid/os/Handler;
.source "PrivilegeSensorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 384
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 389
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 390
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[SensorListener] mCbHandler handleMessage msg.what : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 548
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v7, "[SensorListener] handleMessage case default"

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 397
    :pswitch_0
    const/4 v4, 0x0

    .line 398
    .local v4, "extra":[Landroid/os/Bundle;
    const-string v6, "_Extra"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 399
    const-string v6, "_Extra"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, [Landroid/os/Bundle;

    move-object v4, v6

    check-cast v4, [Landroid/os/Bundle;

    .line 401
    :cond_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/RemoteCallbackList;

    .line 402
    .local v2, "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-eqz v2, :cond_0

    .line 404
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 406
    :try_start_0
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 407
    .local v0, "broadcastNum":I
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HANDLER_DATA_RECEIVED broadcastNum = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v0, :cond_6

    .line 412
    :try_start_1
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 414
    if-eqz v4, :cond_4

    .line 415
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v6, v8, v4}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;->onBulkDataReceived(I[Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 426
    :cond_2
    :goto_2
    :try_start_2
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 427
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 408
    :cond_3
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 417
    :cond_4
    :try_start_3
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v8, "HANDLER_DATA_RECEIVED  data and extra are null"

    invoke-static {v6, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 420
    :catch_0
    move-exception v3

    .line 422
    .local v3, "e":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 426
    :try_start_5
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 427
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_3

    .line 431
    .end local v0    # "broadcastNum":I
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v5    # "i":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v6

    .line 426
    .restart local v0    # "broadcastNum":I
    .restart local v5    # "i":I
    :catchall_1
    move-exception v6

    :try_start_6
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 427
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_5
    throw v6

    .line 430
    :cond_6
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 431
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 438
    .end local v0    # "broadcastNum":I
    .end local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    .end local v4    # "extra":[Landroid/os/Bundle;
    .end local v5    # "i":I
    :pswitch_1
    const/4 v4, 0x0

    .line 439
    .local v4, "extra":Landroid/os/Bundle;
    const-string v6, "_Extra"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 440
    const-string v6, "_Extra"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .end local v4    # "extra":Landroid/os/Bundle;
    check-cast v4, Landroid/os/Bundle;

    .line 442
    .restart local v4    # "extra":Landroid/os/Bundle;
    :cond_7
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/RemoteCallbackList;

    .line 443
    .restart local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-eqz v2, :cond_0

    .line 445
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 447
    :try_start_7
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 448
    .restart local v0    # "broadcastNum":I
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "HANDLER_DATA_RECEIVED broadcastNum = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 449
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    if-ge v5, v0, :cond_c

    .line 453
    :try_start_8
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 455
    if-eqz v4, :cond_a

    .line 456
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v6, v8, v4}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;->onDataReceived(ILandroid/os/Bundle;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 467
    :cond_8
    :goto_5
    :try_start_9
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 468
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 449
    :cond_9
    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 458
    :cond_a
    :try_start_a
    const-string v6, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v8, "HANDLER_DATA_RECEIVED  data and extra are null"

    invoke-static {v6, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    goto :goto_5

    .line 461
    :catch_1
    move-exception v3

    .line 463
    .restart local v3    # "e":Landroid/os/RemoteException;
    :try_start_b
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 467
    :try_start_c
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 468
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_6

    .line 472
    .end local v0    # "broadcastNum":I
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v5    # "i":I
    :catchall_2
    move-exception v6

    monitor-exit v7
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v6

    .line 467
    .restart local v0    # "broadcastNum":I
    .restart local v5    # "i":I
    :catchall_3
    move-exception v6

    :try_start_d
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 468
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_b
    throw v6

    .line 471
    :cond_c
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 472
    monitor-exit v7
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto/16 :goto_0

    .line 479
    .end local v0    # "broadcastNum":I
    .end local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    .end local v4    # "extra":Landroid/os/Bundle;
    .end local v5    # "i":I
    :pswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/RemoteCallbackList;

    .line 480
    .restart local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-eqz v2, :cond_0

    .line 482
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 484
    :try_start_e
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    move-result v0

    .line 485
    .restart local v0    # "broadcastNum":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_7
    if-ge v5, v0, :cond_10

    .line 489
    :try_start_f
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 491
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget v9, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v6, v8, v9}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;->onDataStarted(II)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 501
    :cond_d
    :try_start_10
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 502
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 485
    :cond_e
    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    .line 495
    :catch_2
    move-exception v3

    .line 497
    .restart local v3    # "e":Landroid/os/RemoteException;
    :try_start_11
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    .line 501
    :try_start_12
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_e

    .line 502
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_8

    .line 506
    .end local v0    # "broadcastNum":I
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v5    # "i":I
    :catchall_4
    move-exception v6

    monitor-exit v7
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    throw v6

    .line 501
    .restart local v0    # "broadcastNum":I
    .restart local v5    # "i":I
    :catchall_5
    move-exception v6

    :try_start_13
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    if-eqz v8, :cond_f

    .line 502
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_f
    throw v6

    .line 505
    :cond_10
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 506
    monitor-exit v7
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    goto/16 :goto_0

    .line 514
    .end local v0    # "broadcastNum":I
    .end local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    .end local v5    # "i":I
    :pswitch_3
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/RemoteCallbackList;

    .line 515
    .restart local v2    # "dataCallbackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-eqz v2, :cond_0

    .line 517
    sget-object v7, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->beginBroadcast:Ljava/lang/Object;

    monitor-enter v7

    .line 519
    :try_start_14
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    move-result v0

    .line 520
    .restart local v0    # "broadcastNum":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_9
    if-ge v5, v0, :cond_14

    .line 524
    :try_start_15
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_11

    .line 526
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    iget v8, p1, Landroid/os/Message;->arg1:I

    iget v9, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v6, v8, v9}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;->onDataStopped(II)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_7

    .line 535
    :cond_11
    :try_start_16
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_12

    .line 536
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    .line 520
    :cond_12
    :goto_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 529
    :catch_3
    move-exception v3

    .line 531
    .restart local v3    # "e":Landroid/os/RemoteException;
    :try_start_17
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    .line 535
    :try_start_18
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    if-eqz v6, :cond_12

    .line 536
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    goto :goto_a

    .line 540
    .end local v0    # "broadcastNum":I
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v5    # "i":I
    :catchall_6
    move-exception v6

    monitor-exit v7
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    throw v6

    .line 535
    .restart local v0    # "broadcastNum":I
    .restart local v5    # "i":I
    :catchall_7
    move-exception v6

    :try_start_19
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    if-eqz v8, :cond_13

    .line 536
    invoke-virtual {v2, v5}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_13
    throw v6

    .line 539
    :cond_14
    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 540
    monitor-exit v7
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    goto/16 :goto_0

    .line 392
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
