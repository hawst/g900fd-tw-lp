.class public Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;
.super Landroid/content/ContentProvider;
.source "TrustZoneSecurityProvider.java"


# static fields
.field private static final ENABLE_TRUTSTZONE_FILE:Ljava/lang/String; = "enable_trustzone_in_SHealth"

.field private static final IS_SECURE_STORAGE_ENABLED:Z = true

.field private static final KEY_IS_SECURE_STORAGE_SUPPORTS:Ljava/lang/String; = "KEY_IS_SECURE_STORAGE_SUPPORTS"

.field private static final NOT_SUPPORTED:I = 0x0

.field private static final NOT_TESTED:I = -0x1

.field public static final PKG_NAME:Ljava/lang/String; = "com.sec.android.service.health"

.field private static final SUPPORTED:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field private static final TEST_TRUST_KEY:Ljava/lang/String; = "com.test.key"

.field public static final TRUST_KEY:Ljava/lang/String; = "com.sec.android.app.shealth"

.field private static mContext:Landroid/content/Context;

.field private static mPasswd:[B

.field private static mSyncObject:Ljava/lang/Object;


# instance fields
.field private mIsTestTrustZoneSupportsOurApp:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mSyncObject:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I

    return-void
.end method

.method public static createTrustZoneSecurePasswd(Ljava/lang/String;Landroid/content/Context;)[B
    .locals 16
    .param p0, "secureKey"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 138
    sget-object v14, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mSyncObject:Ljava/lang/Object;

    monitor-enter v14

    .line 140
    :try_start_0
    sget-object v12, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v13, " generate new password"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const/16 v12, 0x8

    new-array v8, v12, [B

    .line 149
    .local v8, "passwd":[B
    new-instance v5, Ljava/io/File;

    const-string v12, "/dev/random"

    invoke-direct {v5, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v5, "f":Ljava/io/File;
    const/4 v6, 0x0

    .line 151
    .local v6, "fis":Ljava/io/FileInputStream;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v12

    if-eqz v12, :cond_1

    .line 155
    :try_start_1
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v12, 0x0

    const/16 v13, 0x8

    :try_start_2
    invoke-virtual {v7, v8, v12, v13}, Ljava/io/FileInputStream;->read([BII)I

    move-result v9

    .line 158
    .local v9, "readByte":I
    const/16 v12, 0x8

    if-eq v9, v12, :cond_2

    .line 160
    new-instance v12, Ljava/lang/Exception;

    const-string v13, "not enough byte read"

    invoke-direct {v12, v13}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 163
    .end local v9    # "readByte":I
    :catch_0
    move-exception v2

    move-object v6, v7

    .line 165
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :goto_0
    :try_start_3
    instance-of v12, v2, Ljava/io/FileNotFoundException;

    if-nez v12, :cond_0

    instance-of v12, v2, Ljava/io/IOException;

    if-nez v12, :cond_0

    const-string v12, "not enough byte read"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 167
    :cond_0
    new-instance v10, Ljava/security/SecureRandom;

    invoke-direct {v10}, Ljava/security/SecureRandom;-><init>()V

    .line 168
    .local v10, "sr":Ljava/security/SecureRandom;
    invoke-virtual {v10, v8}, Ljava/security/SecureRandom;->nextBytes([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 180
    if-eqz v6, :cond_1

    .line 181
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 205
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v10    # "sr":Ljava/security/SecureRandom;
    :cond_1
    :goto_1
    const/4 v4, 0x0

    .line 206
    .local v4, "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_2
    const/4 v12, 0x3

    if-ge v1, v12, :cond_c

    .line 210
    :try_start_5
    new-instance v11, Lcom/sec/android/securestorage/SecureStorage;

    invoke-direct {v11}, Lcom/sec/android/securestorage/SecureStorage;-><init>()V

    .line 212
    .local v11, "storage":Lcom/sec/android/securestorage/SecureStorage;
    move-object/from16 v0, p0

    invoke-virtual {v11, v0, v8}, Lcom/sec/android/securestorage/SecureStorage;->put(Ljava/lang/String;[B)Z

    .line 214
    const-string v12, "com.test.key"

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v12

    if-eqz v12, :cond_9

    .line 216
    :try_start_6
    monitor-exit v14
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 247
    .end local v8    # "passwd":[B
    .end local v11    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    :goto_3
    return-object v8

    .line 180
    .end local v1    # "count":I
    .end local v4    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "passwd":[B
    .restart local v9    # "readByte":I
    :cond_2
    if-eqz v7, :cond_3

    .line 181
    :try_start_7
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_3
    move-object v6, v7

    .line 186
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 183
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    .line 185
    .local v2, "e":Ljava/io/IOException;
    :try_start_8
    sget-object v13, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_4

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    :goto_4
    invoke-static {v13, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 188
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 185
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :cond_4
    const-string v12, " "
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_4

    .line 173
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "readByte":I
    .local v2, "e":Ljava/lang/Exception;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_5
    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 178
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v12

    .line 180
    :goto_5
    if-eqz v6, :cond_6

    .line 181
    :try_start_a
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 186
    :cond_6
    :goto_6
    :try_start_b
    throw v12

    .line 245
    .end local v5    # "f":Ljava/io/File;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "passwd":[B
    :catchall_1
    move-exception v12

    monitor-exit v14
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v12

    .line 183
    .restart local v2    # "e":Ljava/lang/Exception;
    .restart local v5    # "f":Ljava/io/File;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "passwd":[B
    .restart local v10    # "sr":Ljava/security/SecureRandom;
    :catch_2
    move-exception v2

    .line 185
    .local v2, "e":Ljava/io/IOException;
    :try_start_c
    sget-object v13, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    :goto_7
    invoke-static {v13, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    const-string v12, " "

    goto :goto_7

    .line 183
    .end local v2    # "e":Ljava/io/IOException;
    .end local v10    # "sr":Ljava/security/SecureRandom;
    :catch_3
    move-exception v2

    .line 185
    .restart local v2    # "e":Ljava/io/IOException;
    sget-object v15, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_8

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    :goto_8
    invoke-static {v15, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_8
    const-string v13, " "
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_8

    .line 219
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "count":I
    .restart local v4    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .restart local v11    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    :cond_9
    :try_start_d
    sput-object v8, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    .line 220
    sget-object v8, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B
    :try_end_d
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .end local v8    # "passwd":[B
    :try_start_e
    monitor-exit v14

    goto :goto_3

    .line 223
    .end local v11    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    .restart local v8    # "passwd":[B
    :catch_4
    move-exception v2

    .line 225
    .local v2, "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    sget-object v13, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_a

    invoke-virtual {v2}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v12

    :goto_9
    invoke-static {v13, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    monitor-enter p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 231
    const-wide/16 v12, 0xc8

    :try_start_f
    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Ljava/lang/Object;->wait(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 237
    :goto_a
    :try_start_10
    monitor-exit p1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 238
    move-object v4, v2

    .line 206
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 225
    :cond_a
    :try_start_11
    const-string v12, " "
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto :goto_9

    .line 233
    :catch_5
    move-exception v3

    .line 235
    .local v3, "e1":Ljava/lang/InterruptedException;
    :try_start_12
    sget-object v13, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_b

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v12

    :goto_b
    invoke-static {v13, v12}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 237
    .end local v3    # "e1":Ljava/lang/InterruptedException;
    :catchall_2
    move-exception v12

    monitor-exit p1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    :try_start_13
    throw v12
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 235
    .restart local v3    # "e1":Ljava/lang/InterruptedException;
    :cond_b
    :try_start_14
    const-string v12, " "
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    goto :goto_b

    .line 241
    .end local v2    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v3    # "e1":Ljava/lang/InterruptedException;
    :cond_c
    if-eqz v4, :cond_d

    .line 243
    :try_start_15
    throw v4

    .line 245
    :cond_d
    monitor-exit v14
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 247
    sget-object v8, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    goto/16 :goto_3

    .line 178
    .end local v1    # "count":I
    .end local v4    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catchall_3
    move-exception v12

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_5

    .line 163
    :catch_6
    move-exception v2

    goto/16 :goto_0
.end method

.method private getCallerPackage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 509
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 510
    .local v0, "caller":I
    if-nez v0, :cond_0

    .line 512
    const/4 v2, 0x0

    .line 515
    :goto_0
    return-object v2

    .line 514
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "pkgName":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    goto :goto_0
.end method

.method public static getKeyFromAES()[B
    .locals 3

    .prologue
    .line 268
    sget-object v1, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v2, " getAESEncryptSecurePasswd"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    new-instance v0, Lcom/sec/android/service/health/cp/common/AESEncryption;

    invoke-direct {v0}, Lcom/sec/android/service/health/cp/common/AESEncryption;-><init>()V

    .line 270
    .local v0, "enc":Lcom/sec/android/service/health/cp/common/AESEncryption;
    iget-object v1, v0, Lcom/sec/android/service/health/cp/common/AESEncryption;->str:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    return-object v1
.end method

.method public static getKeyFromTrustZone(Landroid/content/Context;)[B
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    sget-object v7, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mSyncObject:Ljava/lang/Object;

    monitor-enter v7

    .line 76
    :try_start_0
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    if-eqz v6, :cond_0

    .line 78
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    monitor-exit v7

    .line 131
    :goto_0
    return-object v6

    .line 86
    :cond_0
    invoke-static {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->isTrustZonePermissionDeclared(Landroid/content/Context;)Z

    move-result v4

    .line 88
    .local v4, "isTrustZonePermissionDeclared":Z
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isTrustZonePermissionDeclared "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    if-eqz v4, :cond_4

    invoke-static {}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->secIsSupported()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_4

    .line 92
    const/4 v3, 0x0

    .line 93
    .local v3, "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_1
    const/4 v6, 0x3

    if-ge v0, v6, :cond_3

    .line 97
    :try_start_1
    new-instance v5, Lcom/sec/android/securestorage/SecureStorage;

    invoke-direct {v5}, Lcom/sec/android/securestorage/SecureStorage;-><init>()V

    .line 99
    .local v5, "storage":Lcom/sec/android/securestorage/SecureStorage;
    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {v5, v6}, Lcom/sec/android/securestorage/SecureStorage;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    sput-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    .line 100
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B
    :try_end_1
    .catch Lcom/sec/android/securestorage/SecureStorage$SecureStorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v7

    goto :goto_0

    .line 132
    .end local v0    # "count":I
    .end local v3    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v4    # "isTrustZonePermissionDeclared":Z
    .end local v5    # "storage":Lcom/sec/android/securestorage/SecureStorage;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 102
    .restart local v0    # "count":I
    .restart local v3    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .restart local v4    # "isTrustZonePermissionDeclared":Z
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    :try_start_3
    sget-object v8, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;->getMessage()Ljava/lang/String;

    move-result-object v6

    :goto_2
    invoke-static {v8, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 110
    const-wide/16 v8, 0xc8

    :try_start_4
    invoke-virtual {p0, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 116
    :goto_3
    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 117
    move-object v3, v1

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 104
    :cond_1
    :try_start_6
    const-string v6, " "
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 112
    :catch_1
    move-exception v2

    .line 114
    .local v2, "e1":Ljava/lang/InterruptedException;
    :try_start_7
    sget-object v8, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    :goto_4
    invoke-static {v8, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 116
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v6

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 114
    .restart local v2    # "e1":Ljava/lang/InterruptedException;
    :cond_2
    :try_start_9
    const-string v6, " "
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_4

    .line 120
    .end local v1    # "e":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    :cond_3
    if-eqz v3, :cond_5

    .line 122
    :try_start_a
    throw v3

    .line 128
    .end local v0    # "count":I
    .end local v3    # "exception":Lcom/sec/android/securestorage/SecureStorage$SecureStorageException;
    :cond_4
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v8, "Returning null pass"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const/4 v6, 0x0

    sput-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    .line 131
    :cond_5
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mPasswd:[B

    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0
.end method

.method private static isTrustZonePermissionDeclared(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 476
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v7, "[+] isTrustZonePermissionDeclared"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const/4 v3, 0x0

    .line 478
    .local v3, "isTrustZonePermissionDeclared":Z
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 481
    .local v5, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v6, "com.sec.android.service.health"

    const/16 v7, 0x1000

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 484
    .local v4, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v4, Landroid/content/pm/PackageInfo;->permissions:[Landroid/content/pm/PermissionInfo;

    .line 486
    .local v0, "declaredPermissions":[Landroid/content/pm/PermissionInfo;
    if-eqz v0, :cond_0

    .line 488
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v0

    if-ge v2, v6, :cond_0

    .line 490
    const-string v6, "com.sec.android.securestorage.permission.SECURE_STORAGE"

    aget-object v7, v0, v2

    iget-object v7, v7, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_1

    .line 492
    const/4 v3, 0x1

    .line 502
    .end local v0    # "declaredPermissions":[Landroid/content/pm/PermissionInfo;
    .end local v2    # "i":I
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_1
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[-]isTrustZonePermissionDeclared "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    return v3

    .line 488
    .restart local v0    # "declaredPermissions":[Landroid/content/pm/PermissionInfo;
    .restart local v2    # "i":I
    .restart local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 498
    .end local v0    # "declaredPermissions":[Landroid/content/pm/PermissionInfo;
    .end local v2    # "i":I
    .end local v4    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 500
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private static secIsSupported()Z
    .locals 4

    .prologue
    .line 422
    sget-object v2, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 423
    invoke-static {}, Lcom/sec/android/securestorage/SecureStorage;->isSupported()Z

    move-result v2

    .line 432
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    :goto_0
    return v2

    .line 424
    .end local v1    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_0
    sget-object v2, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 425
    .restart local v1    # "sharedPref":Landroid/content/SharedPreferences;
    const-string v2, "KEY_IS_SECURE_STORAGE_SUPPORTS"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 427
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 429
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "KEY_IS_SECURE_STORAGE_SUPPORTS"

    invoke-static {}, Lcom/sec/android/securestorage/SecureStorage;->isSupported()Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 430
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 432
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    const-string v2, "KEY_IS_SECURE_STORAGE_SUPPORTS"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_0
.end method

.method private testSecureStorageSuportForShealth()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 436
    sget-object v5, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v6, "[+] testSecureStorageSuportForShealth"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    const/4 v2, 0x0

    .line 452
    .local v2, "retVal":Z
    iget v5, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 453
    const/4 v1, 0x0

    .line 455
    .local v1, "pass":[B
    :try_start_0
    const-string v3, "com.test.key"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->createTrustZoneSecurePasswd(Ljava/lang/String;Landroid/content/Context;)[B

    move-result-object v1

    .line 456
    sget-object v3, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v5, " testSecureStorageSuportForShealth is supported "

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    const/4 v3, 0x1

    iput v3, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    const/4 v2, 0x1

    .line 469
    .end local v1    # "pass":[B
    :goto_0
    sget-object v3, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v4, "[-] testSecureStorageSuportForShealth"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    return v2

    .line 459
    .restart local v1    # "pass":[B
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v5, " testSecureStorageSuportForShealth exception so not supported"

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const/4 v2, 0x0

    .line 462
    iput v4, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I

    goto :goto_0

    .line 465
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "pass":[B
    :cond_0
    sget-object v5, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[+] testSecureStorageSuportForShealth already tested once and status is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget v5, p0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mIsTestTrustZoneSupportsOurApp:I

    if-ne v5, v3, :cond_1

    move v2, v3

    :goto_1
    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1
.end method


# virtual methods
.method public call(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x0

    .line 341
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v7, "[+] call"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TZP call. Method: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 344
    .local v5, "resultBundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v3

    .line 345
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 347
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TZP call. Signature Pass. packageName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const-string v6, "get_secure_password"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 352
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getKeyFromTrustZone(Landroid/content/Context;)[B

    move-result-object v4

    .line 353
    .local v4, "result":[B
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    .end local v4    # "result":[B
    :cond_0
    :goto_0
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v7, "[-] call"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    return-object v5

    .line 355
    :catch_0
    move-exception v0

    .line 357
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error in getting Trustzone password : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v9}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0

    .line 361
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_1
    const-string v6, "create_secure_password"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 365
    :try_start_1
    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->createTrustZoneSecurePasswd(Ljava/lang/String;Landroid/content/Context;)[B

    move-result-object v4

    .line 366
    .restart local v4    # "result":[B
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 368
    .end local v4    # "result":[B
    :catch_1
    move-exception v0

    .line 370
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error in getting Trustzone password : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v9}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0

    .line 374
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    const-string v6, "get_aes_password"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 378
    :try_start_2
    invoke-static {}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getKeyFromAES()[B

    move-result-object v4

    .line 379
    .restart local v4    # "result":[B
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 381
    .end local v4    # "result":[B
    :catch_2
    move-exception v0

    .line 383
    .restart local v0    # "ex":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error in getting Trustzone password : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const-string v6, "value_of_password"

    invoke-virtual {v5, v6, v9}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto/16 :goto_0

    .line 387
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_3
    const-string v6, "secure_storage_support"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 389
    const/4 v1, 0x0

    .line 391
    .local v1, "isSupported":Z
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->isTrustZonePermissionDeclared(Landroid/content/Context;)Z

    move-result v2

    .line 392
    .local v2, "isTrustZonePermissionDeclared":Z
    if-eqz v2, :cond_4

    invoke-static {}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->secIsSupported()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 394
    invoke-direct {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->testSecureStorageSuportForShealth()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 395
    const/4 v1, 0x1

    .line 398
    :cond_4
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "KEY_IS_SECURE_STORAGE_SUPPORTS "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    const-string v6, "boolean_secure_storage_support"

    invoke-virtual {v5, v6, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 404
    .end local v1    # "isSupported":Z
    .end local v2    # "isTrustZonePermissionDeclared":Z
    :cond_5
    sget-object v6, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Call API not Availiable for the caller\'s signature, caller package : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getCallerPackage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    new-instance v6, Ljava/lang/SecurityException;

    const-string v7, "Call API is not Availiable for your app"

    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 303
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In delete Uri call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;

    .prologue
    .line 309
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In getType call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;

    .prologue
    .line 315
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In insert call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 321
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In onCreate call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->mContext:Landroid/content/Context;

    .line 323
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # [Ljava/lang/String;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;
    .param p5, "arg4"    # Ljava/lang/String;

    .prologue
    .line 329
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In query call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Landroid/content/ContentValues;
    .param p3, "arg2"    # Ljava/lang/String;
    .param p4, "arg3"    # [Ljava/lang/String;

    .prologue
    .line 335
    sget-object v0, Lcom/sec/android/service/health/cp/TrustZoneSecurityProvider;->TAG:Ljava/lang/String;

    const-string v1, "In update call"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const/4 v0, 0x0

    return v0
.end method
