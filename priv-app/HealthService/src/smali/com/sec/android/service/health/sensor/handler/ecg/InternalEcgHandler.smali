.class public Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
.super Ljava/lang/Object;
.source "InternalEcgHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
.implements Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;,
        Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;,
        Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final CMD:[B

.field private static final TAG:Ljava/lang/String;

.field public static TEST_FLAG:Z

.field private static bListenerRegistered:Z


# instance fields
.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

.field mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

.field mManager:Lcom/samsung/android/ssensor/SSensorManager;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field private mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    .line 29
    const/16 v0, 0x18

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    .line 31
    sput-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 39
    sput-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TEST_FLAG:Z

    return-void

    .line 29
    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x1t
        0x0t
        -0x80t
        0x0t
        0x30t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    .line 27
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    .line 32
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 33
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 35
    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    .line 230
    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    return-object v0
.end method

.method private registerEcgListener()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/ssensor/SSensorManager;->registerListener(Lcom/samsung/android/ssensor/SSensorListener;Landroid/os/Handler;I)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    .line 184
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 185
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "Ecg sensor registered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :goto_0
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    return v0

    .line 189
    :cond_0
    sput-boolean v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 190
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "Ecg sensor not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private unregisterEcgListener()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 197
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterEcgListener : current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v0, :cond_0

    .line 201
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "unregisterSensorManager is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    sput-boolean v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 203
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/ssensor/SSensorManager;->unregisterListener(Lcom/samsung/android/ssensor/SSensorListener;)V

    .line 206
    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->unregisterEcgListener()V

    .line 84
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    .line 212
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 213
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 214
    invoke-static {}, Lcom/samsung/android/ssensor/SSensorManager;->getSSensorManager()Lcom/samsung/android/ssensor/SSensorManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    .line 215
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    .line 217
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1, p0, v2, p4}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I

    .line 221
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    .line 76
    return-void
.end method

.method public onDataReceived(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 253
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(ILandroid/os/Bundle;)V

    .line 257
    :cond_0
    return-void
.end method

.method public onDataReceived([Landroid/os/Bundle;[Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # [Landroid/os/Bundle;
    .param p2, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 244
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived[]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 248
    :cond_0
    return-void
.end method

.method public onDataStarted(I)V
    .locals 3
    .param p1, "dataType"    # I

    .prologue
    .line 262
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStarted dataType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 266
    :cond_0
    return-void
.end method

.method public onDataStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 271
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStopped dataType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->stopReceivingData()I

    .line 274
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;)V
    .locals 2
    .param p1, "response"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;

    .prologue
    .line 308
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onResponseReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 279
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    return-void
.end method

.method public sendRawData([B)I
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/16 v0, 0x3e8

    const/4 v1, 0x0

    .line 288
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v3, "sendRawData"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    if-nez p1, :cond_1

    .line 302
    :cond_0
    :goto_0
    return v0

    .line 295
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v2, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    move v0, v1

    .line 299
    goto :goto_0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;)V
    .locals 0
    .param p1, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 105
    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 0
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 93
    return-void
.end method

.method public startReceivingData()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 128
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TEST_FLAG:Z

    if-eqz v0, :cond_1

    .line 130
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    .line 131
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;->start()V

    .line 132
    sput-boolean v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 153
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v4, 0x4

    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    invoke-interface {v3, v4, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 155
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_5

    :goto_2
    return v2

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    if-eqz v0, :cond_0

    .line 136
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-nez v0, :cond_2

    .line 138
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->registerEcgListener()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 141
    :cond_2
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyStart()I

    .line 144
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startEcgMeasurement()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 146
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->unregisterEcgListener()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 144
    goto :goto_3

    :cond_4
    move v0, v1

    .line 153
    goto :goto_1

    :cond_5
    move v2, v1

    .line 155
    goto :goto_2
.end method

.method public stopReceivingData()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->unregisterEcgListener()V

    .line 163
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TEST_FLAG:Z

    if-eqz v0, :cond_0

    .line 165
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    .line 166
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$DummyDataThread;->interrupt()V

    .line 172
    :goto_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStopped(II)V

    .line 173
    return v2

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyStop()I

    goto :goto_0
.end method
