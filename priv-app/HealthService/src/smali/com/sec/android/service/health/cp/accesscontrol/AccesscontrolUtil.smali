.class public Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;
.super Ljava/lang/Object;
.source "AccesscontrolUtil.java"


# static fields
.field private static final APPLICATION_TABLE:Ljava/lang/String; = "application"

.field public static final APP_ID:Ljava/lang/String; = "application__id"

.field private static final APP_NAME:Ljava/lang/String; = "app_name"

.field private static final DEVMODE:Ljava/lang/String; = "devmode_yn"

.field public static final IS_PLUGIN:Ljava/lang/String; = "is_plugin"

.field public static final NO_PERMISSION:I = 0x0

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final PERMISSION:Ljava/lang/String; = "permission"

.field private static final PERMISSION_TABLE:Ljava/lang/String; = "application_permissions"

.field public static final READ_PERMISSION:I = 0x1

.field public static final READ_WRITE_PERMISSION:I = 0x3

.field private static final SIGNATURES:[Landroid/content/pm/Signature;

.field private static final SIGNATURES_ENG:[Landroid/content/pm/Signature;

.field private static final SIGNATURE_DEBUGKEYSTORE:Landroid/content/pm/Signature;

.field private static final SIGNATURE_DOWNLOADABLE:Landroid/content/pm/Signature;

.field private static final SIGNATURE_ENG_RELEASKEY:Landroid/content/pm/Signature;

.field private static final SIGNATURE_USER_RELEASEKEY:Landroid/content/pm/Signature;

.field private static final TAG:Ljava/lang/String;

.field public static final URI:Ljava/lang/String; = "uri"

.field public static final WRITE_PERMISSION:I = 0x2

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    const-class v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    .line 55
    new-instance v0, Landroid/content/pm/Signature;

    const-string v1, "308204d4308203bca003020102020900e5eff0a8f66d92b3300d06092a864886f70d01010505003081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d301e170d3131303632323132323531335a170d3338313130373132323531335a3081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d30820120300d06092a864886f70d01010105000382010d00308201080282010100e9f1edb42423201dce62e68f2159ed8ea766b43a43d348754841b72e9678ce6b03d06d31532d88f2ef2d5ba39a028de0857983cd321f5b7786c2d3699df4c0b40c8d856f147c5dc54b9d1d671d1a51b5c5364da36fc5b0fe825afb513ec7a2db862c48a6046c43c3b71a1e275155f6c30aed2a68326ac327f60160d427cf55b617230907a84edbff21cc256c628a16f15d55d49138cdf2606504e1591196ed0bdc25b7cc4f67b33fb29ec4dbb13dbe6f3467a0871a49e620067755e6f095c3bd84f8b7d1e66a8c6d1e5150f7fa9d95475dc7061a321aaf9c686b09be23ccc59b35011c6823ffd5874d8fa2a1e5d276ee5aa381187e26112c7d5562703b36210b020103a382010b30820107301d0603551d0e041604145b115b23db35655f9f77f78756961006eebe3a9e3081d70603551d230481cf3081cc80145b115b23db35655f9f77f78756961006eebe3a9ea181a8a481a53081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d820900e5eff0a8f66d92b3300c0603551d13040530030101ff300d06092a864886f70d0101050500038201010039c91877eb09c2c84445443673c77a1219c5c02e6552fa2fbad0d736bc5ab6ebaf0375e520fe9799403ecb71659b23afda1475a34ef4b2e1ffcba8d7ff385c21cb6482540bce3837e6234fd4f7dd576d7fcfe9cfa925509f772c494e1569fe44e6fcd4122e483c2caa2c639566dbcfe85ed7818d5431e73154ad453289fb56b607643919cf534fbeefbdc2009c7fcb5f9b1fa97490462363fa4bedc5e0b9d157e448e6d0e7cfa31f1a2faa9378d03c8d1163d3803bc69bf24ec77ce7d559abcaf8d345494abf0e3276f0ebd2aa08e4f4f6f5aaea4bc523d8cc8e2c9200ba551dd3d4e15d5921303ca9333f42f992ddb70c2958e776c12d7e3b7bd74222eb5c7a"

    invoke-direct {v0, v1}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_USER_RELEASEKEY:Landroid/content/pm/Signature;

    .line 56
    new-instance v0, Landroid/content/pm/Signature;

    const-string v1, "308201e53082014ea00302010202044f54468b300d06092a864886f70d01010505003037310b30090603550406130255533110300e060355040a1307416e64726f6964311630140603550403130d416e64726f6964204465627567301e170d3132303330353034353232375a170d3432303232363034353232375a3037310b30090603550406130255533110300e060355040a1307416e64726f6964311630140603550403130d416e64726f696420446562756730819f300d06092a864886f70d010101050003818d00308189028181008a53be36d02befe1d152724281630bd1c42eff0edf5fdca8eb944f536ab3f54dca9b22cfb421b37706a4ad259101815723202b359250cf6c59905032798273462bfa3f9f1881f7475ee5b25849edefac81085815f42383a44cb2be1bfd5c1f049ef42f5818f35fe0b1131c769cee347d558395a5fa87c3d425b2b9c819cf91870203010001300d06092a864886f70d0101050500038181000512992268a01e0941481931f3f9b6647fbe25ee0bc9648f35d56c55f8cfa6c935fb3d435125fd60ef566769ac7e64fe2823409461ca7a04570c43baaab3fb877bf3a6a8dd9ef7e69944f65b0e5e36f2ac2bf085fdeda063898855ea2ce84c60655d824844fe1659a77c12604c3fb84d41df6f1a7705a1b9962ac2fdc9933122"

    invoke-direct {v0, v1}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_DEBUGKEYSTORE:Landroid/content/pm/Signature;

    .line 57
    new-instance v0, Landroid/content/pm/Signature;

    const-string v1, "308204a830820390a003020102020900936eacbe07f201df300d06092a864886f70d0101050500308194310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e20566965773110300e060355040a1307416e64726f69643110300e060355040b1307416e64726f69643110300e06035504031307416e64726f69643122302006092a864886f70d0109011613616e64726f696440616e64726f69642e636f6d301e170d3038303232393031333334365a170d3335303731373031333334365a308194310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e20566965773110300e060355040a1307416e64726f69643110300e060355040b1307416e64726f69643110300e06035504031307416e64726f69643122302006092a864886f70d0109011613616e64726f696440616e64726f69642e636f6d30820120300d06092a864886f70d01010105000382010d00308201080282010100d6931904dec60b24b1edc762e0d9d8253e3ecd6ceb1de2ff068ca8e8bca8cd6bd3786ea70aa76ce60ebb0f993559ffd93e77a943e7e83d4b64b8e4fea2d3e656f1e267a81bbfb230b578c20443be4c7218b846f5211586f038a14e89c2be387f8ebecf8fcac3da1ee330c9ea93d0a7c3dc4af350220d50080732e0809717ee6a053359e6a694ec2cb3f284a0a466c87a94d83b31093a67372e2f6412c06e6d42f15818dffe0381cc0cd444da6cddc3b82458194801b32564134fbfde98c9287748dbf5676a540d8154c8bbca07b9e247553311c46b9af76fdeeccc8e69e7c8a2d08e782620943f99727d3c04fe72991d99df9bae38a0b2177fa31d5b6afee91f020103a381fc3081f9301d0603551d0e04160414485900563d272c46ae118605a47419ac09ca8c113081c90603551d230481c13081be8014485900563d272c46ae118605a47419ac09ca8c11a1819aa48197308194310b3009060355040613025553311330110603550408130a43616c69666f726e6961311630140603550407130d4d6f756e7461696e20566965773110300e060355040a1307416e64726f69643110300e060355040b1307416e64726f69643110300e06035504031307416e64726f69643122302006092a864886f70d0109011613616e64726f696440616e64726f69642e636f6d820900936eacbe07f201df300c0603551d13040530030101ff300d06092a864886f70d010105050003820101007aaf968ceb50c441055118d0daabaf015b8a765a27a715a2c2b44f221415ffdace03095abfa42df70708726c2069e5c36eddae0400be29452c084bc27eb6a17eac9dbe182c204eb15311f455d824b656dbe4dc2240912d7586fe88951d01a8feb5ae5a4260535df83431052422468c36e22c2a5ef994d61dd7306ae4c9f6951ba3c12f1d1914ddc61f1a62da2df827f603fea5603b2c540dbd7c019c36bab29a4271c117df523cdbc5f3817a49e0efa60cbd7f74177e7a4f193d43f4220772666e4c4d83e1bd5a86087cf34f2dec21e245ca6c2bb016e683638050d2c430eea7c26a1c49d3760a58ab7f1a82cc938b4831384324bd0401fa12163a50570e684d"

    invoke-direct {v0, v1}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_ENG_RELEASKEY:Landroid/content/pm/Signature;

    .line 58
    new-instance v0, Landroid/content/pm/Signature;

    const-string v1, "308204d4308203bca003020102020900d20995a79c0daad6300d06092a864886f70d01010505003081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d301e170d3131303632323132323531325a170d3338313130373132323531325a3081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d30820120300d06092a864886f70d01010105000382010d00308201080282010100c986384a3e1f2fb206670e78ef232215c0d26f45a22728db99a44da11c35ac33a71fe071c4a2d6825a9b4c88b333ed96f3c5e6c666d60f3ee94c490885abcf8dc660f707aabc77ead3e2d0d8aee8108c15cd260f2e85042c28d2f292daa3c6da0c7bf2391db7841aade8fdf0c9d0defcf77124e6d2de0a9e0d2da746c3670e4ffcdc85b701bb4744861b96ff7311da3603c5a10336e55ffa34b4353eedc85f51015e1518c67e309e39f87639ff178107f109cd18411a6077f26964b6e63f8a70b9619db04306a323c1a1d23af867e19f14f570ffe573d0e3a0c2b30632aaec3173380994be1e341e3a90bd2e4b615481f46db39ea83816448ec35feb1735c1f3020103a382010b30820107301d0603551d0e04160414932c3af70b627a0c7610b5a0e7427d6cfaea3f1e3081d70603551d230481cf3081cc8014932c3af70b627a0c7610b5a0e7427d6cfaea3f1ea181a8a481a53081a2310b3009060355040613024b52311430120603550408130b536f757468204b6f726561311330110603550407130a5375776f6e2043697479311c301a060355040a131353616d73756e6720436f72706f726174696f6e310c300a060355040b1303444d43311530130603550403130c53616d73756e6720436572743125302306092a864886f70d0109011616616e64726f69642e6f734073616d73756e672e636f6d820900d20995a79c0daad6300c0603551d13040530030101ff300d06092a864886f70d01010505000382010100329601fe40e036a4a86cc5d49dd8c1b5415998e72637538b0d430369ac51530f63aace8c019a1a66616a2f1bb2c5fabd6f313261f380e3471623f053d9e3c53f5fd6d1965d7b000e4dc244c1b27e2fe9a323ff077f52c4675e86247aa801187137e30c9bbf01c567a4299db4bf0b25b7d7107a7b81ee102f72ff47950164e26752e114c42f8b9d2a42e7308897ec640ea1924ed13abbe9d120912b62f4926493a86db94c0b46f44c6161d58c2f648164890c512dfb28d42c855bf470dbee2dab6960cad04e81f71525ded46cdd0f359f99c460db9f007d96ce83b4b218ac2d82c48f12608d469733f05a3375594669ccbf8a495544d6c5701e9369c08c810158"

    invoke-direct {v0, v1}, Landroid/content/pm/Signature;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_DOWNLOADABLE:Landroid/content/pm/Signature;

    .line 60
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/content/pm/Signature;

    sget-object v1, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_USER_RELEASEKEY:Landroid/content/pm/Signature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_DOWNLOADABLE:Landroid/content/pm/Signature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_DEBUGKEYSTORE:Landroid/content/pm/Signature;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_ENG_RELEASKEY:Landroid/content/pm/Signature;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURES_ENG:[Landroid/content/pm/Signature;

    .line 61
    new-array v0, v5, [Landroid/content/pm/Signature;

    sget-object v1, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_USER_RELEASEKEY:Landroid/content/pm/Signature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURE_DOWNLOADABLE:Landroid/content/pm/Signature;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURES:[Landroid/content/pm/Signature;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkSignature(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    .line 231
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "checkSignature: packagename "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v9, 0x0

    .line 235
    .local v9, "pkgInfo":Landroid/content/pm/PackageInfo;
    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 236
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    const/16 v14, 0x40

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 247
    :goto_0
    if-eqz v9, :cond_0

    iget-object v13, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-nez v13, :cond_2

    .line 249
    :cond_0
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    const-string v14, "pkgInfo is null"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v13, 0x0

    .line 291
    :goto_1
    return v13

    .line 238
    :cond_1
    :try_start_1
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    const-string v14, "There is problem while getting package info. Context is null"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 240
    :catch_0
    move-exception v4

    .line 242
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "exception occured while getting packageinfo :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 244
    const/4 v13, 0x0

    goto :goto_1

    .line 253
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    iget-object v11, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 256
    .local v11, "signatures":[Landroid/content/pm/Signature;
    sget-object v13, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v14, "eng"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_3

    sget-object v13, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v14, "userdebug"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 258
    :cond_3
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    const-string v14, " SIGNATURES_ENG "

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    sget-object v12, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURES_ENG:[Landroid/content/pm/Signature;

    .line 267
    .local v12, "storedSignature":[Landroid/content/pm/Signature;
    :goto_2
    move-object v2, v11

    .local v2, "arr$":[Landroid/content/pm/Signature;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v6, v5

    .end local v2    # "arr$":[Landroid/content/pm/Signature;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .local v6, "i$":I
    :goto_3
    if-ge v6, v7, :cond_7

    aget-object v10, v2, v6

    .line 269
    .local v10, "signature":Landroid/content/pm/Signature;
    move-object v3, v12

    .local v3, "arr$":[Landroid/content/pm/Signature;
    array-length v8, v3

    .local v8, "len$":I
    const/4 v5, 0x0

    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :goto_4
    if-ge v5, v8, :cond_6

    aget-object v1, v3, v5

    .line 271
    .local v1, "allowedSignature":Landroid/content/pm/Signature;
    invoke-virtual {v1, v10}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 273
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    const-string v14, " signature matched "

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v13, 0x1

    goto :goto_1

    .line 263
    .end local v1    # "allowedSignature":Landroid/content/pm/Signature;
    .end local v3    # "arr$":[Landroid/content/pm/Signature;
    .end local v5    # "i$":I
    .end local v8    # "len$":I
    .end local v10    # "signature":Landroid/content/pm/Signature;
    .end local v12    # "storedSignature":[Landroid/content/pm/Signature;
    :cond_4
    sget-object v12, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->SIGNATURES:[Landroid/content/pm/Signature;

    .restart local v12    # "storedSignature":[Landroid/content/pm/Signature;
    goto :goto_2

    .line 269
    .restart local v1    # "allowedSignature":Landroid/content/pm/Signature;
    .restart local v3    # "arr$":[Landroid/content/pm/Signature;
    .restart local v5    # "i$":I
    .restart local v8    # "len$":I
    .restart local v10    # "signature":Landroid/content/pm/Signature;
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 267
    .end local v1    # "allowedSignature":Landroid/content/pm/Signature;
    :cond_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    .end local v5    # "i$":I
    .restart local v6    # "i$":I
    goto :goto_3

    .line 280
    .end local v3    # "arr$":[Landroid/content/pm/Signature;
    .end local v8    # "len$":I
    .end local v10    # "signature":Landroid/content/pm/Signature;
    :cond_7
    array-length v13, v11

    if-lez v13, :cond_8

    .line 282
    move-object v2, v11

    .restart local v2    # "arr$":[Landroid/content/pm/Signature;
    array-length v7, v2

    .restart local v7    # "len$":I
    const/4 v5, 0x0

    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :goto_5
    if-ge v5, v7, :cond_9

    aget-object v10, v2, v5

    .line 284
    .restart local v10    # "signature":Landroid/content/pm/Signature;
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " signature : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 289
    .end local v2    # "arr$":[Landroid/content/pm/Signature;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    .end local v10    # "signature":Landroid/content/pm/Signature;
    .restart local v6    # "i$":I
    :cond_8
    sget-object v13, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->TAG:Ljava/lang/String;

    const-string v14, " no signatures"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 291
    .end local v6    # "i$":I
    .restart local v5    # "i$":I
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_1
.end method

.method public static getCallerPackage(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 411
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 412
    .local v0, "caller":I
    if-nez v0, :cond_0

    .line 414
    const/4 v2, 0x0

    .line 417
    :goto_0
    return-object v2

    .line 416
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 417
    .local v1, "pkgName":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    goto :goto_0
.end method
