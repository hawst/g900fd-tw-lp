.class Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;
.super Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;
.source "PrivilegeSensorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/PrivilegeSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public checkAvailability(III)I
    .locals 4
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 298
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 300
    const-string v2, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v3, "[SensorListener] checkAvailability : Security exception - Caller application not privileged"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    :cond_0
    :goto_0
    return v1

    .line 305
    :cond_1
    const/4 v0, 0x0

    .line 307
    .local v0, "bAvialable":Z
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->isFeatureEnabled(II)Z
    invoke-static {v2, p1, p3}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$400(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;II)Z

    move-result v0

    .line 309
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public close(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 2
    .param p1, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 372
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 374
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "[SensorListener] close : Security exception - Caller application not privileged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    :goto_0
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getConnectivityType()I

    move-result v1

    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$500(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->close(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V

    goto :goto_0
.end method

.method public getAPIVersion()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 332
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    const-string v0, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v1, "[SensorListener] getAPIVersion : Security exception - Caller application not privileged"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v0, 0x0

    .line 338
    :goto_0
    return v0

    :cond_0
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->API_VERSION:I
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$600()I

    move-result v0

    goto :goto_0
.end method

.method public getConnectedDevices(III)Ljava/util/List;
    .locals 5
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 316
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 318
    const-string v3, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v4, "[SensorListener] getConnectedDevices : Security exception - Caller application not privileged"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :goto_0
    return-object v2

    .line 322
    :cond_0
    iget-object v3, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-static {v3, p1}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$500(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v0

    .line 324
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    new-instance v1, Lcom/sec/android/service/health/sensor/manager/util/Filter;

    invoke-direct {v1, p2, p3, v2}, Lcom/sec/android/service/health/sensor/manager/util/Filter;-><init>(IILjava/lang/String;)V

    .line 326
    .local v1, "filter":Lcom/sec/android/service/health/sensor/manager/util/Filter;
    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;

    move-result-object v2

    goto :goto_0
.end method

.method public registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z
    .locals 4
    .param p1, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "l"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 346
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 348
    const-string v2, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v3, "[SensorListener] registerListener : Security exception - Caller application not privileged"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :cond_0
    :goto_0
    return v1

    .line 352
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getConnectivityType()I

    move-result v3

    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$500(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3, p2}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)I

    move-result v0

    .line 353
    .local v0, "statusCode":I
    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z
    .locals 4
    .param p1, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 359
    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$300()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->getCallerPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/service/health/cp/accesscontrol/AccesscontrolUtil;->checkSignature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 361
    const-string v2, "[PrivHealthSensor]PrivilegeSensorService"

    const-string v3, "[SensorListener] unregisterListener : Security exception - Caller application not privileged"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_0
    :goto_0
    return v1

    .line 365
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/PrivilegeSensorService$1;->this$0:Lcom/sec/android/service/health/sensor/PrivilegeSensorService;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getConnectivityType()I

    move-result v3

    # invokes: Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->getSensorManager(I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-static {v2, v3}, Lcom/sec/android/service/health/sensor/PrivilegeSensorService;->access$500(Lcom/sec/android/service/health/sensor/PrivilegeSensorService;I)Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)I

    move-result v0

    .line 366
    .local v0, "statusCode":I
    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method
