.class public interface abstract Lcom/sec/android/service/health/sensor/manager/ISensorListener;
.super Ljava/lang/Object;
.source "ISensorListener.java"


# virtual methods
.method public abstract onDataReceived(Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract onDataReceived([Landroid/os/Bundle;Landroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/os/Bundle;",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract onDataStarted(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation
.end method

.method public abstract onDataStopped(IILandroid/os/RemoteCallbackList;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            "II)V"
        }
    .end annotation
.end method
