.class public Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
.super Ljava/lang/Object;
.source "HeartRateMonitorHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;
    }
.end annotation


# static fields
.field private static final RAW_DATA_INTERVAL_CNT:I = 0x5

.field private static final TAG:Ljava/lang/String;

.field private static final TEST_FLAG:Z = false

.field private static final TYPE_BASE:I = 0x10000

.field private static final TYPE_HRM_BIO:I = 0x1001a

.field public static final TYPE_HRM_RAW:I = 0x10019


# instance fields
.field HRMListener:Landroid/hardware/SensorEventListener;

.field HRM_rawListener:Landroid/hardware/SensorEventListener;

.field private bListenerRegistered:Z

.field mContext:Landroid/content/Context;

.field private mDummyDataThread:Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$DummyDataThread;

.field private mHeartRate:I

.field mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mHrmManager:Landroid/hardware/SensorManager;

.field private mHrmSensor:Landroid/hardware/Sensor;

.field private mHrmSensorRAW:Landroid/hardware/Sensor;

.field private mInterval:J

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field private mSNR:F

.field private mSNRUNIT:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PrivHealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    .line 24
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    .line 25
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensorRAW:Landroid/hardware/Sensor;

    .line 26
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensor:Landroid/hardware/Sensor;

    .line 27
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 28
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F

    .line 38
    const v0, 0x4baf1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNRUNIT:I

    .line 227
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRMListener:Landroid/hardware/SensorEventListener;

    .line 298
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$2;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler$2;-><init>(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRM_rawListener:Landroid/hardware/SensorEventListener;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
    .param p1, "x1"    # J

    .prologue
    .line 19
    iput-wide p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mInterval:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
    .param p1, "x1"    # F

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mSNR:F

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;

    .prologue
    .line 19
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;
    .param p1, "x1"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHeartRate:I

    return p1
.end method

.method private registerHRMListener()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 174
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    const v3, 0x10019

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensorRAW:Landroid/hardware/Sensor;

    .line 179
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRM_rawListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensorRAW:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 180
    .local v0, "rawHRM":Z
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HRM sensor TYPE_HRM_RAW was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_2

    const-string v2, ""

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "registered"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    const v3, 0x1001a

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensor:Landroid/hardware/Sensor;

    .line 184
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRMListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v1

    .line 185
    .local v1, "rawHRMBio":Z
    sget-object v3, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HRM sensor TYPE_HRM_BIO was "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v1, :cond_3

    const-string v2, ""

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "registered"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    if-nez v0, :cond_0

    if-eqz v1, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    .line 188
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HRM sensor bListenerRegistered :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local v0    # "rawHRM":Z
    .end local v1    # "rawHRMBio":Z
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    return v2

    .line 180
    .restart local v0    # "rawHRM":Z
    :cond_2
    const-string v2, "not "

    goto :goto_0

    .line 185
    .restart local v1    # "rawHRMBio":Z
    :cond_3
    const-string v2, "not "

    goto :goto_1

    .line 187
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private unregisterHRMListener()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 196
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterHRMListener : current state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    if-eqz v1, :cond_0

    .line 200
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    const-string v2, "unregisterSensorManager is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRM_rawListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->HRMListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 203
    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    .line 204
    const/4 v0, 0x1

    .line 208
    :cond_0
    return v0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->unregisterHRMListener()Z

    .line 86
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mContext:Landroid/content/Context;

    .line 216
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 217
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 218
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mContext:Landroid/content/Context;

    .line 77
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmManager:Landroid/hardware/SensorManager;

    .line 78
    return-void
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;)V
    .locals 0
    .param p1, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 101
    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 0
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mHrmDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 91
    return-void
.end method

.method public startReceivingData()I
    .locals 4

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 113
    .local v0, "ret":I
    iget-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->bListenerRegistered:Z

    if-nez v1, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->registerHRMListener()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    const/4 v0, 0x0

    .line 130
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v1, :cond_1

    .line 132
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 135
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() is called. ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    return v0

    .line 126
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 4

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 156
    .local v0, "ret":I
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->unregisterHRMListener()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    const/4 v0, 0x0

    .line 162
    :goto_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStopped(II)V

    .line 167
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/hrm/HeartRateMonitorHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopReceivingData() is called. ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return v0

    .line 159
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
