.class public Lcom/sec/android/service/health/upgrade/PackageInstallService;
.super Landroid/app/Service;
.source "PackageInstallService.java"

# interfaces
.implements Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;


# static fields
.field public static final ACTION_PACKAGE_INSTALL:Ljava/lang/String; = "com.sec.android.service.health.upgrade.PackageInstall"

.field public static final PACKAGE_NAME_HEALTH_SERVICE:Ljava/lang/String; = "com.sec.android.service.health"

.field private static final TAG:Ljava/lang/String; = "SHealthUpgrade(PackageInstallService)"


# instance fields
.field mBinder:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;

.field private mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

.field private mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;

.field private mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    .line 21
    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    .line 87
    new-instance v0, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/upgrade/PackageInstallService$1;-><init>(Lcom/sec/android/service/health/upgrade/PackageInstallService;)V

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mBinder:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstallService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/upgrade/PackageInstallService;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/PackageInstaller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/upgrade/PackageInstallService;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/upgrade/PackageInstallService;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/upgrade/PackageInstallService;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/service/health/upgrade/PackageInstallService;Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;)Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/upgrade/PackageInstallService;
    .param p1, "x1"    # Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    return-object p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "onBind()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 29
    const-string v1, "com.sec.android.service.health.upgrade.PackageInstall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mBinder:Lcom/sec/android/service/health/upgrade/IPackageInstallInterface$Stub;

    .line 33
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 39
    iput-object p0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    .line 40
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :try_start_0
    new-instance v1, Lcom/sec/android/service/health/upgrade/PackageInstaller;

    invoke-direct {v1, p0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    .line 44
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    invoke-virtual {v1, p0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->setOnInstalledPackaged(Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    .line 50
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 47
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mContext:Lcom/sec/android/service/health/upgrade/PackageInstallService;

    .line 84
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 85
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 57
    if-nez p1, :cond_0

    .line 58
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "onStartCommand() : Intent is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    .line 77
    :goto_0
    return v1

    .line 62
    :cond_0
    const-string v1, "info_shealth_deleted"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 63
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "onStartCommand() : command = true. Deleting health service."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mPackageInstaller:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    const-string v2, "com.sec.android.service.health"

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->deletePackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 77
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 68
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 70
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    .line 74
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_1
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    const-string v2, "onStartCommand() : command = false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public packageDeleteFailed(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 220
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageDeleteFailed() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    if-eqz v1, :cond_0

    .line 224
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    invoke-interface {v1, p1}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageDeleteFailed(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public packageDeleted(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 207
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageDeleted() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    if-eqz v1, :cond_0

    .line 211
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageDeleted(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public packageInstallFailed(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 194
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageInstallFailed() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    if-eqz v1, :cond_0

    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    invoke-interface {v1, p1}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageInstallFailed(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public packageInstalled(Ljava/lang/String;I)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 181
    const-string v1, "SHealthUpgrade(PackageInstallService)"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "packageInstalled() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    if-eqz v1, :cond_0

    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/upgrade/PackageInstallService;->mCallback:Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;

    invoke-interface {v1, p1, p2}, Lcom/sec/android/service/health/upgrade/IRemoteServiceCallback;->packageInstalled(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
