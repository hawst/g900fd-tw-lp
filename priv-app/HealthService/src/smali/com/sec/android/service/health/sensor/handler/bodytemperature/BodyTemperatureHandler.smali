.class public Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;
.super Ljava/lang/Object;
.source "BodyTemperatureHandler.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;


# static fields
.field private static final CACHED_DATA_INTERVAL_CNT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static TEST_FLAG:Z = false

.field private static final TYPE_BASE:I = 0x10000

.field public static final TYPE_BODY_TEMPERATURE:I = 0x1001e


# instance fields
.field private bListenerRegistered:Z

.field finalData:[Landroid/os/Bundle;

.field private mBodyTemperatureSensor:Landroid/hardware/Sensor;

.field mContext:Landroid/content/Context;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field mbodytemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private raw_data_cnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TEST_FLAG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    .line 25
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    .line 26
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mBodyTemperatureSensor:Landroid/hardware/Sensor;

    .line 31
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    .line 33
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 34
    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mbodytemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Bundle;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    return-void
.end method

.method private registerTemperatureListener()Z
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001e

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mBodyTemperatureSensor:Landroid/hardware/Sensor;

    .line 158
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mBodyTemperatureSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    .line 159
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Temperature sensor was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "registered"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    return v0

    .line 159
    :cond_1
    const-string v0, "not "

    goto :goto_0
.end method

.method private unregisterTemperatureListener()V
    .locals 3

    .prologue
    .line 169
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterTemperatureListener : current state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 173
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    const-string v1, "unregisterSensorManager is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    .line 177
    :cond_0
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->unregisterTemperatureListener()V

    .line 57
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mbodytemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mContext:Landroid/content/Context;

    .line 45
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mbodytemperatureDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 46
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 47
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mSensorManager:Landroid/hardware/SensorManager;

    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 119
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 124
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const v1, 0x1001e

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    if-ge v0, v4, :cond_2

    .line 126
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BodyTemperature onSensorChanged 3rd element : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    aput-object v2, v0, v1

    .line 131
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "temperature"

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "time_stamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "temperature_unit"

    const v2, 0x27101

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    iget v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    aget-object v0, v0, v1

    const-string v1, "temperature_type"

    const v2, 0x11171

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 137
    :cond_0
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    .line 139
    iget v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    if-lt v0, v4, :cond_2

    .line 141
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->finalData:[Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 144
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    .line 147
    :cond_2
    return-void
.end method

.method public startReceivingData()I
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v1, 0x0

    .line 62
    iput v1, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    .line 63
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    if-nez v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->registerTemperatureListener()Z

    .line 66
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->TEST_FLAG:Z

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 69
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;-><init>(Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;)V

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler$1;->start()V

    .line 96
    :goto_0
    return v1

    .line 95
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v2, v3, v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public stopReceivingData()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->unregisterTemperatureListener()V

    .line 103
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->raw_data_cnt:I

    .line 104
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/bodytemperature/BodyTemperatureHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStopped(II)V

    .line 105
    return v2
.end method
