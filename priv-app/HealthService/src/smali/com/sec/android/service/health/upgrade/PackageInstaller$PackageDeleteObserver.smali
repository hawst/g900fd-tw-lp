.class Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "PackageInstaller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/upgrade/PackageInstaller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/upgrade/PackageInstaller;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageDeleteObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageDeleteObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstaller;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstaller;)Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/service/health/upgrade/PackageInstaller$PackageDeleteObserver;->this$0:Lcom/sec/android/service/health/upgrade/PackageInstaller;

    # getter for: Lcom/sec/android/service/health/upgrade/PackageInstaller;->mOnPackageInstalled:Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;
    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/PackageInstaller;->access$000(Lcom/sec/android/service/health/upgrade/PackageInstaller;)Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/service/health/upgrade/IPackageInstallEventListener;->packageDeleted(Ljava/lang/String;I)V

    .line 121
    :cond_0
    return-void
.end method
