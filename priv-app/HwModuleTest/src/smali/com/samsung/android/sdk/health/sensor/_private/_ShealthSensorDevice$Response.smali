.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;",
            ">;"
        }
    .end annotation
.end field

.field public static final ERROR_ALREADY_CONTROLLED:I = 0x8

.field public static final ERROR_CANCEL:I = 0x6

.field public static final ERROR_DATA_NOT_FOUND:I = 0x6a

.field public static final ERROR_DEPENDANCY_APK_NOT_INSTALLED:I = 0x6b

.field public static final ERROR_DEVICE_BUSY:I = 0x9

.field public static final ERROR_NOT_JOINED:I = 0x7

.field public static final ERROR_NO_RESPONSE:I = 0x3

.field public static final ERROR_SENSOR_SEARCH_FAILED:I = 0x69

.field public static final ERROR_WRONG_DATA_FORMAT:I = 0x5

.field public static final ERROR_WRONG_REQUEST:I = 0xa


# instance fields
.field private commandId:Ljava/lang/String;

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private errorCode:I

.field private errorDescription:Ljava/lang/String;

.field private event:I

.field private response:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 713
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>()V

    .line 731
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "errorCode"    # I
    .param p2, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 740
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>()V

    .line 741
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    .line 742
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    .line 743
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 750
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>()V

    .line 751
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->readFromParcel(Landroid/os/Parcel;)V

    .line 752
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 756
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    .line 757
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    .line 758
    const-class v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    .line 759
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    .line 760
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    .line 761
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    .line 762
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    const-class v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 768
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 690
    const/4 v0, 0x0

    return v0
.end method

.method public getCommandId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 777
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    return v0
.end method

.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getEvent()I
    .locals 1

    .prologue
    .line 852
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    return v0
.end method

.method public getResponse()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    return-object v0
.end method

.method public setCommandId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 823
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    .line 824
    return-void
.end method

.method public setErrorCode(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 787
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    .line 788
    return-void
.end method

.method public setErrorDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 805
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    .line 806
    return-void
.end method

.method public setEvent(I)V
    .locals 0
    .param p1, "eventType"    # I

    .prologue
    .line 861
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    .line 862
    return-void
.end method

.method public setResponse(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "response"    # Landroid/os/Bundle;

    .prologue
    .line 843
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    .line 844
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 702
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 703
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->errorDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 704
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 705
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->commandId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 706
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->event:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 707
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->response:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 708
    return-void
.end method
