.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$GoalColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GoalColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CURRENT_VALUE:Ljava/lang/String; = "current_value"

.field public static final GOAL_SUBTYPE:Ljava/lang/String; = "goal_subtype"

.field public static final GOAL_TYPE:Ljava/lang/String; = "goal_type"

.field public static final PERIOD:Ljava/lang/String; = "period"

.field public static final SET_TIME:Ljava/lang/String; = "set_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field public static final VALUE:Ljava/lang/String; = "value"
