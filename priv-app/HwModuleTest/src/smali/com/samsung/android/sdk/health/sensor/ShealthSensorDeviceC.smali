.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.source "ShealthSensorDeviceC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BodyTemperatureC;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$ElectroCardiogramC;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BloodGlucoseC;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$PulseOximeterC;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$HeartRateMonitorC;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;
    }
.end annotation


# instance fields
.field private bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field eventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

.field private healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field private mContext:Landroid/content/Context;

.field mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

.field private mPid:I

.field mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

.field private mRecord:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Landroid/content/Context;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .param p2, "uContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 265
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    .line 56
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 58
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 266
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthSensorDeviceC()  "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    if-nez p1, :cond_0

    .line 270
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    .line 273
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mStateStack:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    :cond_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPid:I

    .line 278
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->loadDataManager()V

    .line 279
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPid:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mRecord:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
    .param p1, "x1"    # [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    return-object p1
.end method

.method private insertOrUpdateDeviceId()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    .line 345
    const/4 v8, 0x0

    .line 349
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getConnectivityType()I

    move-result v0

    if-ne v0, v13, :cond_1

    .line 351
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 352
    .local v6, "android_id":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 359
    .end local v6    # "android_id":Ljava/lang/String;
    .local v7, "connectedDeviceId":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 360
    .local v10, "userDeviceCursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 362
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 364
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 451
    :cond_0
    :goto_1
    return-void

    .line 356
    .end local v7    # "connectedDeviceId":Ljava/lang/String;
    .end local v10    # "userDeviceCursor":Landroid/database/Cursor;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getId()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "connectedDeviceId":Ljava/lang/String;
    goto :goto_0

    .line 386
    .restart local v10    # "userDeviceCursor":Landroid/database/Cursor;
    :cond_2
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 388
    .local v11, "values":Landroid/content/ContentValues;
    if-nez v8, :cond_5

    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getConnectivityType()I

    move-result v0

    if-ne v0, v13, :cond_3

    .line 393
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 394
    .restart local v6    # "android_id":Ljava/lang/String;
    const-string v0, "device_id"

    invoke-virtual {v11, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 396
    .local v9, "deviceId":Ljava/lang/String;
    const-string v0, "_id"

    invoke-virtual {v11, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v0, "connectivity_type"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getConnectivityType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 398
    const-string v0, "device_type"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 399
    const-string v0, "model"

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v0, "custom_name"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    const-string v0, "device_group_type"

    const v1, 0x57e41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 402
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "Adding to user_device DEVICE_ID : android_id"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding to user_device Model : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "Adding to user_device Group : Mobile"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    .end local v6    # "android_id":Ljava/lang/String;
    .end local v9    # "deviceId":Ljava/lang/String;
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446
    :goto_3
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 448
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 408
    :cond_3
    :try_start_2
    const-string v0, "_id"

    invoke-virtual {v11, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string v0, "connectivity_type"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getConnectivityType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    const-string v0, "device_type"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string v0, "device_id"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v0, "custom_name"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 426
    :pswitch_0
    const-string v0, "device_group_type"

    const v1, 0x57e42

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 427
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "Adding to user_device Group : External"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :goto_4
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding to user_device : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 446
    .end local v7    # "connectedDeviceId":Ljava/lang/String;
    .end local v10    # "userDeviceCursor":Landroid/database/Cursor;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 448
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 422
    .restart local v7    # "connectedDeviceId":Ljava/lang/String;
    .restart local v10    # "userDeviceCursor":Landroid/database/Cursor;
    .restart local v11    # "values":Landroid/content/ContentValues;
    :pswitch_1
    :try_start_3
    const-string v0, "device_group_type"

    const v1, 0x57e43

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 423
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "Adding to user_device Group : Companion"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 438
    :cond_5
    const-string v0, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 439
    const-string v0, "custom_name"

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 441
    .local v12, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v11, v12, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x2723
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private loadDataManager()V
    .locals 7

    .prologue
    .line 285
    const-string v0, "com.sec.android.service.health.cp.database.datahandler.DataManagerFactory"

    .line 286
    .local v0, "classNameToBeLoaded":Ljava/lang/String;
    const/4 v3, 0x0

    .line 289
    .local v3, "myClass":Ljava/lang/Class;
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 290
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class loaded"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    const/4 v1, 0x0

    .line 299
    .local v1, "dataManagerInstance":Ljava/lang/Object;
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .line 300
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class instance created"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    .line 313
    const/4 v4, 0x0

    .line 315
    .local v4, "myMethod":Ljava/lang/reflect/Method;
    :try_start_2
    const-string v5, "getDataManager"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 316
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class method retrieved"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3

    .line 325
    const/4 v5, 0x0

    :try_start_3
    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    .line 327
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Datamanager retreivedn"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_6

    .line 339
    .end local v1    # "dataManagerInstance":Ljava/lang/Object;
    .end local v4    # "myMethod":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v2

    .line 292
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class NOT loaded"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 301
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v2

    .line 303
    .local v2, "e":Ljava/lang/InstantiationException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class instance EXCEPTION"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 306
    .end local v2    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v2

    .line 308
    .local v2, "e":Ljava/lang/IllegalAccessException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class instance IllegalAccessException"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 317
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    .restart local v1    # "dataManagerInstance":Ljava/lang/Object;
    .restart local v4    # "myMethod":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v2

    .line 319
    .local v2, "e":Ljava/lang/NoSuchMethodException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Class method NoSuchMethodException"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    invoke-virtual {v2}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 328
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v2

    .line 329
    .local v2, "e":Ljava/lang/IllegalAccessException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Datamanager IllegalAccessException"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 331
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_5
    move-exception v2

    .line 332
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Datamanager IllegalArgumentException"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 334
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6
    move-exception v2

    .line 335
    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v6, "loadDataManager Datamanager InvocationTargetException"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 855
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v3, "ShealthSensorDeviceC close() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 859
    .local v0, "curTop":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_1

    .line 863
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0x10

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1

    .line 865
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 885
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v2, :cond_2

    .line 886
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 869
    :catch_0
    move-exception v1

    .line 871
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 873
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 875
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 877
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 879
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 888
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->close()V
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5

    .line 900
    :goto_1
    return-void

    .line 889
    :catch_3
    move-exception v1

    .line 891
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;->printStackTrace()V

    goto :goto_1

    .line 892
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    :catch_4
    move-exception v1

    .line 894
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 895
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :catch_5
    move-exception v1

    .line 897
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public getConnectivityType()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 571
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 572
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 574
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getConnectivityType() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getConnectivityType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getConnectivityType()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toConnectivityType(I)I

    move-result v0

    return v0
.end method

.method public getDataType()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 534
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v3, :cond_0

    .line 535
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Invalid device"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 537
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    .line 538
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 539
    .local v2, "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_1

    .line 541
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 543
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDataType(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    .end local v0    # "index":I
    :cond_1
    return-object v2
.end method

.method public getId()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 461
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 462
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 494
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 478
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 479
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 483
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 558
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 507
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getDeviceType()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDeviceType(I)I

    move-result v0

    return v0
.end method

.method public isConnected()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 590
    const/4 v0, 0x1

    return v0
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 607
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v3, "ShealthSensorDeviceC join() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    if-nez p1, :cond_0

    .line 610
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 612
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v2, :cond_1

    .line 613
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 616
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 618
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 619
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    .line 620
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    .line 622
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "callBackHandlerWorker"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 625
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 626
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    .line 629
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 631
    .local v1, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 632
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->onConnected(I)V

    .line 633
    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->eventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    .line 634
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->insertOrUpdateDeviceId()V

    .line 635
    return-void

    .line 622
    .end local v0    # "commandWorker":Landroid/os/HandlerThread;
    .end local v1    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public leave()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 646
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, " ShealthSensorDeviceC leave() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 648
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 652
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->eventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->onDisconnected(I)V

    .line 654
    return-void
.end method

.method public record()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 728
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthSensorDeviceC record() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 730
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mRecord:Z

    .line 733
    return-void
.end method

.method public record(J)V
    .locals 3
    .param p1, "exerciseId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 763
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDeviceC record(exerciseId) is called - exerciseId is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mRecord:Z

    .line 767
    return-void
.end method

.method public rename(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 669
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthSensorDeviceC rename() is called - name is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    return-void
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 0
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 849
    return-void
.end method

.method public startListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 815
    return-void
.end method

.method public declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 686
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v3, "ShealthSensorDeviceC startReceivingData() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    if-nez p1, :cond_0

    .line 689
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 686
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 691
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v2, :cond_1

    .line 692
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 694
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getState()I

    move-result v2

    if-nez v2, :cond_2

    .line 695
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Wrong State errorCode : 3"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 697
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mRecord:Z

    .line 699
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 701
    .local v1, "privDataListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;
    :try_start_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->registerListener(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;)Z
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 711
    :try_start_3
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 712
    monitor-exit p0

    return-void

    .line 702
    :catch_0
    move-exception v0

    .line 704
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    .line 705
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Service not bound"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 706
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :catch_1
    move-exception v0

    .line 708
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;->printStackTrace()V

    .line 709
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Device not supported"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public stopListeningData()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 828
    return-void
.end method

.method public declared-synchronized stopReceivingData()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 779
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthSensorDeviceC stopReceivingData() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    if-nez v1, :cond_0

    .line 782
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 779
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 784
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getState()I

    move-result v1

    if-nez v1, :cond_1

    .line 785
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Wrong State errorCode : 3"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 788
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPrivDevice:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->unregisterListener()Z
    :try_end_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 798
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 799
    monitor-exit p0

    return-void

    .line 790
    :catch_0
    move-exception v0

    .line 791
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    .line 792
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 793
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :catch_1
    move-exception v0

    .line 795
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;->printStackTrace()V

    .line 796
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Device not supported"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
