.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ActivityType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DistanceUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GenderType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeightUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$WeightUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Profile"
.end annotation


# instance fields
.field public activity:I

.field public activityClass:I

.field public age:I

.field public birthday:J

.field public distanceUnit:I

.field public gender:I

.field public height:F

.field public heightUnit:I

.field public name:Ljava/lang/String;

.field public weight:F

.field public weightUnit:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    const v2, 0x7fffffff

    .line 2770
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2773
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->height:F

    .line 2775
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->weight:F

    .line 2777
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->gender:I

    .line 2779
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->age:I

    .line 2781
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->distanceUnit:I

    .line 2783
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->weightUnit:I

    .line 2785
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->heightUnit:I

    .line 2787
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->activityClass:I

    .line 2789
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->birthday:J

    .line 2797
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;->activity:I

    return-void
.end method
