.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WrapperSensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0

    .prologue
    .line 2880
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 2885
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2886
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onConnected(I)V

    .line 2887
    :cond_0
    return-void
.end method

.method public onLeft(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 2892
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2893
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onDisconnected(I)V

    .line 2894
    :cond_0
    return-void
.end method

.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 2920
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2921
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .line 2922
    :cond_0
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 2927
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2928
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V

    .line 2929
    :cond_0
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 1
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 2906
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2907
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V

    .line 2908
    :cond_0
    return-void
.end method

.method public onStarted(II)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2913
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2914
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onStarted(II)V

    .line 2915
    :cond_0
    return-void
.end method

.method public onStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 2899
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2900
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onStateChanged(I)V

    .line 2901
    :cond_0
    return-void
.end method

.method public onStopped(II)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2934
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2935
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onStopped(II)V

    .line 2936
    :cond_0
    return-void
.end method
