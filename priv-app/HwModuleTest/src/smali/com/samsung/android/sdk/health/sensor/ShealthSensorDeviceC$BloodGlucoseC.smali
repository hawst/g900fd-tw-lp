.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BloodGlucoseC;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodGlucoseC"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toBulkBloodGlucose([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;
    .locals 6
    .param p0, "data"    # [Landroid/os/Bundle;

    .prologue
    .line 951
    if-eqz p0, :cond_1

    .line 953
    array-length v3, p0

    new-array v0, v3, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    .line 954
    .local v0, "bloodGlucose":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    .line 956
    aget-object v1, p0, v2

    .line 957
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 959
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>()V

    aput-object v3, v0, v2

    .line 960
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "glucose_sensor_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseSensorState:I

    .line 961
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "glucose_error_detail"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseErrorDetail:I

    .line 962
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "glucose"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucose:F

    .line 963
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "time_stamp"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->time:J

    .line 964
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "glucose_unit"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->glucoseUnit:I

    .line 965
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string v5, "sample_type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;->sampleType:I

    .line 954
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 970
    .end local v0    # "bloodGlucose":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "index":I
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method
