.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$SleepColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SleepColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final AWAKE_COUNT:Ljava/lang/String; = "awake_count"

.field public static final BED_TIME:Ljava/lang/String; = "bed_time"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final EFFICIENCY:Ljava/lang/String; = "efficiency"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final MOVEMENT:Ljava/lang/String; = "movement"

.field public static final NOISE:Ljava/lang/String; = "noise"

.field public static final QUALITY:Ljava/lang/String; = "quality"

.field public static final RISE_TIME:Ljava/lang/String; = "rise_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
