.class Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;
.super Ljava/lang/Object;
.source "ShealthAccessControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RegistrationCallBack"
.end annotation


# instance fields
.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;I)V
    .locals 0
    .param p2, "error"    # I

    .prologue
    .line 339
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iput p2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->error:I

    .line 341
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->access$500(Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->access$500(Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->error:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;->onRegistrationCompleted(I)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$600(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 349
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;->this$1:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$602(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Landroid/os/Handler;)Landroid/os/Handler;

    .line 350
    return-void
.end method
