.class public interface abstract Lcom/samsung/android/sdk/health/sensor/_SensorService;
.super Ljava/lang/Object;
.source "_SensorService.java"


# virtual methods
.method public abstract checkAvailability(IIILjava/lang/String;)I
.end method

.method public abstract getApplicationContextPackageName()Ljava/lang/String;
.end method

.method public abstract getConnectedDevices(III)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isApplicationPlugin()Z
.end method

.method public abstract isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z
.end method

.method public abstract join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V
.end method

.method public abstract leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
.end method

.method public abstract record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;J)I
.end method

.method public abstract rename(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/String;)I
.end method

.method public abstract request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method public abstract showConfirmation(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)V
.end method

.method public abstract startListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
.end method

.method public abstract startReceiving(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method public abstract startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I
.end method

.method public abstract startScan(IIIILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I
.end method

.method public abstract startScanByDeviceId(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I
.end method

.method public abstract stopListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I
.end method

.method public abstract stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I
.end method

.method public abstract stopScan()V
.end method

.method public abstract updateConfirmation(ZJ)V
.end method
