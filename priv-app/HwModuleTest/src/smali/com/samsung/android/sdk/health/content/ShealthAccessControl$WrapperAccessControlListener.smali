.class Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
.super Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener$Stub;
.source "ShealthAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperAccessControlListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener$Stub;-><init>()V

    .line 318
    iput-object p2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

    .line 319
    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;

    return-object v0
.end method


# virtual methods
.method public onRegistrationComplete(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 327
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mServiceConnetion:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$100(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$300(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$300(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$400(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$600(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 354
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;-><init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;I)V

    .line 355
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$600(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 360
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener$1RegistrationCallBack;
    :cond_1
    return-void

    .line 328
    :catch_0
    move-exception v1

    goto :goto_0
.end method
