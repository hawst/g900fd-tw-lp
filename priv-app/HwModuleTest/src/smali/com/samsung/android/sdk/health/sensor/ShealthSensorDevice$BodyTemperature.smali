.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TemperatureType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TemperatureUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BodyTemperature"
.end annotation


# instance fields
.field public temperature:F

.field public temperatureType:I

.field public temperatureUnit:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 2388
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2391
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperature:F

    .line 2394
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperatureUnit:I

    .line 2397
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;->temperatureType:I

    return-void
.end method
