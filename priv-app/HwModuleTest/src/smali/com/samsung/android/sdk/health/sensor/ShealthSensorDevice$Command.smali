.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Command"
.end annotation


# instance fields
.field private _command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1761
    invoke-direct {p0, v0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1762
    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V
    .locals 0
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    .prologue
    .line 1779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    .line 1781
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    .param p2, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;

    .prologue
    .line 1754
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "commandId"    # Ljava/lang/String;
    .param p2, "params"    # Landroid/os/Bundle;

    .prologue
    .line 1773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    .line 1776
    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    return-object v0
.end method


# virtual methods
.method public getCommandId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1790
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParameters()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1812
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->getParameters()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setCommandId(Ljava/lang/String;)V
    .locals 1
    .param p1, "commandId"    # Ljava/lang/String;

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->setCommandId(Ljava/lang/String;)V

    .line 1803
    return-void
.end method

.method public setParameters(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->setParameters(Landroid/os/Bundle;)V

    .line 1824
    return-void
.end method
