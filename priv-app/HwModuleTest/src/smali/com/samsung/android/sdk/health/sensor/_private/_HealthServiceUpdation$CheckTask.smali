.class Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
.super Landroid/os/AsyncTask;
.source "_HealthServiceUpdation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
    .param p2, "x1"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$1;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 13
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v12, 0x0

    .line 105
    :try_start_0
    const-string v9, "HealthServiceUpdation"

    const-string v10, "start update check"

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 109
    .local v3, "model":Ljava/lang/String;
    const-string v9, "OMAP_SS"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 111
    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->readModelCMCC()Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$100()Ljava/lang/String;

    move-result-object v3

    .line 115
    :cond_0
    const-string v9, "SAMSUNG-"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 117
    const/16 v9, 0x8

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 123
    :cond_1
    const/4 v2, 0x0

    .line 129
    .local v2, "i":Landroid/content/pm/PackageInfo;
    :try_start_1
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$200(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v10, "com.sec.android.service.health"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 139
    const/4 v6, 0x0

    .line 140
    .local v6, "updateFileStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 141
    .local v4, "result":Z
    :try_start_2
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->isLocalTestMode()Z
    invoke-static {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$300(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 143
    new-instance v1, Ljava/io/File;

    const-string v9, "mnt/sdcard/healthservice_update.xml"

    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 144
    .local v1, "fileDummyUpdate":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v9

    if-eqz v9, :cond_2

    .line 148
    :try_start_3
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 149
    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    .local v7, "updateFileStream":Ljava/io/InputStream;
    :try_start_4
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->checkUpdate(Ljava/io/InputStream;)Z
    invoke-static {v9, v7}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$400(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Ljava/io/InputStream;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result v4

    .line 153
    if-eqz v7, :cond_8

    .line 155
    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    move-object v6, v7

    .line 215
    .end local v1    # "fileDummyUpdate":Ljava/io/File;
    .end local v7    # "updateFileStream":Ljava/io/InputStream;
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    :cond_2
    :goto_0
    if-nez v4, :cond_7

    .line 218
    const-string v9, "HealthServiceUpdation"

    const-string v10, "No update~~~~~"

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 248
    .end local v2    # "i":Landroid/content/pm/PackageInfo;
    .end local v3    # "model":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    :goto_1
    return-object v9

    .line 131
    .restart local v2    # "i":Landroid/content/pm/PackageInfo;
    .restart local v3    # "model":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "HealthServiceUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_1

    .line 153
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v1    # "fileDummyUpdate":Ljava/io/File;
    .restart local v4    # "result":Z
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v9

    :goto_2
    if-eqz v6, :cond_3

    .line 155
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    :cond_3
    throw v9
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 237
    .end local v1    # "fileDummyUpdate":Ljava/io/File;
    .end local v2    # "i":Landroid/content/pm/PackageInfo;
    .end local v3    # "model":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 240
    .local v0, "e":Ljava/io/IOException;
    :try_start_6
    const-string v9, "HealthServiceUpdation"

    const-string v10, "Fail Upload"

    invoke-static {v9, v10, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 248
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_1

    .line 162
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "i":Landroid/content/pm/PackageInfo;
    .restart local v3    # "model":Ljava/lang/String;
    .restart local v4    # "result":Z
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    :cond_4
    :try_start_7
    const-string v5, "http://hub.samsungapps.com/product/appCheck.as?"

    .line 164
    .local v5, "server_url":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "appInfo=com.sec.android.service.health@"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 169
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&deviceId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 171
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&mcc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getMCC()Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$500(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 173
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&mnc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getMNC()Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$600(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 175
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&csc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getCSC()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 177
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&openApi="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 180
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->isPD()Z
    invoke-static {v9}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$700(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 183
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&pd=1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 193
    :goto_3
    const-string v9, "HealthServiceUpdation"

    const-string v10, "================================================="

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    const-string v9, "HealthServiceUpdation"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Update request] "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v9, "HealthServiceUpdation"

    const-string v10, "================================================="

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    new-instance v8, Ljava/net/URL;

    invoke-direct {v8, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 202
    .local v8, "url":Ljava/net/URL;
    :try_start_8
    invoke-virtual {v8}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v6

    .line 203
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # invokes: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->checkUpdate(Ljava/io/InputStream;)Z
    invoke-static {v9, v6}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$400(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Ljava/io/InputStream;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result v4

    .line 207
    if-eqz v6, :cond_2

    .line 209
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_0

    .line 244
    .end local v2    # "i":Landroid/content/pm/PackageInfo;
    .end local v3    # "model":Ljava/lang/String;
    .end local v4    # "result":Z
    .end local v5    # "server_url":Ljava/lang/String;
    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    .end local v8    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v9

    throw v9

    .line 189
    .restart local v2    # "i":Landroid/content/pm/PackageInfo;
    .restart local v3    # "model":Ljava/lang/String;
    .restart local v4    # "result":Z
    .restart local v5    # "server_url":Ljava/lang/String;
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    :cond_5
    :try_start_a
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "&pd="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    .line 207
    .restart local v8    # "url":Ljava/net/URL;
    :catchall_2
    move-exception v9

    if-eqz v6, :cond_6

    .line 209
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    :cond_6
    throw v9

    .line 226
    .end local v5    # "server_url":Ljava/lang/String;
    .end local v8    # "url":Ljava/net/URL;
    :cond_7
    const-string v9, "HealthServiceUpdation"

    const-string v10, "Update needed!!!!!"

    invoke-static {v9, v10}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUpdateNeeded:Z
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$802(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Z)Z

    .line 232
    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v9

    goto/16 :goto_1

    .line 153
    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    .restart local v1    # "fileDummyUpdate":Ljava/io/File;
    .restart local v7    # "updateFileStream":Ljava/io/InputStream;
    :catchall_3
    move-exception v9

    move-object v6, v7

    .end local v7    # "updateFileStream":Ljava/io/InputStream;
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    goto/16 :goto_2

    .end local v6    # "updateFileStream":Ljava/io/InputStream;
    .restart local v7    # "updateFileStream":Ljava/io/InputStream;
    :cond_8
    move-object v6, v7

    .end local v7    # "updateFileStream":Ljava/io/InputStream;
    .restart local v6    # "updateFileStream":Ljava/io/InputStream;
    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$900(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$900(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$1000(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$1000(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUpdateNeeded:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$800(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->version:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$1100(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;->onResult(ZLjava/lang/String;)V

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    # getter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$900(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->this$0:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->access$902(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    .line 270
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 99
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
