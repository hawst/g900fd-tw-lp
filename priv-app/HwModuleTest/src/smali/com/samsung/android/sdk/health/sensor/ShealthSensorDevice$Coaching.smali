.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Coaching"
.end annotation


# instance fields
.field public ac:C

.field public lastTrainingLevelUpdate:J

.field public latestExerciseTime:J

.field public latestFeedbackPhraseNumber:I

.field public maxHeartRate:C

.field public maxMET:J

.field public previousToPreviousTrainingLevel:I

.field public previousTrainingLevel:I

.field public recourceRecovery:I

.field public startDate:J

.field public trainingLevel:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v1, 0xffff

    const-wide v2, 0x7fffffffffffffffL

    const v0, 0x7fffffff

    .line 2604
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2606
    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->ac:C

    .line 2608
    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxHeartRate:C

    .line 2613
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxMET:J

    .line 2615
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->recourceRecovery:I

    .line 2617
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->startDate:J

    .line 2622
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->trainingLevel:I

    .line 2624
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->lastTrainingLevelUpdate:J

    .line 2626
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousTrainingLevel:I

    .line 2628
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousToPreviousTrainingLevel:I

    .line 2633
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestFeedbackPhraseNumber:I

    .line 2635
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestExerciseTime:J

    return-void
.end method
