.class Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;
.super Ljava/lang/Object;
.source "ShealthAccessControl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    const/4 v4, 0x6

    .line 381
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Access Control Service Connected"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$702(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    .line 385
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$900(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Upgrade is needed"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$700(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mDataCallback:Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$1000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;->showConfirmation(Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;Ljava/lang/String;)V

    .line 412
    :goto_0
    return-void

    .line 394
    :cond_0
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Upgrade is not needed"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$700(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAppId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$1100(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;->checkSignature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # invokes: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->sendAccessControlRequest()V
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$1200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    .line 410
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 401
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$800()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Illegal access from other apps except Shealth"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$702(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    .line 376
    return-void
.end method
