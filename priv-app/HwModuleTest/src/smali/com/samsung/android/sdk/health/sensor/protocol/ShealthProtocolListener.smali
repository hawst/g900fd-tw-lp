.class public interface abstract Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolListener;
.super Ljava/lang/Object;
.source "ShealthProtocolListener.java"


# virtual methods
.method public abstract onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
.end method

.method public abstract onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
.end method

.method public abstract onDataStarted(I)V
.end method

.method public abstract onDataStopped(II)V
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
.end method

.method public abstract onStateChanged(I)V
.end method

.method public abstract sendRawData([B)I
.end method
