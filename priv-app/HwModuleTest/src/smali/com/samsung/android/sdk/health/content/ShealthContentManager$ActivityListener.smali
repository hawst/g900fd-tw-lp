.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
.super Ljava/lang/Object;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ActivityListener"
.end annotation


# static fields
.field public static final ERROR_ALREADY_REQUESTED:I = 0xa

.field public static final ERROR_BUSY:I = 0x1

.field public static final ERROR_CANCELLED:I = 0x9

.field public static final ERROR_DB_OPERATION:I = 0xd

.field public static final ERROR_INVALID_SYNC_TYPE:I = 0x6

.field public static final ERROR_NETWORK:I = 0x4

.field public static final ERROR_NETWORK_NOT_AVAILABLE:I = 0x3

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_RESPONDING:I = 0x7

.field public static final ERROR_NO_BACKUP_DATA:I = 0xe

.field public static final ERROR_NO_PERMISSION:I = 0xc

.field public static final ERROR_NO_SAMSUNG_ACCOUNT:I = 0x2

.field public static final ERROR_NO_SHEALTH_ACCOUNT:I = 0xf

.field public static final ERROR_PROFILE_NOT_EXIST:I = 0x8

.field public static final ERROR_SAMSUNG_ACCOUNT:I = 0xb

.field public static final ERROR_SERVER:I = 0x5


# virtual methods
.method public abstract onFinished(II)V
.end method

.method public abstract onProgress(II)V
.end method

.method public abstract onStarted(II)V
.end method
