.class public Lcom/samsung/android/sdk/health/content/ShealthProfile;
.super Ljava/lang/Object;
.source "ShealthProfile.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activity:I

.field private activityType:I

.field private birthDate:Ljava/lang/String;

.field private transient context:Landroid/content/Context;

.field private country:Ljava/lang/String;

.field private distanceUnit:I

.field private gender:I

.field private height:F

.field private heightUnit:I

.field private isProfileChanged:Z

.field private name:Ljava/lang/String;

.field private profileCreateTime:J

.field private profileDiscloseYN:Ljava/lang/String;

.field private temperatureUnit:I

.field private updateTime:J

.field private weight:F

.field private weightUnit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 84
    if-nez p1, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 93
    return-void
.end method

.method private checkValidity()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const v4, 0x461c4000    # 10000.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 499
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x32

    if-gt v1, v2, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    const v2, 0x2e635

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    const v2, 0x2e636

    if-ne v1, v2, :cond_3

    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    const v2, 0x249f1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    const v2, 0x249f2

    if-ne v1, v2, :cond_3

    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    const v2, 0x1fbd1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    const v2, 0x1fbd2

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    const v2, 0x1fbd3

    if-ne v1, v2, :cond_3

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf24

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf22

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf21

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf23

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf25

    if-eq v1, v2, :cond_4

    .line 515
    :cond_3
    const/4 v0, 0x0

    .line 517
    :cond_4
    return v0
.end method

.method private convertBooleanToString(Z)Ljava/lang/String;
    .locals 1
    .param p1, "isProfileDisclose"    # Z

    .prologue
    .line 555
    if-eqz p1, :cond_0

    .line 556
    const-string v0, "Y"

    .line 558
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method private convertStringToBoolean(Ljava/lang/String;)Z
    .locals 2
    .param p1, "profileDiscloseValue"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 543
    if-eqz p1, :cond_0

    .line 545
    const-string v1, "Y"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 546
    const/4 v0, 0x1

    .line 550
    :cond_0
    return v0
.end method

.method private getSystemTimeMillis(Ljava/lang/String;)J
    .locals 6
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 565
    const/4 v2, 0x0

    .line 566
    .local v2, "tempDate":Ljava/util/Date;
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd"

    invoke-direct {v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 567
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 568
    if-nez v2, :cond_0

    .line 585
    .end local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :goto_0
    return-wide v4

    .line 571
    .restart local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_0
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v4

    goto :goto_0

    .line 573
    .end local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 577
    .end local v0    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v0

    .line 579
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 581
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 583
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getActivity()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    return v0
.end method

.method public getActivityType()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    return v0
.end method

.method public getBirthDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getDistanceUnit()I
    .locals 1

    .prologue
    .line 345
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    return v0
.end method

.method public getGender()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    return v0
.end method

.method public getHeightUnit()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    return v0
.end method

.method public getLastProfileUpdateTime()J
    .locals 2

    .prologue
    .line 387
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileCreateTime()J
    .locals 2

    .prologue
    .line 416
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    return-wide v0
.end method

.method public getProfileDisclose()Z
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertStringToBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getTemperatureUnit()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    return v0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    return v0
.end method

.method public getWeightUnit()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    return v0
.end method

.method public load()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 447
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 448
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "value"

    invoke-virtual {v0, v3, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 449
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 452
    .local v1, "result":Landroid/os/Bundle;
    const-string v3, "value"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 455
    .local v2, "resultProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 456
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    .line 458
    :cond_0
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 459
    const-string v3, ""

    iput-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 461
    :cond_1
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    if-nez v3, :cond_2

    .line 462
    iput v7, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 464
    :cond_2
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    if-nez v3, :cond_3

    .line 465
    iput v7, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 467
    :cond_3
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    if-nez v3, :cond_4

    .line 468
    iput v7, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 470
    :cond_4
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    if-nez v3, :cond_5

    .line 471
    iput v7, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 473
    :cond_5
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    if-nez v3, :cond_6

    .line 474
    iput v7, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 476
    :cond_6
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 477
    const-string v3, "N"

    iput-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 479
    :cond_7
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    .line 480
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 481
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 482
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    .line 483
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 484
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    .line 485
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 486
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    .line 487
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 488
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 489
    iget v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    .line 490
    const-string v3, "update_time"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    .line 491
    iget-object v3, v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 492
    const-string v3, "profile_create_time"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    .line 496
    return-void
.end method

.method public save()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 527
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->checkValidity()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 528
    iget-boolean v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    if-eqz v1, :cond_1

    .line 529
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    .line 530
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 531
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    .line 532
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 533
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "value"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 534
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v3, "CONFIG_OPTION_PUT"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 539
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 538
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Some of user information is not valid."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setActivity(I)V
    .locals 0
    .param p1, "activity"    # I

    .prologue
    .line 287
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    .line 288
    return-void
.end method

.method public setActivityType(I)V
    .locals 2
    .param p1, "activityType"    # I

    .prologue
    .line 328
    const v0, 0x2bf24

    if-eq p1, v0, :cond_0

    const v0, 0x2bf22

    if-eq p1, v0, :cond_0

    const v0, 0x2bf21

    if-eq p1, v0, :cond_0

    const v0, 0x2bf23

    if-eq p1, v0, :cond_0

    const v0, 0x2bf25

    if-eq p1, v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity type is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    if-eq p1, v0, :cond_1

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 336
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 337
    return-void
.end method

.method public setBirthDate(Ljava/lang/String;)V
    .locals 4
    .param p1, "birthDate"    # Ljava/lang/String;

    .prologue
    .line 298
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Birthdate is in future."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 303
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    .line 304
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 2
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 111
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Country should be composed with 3 characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 116
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setDistanceUnit(I)V
    .locals 2
    .param p1, "distanceUnit"    # I

    .prologue
    .line 359
    const v0, 0x29811

    if-eq p1, v0, :cond_0

    const v0, 0x29813

    if-eq p1, v0, :cond_0

    const v0, 0x29812

    if-eq p1, v0, :cond_0

    .line 362
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Distance unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 363
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    if-eq p1, v0, :cond_1

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 366
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 367
    return-void
.end method

.method public setGender(I)V
    .locals 2
    .param p1, "gender"    # I

    .prologue
    .line 161
    const v0, 0x2e635

    if-eq p1, v0, :cond_0

    const v0, 0x2e636

    if-eq p1, v0, :cond_0

    .line 162
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Gender is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    if-eq p1, v0, :cond_1

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 166
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 167
    return-void
.end method

.method public setHeight(F)V
    .locals 2
    .param p1, "height"    # F

    .prologue
    .line 187
    const/high16 v0, 0x41a00000    # 20.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x43960000    # 300.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 188
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height range should be 20~300"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 192
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    .line 193
    return-void
.end method

.method public setHeightUnit(I)V
    .locals 2
    .param p1, "heightUnit"    # I

    .prologue
    .line 212
    const v0, 0x249f1

    if-eq p1, v0, :cond_0

    const v0, 0x249f2

    if-eq p1, v0, :cond_0

    .line 213
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    if-eq p1, v0, :cond_1

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 217
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 218
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 136
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_1

    .line 137
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Name should be composed with 1~50 characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 139
    iput-boolean v2, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 141
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setProfileDisclose(Z)V
    .locals 1
    .param p1, "isProfileDiscloseYN"    # Z

    .prologue
    .line 396
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertStringToBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 398
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertBooleanToString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 399
    return-void
.end method

.method public setTemperatureUnit(I)V
    .locals 2
    .param p1, "temperatureUnit"    # I

    .prologue
    .line 377
    const v0, 0x27101

    if-eq p1, v0, :cond_0

    const v0, 0x27102

    if-eq p1, v0, :cond_0

    .line 379
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Temperature unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    if-eq p1, v0, :cond_1

    .line 381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 383
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    .line 384
    return-void
.end method

.method public setWeight(F)V
    .locals 2
    .param p1, "weight"    # F

    .prologue
    .line 238
    const/high16 v0, 0x40000000    # 2.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x43fa0000    # 500.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 239
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Weight range should be 2~500"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 243
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    .line 244
    return-void
.end method

.method public setWeightUnit(I)V
    .locals 2
    .param p1, "weightUnit"    # I

    .prologue
    .line 264
    const v0, 0x1fbd1

    if-eq p1, v0, :cond_0

    const v0, 0x1fbd2

    if-eq p1, v0, :cond_0

    .line 265
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Weight unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    if-eq p1, v0, :cond_1

    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 269
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 270
    return-void
.end method
