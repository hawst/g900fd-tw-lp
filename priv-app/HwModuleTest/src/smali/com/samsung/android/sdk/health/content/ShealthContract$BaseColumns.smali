.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseColumns"
.end annotation


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DAYLIGHT_SAVING:Ljava/lang/String; = "daylight_saving"

.field public static final HDID:Ljava/lang/String; = "hdid"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final _ID:Ljava/lang/String; = "_id"
