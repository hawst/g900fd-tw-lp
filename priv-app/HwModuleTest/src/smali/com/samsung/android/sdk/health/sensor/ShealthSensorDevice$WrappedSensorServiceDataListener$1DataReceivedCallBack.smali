.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataReceivedCallBack"
.end annotation


# instance fields
.field data:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

.field dataType:I

.field extra:Landroid/os/Bundle;

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 636
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->dataType:I

    .line 638
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->data:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .line 639
    iput-object p4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->extra:Landroid/os/Bundle;

    .line 640
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 644
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->dataType:I

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->data:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;->extra:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .line 646
    return-void
.end method
