.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$LocationDataColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LocationDataColumns"
.end annotation


# static fields
.field public static final ACCURACY:Ljava/lang/String; = "accuracy"

.field public static final ALTITUDE:Ljava/lang/String; = "altitude"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
