.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_Location"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1578
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1548
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V

    .line 1549
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1552
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V

    .line 1553
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->readFromParcel(Landroid/os/Parcel;)V

    .line 1554
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1593
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    .line 1594
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    .line 1595
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    .line 1596
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->altitude:D

    .line 1597
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->accuracy:F

    .line 1598
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->speed:F

    .line 1599
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->bearing:F

    .line 1601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    .line 1602
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1559
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 1607
    const/4 v0, 0x6

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1613
    const/4 v0, 0x0

    .line 1615
    .local v0, "extraStr":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    if-eqz v1, :cond_0

    .line 1616
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1618
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[LOCATION] (time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", latitude: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", longitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", altitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->altitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", accuracy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->accuracy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", speed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->speed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", extra : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1565
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1566
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1567
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1568
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->altitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1569
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->accuracy:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1570
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1571
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->bearing:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1572
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Extra;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1573
    return-void
.end method
