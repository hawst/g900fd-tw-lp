.class public final enum Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;
.super Ljava/lang/Enum;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "StateTransitionEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_DATA_STARTED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_DATA_STARTED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_DATA_STOPPED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_DATA_STOPPED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_JOINED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_LEFT_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_LEFT_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum RECORD_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum REQUEST_COMMAND_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum SERVICE_UNBIND:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum START_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum STOP_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

.field public static final enum STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 154
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "SERVICE_UNBIND"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->SERVICE_UNBIND:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 155
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "JOIN_CALLED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 156
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_JOINED_SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 157
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_JOINED_FAIL"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 158
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "LEAVE_CALLED"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 159
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_LEFT_SUCCESS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_LEFT_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 160
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_LEFT_FAIL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_LEFT_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 161
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "START_RECEIVING_CALLED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 162
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_DATA_STARTED_SUCCESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 163
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_DATA_STARTED_FAIL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 164
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "STOP_RECEIVING_CALLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 165
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_DATA_STOPPED_SUCCESS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 166
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_DATA_STOPPED_FAIL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 167
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "START_LISTENING_CALLED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 168
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "STOP_LISTENING_CALLED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 169
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "RECORD_CALLED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->RECORD_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 170
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "REQUEST_COMMAND_CALLED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->REQUEST_COMMAND_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 171
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    const-string v1, "ON_RESPONSE_RECEIVED_CALLED"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .line 153
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->SERVICE_UNBIND:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_LEFT_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_LEFT_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->RECORD_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->REQUEST_COMMAND_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->$VALUES:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->$VALUES:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    return-object v0
.end method
