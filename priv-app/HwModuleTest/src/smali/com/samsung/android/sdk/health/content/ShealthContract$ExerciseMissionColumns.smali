.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMissionColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseMissionColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final MISSION_ACHIEVEMENT_TYPE:Ljava/lang/String; = "mission_achievement_type"

.field public static final MISSION_TYPE:Ljava/lang/String; = "mission_type"

.field public static final MISSION_VALUE:Ljava/lang/String; = "mission_value"
