.class public interface abstract Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
.super Ljava/lang/Object;
.source "__ShealthAccessControlAgent.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;
    }
.end annotation


# virtual methods
.method public abstract checkSignature(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract registerForPermission(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract registerForPermissionWithServerAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract showConfirmation(Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract updateConfirmation(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
