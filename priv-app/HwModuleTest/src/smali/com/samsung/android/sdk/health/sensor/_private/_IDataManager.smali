.class public interface abstract Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;
.super Ljava/lang/Object;
.source "_IDataManager.java"


# virtual methods
.method public abstract addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z
.end method

.method public abstract addData([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;[Landroid/os/Bundle;)Z
.end method

.method public abstract isRunning(J)Z
.end method

.method public abstract start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;JI)V"
        }
    .end annotation
.end method

.method public abstract stop(Ljava/lang/String;ILjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method
