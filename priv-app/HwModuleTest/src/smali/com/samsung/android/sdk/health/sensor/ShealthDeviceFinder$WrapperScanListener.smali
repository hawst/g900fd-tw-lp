.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinder.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperScanListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    return-object v0
.end method


# virtual methods
.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    .prologue
    .line 141
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 143
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 145
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Landroid/content/Context;)V

    .line 146
    .local v1, "sensorDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->addDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    invoke-static {v2, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$400(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 147
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1DeviceFoundCallBack;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1DeviceFoundCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 148
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1DeviceFoundCallBack;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 150
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1DeviceFoundCallBack;
    .end local v1    # "sensorDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_0
    monitor-exit v3

    .line 151
    return-void

    .line 150
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onScanStarted(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 172
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 174
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 176
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStartedCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStartedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;I)V

    .line 177
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStartedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 179
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStartedCallBack;
    :cond_0
    monitor-exit v2

    .line 180
    return-void

    .line 179
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onScanStopped(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 212
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 214
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 216
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;I)V

    .line 217
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;
    :cond_0
    monitor-exit v2

    .line 220
    return-void

    .line 219
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
