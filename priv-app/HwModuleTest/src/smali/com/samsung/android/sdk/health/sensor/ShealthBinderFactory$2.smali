.class Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;
.super Ljava/lang/Object;
.source "ShealthBinderFactory.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

.field final synthetic val$ErrorNumber:I

.field final synthetic val$bTrueConneced:Z

.field final synthetic val$u:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;I)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iput-boolean p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$bTrueConneced:Z

    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$u:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$ErrorNumber:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$bTrueConneced:Z

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "ShealthBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$u:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$u:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$ErrorNumber:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;->onServiceConnected(I)V

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$u:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;->val$ErrorNumber:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;->onServiceDisconnected(I)V

    goto :goto_0
.end method
