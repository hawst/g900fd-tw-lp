.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperScanListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

    .line 107
    return-void
.end method


# virtual methods
.method public onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;->onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 115
    return-void
.end method

.method public onStarted(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;->onStarted(I)V

    .line 123
    return-void
.end method

.method public onStopped(I)V
    .locals 1
    .param p1, "error"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;->onStopped(I)V

    .line 130
    return-void
.end method
