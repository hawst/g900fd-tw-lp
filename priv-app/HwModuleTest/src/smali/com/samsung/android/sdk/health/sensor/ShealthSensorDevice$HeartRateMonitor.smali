.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeartRateSNRUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeartRateMonitor"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;
    }
.end annotation


# instance fields
.field public SNR:F

.field public SNRUnit:I

.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public eventTime:J

.field public heartRate:I

.field public heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

.field public interval:J

.field public tagIcon:I

.field public tagIndex:I

.field public tagging:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7fffffff

    .line 2191
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2200
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRate:I

    .line 2202
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->eventTime:J

    .line 2204
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->interval:J

    .line 2206
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->SNR:F

    .line 2208
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->SNRUnit:I

    .line 2210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    .line 2216
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->tagIndex:I

    .line 2224
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->crud:I

    .line 2225
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;->tagIcon:I

    return-void
.end method
