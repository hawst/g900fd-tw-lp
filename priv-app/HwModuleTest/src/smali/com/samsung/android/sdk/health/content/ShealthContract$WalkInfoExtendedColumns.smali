.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtendedColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WalkInfoExtendedColumns"
.end annotation


# static fields
.field public static final ACTIVE_TIME:Ljava/lang/String; = "active_time"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final POWER_STEP:Ljava/lang/String; = "power_step"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
