.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;
.super Ljava/lang/Object;
.source "ShealthDeviceFinder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->onScanStopped(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ScanStoppedCallBack"
.end annotation


# instance fields
.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;I)V
    .locals 0
    .param p2, "error"    # I

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->error:I

    .line 192
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->error:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;->onStopped(I)V

    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$202(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Landroid/os/Handler;)Landroid/os/Handler;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener$1ScanStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 207
    monitor-exit v1

    .line 209
    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
