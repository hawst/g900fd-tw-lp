.class public interface abstract Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerInterface;
.super Ljava/lang/Object;
.source "ShealthSensorProfileHandlerInterface.java"


# static fields
.field public static final ERROR_CONNECTION_LOST:I = 0x3ee

.field public static final ERROR_DATA_NOT_FOUND:I = 0x3ed

.field public static final ERROR_DEVICE_BUSY:I = 0x3ec

.field public static final ERROR_FAILURE:I = 0x3e8

.field public static final ERROR_INVALID_RESPONSE:I = 0x3f0

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_READY:I = 0x3ef

.field public static final ERROR_NOT_SUPPORTED:I = 0x3ea

.field public static final ERROR_NO_RESPONSE:I = 0x3eb

.field public static final ERROR_WRONG_REQUEST:I = 0x3e9


# virtual methods
.method public abstract deinitialize()V
.end method

.method public abstract getCapability()Lcom/samsung/android/sdk/health/sensor/capability/ShealthSensorCapability;
.end method

.method public abstract getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end method

.method public abstract getProtocol()Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;
.end method

.method public abstract initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/Object;)I
.end method

.method public abstract isProtocolSupported()Z
.end method

.method public abstract request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method public abstract setProtocol(Lcom/samsung/android/sdk/health/sensor/protocol/ShealthProtocolInterface;)V
.end method

.method public abstract startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I
.end method

.method public abstract stopReceivingData()I
.end method
