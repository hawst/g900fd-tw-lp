.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$WeightUnit;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WeightUnit"
.end annotation


# static fields
.field public static final KILOGRAM:I = 0x1fbd1

.field public static final LBS:I = 0x1fbd2

.field public static final LITRE:I = 0x1fbd3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NOT_DEFINED:I = -0x1
