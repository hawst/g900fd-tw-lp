.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrivilegeConnectionListener"
.end annotation


# instance fields
.field isConnected:Z

.field isDisconnected:Z

.field mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .prologue
    const/4 v0, 0x0

    .line 193
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    .line 190
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    .line 194
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 195
    return-void
.end method


# virtual methods
.method isConnected()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    return v0
.end method

.method isDisconnected()Z
    .locals 1

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    return v0
.end method

.method public onServiceConnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    .line 200
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC PrivilegeConnectionListener onServiceConnected() is called + normal finder connected status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceConnected(I)V

    .line 205
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    .line 210
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC PrivilegeConnectionListener onServiceDisconnected() is called + normal finder disconnected status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceDisconnected(I)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 216
    return-void
.end method
