.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingDataColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FirstBeatCoachingDataColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final FITNESS_LEVEL:Ljava/lang/String; = "fitness_level"

.field public static final RECOVERY_TIME:Ljava/lang/String; = "recovery_time"

.field public static final TRAINING_EFFECT:Ljava/lang/String; = "training_effect"
