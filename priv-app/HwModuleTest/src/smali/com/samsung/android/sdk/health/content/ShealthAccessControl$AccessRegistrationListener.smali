.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
.super Ljava/lang/Object;
.source "ShealthAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AccessRegistrationListener"
.end annotation


# static fields
.field public static final ERROR_INVALID_ACCESSTOKEN:I = 0x5

.field public static final ERROR_INVALID_SAMSUNG_ACCOUNT:I = 0x1

.field public static final ERROR_NETWORK:I = 0x3

.field public static final ERROR_NETWORK_NOT_AVAILABLE:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_SAMSUNG_ACCOUNT_NO_RESPONSE:I = 0x7

.field public static final ERROR_SERVER:I = 0x4

.field public static final ERROR_UNKNOWN:I = 0x6

.field public static final ERROR_UPGRADE_ONGOING:I = 0xa

.field public static final ERROR_UPGRADE_REQUIRED:I = 0x9

.field public static final ERROR_USER_DENIED:I = 0x8


# virtual methods
.method public abstract onRegistrationCompleted(I)V
.end method
