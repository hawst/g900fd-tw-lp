.class public Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mLock:Ljava/lang/Object;

.field private mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

.field private mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

.field private mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mCallBackHandler:Landroid/os/Handler;

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mLock:Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 256
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context and Listener can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 260
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mContext:Landroid/content/Context;

    .line 265
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    .line 266
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 268
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    .line 269
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 271
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    return-object p1
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 281
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC cleanup finished, this instance is now unusable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->close()V

    .line 287
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 288
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 290
    return-void
.end method

.method public getAPIVersion()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->getAPIVersion()I

    move-result v0

    return v0
.end method

.method public getConnectedDeviceList(III)Ljava/util/List;
    .locals 9
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 376
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ShealthDeviceFinderC getConnectedDeviceList - connectivityType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " deviceType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " dataType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v5, :cond_0

    .line 379
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "Local Service not bound"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 381
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-nez v5, :cond_1

    .line 382
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "Remote Service not bound"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 385
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v5, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v3

    .line 386
    .local v3, "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-nez v3, :cond_2

    .line 387
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .restart local v3    # "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_2
    const/4 v4, 0x0

    .line 390
    .local v4, "privilegeDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;>;"
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeConnectivityType(I)I

    move-result v6

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDeviceType(I)I

    move-result v7

    invoke-static {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDataType(I)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 400
    :goto_0
    if-eqz v4, :cond_4

    .line 405
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 407
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    .line 408
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    if-eqz v0, :cond_3

    .line 410
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mContext:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toSensorDevice(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Landroid/content/Context;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 392
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .end local v2    # "index":I
    :catch_0
    move-exception v1

    .line 394
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 395
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :catch_1
    move-exception v1

    .line 397
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 414
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    :cond_4
    return-object v3
.end method

.method public isAvailable(IIILjava/lang/String;)Z
    .locals 5
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 433
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC isAvailable() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 436
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Local Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-nez v1, :cond_1

    .line 439
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Remote Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 442
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 443
    const/4 v1, 0x1

    .line 453
    :goto_0
    return v1

    .line 445
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeConnectivityType(I)I

    move-result v2

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDeviceType(I)I

    move-result v3

    invoke-static {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDataType(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isAvailable(III)Z
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 446
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    .line 453
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 449
    :catch_1
    move-exception v0

    .line 450
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC isAvailable() not supported device"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V
    .locals 6
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "durationSeconds"    # I
    .param p5, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 313
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC startScan() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Local Service not bound"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;

    invoke-direct {v5, p0, p5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V

    .line 318
    .local v5, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 319
    return-void
.end method

.method public startScan(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V
    .locals 3
    .param p1, "connectivityType"    # I
    .param p2, "deviceId"    # Ljava/lang/String;
    .param p3, "durationSeconds"    # I
    .param p4, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 337
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC startScan() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 339
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Local Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 341
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;

    invoke-direct {v0, p0, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V

    .line 342
    .local v0, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 343
    return-void
.end method

.method public stopScan()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 352
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC - stopScan"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 354
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Local Service not bound"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V

    .line 357
    return-void
.end method
