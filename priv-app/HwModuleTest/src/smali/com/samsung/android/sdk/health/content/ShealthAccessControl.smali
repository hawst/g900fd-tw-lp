.class public Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
.super Ljava/lang/Object;
.source "ShealthAccessControl.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;,
        Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
    }
.end annotation


# static fields
.field private static final ACCESS_CONTROL_SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.service.health.cp.accesscontrol"

.field private static final MAJOR_VERSION_UPGRADE:I = 0x0

.field private static final MEDIO_VERSION_UPGRADE:I = 0x1

.field private static final MINOR_VERSION_UPGRADE:I = 0x2

.field private static final NO_UPGRADE:I = -0x1

.field private static final TAG:Ljava/lang/String;

.field protected static UPGRADE_TYPE:Ljava/lang/String;


# instance fields
.field private isUpgradeRequired:Z

.field private mAccessToken:Ljava/lang/String;

.field private mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

.field private mAppId:Ljava/lang/String;

.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private final mDataCallback:Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;

.field private mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

.field private mServiceConnetion:Landroid/content/ServiceConnection;

.field private mWakeUpHandler:Landroid/os/Handler;

.field private mWakeUpRunnalbe:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;

    .line 88
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$1;-><init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;

    .line 367
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$2;-><init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mServiceConnetion:Landroid/content/ServiceConnection;

    .line 435
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;-><init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mDataCallback:Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;

    .line 474
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 164
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    if-nez p1, :cond_1

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context is not valied"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;

    .line 175
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mServiceConnetion:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mDataCallback:Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAppId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->sendAccessControlRequest()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    return-object p1
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    return p1
.end method

.method private checkVersionTypeUpdate(Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p1, "serviceVersionInAppStore"    # Ljava/lang/String;
    .param p2, "serviceVersion"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    .line 555
    const-string v6, "\\."

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 556
    .local v4, "serviceVersionsInAppStore":[Ljava/lang/String;
    const-string v6, "\\."

    invoke-virtual {p2, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 557
    .local v3, "serviceVersions":[Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    const/4 v1, 0x0

    .line 559
    .local v1, "i":I
    :goto_0
    array-length v6, v4

    if-ge v1, v6, :cond_2

    array-length v6, v3

    if-ge v1, v6, :cond_2

    .line 561
    aget-object v6, v4, v1

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 562
    .local v0, "appStoreVersion":I
    aget-object v6, v3, v1

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 563
    .local v2, "localVerion":I
    if-ge v0, v2, :cond_1

    move v1, v5

    .line 579
    .end local v0    # "appStoreVersion":I
    .end local v1    # "i":I
    .end local v2    # "localVerion":I
    :cond_0
    :goto_1
    return v1

    .line 565
    .restart local v0    # "appStoreVersion":I
    .restart local v1    # "i":I
    .restart local v2    # "localVerion":I
    :cond_1
    if-gt v0, v2, :cond_0

    .line 572
    add-int/lit8 v1, v1, 0x1

    .line 573
    goto :goto_0

    .line 574
    .end local v0    # "appStoreVersion":I
    .end local v2    # "localVerion":I
    :cond_2
    array-length v6, v3

    if-lt v1, v6, :cond_3

    array-length v6, v4

    if-lt v1, v6, :cond_0

    :cond_3
    move v1, v5

    .line 579
    goto :goto_1
.end method

.method private initializeHandler(Z)V
    .locals 6
    .param p1, "isTimeoutRequired"    # Z

    .prologue
    .line 178
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 179
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    .line 180
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    .line 183
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "callBackHandlerWorker"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 184
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 186
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 188
    .local v1, "looper":Landroid/os/Looper;
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mCallBackHandler:Landroid/os/Handler;

    .line 192
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;

    .line 193
    if-eqz p1, :cond_1

    .line 194
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 196
    :cond_1
    return-void
.end method

.method private sendAccessControlRequest()V
    .locals 6

    .prologue
    .line 483
    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    .line 484
    .local v1, "serverAddress":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAppId:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAccessToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    invoke-interface {v2, v3, v4, v1, v5}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;->registerForPermissionWithServerAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    .end local v1    # "serverAddress":Ljava/lang/String;
    :goto_0
    return-void

    .line 486
    :catch_0
    move-exception v0

    .line 489
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    .line 490
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private shouldAskUpdate(Ljava/lang/String;)Z
    .locals 8
    .param p1, "serviceVersionInAppStore"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 508
    const/4 v1, 0x0

    .line 509
    .local v1, "i":Landroid/content/pm/PackageInfo;
    const/4 v2, 0x0

    .line 512
    .local v2, "serviceVersion":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.service.health"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 513
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->checkVersionTypeUpdate(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 545
    iput-boolean v4, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 546
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    const-string v5, "No Upgrade"

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 549
    :goto_0
    return v3

    .line 515
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 524
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :pswitch_0
    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    const-string v5, "Major Version Upgrade"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    const-string v4, "MajorVersion"

    sput-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    .line 526
    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 527
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->startService()V

    goto :goto_0

    .line 531
    :pswitch_1
    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    const-string v5, "Medio Version Upgrade"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    const-string v4, "MedioVersion"

    sput-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    .line 533
    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 534
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->startService()V

    goto :goto_0

    .line 538
    :pswitch_2
    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;

    const-string v5, "Minor Version Upgrade"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    const-string v4, "MinorVersion"

    sput-object v4, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    .line 540
    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 541
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->startService()V

    goto :goto_0

    .line 521
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 287
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.service.health.cp.accesscontrol"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, "serviceIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mServiceConnetion:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 295
    return-void
.end method


# virtual methods
.method public onResult(ZLjava/lang/String;)V
    .locals 4
    .param p1, "result"    # Z
    .param p2, "versionName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 497
    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->shouldAskUpdate(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    .line 500
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z

    .line 501
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 502
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->startService()V

    .line 504
    :cond_1
    return-void
.end method

.method public requestPermission(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;)V
    .locals 2
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "appSecret"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 206
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameters cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAppId:Ljava/lang/String;

    .line 215
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;-><init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Lcom/samsung/android/sdk/health/content/ShealthAccessControl$AccessRegistrationListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    .line 226
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->initializeHandler(Z)V

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->startService()V

    .line 229
    return-void
.end method
