.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Humidity"
.end annotation


# instance fields
.field public accuracy:I

.field public humidity:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2518
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2520
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->accuracy:I

    .line 2522
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;->humidity:F

    return-void
.end method
