.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Constants"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SyncStatus;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$PairedType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceGroupType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TagTableType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MissionAchievementType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MissionType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeartRateSNRUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$RealTimeDataType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ExerciseInfoType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealCategoryType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$FoodServerSourceType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DayLightSaving;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GoalStatus;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$InputSourceType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HRMConditionType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HRMMethodType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceSamplePositionType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$StepsType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$CaloriesUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SpeedUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GenderType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ActivityType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DistanceUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TemperatureUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeightUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucosetUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$WeightUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealItemUnit;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SourceType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucoseSampleSourceType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucoseSampleType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MeasurementContextType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealTimeType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TemperatureType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GoalSubType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GoalType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$CadenceType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ExerciseType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceConnectivityType;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceType;
    }
.end annotation


# static fields
.field public static final ENTRY_MANUAL:I = -0x2
