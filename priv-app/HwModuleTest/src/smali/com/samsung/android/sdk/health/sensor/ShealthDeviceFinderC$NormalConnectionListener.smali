.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NormalConnectionListener"
.end annotation


# instance fields
.field isConnected:Z

.field isDisconnected:Z

.field mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .prologue
    const/4 v0, 0x0

    .line 150
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected:Z

    .line 147
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected:Z

    .line 151
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 152
    return-void
.end method


# virtual methods
.method isConnected()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected:Z

    return v0
.end method

.method isDisconnected()Z
    .locals 1

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected:Z

    return v0
.end method

.method public onServiceConnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected:Z

    .line 157
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC NormalConnectionListener onServiceConnected() is called + priv finder connected status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceConnected(I)V

    .line 162
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected:Z

    .line 167
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC NormalConnectionListener onServiceDisconnected() is called + priv finder disconnected status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceDisconnected(I)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$202(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 173
    return-void
.end method
