.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealItemUnit;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MealItemUnit"
.end annotation


# static fields
.field public static final DEFAULT:I = 0x1d4c1

.field public static final GRAM:I = 0x1d4c2

.field public static final KCAL:I = 0x1d4c4

.field public static final NOT_DEFINED:I = -0x1

.field public static final OUNCE:I = 0x1d4c3
