.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodPressure"
.end annotation


# instance fields
.field public diastolic:F

.field public mean:F

.field public pulse:I

.field public systolic:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 2151
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2153
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->systolic:F

    .line 2155
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->diastolic:F

    .line 2157
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->pulse:I

    .line 2159
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;->mean:F

    return-void
.end method
