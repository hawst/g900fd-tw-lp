.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$CaloriesUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceSamplePositionType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DistanceUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SpeedUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$StepsType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Pedometer"
.end annotation


# static fields
.field public static final ADD_ACTIVE_TIME:Ljava/lang/String; = "ACTIVE_TIME"

.field public static final ADD_INACTIVE_TIMER:Ljava/lang/String; = "INACTIVE_TIMER"

.field public static final ALERT_COUNT:Ljava/lang/String; = "ALERT_COUNT"

.field public static final DURATION:Ljava/lang/String; = "DURATION"

.field public static final STEP_TYPE_DOWN:I = 0x7

.field public static final STEP_TYPE_MARK:I = 0x1

.field public static final STEP_TYPE_RUN:I = 0x4

.field public static final STEP_TYPE_RUSH:I = 0x5

.field public static final STEP_TYPE_STOP:I = 0x0

.field public static final STEP_TYPE_STROLL:I = 0x2

.field public static final STEP_TYPE_UNKNOWN:I = -0x1

.field public static final STEP_TYPE_UP:I = 0x6

.field public static final STEP_TYPE_WALK:I = 0x3


# instance fields
.field public activeTime:J

.field public cadence:F

.field public calories:F

.field public caloriesUnit:I

.field public climbStep:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public distance:F

.field public distanceUnit:I

.field public downStep:I

.field public healthyStep:I

.field public runDownStep:I

.field public runStep:I

.field public runTime:J

.field public runUpStep:I

.field public samplePosition:I

.field public speed:F

.field public speedUnit:I

.field public stepType:I

.field public totalStep:I

.field public upStep:I

.field public updownStep:I

.field public walkDownStep:I

.field public walkStep:I

.field public walkTime:J

.field public walkUpStep:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const v1, 0x7fffffff

    .line 2231
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2263
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->distance:F

    .line 2265
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->distanceUnit:I

    .line 2267
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->calories:F

    .line 2269
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->caloriesUnit:I

    .line 2271
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->speed:F

    .line 2273
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->speedUnit:I

    .line 2275
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->stepType:I

    .line 2277
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->totalStep:I

    .line 2279
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->runStep:I

    .line 2281
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->walkStep:I

    .line 2283
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->updownStep:I

    .line 2285
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->samplePosition:I

    .line 2287
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->cadence:F

    .line 2291
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->walkTime:J

    .line 2293
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->runTime:J

    .line 2295
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->healthyStep:I

    .line 2297
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->activeTime:J

    .line 2305
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->crud:I

    .line 2309
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->climbStep:I

    .line 2314
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->walkUpStep:I

    .line 2316
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->walkDownStep:I

    .line 2318
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->runUpStep:I

    .line 2320
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->runDownStep:I

    .line 2324
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->upStep:I

    .line 2326
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;->downStep:I

    return-void
.end method
