.class Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;
.super Ljava/lang/Object;
.source "ShealthBinderFactory.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v0

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$500()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v0

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$200()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 291
    :cond_0
    const-string v0, "ShealthBinderFactory"

    const-string v1, "Service Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const-string v0, "[ShealthBinderFactory] onServiceConnected"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    check-cast p2, Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .end local p2    # "binder":Landroid/os/IBinder;
    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    invoke-static {v0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$600()I

    move-result v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I

    .line 295
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    const/4 v1, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z

    .line 301
    :goto_0
    return-void

    .line 299
    .restart local p2    # "binder":Landroid/os/IBinder;
    :cond_1
    const-string v0, "ShealthBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect state found : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 279
    const-string v0, "ShealthBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service disconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const-string v0, "[ShealthBinderFactory] onServiceDisconnected"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v0, v3, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$200()I

    move-result v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I

    .line 283
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$300()Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    move-result-object v0

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 284
    return-void
.end method
