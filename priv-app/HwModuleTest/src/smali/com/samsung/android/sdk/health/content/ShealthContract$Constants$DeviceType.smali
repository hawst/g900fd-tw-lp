.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceType"
.end annotation


# static fields
.field public static final ACTIVITY_TRACKER:I = 0x2727

.field public static final ALL:I = 0x2711

.field public static final BIKE_POWER:I = 0x2720

.field public static final BIKE_SPEED_CADENCE:I = 0x271f

.field public static final BLOOD_GLUCOSE:I = 0x2714

.field public static final BLOOD_PRESSURE:I = 0x2713

.field public static final BODY_TEMPERATURE:I = 0x2716

.field public static final ELECTROCARDIOGRAM:I = 0x2729

.field public static final FITNESS_EQUIPMENT:I = 0x271b

.field public static final GEAR:I = 0x2724

.field public static final GEAR2:I = 0x2726

.field public static final GEARS:I = 0x272e

.field public static final GEAR_FIT:I = 0x2723

.field public static final GPS:I = 0x271c

.field public static final HEART_RATE_MONITOR:I = 0x2718

.field public static final HUMIDITY:I = 0x272b

.field public static final NOT_DEFINED:I = -0x1

.field public static final PEDOMETER:I = 0x2719

.field public static final PULSEOXIMETER:I = 0x272a

.field public static final SDM:I = 0x271a

.field public static final TEMPERATURE:I = 0x272c

.field public static final TIZEN_GEAR1:I = 0x2728

.field public static final UV_RAY:I = 0x272d

.field public static final WATCH:I = 0x271d

.field public static final WEIGHT_SCALE:I = 0x2712

.field public static final WINGTIP:I = 0x2723
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field
