.class public Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
.super Ljava/lang/Object;
.source "_PrivilegeSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private connectivityType:I

.field private dataTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private deviceType:I

.field private extras:Landroid/os/Bundle;

.field private mObjectId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "connectivityType"    # I
    .param p4, "deviceType"    # I
    .param p5, "dataType"    # I
    .param p6, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 111
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceId:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    .line 113
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->connectivityType:I

    .line 114
    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceType:I

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iput-object p6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->extras:Landroid/os/Bundle;

    .line 118
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceId:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceType:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->connectivityType:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->extras:Landroid/os/Bundle;

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->mObjectId:I

    .line 91
    return-void
.end method


# virtual methods
.method public addDataType(I)V
    .locals 2
    .param p1, "dataType"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_1
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectivityType()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->connectivityType:I

    return v0
.end method

.method public getDataType()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceType:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectId()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->mObjectId:I

    return v0
.end method

.method public setDataType(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "dataType":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    .line 163
    return-void
.end method

.method public setObjectId(I)V
    .locals 0
    .param p1, "objId"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->mObjectId:I

    .line 199
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_ShealthSensorDevice : deviceId == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectivityType == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->connectivityType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceType == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataTypeList == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->deviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->connectivityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->extras:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 53
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->mObjectId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    return-void
.end method
