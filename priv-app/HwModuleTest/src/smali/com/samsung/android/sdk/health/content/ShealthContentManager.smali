.class public Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.super Ljava/lang/Object;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;,
        Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    }
.end annotation


# static fields
.field private static final ACCOUNT:Ljava/lang/String; = "Account"

.field private static final ACCOUNT_FOUND:Ljava/lang/String; = "ACCOUNT_FOUND"

.field private static final ACCOUNT_NOT_FOUND:Ljava/lang/String; = "ACCOUNT_NOT_FOUND"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "AccountType"

.field public static final ACCOUNT_TYPE_HEALTH:Ljava/lang/String; = "HEALTH_ACCOUNT"

.field public static final ACCOUNT_TYPE_SAM:Ljava/lang/String; = "SAMSUNG_ACCOUNT"

.field private static ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String; = null

.field public static final ACTION_BACKUP_COMPLETED:Ljava/lang/String; = "action_backup_completed"

.field public static final ACTION_BACKUP_STARTED:Ljava/lang/String; = "action_backup_started"

.field public static final ACTION_DELETION_COMPLETED:Ljava/lang/String; = "action_deletion_completed"

.field public static final ACTION_DELETION_STARTED:Ljava/lang/String; = "action_deletion_started"

.field public static final ACTION_RESTORATION_COMPLETED:Ljava/lang/String; = "action_restoration_completed"

.field public static final ACTION_RESTORATION_ONGOING_DB_REPLACE:Ljava/lang/String; = "action_restoration_ongoing_db_replace"

.field public static final ACTION_RESTORATION_STARTED:Ljava/lang/String; = "action_restoration_started"

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.shealth.cp.HealthContentProvider"

.field public static final BACKUP_TYPE_ALL:I = 0x1

.field public static final BACKUP_TYPE_BIO_BLOODGLUCOSE:I = 0x66

.field public static final BACKUP_TYPE_BIO_BLOODPRESURE:I = 0x67

.field public static final BACKUP_TYPE_BIO_BODYTEMPERATURE:I = 0x69

.field public static final BACKUP_TYPE_BIO_ECG:I = 0x6d

.field public static final BACKUP_TYPE_BIO_HRM:I = 0x6c

.field public static final BACKUP_TYPE_BIO_PO:I = 0x6e

.field public static final BACKUP_TYPE_BIO_SKIN:I = 0x6f

.field public static final BACKUP_TYPE_BIO_WEIGHT:I = 0x68

.field public static final BACKUP_TYPE_EXERCISE:I = 0xc8

.field public static final BACKUP_TYPE_FOOD:I = 0x320

.field public static final BACKUP_TYPE_PROFILE:I = 0x190

.field public static final BACKUP_TYPE_RANKING:I = 0xc9

.field public static final BACKUP_TYPE_SLEEP:I = 0x1f5

.field public static final BACKUP_TYPE_STRESS:I = 0x1f6

.field public static final BACKUP_TYPE_TEMPERATURE_HUMIDITY:I = 0x25b

.field public static final BACKUP_TYPE_UV:I = 0x259

.field public static final BACKUP_TYPE_UV_PROTECTION:I = 0x25a

.field public static final BACKUP_TYPE_WATER_INTAKE:I = 0x12c

.field public static final DATA_TYPE:Ljava/lang/String; = "data_type"

.field private static final GET_SAMSUNG_ACCOUNT:Ljava/lang/String; = "GET_SAMSUNG_ACCOUNT"

.field public static final GET_SYNC_STATUS:Ljava/lang/String; = "GET_SYNC_STATUS"

.field public static final IS_RESTORE_COMPLETED:Ljava/lang/String; = "Is_Restore_Completed"

.field public static final PROGRESS_STATUS:Ljava/lang/String; = "PROGRESS_STATUS"

.field public static final RECEIVER_ACTION:Ljava/lang/String; = "com.private.syncadapter.receiver"

.field public static final REQUEST_TYPE:Ljava/lang/String; = "request_type"

.field public static final REQUEST_TYPE_BACKUP:I = 0x1869f

.field public static final REQUEST_TYPE_DELETE_BACKUP:I = 0x1869c

.field public static final REQUEST_TYPE_RESTORE:I = 0x1869e

.field public static final REQUEST_TYPE_SYNC:I = 0x1869d

.field public static final RESTORE_TYPE_ALL:I = 0xb

.field public static final RESTORE_TYPE_BIO_BLOODGLUCOSE:I = 0x44e

.field public static final RESTORE_TYPE_BIO_BLOODPRESURE:I = 0x44f

.field public static final RESTORE_TYPE_BIO_BODYTEMPERATURE:I = 0x451

.field public static final RESTORE_TYPE_BIO_ECG:I = 0x455

.field public static final RESTORE_TYPE_BIO_HRM:I = 0x454

.field public static final RESTORE_TYPE_BIO_PO:I = 0x456

.field public static final RESTORE_TYPE_BIO_SKIN:I = 0x457

.field public static final RESTORE_TYPE_BIO_WEIGHT:I = 0x450

.field public static final RESTORE_TYPE_EXERCISE:I = 0x4b0

.field public static final RESTORE_TYPE_FOOD:I = 0x708

.field public static final RESTORE_TYPE_PROFILE:I = 0x578

.field public static final RESTORE_TYPE_SLEEP:I = 0x5dd

.field public static final RESTORE_TYPE_STRESS:I = 0x5de

.field public static final RESTORE_TYPE_TEMPERATURE_HUMIDITY:I = 0x643

.field public static final RESTORE_TYPE_UV:I = 0x641

.field public static final RESTORE_TYPE_UV_PROTECTION:I = 0x642

.field public static final RESTORE_TYPE_WATER_INTAKE:I = 0x514

.field public static final SAMSUNG_AC_LINKED_FOR_HEALTH:Ljava/lang/String; = "SAMSUNG_AC_LINKED_FOR_HEALTH"

.field public static final SET_AUTOBACKUP_INTERVAL:Ljava/lang/String; = "autobackup_interval"

.field public static final SHEALTH_2_5_EXISTS:Ljava/lang/String; = "SHEALTH_2_5_EXISTS"

.field private static final STATE_READY:I = 0x0

.field private static final STATE_STARTED:I = 0x2

.field private static final STATE_STARTING:I = 0x1

.field private static final STATE_STOPPED:I = 0x4

.field private static final STATE_STOPPING:I = 0x3

.field private static final STATUS:Ljava/lang/String; = "Status"

.field private static final SYNC_STATE_END:I = 0x1

.field private static final SYNC_STATE_START:I = 0x0

.field public static final WIFI_STATUS_KEY:Ljava/lang/String; = "SET_WIFI_STATUS"

.field private static mListenerColl:Ljava/util/Map;


# instance fields
.field private final INTENT_ACTION_SYNC_AGENT:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private errorCode:I

.field private isRegistered:Z

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private shealthAgent:Lcom/samsung/android/sdk/health/content/serversync/__ShealthAgent;

.field private syncState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    .line 131
    const-string v0, "com.osp.app.signin"

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const-class v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    .line 116
    const-string v0, "com.sec.android.service.health.cp.serversync.agent.ShealthAgent"

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->INTENT_ACTION_SYNC_AGENT:Ljava/lang/String;

    .line 242
    iput v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->syncState:I

    .line 294
    iput v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    .line 296
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 585
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;-><init>(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 302
    if-nez p1, :cond_0

    .line 303
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 306
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    .line 309
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    return-void
.end method

.method private checkNetworkAndAccount()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 765
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "checkNetworkAndAccount()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 769
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "GET_SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const-string v3, "value"

    iget-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    invoke-virtual {v3, v4, v5, v2, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 772
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 774
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v3, "result is not null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    .end local v1    # "result":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 779
    .restart local v1    # "result":Landroid/os/Bundle;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    const/4 v3, 0x3

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    move-object v1, v2

    .line 783
    goto :goto_0
.end method

.method private connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 11
    .param p1, "requestType"    # I
    .param p2, "dataType"    # I
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .prologue
    .line 898
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "connectToSyncAdapter(): requestType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ". dataType: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isBackUpRestoreInProgress()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 903
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Cancel the previous sync opepration"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    const-string v9, "account"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/AccountManager;

    .line 905
    .local v2, "accountManager":Landroid/accounts/AccountManager;
    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 906
    .local v1, "account":[Landroid/accounts/Account;
    if-eqz v1, :cond_0

    array-length v8, v1

    if-gtz v8, :cond_3

    .line 908
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): No account found."

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    if-eqz p3, :cond_2

    .line 911
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): previous sync is in progress, listener is not null, send onstated callbackup with errorcode: 2"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    const/4 v8, 0x2

    invoke-interface {p3, p2, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    .line 979
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    :cond_1
    :goto_0
    return-void

    .line 915
    .restart local v1    # "account":[Landroid/accounts/Account;
    .restart local v2    # "accountManager":Landroid/accounts/AccountManager;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): previous sync is in progress, listener is null"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 920
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Account found, cancel the previous sync"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    const/4 v8, 0x0

    aget-object v8, v1, v8

    const-string v9, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v8, v9}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 924
    .end local v1    # "account":[Landroid/accounts/Account;
    .end local v2    # "accountManager":Landroid/accounts/AccountManager;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v4

    .line 925
    .local v4, "result":Landroid/os/Bundle;
    if-eqz v4, :cond_9

    .line 927
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter() result is not null"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    const-string v8, "Status"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 929
    .local v5, "status":Ljava/lang/String;
    const-string v8, "AccountType"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 930
    .local v0, "accType":Ljava/lang/String;
    if-eqz v0, :cond_5

    const-string v8, "NONE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 931
    :cond_5
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): No account found so returning.."

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    const/4 v8, 0x2

    invoke-interface {p3, p2, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    goto :goto_0

    .line 935
    :cond_6
    const-string v8, "SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 936
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Samsung account is present"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 937
    if-eqz v5, :cond_1

    const-string v8, "ACCOUNT_FOUND"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 938
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): account found"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 939
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 940
    .local v3, "extraData":Landroid/os/Bundle;
    const-string v8, "request_type"

    invoke-virtual {v3, v8, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 941
    const-string v8, "data_type"

    invoke-virtual {v3, v8, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 942
    const-string v8, "force"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 943
    const-string v8, "expedited"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 944
    const-string v8, "packageName"

    iget-object v9, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 946
    .local v6, "timeStamp":J
    const-string v8, "TimeStamp"

    invoke-virtual {v3, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 947
    const-string v8, "Shealth_SDK_Is_Caller"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 948
    const-string v8, "Account"

    invoke-virtual {v4, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 949
    .local v1, "account":Landroid/accounts/Account;
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "adding time stamp to mListenerColl  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    const-string v8, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v1, v8, v3}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 952
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->registerSyncAdapterBroadCastReceiver()V

    goto/16 :goto_0

    .line 958
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v3    # "extraData":Landroid/os/Bundle;
    .end local v6    # "timeStamp":J
    :cond_7
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Samsung account is not present"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    if-eqz p3, :cond_8

    .line 961
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Samsung account is not present, listener is not null, send onstated callbackup with errorcode: 2"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    const/4 v8, 0x2

    invoke-interface {p3, p2, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    goto/16 :goto_0

    .line 965
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter(): Samsung account is not present, listener is null"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 970
    .end local v0    # "accType":Ljava/lang/String;
    .end local v5    # "status":Ljava/lang/String;
    :cond_9
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter() result is null"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    if-eqz p3, :cond_a

    .line 973
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter() result is null, listener is not null"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 974
    iget v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    invoke-interface {p3, p2, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    goto/16 :goto_0

    .line 977
    :cond_a
    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v9, "connectToSyncAdapter() result is null, listener is null"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private registerSyncAdapterBroadCastReceiver()V
    .locals 4

    .prologue
    .line 647
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "registerSyncAdapterBroadCastReceiver(): REGISTERED THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.private.syncadapter.receiver"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 649
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 651
    return-void
.end method

.method private unRegisterSyncAdapterBroadCastReceiver()V
    .locals 2

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "unRegisterSyncAdapterBroadCastReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "unRegisterSyncAdapterBroadCastReceiver(): UN REGISTERING THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 663
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 666
    :cond_0
    return-void
.end method


# virtual methods
.method public deleteBackUp(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 892
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "deleteBackUp()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    const v0, 0x1869c

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 894
    return-void
.end method

.method public isBackUpRestoreInProgress()Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 793
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress()"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v3

    .line 795
    .local v3, "result":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 797
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): result is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    const-string v7, "Status"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 799
    .local v5, "status":Ljava/lang/String;
    const-string v7, "AccountType"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 800
    .local v0, "accType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v7, "NONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 802
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isBackUpRestoreInProgress(): accType: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    const-string v7, "SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 805
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): accType is samsung account"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    if-eqz v5, :cond_0

    const-string v7, "ACCOUNT_FOUND"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 808
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): account found"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    const-string v7, "Account"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 810
    .local v1, "account":Landroid/accounts/Account;
    if-eqz v1, :cond_0

    .line 812
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): account is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 818
    .local v2, "extras":Landroid/os/Bundle;
    const-string v7, "key"

    const-string v8, "GET_SYNC_STATUS"

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    const-string v7, "value"

    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const-string v7, "Account"

    invoke-virtual {v2, v7, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 821
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v9, "CONFIG_OPTION_GET"

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10, v2}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 822
    .local v4, "retVal":Landroid/os/Bundle;
    if-eqz v4, :cond_1

    .line 824
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): retVal is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 825
    const-string v7, "GET_SYNC_STATUS"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 838
    .end local v0    # "accType":Ljava/lang/String;
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v4    # "retVal":Landroid/os/Bundle;
    .end local v5    # "status":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 829
    .restart local v0    # "accType":Ljava/lang/String;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "extras":Landroid/os/Bundle;
    .restart local v4    # "retVal":Landroid/os/Bundle;
    .restart local v5    # "status":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): retVal is null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isWifiChosenForBackup()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 871
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "isWifiChosenForBackup()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 873
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "SET_WIFI_STATUS"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 876
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 878
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isWifiChosenForBackup(): value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "value"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    const-string v3, "value"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 882
    :goto_0
    return v2

    .line 881
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "isWifiChosenForBackup(): wifi is not chosen for backup"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 480
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "startBackup()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    const v0, 0x1869f

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 482
    return-void
.end method

.method public startRestore(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 548
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "startRestore()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const v0, 0x1869e

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 550
    return-void
.end method

.method public stopBackup()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 486
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v1

    .line 488
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 489
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup() result is not null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    const-string v3, "Status"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 491
    .local v2, "status":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "ACCOUNT_FOUND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 492
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup() account found"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    const-string v3, "Account"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 496
    .local v0, "account":Landroid/accounts/Account;
    const-string v3, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 497
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 498
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup() clear mListenerColl"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 501
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup() unregister braodcast receiver"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    .line 512
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "status":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 508
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopBackup() result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopRestore()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 554
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v1

    .line 556
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 557
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore() result is not null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    const-string v3, "Status"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 559
    .local v2, "status":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "ACCOUNT_FOUND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 560
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore() account found"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    const-string v3, "Account"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 564
    .local v0, "account":Landroid/accounts/Account;
    const-string v3, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 565
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 566
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore() clear mListenerColl"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 569
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore() unregister braodcast receiver"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    .line 580
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "status":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 576
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "stopRestore() result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public useWifiForBackup(Z)Z
    .locals 6
    .param p1, "useWifi"    # Z

    .prologue
    .line 848
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v3, "useWifiForBackup()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 850
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "key"

    const-string v3, "SET_WIFI_STATUS"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    const-string v2, "value"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 852
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "CONFIG_OPTION_PUT"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 853
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 855
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v3, "useWifiForBackup(): result is not null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    const/4 v2, 0x1

    .line 861
    :goto_0
    return v2

    .line 860
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v3, "useWifiForBackup(): result is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    const/4 v2, 0x0

    goto :goto_0
.end method
