.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;
.super Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0

    .prologue
    .line 1353
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataAction(J[Z[Ljava/lang/String;)V
    .locals 4
    .param p1, "requestTime"    # J
    .param p3, "status"    # [Z
    .param p4, "approvedIndex"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1360
    const/4 v0, 0x0

    aget-boolean v0, p3, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$400(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1365
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$700(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$500(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$600(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;J)I

    .line 1401
    :cond_0
    return-void
.end method
