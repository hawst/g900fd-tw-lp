.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ExerciseInfoType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseInfoType"
.end annotation


# static fields
.field public static final AEROBIC_HIGH_IMPACT:I = 0x2eeb

.field public static final AEROBIC_LOW_IMPACT:I = 0x2eea

.field public static final AQUAROBICS:I = 0x32d1

.field public static final ARCHERY_NON_HUNTING:I = 0x2ef6

.field public static final BACKSTROKE_RECREATIONAL:I = 0x32cd

.field public static final BACKSTROKE_TRAINING:I = 0x32cc

.field public static final BADMINTON_COMPETATIVE:I = 0x2f08

.field public static final BADMINTON_SOCIAL_SINGLE_DOUBLES:I = 0x2f09

.field public static final BALLET:I = 0x2ee2

.field public static final BALLROOM_DANCE_FAST:I = 0x2f00

.field public static final BALLROOM_DANCE_SLOW:I = 0x2f01

.field public static final BARBELL_BENCHPRESS:I = 0x3e85

.field public static final BARBELL_BENT_OVER_ROW:I = 0x3e8b

.field public static final BARBELL_CURL:I = 0x3e8f

.field public static final BARBELL_SQUAT:I = 0x3e96

.field public static final BARBELL_UPRIGHT_ROW:I = 0x3e8c

.field public static final BASEBALL:I = 0x2f02

.field public static final BASKETBALL_GAME:I = 0x2efe

.field public static final BASKETBALL_GENERAL:I = 0x2efd

.field public static final BICYCLING_FAST:I = 0x36b3

.field public static final BICYCLING_MODERATE:I = 0x36b2

.field public static final BICYCLING_SLOW:I = 0x36b1

.field public static final BICYCLING_VERY_FAST:I = 0x36b4

.field public static final BILLIARD:I = 0x2f0a

.field public static final BOWLING:I = 0x2f1b

.field public static final BOXING_IN_RING:I = 0x2f0b

.field public static final BOXING_PUNCHING_BAG:I = 0x2f0c

.field public static final BOXING_SPARRING:I = 0x2f0d

.field public static final BREASTSTROKE_RECREATIONAL:I = 0x32cf

.field public static final BREASTSTROKE_TRAINING:I = 0x32ce

.field public static final BUTTERFLY_STROKE_SWIMMING:I = 0x32d0

.field public static final CANOEING_ROWING:I = 0x32d2

.field public static final CATEGORY_BICYCLING:I = 0x36b0

.field public static final CATEGORY_CUSTOM:I = 0x4268

.field public static final CATEGORY_GENERAL:I = 0x2ee0

.field public static final CATEGORY_REALTIME_WORKOUT:I = 0x4650

.field public static final CATEGORY_RUNNING:I = 0x2af8

.field public static final CATEGORY_STRENGTH:I = 0x3e80

.field public static final CATEGORY_WALKING:I = 0x2710

.field public static final CATEGORY_WATER_ACTIVITIES:I = 0x32c8

.field public static final CATEGORY_WINTER_ACTIVITIES:I = 0x3a98

.field public static final CRUNCH:I = 0x3e9d

.field public static final DANCING:I = 0x2ee3

.field public static final DUMBBELL_BENCHPRESS:I = 0x3e84

.field public static final DUMBBELL_KICKBACK:I = 0x3e94

.field public static final DUMBBELL_LATERAL_RAISE:I = 0x3e88

.field public static final DUMBBELL_OVERHEAD_PRESS:I = 0x3e8a

.field public static final FAST_WALKING:I = 0x2713

.field public static final FIELD_HOCKEY:I = 0x2ef7

.field public static final FLAT_BENCH_DUMBBELL_FLYE:I = 0x3e86

.field public static final FREE_STYLE_SWIMMING_FAST:I = 0x32ca

.field public static final FREE_STYLE_SWIMMING_SLOW:I = 0x32cb

.field public static final GOLF:I = 0x2efc

.field public static final HANDBALL_GENERAL:I = 0x2f0e

.field public static final HANDBALL_TEAM:I = 0x2f0f

.field public static final HANGLIDING:I = 0x2ef4

.field public static final HIKING:I = 0x2ee1

.field public static final HORSEBACK_RIDING:I = 0x2f10

.field public static final HULA_HOOPING:I = 0x2eef

.field public static final ICE_DANCING:I = 0x3a9c

.field public static final ICE_HOCKEY_COMPETATIVE:I = 0x3aa2

.field public static final ICE_HOCKEY_GENERAL:I = 0x3aa1

.field public static final ICE_SKATING_GENERAL:I = 0x3a9e

.field public static final ICE_SKATING_REPIDLY:I = 0x3a9f

.field public static final ICE_SKATING_SLOW:I = 0x3a9d

.field public static final INCLINE_BARBELL_PRESS:I = 0x3e87

.field public static final INCLINE_DUMBBELL_CURL:I = 0x3e91

.field public static final INLINE_SKATING_FAST:I = 0x2ee6

.field public static final INLINE_SKATING_NORMAL:I = 0x2ee5

.field public static final INLINE_SKATING_SLOW:I = 0x2ee4

.field public static final LAT_PULLDOEN:I = 0x3e8e

.field public static final LEG_EXTENSION:I = 0x3e95

.field public static final LEG_PRESS:I = 0x3e97

.field public static final LYING_BARBELL_EXTENSION:I = 0x3e92

.field public static final LYING_LEG_CURL:I = 0x3e9a

.field public static final MARTIAL_ARTS_MODERATE_PACE:I = 0x2f11

.field public static final MARTIAL_ARTS_SLOWER_PACE:I = 0x2f12

.field public static final NORMAL_WALKING:I = 0x2712

.field public static final ONE_ARM_DUMBBELL_ROW:I = 0x3e8d

.field public static final OTHERS:I = 0x4a38

.field public static final PEDOMETER:I = 0x4651

.field public static final PILATES:I = 0x2ee8

.field public static final PISTOL_SHOOTING:I = 0x2ef5

.field public static final PREACHER_CURL_MACHINE:I = 0x3e90

.field public static final PULL_UP:I = 0x3e82

.field public static final PUSH_UP:I = 0x3e81

.field public static final REALTIME_CYCLING:I = 0x4654

.field public static final REALTIME_HIKING:I = 0x4655

.field public static final REALTIME_RUNNING:I = 0x4653

.field public static final REALTIME_WALKING:I = 0x4652

.field public static final REVERSE_CRUNCH:I = 0x3e9e

.field public static final ROCK_CLIMBING_HIGH_DIFFICULTY:I = 0x2ef0

.field public static final ROCK_CLIMBING_LOW_MODERATE_DIFFICULTY:I = 0x2ef1

.field public static final ROMANIAN_DEADLIFT:I = 0x3e99

.field public static final ROPE_PRESSDOWN:I = 0x3e93

.field public static final RUGBY_TOUCH_NON_COMPETATIVE:I = 0x2efb

.field public static final RUGBY_UNION_TEAM_COMPETATIVE:I = 0x2efa

.field public static final RUNNING_10_MPH:I = 0x2b00

.field public static final RUNNING_11_MPH:I = 0x2b01

.field public static final RUNNING_12_MPH:I = 0x2b02

.field public static final RUNNING_13_MPH:I = 0x2b03

.field public static final RUNNING_14_MPH:I = 0x2b04

.field public static final RUNNING_4_MPH:I = 0x2afa

.field public static final RUNNING_5_MPH:I = 0x2afb

.field public static final RUNNING_6_MPH:I = 0x2afc

.field public static final RUNNING_7_MPH:I = 0x2afd

.field public static final RUNNING_8_MPH:I = 0x2afe

.field public static final RUNNING_9_MPH:I = 0x2aff

.field public static final RUNNING_GENERAL:I = 0x2af9

.field public static final SAILING_YATCHING_FOR_LEISURE:I = 0x32d3

.field public static final SEATED_CALF_RAISE:I = 0x3e9b

.field public static final SEATED_LEG_CURL:I = 0x3e98

.field public static final SIT_UP:I = 0x3e83

.field public static final SKIING_SNOWBOARDING_LIGHT:I = 0x3a99

.field public static final SKIING_SNOWBOARDING_MODERATE:I = 0x3a9a

.field public static final SKIING_SNOWBOARDING_VIGOROUS:I = 0x3a9b

.field public static final SKINDIVING_FAST:I = 0x32d4

.field public static final SKINDIVING_SCUBA_DIVING_GENERAL_ROWING:I = 0x32d6

.field public static final SKINDIVING_SLOW:I = 0x32d5

.field public static final SKIPPING_100_120_PER_MIN:I = 0x2eed

.field public static final SKIPPING_100_PER_MIN:I = 0x2eee

.field public static final SKIPPING_120_160_PER_MIN:I = 0x2eec

.field public static final SLOW_WALKING:I = 0x2711

.field public static final SMITH_MACHINE_UPRIGHT_ROW:I = 0x3e89

.field public static final SNORKELING_FOR_LEISURE:I = 0x32d7

.field public static final SNOWBOARDING:I = 0x3aa3

.field public static final SOCCER_CASUAL:I = 0x2f04

.field public static final SOCCER_COMPETATIVE:I = 0x2f03

.field public static final SOFTBALL_GENERAL:I = 0x2f13

.field public static final SOFTBALL_PITCHING:I = 0x2f15

.field public static final SOFTBALL_PRACTICE:I = 0x2f14

.field public static final SPEED_SKATING_COMPETATIVE:I = 0x3aa0

.field public static final SQUASH_GENERAL:I = 0x2eff

.field public static final STANDING_CALF_RAISE:I = 0x3e9c

.field public static final STATIONARY_BICYCLE_201_270_WATTS_VERY_VIGOROUS_EFFORT:I = 0x36b9

.field public static final STATIONARY_BICYCLE_51_89_WATTS_LIGHT_TO_MODERATE_EFFORT:I = 0x36ba

.field public static final STATIONARY_BICYCLE_MODERATE_TO_VIGOROUS_EFFORT:I = 0x36b6

.field public static final STATIONARY_BICYCLE_VERY_LIGHT_TO_LIGHT_EFFORT:I = 0x36b5

.field public static final STATIONARY_BICYCLE_VIGOROUE_EFFORT_101_160_WATTS:I = 0x36b7

.field public static final STATIONARY_BICYCLE_VIGOROUS_EFFORT:I = 0x36b8

.field public static final STEP_MACHINE:I = 0x2f19

.field public static final STRENGTH_WORKOUT:I = 0x3e9f

.field public static final STRETCHING_MILD:I = 0x2ee7

.field public static final TABLE_TENNIS:I = 0x2f16

.field public static final TAI_CHI_GENERAL:I = 0x2ef2

.field public static final TAI_CHI_LIGHT_EFFORT:I = 0x2ef3

.field public static final TENNIS_DOUBLES:I = 0x2f07

.field public static final TENNIS_GENERAL:I = 0x2f05

.field public static final TENNIS_SINGLES:I = 0x2f06

.field public static final VOLLEYBALL_COMPETATIVE:I = 0x2f17

.field public static final VOLLEYBALL_GENERAL:I = 0x2f18

.field public static final WALKING_DOWNSTAIRS:I = 0x2715

.field public static final WALKING_UPSTAIRS:I = 0x2714

.field public static final WATER_SKIING:I = 0x32c9

.field public static final WEIGHT_MACHINE:I = 0x2f1a

.field public static final YOGA:I = 0x2ee9
