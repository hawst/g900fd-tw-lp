.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Sleep"
.end annotation


# instance fields
.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public efficiency:D

.field public endTime:J

.field public rating:I

.field public startTime:J

.field public status:[I

.field public tagIcon:I

.field public tagIndex:I

.field public tagging:Ljava/lang/String;

.field public time:[J


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    const-wide v4, 0x7fffffffffffffffL

    const v2, 0x7fffffff

    .line 2539
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2541
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->startTime:J

    .line 2543
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->endTime:J

    .line 2545
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->efficiency:D

    .line 2547
    new-array v0, v6, [I

    aput v2, v0, v3

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->status:[I

    .line 2549
    new-array v0, v6, [J

    aput-wide v4, v0, v3

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->time:[J

    .line 2553
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->rating:I

    .line 2557
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->tagIndex:I

    .line 2565
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->crud:I

    .line 2566
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;->tagIcon:I

    return-void
.end method
