.class public abstract Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;
.super Landroid/os/Binder;
.source "__ShealthAccessControlAgent.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

.field static final TRANSACTION_checkSignature:I = 0x2

.field static final TRANSACTION_registerForPermission:I = 0x1

.field static final TRANSACTION_registerForPermissionWithServerAddress:I = 0x3

.field static final TRANSACTION_showConfirmation:I = 0x4

.field static final TRANSACTION_updateConfirmation:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 104
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v5, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;

    move-result-object v2

    .line 54
    .local v2, "_arg2":Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;
    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->registerForPermission(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;)V

    .line 55
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 60
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;
    :sswitch_2
    const-string v7, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 63
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->checkSignature(Ljava/lang/String;)Z

    move-result v4

    .line 64
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 65
    if-eqz v4, :cond_0

    move v5, v6

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 70
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":Z
    :sswitch_3
    const-string v5, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 76
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;

    move-result-object v3

    .line 79
    .local v3, "_arg3":Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->registerForPermissionWithServerAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;)V

    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 85
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Ljava/lang/String;
    .end local v3    # "_arg3":Lcom/samsung/android/sdk/health/content/_AccessRegistrationListener;
    :sswitch_4
    const-string v5, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;

    move-result-object v0

    .line 89
    .local v0, "_arg0":Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 90
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->showConfirmation(Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 96
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback;
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_5
    const-string v7, "com.samsung.android.sdk.health.content.__ShealthAccessControlAgent"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    move v0, v6

    .line 99
    .local v0, "_arg0":Z
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent$Stub;->updateConfirmation(Z)V

    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    :cond_1
    move v0, v5

    .line 98
    goto :goto_1

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
