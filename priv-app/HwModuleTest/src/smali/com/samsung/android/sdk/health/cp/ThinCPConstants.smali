.class public Lcom/samsung/android/sdk/health/cp/ThinCPConstants;
.super Ljava/lang/Object;
.source "ThinCPConstants.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.service.health.cp.TrustZoneSecurityProvider"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final KEY_CREATE_SECURE_PASSWORD:Ljava/lang/String; = "create_secure_password"

.field public static final KEY_GET_AES_PASSWORD:Ljava/lang/String; = "get_aes_password"

.field public static final KEY_GET_HEALTHSERVICE_DB_PASSWORD:Ljava/lang/String; = "get_secure_password"

.field public static final KEY_GET_SECURE_PASSWORD:Ljava/lang/String; = "get_secure_password"

.field public static final KEY_IS_SECURE_STORAGE_SUPPORTS:Ljava/lang/String; = "secure_storage_support"

.field public static final MIGRATION_AUTHORITY:Ljava/lang/String; = "com.sec.android.service.health.cp.MigrationCpProvider"

.field public static final MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

.field public static final VALUE_GET_CREATE_PASSWORD:Ljava/lang/String; = "value_of_password"

.field public static final VALUE_IS_SECURE_STORAGE_SUPPORTS:Ljava/lang/String; = "boolean_secure_storage_support"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "content://com.sec.android.service.health.cp.TrustZoneSecurityProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->AUTHORITY_URI:Landroid/net/Uri;

    .line 29
    const-string v0, "content://com.sec.android.service.health.cp.MigrationCpProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/cp/ThinCPConstants;->MIGRATION_AUTHORITY_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
