.class public abstract Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;
.super Landroid/os/Binder;
.source "_PlatformSensorService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor._PlatformSensorService"

.field static final TRANSACTION_checkAvailability:I = 0x2

.field static final TRANSACTION_close:I = 0x5

.field static final TRANSACTION_getAPIVersion:I = 0x6

.field static final TRANSACTION_getConnectedDevices:I = 0x1

.field static final TRANSACTION_registerListener:I = 0x3

.field static final TRANSACTION_unregisterListener:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 128
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v5, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 51
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 53
    .local v1, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 54
    .local v2, "_arg2":I
    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->getConnectedDevices(III)Ljava/util/List;

    move-result-object v4

    .line 55
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 56
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 61
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":I
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    :sswitch_2
    const-string v5, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 65
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 67
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 68
    .restart local v2    # "_arg2":I
    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->checkAvailability(III)I

    move-result v3

    .line 69
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":I
    .end local v3    # "_result":I
    :sswitch_3
    const-string v7, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    .line 78
    sget-object v7, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 84
    .local v0, "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-result-object v1

    .line 85
    .local v1, "_arg1":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z

    move-result v3

    .line 86
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 87
    if-eqz v3, :cond_0

    move v5, v6

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 81
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .end local v1    # "_arg1":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    .end local v3    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    goto :goto_1

    .line 92
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :sswitch_4
    const-string v7, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_3

    .line 95
    sget-object v7, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 100
    .restart local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v3

    .line 101
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    if-eqz v3, :cond_2

    move v5, v6

    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 98
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .end local v3    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    goto :goto_2

    .line 107
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :sswitch_5
    const-string v5, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_4

    .line 110
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 115
    .restart local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->close(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V

    .line 116
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 113
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    goto :goto_3

    .line 121
    .end local v0    # "_arg0":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    :sswitch_6
    const-string v5, "com.samsung.android.sdk.health.sensor._PlatformSensorService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->getAPIVersion()I

    move-result v3

    .line 123
    .local v3, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
