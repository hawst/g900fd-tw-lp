.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeterColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PulseOximeterColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final HEART_RATE:Ljava/lang/String; = "heart_rate"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field public static final VALUE:Ljava/lang/String; = "value"
