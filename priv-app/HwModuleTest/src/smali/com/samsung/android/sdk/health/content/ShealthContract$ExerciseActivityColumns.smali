.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivityColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseActivityColumns"
.end annotation


# static fields
.field public static final ALTITUDE_GAIN:Ljava/lang/String; = "altitude_gain"

.field public static final ALTITUDE_LOSS:Ljava/lang/String; = "altitude_loss"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CADENCE:Ljava/lang/String; = "cadence"

.field public static final CADENCE_TYPE:Ljava/lang/String; = "cadence_type"

.field public static final CALORIE:Ljava/lang/String; = "calorie"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final DECLINE_DISTANCE:Ljava/lang/String; = "decline_distance"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final DURATION_MILLISECOND:Ljava/lang/String; = "duration_millisecond"

.field public static final DURATION_MIN:Ljava/lang/String; = "duration_min"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final INCLINE:Ljava/lang/String; = "incline"

.field public static final INCLINE_DISTANCE:Ljava/lang/String; = "incline_distance"

.field public static final MAX_ALTITUDE:Ljava/lang/String; = "max_altitude"

.field public static final MAX_CADENCE_RATE_PER_MIN:Ljava/lang/String; = "max_cadence_rate_per_min"

.field public static final MAX_CALORICBURN_RATE_PER_HOUR:Ljava/lang/String; = "max_caloricburn_rate_per_hour"

.field public static final MAX_HEART_RATE_PER_MIN:Ljava/lang/String; = "max_heart_rate_per_min"

.field public static final MAX_SPEED_PER_HOUR:Ljava/lang/String; = "max_speed_per_hour"

.field public static final MEAN_CADENCE_RATE_PER_MIN:Ljava/lang/String; = "mean_cadence_rate_per_min"

.field public static final MEAN_CALORICBURN_RATE_PER_HOUR:Ljava/lang/String; = "mean_caloricburn_rate_per_hour"

.field public static final MEAN_HEART_RATE_PER_MIN:Ljava/lang/String; = "mean_heart_rate_per_min"

.field public static final MEAN_SPEED_PER_HOUR:Ljava/lang/String; = "mean_speed_per_hour"

.field public static final MIN_ALTITUDE:Ljava/lang/String; = "min_altitude"

.field public static final POWER_WATT:Ljava/lang/String; = "power_watt"

.field public static final RESISTANCE:Ljava/lang/String; = "resistance"

.field public static final SAMPLE_POSITION:Ljava/lang/String; = "sample_position"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final STRIDE_LENGTH:Ljava/lang/String; = "stride_length"
