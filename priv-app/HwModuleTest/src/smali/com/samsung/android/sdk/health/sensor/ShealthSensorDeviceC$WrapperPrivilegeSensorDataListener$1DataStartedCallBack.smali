.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->onStarted(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataStartedCallBack"
.end annotation


# instance fields
.field dataType:I

.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;II)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "error"    # I

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->dataType:I

    .line 79
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->error:I

    .line 80
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->error:I

    if-nez v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getDataType()Ljava/util/List;

    move-result-object v5

    const-wide/16 v6, -0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mPid:I
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)I

    move-result v8

    invoke-interface/range {v1 .. v8}, Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;->start(Landroid/content/Context;Ljava/lang/String;ILjava/util/List;JI)V

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->listener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->dataType:I

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->error:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStarted(II)V

    .line 92
    return-void

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    goto :goto_0
.end method
