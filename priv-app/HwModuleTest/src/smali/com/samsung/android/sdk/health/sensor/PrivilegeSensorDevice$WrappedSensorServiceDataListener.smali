.class final Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;
.super Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener$Stub;
.source "PrivilegeSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WrappedSensorServiceDataListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener$Stub;-><init>()V

    .line 68
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    .line 69
    return-void
.end method


# virtual methods
.method public onBulkDataReceived(I[Landroid/os/Bundle;)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "extra"    # [Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;->onReceived(I[Landroid/os/Bundle;)V

    .line 87
    return-void
.end method

.method public onDataReceived(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "extra"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;->onReceived(ILandroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public onDataStarted(II)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "error"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;->onStarted(II)V

    .line 75
    return-void
.end method

.method public onDataStopped(II)V
    .locals 1
    .param p1, "dataType"    # I
    .param p2, "error"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;->onStopped(II)V

    .line 93
    return-void
.end method
