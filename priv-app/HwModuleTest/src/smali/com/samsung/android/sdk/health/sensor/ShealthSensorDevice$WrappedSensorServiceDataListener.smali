.class final Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WrappedSensorServiceDataListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .prologue
    .line 571
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 572
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 573
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    .prologue
    .line 567
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    return-object v0
.end method


# virtual methods
.method public onBulkDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;[Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;
    .param p2, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 681
    array-length v3, p1

    new-array v1, v3, [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .line 683
    .local v1, "dataArray":[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 684
    aget-object v3, p1, v2

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    aput-object v3, v1, v2

    .line 683
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 705
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v4, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 707
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 708
    if-eqz p1, :cond_1

    array-length v3, p1

    if-lez v3, :cond_1

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 710
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v3

    invoke-direct {v0, p0, v3, v1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V

    .line 712
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 717
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;
    :cond_1
    monitor-exit v4

    .line 718
    return-void

    .line 717
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "data"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;
    .param p2, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 630
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v1

    .line 649
    .local v1, "health":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 651
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 654
    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 655
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v2

    const/4 v4, 0x0

    invoke-direct {v0, p0, v2, v4, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .line 662
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 666
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;
    :cond_0
    monitor-exit v3

    .line 667
    return-void

    .line 658
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    invoke-direct {v0, p0, v4, v2, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .restart local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;
    goto :goto_0

    .line 666
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataReceivedCallBack;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onDataStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 607
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 609
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 611
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;II)V

    .line 612
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 615
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;
    :cond_0
    monitor-exit v2

    .line 616
    return-void

    .line 615
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDataStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 755
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 757
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 759
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStoppedCallBack;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStoppedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;II)V

    .line 760
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStoppedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 763
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStoppedCallBack;
    :cond_0
    monitor-exit v2

    .line 764
    return-void

    .line 763
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
