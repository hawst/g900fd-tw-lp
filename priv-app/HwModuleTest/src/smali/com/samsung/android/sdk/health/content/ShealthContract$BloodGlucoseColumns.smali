.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BloodGlucoseColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final GLUCOSE:Ljava/lang/String; = "glucose"

.field public static final HBA1C:Ljava/lang/String; = "hba1c"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final MEAL_TIME:Ljava/lang/String; = "meal_time"

.field public static final MEASUREMENT_CONTEXT_TYPE:Ljava/lang/String; = "measurement_context_type"

.field public static final SAMPLE_SOURCE_TYPE:Ljava/lang/String; = "sample_source_type"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SAMPLE_TYPE:Ljava/lang/String; = "sample_type"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
