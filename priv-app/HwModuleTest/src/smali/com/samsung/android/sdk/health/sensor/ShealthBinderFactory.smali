.class public Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
.super Ljava/lang/Object;
.source "ShealthBinderFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    }
.end annotation


# static fields
.field public static final DEVICE_TYPE_DEVICE:I = 0x65

.field public static final DEVICE_TYPE_FINDER:I = 0x64

.field private static SERVICE_CONNECTION_ESTABLISHED:I = 0x0

.field private static SERVICE_CONNECTION_LOST:I = 0x0

.field private static SERVICE_CONNECTION_REQUESTED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ShealthBinderFactory"

.field private static sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mObjDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceConnectionState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 328
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .line 344
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    .line 345
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    .line 346
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 322
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 326
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 136
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    .line 138
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->initializeCallbackHandlerThread()Z

    .line 139
    return-void
.end method

.method private NotifyServiceConnectionStatus(ZI)Z
    .locals 10
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 232
    const/4 v0, 0x0

    .line 235
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 237
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 238
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 240
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    .line 242
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    .line 244
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;I)V

    .line 259
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 260
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 261
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x32

    invoke-virtual {v6, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 265
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    if-ne v0, v4, :cond_2

    .line 271
    :goto_1
    return v4

    .line 270
    :cond_2
    const-string v4, "ShealthBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Z
    .locals 10
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I
    .param p3, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    const/4 v4, 0x1

    .line 188
    const/4 v0, 0x0

    .line 191
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 193
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 194
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 196
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    .line 198
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-ne v6, p3, :cond_0

    .line 201
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$1;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;I)V

    .line 215
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 216
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 217
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x32

    invoke-virtual {v6, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 221
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    if-ne v0, v4, :cond_2

    .line 227
    :goto_1
    return v4

    .line 226
    :cond_2
    const-string v4, "ShealthBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    return p1
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    return v0
.end method

.method static synthetic access$300()Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    return-object p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    return v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return v0
.end method

.method private dropServiceConnection()Z
    .locals 2

    .prologue
    .line 178
    const-string v0, "ShealthBinderFactory"

    const-string v1, "dropServiceConnection: Unbinding with Health Service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 181
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public static declared-synchronized getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J
    .param p4, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 80
    const-class v7, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v7

    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    if-nez v2, :cond_0

    .line 82
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .line 85
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v2, v3, :cond_1

    .line 87
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 88
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 91
    :cond_1
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    .line 92
    .local v1, "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v2, v3, :cond_3

    .line 96
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 97
    .local v0, "it":Landroid/content/Intent;
    const-string v2, "com.sec.android.service.health.sensor.SensorService"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    .line 103
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 105
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v2, v3, :cond_2

    .line 106
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 107
    :cond_2
    const-string v2, "ShealthBinderFactory"

    const-string v3, "Requested Binding to Health Service"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    .end local v0    # "it":Landroid/content/Intent;
    :cond_3
    if-eqz p4, :cond_4

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    if-ne v2, v3, :cond_4

    .line 121
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Z

    .line 124
    :cond_4
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v7

    return-object v2

    .line 112
    .restart local v0    # "it":Landroid/content/Intent;
    :cond_5
    :try_start_1
    const-string v2, "ShealthBinderFactory"

    const-string v3, "Binding to Shealth Service wont happen.."

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    sget v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    const/4 v2, 0x0

    goto :goto_0

    .line 80
    .end local v0    # "it":Landroid/content/Intent;
    .end local v1    # "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v2

    monitor-exit v7

    throw v2
.end method

.method public static declared-synchronized getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 11
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # J

    .prologue
    .line 51
    const-class v5, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v5

    const/4 v3, 0x0

    .line 53
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :try_start_0
    sget-object v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v6, v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 55
    :try_start_1
    sget-object v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 56
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 58
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    move-object v3, v0

    .line 59
    iget-wide v8, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v4, v8, p1

    if-nez v4, :cond_0

    .line 64
    :cond_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 65
    if-nez v3, :cond_2

    .line 67
    :try_start_2
    const-string v4, "ShealthBinderFactory"

    const-string v6, "Looke like getDefaultBinder was not called"

    invoke-static {v4, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 68
    const/4 v4, 0x0

    .line 73
    :goto_0
    monitor-exit v5

    return-object v4

    .line 64
    .end local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 51
    :catchall_1
    move-exception v4

    monitor-exit v5

    throw v4

    .line 73
    .restart local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_2
    :try_start_5
    sget-object v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method private initializeCallbackHandlerThread()Z
    .locals 3

    .prologue
    .line 306
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ShealthBinderFactory-Callback"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 307
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 308
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 310
    const/4 v1, 0x1

    return v1
.end method

.method public static declared-synchronized releaseReference(II)Z
    .locals 4
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # I

    .prologue
    .line 130
    const-class v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    int-to-long v2, p1

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->removeMatchingObject(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    const/4 v0, 0x0

    monitor-exit v1

    return v0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 317
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 320
    :cond_0
    return-void
.end method

.method removeMatchingObject(IJ)Z
    .locals 10
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J

    .prologue
    const/4 v5, 0x1

    .line 143
    const/4 v3, 0x0

    .line 146
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v6

    .line 148
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 149
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 151
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    move-object v3, v0

    .line 152
    iget-wide v8, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v4, v8, p2

    if-nez v4, :cond_0

    .line 157
    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    if-nez v3, :cond_2

    .line 161
    const-string v4, "ShealthBinderFactory"

    const-string v5, "removeMatchingObject:Matching Object not found"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v4, 0x0

    .line 172
    :goto_0
    return v4

    .line 157
    .end local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 167
    .restart local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 168
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-ne v4, v5, :cond_3

    .line 170
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->dropServiceConnection()Z

    :cond_3
    move v4, v5

    .line 172
    goto :goto_0
.end method
