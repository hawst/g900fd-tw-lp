.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$InputSourceType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InputSourceType"
.end annotation


# static fields
.field public static final DEVICE:I = 0x3f7a2

.field public static final FITBIT:I = 0x3f7a4

.field public static final HEALTHVAULT:I = 0x3f7a5

.field public static final MANUAL:I = 0x3f7a1

.field public static final NETPULSE:I = 0x3f7a3

.field public static final NOT_DEFINED:I = -0x1

.field public static final WITHINGS:I = 0x3f7a6
