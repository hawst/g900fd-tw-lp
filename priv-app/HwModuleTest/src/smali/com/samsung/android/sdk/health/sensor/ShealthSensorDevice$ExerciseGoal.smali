.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExerciseGoal"
.end annotation


# instance fields
.field public achievedBadge:I

.field public achievedType:I

.field public achievedValue:D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 2757
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2759
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedType:I

    .line 2761
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedValue:D

    .line 2763
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;->achievedBadge:I

    return-void
.end method
