.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WalkInfoColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CALORIE:Ljava/lang/String; = "calorie"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final DOWN_STEP:Ljava/lang/String; = "down_step"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final RUN_STEP:Ljava/lang/String; = "run_step"

.field public static final SAMPLE_POSITION:Ljava/lang/String; = "sample_position"

.field public static final SPEED:Ljava/lang/String; = "speed"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final TOTAL_STEP:Ljava/lang/String; = "total_step"

.field public static final UPDOWN_STEP:Ljava/lang/String; = "updown_step"

.field public static final UP_STEP:Ljava/lang/String; = "up_step"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field public static final WALK_STEP:Ljava/lang/String; = "walk_step"
