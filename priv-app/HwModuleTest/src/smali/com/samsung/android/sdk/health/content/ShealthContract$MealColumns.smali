.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$MealColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MealColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final FAVORITE:Ljava/lang/String; = "favorite"

.field public static final MEAL_PLAN_ID:Ljava/lang/String; = "meal_plan__id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final TOTAL_KILO_CALORIE:Ljava/lang/String; = "total_kilo_calorie"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final USER_DEVICE__ID:Ljava/lang/String; = "user_device__id"
