.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtectionColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContext;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContextColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Tag;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$TagColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$SkinColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingData;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingDataColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariable;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariableColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResultColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhotoColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeterColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysis;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysisColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiography;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRaysColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateData;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateDataColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRate;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestion;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WaterIngestionColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$LocationDataColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$GoalColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WeightColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$StressColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$SleepDataColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$SleepColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidity;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$TemperatureHumidityColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperature;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BodyTemperatureColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressure;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressureColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucose;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitness;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitnessColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtendedColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMission;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseMissionColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivity;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseActivityColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeDataColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfoColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrientColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfoColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlan;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealPlanColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealImage;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealImageColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealItemColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$MealColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDevice;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$PairedDeviceColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$UserDevice;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$UserDeviceColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfo;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseDeviceInfoColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;,
        Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.shealth.cp.HealthContentProvider"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final CONTENT_EXERCISE_PHOTO_READ_URI:Landroid/net/Uri;

.field public static final CONTENT_EXERCISE_PHOTO_WRITE_URI:Landroid/net/Uri;

.field public static final CONTENT_MEAL_IMAGES_READ_URI:Landroid/net/Uri;

.field public static final CONTENT_MEAL_IMAGES_WRITE_URI:Landroid/net/Uri;

.field public static final CONTENT_RAWQUERY_URI:Landroid/net/Uri;

.field public static final CONTENT_USER_IMAGE_READ_URI:Landroid/net/Uri;

.field public static final CONTENT_USER_IMAGE_WRITE_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    const-string v0, "content://com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 113
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "read_files_meal"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_MEAL_IMAGES_READ_URI:Landroid/net/Uri;

    .line 118
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "write_files_meal"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_MEAL_IMAGES_WRITE_URI:Landroid/net/Uri;

    .line 124
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "read_user_image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_USER_IMAGE_READ_URI:Landroid/net/Uri;

    .line 129
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "write_user_image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_USER_IMAGE_WRITE_URI:Landroid/net/Uri;

    .line 134
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "read_exercise_image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_EXERCISE_PHOTO_READ_URI:Landroid/net/Uri;

    .line 139
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "write_exercise_image"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_EXERCISE_PHOTO_WRITE_URI:Landroid/net/Uri;

    .line 145
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "rawquery"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    return-void
.end method
