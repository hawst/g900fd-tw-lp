.class public Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
.super Ljava/lang/Object;
.source "PrivilegeSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;,
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field _device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Landroid/content/Context;)V
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "uContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->getNextInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->setObjectId(I)V

    .line 48
    const/16 v0, 0x6a

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {p2, v0, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 49
    return-void
.end method

.method private deinitialize()V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 60
    return-void
.end method

.method private getNextInt()I
    .locals 4

    .prologue
    .line 99
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 100
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v1

    return v1
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;,
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 213
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->TAG:Ljava/lang/String;

    const-string v1, "close() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->close(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V

    .line 218
    :cond_0
    return-void
.end method

.method public getConnectivityType()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getConnectivityType() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getConnectivityType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getConnectivityType()I

    move-result v0

    return v0
.end method

.method public getDataType()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceType()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDeviceType()I

    move-result v0

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized registerListener(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;)Z
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 183
    monitor-enter p0

    if-nez p1, :cond_0

    .line 184
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 186
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v2, :cond_1

    .line 187
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 189
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-nez v2, :cond_2

    .line 190
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;

    const-string v3, "Service is not connected"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 192
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;)V

    .line 193
    .local v1, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$WrappedSensorServiceDataListener;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 195
    .local v0, "registrationStatus":Z
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized unregisterListener()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-nez v1, :cond_0

    .line 201
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 203
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-nez v1, :cond_1

    .line 204
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->mPlatformServiceBinder:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 208
    .local v0, "unregistrationStatus":Z
    monitor-exit p0

    return v0
.end method
