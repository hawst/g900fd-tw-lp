.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Location"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;
    }
.end annotation


# instance fields
.field public accuracy:F

.field public altitude:D

.field public bearing:F

.field public extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

.field public latitude:D

.field public longitude:D

.field public speed:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2333
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2335
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    .line 2337
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    .line 2339
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    .line 2341
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    .line 2343
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    .line 2345
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    .line 2347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    .line 2349
    return-void
.end method
