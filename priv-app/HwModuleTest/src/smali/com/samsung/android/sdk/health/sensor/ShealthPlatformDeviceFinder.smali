.class public Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
.super Ljava/lang/Object;
.source "ShealthPlatformDeviceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDiscoveredDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

.field private mObjectId:I

.field private mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    .line 84
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinder() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 87
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context and Listener can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinder constructor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mContext:Landroid/content/Context;

    .line 92
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mListener:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    .line 93
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->getNextInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    .line 96
    const/16 v0, 0x69

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    int-to-long v2, v1

    invoke-static {p1, v0, v2, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 99
    return-void
.end method

.method private addDeviceFound(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    .prologue
    .line 348
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    monitor-exit v1

    .line 352
    return-void

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private cleanupDevices()V
    .locals 8

    .prologue
    .line 308
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 310
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    .local v0, "d":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    :try_start_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 323
    :goto_1
    :try_start_2
    const-class v4, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    const-string v6, "deinitialize"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 324
    .local v3, "privateDeviceMethod":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 326
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 327
    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 330
    .end local v3    # "privateDeviceMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 334
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 339
    .end local v0    # "d":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 316
    .restart local v0    # "d":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v1

    .line 318
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 338
    .end local v0    # "d":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 339
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 340
    return-void
.end method

.method private getNextInt()I
    .locals 4

    .prologue
    .line 103
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 104
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v1

    return v1
.end method

.method private isValidConnectivityType(I)Z
    .locals 1
    .param p1, "connectivityType"    # I

    .prologue
    const/4 v0, 0x1

    .line 276
    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDataType(I)Z
    .locals 2
    .param p1, "dataType"    # I

    .prologue
    const/4 v0, 0x1

    .line 296
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x7

    if-eq p1, v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDeviceType(I)Z
    .locals 1
    .param p1, "deviceType"    # I

    .prologue
    .line 286
    const/16 v0, 0x2718

    if-eq p1, v0, :cond_0

    const/16 v0, 0x272a

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2714

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2729

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2716

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 116
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinder cleanup finished, this instance is now unusable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->cleanupDevices()V

    .line 120
    const/16 v0, 0x69

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->releaseReference(II)Z

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 125
    return-void
.end method

.method public getAPIVersion()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    const/16 v0, 0x69

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->getAPIVersion()I

    move-result v0

    .line 69
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getConnectedDeviceList(III)Ljava/util/List;
    .locals 10
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 144
    sget-object v7, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ShealthDeviceFinder getConnectedDeviceList - connectivityType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " deviceType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " dataType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const/16 v7, 0x69

    iget v8, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    int-to-long v8, v8

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 146
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidConnectivityType(I)Z

    move-result v7

    if-nez v7, :cond_1

    move-object v1, v6

    .line 206
    :cond_0
    :goto_0
    return-object v1

    .line 152
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidDeviceType(I)Z

    move-result v7

    if-nez v7, :cond_2

    move-object v1, v6

    .line 154
    goto :goto_0

    .line 158
    :cond_2
    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidDataType(I)Z

    move-result v7

    if-nez v7, :cond_3

    move-object v1, v6

    .line 160
    goto :goto_0

    .line 164
    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isAvailable(III)Z

    move-result v7

    if-nez v7, :cond_4

    move-object v1, v6

    .line 166
    goto :goto_0

    .line 170
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-eqz v7, :cond_7

    .line 174
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    invoke-interface {v7, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->getConnectedDevices(III)Ljava/util/List;

    move-result-object v0

    .line 175
    .local v0, "_deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;>;"
    if-nez v0, :cond_5

    move-object v1, v6

    .line 179
    goto :goto_0

    .line 182
    :cond_5
    const/4 v4, 0x0

    .local v4, "index":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 184
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 185
    .local v2, "devicee":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    if-eqz v2, :cond_6

    .line 188
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    iget-object v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mContext:Landroid/content/Context;

    invoke-direct {v5, v2, v7}, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Landroid/content/Context;)V

    .line 189
    .local v5, "sensorDevice":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->addDeviceFound(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v5    # "sensorDevice":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 196
    .end local v0    # "_deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;>;"
    .end local v2    # "devicee":Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .end local v4    # "index":I
    :catch_0
    move-exception v3

    .line 198
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v6

    .line 206
    goto :goto_0

    .line 203
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_7
    new-instance v6, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;

    const-string v7, "getConnectedDeviceList - ShealthSensorService is not bounded."

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public isAvailable(III)Z
    .locals 6
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 225
    const/16 v3, 0x69

    iget v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mObjectId:I

    int-to-long v4, v4

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 226
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAvailable - connectivityType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " deviceType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " dataType : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidConnectivityType(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 266
    :cond_0
    :goto_0
    return v2

    .line 234
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidDeviceType(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 240
    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isValidDataType(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 246
    const/4 v0, 0x1

    .line 248
    .local v0, "code":I
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    if-eqz v3, :cond_2

    .line 252
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    invoke-interface {v3, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;->checkAvailability(III)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 264
    :goto_1
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAvailable - code : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    if-nez v0, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 254
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 261
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;

    const-string v3, "isAvailable - ShealthSensorService is not bounded."

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
