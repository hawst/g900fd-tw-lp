.class public Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;
.super Ljava/lang/Object;
.source "ShealthPlatformConnectionFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field static mConnectionRefCount:I

.field private static mSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;


# instance fields
.field private mSHealthServiseConnectionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->TAG:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    .line 25
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    return-void
.end method

.method private constructor <init>(Landroid/content/ServiceConnection;)V
    .locals 1
    .param p1, "ServiceConnection"    # Landroid/content/ServiceConnection;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    .line 33
    return-void
.end method

.method static getInstance(Landroid/content/ServiceConnection;I)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;
    .locals 1
    .param p0, "ServiceConnection"    # Landroid/content/ServiceConnection;
    .param p1, "ObjectType"    # I

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;-><init>(Landroid/content/ServiceConnection;)V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    .line 41
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;

    return-object v0
.end method


# virtual methods
.method closeAllConnection(Landroid/content/Context;Landroid/content/ServiceConnection;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mServiceConnection"    # Landroid/content/ServiceConnection;
    .param p3, "ObjectType"    # I

    .prologue
    .line 83
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    .line 84
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->closeConnection(Landroid/content/Context;Landroid/content/ServiceConnection;IJ)V

    .line 85
    return-void
.end method

.method closeConnection(Landroid/content/Context;Landroid/content/ServiceConnection;IJ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mServiceConnection"    # Landroid/content/ServiceConnection;
    .param p3, "ObjectType"    # I
    .param p4, "FinderObjId"    # J

    .prologue
    .line 47
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->TAG:Ljava/lang/String;

    const-string v2, "close"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    sget v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mSHealthServiseConnectionMap:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 57
    sget v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    .line 61
    :cond_0
    sget v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mConnectionRefCount:I

    if-nez v1, :cond_1

    .line 66
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinder cleanup finished, this instance is now unusable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformConnectionFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 79
    :cond_1
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
