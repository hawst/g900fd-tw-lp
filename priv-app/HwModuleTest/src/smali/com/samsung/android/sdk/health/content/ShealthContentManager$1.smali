.class Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;
.super Landroid/content/BroadcastReceiver;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v10, -0x1

    .line 591
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive(): number of times "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.private.syncadapter.receiver"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 594
    const-string v6, "DATA_SYNC_TYPE"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 596
    .local v1, "dataType":I
    const-string v6, "Status_bTrueFinished"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 597
    .local v0, "bTrueFinished":Z
    const-string v6, "TimeStamp"

    const-wide/16 v8, 0x0

    invoke-virtual {p2, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 598
    .local v2, "key":J
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v6

    if-eqz v6, :cond_4

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 599
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive(): mListenerColl has key: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .line 601
    .local v5, "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    if-eqz v5, :cond_3

    .line 603
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "onReceive(): syncCallBack is not null"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const-string v6, "PROGRESS_STATUS"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 606
    .local v4, "progressStatus":I
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive(): progress: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    if-ltz v4, :cond_1

    const/16 v6, 0x64

    if-gt v4, v6, :cond_1

    .line 609
    invoke-interface {v5, v1, v4}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onProgress(II)V

    .line 638
    .end local v0    # "bTrueFinished":Z
    .end local v1    # "dataType":I
    .end local v2    # "key":J
    .end local v4    # "progressStatus":I
    .end local v5    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_0
    :goto_0
    return-void

    .line 613
    .restart local v0    # "bTrueFinished":Z
    .restart local v1    # "dataType":I
    .restart local v2    # "key":J
    .restart local v4    # "progressStatus":I
    .restart local v5    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_1
    if-nez v0, :cond_2

    .line 615
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive(): calling on started for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const-string v6, "ERROR_TYPE"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-interface {v5, v1, v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    goto :goto_0

    .line 620
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive(): calling on onFinished for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    const-string v6, "ERROR_TYPE"

    invoke-virtual {p2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-interface {v5, v1, v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onFinished(II)V

    .line 622
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "size of mListenerColl before removing"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removing list with key"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 625
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "size of mListenerColl after removing"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "is empty "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 628
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # invokes: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$200(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V

    goto/16 :goto_0

    .line 632
    .end local v4    # "progressStatus":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no call reg with key ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 635
    .end local v5    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no call reg with key ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
