.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ElectroCardiographyColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final MAX_HEART_RATE:Ljava/lang/String; = "max_heart_rate"

.field public static final MEAN_HEART_RATE:Ljava/lang/String; = "mean_heart_rate"

.field public static final MIN_HEART_RATE:Ljava/lang/String; = "min_heart_rate"

.field public static final SAMPLE_COUNT:Ljava/lang/String; = "sample_count"

.field public static final SAMPLING_FREQUENCY:Ljava/lang/String; = "sampling_frequency"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
