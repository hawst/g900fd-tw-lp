.class Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;
.super Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;
.source "ShealthAccessControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthAccessControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/_UpdateServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateAction(Z)V
    .locals 5
    .param p1, "isUpdate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 441
    if-eqz p1, :cond_1

    .line 443
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getIntentForHealthServiceUpdate()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 444
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    .line 469
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # setter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->isUpgradeRequired:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$902(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;Z)Z

    .line 470
    return-void

    .line 449
    :cond_1
    const-string v0, "MajorVersion"

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "MedioVersion"

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->UPGRADE_TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 451
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    goto :goto_0

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mListener:Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$000(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$WrapperAccessControlListener;->onRegistrationComplete(I)V

    .line 457
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$300(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mWakeUpRunnalbe:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$400(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 458
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$800()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Upgrade is not needed"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAcessControlAgent:Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$700(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->mAppId:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$1100(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/content/__ShealthAccessControlAgent;->checkSignature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthAccessControl$3;->this$0:Lcom/samsung/android/sdk/health/content/ShealthAccessControl;

    # invokes: Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->sendAccessControlRequest()V
    invoke-static {v0}, Lcom/samsung/android/sdk/health/content/ShealthAccessControl;->access$1200(Lcom/samsung/android/sdk/health/content/ShealthAccessControl;)V

    goto :goto_0
.end method
