.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SensorRequestListener"
.end annotation


# instance fields
.field command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 1
    .param p2, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    .line 2991
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 2988
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 2992
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 2993
    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 3001
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/SensorListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_6

    .line 3021
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3022
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onConnected(I)V

    .line 3023
    :cond_0
    return-void

    .line 3003
    :catch_0
    move-exception v0

    .line 3004
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 3005
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 3006
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 3007
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 3008
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 3009
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 3011
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 3012
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 3013
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 3014
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 3015
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 3016
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_6
    move-exception v0

    .line 3018
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method
