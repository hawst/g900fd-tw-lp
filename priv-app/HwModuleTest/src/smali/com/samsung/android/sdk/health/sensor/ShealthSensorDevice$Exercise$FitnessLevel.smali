.class public interface abstract Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise$FitnessLevel;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FitnessLevel"
.end annotation


# static fields
.field public static final AVERAGE:I = 0x4

.field public static final BELOW_AVERAGE:I = 0x2

.field public static final EXELLENT:I = 0x7

.field public static final FAIR:I = 0x3

.field public static final GOOD:I = 0x5

.field public static final VERY_GOOD:I = 0x6

.field public static final WELL_BELOW_AVERAGE:I = 0x1
