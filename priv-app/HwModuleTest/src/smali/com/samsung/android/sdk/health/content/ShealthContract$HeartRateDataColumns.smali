.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$HeartRateDataColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HeartRateDataColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final HEART_BEAT_COUNT:Ljava/lang/String; = "heart_beat_count"

.field public static final HEART_RATE_ID:Ljava/lang/String; = "heart_rate__id"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final VALUE:Ljava/lang/String; = "value"
