.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$2;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvProfile;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$UvRay;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$InactiveTimer;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Profile;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Stress;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Sleep;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Temperature;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Humidity;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BodyTemperature;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Pedometer;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodPressure;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Type;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;
    }
.end annotation


# static fields
.field public static final ACTION_HEALTH_DATA_UPDATED:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

.field public static final ACTION_SYNC_FAILED:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.action.SYNC_FAILED"

.field public static final CONNECTIVITY_TYPE_ALL:I = 0x0

.field public static final CONNECTIVITY_TYPE_ANT:I = 0x5

.field public static final CONNECTIVITY_TYPE_BLUETOOTH:I = 0x1

.field public static final CONNECTIVITY_TYPE_INTERNAL:I = 0x4

.field public static final CONNECTIVITY_TYPE_NFC:I = 0x6

.field public static final CONNECTIVITY_TYPE_SAMSUNG_ACCESSORY:I = 0x7

.field public static final CONNECTIVITY_TYPE_SAP:I = 0x3

.field public static final CONNECTIVITY_TYPE_USB:I = 0x2

.field public static final DATA_TYPE_ALL:I = 0x0

.field public static final DATA_TYPE_BLOODGLUCOSE:I = 0x3

.field public static final DATA_TYPE_BODYTEMPERATURE:I = 0x7

.field public static final DATA_TYPE_BlOODPRESSURE:I = 0x2

.field public static final DATA_TYPE_COACHING:I = 0xc

.field public static final DATA_TYPE_COACHING_RESULT:I = 0xd

.field public static final DATA_TYPE_ELECTROCARDIOGRAM:I = 0x10

.field public static final DATA_TYPE_EXERCISE:I = 0x9

.field public static final DATA_TYPE_FITNESS:I = 0x8

.field public static final DATA_TYPE_HEARTRATEMONITOR:I = 0x4

.field public static final DATA_TYPE_HUMIDITY:I = 0x12

.field public static final DATA_TYPE_INACTIVE_TIMER:I = 0x15

.field public static final DATA_TYPE_LOCATION:I = 0x6

.field public static final DATA_TYPE_PEDOMETER:I = 0x5

.field public static final DATA_TYPE_PULSEOXIMETER:I = 0x11

.field public static final DATA_TYPE_SLEEP:I = 0xa

.field public static final DATA_TYPE_STEP_LEVEL:I = 0xf

.field public static final DATA_TYPE_STRESS:I = 0xb

.field public static final DATA_TYPE_TEMPERATURE:I = 0x13

.field public static final DATA_TYPE_USER_PROFILE:I = 0xe

.field public static final DATA_TYPE_UVRAY_PROFILE:I = 0x16

.field public static final DATA_TYPE_UV_RAY:I = 0x14

.field public static final DATA_TYPE_WEIGHT:I = 0x1

.field public static final EXTRA_CONNECTION_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.extra.CONNECTION_TYPE"

.field public static final EXTRA_DATA_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.extra.DATA_TYPE"

.field public static final EXTRA_DEVICE_TYPE:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.extra.DEVICE_TYPE"

.field public static final EXTRA_ERROR_CODE:Ljava/lang/String; = "com.samsung.android.sdk.health.sensor.extra.ERROR_CODE"

.field public static final MAX_LENGTH_DEVICE_NAME:I = 0x20

.field public static final STATE_JOINED:I = 0x2

.field public static final STATE_JOIN_IN_PROGRESS:I = 0x1

.field public static final STATE_LISTENING:I = 0x80

.field public static final STATE_NOT_BOUND:I = 0x800

.field public static final STATE_NOT_JOINED:I = 0x0

.field public static final STATE_RECEIVING:I = 0x10

.field public static final STATE_RECORDING:I = 0x200

.field public static final STATE_REQUEST_IN_PROGRESS:I = 0x400

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private _device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

.field private isCanceled:Z

.field protected mCallBackHandler:Landroid/os/Handler;

.field private final mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

.field private mExerciseId:J

.field protected final mLock:Ljava/lang/Object;

.field private mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

.field protected mStateStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sensorListener:Lcom/samsung/android/sdk/health/sensor/SensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    .line 117
    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 128
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->sensorListener:Lcom/samsung/android/sdk/health/sensor/SensorListener;

    .line 1353
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

    .line 828
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 829
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 831
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 832
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    :cond_1
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Landroid/content/Context;)V
    .locals 5
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .param p2, "uContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    .line 117
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 128
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    .line 134
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->sensorListener:Lcom/samsung/android/sdk/health/sensor/SensorListener;

    .line 1353
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

    .line 843
    if-nez p1, :cond_0

    .line 844
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    .line 846
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 847
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 849
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getNextInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setObjectId(I)V

    .line 850
    const/16 v0, 0x65

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v1

    int-to-long v2, v1

    invoke-static {p2, v0, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 851
    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    return-object v0
.end method

.method private deinitialize()V
    .locals 2

    .prologue
    .line 222
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 224
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->SERVICE_UNBIND:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 225
    return-void
.end method

.method private getNextInt()I
    .locals 4

    .prologue
    .line 854
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 855
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v1

    return v1
.end method

.method private declared-synchronized getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .locals 1

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->sensorListener:Lcom/samsung/android/sdk/health/sensor/SensorListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "target"    # Ljava/lang/Integer;
    .param p2, "replaceWith"    # Ljava/lang/Integer;

    .prologue
    .line 1723
    monitor-enter p0

    const/4 v1, 0x0

    .line 1724
    .local v1, "curNode":Ljava/lang/Integer;
    :try_start_0
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    .line 1727
    .local v2, "tempStack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1729
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/Integer;

    move-object v1, v0

    .line 1730
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 1732
    if-eqz p2, :cond_0

    .line 1734
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v3, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1723
    .end local v2    # "tempStack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1739
    .restart local v2    # "tempStack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Integer;>;"
    :cond_1
    :try_start_1
    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1744
    :cond_2
    :goto_1
    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1746
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1748
    :cond_3
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V
    .locals 2
    .param p1, "sensorListener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 143
    monitor-enter p0

    if-nez p1, :cond_0

    .line 144
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 146
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->sensorListener:Lcom/samsung/android/sdk/health/sensor/SensorListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public close()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 1909
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v4, "close() is called"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1910
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v3, :cond_3

    .line 1912
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>()V

    .line 1913
    .local v2, "removeObselete":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    const-string v3, "CLEAR_OBSOLETE_CALLBACKS"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->setCommandId(Ljava/lang/String;)V

    .line 1914
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v3, :cond_5

    .line 1915
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1917
    .local v0, "curTop":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_2

    .line 1921
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x10

    if-eq v3, v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x200

    if-ne v3, v4, :cond_4

    .line 1923
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1944
    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_1 .. :try_end_1} :catch_7

    .line 1967
    :cond_2
    :goto_1
    const/16 v3, 0x65

    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getObjectId()I

    move-result v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->releaseReference(II)Z

    .line 1974
    .end local v0    # "curTop":Ljava/lang/Integer;
    .end local v2    # "removeObselete":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    :cond_3
    return-void

    .line 1925
    .restart local v0    # "curTop":Ljava/lang/Integer;
    .restart local v2    # "removeObselete":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    :cond_4
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x80

    if-ne v3, v4, :cond_1

    .line 1927
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopListeningData()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 1930
    :catch_0
    move-exception v1

    .line 1932
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1934
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 1936
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1938
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v1

    .line 1940
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 1946
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_3
    move-exception v1

    .line 1948
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 1950
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v1

    .line 1952
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_1

    .line 1954
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v1

    .line 1956
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 1958
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :catch_6
    move-exception v1

    .line 1960
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 1962
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_7
    move-exception v1

    .line 1964
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_1

    .line 1971
    .end local v0    # "curTop":Ljava/lang/Integer;
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_5
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v4, "Service is not connected"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public connect(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 3040
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v3, "connect"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3042
    if-nez p1, :cond_0

    .line 3043
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3044
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_1

    .line 3045
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3047
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-nez v2, :cond_2

    .line 3049
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v3, "ShealthSensorDevice: Android service connection with service failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3050
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Service connection with health service failed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3052
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-nez v2, :cond_3

    .line 3053
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Service is not connected"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 3056
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 3058
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 3059
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    .line 3060
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 3062
    :cond_4
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3064
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "callBackHandlerWorker"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 3065
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 3066
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 3068
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 3069
    .local v1, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 3070
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V

    .line 3071
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V

    .line 3072
    return-void

    .line 3062
    .end local v0    # "commandWorker":Landroid/os/HandlerThread;
    .end local v1    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public disconnect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 3083
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v1, "disconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3084
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 3085
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3088
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v0, :cond_1

    .line 3089
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 3090
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    .line 3095
    return-void

    .line 3093
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Service is not connected"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getConnectivityType()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 970
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 971
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 973
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getConnectivityType() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    return v0
.end method

.method public getDataType()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 939
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 940
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 864
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 865
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 896
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 897
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 901
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getManufacturer()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 880
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 881
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 953
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 954
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 958
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getProtocol()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 818
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 819
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getState()I
    .locals 4

    .prologue
    .line 1613
    monitor-enter p0

    const/16 v0, 0x800

    .line 1614
    .local v0, "state":I
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1616
    const/16 v0, 0x800

    .line 1623
    :goto_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getState() state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1624
    monitor-exit p0

    return v0

    .line 1620
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1613
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public getType()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 913
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 914
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    return v0
.end method

.method public isConnected()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 989
    const/4 v0, 0x0

    .line 991
    .local v0, "bConnected":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_0

    .line 992
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 993
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-nez v1, :cond_1

    .line 994
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 996
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->isConnected(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)Z

    move-result v0

    .line 997
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isConnected() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    return v0
.end method

.method public join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1016
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v4, "join() is called"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    if-nez p1, :cond_0

    .line 1019
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "invalid listener"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1020
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v3, :cond_1

    .line 1021
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v4, "Invalid device"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1023
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-nez v3, :cond_2

    .line 1024
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v4, "Service is not connected"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1026
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1028
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1029
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Looper;->quit()V

    .line 1030
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 1032
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1034
    new-instance v0, Landroid/os/HandlerThread;

    const-string v3, "callBackHandlerWorker"

    invoke-direct {v0, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1035
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1036
    new-instance v3, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    .line 1039
    :try_start_1
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    invoke-direct {v2, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 1041
    .local v2, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->JOIN_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1042
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v3, :cond_4

    .line 1043
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v3, v4, v2}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->join(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1055
    return-void

    .line 1032
    .end local v0    # "commandWorker":Landroid/os/HandlerThread;
    .end local v2    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1045
    .restart local v0    # "commandWorker":Landroid/os/HandlerThread;
    .restart local v2    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    :cond_4
    :try_start_3
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v4, "Service is not connected"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1

    .line 1046
    .end local v2    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
    :catch_0
    move-exception v1

    .line 1047
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1048
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1049
    throw v1

    .line 1050
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 1051
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1052
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1053
    throw v1
.end method

.method public leave()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 1066
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v1, "leave() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 1068
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v0, :cond_1

    .line 1072
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->LEAVE_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1073
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->leave(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)V

    .line 1078
    return-void

    .line 1076
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Service is not connected"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected declared-synchronized processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    .prologue
    const/4 v4, 0x1

    .line 1633
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StateTransitionEvent received : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for device:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1634
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current state stack : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for device:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$2;->$SwitchMap$com$samsung$android$sdk$health$sensor$ShealthSensorDevice$StateTransitionEvent:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1718
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "After state change stack : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for device:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1719
    monitor-exit p0

    return-void

    .line 1640
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1633
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1645
    :pswitch_1
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1647
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1648
    .local v0, "curTop":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_0

    .line 1650
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1657
    .end local v0    # "curTop":Ljava/lang/Integer;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1659
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1660
    .restart local v0    # "curTop":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1662
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1663
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1672
    .end local v0    # "curTop":Ljava/lang/Integer;
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->clear()V

    .line 1673
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1678
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1683
    :pswitch_5
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1693
    :pswitch_6
    const/16 v1, 0x200

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1694
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1695
    const/16 v1, 0x80

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1701
    :pswitch_7
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x200

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1706
    :pswitch_8
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mStateStack:Ljava/util/Stack;

    const/16 v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1712
    :pswitch_9
    const/16 v1, 0x400

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->searchAndRemove(Ljava/lang/Integer;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1636
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public record()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x2718

    const/4 v7, 0x4

    const-wide/16 v8, -0x1

    .line 1181
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v6, "record() is called"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1182
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v5, :cond_0

    .line 1183
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v6, "Invalid device"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1185
    :cond_0
    const/4 v0, 0x0

    .line 1186
    .local v0, "checkDeviceParams":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v5}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->isApplicationPlugin()Z

    move-result v3

    .line 1187
    .local v3, "isShealth":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v2

    .line 1188
    .local v2, "deviceType":I
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    .line 1190
    .local v1, "connectivityType":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v5

    if-nez v5, :cond_1

    .line 1191
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Wrong State errorCode : 3"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1193
    :cond_1
    if-ne v1, v7, :cond_2

    if-ne v2, v10, :cond_2

    .line 1195
    const/4 v0, 0x1

    .line 1198
    :cond_2
    if-eqz v0, :cond_6

    .line 1199
    if-eqz v3, :cond_5

    .line 1200
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v5, :cond_4

    .line 1201
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v5, v6, v8, v9}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;J)I

    move-result v4

    .line 1204
    .local v4, "returnValue":I
    if-eqz v4, :cond_3

    .line 1206
    sparse-switch v4, :sswitch_data_0

    .line 1215
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Internal Error - errorCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1209
    :sswitch_0
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wrong State errorCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1212
    :sswitch_1
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v6, "record cannot be called on non-streamingDataType"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1221
    :cond_3
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->RECORD_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1223
    iput-wide v8, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    .line 1261
    .end local v4    # "returnValue":I
    :goto_0
    return-void

    .line 1225
    :cond_4
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "Service is not connected"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1229
    :cond_5
    iput-wide v8, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    .line 1231
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    .line 1232
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->showConfirmation(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)V

    goto :goto_0

    .line 1239
    :cond_6
    const/16 v5, 0x2719

    if-eq v2, v5, :cond_8

    const/16 v5, 0x271b

    if-eq v2, v5, :cond_8

    if-ne v2, v10, :cond_7

    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v5

    if-ne v5, v7, :cond_8

    :cond_7
    const/16 v5, 0x271f

    if-eq v2, v5, :cond_8

    const/16 v5, 0x2720

    if-eq v2, v5, :cond_8

    const/16 v5, 0x271a

    if-eq v2, v5, :cond_8

    const/16 v5, 0x271c

    if-eq v2, v5, :cond_8

    const/16 v5, 0x2724

    if-ne v2, v5, :cond_9

    .line 1248
    :cond_8
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wrong device type : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1251
    :cond_9
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v6, "record cannot be called on non-streamingDataType"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1206
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public record(J)V
    .locals 9
    .param p1, "exerciseId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x2718

    const/4 v6, 0x4

    .line 1280
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "record(exerciseId) is called - exerciseId is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1281
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v3, :cond_0

    .line 1282
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Invalid device"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1284
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v3}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->isApplicationPlugin()Z

    move-result v1

    .line 1285
    .local v1, "isShealth":Z
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v0

    .line 1287
    .local v0, "deviceType":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v3

    if-nez v3, :cond_1

    .line 1288
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Wrong State errorCode : 3"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1290
    :cond_1
    const/16 v3, 0x2719

    if-eq v0, v3, :cond_3

    const/16 v3, 0x271b

    if-eq v0, v3, :cond_3

    if-ne v0, v7, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    if-ne v3, v6, :cond_3

    :cond_2
    const/16 v3, 0x271f

    if-eq v0, v3, :cond_3

    const/16 v3, 0x2720

    if-eq v0, v3, :cond_3

    const/16 v3, 0x271a

    if-eq v0, v3, :cond_3

    const/16 v3, 0x271c

    if-eq v0, v3, :cond_3

    const/16 v3, 0x2724

    if-ne v0, v3, :cond_8

    .line 1299
    :cond_3
    if-eqz v1, :cond_7

    .line 1300
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gez v3, :cond_4

    .line 1301
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong Exercise Id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1303
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v3, :cond_6

    .line 1305
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v3, v4, p1, p2}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->record(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;J)I

    move-result v2

    .line 1306
    .local v2, "returnValue":I
    if-eqz v2, :cond_5

    .line 1308
    sparse-switch v2, :sswitch_data_0

    .line 1317
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Internal Error - errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1311
    :sswitch_0
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong State errorCode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1314
    :sswitch_1
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v4, "record cannot be called on non-streamingDataType"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1323
    :cond_5
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->RECORD_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1325
    iput-wide p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    .line 1346
    .end local v2    # "returnValue":I
    :goto_0
    return-void

    .line 1329
    :cond_6
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v4, "Service is not connected"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1332
    :cond_7
    iput-wide p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mExerciseId:J

    .line 1334
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    .line 1335
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mDataCallback:Lcom/samsung/android/sdk/health/content/_ContentServiceCallback$Stub;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->showConfirmation(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/content/_ContentServiceCallback;)V

    goto :goto_0

    .line 1338
    :cond_8
    if-ne v0, v7, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v3

    if-ne v3, v6, :cond_9

    .line 1340
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong device type : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1344
    :cond_9
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v4, "record cannot be called on non-streamingDataType"

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1308
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public rename(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 1093
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rename() is called - name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x20

    if-le v1, v2, :cond_1

    .line 1097
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1099
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_2

    .line 1100
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1101
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-nez v1, :cond_3

    .line 1102
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1104
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v2, p1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->rename(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Ljava/lang/String;)I

    move-result v0

    .line 1105
    .local v0, "error":I
    if-nez v0, :cond_4

    .line 1106
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->setName(Ljava/lang/String;)V

    .line 1110
    return-void

    .line 1108
    :cond_4
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Device is wrong state - error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceWrongStatusException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/SensorListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .param p2, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 3242
    if-nez p2, :cond_0

    .line 3243
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid command"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3244
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 3245
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3248
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v1

    const/16 v2, 0x800

    if-eq v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v1

    if-nez v1, :cond_4

    .line 3250
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V

    .line 3251
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;

    invoke-direct {v1, p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorRequestListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V

    .line 3288
    :cond_3
    return-void

    .line 3257
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v1, :cond_5

    .line 3258
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V

    .line 3259
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->REQUEST_COMMAND_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 3260
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->access$800(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    .line 3262
    .local v0, "errorCode":I
    if-eqz v0, :cond_3

    .line 3263
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 3264
    sparse-switch v0, :sswitch_data_0

    .line 3280
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal Error - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3266
    :sswitch_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v2, "ERROR_NOT_SUPPORTED"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3270
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v2, "ERROR_WRONG_REQUEST"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3274
    :sswitch_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Wrong State Some othe Application has requested the data"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3277
    :sswitch_3
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;

    const-string v2, "Already StartReceivingData/request has been called"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3284
    .end local v0    # "errorCode":I
    :cond_5
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3264
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_0
    .end sparse-switch
.end method

.method public request(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 4
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1566
    if-nez p1, :cond_0

    .line 1567
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid command"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1568
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_1

    .line 1569
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1571
    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request() is called - Command id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->getCommandId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1573
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v1, :cond_2

    .line 1574
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->REQUEST_COMMAND_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1575
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->access$800(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->request(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v0

    .line 1577
    .local v0, "errorCode":I
    if-eqz v0, :cond_3

    .line 1578
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1579
    sparse-switch v0, :sswitch_data_0

    .line 1595
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal Error - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1581
    :sswitch_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v2, "ERROR_NOT_SUPPORTED"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1585
    :sswitch_1
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v2, "ERROR_WRONG_REQUEST"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1589
    :sswitch_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Wrong State Some othe Application has requested the data"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1592
    :sswitch_3
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;

    const-string v2, "Already StartReceivingData/request has been called"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1599
    .end local v0    # "errorCode":I
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1603
    .restart local v0    # "errorCode":I
    :cond_3
    return-void

    .line 1579
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_0
    .end sparse-switch
.end method

.method public startListeningData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1464
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v3, "startListeningData() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1466
    if-nez p1, :cond_0

    .line 1467
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1468
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_1

    .line 1469
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1471
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v2, :cond_4

    .line 1473
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v2

    if-nez v2, :cond_2

    .line 1474
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Wrong State errorCode : 3"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1476
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 1478
    .local v1, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->startListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I

    move-result v0

    .line 1480
    .local v0, "errorCode":I
    if-eqz v0, :cond_3

    .line 1482
    sparse-switch v0, :sswitch_data_0

    .line 1491
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Internal Error - errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1485
    :sswitch_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrong State - errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1488
    :sswitch_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Arguments passed or not valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1496
    :cond_3
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1502
    return-void

    .line 1499
    .end local v0    # "errorCode":I
    .end local v1    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    :cond_4
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Service is not connected"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1482
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V
    .locals 7
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3107
    monitor-enter p0

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3108
    monitor-exit p0

    return-void

    .line 3107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;ZJ)V
    .locals 7
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .param p2, "record"    # Z
    .param p3, "exerciseId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 3124
    monitor-enter p0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3125
    monitor-exit p0

    return-void

    .line 3124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 11
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/SensorListener;
    .param p2, "record"    # Z
    .param p3, "exerciseId"    # J
    .param p5, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x2718

    const/4 v1, 0x4

    .line 3142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_0

    .line 3143
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 3145
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getType()I

    move-result v8

    .line 3146
    .local v8, "deviceType":I
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v7

    .line 3148
    .local v7, "connectivityType":I
    if-eqz p2, :cond_5

    .line 3151
    const/16 v0, 0x2719

    if-eq v8, v0, :cond_2

    const/16 v0, 0x271b

    if-eq v8, v0, :cond_2

    if-ne v8, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    if-ne v0, v1, :cond_2

    :cond_1
    const/16 v0, 0x271f

    if-eq v8, v0, :cond_2

    const/16 v0, 0x2720

    if-eq v8, v0, :cond_2

    const/16 v0, 0x271a

    if-eq v8, v0, :cond_2

    const/16 v0, 0x271c

    if-eq v8, v0, :cond_2

    const/16 v0, 0x2724

    if-ne v8, v0, :cond_3

    .line 3160
    :cond_2
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_5

    .line 3161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong Exercise Id : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3163
    :cond_3
    if-ne v8, v3, :cond_4

    if-ne v7, v1, :cond_4

    .line 3165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong device type : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3169
    :cond_4
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    const-string v1, "record cannot be called on non-streamingDataType"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3173
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v0

    const/16 v1, 0x800

    if-eq v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v0

    if-nez v0, :cond_7

    .line 3175
    :cond_6
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V

    .line 3176
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;

    move-object v2, p0

    move v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3219
    :goto_0
    monitor-exit p0

    return-void

    .line 3180
    :cond_7
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    .line 3182
    if-nez p1, :cond_8

    .line 3183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3184
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v0, :cond_9

    .line 3185
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v1, "Invalid device"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3187
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v0, :cond_c

    .line 3188
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->setSensorListener(Lcom/samsung/android/sdk/health/sensor/SensorListener;)V

    .line 3189
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 3190
    .local v2, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez p5, :cond_a

    const/4 v6, 0x0

    :goto_1
    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->startReceiving(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;ZJLcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;)I

    move-result v9

    .line 3192
    .local v9, "errorCode":I
    if-eqz v9, :cond_b

    .line 3194
    packed-switch v9, :pswitch_data_0

    .line 3209
    :pswitch_0
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal Error - errorCode : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3190
    .end local v9    # "errorCode":I
    :cond_a
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->_command:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    invoke-static/range {p5 .. p5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;->access$800(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;

    move-result-object v6

    goto :goto_1

    .line 3197
    .restart local v9    # "errorCode":I
    :pswitch_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;

    const-string v1, "Already StartReceivingData has been called"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3200
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong State - errorCode : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3203
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3206
    :pswitch_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong State - errorCode : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3213
    :cond_b
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    goto/16 :goto_0

    .line 3216
    .end local v2    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    .end local v9    # "errorCode":I
    :cond_c
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Service is not connected"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3194
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1124
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v3, "startReceivingData() is called"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    .line 1128
    if-nez p1, :cond_0

    .line 1129
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1124
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 1130
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v2, :cond_1

    .line 1131
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v3, "Invalid device"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1133
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v2, :cond_3

    .line 1134
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 1136
    .local v1, "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v2, v3, v1}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/_SensorServiceDataListener;)I

    move-result v0

    .line 1139
    .local v0, "errorCode":I
    if-eqz v0, :cond_2

    .line 1141
    packed-switch v0, :pswitch_data_0

    .line 1156
    :pswitch_0
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Internal Error - errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1144
    :pswitch_1
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;

    const-string v3, "Already StartReceivingData has been called"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1147
    :pswitch_2
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrong State - errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1150
    :pswitch_3
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "invalid listener"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1153
    :pswitch_4
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrong State - errorCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1160
    :cond_2
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->START_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1165
    monitor-exit p0

    return-void

    .line 1163
    .end local v0    # "errorCode":I
    .end local v1    # "wrappedListener":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;
    :cond_3
    :try_start_2
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v3, "Service is not connected"

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1141
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public stopListeningData()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1514
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v2, "stopListeningData() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_0

    .line 1516
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1518
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v1, :cond_3

    .line 1520
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v1

    if-nez v1, :cond_1

    .line 1521
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Wrong State errorCode : 3"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1523
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->stopListeningData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I

    move-result v0

    .line 1526
    .local v0, "errorCode":I
    if-eqz v0, :cond_2

    .line 1528
    packed-switch v0, :pswitch_data_0

    .line 1534
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal Error - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1531
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong State - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1539
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_LISTENING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1546
    return-void

    .line 1542
    .end local v0    # "errorCode":I
    :cond_3
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1528
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized stopReceivingData()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalStateException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
        }
    .end annotation

    .prologue
    .line 1414
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    const-string v2, "stopReceivingData() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1416
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    if-nez v1, :cond_0

    .line 1417
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    const-string v2, "Invalid device"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1414
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1419
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getState()I

    move-result v1

    if-nez v1, :cond_1

    .line 1420
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Wrong State errorCode : 3"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1422
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->isCanceled:Z

    .line 1424
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v1, :cond_3

    .line 1426
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mServiceBinder:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->_device:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->stopReceivingData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;)I

    move-result v0

    .line 1427
    .local v0, "errorCode":I
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 1429
    if-eqz v0, :cond_2

    .line 1431
    packed-switch v0, :pswitch_data_0

    .line 1437
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal Error - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1434
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong State - errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1442
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->STOP_RECEIVING_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1449
    monitor-exit p0

    return-void

    .line 1447
    .end local v0    # "errorCode":I
    :cond_3
    :try_start_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Service is not connected"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1431
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
