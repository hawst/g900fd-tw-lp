.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$TagColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TagColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CUSTOM_ICON_INDEX:Ljava/lang/String; = "custom_icon_index"

.field public static final CUSTOM_TAG_NAME:Ljava/lang/String; = "custom_tag_name"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final TABLE_NAME_TYPE:Ljava/lang/String; = "table_name_type"

.field public static final TABLE_RECORD_ID:Ljava/lang/String; = "table_record_id"

.field public static final TAG_INDEX:Ljava/lang/String; = "tag_index"
