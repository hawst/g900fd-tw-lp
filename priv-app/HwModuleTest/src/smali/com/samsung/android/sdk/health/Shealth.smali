.class public Lcom/samsung/android/sdk/health/Shealth;
.super Ljava/lang/Object;
.source "Shealth.java"


# static fields
.field public static final ENABLE_HEALTH_SERVICE_UPDATE:Z = false

.field private static final HEALTH_SERVICE_PLATFORM:Ljava/lang/String; = "com.sec.android.service.health"

.field private static final ONE_MONTH_IN_MILLIS:J = 0x9a7ec800L

.field public static final SHEALTH_NEEDS_TO_BE_UPGRADED:I = 0x3e9

.field public static final S_HEALTH_PACKAGE:Ljava/lang/String; = "com.sec.android.app.shealth"

.field private static final TAG:Ljava/lang/String;

.field private static formatterYMD:Ljava/text/SimpleDateFormat;

.field private static formatterYMDHMS:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    const-class v0, Lcom/samsung/android/sdk/health/Shealth;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/Shealth;->TAG:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/samsung/android/sdk/health/Shealth;->formatterYMD:Ljava/text/SimpleDateFormat;

    .line 68
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/samsung/android/sdk/health/Shealth;->formatterYMDHMS:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    return-void
.end method

.method private static getCountryISOCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 256
    const-string v2, ""

    .line 259
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 260
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 261
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 262
    :catch_0
    move-exception v3

    .line 263
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isChinaModel()Z
    .locals 4

    .prologue
    .line 272
    const/4 v0, 0x0

    .line 273
    .local v0, "result":Z
    const-string v1, "CN"

    invoke-static {}, Lcom/samsung/android/sdk/health/Shealth;->getCountryISOCode()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    const/4 v0, 0x1

    .line 276
    :cond_0
    return v0
.end method

.method public static printLog(Ljava/lang/String;)V
    .locals 24
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 174
    if-eqz p0, :cond_0

    const-string v20, ""

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const/16 v17, 0x0

    .line 183
    .local v17, "out":Ljava/io/PrintWriter;
    :try_start_0
    new-instance v16, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/Android/data/com.sec.android.app.shealth/file/logs"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    .local v16, "logFolder":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->mkdirs()Z

    .line 186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 188
    .local v7, "currentTime":Ljava/util/Calendar;
    new-instance v14, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Lcom/samsung/android/sdk/health/Shealth;->formatterYMD:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".log"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 192
    .local v14, "logFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->createNewFile()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 194
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v15

    .line 195
    .local v15, "logFiles":[Ljava/io/File;
    move-object v4, v15

    .local v4, "arr$":[Ljava/io/File;
    array-length v12, v4

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_3

    aget-object v10, v4, v11
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    .local v10, "f":Ljava/io/File;
    :try_start_1
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    sget-object v22, Lcom/samsung/android/sdk/health/Shealth;->formatterYMD:Ljava/text/SimpleDateFormat;

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    sub-long v20, v20, v22

    const-wide v22, 0x9a7ec800L

    cmp-long v20, v20, v22

    if-lez v20, :cond_2

    .line 201
    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    :cond_2
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 204
    :catch_0
    move-exception v8

    .line 206
    .local v8, "e":Ljava/text/ParseException;
    :try_start_2
    sget-object v20, Lcom/samsung/android/sdk/health/Shealth;->TAG:Ljava/lang/String;

    const-string v21, "File format is not the custom log file"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 238
    .end local v4    # "arr$":[Ljava/io/File;
    .end local v7    # "currentTime":Ljava/util/Calendar;
    .end local v8    # "e":Ljava/text/ParseException;
    .end local v10    # "f":Ljava/io/File;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v14    # "logFile":Ljava/io/File;
    .end local v15    # "logFiles":[Ljava/io/File;
    .end local v16    # "logFolder":Ljava/io/File;
    :catch_1
    move-exception v8

    .line 240
    .local v8, "e":Ljava/io/IOException;
    :goto_3
    :try_start_3
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 248
    if-eqz v17, :cond_0

    .line 250
    invoke-virtual/range {v17 .. v17}, Ljava/io/PrintWriter;->close()V

    goto/16 :goto_0

    .line 211
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v7    # "currentTime":Ljava/util/Calendar;
    .restart local v14    # "logFile":Ljava/io/File;
    .restart local v16    # "logFolder":Ljava/io/File;
    :cond_3
    :try_start_4
    new-instance v18, Ljava/io/PrintWriter;

    new-instance v20, Ljava/io/BufferedWriter;

    new-instance v21, Ljava/io/FileWriter;

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v0, v14, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V

    invoke-direct/range {v20 .. v21}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 212
    .end local v17    # "out":Ljava/io/PrintWriter;
    .local v18, "out":Ljava/io/PrintWriter;
    const/16 v13, 0x1000

    .line 213
    .local v13, "length":I
    const/4 v6, 0x1

    .line 216
    .local v6, "count":I
    :try_start_5
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v19

    .line 217
    .local v19, "stackTraceElements":[Ljava/lang/StackTraceElement;
    const-string v5, " : "

    .line 218
    .local v5, "classNameAndLine":Ljava/lang/String;
    if-eqz v19, :cond_5

    .line 220
    move-object/from16 v4, v19

    .local v4, "arr$":[Ljava/lang/StackTraceElement;
    array-length v12, v4

    .restart local v12    # "len$":I
    const/4 v11, 0x0

    .restart local v11    # "i$":I
    :goto_4
    if-ge v11, v12, :cond_5

    aget-object v9, v4, v11

    .line 222
    .local v9, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_6

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.sec.android.service.health"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_4

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.sec.android.app.shealth"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 224
    :cond_4
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v9}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "line : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 229
    .end local v4    # "arr$":[Ljava/lang/StackTraceElement;
    .end local v9    # "element":Ljava/lang/StackTraceElement;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    :cond_5
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/samsung/android/sdk/health/Shealth;->formatterYMDHMS:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 230
    :goto_5
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v20

    mul-int v21, v13, v6

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_7

    .line 232
    add-int/lit8 v20, v6, -0x1

    mul-int v20, v20, v13

    mul-int v21, v13, v6

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 233
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 220
    .restart local v4    # "arr$":[Ljava/lang/StackTraceElement;
    .restart local v9    # "element":Ljava/lang/StackTraceElement;
    .restart local v11    # "i$":I
    .restart local v12    # "len$":I
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_4

    .line 235
    .end local v4    # "arr$":[Ljava/lang/StackTraceElement;
    .end local v9    # "element":Ljava/lang/StackTraceElement;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    :cond_7
    add-int/lit8 v20, v6, -0x1

    mul-int v20, v20, v13

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v21

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 248
    if-eqz v18, :cond_9

    .line 250
    invoke-virtual/range {v18 .. v18}, Ljava/io/PrintWriter;->close()V

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/PrintWriter;
    .restart local v17    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_0

    .line 242
    .end local v5    # "classNameAndLine":Ljava/lang/String;
    .end local v6    # "count":I
    .end local v7    # "currentTime":Ljava/util/Calendar;
    .end local v13    # "length":I
    .end local v14    # "logFile":Ljava/io/File;
    .end local v16    # "logFolder":Ljava/io/File;
    .end local v19    # "stackTraceElements":[Ljava/lang/StackTraceElement;
    :catch_2
    move-exception v20

    .line 248
    :goto_6
    if-eqz v17, :cond_0

    .line 250
    invoke-virtual/range {v17 .. v17}, Ljava/io/PrintWriter;->close()V

    goto/16 :goto_0

    .line 248
    :catchall_0
    move-exception v20

    :goto_7
    if-eqz v17, :cond_8

    .line 250
    invoke-virtual/range {v17 .. v17}, Ljava/io/PrintWriter;->close()V

    :cond_8
    throw v20

    .line 248
    .end local v17    # "out":Ljava/io/PrintWriter;
    .restart local v6    # "count":I
    .restart local v7    # "currentTime":Ljava/util/Calendar;
    .restart local v13    # "length":I
    .restart local v14    # "logFile":Ljava/io/File;
    .restart local v16    # "logFolder":Ljava/io/File;
    .restart local v18    # "out":Ljava/io/PrintWriter;
    :catchall_1
    move-exception v20

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/PrintWriter;
    .restart local v17    # "out":Ljava/io/PrintWriter;
    goto :goto_7

    .line 242
    .end local v17    # "out":Ljava/io/PrintWriter;
    .restart local v18    # "out":Ljava/io/PrintWriter;
    :catch_3
    move-exception v20

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/PrintWriter;
    .restart local v17    # "out":Ljava/io/PrintWriter;
    goto :goto_6

    .line 238
    .end local v17    # "out":Ljava/io/PrintWriter;
    .restart local v18    # "out":Ljava/io/PrintWriter;
    :catch_4
    move-exception v8

    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/PrintWriter;
    .restart local v17    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_3

    .end local v17    # "out":Ljava/io/PrintWriter;
    .restart local v5    # "classNameAndLine":Ljava/lang/String;
    .restart local v18    # "out":Ljava/io/PrintWriter;
    .restart local v19    # "stackTraceElements":[Ljava/lang/StackTraceElement;
    :cond_9
    move-object/from16 v17, v18

    .end local v18    # "out":Ljava/io/PrintWriter;
    .restart local v17    # "out":Ljava/io/PrintWriter;
    goto/16 :goto_0
.end method
