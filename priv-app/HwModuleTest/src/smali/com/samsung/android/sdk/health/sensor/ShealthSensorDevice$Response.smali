.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# static fields
.field public static final ERROR_FAILURE:I = 0x1

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_SUPPORTED:I = 0x2


# instance fields
.field private _response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849
    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 0
    .param p1, "response"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    .prologue
    .line 1852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->_response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    .line 1854
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;
    .param p2, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;

    .prologue
    .line 1831
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V

    return-void
.end method


# virtual methods
.method public getCommandId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->_response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getCommandId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 1863
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->_response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getErrorCode()I

    move-result v0

    return v0
.end method

.method public getErrorDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->_response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getErrorDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponse()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1894
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;->_response:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;->getResponse()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
