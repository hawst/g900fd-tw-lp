.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrientColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FoodNutrientColumns"
.end annotation


# static fields
.field public static final CALCIUM:Ljava/lang/String; = "calcium"

.field public static final CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field public static final CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field public static final DEFAULT_NUMBER_OF_SERVING_UNIT:Ljava/lang/String; = "default_number_of_serving_unit"

.field public static final DIETARY_FIBER:Ljava/lang/String; = "dietary_fiber"

.field public static final FOOD_INFO_ID:Ljava/lang/String; = "food_info__id"

.field public static final IRON:Ljava/lang/String; = "iron"

.field public static final KCAL_IN_UNIT:Ljava/lang/String; = "kcal_in_unit"

.field public static final METRIC_SERVING_AMOUNT:Ljava/lang/String; = "metric_serving_amount"

.field public static final METRIC_SERVING_UNIT:Ljava/lang/String; = "metric_serving_unit"

.field public static final MONOSATURATED_FAT:Ljava/lang/String; = "monosaturated_fat"

.field public static final POLYSATURATED_FAT:Ljava/lang/String; = "polysaturated_fat"

.field public static final POTASSIUM:Ljava/lang/String; = "potassium"

.field public static final PROTEIN:Ljava/lang/String; = "protein"

.field public static final SATURATED_FAT:Ljava/lang/String; = "saturated_fat"

.field public static final SERVING_DESCRIPTION:Ljava/lang/String; = "serving_description"

.field public static final SODIUM:Ljava/lang/String; = "sodium"

.field public static final SUGAR:Ljava/lang/String; = "sugar"

.field public static final TOTAL_FAT:Ljava/lang/String; = "total_fat"

.field public static final TRANS_FAT:Ljava/lang/String; = "trans_fat"

.field public static final VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field public static final VITAMIN_C:Ljava/lang/String; = "vitamin_c"
