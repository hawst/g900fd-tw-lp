.class public interface abstract Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DataListener"
.end annotation


# static fields
.field public static final ERROR_DATA_CANCEL:I = 0x6

.field public static final ERROR_DATA_NOT_FOUND:I = 0x5

.field public static final ERROR_DEVICE_BUSY:I = 0x4

.field public static final ERROR_FAILURE:I = 0x1

.field public static final ERROR_ILLEGAL_ARGUMENT:I = 0x7

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_JOINED:I = 0x3

.field public static final ERROR_UNSUPPORTED:I = 0x8

.field public static final ERROR_WRONG_REQUEST:I = 0x2


# virtual methods
.method public abstract onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
.end method

.method public abstract onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
.end method

.method public abstract onStarted(II)V
.end method

.method public abstract onStopped(II)V
.end method
