.class public interface abstract Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScanListener"
.end annotation


# static fields
.field public static final ERROR_CANCELLED:I = 0x1

.field public static final ERROR_IN_PROGRESS:I = 0x3

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_SENSOR_NOT_READY:I = 0x2

.field public static final MAXIMUM_DURATION:I = 0x64

.field public static final MINIMUM_DURATION:I = 0xa


# virtual methods
.method public abstract onDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
.end method

.method public abstract onStarted(I)V
.end method

.method public abstract onStopped(I)V
.end method
