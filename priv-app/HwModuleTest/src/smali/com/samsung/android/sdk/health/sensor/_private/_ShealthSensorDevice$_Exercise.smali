.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_Exercise"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2698
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2512
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;-><init>()V

    .line 2513
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2516
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;-><init>()V

    .line 2517
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 2518
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 9
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v8, 0x0

    .line 2644
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    .line 2645
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    .line 2646
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    .line 2647
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    .line 2648
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    .line 2649
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    .line 2650
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    .line 2651
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    .line 2652
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    .line 2653
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    .line 2654
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    .line 2655
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    .line 2657
    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    .line 2659
    const-class v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 2660
    .local v1, "temp":[Landroid/os/Parcelable;
    if-eqz v1, :cond_1

    .line 2661
    array-length v5, v1

    new-array v4, v5, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    .line 2662
    .local v4, "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v1

    if-ge v0, v5, :cond_0

    .line 2663
    aget-object v5, v1, v0

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    aput-object v5, v4, v0

    .line 2662
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2664
    :cond_0
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2668
    .end local v0    # "i":I
    .end local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
    :goto_1
    const-class v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 2669
    .local v3, "tempHeartRate":[Landroid/os/Parcelable;
    if-eqz v3, :cond_3

    .line 2670
    array-length v5, v3

    new-array v4, v5, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    .line 2671
    .local v4, "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v5, v3

    if-ge v0, v5, :cond_2

    .line 2672
    aget-object v5, v3, v0

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    aput-object v5, v4, v0

    .line 2671
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2666
    .end local v0    # "i":I
    .end local v3    # "tempHeartRate":[Landroid/os/Parcelable;
    .end local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :cond_1
    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    goto :goto_1

    .line 2673
    .restart local v0    # "i":I
    .restart local v3    # "tempHeartRate":[Landroid/os/Parcelable;
    .restart local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :cond_2
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    .line 2677
    .end local v0    # "i":I
    .end local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :goto_3
    const-class v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v2

    .line 2678
    .local v2, "tempExGoal":[Landroid/os/Parcelable;
    if-eqz v2, :cond_5

    .line 2679
    array-length v5, v2

    new-array v4, v5, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    .line 2680
    .local v4, "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    array-length v5, v2

    if-ge v0, v5, :cond_4

    .line 2681
    aget-object v5, v2, v0

    check-cast v5, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    aput-object v5, v4, v0

    .line 2680
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2675
    .end local v0    # "i":I
    .end local v2    # "tempExGoal":[Landroid/os/Parcelable;
    .end local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    :cond_3
    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    goto :goto_3

    .line 2682
    .restart local v0    # "i":I
    .restart local v2    # "tempExGoal":[Landroid/os/Parcelable;
    .restart local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    :cond_4
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    .line 2687
    .end local v0    # "i":I
    .end local v4    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    :goto_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->trainingEffect:F

    .line 2688
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->recoveryTime:J

    .line 2692
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fatBurnTime:J

    .line 2693
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    .line 2694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->crud:I

    .line 2696
    return-void

    .line 2684
    :cond_5
    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    goto :goto_5
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2523
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 2586
    const/16 v0, 0x9

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 14

    .prologue
    .line 2591
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2592
    .local v8, "loctionRes":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "coachingStr":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2594
    .local v7, "locationStr":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v9, :cond_1

    .line 2595
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v9, v9

    if-ge v6, v9, :cond_1

    .line 2596
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v9, v9, v6

    if-eqz v9, :cond_0

    .line 2597
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2595
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2600
    .end local v6    # "i":I
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2601
    .local v4, "hrrRes":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .line 2603
    .local v5, "hrrResStr":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    if-eqz v9, :cond_3

    .line 2604
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v9, v9

    if-ge v6, v9, :cond_3

    .line 2605
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v9, v9, v6

    if-eqz v9, :cond_2

    .line 2606
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2604
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2610
    .end local v6    # "i":I
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2611
    .local v2, "goalRes":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 2612
    .local v3, "goalResStr":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    if-eqz v9, :cond_5

    .line 2613
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    array-length v9, v9

    if-ge v6, v9, :cond_5

    .line 2614
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    aget-object v9, v9, v6

    if-eqz v9, :cond_4

    .line 2615
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    aget-object v9, v9, v6

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2618
    .end local v6    # "i":I
    :cond_5
    if-eqz v4, :cond_6

    .line 2619
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2621
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    if-eqz v9, :cond_7

    .line 2622
    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2624
    :cond_7
    if-eqz v8, :cond_8

    .line 2625
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2626
    :cond_8
    if-eqz v2, :cond_9

    .line 2627
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2630
    :cond_9
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy-MM-dd  HH:mm:ss a"

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2631
    .local v1, "format":Ljava/text/SimpleDateFormat;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[EXERCISE] type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " time="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-static {v10, v11}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/util/Date;

    iget-wide v12, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-direct {v10, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", duration = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", calorie ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", heartRate = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", distance ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", fitnessLevel = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", averageSpeed = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", maxSpeed = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", maxHeartRate= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", coachingResult ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", GPS = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", heartRateRawData = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", crud = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->crud:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", goalResStr = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", devicePkId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", recoveryTime = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->recoveryTime:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", fatBurnTime = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fatBurnTime:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", trainingEffect = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->trainingEffect:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    const/4 v4, 0x0

    .line 2529
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->time:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2530
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->type:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2531
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->duration:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2532
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->calorie:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2533
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRate:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2534
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->distance:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2535
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fitnessLevel:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2536
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->averageSpeed:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2537
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxSpeed:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2538
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxHeartRate:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2539
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->maxAltitude:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2540
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->minAltitude:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2541
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2542
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v2, :cond_0

    .line 2543
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    .line 2544
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2545
    aget-object v2, v1, v0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2549
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;
    :cond_0
    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 2551
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    if-eqz v2, :cond_2

    .line 2552
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    .line 2553
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2554
    aget-object v2, v1, v0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2553
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2558
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :cond_2
    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 2560
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    if-eqz v2, :cond_4

    .line 2561
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;

    .line 2562
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 2563
    aget-object v2, v1, v0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2562
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2567
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;
    :cond_4
    invoke-virtual {p1, v4, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 2570
    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->trainingEffect:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2571
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->recoveryTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2575
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->fatBurnTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 2576
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->devicePkId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2577
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;->crud:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2581
    return-void
.end method
