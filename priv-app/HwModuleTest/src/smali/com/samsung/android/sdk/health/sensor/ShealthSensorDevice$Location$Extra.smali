.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Extra"
.end annotation


# instance fields
.field public averagePace:F

.field public averageSpeed:F

.field public consumedCalorie:F

.field public declineDistance:F

.field public declineTime:J

.field public flatDistance:F

.field public flatTime:J

.field public inclineDistance:F

.field public inclineTime:J

.field public maxAltitude:F

.field public maxPace:F

.field public maxSpeed:F

.field public minAltitude:F

.field public pace:F

.field public stepCount:I

.field public totalDistance:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 2349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    .line 2353
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    .line 2355
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    .line 2357
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    .line 2359
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    .line 2361
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    .line 2363
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    .line 2365
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    .line 2367
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    .line 2369
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    .line 2371
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    .line 2373
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->pace:F

    .line 2375
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxPace:F

    .line 2377
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averagePace:F

    .line 2379
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    .line 2381
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->stepCount:I

    return-void
.end method
