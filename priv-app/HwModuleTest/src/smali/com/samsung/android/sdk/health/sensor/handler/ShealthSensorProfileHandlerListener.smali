.class public interface abstract Lcom/samsung/android/sdk/health/sensor/handler/ShealthSensorProfileHandlerListener;
.super Ljava/lang/Object;
.source "ShealthSensorProfileHandlerListener.java"


# virtual methods
.method public abstract onDataReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
.end method

.method public abstract onDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V
.end method

.method public abstract onDataStarted(II)V
.end method

.method public abstract onDataStopped(II)V
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
.end method

.method public abstract onStateChanged(I)V
.end method
