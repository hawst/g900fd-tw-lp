.class Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;
.super Lcom/samsung/android/sdk/health/content/serversync/_ActivityListener$Stub;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrapperSyncListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthContentManager;Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/serversync/_ActivityListener$Stub;-><init>()V

    .line 317
    iput-object p2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .line 318
    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 1
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onFinished(II)V

    .line 344
    :cond_0
    return-void
.end method

.method public onProgress(II)V
    .locals 1
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onProgress(II)V

    .line 335
    :cond_0
    return-void
.end method

.method public onStarted(II)V
    .locals 1
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 323
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$WrapperSyncListener;->mActualListener:Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(II)V

    .line 327
    :cond_0
    return-void
.end method
