.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$FoodServerSourceType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FoodServerSourceType"
.end annotation


# static fields
.field public static final BOOHEE:I = 0x46cd4

.field public static final FAT_SECRET:I = 0x46cd2

.field public static final MY_FOOD:I = 0x46cd1

.field public static final NOT_DEFINED:I = -0x1

.field public static final QUICK_INPUT:I = 0x46cd6

.field public static final QUICK_INPUT_SKIPPED:I = 0x46cd7

.field public static final SAMSUNG_OSP:I = 0x46cd3

.field public static final SAMSUNG_WELSTORY:I = 0x46cd5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field
