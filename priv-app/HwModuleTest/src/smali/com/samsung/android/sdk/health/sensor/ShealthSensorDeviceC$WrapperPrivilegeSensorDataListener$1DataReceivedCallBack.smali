.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->onReceived(ILandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataReceivedCallBack"
.end annotation


# instance fields
.field data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

.field dataType:I

.field extra:Landroid/os/Bundle;

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "data"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->dataType:I

    .line 128
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 129
    iput-object p4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->extra:Landroid/os/Bundle;

    .line 130
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mRecord:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;->addData(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->listener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->dataType:I

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;->extra:Landroid/os/Bundle;

    invoke-interface {v1, v2, v0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .line 138
    return-void
.end method
