.class public Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;
.super Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;
.source "HealthCoverBia.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;,
        Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
    }
.end annotation


# static fields
.field public static final BIA_MEASUREMENT_DURATION:I = 0x5

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBodyFatRatio:F

.field private mBodyFatResult:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field private mBodyImpedance:F

.field public mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;IFF)V
    .locals 2
    .param p1, "userInfo"    # Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;
    .param p2, "nFs"    # I
    .param p3, "fCoefA"    # F
    .param p4, "fCoefB"    # F

    .prologue
    const/4 v1, 0x0

    .line 69
    mul-int/lit8 v0, p2, 0x5

    invoke-direct {p0, p2, v0, p3, p4}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;-><init>(IIFF)V

    .line 48
    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatRatio:F

    .line 49
    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyImpedance:F

    .line 50
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->HEALTHY:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatResult:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    .line 70
    iput-object p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    .line 71
    return-void
.end method

.method private getMeanResistance()F
    .locals 10

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, "fRtn":F
    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mPacketCount:I

    .line 101
    .local v2, "nLength":I
    const/4 v4, 0x0

    .line 102
    .local v4, "nStartIdx":I
    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    .line 104
    .local v3, "nLoop":I
    iget v7, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    if-le v2, v7, :cond_0

    .line 105
    iget v7, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    sub-int v4, v2, v7

    .line 109
    :goto_0
    const/4 v6, 0x0

    .line 110
    .local v6, "nSumResist":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v5, v4

    .end local v4    # "nStartIdx":I
    .local v5, "nStartIdx":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 112
    iget-object v7, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mReceivedRawData:[I

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "nStartIdx":I
    .restart local v4    # "nStartIdx":I
    aget v7, v7, v5

    add-int/2addr v6, v7

    .line 110
    add-int/lit8 v1, v1, 0x1

    move v5, v4

    .end local v4    # "nStartIdx":I
    .restart local v5    # "nStartIdx":I
    goto :goto_1

    .line 107
    .end local v1    # "i":I
    .end local v5    # "nStartIdx":I
    .end local v6    # "nSumResist":I
    .restart local v4    # "nStartIdx":I
    :cond_0
    move v3, v2

    goto :goto_0

    .line 114
    .end local v4    # "nStartIdx":I
    .restart local v1    # "i":I
    .restart local v5    # "nStartIdx":I
    .restart local v6    # "nSumResist":I
    :cond_1
    int-to-float v7, v6

    int-to-float v8, v3

    div-float v0, v7, v8

    .line 116
    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyImpedance:F

    .line 117
    sget-object v7, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getMeanResistance(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    return v0
.end method


# virtual methods
.method public checkBiaMeasurementEndCondition()Z
    .locals 14

    .prologue
    .line 123
    const/4 v0, 0x0

    .line 124
    .local v0, "bRtn":Z
    iget v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mPacketCount:I

    iget v10, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    mul-int/lit8 v10, v10, 0x2

    if-lt v9, v10, :cond_1

    .line 125
    iget v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    int-to-double v10, v9

    const-wide v12, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v10, v12

    double-to-int v8, v10

    .line 127
    .local v8, "nWindowLength":I
    const/4 v7, 0x0

    .line 128
    .local v7, "nSumOfPreResist":I
    const/4 v6, 0x0

    .line 130
    .local v6, "nSumOfPostResist":I
    iget v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mPacketCount:I

    iget v10, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mFs:I

    add-int/2addr v10, v8

    sub-int v4, v9, v10

    .line 131
    .local v4, "nIdxPre":I
    iget v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mPacketCount:I

    add-int/lit8 v10, v8, 0x2

    sub-int v2, v9, v10

    .line 132
    .local v2, "nIdxPost":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v3, v2

    .end local v2    # "nIdxPost":I
    .local v3, "nIdxPost":I
    move v5, v4

    .end local v4    # "nIdxPre":I
    .local v5, "nIdxPre":I
    :goto_0
    if-ge v1, v8, :cond_0

    .line 133
    iget-object v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mReceivedRawData:[I

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "nIdxPre":I
    .restart local v4    # "nIdxPre":I
    aget v9, v9, v5

    add-int/2addr v7, v9

    .line 134
    iget-object v9, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mReceivedRawData:[I

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nIdxPost":I
    .restart local v2    # "nIdxPost":I
    aget v9, v9, v3

    add-int/2addr v6, v9

    .line 132
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    .end local v2    # "nIdxPost":I
    .restart local v3    # "nIdxPost":I
    move v5, v4

    .end local v4    # "nIdxPre":I
    .restart local v5    # "nIdxPre":I
    goto :goto_0

    .line 137
    :cond_0
    sub-int v9, v7, v6

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    mul-int/lit8 v10, v8, 0x2

    if-gt v9, v10, :cond_1

    .line 138
    const/4 v0, 0x1

    .line 142
    .end local v1    # "i":I
    .end local v3    # "nIdxPost":I
    .end local v5    # "nIdxPre":I
    .end local v6    # "nSumOfPostResist":I
    .end local v7    # "nSumOfPreResist":I
    .end local v8    # "nWindowLength":I
    :cond_1
    return v0
.end method

.method public getBodyFatRatio()F
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatRatio:F

    return v0
.end method

.method public getBodyFatResult()Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatResult:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    return-object v0
.end method

.method public getBodyFatResultString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatResult:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    invoke-virtual {v0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->getResultString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBodyImpedance()F
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyImpedance:F

    return v0
.end method

.method public processBiaData([I)I
    .locals 6
    .param p1, "arrRawData"    # [I

    .prologue
    .line 75
    const/4 v1, 0x1

    .line 77
    .local v1, "nRtn":I
    array-length v2, p1

    invoke-virtual {p0, p1, v2}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->addRawData([II)I

    .line 79
    invoke-virtual {p0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->checkBiaMeasurementEndCondition()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 80
    const/16 v1, 0xff

    .line 81
    invoke-direct {p0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->getMeanResistance()F

    move-result v0

    .line 82
    .local v0, "fAveRes":F
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v2, v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mGender:I

    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v3, v3, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mAge:I

    iget-object v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v4, v4, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mHeight:F

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v5, v5, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mWeight:F

    invoke-static {v2, v3, v4, v5, v0}, Lcom/sec/dmc/hsl/android/healthsuite/HealthSuiteJni;->getBodyFatRatio(IIFFF)F

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatRatio:F

    .line 83
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatResult:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v3, v3, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mGender:I

    iget-object v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    iget v4, v4, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;->mAge:I

    iget v5, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mBodyFatRatio:F

    invoke-static {v3, v4, v5}, Lcom/sec/dmc/hsl/android/healthsuite/HealthSuiteJni;->getBiaResult(IIF)I

    move-result v3

    # setter for: Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->mValue:I
    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->access$002(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;I)I

    .line 89
    .end local v0    # "fAveRes":F
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mPacketCount:I

    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;->mMaxMeasurementCount:I

    if-lt v2, v3, :cond_0

    .line 86
    const/4 v1, 0x2

    goto :goto_0
.end method
