.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgCommonRawDataFormat"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
    }
.end annotation


# static fields
.field private static RESERVED_SIZE:I


# instance fields
.field public mNumOfChannel:S

.field public mRawDataChannelFormat:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;",
            ">;"
        }
    .end annotation
.end field

.field public mReserved:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x7

    sput v0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->RESERVED_SIZE:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 11
    .param p1, "arrData"    # [B

    .prologue
    const/4 v10, 0x0

    .line 146
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 141
    sget v4, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->RESERVED_SIZE:I

    new-array v4, v4, [B

    iput-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mReserved:[B

    .line 142
    const/4 v4, -0x1

    iput-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mNumOfChannel:S

    .line 143
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    .line 147
    if-eqz p1, :cond_0

    array-length v4, p1

    int-to-long v4, v4

    iget-wide v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mMessageLength:J

    const-wide/16 v8, 0x8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    .line 149
    const/16 v2, 0x8

    .line 150
    .local v2, "nIdx":I
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mReserved:[B

    sget v5, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->RESERVED_SIZE:I

    invoke-static {p1, v2, v4, v10, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    sget v4, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->RESERVED_SIZE:I

    add-int/2addr v2, v4

    .line 151
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    aget-byte v4, p1, v2

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    iput-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mNumOfChannel:S

    .line 152
    sget v4, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RAW_DATA_CHANNEL_FORMAT_SIZE:I

    new-array v0, v4, [B

    .line 153
    .local v0, "arrTmp":[B
    const/4 v1, 0x0

    .local v1, "i":I
    move v2, v3

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    :goto_0
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mNumOfChannel:S

    if-ge v1, v4, :cond_0

    .line 155
    sget v4, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RAW_DATA_CHANNEL_FORMAT_SIZE:I

    invoke-static {p1, v2, v0, v10, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 156
    sget v4, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->RAW_DATA_CHANNEL_FORMAT_SIZE:I

    add-int/2addr v2, v4

    .line 157
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    invoke-direct {v5, v0}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;-><init>([B)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    .end local v0    # "arrTmp":[B
    .end local v1    # "i":I
    .end local v2    # "nIdx":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getChannelFormatById(I)Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
    .locals 4
    .param p1, "nChnId"    # I

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 169
    .local v1, "rawDataChnFormat":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mNumOfChannel:S

    if-ge v0, v2, :cond_0

    .line 171
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    iget-short v2, v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mChannelID:S

    int-to-short v3, p1

    if-ne v2, v3, :cond_1

    .line 173
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "rawDataChnFormat":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
    check-cast v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    .line 177
    .restart local v1    # "rawDataChnFormat":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;
    :cond_0
    return-object v1

    .line 169
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getChannelIDByType(IS)I
    .locals 3
    .param p1, "nStartIdx"    # I
    .param p2, "sType"    # S

    .prologue
    .line 182
    const/4 v1, -0x1

    .line 184
    .local v1, "nRtn":I
    move v0, p1

    .local v0, "i":I
    :goto_0
    iget-short v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mNumOfChannel:S

    if-ge v0, v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    iget-short v2, v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mType:S

    if-ne v2, p2, :cond_2

    .line 188
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;->mRawDataChannelFormat:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;

    iget-short v1, v2, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat$RawDataChannelFormat;->mChannelID:S

    .line 194
    :cond_0
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 200
    :cond_1
    return v1

    .line 184
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
