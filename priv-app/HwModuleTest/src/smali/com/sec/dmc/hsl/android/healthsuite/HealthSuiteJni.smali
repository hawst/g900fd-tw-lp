.class public Lcom/sec/dmc/hsl/android/healthsuite/HealthSuiteJni;
.super Ljava/lang/Object;
.source "HealthSuiteJni.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "_bodyfat_v02_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getBiaResult(IIF)I
.end method

.method public static native getBodyFatRatio(IIFFF)F
.end method

.method public static native getImpedanceAdjustment()D
.end method
