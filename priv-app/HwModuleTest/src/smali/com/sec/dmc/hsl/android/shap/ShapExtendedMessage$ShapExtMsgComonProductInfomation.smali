.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgComonProductInfomation"
.end annotation


# instance fields
.field public Reserved:[B

.field public mManufactureDate:Ljava/util/Calendar;

.field public mManufacturer:Ljava/lang/String;

.field public mManufacturerURL:Ljava/lang/String;

.field public mProductHWVersion:Ljava/lang/String;

.field public mProductModelName:Ljava/lang/String;

.field public mProductSN:Ljava/lang/String;

.field public mProductSWVersion:Ljava/lang/String;

.field public mProductType:I


# direct methods
.method public constructor <init>([B)V
    .locals 8
    .param p1, "arrData"    # [B

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 49
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufactureDate:Ljava/util/Calendar;

    .line 56
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->Reserved:[B

    .line 62
    if-eqz p1, :cond_0

    array-length v2, p1

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mMessageLength:J

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 64
    const/16 v0, 0x8

    .line 66
    .local v0, "nIdx":I
    aget-byte v2, p1, v0

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    iput v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductType:I

    add-int/lit8 v0, v0, 0x2

    .line 67
    aget-byte v2, p1, v0

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    .local v1, "nTmp":I
    add-int/lit8 v0, v0, 0x2

    .line 68
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufactureDate:Ljava/util/Calendar;

    aget-byte v3, p1, v0

    const/16 v4, 0xd

    aget-byte v4, p1, v4

    invoke-virtual {v2, v1, v3, v4}, Ljava/util/Calendar;->set(III)V

    add-int/lit8 v0, v0, 0x2

    .line 70
    aget-byte v2, p1, v0

    const/16 v3, 0xf

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 71
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductModelName:Ljava/lang/String;

    add-int/lit8 v0, v1, 0x10

    .line 73
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 74
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSN:Ljava/lang/String;

    add-int/2addr v0, v1

    .line 76
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 77
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductHWVersion:Ljava/lang/String;

    add-int/2addr v0, v1

    .line 79
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 80
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSWVersion:Ljava/lang/String;

    add-int/2addr v0, v1

    .line 82
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 83
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturer:Ljava/lang/String;

    add-int/2addr v0, v1

    .line 85
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 86
    invoke-static {p1, v0, v1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturerURL:Ljava/lang/String;

    add-int/2addr v0, v1

    .line 88
    aget-byte v2, p1, v0

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p1, v3

    invoke-static {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v1

    add-int/lit8 v0, v0, 0x2

    .line 90
    new-array v2, v1, [B

    iput-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->Reserved:[B

    .line 91
    iget-object v2, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->Reserved:[B

    const/4 v3, 0x0

    invoke-static {p1, v0, v2, v3, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    add-int/lit8 v0, v0, 0x2

    .line 95
    .end local v0    # "nIdx":I
    .end local v1    # "nTmp":I
    :cond_0
    return-void
.end method
