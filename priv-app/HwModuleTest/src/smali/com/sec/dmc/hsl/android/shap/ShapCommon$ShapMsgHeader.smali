.class public abstract Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.super Ljava/lang/Object;
.source "ShapCommon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ShapMsgHeader"
.end annotation


# instance fields
.field public mInvokeID:B

.field public mMessageFlag:B

.field public mMessageID:B

.field public mMessageLength:J

.field public mMessageType:B


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "arrData"    # [B

    .prologue
    const/4 v2, 0x4

    .line 350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 351
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageLength:J

    .line 352
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lt v0, v2, :cond_0

    .line 354
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageType:B

    .line 355
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mInvokeID:B

    .line 356
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    .line 357
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageFlag:B

    .line 359
    iget-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageType:B

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    array-length v0, p1

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 361
    aget-byte v0, p1, v2

    const/4 v1, 0x5

    aget-byte v1, p1, v1

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint32(BBBB)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageLength:J

    .line 364
    :cond_0
    return-void
.end method


# virtual methods
.method public makeMessage()[B
    .locals 4

    .prologue
    .line 367
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 368
    .local v0, "arrMsg":[B
    const/4 v1, 0x0

    .line 369
    .local v1, "nIdx":I
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "nIdx":I
    .local v2, "nIdx":I
    iget-byte v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageType:B

    aput-byte v3, v0, v1

    .line 370
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "nIdx":I
    .restart local v1    # "nIdx":I
    iget-byte v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mInvokeID:B

    aput-byte v3, v0, v2

    .line 371
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "nIdx":I
    .restart local v2    # "nIdx":I
    iget-byte v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    aput-byte v3, v0, v1

    .line 372
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "nIdx":I
    .restart local v1    # "nIdx":I
    iget-byte v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageFlag:B

    aput-byte v3, v0, v2

    .line 374
    return-object v0
.end method
