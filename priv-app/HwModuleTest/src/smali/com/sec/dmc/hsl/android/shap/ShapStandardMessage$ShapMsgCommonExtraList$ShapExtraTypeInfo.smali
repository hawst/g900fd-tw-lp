.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;
.super Ljava/lang/Object;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtraTypeInfo"
.end annotation


# instance fields
.field public mDeviceType:S

.field public mExtraID:S

.field public mSupportFlag:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-short v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mExtraID:S

    .line 245
    iput-short v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mDeviceType:S

    .line 246
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mSupportFlag:B

    .line 247
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "arrData"    # [B

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    invoke-virtual {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->setData([B)V

    .line 251
    return-void
.end method


# virtual methods
.method public setData(SSB)V
    .locals 0
    .param p1, "sExtraID"    # S
    .param p2, "sDeviceType"    # S
    .param p3, "sSupportFlag"    # B

    .prologue
    .line 262
    iput-short p1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mExtraID:S

    .line 263
    iput-short p2, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mDeviceType:S

    .line 264
    iput-byte p3, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mSupportFlag:B

    .line 265
    return-void
.end method

.method public setData([B)V
    .locals 1
    .param p1, "arrData"    # [B

    .prologue
    .line 255
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    iput-short v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mExtraID:S

    .line 256
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    iput-short v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mDeviceType:S

    .line 257
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList$ShapExtraTypeInfo;->mSupportFlag:B

    .line 258
    return-void
.end method
