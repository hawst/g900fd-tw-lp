.class public Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;
.super Ljava/lang/Object;
.source "HealthCoverEcg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EcgFilterFor500Hz"
.end annotation


# static fields
.field private static final FILTER_LENGTH:I = 0x2bd


# instance fields
.field private mArrInput:[I

.field mFilterCoef:[F

.field private mIndex:I

.field final synthetic this$0:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;


# direct methods
.method public constructor <init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;)V
    .locals 2

    .prologue
    const/16 v1, 0x2bd

    .line 86
    iput-object p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->this$0:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mFilterCoef:[F

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    .line 270
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mArrInput:[I

    return-void

    .line 90
    :array_0
    .array-data 4
        -0x45a1f548
        -0x46032ccf
        -0x46bd9c5d
        -0x48902c75
        -0x49aecd33
        -0x46dcbccc
        -0x4616e0ca
        -0x45aa0040
        -0x45629445
        -0x4537fc12
        -0x451d506b
        -0x4517ad71
        -0x452833bb
        -0x454bca7d
        -0x457bafc1
        -0x45dd78fb    # -6.199929E-4f
        -0x466c7f91
        -0x4787c112
        0x362d8516
        -0x471a3acd
        -0x463ad6f5
        -0x45bbfe9c
        -0x45689fb1
        -0x4538a9e7
        -0x451739d0
        -0x450ab53c
        -0x45158e74
        -0x4535c5b8
        -0x4565432f
        -0x45b5f328
        -0x46326deb
        -0x47090be4
        -0x4a3319c4
        -0x4772ba5a
        -0x46604a46
        -0x45d06787
        -0x45707fb2
        -0x453b75dd
        -0x45132664
        -0x44ffa2a4
        -0x4503ad93
        -0x451f9de1
        -0x454dd4f0
        -0x458b14a9
        -0x45f82adb
        -0x46b7987c
        -0x4871eebe
        -0x47ff85bb
        -0x468d264a
        -0x45e70ca4
        -0x457a357f
        -0x45407a88
        -0x45114618
        -0x44facd5b
        -0x44f96850
        -0x45098992
        -0x45358905
        -0x456e7a14
        -0x45d2fe33
        -0x466d0098
        -0x47c2971f
        -0x4a7cee9a
        -0x46d9f69f
        -0x45ffb7d1
        -0x458b7dfc
        -0x4547d063
        -0x4511c86b
        -0x44f6f9f7
        -0x44f19ce9
        -0x44f9e179
        -0x451c871f
        -0x4555d737
        -0x45a99755
        -0x46354d6f
        -0x4747f629
        0x37a6fdaa
        -0x474af902
        -0x4634577a
        -0x45a62e67
        -0x45518e54
        -0x4514dce0
        -0x44f44876
        -0x44ea971e
        -0x44ef43cd
        -0x4502fa44
        -0x453bb2a0
        -0x457def6c
        -0x45fa0f1d
        -0x46e68ac5
        0x37fd9ad4
        -0x4870939e
        -0x466c492c
        -0x45c46dc0
        -0x455dca73
        -0x451ab3cd
        -0x44f2d9fc
        -0x44e47b0f
        -0x44e50c4d
        -0x44f48832
        -0x45201eee
        -0x4564dec9
        -0x45d46c1e
        -0x46968a89
        0x37ebbd91
        0x3847d659
        -0x46cd6337
        -0x45e62aaf
        -0x456c9b32
        -0x45237f9b
        -0x44f2d159
        -0x44df6ed5
        -0x44db5d4c
        -0x44e77cf1
        -0x45032f6d
        -0x45498cdd
        -0x45a96a1b
        -0x465906a3
        0x3740a3bb
        0x38de860b
        -0x47979f73
        -0x4616a694
        -0x457e1905
        -0x452f76a8
        -0x44f453ea
        -0x44db9b1b
        -0x44d25b24
        -0x44da748e
        -0x44f27b5e
        -0x452be96a
        -0x457c5546
        -0x461b78dc
        -0x4848b35d
        0x3923f2b3
        0x3881adbc
        -0x4667b066
        -0x45a4c18b
        -0x453ed607
        -0x44f78ae5
        -0x44d92c14
        -0x44ca2c99
        -0x44cd88eb
        -0x44e2c279
        -0x450bddf8
        -0x4560de9b
        -0x45e8b21c
        -0x4764040c
        0x394dce67
        0x393cdb19
        -0x46fec339    # -1.232504E-4f
        -0x45d32e25
        -0x4551e58f
        -0x44fca550
        -0x44d852fa
        -0x44c2fb92
        -0x44c0d45b
        -0x44d2728a
        -0x44f4a4db
        -0x45420b21
        -0x45bcab3c
        -0x46e3ceeb
        0x396a62cc
        0x399c4bf8
        0x386e12a3
        -0x460fb4e9
        -0x4568fdd7
        -0x4507b5f0
        -0x44d94863
        -0x44bcf63f
        -0x44b47155
        -0x44c18cc0
        -0x44e1fdbe
        -0x451f7fbe
        -0x4588c2bc
        -0x467d7647
        0x3976e73f
        0x39d956ce
        0x398304de
        -0x468d43ee
        -0x45892272
        -0x451ae1e5
        -0x44dc4fd7
        -0x44b85106
        -0x44a87a28
        -0x44b00b6a
        -0x44cdd46d
        -0x44fc607f
        -0x4565e88b
        -0x463977bc
        0x397016b5
        0x3a0a6f6a
        0x39ef9eab
        0x37cb4436
        -0x45ca734e
        -0x45338144
        -0x44e1bd83
        -0x44b54994
        -0x449d08af
        -0x449dde15
        -0x44b7f02a
        -0x44e690db
        -0x45422474
        -0x45f18773
        0x3951eff2
        0x3a271bd8
        0x3a323749
        0x39a43d5f
        -0x462f3a58
        -0x455285d5
        -0x44e9ff60
        -0x44b42bff
        -0x44923605
        -0x448ae384
        -0x449ff9bb
        -0x44cdd37e
        -0x4517feb7
        -0x45bb2c85
        0x39174b0b
        0x3a425d0f
        0x3a717d18
        0x3a286003
        -0x47b120f4
        -0x4579458c
        -0x44f5ac90
        -0x44b55b73
        -0x44881a3b
        -0x447b6fe2
        -0x44856925
        -0x44b16c96
        -0x44f2f04f
        -0x457b7317
        0x3864863d
        0x3a5be30d
        0x3a9b7561
        0x3a85ec0a
        0x39c10643
        -0x45d375ca
        -0x450b4042
        -0x44b96189
        -0x447f6607
        -0x4470b57c
        -0x4473b2d1
        -0x44903867
        -0x44d4ae56
        -0x455045ce
        -0x475a8407
        0x3a73627a
        0x3ac2571c
        0x3ac0738c
        0x3a65c28e
        -0x46b7d682
        -0x453654d8
        -0x44c10acf
        -0x447b3052
        -0x4464e8be
        -0x446244e0
        -0x44742e94
        -0x44af456c
        -0x4518be76
        -0x4671e858
        0x3a844ad1
        0x3aef45ca
        0x3b03b906
        0x3ac63e31
        0x39d871b7
        -0x4570e59a
        -0x44cd9fca
        -0x447775a4
        -0x44576a15
        -0x444d353b
        -0x445b5604
        -0x447fae48
        -0x44e79acb
        -0x45f0853d
        0x3a8d9eaf
        0x3b12d4c8
        0x3b30b958
        0x3b1978c4
        0x3aa02b47
        -0x460a5b38
        -0x44e16186
        -0x44743e9c
        -0x44470da3
        -0x44324495
        -0x443a9d91
        -0x445f5c4f
        -0x44b49087
        -0x45859c32
        0x3a9590f7    # 0.0011411001f
        0x3b361418
        0x3b6d971e
        0x3b64dfe0
        0x3b1b6f4b
        0x39e9bd4e
        -0x45017c18
        -0x447192b3
        -0x44316943
        -0x440cd120
        -0x440be42f
        -0x44302be3
        -0x44741628
        -0x452f03c3
        0x3a9c09d5
        0x3b68ae69
        0x3ba42fff
        0x3bac02e6
        0x3b888768
        0x3afaa16a
        -0x456bbf1f
        -0x446f7825
        -0x4410b636
        -0x43e8d520
        -0x43e01512
        -0x43f0fc9e
        -0x44334475
        -0x44cdba29
        0x3aa0f5d9
        0x3b9f0d98
        0x3bf39229
        0x3c099eba
        0x3bf488e2
        0x3b995488
        0x39f458f9
        -0x446df3e3
        -0x43e98df0
        -0x43aeb499
        -0x43935d08
        -0x439f7011
        -0x43d3ee99
        -0x445464ef
        0x3aa44631
        0x3c002266
        0x3c581afe
        0x3c8490e3
        0x3c829611
        0x3c468ed7
        0x3ba3d5b1    # 0.0049998392f
        -0x446d097e
        -0x438cdb91
        -0x4339cde5
        -0x4305a8fb
        -0x42fc5e76
        -0x431dd49e
        -0x4379cd1e
        0x3aa5f0e0
        0x3cc5e59e
        0x3d4e1b71
        0x3d9e1e01
        0x3dd0ec13
        0x3df9efea
        0x3e0a4984
        0x3e0ee726
        0x3e0a4984
        0x3df9efea
        0x3dd0ec13
        0x3d9e1e01
        0x3d4e1b71
        0x3cc5e59e
        0x3aa5f0e0
        -0x4379cd1e
        -0x431dd49e
        -0x42fc5e76
        -0x4305a8fb
        -0x4339cde5
        -0x438cdb91
        -0x446d097e
        0x3ba3d5b1    # 0.0049998392f
        0x3c468ed7
        0x3c829611
        0x3c8490e3
        0x3c581afe
        0x3c002266
        0x3aa44631
        -0x445464ef
        -0x43d3ee99
        -0x439f7011
        -0x43935d08
        -0x43aeb499
        -0x43e98df0
        -0x446df3e3
        0x39f458f9
        0x3b995488
        0x3bf488e2
        0x3c099eba
        0x3bf39229
        0x3b9f0d98
        0x3aa0f5d9
        -0x44cdba29
        -0x44334475
        -0x43f0fc9e
        -0x43e01512
        -0x43e8d520
        -0x4410b636
        -0x446f7825
        -0x456bbf1f
        0x3afaa16a
        0x3b888768
        0x3bac02e6
        0x3ba42fff
        0x3b68ae69
        0x3a9c09d5
        -0x452f03c3
        -0x44741628
        -0x44302be3
        -0x440be42f
        -0x440cd120
        -0x44316943
        -0x447192b3
        -0x45017c18
        0x39e9bd4e
        0x3b1b6f4b
        0x3b64dfe0
        0x3b6d971e
        0x3b361418
        0x3a9590f7    # 0.0011411001f
        -0x45859c32
        -0x44b49087
        -0x445f5c4f
        -0x443a9d91
        -0x44324495
        -0x44470da3
        -0x44743e9c
        -0x44e16186
        -0x460a5b38
        0x3aa02b47
        0x3b1978c4
        0x3b30b958
        0x3b12d4c8
        0x3a8d9eaf
        -0x45f0853d
        -0x44e79acb
        -0x447fae48
        -0x445b5604
        -0x444d353b
        -0x44576a15
        -0x447775a4
        -0x44cd9fca
        -0x4570e59a
        0x39d871b7
        0x3ac63e31
        0x3b03b906
        0x3aef45ca
        0x3a844ad1
        -0x4671e858
        -0x4518be76
        -0x44af456c
        -0x44742e94
        -0x446244e0
        -0x4464e8be
        -0x447b3052
        -0x44c10acf
        -0x453654d8
        -0x46b7d682
        0x3a65c28e
        0x3ac0738c
        0x3ac2571c
        0x3a73627a
        -0x475a8407
        -0x455045ce
        -0x44d4ae56
        -0x44903867
        -0x4473b2d1
        -0x4470b57c
        -0x447f6607
        -0x44b96189
        -0x450b4042
        -0x45d375ca
        0x39c10643
        0x3a85ec0a
        0x3a9b7561
        0x3a5be30d
        0x3864863d
        -0x457b7317
        -0x44f2f04f
        -0x44b16c96
        -0x44856925
        -0x447b6fe2
        -0x44881a3b
        -0x44b55b73
        -0x44f5ac90
        -0x4579458c
        -0x47b120f4
        0x3a286003
        0x3a717d18
        0x3a425d0f
        0x39174b0b
        -0x45bb2c85
        -0x4517feb7
        -0x44cdd37e
        -0x449ff9bb
        -0x448ae384
        -0x44923605
        -0x44b42bff
        -0x44e9ff60
        -0x455285d5
        -0x462f3a58
        0x39a43d5f
        0x3a323749
        0x3a271bd8
        0x3951eff2
        -0x45f18773
        -0x45422474
        -0x44e690db
        -0x44b7f02a
        -0x449dde15
        -0x449d08af
        -0x44b54994
        -0x44e1bd83
        -0x45338144
        -0x45ca734e
        0x37cb4436
        0x39ef9eab
        0x3a0a6f6a
        0x397016b5
        -0x463977bc
        -0x4565e88b
        -0x44fc607f
        -0x44cdd46d
        -0x44b00b6a
        -0x44a87a28
        -0x44b85106
        -0x44dc4fd7
        -0x451ae1e5
        -0x45892272
        -0x468d43ee
        0x398304de
        0x39d956ce
        0x3976e73f
        -0x467d7647
        -0x4588c2bc
        -0x451f7fbe
        -0x44e1fdbe
        -0x44c18cc0
        -0x44b47155
        -0x44bcf63f
        -0x44d94863
        -0x4507b5f0
        -0x4568fdd7
        -0x460fb4e9
        0x386e12a3
        0x399c4bf8
        0x396a62cc
        -0x46e3ceeb
        -0x45bcab3c
        -0x45420b21
        -0x44f4a4db
        -0x44d2728a
        -0x44c0d45b
        -0x44c2fb92
        -0x44d852fa
        -0x44fca550
        -0x4551e58f
        -0x45d32e25
        -0x46fec339    # -1.232504E-4f
        0x393cdb19
        0x394dce67
        -0x4764040c
        -0x45e8b21c
        -0x4560de9b
        -0x450bddf8
        -0x44e2c279
        -0x44cd88eb
        -0x44ca2c99
        -0x44d92c14
        -0x44f78ae5
        -0x453ed607
        -0x45a4c18b
        -0x4667b066
        0x3881adbc
        0x3923f2b3
        -0x4848b35d
        -0x461b78dc
        -0x457c5546
        -0x452be96a
        -0x44f27b5e
        -0x44da748e
        -0x44d25b24
        -0x44db9b1b
        -0x44f453ea
        -0x452f76a8
        -0x457e1905
        -0x4616a694
        -0x47979f73
        0x38de860b
        0x3740a3bb
        -0x465906a3
        -0x45a96a1b
        -0x45498cdd
        -0x45032f6d
        -0x44e77cf1
        -0x44db5d4c
        -0x44df6ed5
        -0x44f2d159
        -0x45237f9b
        -0x456c9b32
        -0x45e62aaf
        -0x46cd6337
        0x3847d659
        0x37ebbd91
        -0x46968a89
        -0x45d46c1e
        -0x4564dec9
        -0x45201eee
        -0x44f48832
        -0x44e50c4d
        -0x44e47b0f
        -0x44f2d9fc
        -0x451ab3cd
        -0x455dca73
        -0x45c46dc0
        -0x466c492c
        -0x4870939e
        0x37fd9ad4
        -0x46e68ac5
        -0x45fa0f1d
        -0x457def6c
        -0x453bb2a0
        -0x4502fa44
        -0x44ef43cd
        -0x44ea971e
        -0x44f44876
        -0x4514dce0
        -0x45518e54
        -0x45a62e67
        -0x4634577a
        -0x474af902
        0x37a6fdaa
        -0x4747f629
        -0x46354d6f
        -0x45a99755
        -0x4555d737
        -0x451c871f
        -0x44f9e179
        -0x44f19ce9
        -0x44f6f9f7
        -0x4511c86b
        -0x4547d063
        -0x458b7dfc
        -0x45ffb7d1
        -0x46d9f69f
        -0x4a7cee9a
        -0x47c2971f
        -0x466d0098
        -0x45d2fe33
        -0x456e7a14
        -0x45358905
        -0x45098992
        -0x44f96850
        -0x44facd5b
        -0x45114618
        -0x45407a88
        -0x457a357f
        -0x45e70ca4
        -0x468d264a
        -0x47ff85bb
        -0x4871eebe
        -0x46b7987c
        -0x45f82adb
        -0x458b14a9
        -0x454dd4f0
        -0x451f9de1
        -0x4503ad93
        -0x44ffa2a4
        -0x45132664
        -0x453b75dd
        -0x45707fb2
        -0x45d06787
        -0x46604a46
        -0x4772ba5a
        -0x4a3319c4
        -0x47090be4
        -0x46326deb
        -0x45b5f328
        -0x4565432f
        -0x4535c5b8
        -0x45158e74
        -0x450ab53c
        -0x451739d0
        -0x4538a9e7
        -0x45689fb1
        -0x45bbfe9c
        -0x463ad6f5
        -0x471a3acd
        0x362d8516
        -0x4787c112
        -0x466c7f91
        -0x45dd78fb    # -6.199929E-4f
        -0x457bafc1
        -0x454bca7d
        -0x452833bb
        -0x4517ad71
        -0x451d506b
        -0x4537fc12
        -0x45629445
        -0x45aa0040
        -0x4616e0ca
        -0x46dcbccc
        -0x49aecd33
        -0x48902c75
        -0x46bd9c5d
        -0x46032ccf
        -0x45a1f548
    .end array-data
.end method


# virtual methods
.method public filterEcgData(I)F
    .locals 7
    .param p1, "nInput"    # I

    .prologue
    const/16 v6, 0x2bd

    .line 283
    const/4 v0, 0x0

    .line 284
    .local v0, "fRtn":F
    const/4 v2, 0x0

    .line 286
    .local v2, "nIdx":I
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mArrInput:[I

    iget v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    aput p1, v3, v4

    .line 287
    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    if-lt v3, v6, :cond_0

    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    .line 289
    :cond_0
    const/4 v0, 0x0

    .line 290
    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    .line 291
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_2

    .line 293
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mArrInput:[I

    aget v3, v3, v2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mFilterCoef:[F

    aget v4, v4, v1

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    .line 294
    add-int/lit8 v2, v2, 0x1

    .line 295
    if-lt v2, v6, :cond_1

    const/4 v2, 0x0

    .line 291
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_2
    return v0
.end method

.method public initEcgFilter()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 274
    iput v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mIndex:I

    .line 275
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x2bd

    if-ge v0, v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$EcgFilterFor500Hz;->mArrInput:[I

    aput v2, v1, v0

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 279
    :cond_0
    return-void
.end method
