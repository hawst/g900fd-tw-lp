.class public Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon;
.source "ShapHeartRateMonitorMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;,
        Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    }
.end annotation


# static fields
.field public static final SHAP_MSG_HRM_HEART_RATE:B = 0x50t


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    .line 49
    return-void
.end method

.method public static parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    .locals 2
    .param p0, "arrMessage"    # [B

    .prologue
    .line 8
    invoke-static {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->parseCommonMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    .line 9
    .local v0, "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    if-nez v0, :cond_0

    .line 11
    const/4 v1, 0x2

    aget-byte v1, p0, v1

    sparse-switch v1, :sswitch_data_0

    .line 22
    :cond_0
    :goto_0
    return-object v0

    .line 14
    :sswitch_0
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;

    .end local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;-><init>([B)V

    .line 15
    .restart local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 17
    :sswitch_1
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;

    .end local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;-><init>([B)V

    .restart local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 11
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x50 -> :sswitch_0
    .end sparse-switch
.end method
