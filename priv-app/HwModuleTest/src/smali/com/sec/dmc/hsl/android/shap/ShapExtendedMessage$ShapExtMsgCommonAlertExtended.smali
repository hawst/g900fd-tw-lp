.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgCommonAlertExtended"
.end annotation


# instance fields
.field public mAlertID:I

.field public mAlertLevel:S

.field public mAlertMessage:Ljava/lang/String;

.field public mReserved:[B


# direct methods
.method public constructor <init>([B)V
    .locals 10
    .param p1, "arrData"    # [B

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 23
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mReserved:[B

    .line 28
    if-eqz p1, :cond_0

    array-length v3, p1

    int-to-long v4, v3

    iget-wide v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mMessageLength:J

    const-wide/16 v8, 0x8

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 31
    const/16 v0, 0x8

    .line 32
    .local v0, "nIdx":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v3, p1, v0

    int-to-short v3, v3

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    iput-short v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mAlertLevel:S

    .line 33
    aget-byte v3, p1, v1

    const/16 v4, 0xa

    aget-byte v4, p1, v4

    invoke-static {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v3

    iput v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mAlertID:I

    add-int/lit8 v0, v1, 0x2

    .line 34
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    aget-byte v3, p1, v0

    const/16 v4, 0xc

    aget-byte v4, p1, v4

    invoke-static {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    .local v2, "nTmp":I
    add-int/lit8 v0, v0, 0x2

    .line 35
    invoke-static {p1, v0, v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getStringFromByteArray([BII)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mAlertMessage:Ljava/lang/String;

    add-int/lit8 v0, v2, 0xd

    .line 36
    aget-byte v3, p1, v0

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, p1, v4

    invoke-static {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v2

    add-int/lit8 v0, v0, 0x2

    .line 37
    if-lez v2, :cond_0

    .line 39
    new-array v3, v2, [B

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mReserved:[B

    .line 40
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mReserved:[B

    const/4 v4, 0x0

    invoke-static {p1, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 43
    .end local v0    # "nIdx":I
    .end local v2    # "nTmp":I
    :cond_0
    return-void
.end method
