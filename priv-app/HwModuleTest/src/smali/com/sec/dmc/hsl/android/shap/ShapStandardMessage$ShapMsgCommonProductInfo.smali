.class public Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapStandardMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgCommonProductInfo"
.end annotation


# instance fields
.field public mManufacturerID:I

.field public mPostamble:[B

.field public mPreamble:[B

.field public mProductType:S

.field public mProtocolVersion:[B

.field public mReserved:B


# direct methods
.method public constructor <init>([B)V
    .locals 7
    .param p1, "arrData"    # [B

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 73
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 64
    new-array v1, v4, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mPreamble:[B

    .line 65
    new-array v1, v3, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mProtocolVersion:[B

    .line 69
    new-array v1, v6, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mPostamble:[B

    .line 75
    if-eqz p1, :cond_0

    array-length v1, p1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 77
    const/4 v0, 0x4

    .line 78
    .local v0, "nIdx":I
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mPreamble:[B

    invoke-static {p1, v0, v1, v5, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 79
    add-int/lit8 v0, v0, 0x4

    .line 81
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mProtocolVersion:[B

    invoke-static {p1, v0, v1, v5, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 82
    add-int/lit8 v0, v0, 0x3

    .line 84
    aget-byte v1, p1, v0

    const/16 v2, 0xc

    aget-byte v2, p1, v2

    invoke-static {v1, v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mProductType:S

    .line 85
    add-int/lit8 v0, v0, 0x2

    .line 87
    aget-byte v1, p1, v0

    const/16 v2, 0xe

    aget-byte v2, p1, v2

    const/16 v3, 0xf

    aget-byte v3, p1, v3

    const/16 v4, 0x10

    aget-byte v4, p1, v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt32(BBBB)I

    move-result v1

    iput v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mManufacturerID:I

    .line 88
    add-int/lit8 v0, v0, 0x4

    .line 90
    aget-byte v1, p1, v0

    iput-byte v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mReserved:B

    .line 91
    add-int/lit8 v0, v0, 0x1

    .line 93
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;->mPostamble:[B

    invoke-static {p1, v0, v1, v5, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    add-int/lit8 v0, v0, 0x2

    .line 96
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method
