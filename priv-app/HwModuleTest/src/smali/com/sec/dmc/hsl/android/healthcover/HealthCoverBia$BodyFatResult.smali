.class public final enum Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
.super Ljava/lang/Enum;
.source "HealthCoverBia.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BodyFatResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field public static final enum HEALTHY:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field public static final enum OBESE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field public static final enum OVERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field public static final enum RE_MEASUREMENT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

.field public static final enum UNDERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;


# instance fields
.field private mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    const-string v1, "RE_MEASUREMENT"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->RE_MEASUREMENT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    const-string v1, "UNDERFAT"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->UNDERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    const-string v1, "HEALTHY"

    invoke-direct {v0, v1, v5, v4}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->HEALTHY:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    const-string v1, "OVERFAT"

    invoke-direct {v0, v1, v6, v5}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->OVERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    const-string v1, "OBESE"

    invoke-direct {v0, v1, v7, v6}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->OBESE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    .line 13
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->RE_MEASUREMENT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->UNDERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->HEALTHY:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->OVERFAT:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->OBESE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput p3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->mValue:I

    .line 19
    return-void
.end method

.method static synthetic access$002(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->mValue:I

    return p1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    return-object v0
.end method

.method public static values()[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    invoke-virtual {v0}, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;

    return-object v0
.end method


# virtual methods
.method public getResultString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22
    const/4 v0, 0x0

    .line 23
    .local v0, "strRtn":Ljava/lang/String;
    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$BodyFatResult;->mValue:I

    packed-switch v1, :pswitch_data_0

    .line 41
    const-string v0, "Not measured"

    .line 43
    :goto_0
    return-object v0

    .line 26
    :pswitch_0
    const-string v0, "Remeasurement required"

    .line 27
    goto :goto_0

    .line 29
    :pswitch_1
    const-string v0, "Underfat"

    .line 30
    goto :goto_0

    .line 32
    :pswitch_2
    const-string v0, "Healthy"

    .line 33
    goto :goto_0

    .line 35
    :pswitch_3
    const-string v0, "Overfat"

    .line 36
    goto :goto_0

    .line 38
    :pswitch_4
    const-string v0, "Obese"

    .line 39
    goto :goto_0

    .line 23
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
