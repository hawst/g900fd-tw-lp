.class public Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;
.super Ljava/lang/Object;
.source "HealthCoverEcg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FIR_LPF_500Hz"
.end annotation


# static fields
.field private static final FILTER_LENGTH:I = 0x118

.field public static final FS:I = 0x1f4

.field public static final Fpass:I = 0x28

.field public static final Fstop:I = 0x31


# instance fields
.field private mArrInput:[F

.field mFilterCoef:[F

.field private mIndex:I

.field final synthetic this$0:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;


# direct methods
.method public constructor <init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;)V
    .locals 2

    .prologue
    const/16 v1, 0x118

    .line 369
    iput-object p1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->this$0:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 307
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mFilterCoef:[F

    .line 365
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    .line 366
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mArrInput:[F

    .line 370
    invoke-virtual {p0}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->initFilter()V

    .line 371
    return-void

    .line 307
    nop

    :array_0
    .array-data 4
        0x364f376a
        0x36e15123
        0x371a3155
        0x3714ec0c
        0x36a0317b
        -0x49c2b3eb
        -0x48b613f7
        -0x485a84b8
        -0x484413f1
        -0x486cf5a4
        -0x495eed74
        0x375cc92b
        0x380250a5
        0x38319d3e
        0x382d239a
        0x37d4b6c1
        -0x49b0ad71
        -0x47e6180e
        -0x47785f87
        -0x476095ac
        -0x477b0a78
        -0x48218e46
        0x37de2465
        0x38a86365
        0x38f4699f
        0x38faf91d
        0x38adbb88
        0x3745ee4e
        -0x4762aaf3
        -0x46e1e871
        -0x46bacd70
        -0x46cf8990
        -0x4744b760
        0x38036aee
        0x392723dc
        0x3984bac6
        0x3990ec66
        0x395dc129
        0x388dfc5d
        -0x4708a338
        -0x46682bea
        -0x46344719
        -0x463c9529
        -0x468c23a1
        0x35c50c5b
        0x398a72a6
        0x39f7b50b
        0x3a10a007    # 5.51701E-4f
        0x39f15e3b
        0x395a62d6
        -0x46ec4821
        -0x46022a53
        -0x45c62c57
        -0x45c106f7
        -0x45f944cc
        -0x471a781f
        0x39c41cae
        0x3a4e8e94
        0x3a81d6a8
        0x3a6a6999
        0x3a02daf4
        -0x47343eed
        -0x45c2ab89
        -0x4565bdb0
        -0x4555f157
        -0x457b5547
        -0x463c1133
        0x39e9e4a6
        0x3a9d5abc
        0x3ad704d6
        0x3ad10e3f
        0x3a8779dc    # 0.0010336f
        0x38bb32fe
        -0x4581cb6f
        -0x4512434f
        -0x44f2e9c6
        -0x450fbfaa
        -0x45948062
        0x39d4bc14
        0x3ade0c4b
        0x3b274e2b
        0x3b2efe23
        0x3afee466
        0x3a0c8d8f
        -0x4567831c    # -0.00116339f
        -0x44d3441b
        -0x44a1be52
        -0x44b2f489
        -0x450e05a7
        0x390aff3e
        0x3b126ddf
        0x3b78fb07
        0x3b8c5b00
        0x3b619b91    # 0.0034425f
        0x3abc0156
        -0x4562eb3a
        -0x448f8987
        -0x44565a5f
        -0x4456dd94
        -0x449aad5a
        -0x45e7d03d
        0x3b35a05c
        0x3bb514f3
        0x3bdd5bc0
        0x3bc27ecb
        0x3b4e6377
        -0x4596e570
        -0x445bffb7
        -0x43feab60
        -0x43f45aa0
        -0x442b879f
        -0x44f17339
        0x3b54c75c
        0x3c05b51b
        0x3c3362c3
        0x3c2c1ab0
        0x3bd9530b
        0x3953aac9
        -0x441a2d95
        -0x43ae599a
        -0x43882d15
        -0x43adb748
        -0x44393011
        0x3b6c1d71
        0x3c5cd3cb
        0x3ca74244
        0x3cb203f4
        0x3c838987
        0x3b822452
        -0x43bd6861
        -0x43214bb0
        -0x42e8d15d
        -0x42ea9809
        -0x4343917a
        0x3b789ed2
        0x3d28ea1a
        0x3dac1f5e
        0x3e0048fb
        0x3e2190f3
        0x3e33e525
        0x3e33e525
        0x3e2190f3
        0x3e0048fb
        0x3dac1f5e
        0x3d28ea1a
        0x3b789ed2
        -0x4343917a
        -0x42ea9809
        -0x42e8d15d
        -0x43214bb0
        -0x43bd6861
        0x3b822452
        0x3c838987
        0x3cb203f4
        0x3ca74244
        0x3c5cd3cb
        0x3b6c1d71
        -0x44393011
        -0x43adb748
        -0x43882d15
        -0x43ae599a
        -0x441a2d95
        0x3953aac9
        0x3bd9530b
        0x3c2c1ab0
        0x3c3362c3
        0x3c05b51b
        0x3b54c75c
        -0x44f17339
        -0x442b879f
        -0x43f45aa0
        -0x43feab60
        -0x445bffb7
        -0x4596e570
        0x3b4e6377
        0x3bc27ecb
        0x3bdd5bc0
        0x3bb514f3
        0x3b35a05c
        -0x45e7d03d
        -0x449aad5a
        -0x4456dd94
        -0x44565a5f
        -0x448f8987
        -0x4562eb3a
        0x3abc0156
        0x3b619b91    # 0.0034425f
        0x3b8c5b00
        0x3b78fb07
        0x3b126ddf
        0x390aff3e
        -0x450e05a7
        -0x44b2f489
        -0x44a1be52
        -0x44d3441b
        -0x4567831c    # -0.00116339f
        0x3a0c8d8f
        0x3afee466
        0x3b2efe23
        0x3b274e2b
        0x3ade0c4b
        0x39d4bc14
        -0x45948062
        -0x450fbfaa
        -0x44f2e9c6
        -0x4512434f
        -0x4581cb6f
        0x38bb32fe
        0x3a8779dc    # 0.0010336f
        0x3ad10e3f
        0x3ad704d6
        0x3a9d5abc
        0x39e9e4a6
        -0x463c1133
        -0x457b5547
        -0x4555f157
        -0x4565bdb0
        -0x45c2ab89
        -0x47343eed
        0x3a02daf4
        0x3a6a6999
        0x3a81d6a8
        0x3a4e8e94
        0x39c41cae
        -0x471a781f
        -0x45f944cc
        -0x45c106f7
        -0x45c62c57
        -0x46022a53
        -0x46ec4821
        0x395a62d6
        0x39f15e3b
        0x3a10a007    # 5.51701E-4f
        0x39f7b50b
        0x398a72a6
        0x35c50c5b
        -0x468c23a1
        -0x463c9529
        -0x46344719
        -0x46682bea
        -0x4708a338
        0x388dfc5d
        0x395dc129
        0x3990ec66
        0x3984bac6
        0x392723dc
        0x38036aee
        -0x4744b760
        -0x46cf8990
        -0x46bacd70
        -0x46e1e871
        -0x4762aaf3
        0x3745ee4e
        0x38adbb88
        0x38faf91d
        0x38f4699f
        0x38a86365
        0x37de2465
        -0x48218e46
        -0x477b0a78
        -0x476095ac
        -0x47785f87
        -0x47e6180e
        -0x49b0ad71
        0x37d4b6c1
        0x382d239a
        0x38319d3e
        0x380250a5
        0x375cc92b
        -0x495eed74
        -0x486cf5a4
        -0x484413f1
        -0x485a84b8
        -0x48b613f7
        -0x49c2b3eb
        0x36a0317b
        0x3714ec0c
        0x371a3155
        0x36e15123
        0x364f376a
    .end array-data
.end method


# virtual methods
.method public filter(F)F
    .locals 7
    .param p1, "fInput"    # F

    .prologue
    const/16 v6, 0x118

    .line 383
    const/4 v0, 0x0

    .line 384
    .local v0, "fRtn":F
    const/4 v2, 0x0

    .line 386
    .local v2, "nIdx":I
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mArrInput:[F

    iget v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    aput p1, v3, v4

    .line 387
    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    if-lt v3, v6, :cond_0

    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    .line 389
    :cond_0
    const/4 v0, 0x0

    .line 390
    iget v2, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    .line 391
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_2

    .line 393
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mArrInput:[F

    aget v3, v3, v2

    iget-object v4, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mFilterCoef:[F

    aget v4, v4, v1

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    .line 394
    add-int/lit8 v2, v2, 0x1

    .line 395
    if-lt v2, v6, :cond_1

    const/4 v2, 0x0

    .line 391
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 397
    :cond_2
    return v0
.end method

.method public initFilter()V
    .locals 3

    .prologue
    .line 374
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mIndex:I

    .line 375
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x118

    if-ge v0, v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->mArrInput:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 375
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 379
    :cond_0
    return-void
.end method
