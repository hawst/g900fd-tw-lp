.class public Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapHeartRateMonitorMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapMsgHrmSettings"
.end annotation


# static fields
.field private static final RESERVED_LENGTH:I = 0xc


# instance fields
.field public mCommonSetting:S

.field public mRecordInterval:I

.field public mReserved:[B

.field public mTickInterval:S


# direct methods
.method public constructor <init>([B)V
    .locals 6
    .param p1, "arrData"    # [B

    .prologue
    const/16 v5, 0xc

    .line 60
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 55
    new-array v1, v5, [B

    iput-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mReserved:[B

    .line 61
    if-eqz p1, :cond_0

    array-length v1, p1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 63
    const/4 v0, 0x4

    .line 64
    .local v0, "nIdx":I
    aget-byte v1, p1, v0

    int-to-short v1, v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mCommonSetting:S

    add-int/lit8 v0, v0, 0x1

    .line 65
    aget-byte v1, p1, v0

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    invoke-static {v1, v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt16(BB)S

    move-result v1

    iput-short v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mTickInterval:S

    add-int/lit8 v0, v0, 0x2

    .line 66
    aget-byte v1, p1, v0

    const/16 v2, 0x8

    aget-byte v2, p1, v2

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    const/16 v4, 0xa

    aget-byte v4, p1, v4

    invoke-static {v1, v2, v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeInt32(BBBB)I

    move-result v1

    iput v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mRecordInterval:I

    add-int/lit8 v0, v0, 0x4

    .line 67
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mReserved:[B

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 70
    .end local v0    # "nIdx":I
    :cond_0
    return-void
.end method


# virtual methods
.method public makeMessage()[B
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 74
    const/4 v2, 0x0

    .line 75
    .local v2, "nIdx":I
    const/16 v4, 0x14

    new-array v0, v4, [B

    .line 76
    .local v0, "arrMsg":[B
    invoke-super {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->makeMessage()[B

    move-result-object v1

    .line 77
    .local v1, "arrTmp":[B
    const/4 v4, 0x4

    invoke-static {v1, v6, v0, v6, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    add-int/lit8 v2, v2, 0x4

    .line 79
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "nIdx":I
    .local v3, "nIdx":I
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mCommonSetting:S

    int-to-byte v4, v4

    aput-byte v4, v0, v2

    .line 80
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "nIdx":I
    .restart local v2    # "nIdx":I
    iget-short v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mTickInterval:S

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 81
    iget v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mRecordInterval:I

    const v5, 0xffff

    and-int/2addr v4, v5

    invoke-static {v0, v2, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->fill16bitData([BII)V

    add-int/lit8 v2, v2, 0x2

    .line 82
    iget-object v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmSettings;->mReserved:[B

    const/16 v5, 0xc

    invoke-static {v4, v6, v0, v2, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    add-int/lit8 v2, v2, 0xc

    .line 83
    return-object v0
.end method
