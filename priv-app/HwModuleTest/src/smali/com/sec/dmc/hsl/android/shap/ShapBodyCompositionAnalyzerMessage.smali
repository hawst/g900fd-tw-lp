.class public Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon;
.source "ShapBodyCompositionAnalyzerMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;,
        Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;
    }
.end annotation


# static fields
.field public static final SHAP_MSG_BCA_BODYFAT:B = 0x50t

.field public static final SHAP_MSG_BCA_INTERMEDIATE_DATA:B = -0x70t

.field public static final SHAP_MSG_BCA_SETTINGS:B = 0x3t


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    .line 51
    return-void
.end method

.method public static parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    .locals 2
    .param p0, "arrMessage"    # [B

    .prologue
    .line 12
    invoke-static {p0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->parseCommonMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    .line 13
    .local v0, "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    if-nez v0, :cond_0

    .line 15
    const/4 v1, 0x2

    aget-byte v1, p0, v1

    sparse-switch v1, :sswitch_data_0

    .line 28
    :cond_0
    :goto_0
    return-object v0

    .line 18
    :sswitch_0
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;

    .end local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaIntermediateData;-><init>([B)V

    .line 19
    .restart local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 21
    :sswitch_1
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;

    .end local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    invoke-direct {v0, p0}, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage$ShapMsgBcaBodyfat;-><init>([B)V

    .line 22
    .restart local v0    # "shapMsg":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    goto :goto_0

    .line 15
    nop

    :sswitch_data_0
    .sparse-switch
        -0x70 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method
