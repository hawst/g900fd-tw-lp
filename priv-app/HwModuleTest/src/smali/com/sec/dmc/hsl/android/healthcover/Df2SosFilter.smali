.class public Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;
.super Ljava/lang/Object;
.source "Df2SosFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;
    }
.end annotation


# instance fields
.field mInitialized:Z

.field mNumberOfSection:I

.field mScaleValues:[D

.field mSosMatrix:[[D

.field mZMatrix:[[D


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-boolean v2, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mInitialized:Z

    .line 15
    iput v2, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    move-object v0, v1

    .line 16
    check-cast v0, [[D

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    move-object v0, v1

    .line 17
    check-cast v0, [[D

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    .line 18
    iput-object v1, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    .line 19
    iput-boolean v2, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mInitialized:Z

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;)V
    .locals 3
    .param p1, "filter"    # Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mInitialized:Z

    .line 12
    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->getFilterSection()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->getSosMatrix()[[D

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->getScaleValueMatrix()[D

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->initializeFilter(I[[D[D)Z

    .line 13
    return-void
.end method

.method private checkFilterMatrix()Z
    .locals 5

    .prologue
    .line 32
    const/4 v0, 0x0

    .line 35
    .local v0, "bOK":Z
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    const/4 v4, 0x0

    aget-object v3, v3, v4

    array-length v1, v3

    .line 36
    .local v1, "nSize_0":I
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    array-length v2, v3

    .line 37
    .local v2, "nSize_1":I
    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    if-ne v3, v1, :cond_1

    iget v3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    mul-int/lit8 v3, v3, 0x6

    if-ne v3, v2, :cond_1

    .line 38
    const/4 v0, 0x1

    .line 43
    :goto_0
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    if-eq v3, v4, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 47
    :cond_0
    iput-boolean v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mInitialized:Z

    .line 48
    return v0

    .line 40
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public filter(D)D
    .locals 13
    .param p1, "dbInput"    # D

    .prologue
    .line 51
    const-wide/16 v0, 0x0

    .line 52
    .local v0, "dbOutput":D
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    if-ge v4, v5, :cond_0

    .line 53
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    aget-wide v6, v5, v4

    mul-double/2addr v6, p1

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x0

    aget-wide v8, v5, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v5, v5, v4

    const/4 v10, 0x4

    aget-wide v10, v5, v10

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x1

    aget-wide v8, v5, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v5, v5, v4

    const/4 v10, 0x5

    aget-wide v10, v5, v10

    mul-double/2addr v8, v10

    sub-double v2, v6, v8

    .line 54
    .local v2, "dbTmp":D
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v5, v5, v4

    const/4 v6, 0x0

    aget-wide v6, v5, v6

    mul-double/2addr v6, v2

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x0

    aget-wide v8, v5, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v5, v5, v4

    const/4 v10, 0x1

    aget-wide v10, v5, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x1

    aget-wide v8, v5, v8

    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v5, v5, v4

    const/4 v10, 0x2

    aget-wide v10, v5, v10

    mul-double/2addr v8, v10

    add-double v0, v6, v8

    .line 55
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v7, v7, v4

    const/4 v8, 0x0

    aget-wide v8, v7, v8

    aput-wide v8, v5, v6

    .line 56
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v5, v5, v4

    const/4 v6, 0x0

    aput-wide v2, v5, v6

    .line 57
    move-wide p1, v0

    .line 52
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 59
    .end local v2    # "dbTmp":D
    :cond_0
    iget-object v5, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    iget v6, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    aget-wide v6, v5, v6

    mul-double/2addr v6, v0

    return-wide v6
.end method

.method public filter([D[D)V
    .locals 18
    .param p1, "arrInput"    # [D
    .param p2, "arrOutput"    # [D

    .prologue
    .line 92
    move-object/from16 v0, p1

    array-length v10, v0

    .line 94
    .local v10, "nArrSize":I
    const-wide/16 v4, 0x0

    .line 95
    .local v4, "dbOutput":D
    const-wide/16 v2, 0x0

    .line 96
    .local v2, "dbInput":D
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_0
    if-ge v9, v10, :cond_1

    .line 98
    aget-wide v2, p1, v9

    .line 99
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    if-ge v8, v11, :cond_0

    .line 100
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    aget-wide v12, v11, v8

    mul-double/2addr v12, v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x0

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x4

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x1

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x5

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    sub-double v6, v12, v14

    .line 101
    .local v6, "dbTmp":D
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x0

    aget-wide v12, v11, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x0

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x1

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x1

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x2

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    add-double v4, v12, v14

    .line 102
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v13, v13, v8

    const/4 v14, 0x0

    aget-wide v14, v13, v14

    aput-wide v14, v11, v12

    .line 103
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x0

    aput-wide v6, v11, v12

    .line 104
    move-wide v2, v4

    .line 99
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 106
    .end local v6    # "dbTmp":D
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    aget-wide v12, v11, v12

    mul-double/2addr v12, v4

    aput-wide v12, p2, v9

    .line 96
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 108
    .end local v8    # "i":I
    :cond_1
    return-void
.end method

.method public filter([S[DI)V
    .locals 18
    .param p1, "arrInput"    # [S
    .param p2, "arrOutput"    # [D
    .param p3, "nSize"    # I

    .prologue
    .line 73
    move-object/from16 v0, p1

    array-length v10, v0

    .line 75
    .local v10, "nArrSize":I
    const-wide/16 v4, 0x0

    .line 76
    .local v4, "dbOutput":D
    const-wide/16 v2, 0x0

    .line 77
    .local v2, "dbInput":D
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_0
    move/from16 v0, p3

    if-ge v9, v0, :cond_1

    .line 79
    aget-short v11, p1, v9

    int-to-double v2, v11

    .line 80
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    if-ge v8, v11, :cond_0

    .line 81
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    aget-wide v12, v11, v8

    mul-double/2addr v12, v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x0

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x4

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x1

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x5

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    sub-double v6, v12, v14

    .line 82
    .local v6, "dbTmp":D
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x0

    aget-wide v12, v11, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x0

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x1

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v14, 0x1

    aget-wide v14, v11, v14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    aget-object v11, v11, v8

    const/16 v16, 0x2

    aget-wide v16, v11, v16

    mul-double v14, v14, v16

    add-double v4, v12, v14

    .line 83
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v13, v13, v8

    const/4 v14, 0x0

    aget-wide v14, v13, v14

    aput-wide v14, v11, v12

    .line 84
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    aget-object v11, v11, v8

    const/4 v12, 0x0

    aput-wide v6, v11, v12

    .line 85
    move-wide v2, v4

    .line 80
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 87
    .end local v6    # "dbTmp":D
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    aget-wide v12, v11, v12

    mul-double/2addr v12, v4

    aput-wide v12, p2, v9

    .line 77
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 89
    .end local v8    # "i":I
    :cond_1
    return-void
.end method

.method public filter([D)[D
    .locals 6
    .param p1, "arrInput"    # [D

    .prologue
    .line 63
    array-length v2, p1

    .line 64
    .local v2, "nArrSize":I
    new-array v0, v2, [D

    .line 66
    .local v0, "arrOutput":[D
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 67
    aget-wide v4, p1, v1

    invoke-virtual {p0, v4, v5}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->filter(D)D

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-object v0
.end method

.method public initializeFilter(I[[D[D)Z
    .locals 2
    .param p1, "nNumOfSection"    # I
    .param p2, "sosMatrix"    # [[D
    .param p3, "scaleValues"    # [D

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    .line 25
    iput-object p2, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mSosMatrix:[[D

    .line 26
    iget v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mNumberOfSection:I

    const/4 v1, 0x2

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mZMatrix:[[D

    .line 27
    iput-object p3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->mScaleValues:[D

    .line 28
    invoke-direct {p0}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->checkFilterMatrix()Z

    move-result v0

    return v0
.end method
