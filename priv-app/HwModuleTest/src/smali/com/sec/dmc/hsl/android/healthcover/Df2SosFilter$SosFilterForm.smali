.class public final enum Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;
.super Ljava/lang/Enum;
.source "Df2SosFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SosFilterForm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field public static final enum FILTER_BPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field public static final enum FILTER_BPF_CHEBY1:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field public static final enum FILTER_ECG:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field public static final enum FILTER_HPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field public static final enum FILTER_LPF_CHEBY2:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

.field static final SCALE_VALUES:[[D

.field static final SOS_FILTER_SECTION:[I

.field static final SOS_MATRIX:[[[D


# instance fields
.field private mFilterIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x6

    .line 116
    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    const-string v1, "FILTER_ECG"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_ECG:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    const-string v1, "FILTER_BPF_BUTTER"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_BPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    const-string v1, "FILTER_BPF_CHEBY1"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_BPF_CHEBY1:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    const-string v1, "FILTER_HPF_BUTTER"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_HPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    new-instance v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    const-string v1, "FILTER_LPF_CHEBY2"

    const/4 v2, 0x4

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_LPF_CHEBY2:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    .line 110
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_ECG:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_BPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_BPF_CHEBY1:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_HPF_BUTTER:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->FILTER_LPF_CHEBY2:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    .line 133
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SOS_FILTER_SECTION:[I

    .line 141
    const/4 v0, 0x5

    new-array v0, v0, [[D

    new-array v1, v8, [D

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v8, [D

    fill-array-data v1, :array_2

    aput-object v1, v0, v6

    new-array v1, v8, [D

    fill-array-data v1, :array_3

    aput-object v1, v0, v7

    new-array v1, v7, [D

    fill-array-data v1, :array_4

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const/16 v2, 0xc

    new-array v2, v2, [D

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SCALE_VALUES:[[D

    .line 155
    const/4 v0, 0x5

    new-array v0, v0, [[[D

    new-array v1, v7, [[D

    new-array v2, v5, [D

    fill-array-data v2, :array_6

    aput-object v2, v1, v4

    new-array v2, v5, [D

    fill-array-data v2, :array_7

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [[D

    new-array v2, v5, [D

    fill-array-data v2, :array_8

    aput-object v2, v1, v4

    new-array v2, v5, [D

    fill-array-data v2, :array_9

    aput-object v2, v1, v6

    aput-object v1, v0, v6

    new-array v1, v7, [[D

    new-array v2, v5, [D

    fill-array-data v2, :array_a

    aput-object v2, v1, v4

    new-array v2, v5, [D

    fill-array-data v2, :array_b

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    new-array v1, v6, [[D

    new-array v2, v5, [D

    fill-array-data v2, :array_c

    aput-object v2, v1, v4

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const/16 v2, 0xb

    new-array v2, v2, [[D

    new-array v3, v5, [D

    fill-array-data v3, :array_d

    aput-object v3, v2, v4

    new-array v3, v5, [D

    fill-array-data v3, :array_e

    aput-object v3, v2, v6

    new-array v3, v5, [D

    fill-array-data v3, :array_f

    aput-object v3, v2, v7

    new-array v3, v5, [D

    fill-array-data v3, :array_10

    aput-object v3, v2, v8

    const/4 v3, 0x4

    new-array v4, v5, [D

    fill-array-data v4, :array_11

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-array v4, v5, [D

    fill-array-data v4, :array_12

    aput-object v4, v2, v3

    new-array v3, v5, [D

    fill-array-data v3, :array_13

    aput-object v3, v2, v5

    const/4 v3, 0x7

    new-array v4, v5, [D

    fill-array-data v4, :array_14

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v4, v5, [D

    fill-array-data v4, :array_15

    aput-object v4, v2, v3

    const/16 v3, 0x9

    new-array v4, v5, [D

    fill-array-data v4, :array_16

    aput-object v4, v2, v3

    const/16 v3, 0xa

    new-array v4, v5, [D

    fill-array-data v4, :array_17

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SOS_MATRIX:[[[D

    return-void

    .line 133
    nop

    :array_0
    .array-data 4
        0x2
        0x2
        0x2
        0x1
        0xb
    .end array-data

    .line 141
    :array_1
    .array-data 8
        0x3fefb11c6d1e108cL    # 0.99037
        0x3feed2f1a9fbe76dL    # 0.96325
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_2
    .array-data 8
        0x3fa61bb05faebc41L    # 0.04318
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_3
    .array-data 8
        0x3fc257fb69984a0eL    # 0.14331
        0x3fc257fb69984a0eL    # 0.14331
        0x3fec851eb851eb85L    # 0.89125
    .end array-data

    :array_4
    .array-data 8
        0x3fefea3100fed88eL    # 0.99733782
        0x3ff0000000000000L    # 1.0
    .end array-data

    :array_5
    .array-data 8
        0x3fe9efe17366c3c8L    # 0.810532308
        0x3fe8f46445cfc2f6L    # 0.77983297
        0x3fe7c5730e956f13L    # 0.742852715
        0x3fe652a91458490fL    # 0.697590389
        0x3fe489a4e7deff88L    # 0.641802266
        0x3fe258893a6aa3d2L    # 0.573307623
        0x3fdf6a8cfa0196e2L    # 0.490878338
        0x3fd95ac8ed88f3c9L    # 0.396166069
        0x3fd2ff2cc549ba78L    # 0.296824639
        0x3fcac9cab7a7544bL    # 0.209283199
        0x3fc4073c255b6a70L    # 0.156470793
        0x3ff0000000000000L    # 1.0
    .end array-data

    .line 155
    :array_6
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4000000000000000L    # -2.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400051eb851eb852L    # -1.98
        0x3fef67a0f9096bbaL    # 0.9814
    .end array-data

    :array_7
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4010000000000000L    # -1.0
        0x0
        0x3ff0000000000000L    # 1.0
        -0x40125a1cac083127L    # -0.9265
        0x0
    .end array-data

    :array_8
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x4000000000000000L    # 2.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400a844284dfce31L    # -1.34271
        0x3fe097f62b6ae7d5L    # 0.51855
    .end array-data

    :array_9
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4000000000000000L    # -2.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x40006caab8a5ce5bL    # -1.97347
        0x3fef29c779a6b50bL    # 0.97385
    .end array-data

    :array_a
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x0
        -0x4010000000000000L    # -1.0
        0x3ff0000000000000L    # 1.0
        -0x40004985f06f6944L    # -1.98205
        0x3fef6fbd273d5babL    # 0.98239
    .end array-data

    :array_b
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x0
        -0x4010000000000000L    # -1.0
        0x3ff0000000000000L    # 1.0
        -0x400599a415f45e0bL    # -1.64999
        0x3fe79e060fe47992L    # 0.73804
    .end array-data

    :array_c
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4000000000000000L    # -2.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400015d66d6815f2L    # -1.994668553
        0x3fefd470decb8e1aL    # 0.994682727
    .end array-data

    :array_d
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x40057240cbe6e291L    # -1.659606174
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x4004ea139a10b7deL    # -1.6928524
        0x3fef000570a4a533L    # 0.968752594
    .end array-data

    :array_e
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4005a7a0af7c48c0L    # -1.646575274
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x4005e5df56d6eb23L    # -1.631378804
        0x3fed06121c7bfb68L    # 0.906991058
    .end array-data

    :array_f
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x40061a67d4696812L    # -1.618553324
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x40070c8f0fdb72e0L    # -1.559433878
        0x3feaf8282370d998L    # 0.842792577
    .end array-data

    :array_10
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4006dd2840213e7aL    # -1.571006536
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x40086e6627dac30dL    # -1.473047108
        0x3fe8b6c0ff3930a3L    # 0.772308825
    .end array-data

    :array_11
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4008135074d15206L    # -1.495284599
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400a1d20ab12b531L    # -1.367888767
        0x3fe6235bd7c21e48L    # 0.691816255
    .end array-data

    :array_12
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4009fec85be8a2dbL    # -1.375297204
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400c29c4226a744eL    # -1.239803186
        0x3fe322682a945bcbL    # 0.597950061
    .end array-data

    :array_13
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x400d1cce2827cd8bL    # -1.180467456
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x400e9dfaeaf5e04aL    # -1.086430628
        0x3fdf473620899e8bL    # 0.488721401
    .end array-data

    :array_14
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x4014c2d9b3b26bf0L    # -0.851214551
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x4012db243abf3b0bL    # -0.910749326
        0x3fd76a3c756268e7L    # 0.365859141
    .end array-data

    :array_15
    .array-data 8
        0x3ff0000000000000L    # 1.0
        -0x402e5bfa560bbf15L    # -0.275636116
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x4018bdd7fcd6aac7L    # -0.72682572
        0x3fce8c6435f94ffcL    # 0.238659407
    .end array-data

    :array_16
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3fe6019f7cdd763dL    # 0.68769812
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x401dec25d6e565b1L    # -0.564923363
        0x3fc04f15426d9efbL    # 0.127413423
    .end array-data

    :array_17
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ffca9edab24ed86L    # 1.791486424
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        -0x402217b14eb96dedL    # -0.467303918
        0x3faf01d369c9f329L    # 0.060560805
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 119
    iput p3, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->mFilterIndex:I

    .line 120
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 110
    const-class v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    return-object v0
.end method

.method public static values()[Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->$VALUES:[Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    invoke-virtual {v0}, [Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;

    return-object v0
.end method


# virtual methods
.method public getFilterSection()I
    .locals 2

    .prologue
    .line 123
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SOS_FILTER_SECTION:[I

    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->mFilterIndex:I

    aget v0, v0, v1

    return v0
.end method

.method public getScaleValueMatrix()[D
    .locals 2

    .prologue
    .line 129
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SCALE_VALUES:[[D

    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->mFilterIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getSosMatrix()[[D
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->SOS_MATRIX:[[[D

    iget v1, p0, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter$SosFilterForm;->mFilterIndex:I

    aget-object v0, v0, v1

    return-object v0
.end method
