.class public Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;
.super Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
.source "ShapExtendedMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShapExtMsgCommonExtraData"
.end annotation


# instance fields
.field public mDeviceType:I

.field public mExtraID:S

.field public mExtraMessageBody:[B


# direct methods
.method public constructor <init>([B)V
    .locals 10
    .param p1, "arrData"    # [B

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;-><init>([B)V

    .line 104
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    .line 108
    if-eqz p1, :cond_0

    array-length v3, p1

    int-to-long v4, v3

    iget-wide v6, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mMessageLength:J

    const-wide/16 v8, 0x8

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-ltz v3, :cond_0

    .line 110
    const/16 v0, 0x8

    .line 111
    .local v0, "nIdx":I
    iget-wide v4, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mMessageLength:J

    const-wide/16 v6, 0x3

    sub-long/2addr v4, v6

    long-to-int v2, v4

    .line 112
    .local v2, "nMsgBodySize":I
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "nIdx":I
    .local v1, "nIdx":I
    aget-byte v3, p1, v0

    int-to-short v3, v3

    iput-short v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraID:S

    .line 113
    aget-byte v3, p1, v1

    const/16 v4, 0xa

    aget-byte v4, p1, v4

    invoke-static {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeUint16(BB)I

    move-result v3

    iput v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mDeviceType:I

    add-int/lit8 v0, v1, 0x2

    .line 114
    .end local v1    # "nIdx":I
    .restart local v0    # "nIdx":I
    new-array v3, v2, [B

    iput-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    .line 115
    iget-object v3, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    const/4 v4, 0x0

    invoke-static {p1, v0, v3, v4, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 117
    .end local v0    # "nIdx":I
    .end local v2    # "nMsgBodySize":I
    :cond_0
    return-void
.end method


# virtual methods
.method public parseExtraMessage()Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    .locals 2

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 123
    .local v0, "shapHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    iget v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mDeviceType:I

    packed-switch v1, :pswitch_data_0

    .line 132
    :goto_0
    :pswitch_0
    return-object v0

    .line 126
    :pswitch_1
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    invoke-static {v1}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    .line 127
    goto :goto_0

    .line 129
    :pswitch_2
    iget-object v1, p0, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonExtraData;->mExtraMessageBody:[B

    invoke-static {v1}, Lcom/sec/dmc/hsl/android/shap/ShapBodyCompositionAnalyzerMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v0

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
