.class public Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
.super Ljava/lang/Object;
.source "InternalEcgHandler.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
.implements Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;,
        Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;
    }
.end annotation


# static fields
.field private static final CMD:[B

.field private static final TAG:Ljava/lang/String;

.field private static bListenerRegistered:Z


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field public isFactorySelf:Z

.field mContext:Landroid/content/Context;

.field mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

.field mManager:Lcom/samsung/android/ssensor/SSensorManager;

.field mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

.field private mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

.field private needToStart:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    .line 35
    const/16 v0, 0x18

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    .line 37
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    return-void

    .line 35
    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x1t
        0x0t
        -0x80t
        0x0t
        0x30t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "InternalEcgHandler_MSP"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CLASS_NAME:Ljava/lang/String;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->isFactorySelf:Z

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z

    .line 32
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    .line 33
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    .line 38
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 39
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 41
    new-instance v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    .line 204
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z

    return p1
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    return p0
.end method


# virtual methods
.method public deinitialize()V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "deinitialize is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->unregisterEcgListener()Z

    .line 55
    :cond_0
    return-void
.end method

.method public getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public getProfileHandlerListener()Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    return-object v0
.end method

.method public get_ShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .param p3, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    .line 186
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 187
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 188
    invoke-static {}, Lcom/samsung/android/ssensor/SSensorManager;->getSSensorManager()Lcom/samsung/android/ssensor/SSensorManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    .line 189
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    .line 191
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1, p0, v2, p4}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I

    .line 195
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mContext:Landroid/content/Context;

    .line 47
    return-void
.end method

.method public onDataReceived(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 236
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(ILandroid/os/Bundle;)V

    .line 240
    :cond_0
    return-void
.end method

.method public onDataReceived([Landroid/os/Bundle;[Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # [Landroid/os/Bundle;
    .param p2, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 227
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onDataReceived[]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataReceived(I[Landroid/os/Bundle;)V

    .line 231
    :cond_0
    return-void
.end method

.method public onDataStarted(I)V
    .locals 3
    .param p1, "dataType"    # I

    .prologue
    .line 245
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStarted dataType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;->onDataStarted(II)V

    .line 249
    :cond_0
    return-void
.end method

.method public onDataStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 254
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDataStopped dataType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->stopReceivingData()I

    .line 257
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;)V
    .locals 2
    .param p1, "response"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice$Response;

    .prologue
    .line 293
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "onResponseReceived"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 262
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    return-void
.end method

.method public registerEcgListener()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/ssensor/SSensorManager;->registerListener(Lcom/samsung/android/ssensor/SSensorListener;Landroid/os/Handler;I)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    .line 154
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 155
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "Ecg sensor registered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_0
    sget-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    return v0

    .line 159
    :cond_0
    sput-boolean v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 160
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v1, "Ecg sensor not registered"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sendRawData([B)I
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/16 v0, 0x3e8

    const/4 v1, 0x0

    .line 271
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v3, "sendRawData"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    if-nez p1, :cond_1

    .line 275
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v2, "sendRawData / buffer == null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    :cond_0
    :goto_0
    return v0

    .line 279
    :cond_1
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v2, :cond_0

    .line 281
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v2, "sendRawData / manager !=null"

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    move v0, v1

    .line 284
    goto :goto_0
.end method

.method public setProfileHandlerListener(Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;)V
    .locals 0
    .param p1, "profileHandlerListener"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProfileHandlerListener:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;

    .line 76
    return-void
.end method

.method public set_ShealthSensorDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V
    .locals 0
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 64
    return-void
.end method

.method public startReceivingData(ZLjava/lang/String;)I
    .locals 5
    .param p1, "isFactorySelf"    # Z
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z

    .line 100
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startReceivingData, bListenerRegistered : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    if-eqz v2, :cond_2

    .line 104
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    iput-boolean v0, v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_ecg_delta:Z

    .line 105
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    iput-boolean v0, v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_heart_rate:Z

    .line 106
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    iput v0, v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->ecgProccessingCount:I

    .line 108
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->init_ecgValues()V

    .line 110
    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-nez v2, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->registerEcgListener()Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 115
    :cond_0
    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v2, :cond_2

    .line 117
    if-eqz p1, :cond_1

    .line 118
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startFactoryMode()I

    .line 119
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startReceivingData, startFactoryMode()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-wide/16 v2, 0xa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v2, p2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyStart(Ljava/lang/String;)I

    .line 131
    :cond_2
    sget-boolean v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v2, :cond_3

    :goto_1
    return v0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 122
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyStop()I

    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public unregisterEcgListener()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 167
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterEcgListener : current state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    if-eqz v1, :cond_0

    .line 171
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;

    const-string v2, "unregisterSensorManager is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    sput-boolean v0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    .line 173
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->CMD:[B

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/ssensor/SSensorManager;->writeData([BI)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mManager:Lcom/samsung/android/ssensor/SSensorManager;

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mListener:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/ssensor/SSensorManager;->unregisterListener(Lcom/samsung/android/ssensor/SSensorListener;)V

    .line 177
    :cond_0
    sget-boolean v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z

    if-eqz v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
