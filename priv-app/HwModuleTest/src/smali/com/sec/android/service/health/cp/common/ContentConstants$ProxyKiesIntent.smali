.class public interface abstract Lcom/sec/android/service/health/cp/common/ContentConstants$ProxyKiesIntent;
.super Ljava/lang/Object;
.source "ContentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/ContentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProxyKiesIntent"
.end annotation


# static fields
.field public static final PROXY_KIES_RESTORE:Ljava/lang/String; = "proxy.android.intent.action.REQUEST_RESTORE_SHEALTH"

.field public static final PROXY_KIES_RESTORE_COMPLETE_INTENT:Ljava/lang/String; = "proxy.android.intent.action.RESPONSE_RESTORE_SHEALTH"

.field public static final PROXY_KIES_RESTORE_ERROR_INTENT:Ljava/lang/String; = "proxy.android.intent.action.RESPONSE_RESTORE_ERROR_SHEALTH"
