.class public interface abstract Lcom/sec/android/service/health/cp/common/ContentConstants$ProfileKeyConstants;
.super Ljava/lang/Object;
.source "ContentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/ContentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProfileKeyConstants"
.end annotation


# static fields
.field public static final ACTIVITY_TYPE:Ljava/lang/String; = "activity_type"

.field public static final BIRTH_DATE:Ljava/lang/String; = "birth_date"

.field public static final BLOOD_GLUCOSE_UNIT:Ljava/lang/String; = "blood_glucose_unit"

.field public static final COUNTRY:Ljava/lang/String; = "country"

.field public static final DISTANCE_UNIT:Ljava/lang/String; = "distance_unit"

.field public static final GENDER:Ljava/lang/String; = "gender"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final HEIGHT_UNIT:Ljava/lang/String; = "height_unit"

.field public static final IN_SYNC:Ljava/lang/String; = "in_sync"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PROFILE_CREATE_TIME:Ljava/lang/String; = "profile_create_time"

.field public static final PROFILE_DISCLOSE_YN:Ljava/lang/String; = "profile_disclose_yn"

.field public static final TEMPERATURE_UNIT:Ljava/lang/String; = "temperature_unit"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final WEIGHT:Ljava/lang/String; = "weight"

.field public static final WEIGHT_UNIT:Ljava/lang/String; = "weight_unit"
