.class public final Lcom/sec/android/service/health/cp/common/ContentConstants$CPConstants;
.super Ljava/lang/Object;
.source "ContentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/ContentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CPConstants"
.end annotation


# static fields
.field public static final COLUMN_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final COLUMN_DAYLIGHT_SAVING:Ljava/lang/String; = "daylight_saving"

.field public static final COLUMN_HDID:Ljava/lang/String; = "hdid"

.field public static final COLUMN_INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final COLUMN_SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final COLUMN_SYNC_STATUS:Ljava/lang/String; = "sync_status"

.field public static final COLUMN_TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final COLUMN_UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final CONFIG_COLUMN_KEY:Ljava/lang/String; = "key"

.field public static final CONFIG_COLUMN_VALUE:Ljava/lang/String; = "value"

.field public static final CONFIG_OPTION_GET:Ljava/lang/String; = "CONFIG_OPTION_GET"

.field public static final CONFIG_OPTION_PUT:Ljava/lang/String; = "CONFIG_OPTION_PUT"

.field public static final FILE_MANAGER:Ljava/lang/String; = "file_manager"

.field public static final FILE_MANAGER_CREATE:Ljava/lang/String; = "file_manager_create"

.field public static final FILE_MANAGER_DETAILS:Ljava/lang/String; = "file_manager_details"

.field public static final FILE_MANAGER_MODIFIED:Ljava/lang/String; = "file_manager_modify"

.field public static final FILE_MANAGER_RENAME:Ljava/lang/String; = "file_manager_rename"

.field public static final PRIVATE_COLUMN_DETAIL_MESSAGE:Ljava/lang/String; = "detail_message"

.field public static final PRIVATE_COLUMN_IS_SUCCESS:Ljava/lang/String; = "is_success"

.field public static final PRIVATE_COLUMN_PACKAGE_LIST:Ljava/lang/String; = "package_list"

.field public static final PRIVATE_OPTION_REMOVE_DOWNLOADABLE:Ljava/lang/String; = "remove_downloadable"

.field public static final RAWQUERY:Ljava/lang/String; = "rawquery"

.field public static final READ_EXERCISE_IMAGE:Ljava/lang/String; = "read_exercise_image"

.field public static final READ_FILES_MEAL:Ljava/lang/String; = "read_files_meal"

.field public static final READ_USER_IMAGE:Ljava/lang/String; = "read_user_image"

.field public static final URI_MIGRATION:Ljava/lang/String; = "migrate"

.field public static final WRITE_EXERCISE_IMAGE:Ljava/lang/String; = "write_exercise_image"

.field public static final WRITE_FILES_MEAL:Ljava/lang/String; = "write_files_meal"

.field public static final WRITE_USER_IMAGE:Ljava/lang/String; = "write_user_image"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
