.class public Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;
.super Ljava/lang/Object;
.source "HttpConnectionConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$1;,
        Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;,
        Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    }
.end annotation


# static fields
.field public static HTTPS_ENABLE:Z = false

.field public static HTTP_PORT:I = 0x0

.field public static HTTP_SERVER:Ljava/lang/String; = null

.field public static IS_DEV_SERVER:Z = false

.field private static final SERVER_ADDR_CN:Ljava/lang/String; = "cn-openapi.samsungshealth.com"

.field private static final SERVER_ADDR_EU:Ljava/lang/String; = "eu-openapi.samsungshealth.com"

.field private static final SERVER_ADDR_KR:Ljava/lang/String; = "kr-openapi.samsungshealth.com"

.field public static final SERVER_ADDR_US:Ljava/lang/String; = "openapi.samsungshealth.com"

.field private static final SERVER_PHASE:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

.field public static final TAG:Ljava/lang/String;

.field private static countryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x1bb

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    const-class v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->TAG:Ljava/lang/String;

    .line 20
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;->STG2:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->SERVER_PHASE:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    .line 43
    invoke-static {}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->initializeCountryMap()V

    .line 47
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->SERVER_PHASE:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;->DEV:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    if-ne v0, v1, :cond_0

    .line 49
    const-string v0, "23.23.220.209"

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    .line 50
    const/16 v0, 0x50

    sput v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    .line 51
    sput-boolean v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTPS_ENABLE:Z

    .line 52
    sput-boolean v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->IS_DEV_SERVER:Z

    .line 81
    :goto_0
    return-void

    .line 54
    :cond_0
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->SERVER_PHASE:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;->STG2:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    if-ne v0, v1, :cond_1

    .line 56
    const-string v0, "107.21.30.112"

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    .line 57
    sput v4, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    .line 58
    sput-boolean v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTPS_ENABLE:Z

    .line 59
    sput-boolean v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->IS_DEV_SERVER:Z

    goto :goto_0

    .line 64
    :cond_1
    sput v4, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_PORT:I

    .line 65
    sput-boolean v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTPS_ENABLE:Z

    .line 66
    sput-boolean v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->IS_DEV_SERVER:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static getPlatformServerURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "mcc"    # Ljava/lang/String;

    .prologue
    .line 86
    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->SERVER_PHASE:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;->PRD:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$ServerPhase;

    if-eq v1, v2, :cond_0

    .line 88
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->HTTP_SERVER:Ljava/lang/String;

    .line 115
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const-string v0, "eu-openapi.samsungshealth.com"

    .line 92
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$1;->$SwitchMap$com$sec$android$service$health$cp$common$HttpConnectionConstants$RegionGroup:[I

    invoke-static {p0}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->getRegionGroup(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 111
    sget-object v1, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->TAG:Ljava/lang/String;

    const-string v2, "MCC is unkown. Directing to default server, EU."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 95
    :pswitch_0
    const-string v0, "eu-openapi.samsungshealth.com"

    .line 96
    goto :goto_0

    .line 99
    :pswitch_1
    const-string v0, "openapi.samsungshealth.com"

    .line 100
    goto :goto_0

    .line 103
    :pswitch_2
    const-string v0, "cn-openapi.samsungshealth.com"

    .line 104
    goto :goto_0

    .line 107
    :pswitch_3
    const-string v0, "kr-openapi.samsungshealth.com"

    .line 108
    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getRegionGroup(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .locals 3
    .param p0, "mcc"    # Ljava/lang/String;

    .prologue
    .line 121
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RegionGroup for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    return-object v0
.end method

.method private static initializeCountryMap()V
    .locals 3

    .prologue
    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    .line 129
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "-1"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "202"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "204"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "206"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "208"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "212"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "213"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "214"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "216"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "218"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "219"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "220"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "222"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "225"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "226"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "228"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "230"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "231"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "232"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "234"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "235"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "238"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "240"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "242"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "244"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "246"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "247"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "248"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "250"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "255"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "257"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "259"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "260"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "262"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "266"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "268"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "270"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "272"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "274"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "276"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "278"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "280"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "282"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "283"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "284"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "286"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "288"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "290"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "292"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "293"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "294"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "295"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "297"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "302"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "308"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "310"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "311"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "316"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "330"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "332"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "334"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "338"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "340"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "342"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "344"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "346"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "348"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "350"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "352"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "354"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "356"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "358"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "360"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "362"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "363"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "364"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "365"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "366"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "368"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "370"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "372"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "374"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "376"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "400"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "401"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "402"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "404"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "405"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "410"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "412"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "413"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "414"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "415"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "416"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "417"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "418"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "419"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "420"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "421"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "422"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "423"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "424"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "425"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "426"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "427"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "428"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "429"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "430"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "431"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "432"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "434"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "436"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "437"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "438"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "440"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "441"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "450"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->KR:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "452"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "454"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "455"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "456"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "457"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "460"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "466"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "467"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->KR:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "470"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "472"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "502"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "505"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "510"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "514"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "515"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "520"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "525"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "528"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "530"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "534"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "535"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "536"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "537"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "539"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "540"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "541"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "542"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "543"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "544"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "545"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "546"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "547"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "548"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "549"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "550"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "551"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "552"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "553"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "555"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "602"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "603"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "604"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "605"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "606"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "607"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "608"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "609"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "610"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "611"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "612"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "613"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "614"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "615"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "616"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "617"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "618"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "619"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "620"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "621"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "622"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "623"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "624"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "625"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "626"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "627"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "628"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "629"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "630"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "631"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "632"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "633"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "634"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "635"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "636"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "637"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "638"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "639"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "640"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "641"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "642"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "643"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "645"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "646"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "647"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "648"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "649"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "650"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "651"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "652"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "653"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "654"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "655"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "657"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "702"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "704"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 341
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "706"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "708"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "710"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "712"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 345
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "714"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "716"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "722"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "724"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "730"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "732"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "734"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "736"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "738"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "740"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "742"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "744"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "746"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "748"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "750"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->US:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v0, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->countryMap:Ljava/util/HashMap;

    const-string v1, "901"

    sget-object v2, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->EU:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    return-void
.end method
