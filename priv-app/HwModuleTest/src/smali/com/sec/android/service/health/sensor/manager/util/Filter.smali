.class public Lcom/sec/android/service/health/sensor/manager/util/Filter;
.super Ljava/lang/Object;
.source "Filter.java"


# instance fields
.field private mRequiredDataType:I

.field private mRequiredDeviceId:Ljava/lang/String;

.field private mRequiredDeviceType:I


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1, "deviceType"    # I
    .param p2, "dataType"    # I
    .param p3, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    .line 18
    iput p2, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    .line 19
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getFilterDataType()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    return v0
.end method

.method public getFilterDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getFilterDeviceType()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceType:I

    return v0
.end method

.method public verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z
    .locals 6
    .param p1, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 39
    if-nez p1, :cond_0

    .line 62
    :goto_0
    return v3

    .line 44
    :cond_0
    const/4 v0, 0x1

    .line 51
    .local v0, "isValid":Z
    iget v4, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    .line 53
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v1

    .line 54
    .local v1, "supportedTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_3

    iget v4, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDataType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    .line 57
    .end local v1    # "supportedTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    .line 59
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/util/Filter;->mRequiredDeviceId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    :cond_2
    :goto_2
    move v3, v0

    .line 62
    goto :goto_0

    .restart local v1    # "supportedTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_3
    move v0, v3

    .line 54
    goto :goto_1

    .end local v1    # "supportedTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_4
    move v0, v3

    .line 59
    goto :goto_2
.end method
