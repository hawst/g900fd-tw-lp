.class public Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
.super Ljava/lang/Object;
.source "ProfileHandlerController.java"


# static fields
.field private static final STATE_DATA_AND_REQUEST_PROCESSING:I = 0x3

.field private static final STATE_DATA_RECEIVING:I = 0x1

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_REQUEST_PROCESSING:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

.field public isFactorySelf:Z

.field private listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

.field private mDataRecord:Z

.field private mDataType:I

.field private mProcessId:I

.field mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;"
        }
    .end annotation
.end field

.field private mcontrollingDevObjId:Ljava/lang/Integer;

.field private uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PrivHealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;)V
    .locals 4
    .param p1, "ShealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "handler"    # Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    .param p3, "listener"    # Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isFactorySelf:Z

    .line 30
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    .line 31
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 34
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataRecord:Z

    .line 35
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mDataType:I

    .line 42
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 43
    iput v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mProcessId:I

    .line 45
    iput-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->uniqueKey:Ljava/lang/String;

    .line 301
    new-instance v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;-><init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    .line 49
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    .line 50
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 51
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->listener:Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;

    .line 52
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getRemoteEventCallBackList(Ljava/lang/String;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method private getSensorDataListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "devId"    # I

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getRemoteEventCallBackList(Ljava/lang/String;)Landroid/os/RemoteCallbackList;

    move-result-object v1

    .line 167
    .local v1, "remoteCallBackList":Landroid/os/RemoteCallbackList;, "Landroid/os/RemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const/4 v0, 0x0

    .line 168
    .local v0, "listener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-nez v1, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 189
    .end local v0    # "listener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    :cond_0
    return-object v0
.end method


# virtual methods
.method public addDataListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)V
    .locals 5
    .param p1, "devObjId"    # Ljava/lang/Integer;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    .line 136
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDataListener() devObjId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addSensorEventListener deviceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .line 144
    .local v1, "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    if-nez v1, :cond_0

    .line 146
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addSensorEventListener RemotecallBackListAdded created with device : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    new-instance v1, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .end local v1    # "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mRemoteCallbackDied:Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;-><init>(Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)V

    .line 152
    .restart local v1    # "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    :cond_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->getSensorDataListener(Ljava/lang/String;I)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    move-result-object v0

    .line 153
    .local v0, "dataListener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-eqz v0, :cond_1

    .line 155
    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 157
    :cond_1
    invoke-virtual {v1, p2, p1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    .line 158
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDataListener :  Registered listner Count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    return-void
.end method

.method public deinitialize()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->deinitialize()V

    .line 64
    :cond_0
    return-void
.end method

.method public initiallize(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->device:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-interface {v0, p1, v1, v2, p2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I

    .line 58
    :cond_0
    return-void
.end method

.method public declared-synchronized registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener() device Id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v1, :cond_0

    .line 98
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "registerListener() exerciseID Invoking Handler\'s start Receving Data"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    .line 101
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->getDevice()Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->mcontrollingDevObjId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->setObjectId(I)V

    .line 104
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->registerEcgListener()Z

    move-result v0

    .line 106
    .local v0, "isResgistered":Z
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener() isResgistered "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    .end local v0    # "isResgistered":Z
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public removeDataListener(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 5
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "devId"    # Ljava/lang/Integer;

    .prologue
    .line 199
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeDataListener() processId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->dataListenerHashMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;

    .line 202
    .local v1, "remoteCallBackList":Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;, "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList<Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;>;"
    const/4 v0, 0x0

    .line 204
    .local v0, "listener":Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;
    if-nez v1, :cond_1

    .line 206
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v3, "removeSensorEventListener :  remoteCallBackList instance is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    if-eqz v0, :cond_0

    .line 226
    invoke-virtual {v1, v0}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 227
    sget-object v2, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeSensorEventListener :  Count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList;->getRegisteredCallbackCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized startReceivingData(ZLjava/lang/String;)I
    .locals 4
    .param p1, "isFactorySelf"    # Z
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1, p1, p2}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->startReceivingData(ZLjava/lang/String;)I

    move-result v0

    .line 71
    .local v0, "error":I
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startReceivingData() error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v0    # "error":I
    :goto_0
    monitor-exit p0

    return v0

    .line 75
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "startReceivingData() exerciseID returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    const/4 v0, 0x1

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized stopReceivingData()I
    .locals 4

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->stopReceivingData()I

    move-result v0

    .line 84
    .local v0, "error":I
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopReceivingData() error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    .end local v0    # "error":I
    :goto_0
    monitor-exit p0

    return v0

    .line 88
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "stopReceivingData() exerciseID returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    const/4 v0, 0x1

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized unregisterListener(Ljava/lang/Integer;)Z
    .locals 4
    .param p1, "devObjectId"    # Ljava/lang/Integer;

    .prologue
    const/4 v0, 0x0

    .line 116
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterListener() devObjectId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    if-nez p1, :cond_0

    .line 120
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "unregisterListener() processId is NULL so returning "

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :goto_0
    monitor-exit p0

    return v0

    .line 124
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    if-eqz v1, :cond_1

    .line 126
    sget-object v0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v1, "unregisterListener() Invoking Handler\'s stop Receving Data"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->handler:Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;->unregisterEcgListener()Z

    move-result v0

    goto :goto_0

    .line 130
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;

    const-string v2, "unregisterListener() returning ERROR_FAILURE"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
