.class Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;
.super Ljava/lang/Object;
.source "ProfileHandlerController.java"

# interfaces
.implements Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/service/health/sensor/manager/HealthRemoteCallbackList$IHealthRemoteCallbackDiedListener",
        "<",
        "Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;


# direct methods
.method constructor <init>(Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController$1;->this$0:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public remoteCallbackDied(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)V
    .locals 3
    .param p1, "d"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "cookie"    # Ljava/lang/Object;

    .prologue
    .line 306
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 308
    # getter for: Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemoteCallbackDied :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :cond_0
    return-void
.end method
