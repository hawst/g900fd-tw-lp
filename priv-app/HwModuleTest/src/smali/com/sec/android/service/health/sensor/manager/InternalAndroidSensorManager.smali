.class public Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
.super Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
.source "InternalAndroidSensorManager.java"


# static fields
.field private static final FEATURE_HEALTHCOVER:Ljava/lang/String; = "com.sec.feature.healthcover"

.field private static final TAG:Ljava/lang/String;

.field private static sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field public isFactorySelf:Z

.field private mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mHandlerMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PrivHealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    .line 48
    new-instance v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    invoke-direct {v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;-><init>()V

    sput-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;-><init>()V

    .line 45
    const-string v0, "InternalAndroidSensorManager_MSP"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->CLASS_NAME:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->isFactorySelf:Z

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 54
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    .line 60
    return-void
.end method

.method public static getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->sInternalSensorManager:Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    return-object v0
.end method


# virtual methods
.method public getConnectedDevices(Lcom/sec/android/service/health/sensor/manager/util/Filter;)Ljava/util/List;
    .locals 2
    .param p1, "filter"    # Lcom/sec/android/service/health/sensor/manager/util/Filter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/service/health/sensor/manager/util/Filter;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    .local v0, "device":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;>;"
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {p1, v1}, Lcom/sec/android/service/health/sensor/manager/util/Filter;->verifyDevice(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    return-object v0
.end method

.method public initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/service/health/sensor/manager/ISensorListener;

    .prologue
    const/4 v5, 0x4

    .line 69
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-super {p0, p1, p2}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    .line 73
    sget-object v0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v1, "ECG - Device is initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v1, "ECG"

    const-string v2, "ECG"

    const/4 v3, 0x1

    const/16 v4, 0x2729

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 75
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    const-class v1, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 76
    return-void
.end method

.method public isDataTypeSupported(I)Z
    .locals 1
    .param p1, "dataType"    # I

    .prologue
    .line 94
    packed-switch p1, :pswitch_data_0

    .line 102
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 99
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z
    .locals 9
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 117
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .line 119
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-nez v4, :cond_1

    .line 121
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "registerListener / profileHandlerController == null"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v3, 0x0

    .line 126
    .local v3, "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mHandlerMap:Landroid/util/SparseArray;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getDataType()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 128
    .local v2, "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 141
    .end local v2    # "handlerClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    :goto_0
    if-eqz v3, :cond_2

    .line 143
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "registerListener / make profileHandlerController"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    new-instance v4, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-direct {v4, p1, v3, v8}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;Lcom/sec/android/service/health/sensor/manager/BaseSensorManagerListener;)V

    iput-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    .line 146
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    iget-object v6, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6, v8}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->initiallize(Landroid/content/Context;Ljava/lang/Object;)V

    .line 147
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->addProfileHandlerController(Ljava/lang/String;Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    iget-boolean v6, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->isFactorySelf:Z

    iput-boolean v6, v4, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->isFactorySelf:Z

    .line 159
    .end local v3    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-nez v4, :cond_3

    move v4, v5

    :goto_2
    return v4

    .line 131
    .restart local v3    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 135
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 154
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_2
    sget-object v4, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->TAG:Ljava/lang/String;

    const-string v6, "createDeviceHandler: profileHandlerInterfaced is null !!"

    invoke-static {v4, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 159
    .end local v3    # "profileHandlerInterface":Lcom/samsung/android/sdk/health/sensor/handler/PrivilegeSensorProfileHandlerInterface;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v4, p1, p2, p3}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z

    move-result v4

    goto :goto_2
.end method

.method public startReceivingData(ZLjava/lang/String;)I
    .locals 1
    .param p1, "isFactorySelf"    # Z
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->startReceivingData(ZLjava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public stopReceivingData()I
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->profileHandlerController:Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->stopReceivingData()I

    move-result v0

    goto :goto_0
.end method

.method public unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p2, "devId"    # Ljava/lang/Integer;

    .prologue
    .line 165
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getProfileHandlerController(Ljava/lang/String;)Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;

    move-result-object v0

    .line 167
    .local v0, "profileController":Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0, p2}, Lcom/sec/android/service/health/sensor/handler/ProfileHandlerController;->unregisterListener(Ljava/lang/Integer;)Z

    move-result v1

    .line 172
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
