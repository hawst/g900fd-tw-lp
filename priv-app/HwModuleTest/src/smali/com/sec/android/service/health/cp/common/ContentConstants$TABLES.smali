.class public final Lcom/sec/android/service/health/cp/common/ContentConstants$TABLES;
.super Ljava/lang/Object;
.source "ContentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/ContentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TABLES"
.end annotation


# static fields
.field public static final APPLICATION:Ljava/lang/String; = "application"

.field public static final APPLICATION_DEVICE_PREF:Ljava/lang/String; = "application_device_pref"

.field public static final APPLICATION_PERMISSIONS:Ljava/lang/String; = "application_permissions"

.field public static final BLOOD_GLUCOSE:Ljava/lang/String; = "blood_glucose"

.field public static final BLOOD_GLUCOSE_MEASUREMENT_CONTEXT:Ljava/lang/String; = "blood_glucose_measurement_context"

.field public static final BLOOD_PRESSURE:Ljava/lang/String; = "blood_pressure"

.field public static final BODY_TEMPERATURE:Ljava/lang/String; = "body_temperature"

.field public static final DELETEINFO_TABLE_NAME:Ljava/lang/String; = "deleted_info"

.field public static final ELECTRO_CARDIOGRAPHY:Ljava/lang/String; = "electro_cardiography"

.field public static final ELECTRO_CARDIOGRAPHY_ANALYSIS:Ljava/lang/String; = "electro_cardiography_analysis"

.field public static final EXERCISE:Ljava/lang/String; = "exercise"

.field public static final EXERCISE_ACTIVITY:Ljava/lang/String; = "exercise_activity"

.field public static final EXERCISE_DEVICE_INFO:Ljava/lang/String; = "exercise_device_info"

.field public static final EXERCISE_INFO:Ljava/lang/String; = "exercise_info"

.field public static final EXERCISE_MISSION:Ljava/lang/String; = "exercise_mission"

.field public static final EXERCISE_PHOTO:Ljava/lang/String; = "exercise_photo"

.field public static final FIRST_BEAT_COACHING_DATA:Ljava/lang/String; = "first_beat_coaching_data"

.field public static final FIRST_BEAT_COACHING_RESULT:Ljava/lang/String; = "first_beat_coaching_result"

.field public static final FIRST_BEAT_COACHING_VARIABLE:Ljava/lang/String; = "first_beat_coaching_variable"

.field public static final FOOD_INFO:Ljava/lang/String; = "food_info"

.field public static final FOOD_NUTRIENT:Ljava/lang/String; = "food_nutrient"

.field public static final GOAL:Ljava/lang/String; = "goal"

.field public static final HEART_RATE:Ljava/lang/String; = "heart_rate"

.field public static final HEART_RATE_DATA:Ljava/lang/String; = "heart_rate_data"

.field public static final LOCATION_DATA:Ljava/lang/String; = "location_data"

.field public static final MEAL:Ljava/lang/String; = "meal"

.field public static final MEAL_IMAGE:Ljava/lang/String; = "meal_image"

.field public static final MEAL_ITEM:Ljava/lang/String; = "meal_item"

.field public static final MEAL_PLAN:Ljava/lang/String; = "meal_plan"

.field public static final PAIRED_DEVICE:Ljava/lang/String; = "paired_device"

.field public static final PULSE_OXIMETER:Ljava/lang/String; = "pulse_oximeter"

.field public static final REALTIME_DATA:Ljava/lang/String; = "realtime_data"

.field public static final SENSOR_HANDLER_DETAILS:Ljava/lang/String; = "sensor_handler_details"

.field public static final SHARED_PREFERENCES:Ljava/lang/String; = "shared_pref"

.field public static final SKIN:Ljava/lang/String; = "skin"

.field public static final SLEEP:Ljava/lang/String; = "sleep"

.field public static final SLEEP_DATA:Ljava/lang/String; = "sleep_data"

.field public static final STRENGTH_FITNESS:Ljava/lang/String; = "strength_fitness"

.field public static final STRESS:Ljava/lang/String; = "stress"

.field public static final TAG:Ljava/lang/String; = "tag"

.field public static final TEMPERATURE_HUMIDITY:Ljava/lang/String; = "temperature_humidity"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final ULTRAVIOLET_RAYS:Ljava/lang/String; = "ultraviolet_rays"

.field public static final USER_DEVICE:Ljava/lang/String; = "user_device"

.field public static final UV_PROTECTION:Ljava/lang/String; = "uv_protection"

.field public static final WALK_INFO:Ljava/lang/String; = "walk_info"

.field public static final WALK_INFO_EXTENDED:Ljava/lang/String; = "walk_info_extended"

.field public static final WATER_INGESTION:Ljava/lang/String; = "water_ingestion"

.field public static final WEIGHT:Ljava/lang/String; = "weight"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
