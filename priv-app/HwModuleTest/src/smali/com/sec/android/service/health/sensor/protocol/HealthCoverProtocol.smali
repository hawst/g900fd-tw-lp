.class public Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
.super Ljava/lang/Object;
.source "HealthCoverProtocol.java"


# static fields
.field private static final DATA_STATUS_STARTED:I = 0x1

.field private static final DATA_STATUS_STOPPED:I = 0x0

.field private static final DEFAULT_WAIT_MS:I = 0x14

.field public static final HR_MAX:I = 0xe1

.field public static final HR_MIN:I = 0x19

.field public static final NOT_ASSIGNED_INT:I = 0x7fffffff

.field private static final PROFILE_NAME:Ljava/lang/String; = "shap"

.field private static final PROTOCOL_NAME:Ljava/lang/String; = "HEALTHCOVER_PROTOCOL"

.field private static final TAG:Ljava/lang/String;

.field private static final mCoefA:F = 4.808108E-8f

.field private static final mCoefB:F = 0.0f

.field private static final mFs:F = 500.0f


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private MEASUREMENT_WAIT_DELAY:I

.field private final MEASUREMENT_WAIT_DELAY_D_RANGE:I

.field private final MEASUREMENT_WAIT_DELAY_FREQUENCY:I

.field private final MEASUREMENT_WAIT_DELAY_IMPEDANCE:I

.field private final MEASUREMENT_WAIT_DELAY_NOISE:I

.field private MEASURING_TIME:I

.field private final MEASURING_TIME_D_RANGE:I

.field private final MEASURING_TIME_FREQUENCY:I

.field private final MEASURING_TIME_IMPEDANCE:I

.field private final MEASURING_TIME_NOISE:I

.field private delta_ecg:F

.field private delta_ecg_rawData:I

.field private delta_ecg_rawData_mV:F

.field public ecgProccessingCount:I

.field private mBundleEcg:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mDataTranceiveStatus:I

.field private mDataTranceiveType:I

.field private mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

.field private mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

.field private mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

.field private mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

.field private mReceivedHeartRate:I

.field public mRespondList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;",
            ">;"
        }
    .end annotation
.end field

.field public mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

.field public mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

.field public mShapExtMsgCommonAlertExtended:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;

.field public mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

.field public mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

.field public mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

.field private max_ecg:F

.field private max_ecg_rawData:I

.field private max_ecg_rawData_mV:F

.field private min_ecg:F

.field private min_ecg_rawData:I

.field private min_ecg_rawData_mV:F

.field public sent_ecg_delta:Z

.field public sent_heart_rate:Z

.field private startTimeSeconds:J

.field private time:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/high16 v5, 0x447a0000    # 1000.0f

    const/4 v4, 0x0

    const/high16 v1, -0x3b860000    # -1000.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "HealthCoverProtocol_MSP"

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    .line 61
    const/16 v0, 0x1b58

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY_D_RANGE:I

    .line 62
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY_FREQUENCY:I

    .line 63
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY_NOISE:I

    .line 64
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY_IMPEDANCE:I

    .line 65
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME_D_RANGE:I

    .line 66
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME_FREQUENCY:I

    .line 67
    const v0, 0x1adb0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME_NOISE:I

    .line 68
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME_IMPEDANCE:I

    .line 76
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg:F

    .line 77
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg:F

    .line 78
    iput v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg:F

    .line 80
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData:I

    .line 81
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData:I

    .line 82
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData:I

    .line 84
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData_mV:F

    .line 85
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData_mV:F

    .line 86
    iput v4, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData_mV:F

    .line 88
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startTimeSeconds:J

    .line 89
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_ecg_delta:Z

    .line 90
    iput-boolean v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_heart_rate:Z

    .line 91
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->ecgProccessingCount:I

    .line 105
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 106
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .line 107
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 108
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 109
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .line 110
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonAlertExtended:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;

    .line 112
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    .line 114
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    .line 115
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    .line 116
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    .line 118
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    .line 119
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 121
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 122
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    .line 131
    new-instance v0, Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-direct {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    iput-object v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 132
    return-void
.end method

.method private getDeviceInformation()I
    .locals 4

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 426
    .local v0, "nRtn":I
    new-instance v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;-><init>([B)V

    iput-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 460
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "test_ts - mShapCommonProductInfo = null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    if-nez v1, :cond_1

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "test_ts - mShapExtMsgComonProductInfomation = null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "test_ts - mShapExtMsgCommonRawDataFormat = null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    :cond_2
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    if-nez v1, :cond_3

    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "test_ts - mShapMsgCommonExtraList = null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_3
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDeviceInformation nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    return v0
.end method

.method private static getPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "mode"    # Ljava/lang/String;
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 690
    const-string v1, "/mnt/sdcard/log/ECG/0/"

    .line 691
    .local v1, "dirPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v2, "/mnt/sdcard/log/ECG/0/"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 692
    .local v0, "dirForFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 693
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 695
    :cond_0
    const-string v2, "RAW"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 696
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/mnt/sdcard/log/ECG/0/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_ecg_rawData.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 700
    :goto_0
    return-object v2

    .line 697
    :cond_1
    const-string v2, "RAW_To_mV"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 698
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/mnt/sdcard/log/ECG/0/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_ecg_rawData_mV.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 700
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/mnt/sdcard/log/ECG/0/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_ecg_filtered.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private initShapMessageVariables()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 416
    sget-object v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v1, "initShapMessageVariables"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .line 418
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 419
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 420
    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .line 421
    return-void
.end method

.method private makeEcgBundle(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;)Landroid/os/Bundle;
    .locals 3
    .param p1, "healthcover"    # Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;

    .prologue
    .line 679
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "makeEcgBundle"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 682
    .local v0, "bundleRtn":Landroid/os/Bundle;
    const-string v1, "SampleFreq"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getFs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 683
    const-string v1, "CoefA"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getCoefA()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 684
    const-string v1, "CoefB"

    invoke-virtual {p1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;->getCoefB()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 685
    return-object v0
.end method

.method public static printHex([B)V
    .locals 7
    .param p0, "arrData"    # [B

    .prologue
    .line 1065
    const-string v1, ""

    .line 1066
    .local v1, "strT":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 1068
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " %02x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aget-byte v6, p0, v0

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1069
    add-int/lit8 v2, v0, 0x1

    rem-int/lit8 v2, v2, 0x8

    if-nez v2, :cond_0

    .line 1071
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    const-string v1, ""

    .line 1066
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1076
    :cond_1
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    return-void
.end method

.method private processBiaRawData([I)I
    .locals 1
    .param p1, "arrRawData"    # [I

    .prologue
    .line 863
    const/4 v0, -0x1

    .line 901
    .local v0, "nRtn":I
    return v0
.end method

.method private processBodyCompositionAnalyzerMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I
    .locals 1
    .param p1, "shapMsgHeader"    # Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    .prologue
    .line 627
    const/4 v0, 0x0

    .line 629
    .local v0, "nRtn":I
    return v0
.end method

.method private processEcgRawData([I)I
    .locals 26
    .param p1, "arrRawData"    # [I

    .prologue
    .line 750
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->ecgProccessingCount:I

    move/from16 v21, v0

    if-nez v21, :cond_0

    .line 751
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startTimeSeconds:J

    .line 752
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->ecgProccessingCount:I

    .line 755
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 756
    .local v6, "currentTimeSeconds":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startTimeSeconds:J

    move-wide/from16 v22, v0

    sub-long v16, v6, v22

    .line 757
    .local v16, "flowedSeconds":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "processEcgRawData / flowedSeconds : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->processEcgData([I)I

    move-result v20

    .line 761
    .local v20, "nRtn":I
    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_1

    const/16 v21, 0xff

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 762
    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 764
    .local v5, "ecgData":Landroid/os/Bundle;
    const/high16 v14, 0x3f800000    # 1.0f

    .line 765
    .local v14, "fCoefA":F
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    .line 766
    .local v19, "nLength":I
    const/4 v13, 0x0

    .line 767
    .local v13, "fArrFiltered":[F
    if-lez v19, :cond_2

    move/from16 v0, v19

    new-array v13, v0, [F

    .line 768
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getCoefA()F

    move-result v21

    const/high16 v22, 0x447a0000    # 1000.0f

    mul-float v14, v21, v22

    .line 770
    move/from16 v0, v19

    new-array v4, v0, [F

    .line 773
    .local v4, "arrRawData_to_mV":[F
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move/from16 v0, v19

    if-ge v15, v0, :cond_9

    .line 779
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mEcgHPF:Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;

    move-object/from16 v21, v0

    aget v22, p1, v15

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v21 .. v23}, Lcom/sec/dmc/hsl/android/healthcover/Df2SosFilter;->filter(D)D

    move-result-wide v10

    .line 781
    .local v10, "dbVal":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mFirLpf:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;

    move-object/from16 v21, v0

    double-to-float v0, v10

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg$FIR_LPF_500Hz;->filter(F)F

    move-result v21

    mul-float v21, v21, v14

    aput v21, v13, v15

    .line 785
    aget v21, p1, v15

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v14

    aput v21, v4, v15

    .line 787
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    cmp-long v21, v16, v22

    if-ltz v21, :cond_8

    .line 788
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg:F

    move/from16 v21, v0

    aget v22, v13, v15

    cmpl-float v21, v21, v22

    if-lez v21, :cond_3

    aget v21, v13, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg:F

    .line 789
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg:F

    move/from16 v21, v0

    aget v22, v13, v15

    cmpg-float v21, v21, v22

    if-gez v21, :cond_4

    aget v21, v13, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg:F

    .line 791
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData:I

    move/from16 v21, v0

    aget v22, p1, v15

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_5

    aget v21, p1, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData:I

    .line 792
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData:I

    move/from16 v21, v0

    aget v22, p1, v15

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    aget v21, p1, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData:I

    .line 794
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData_mV:F

    move/from16 v21, v0

    aget v22, v4, v15

    cmpl-float v21, v21, v22

    if-lez v21, :cond_7

    aget v21, v4, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData_mV:F

    .line 795
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData_mV:F

    move/from16 v21, v0

    aget v22, v4, v15

    cmpg-float v21, v21, v22

    if-gez v21, :cond_8

    aget v21, v4, v15

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData_mV:F

    .line 773
    :cond_8
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 799
    .end local v10    # "dbVal":D
    :cond_9
    const-string v21, "FILTERED"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->time:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v13, v1, v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->writeToFile([FLjava/lang/String;Ljava/lang/String;)V

    .line 801
    const-string v21, "RAW_To_mV"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->time:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v0, v4, v1, v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->writeToFile([FLjava/lang/String;Ljava/lang/String;)V

    .line 805
    new-instance v18, Landroid/content/Intent;

    const-string v21, "com.sec.factory.app.factorytest.ECG_ALL_DATA"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 806
    .local v18, "intent":Landroid/content/Intent;
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 807
    .local v12, "ecg_bundle":Landroid/os/Bundle;
    const-string v21, "ecg_float"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v13}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 808
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 809
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 810
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "PROTOCOL : ecg_bundle have been sent!"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    cmp-long v21, v16, v22

    if-ltz v21, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_ecg_delta:Z

    move/from16 v21, v0

    if-nez v21, :cond_a

    .line 814
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg:F

    .line 815
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData:I

    .line 816
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData_mV:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData_mV:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData_mV:F

    .line 818
    new-instance v15, Landroid/content/Intent;

    .end local v15    # "i":I
    const-string v21, "com.sec.factory.app.factorytest.ECG_DATA"

    move-object/from16 v0, v21

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 819
    .local v15, "i":Landroid/content/Intent;
    const-string v21, "ecg_delta"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg:F

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 820
    const-string v21, "ecg_delta_rawData"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 821
    const-string v21, "ecg_delta_rawData_mV"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData_mV:F

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 824
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sent_ecg_delta:Z

    .line 827
    .end local v15    # "i":Landroid/content/Intent;
    :cond_a
    const-string v21, "ecg_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v5, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 828
    const-string v21, "ecg_electrowave"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0, v13}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->mPacketCount:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getFs()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_c

    .line 832
    const-string v21, "ecg_heartrate"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 833
    const v21, 0x7fffffff

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 835
    const/16 v21, 0xff

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 836
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->getMeanHR()D

    move-result-wide v8

    .line 837
    .local v8, "dbMeanHR":D
    const-wide/high16 v22, 0x4039000000000000L    # 25.0

    cmpl-double v21, v8, v22

    if-ltz v21, :cond_b

    const-wide v22, 0x406c200000000000L    # 225.0

    cmpg-double v21, v8, v22

    if-gtz v21, :cond_b

    .line 838
    sget-object v21, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v22, "MeanHR: %.1f"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    const-string v21, "ecg_heartrate"

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 842
    .end local v8    # "dbMeanHR":D
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    move/from16 v21, v0

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    move-object/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v0, v5, v1}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataReceived(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 849
    .end local v4    # "arrRawData_to_mV":[F
    .end local v5    # "ecgData":Landroid/os/Bundle;
    .end local v12    # "ecg_bundle":Landroid/os/Bundle;
    .end local v13    # "fArrFiltered":[F
    .end local v14    # "fCoefA":F
    .end local v18    # "intent":Landroid/content/Intent;
    .end local v19    # "nLength":I
    :cond_c
    :goto_1
    const/16 v21, 0xff

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_f

    .line 857
    :cond_d
    :goto_2
    return v20

    .line 845
    .restart local v4    # "arrRawData_to_mV":[F
    .restart local v5    # "ecgData":Landroid/os/Bundle;
    .restart local v12    # "ecg_bundle":Landroid/os/Bundle;
    .restart local v13    # "fArrFiltered":[F
    .restart local v14    # "fCoefA":F
    .restart local v18    # "intent":Landroid/content/Intent;
    .restart local v19    # "nLength":I
    :cond_e
    sget-object v21, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v22, "onDataStoppped is already called"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 851
    .end local v4    # "arrRawData_to_mV":[F
    .end local v5    # "ecgData":Landroid/os/Bundle;
    .end local v12    # "ecg_bundle":Landroid/os/Bundle;
    .end local v13    # "fArrFiltered":[F
    .end local v14    # "fCoefA":F
    .end local v18    # "intent":Landroid/content/Intent;
    .end local v19    # "nLength":I
    :cond_f
    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_d

    goto :goto_2
.end method

.method private processHeartRateMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I
    .locals 14
    .param p1, "shapMsgHeader"    # Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    .prologue
    .line 582
    const/4 v7, 0x0

    .line 583
    .local v7, "nRtn":I
    const/16 v9, 0x50

    iget-byte v10, p1, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_3

    .line 584
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 585
    .local v0, "currentTimeSeconds":J
    iget-wide v10, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startTimeSeconds:J

    sub-long v4, v0, v10

    .local v4, "flowedSeconds":J
    move-object v8, p1

    .line 587
    check-cast v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;

    .line 588
    .local v8, "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v10, "HR: %d, status: %02x"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-short v13, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    invoke-static {v13}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-short v13, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mStatus:S

    invoke-static {v13}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    iget-short v9, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mStatus:S

    and-int/lit8 v9, v9, 0x10

    int-to-byte v9, v9

    const/16 v10, 0x10

    if-ne v9, v10, :cond_2

    .line 591
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v10, "Non-contact detected..."

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 593
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v9, :cond_0

    .line 595
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    iget v10, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStopped(II)V

    .line 596
    const/4 v9, 0x0

    iput v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 598
    :cond_0
    new-instance v6, Landroid/content/Intent;

    const-string v9, "com.sec.factory.app.factorytest.DEVICE_DETACHED"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 599
    .local v6, "i":Landroid/content/Intent;
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 621
    .end local v0    # "currentTimeSeconds":J
    .end local v4    # "flowedSeconds":J
    .end local v6    # "i":Landroid/content/Intent;
    .end local v8    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_1
    :goto_0
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processHeartRateMessage nRtn : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    return v7

    .line 600
    .restart local v0    # "currentTimeSeconds":J
    .restart local v4    # "flowedSeconds":J
    .restart local v8    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_2
    iget-short v9, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    const/16 v10, 0x19

    if-lt v9, v10, :cond_1

    iget-short v9, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    const/16 v10, 0xe1

    if-gt v9, v10, :cond_1

    .line 601
    iget-short v9, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    iput v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    .line 602
    const-wide v10, 0x40ed4c0000000000L    # 60000.0

    iget-short v9, v8, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;->mMeanHR:S

    int-to-double v12, v9

    div-double v2, v10, v12

    .line 603
    .local v2, "dbRRI":D
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "processHeartRateMessage : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    new-instance v6, Landroid/content/Intent;

    const-string v9, "com.sec.factory.app.factorytest.HEART_RATE"

    invoke-direct {v6, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 607
    .restart local v6    # "i":Landroid/content/Intent;
    const-string v9, "heart_rate"

    iget v10, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mReceivedHeartRate:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 608
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 611
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    invoke-virtual {v9, v2, v3}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;->addRRI(D)I

    goto :goto_0

    .line 615
    .end local v0    # "currentTimeSeconds":J
    .end local v2    # "dbRRI":D
    .end local v4    # "flowedSeconds":J
    .end local v6    # "i":Landroid/content/Intent;
    .end local v8    # "shapHrmHeartRate":Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage$ShapMsgHrmHeartRate;
    :cond_3
    const/4 v9, 0x3

    iget-byte v10, p1, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_4

    .line 616
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v10, "Heart Rate Monitor Settings Message...."

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 618
    :cond_4
    const/4 v7, 0x2

    goto/16 :goto_0
.end method

.method private processMessage([B)I
    .locals 14
    .param p1, "arrData"    # [B

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 498
    .local v1, "bRetryCnt":Z
    const/4 v5, 0x0

    .line 499
    .local v5, "nRtn":I
    const/4 v0, 0x0

    .line 500
    .local v0, "arrMessage":[B
    const/4 v4, 0x0

    .line 503
    .local v4, "nReceiveBufSize":I
    if-eqz p1, :cond_0

    array-length v9, p1

    if-lez v9, :cond_0

    .line 504
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9, p1}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->addDataToBuffer([B)I

    move-result v4

    .line 510
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 511
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getReceivedBufferSize()I

    move-result v4

    .line 512
    const/16 v9, 0x14

    if-lt v4, v9, :cond_1

    .line 513
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-virtual {v9}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->getMessage()[B

    move-result-object v0

    .line 515
    :cond_1
    if-nez v0, :cond_2

    move v6, v5

    .line 576
    .end local v5    # "nRtn":I
    .local v6, "nRtn":I
    :goto_1
    return v6

    .line 518
    .end local v6    # "nRtn":I
    .restart local v5    # "nRtn":I
    :cond_2
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->parseCommonMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v7

    .line 519
    .local v7, "shapMsgHeader":Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;
    if-nez v7, :cond_4

    .line 521
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Device Specific Message..."

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    const/4 v5, 0x2

    .line 523
    iget v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    const/4 v10, 0x4

    if-ne v9, v10, :cond_3

    .line 524
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapHeartRateMonitorMessage;->parseMessage([B)Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;

    move-result-object v7

    .line 525
    if-eqz v7, :cond_3

    .line 526
    invoke-direct {p0, v7}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processHeartRateMessage(Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;)I

    move-result v5

    .line 572
    :cond_3
    :goto_2
    if-eqz v1, :cond_d

    move v6, v5

    .line 576
    .end local v5    # "nRtn":I
    .restart local v6    # "nRtn":I
    goto :goto_1

    .line 534
    .end local v6    # "nRtn":I
    .restart local v5    # "nRtn":I
    :cond_4
    const/4 v9, 0x1

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_5

    move-object v9, v7

    .line 535
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommonProductInfo:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonProductInfo;

    .line 536
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "SHAP_MSG_COMMON_PRODUCT_INFO"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 537
    :cond_5
    const/16 v9, 0x30

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_6

    move-object v9, v7

    .line 538
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    .line 539
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->setDeviceInformation()I

    .line 540
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "SHAP_EXTMSG_COMMON_PRODUCT_INFO"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 541
    :cond_6
    const/16 v9, 0x31

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_7

    move-object v9, v7

    .line 542
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    .line 543
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "SHAP_EXTMSG_COMMON_RAWDATA_FORMAT"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 544
    :cond_7
    const/16 v9, 0xa

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_a

    move-object v2, v7

    .line 545
    check-cast v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;

    .line 546
    .local v2, "msgCmdResp":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/16 v10, 0xa

    if-lt v9, v10, :cond_8

    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 547
    :cond_8
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mRespondList:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 548
    const-string v10, "Invoke ID: %02x, Respond:%s(%04x)"

    const/4 v9, 0x3

    new-array v11, v9, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-byte v12, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mInvokeID:B

    invoke-static {v12}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v12

    aput-object v12, v11, v9

    const/4 v12, 0x1

    iget-short v9, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mRespond:S

    if-nez v9, :cond_9

    const-string v9, "OK"

    :goto_3
    aput-object v9, v11, v12

    const/4 v9, 0x2

    iget-short v12, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;->mRespond:S

    invoke-static {v12}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 550
    .local v8, "strLog":Ljava/lang/String;
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    invoke-static {v9, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 548
    .end local v8    # "strLog":Ljava/lang/String;
    :cond_9
    const-string v9, "Failed"

    goto :goto_3

    .line 551
    .end local v2    # "msgCmdResp":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonRespond;
    :cond_a
    const/16 v9, 0xc

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_b

    move-object v9, v7

    .line 552
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapMsgCommonExtraList:Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonExtraList;

    .line 553
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "SHAP_MSG_COMMON_EXTRA_LIST"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 554
    :cond_b
    const/16 v9, 0x35

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-eq v9, v10, :cond_3

    .line 556
    const/16 v9, 0x32

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_c

    .line 557
    iget-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "SHAP_EXTMSG_COMMON_RAW_DATA"

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v7

    .line 558
    check-cast v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;

    .line 559
    .local v3, "msgRawData":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;
    invoke-direct {p0, v3}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processRawData(Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;)I

    move-result v5

    goto/16 :goto_2

    .line 560
    .end local v3    # "msgRawData":Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;
    :cond_c
    const/16 v9, 0x33

    iget-byte v10, v7, Lcom/sec/dmc/hsl/android/shap/ShapCommon$ShapMsgHeader;->mMessageID:B

    if-ne v9, v10, :cond_3

    move-object v9, v7

    .line 561
    check-cast v9, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;

    iput-object v9, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonAlertExtended:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;

    .line 562
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v10, "Alert Msg: %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonAlertExtended:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;

    iget-object v13, v13, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonAlertExtended;->mAlertMessage:Ljava/lang/String;

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 573
    :cond_d
    const/4 v1, 0x1

    .line 574
    goto/16 :goto_0
.end method

.method private processRawData(Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;)I
    .locals 10
    .param p1, "msgRawData"    # Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 635
    const/4 v2, 0x0

    .line 636
    .local v2, "nErrorCode":I
    const/4 v0, 0x0

    .line 637
    .local v0, "arrRawData":[I
    const/4 v4, 0x0

    .line 639
    .local v4, "nRtn":I
    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-nez v7, :cond_0

    .line 674
    :goto_0
    return v6

    .line 640
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-short v7, p1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mNumberOfChannel:S

    if-ge v1, v7, :cond_2

    .line 643
    const/4 v3, 0x0

    .line 644
    .local v3, "nRawType":S
    iget-object v7, p1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->mChannelID:[B

    aget-byte v7, v7, v1

    invoke-virtual {p1, v7}, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawData;->convertRawDataToInt32(I)[I

    move-result-object v0

    .line 649
    if-eqz v0, :cond_1

    .line 650
    invoke-direct {p0, v0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processEcgRawData([I)I

    move-result v4

    .line 640
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 654
    .end local v3    # "nRawType":S
    :cond_2
    packed-switch v4, :pswitch_data_0

    :cond_3
    :goto_2
    move v6, v2

    .line 674
    goto :goto_0

    .line 658
    :pswitch_0
    sget-object v7, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "processRawData nRtn : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    iget v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    if-ne v7, v6, :cond_3

    .line 661
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 662
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 663
    iget-object v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v7, :cond_3

    .line 664
    const/16 v7, 0xff

    if-ne v4, v7, :cond_4

    move v2, v5

    .line 665
    :goto_3
    iget-object v6, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    iget v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v6, v7, v2}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStopped(II)V

    .line 666
    iput v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    goto :goto_2

    :cond_4
    move v2, v6

    .line 664
    goto :goto_3

    .line 654
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private sendData([B)I
    .locals 4
    .param p1, "arrMessage"    # [B

    .prologue
    .line 405
    const/4 v0, 0x1

    .line 406
    .local v0, "nRtn":I
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    if-eqz v1, :cond_0

    .line 407
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->sendRawData([B)I

    move-result v0

    .line 409
    :cond_0
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendData nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    return v0
.end method

.method private sendDataRequestMessage(BBBBS)V
    .locals 6
    .param p1, "cInvokeID"    # B
    .param p2, "cReqID"    # B
    .param p3, "cFlag"    # B
    .param p4, "cReqType"    # B
    .param p5, "sSupp2"    # S

    .prologue
    const/4 v5, 0x0

    .line 989
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendDataRequestMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 991
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 992
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 993
    iput-byte p1, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 994
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 995
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 997
    iput-byte p4, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 998
    iput-byte p2, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 999
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    .line 1000
    const/4 v3, -0x1

    iput-short v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 1001
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 1002
    iput-short p5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 1004
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 1005
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 1006
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 1007
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 1008
    return-void
.end method

.method private sendExtraTypeListRequestMessage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1012
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendExtraTypeListRequestMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 1016
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 1017
    const/4 v3, 0x6

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 1018
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 1019
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 1021
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 1022
    const/16 v3, 0xc

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 1023
    const/4 v3, 0x1

    iput-short v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 1024
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 1025
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 1026
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 1028
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 1029
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 1030
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 1031
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 1032
    return-void
.end method

.method private sendProductInfoRequest(Z)V
    .locals 8
    .param p1, "bExtended"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 907
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendProductInfoRequest bExtended : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 910
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 911
    iput-byte v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 912
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 913
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 915
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 917
    if-eqz p1, :cond_0

    .line 918
    const/16 v3, 0x30

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 922
    :goto_0
    iput-short v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 923
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 924
    iput-byte v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestFlag:B

    .line 925
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 926
    iput-short v6, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 928
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 929
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 930
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 931
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 932
    return-void

    .line 920
    .end local v0    # "arrMsg":[B
    .end local v1    # "arrPacket":[B
    :cond_0
    iput-byte v7, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    goto :goto_0
.end method

.method private sendRealtimeDataFormatRequest()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 936
    sget-object v3, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v4, "sendRealtimeDataFormatRequest"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;-><init>([B)V

    .line 939
    .local v2, "shapDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageType:B

    .line 940
    const/4 v3, 0x5

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mInvokeID:B

    .line 941
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageID:B

    .line 942
    const/16 v3, -0x80

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mMessageFlag:B

    .line 943
    iput-byte v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestType:B

    .line 944
    const/16 v3, 0x31

    iput-byte v3, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mRequestMessageID:B

    .line 945
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionMode:S

    .line 946
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mTransmissionInterval:S

    .line 947
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement1:S

    .line 948
    iput-short v5, v2, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->mSupplement2:S

    .line 950
    invoke-virtual {v2}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapMsgCommonDataRequest;->makeMessage()[B

    move-result-object v0

    .line 951
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 952
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 953
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 954
    return-void
.end method

.method private sendStopAllAction()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1036
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "sendStopAllAction"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    new-instance v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;-><init>([B)V

    .line 1039
    .local v4, "shapChangeStatus":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;
    iput-byte v7, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageType:B

    .line 1040
    const/16 v5, 0xa

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mInvokeID:B

    .line 1041
    const/16 v5, 0x21

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageID:B

    .line 1042
    const/16 v5, -0x80

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageFlag:B

    .line 1043
    const/4 v5, -0x1

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mPowerMode:B

    .line 1044
    iput-byte v7, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mStatus:B

    .line 1046
    invoke-virtual {v4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->makeMessage()[B

    move-result-object v0

    .line 1047
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 1048
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 1049
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    move-result v3

    .line 1050
    .local v3, "nRtn":I
    if-eqz v3, :cond_0

    .line 1053
    const-wide/16 v6, 0xc8

    const/4 v5, 0x0

    :try_start_0
    invoke-static {v6, v7, v5}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1058
    :goto_0
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "Re-sending..."

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    :cond_0
    return-void

    .line 1055
    :catch_0
    move-exception v2

    .line 1056
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private setDeviceInformation()I
    .locals 4

    .prologue
    .line 472
    const/4 v0, 0x0

    .line 474
    .local v0, "nRtn":I
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    if-eqz v1, :cond_2

    .line 476
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v1, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSN:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 480
    :cond_0
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v1, v1, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturer:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 484
    :cond_1
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDeviceInformation ProductSN : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v3, v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mProductSN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Manufacturer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgComonProductInfomation:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;

    iget-object v3, v3, Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgComonProductInfomation;->mManufacturer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_2
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDeviceInformation nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    return v0
.end method

.method private setUserInformation(Landroid/os/Bundle;)I
    .locals 8
    .param p1, "parameters"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 287
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "setUserInformation"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/4 v4, 0x0

    .line 291
    .local v4, "nRtn":I
    const-string v5, "IsMan"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    sget-object v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->MALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    .line 292
    .local v2, "gender":Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    :goto_0
    const-string v5, "Age"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 293
    .local v3, "nAge":I
    const-string v5, "Height"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    .line 294
    .local v0, "fHeight":F
    const-string v5, "Weight"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v1

    .line 296
    .local v1, "fWeight":F
    cmpl-float v5, v0, v7

    if-nez v5, :cond_0

    .line 297
    const-string v5, "Height"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v0, v5

    .line 298
    :cond_0
    cmpl-float v5, v1, v7

    if-nez v5, :cond_1

    .line 299
    const-string v5, "Weight"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    int-to-float v1, v5

    .line 300
    :cond_1
    new-instance v5, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    invoke-direct {v5, v2, v3, v0, v1}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;-><init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;IFF)V

    iput-object v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    .line 301
    return v4

    .line 291
    .end local v0    # "fHeight":F
    .end local v1    # "fWeight":F
    .end local v2    # "gender":Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;
    .end local v3    # "nAge":I
    :cond_2
    sget-object v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;->FEMALE:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo$Gender;

    goto :goto_0
.end method

.method private startBodyFatMeasurement()I
    .locals 4

    .prologue
    .line 397
    const/4 v0, 0x1

    .line 398
    .local v0, "nRtn":I
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startMeasurement(S)I

    move-result v0

    .line 399
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startBodyFatMeasurement nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 400
    return v0
.end method

.method private startMeasurement(S)I
    .locals 19
    .param p1, "nDevType"    # S

    .prologue
    .line 306
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startMeasurement nDevType : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/16 v16, 0x1

    .line 309
    .local v16, "nRtn":I
    const/16 v14, 0xff

    .line 311
    .local v14, "nDeviceExtraId":I
    const/4 v15, 0x3

    .line 312
    .local v15, "nInvokeId":I
    const/4 v4, 0x0

    .line 313
    .local v4, "cMsgId":B
    const/4 v6, 0x0

    .line 314
    .local v6, "cReqType":B
    const/16 v7, 0xff

    .line 316
    .local v7, "sSupp2":S
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I

    .line 318
    packed-switch p1, :pswitch_data_0

    .line 341
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "startMeasurement : default"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move/from16 v17, v16

    .line 384
    .end local v16    # "nRtn":I
    .local v17, "nRtn":I
    :goto_0
    return v17

    .line 320
    .end local v17    # "nRtn":I
    .restart local v16    # "nRtn":I
    :pswitch_0
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "startMeasurement : SHAP_DEV_HEART_RATE_MONITOR"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/16 v4, 0x50

    .line 322
    const/16 v18, 0x0

    .line 323
    .local v18, "sWaveType":S
    const/16 v6, 0x40

    .line 324
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-eqz v2, :cond_1

    .line 347
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "startMeasurement : mShapExtMsgCommonRawDataFormat != null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const/4 v13, 0x0

    .line 351
    .local v13, "nChnId":I
    if-ltz v13, :cond_3

    .line 353
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "startMeasurement : nChnId>=0"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    const/high16 v12, 0x43fa0000    # 500.0f

    .line 355
    .local v12, "fFs":F
    const v10, 0x334e81b5

    .line 356
    .local v10, "fCoefA":F
    const/4 v11, 0x0

    .line 358
    .local v11, "fCoefB":F
    const/4 v2, 0x6

    move/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 359
    new-instance v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverUserInfo:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;

    float-to-int v5, v12

    invoke-direct {v2, v3, v5, v10, v11}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;-><init>(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia$HealthCoverUserInfo;IFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverBia:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBia;

    .line 366
    :cond_0
    :goto_1
    const/4 v2, 0x1

    new-array v8, v2, [B

    .line 367
    .local v8, "arrChnId":[B
    const/4 v2, 0x0

    int-to-byte v3, v13

    aput-byte v3, v8, v2

    .line 368
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendRealtimeDataRequest([B)V

    .line 369
    const-wide/16 v2, 0x14

    const/4 v5, 0x0

    :try_start_0
    invoke-static {v2, v3, v5}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :goto_2
    int-to-byte v3, v15

    const/16 v5, -0x80

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendDataRequestMessage(BBBBS)V

    .line 376
    const/16 v16, 0x0

    .line 377
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveStatus:I

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDataTranceiveType:I

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;->onDataStarted(I)V

    .end local v8    # "arrChnId":[B
    .end local v10    # "fCoefA":F
    .end local v11    # "fCoefB":F
    .end local v12    # "fFs":F
    .end local v13    # "nChnId":I
    :cond_1
    :goto_3
    move/from16 v17, v16

    .line 384
    .end local v16    # "nRtn":I
    .restart local v17    # "nRtn":I
    goto :goto_0

    .line 360
    .end local v17    # "nRtn":I
    .restart local v10    # "fCoefA":F
    .restart local v11    # "fCoefB":F
    .restart local v12    # "fFs":F
    .restart local v13    # "nChnId":I
    .restart local v16    # "nRtn":I
    :cond_2
    const/4 v2, 0x3

    move/from16 v0, p1

    if-ne v0, v2, :cond_0

    .line 362
    new-instance v2, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    float-to-int v3, v12

    invoke-direct {v2, v3, v10, v11}, Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;-><init>(IFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    .line 363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mHealthCoverEcg:Lcom/sec/dmc/hsl/android/healthcover/HealthCoverEcg;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->makeEcgBundle(Lcom/sec/dmc/hsl/android/healthcover/HealthCoverBase;)Landroid/os/Bundle;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mBundleEcg:Landroid/os/Bundle;

    goto :goto_1

    .line 369
    .restart local v8    # "arrChnId":[B
    :catch_0
    move-exception v9

    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 382
    .end local v8    # "arrChnId":[B
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v10    # "fCoefA":F
    .end local v11    # "fCoefB":F
    .end local v12    # "fFs":F
    :cond_3
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "onDataStarted will not be called."

    invoke-static {v2, v3}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method private writeToFile([FLjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "results"    # [F
    .param p2, "mode"    # Ljava/lang/String;
    .param p3, "time"    # Ljava/lang/String;

    .prologue
    .line 704
    const/4 v2, 0x0

    .line 705
    .local v2, "file":Ljava/io/File;
    const-string v8, ""

    .line 706
    .local v8, "stringadd":Ljava/lang/String;
    invoke-static {p2, p3}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 707
    .local v6, "path":Ljava/lang/String;
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "writeToFile path : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    move-object v0, p1

    .local v0, "arr$":[F
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget v7, v0, v4

    .line 711
    .local v7, "pulse":F
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 709
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 714
    .end local v7    # "pulse":F
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 715
    .end local v2    # "file":Ljava/io/File;
    .local v3, "file":Ljava/io/File;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    .line 716
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 718
    :cond_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v9, Ljava/io/OutputStreamWriter;

    new-instance v10, Ljava/io/FileOutputStream;

    const/4 v11, 0x1

    invoke-direct {v10, v3, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v9, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v9}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 719
    .local v1, "bufferedWriter":Ljava/io/BufferedWriter;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "line.separator"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 720
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 721
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 723
    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :goto_1
    return-void

    .line 722
    :catch_0
    move-exception v9

    goto :goto_1

    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catch_1
    move-exception v9

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_1
.end method

.method private writeToFile([ILjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "results"    # [I
    .param p2, "mode"    # Ljava/lang/String;
    .param p3, "time"    # Ljava/lang/String;

    .prologue
    .line 727
    const/4 v2, 0x0

    .line 728
    .local v2, "file":Ljava/io/File;
    const-string v8, ""

    .line 729
    .local v8, "stringadd":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p2, p3}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 730
    .local v6, "path":Ljava/lang/String;
    sget-object v9, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "writeToFile path : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget v9, v0, v4

    int-to-float v7, v9

    .line 734
    .local v7, "pulse":F
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 732
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 737
    .end local v7    # "pulse":F
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 738
    .end local v2    # "file":Ljava/io/File;
    .local v3, "file":Ljava/io/File;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    .line 739
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 741
    :cond_1
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v9, Ljava/io/OutputStreamWriter;

    new-instance v10, Ljava/io/FileOutputStream;

    const/4 v11, 0x1

    invoke-direct {v10, v3, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v9, v10}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v9}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 742
    .local v1, "bufferedWriter":Ljava/io/BufferedWriter;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "line.separator"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 743
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 744
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 746
    .end local v1    # "bufferedWriter":Ljava/io/BufferedWriter;
    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :goto_1
    return-void

    .line 745
    :catch_0
    move-exception v9

    goto :goto_1

    .end local v2    # "file":Ljava/io/File;
    .restart local v3    # "file":Ljava/io/File;
    :catch_1
    move-exception v9

    move-object v2, v3

    .end local v3    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    goto :goto_1
.end method


# virtual methods
.method public init_ecgValues()V
    .locals 4

    .prologue
    const/high16 v3, 0x447a0000    # 1000.0f

    const/4 v2, 0x0

    const/high16 v1, -0x3b860000    # -1000.0f

    .line 147
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg:F

    .line 148
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg:F

    .line 149
    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg:F

    .line 151
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData:I

    .line 152
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData:I

    .line 153
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData:I

    .line 155
    iput v3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->min_ecg_rawData_mV:F

    .line 156
    iput v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->max_ecg_rawData_mV:F

    .line 157
    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->delta_ecg_rawData_mV:F

    .line 158
    return-void
.end method

.method public initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Object;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;
    .param p3, "device"    # Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    .line 138
    iput-object p2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mListener:Lcom/samsung/android/sdk/health/sensor/protocol/PrivilegeSensorProtocolListener;

    .line 139
    iput-object p3, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->init_ecgValues()V

    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public notifyRawDataReceived([B)I
    .locals 3
    .param p1, "arrData"    # [B

    .prologue
    .line 161
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "notifyRawDataReceived"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "nRtn":I
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->processMessage([B)I

    move-result v0

    .line 165
    return v0
.end method

.method public notifyStart(Ljava/lang/String;)I
    .locals 8
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x2710

    const/4 v6, 0x0

    .line 169
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->time:Ljava/lang/String;

    .line 171
    const-string v2, "D_range"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 172
    const/16 v2, 0x1b58

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    .line 173
    const/16 v2, 0x3e8

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME:I

    .line 184
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "notifyStart"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "notifyStart"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WAIT_DELAY = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startTimeSeconds:J

    .line 188
    iput v6, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->ecgProccessingCount:I

    .line 191
    sget-object v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v3, "notifyStart"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initShapMessageVariables()V

    .line 194
    new-instance v2, Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    invoke-direct {v2}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;-><init>()V

    iput-object v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapCommon:Lcom/sec/dmc/hsl/android/shap/ShapCommon;

    .line 195
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I

    move-result v1

    .line 196
    .local v1, "nRtn":I
    if-eqz v1, :cond_1

    .line 198
    const-wide/16 v2, 0x64

    const/4 v4, 0x0

    :try_start_0
    invoke-static {v2, v3, v4}, Ljava/lang/Thread;->sleep(JI)V

    .line 199
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->getDeviceInformation()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 206
    :cond_1
    :goto_1
    return v6

    .line 174
    .end local v1    # "nRtn":I
    :cond_2
    const-string v2, "Frequency"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Self_test"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Graph"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 175
    :cond_3
    const/16 v2, 0xfa0

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    .line 176
    const/16 v2, 0x7d0

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME:I

    goto/16 :goto_0

    .line 177
    :cond_4
    const-string v2, "System noise"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 178
    iput v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    .line 179
    const v2, 0x1adb0

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME:I

    goto/16 :goto_0

    .line 180
    :cond_5
    const-string v2, "Impedance"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    iput v7, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASUREMENT_WAIT_DELAY:I

    .line 182
    const/16 v2, 0x4e20

    iput v2, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->MEASURING_TIME:I

    goto/16 :goto_0

    .line 201
    .restart local v1    # "nRtn":I
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public notifyStop()I
    .locals 4

    .prologue
    .line 210
    sget-object v1, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v2, "notifyStop"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendStopAllAction()V

    .line 212
    invoke-direct {p0}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->initShapMessageVariables()V

    .line 214
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 216
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 218
    const/4 v1, 0x0

    return v1
.end method

.method public sendRealtimeDataRequest([B)V
    .locals 9
    .param p1, "arrChnId"    # [B

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 958
    sget-object v5, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->TAG:Ljava/lang/String;

    const-string v6, "sendRealtimeDataRequest"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    new-instance v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;-><init>([B)V

    .line 961
    .local v4, "shapRawDataRequest":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;
    iput-byte v8, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageType:B

    .line 962
    const/4 v5, 0x2

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mInvokeID:B

    .line 963
    const/4 v5, 0x7

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageID:B

    .line 964
    const/16 v5, -0x80

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mMessageFlag:B

    .line 966
    if-eqz p1, :cond_0

    array-length v5, p1

    if-gtz v5, :cond_2

    .line 968
    :cond_0
    iput-byte v8, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    .line 981
    :cond_1
    invoke-virtual {v4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->makeMessage()[B

    move-result-object v0

    .line 982
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 983
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 984
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 985
    return-void

    .line 972
    .end local v0    # "arrMsg":[B
    .end local v1    # "arrPacket":[B
    :cond_2
    array-length v3, p1

    .line 973
    .local v3, "nNumOfChn":I
    const/4 v5, 0x5

    if-le v3, v5, :cond_3

    const/4 v3, 0x5

    .line 974
    :cond_3
    int-to-byte v5, v3

    iput-byte v5, v4, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->mNumberOfChannel:B

    .line 976
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 978
    aget-byte v5, p1, v2

    invoke-virtual {v4, v2, v5, v7, v7}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCommonRawdataRequest;->setChannelInfo(IBBB)V

    .line 976
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public startEcgMeasurement()I
    .locals 4

    .prologue
    .line 389
    const/4 v0, 0x1

    .line 390
    .local v0, "nRtn":I
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startMeasurement(S)I

    move-result v0

    .line 391
    iget-object v1, p0, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startEcgMeasurement nRtn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    return v0
.end method

.method public startFactoryMode()I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 223
    const/4 v2, 0x0

    .line 225
    .local v2, "nRtn":I
    new-instance v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;-><init>([B)V

    .line 226
    .local v3, "shapChangeStatus":Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;
    iput-byte v5, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageType:B

    .line 227
    const/16 v4, 0x60

    iput-byte v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mInvokeID:B

    .line 228
    const/16 v4, -0x20

    iput-byte v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageID:B

    .line 229
    const/16 v4, -0x80

    iput-byte v4, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mMessageFlag:B

    .line 230
    iput-byte v5, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mPowerMode:B

    .line 231
    iput-byte v5, v3, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->mStatus:B

    .line 233
    invoke-virtual {v3}, Lcom/sec/dmc/hsl/android/shap/ShapStandardMessage$ShapCtlMsgCommonChangeStatus;->makeMessage()[B

    move-result-object v0

    .line 234
    .local v0, "arrMsg":[B
    invoke-static {v0}, Lcom/sec/dmc/hsl/android/shap/ShapCommon;->makeDataPacket([B)[B

    move-result-object v1

    .line 235
    .local v1, "arrPacket":[B
    const/4 v0, 0x0

    .line 236
    invoke-direct {p0, v1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->sendData([B)I

    .line 238
    return v2
.end method
