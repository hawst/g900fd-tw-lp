.class Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;
.super Ljava/lang/Object;
.source "InternalEcgHandler.java"

# interfaces
.implements Lcom/samsung/android/ssensor/SSensorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EcgDataListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;


# direct methods
.method private constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;
    .param p2, "x1"    # Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$1;

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;-><init>(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)V

    return-void
.end method


# virtual methods
.method public onDataReceived([BI)V
    .locals 6
    .param p1, "buffer"    # [B
    .param p2, "size"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 209
    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onDataReceived :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v0, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 211
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->notifyRawDataReceived([B)I

    .line 213
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->mShapExtMsgCommonRawDataFormat:Lcom/sec/dmc/hsl/android/shap/ShapExtendedMessage$ShapExtMsgCommonRawDataFormat;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$300(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # getter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->mProtocol:Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;
    invoke-static {v2}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$200(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;)Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/sensor/protocol/HealthCoverProtocol;->startEcgMeasurement()I

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    # setter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->bListenerRegistered:Z
    invoke-static {v0}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$402(Z)Z

    .line 216
    iget-object v0, p0, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler$EcgDataListener;->this$0:Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;

    # setter for: Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->needToStart:Z
    invoke-static {v0, v1}, Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;->access$302(Lcom/sec/android/service/health/sensor/handler/ecg/InternalEcgHandler;Z)Z

    .line 220
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 215
    goto :goto_0
.end method
