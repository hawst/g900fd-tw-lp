.class public interface abstract Lcom/sec/android/service/health/cp/common/ContentConstants$TrustzoneConstants;
.super Ljava/lang/Object;
.source "ContentConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/service/health/cp/common/ContentConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TrustzoneConstants"
.end annotation


# static fields
.field public static final KEY_NAME:Ljava/lang/String; = "securepasswd"

.field public static final TRUSTZONE_AUTHORITY:Ljava/lang/String; = "com.sec.android.service.health.cp.TrustZoneSecurityProvider"

.field public static final TRUSTZONE_AUTHORITY_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 161
    const-string v0, "content://com.sec.android.service.health.cp.TrustZoneSecurityProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/service/health/cp/common/ContentConstants$TrustzoneConstants;->TRUSTZONE_AUTHORITY_URI:Landroid/net/Uri;

    return-void
.end method
