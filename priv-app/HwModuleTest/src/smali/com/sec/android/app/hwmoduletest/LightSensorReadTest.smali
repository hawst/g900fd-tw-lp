.class public Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "LightSensorReadTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;
    }
.end annotation


# instance fields
.field private final LEVEL_1:I

.field private LEVEL_1_MAX:I

.field private final LEVEL_2:I

.field private LEVEL_2_MAX:I

.field private final LEVEL_3:I

.field private LEVEL_3_MAX:I

.field private final LEVEL_4:I

.field private LEVEL_4_MAX:I

.field private final LEVEL_5:I

.field private final LEVEL_6:I

.field private final LEVEL_7:I

.field private final MSG_UPDATE_LEVEL:B

.field private mAdcText:Landroid/widget/TextView;

.field private mAdcTitle:Landroid/widget/TextView;

.field private mBackButton:Landroid/widget/Button;

.field private mHandler:Landroid/os/Handler;

.field private mIsRGBSensor:Z

.field private mIsReadFromManager:Z

.field private mLevelColor:I

.field private mLevelDownTable:[J

.field private mLevelText:Landroid/widget/TextView;

.field private mLevelUpTable:[J

.field mLightSensor:Landroid/hardware/Sensor;

.field mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

.field private mLuxText:Landroid/widget/TextView;

.field private mReadTargetFileID:Ljava/lang/String;

.field private mRgbwText:Landroid/widget/TextView;

.field mSensorManager:Landroid/hardware/SensorManager;

.field mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 100
    const-string v0, "LightSensorReadTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 30
    iput-byte v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->MSG_UPDATE_LEVEL:B

    .line 40
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    .line 55
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_1_MAX:I

    .line 56
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_2_MAX:I

    .line 57
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_3_MAX:I

    .line 58
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_4_MAX:I

    .line 60
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_1:I

    .line 61
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_2:I

    .line 62
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_3:I

    .line 63
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_4:I

    .line 64
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_5:I

    .line 65
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_6:I

    .line 66
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->LEVEL_7:I

    .line 68
    new-array v0, v4, [J

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    .line 69
    new-array v0, v4, [J

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    .line 72
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mHandler:Landroid/os/Handler;

    .line 101
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLuxText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mRgbwText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)[J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;
    .param p1, "x1"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z

    return v0
.end method

.method private checkFile()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "LIGHT_SENSOR_RAW"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    const-string v2, "read target : LIGHT_SENSOR_RAW"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "LIGHT_SENSOR_RAW"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    .line 203
    :goto_0
    return-void

    .line 191
    :cond_0
    const-string v0, "LIGHT_SENSOR_LUX"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    const-string v2, "read target : LIGHT_SENSOR_LUX"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v0, "LIGHT_SENSOR_LUX"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    goto :goto_0

    .line 195
    :cond_1
    const-string v0, "LIGHT_SENSOR_ADC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    const-string v2, "read target : LIGHT_SENSOR_ADC"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v0, "LIGHT_SENSOR_ADC"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    goto :goto_0

    .line 200
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkFile"

    const-string v2, "no read target"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;

    goto :goto_0
.end method

.method private isReadFromManager()Z
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "isReadFromManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IS_DISPLAY_LUX_ADC : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "IS_DISPLAY_LUX_ADC"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "IS_DISPLAY_LUX_ADC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRgbSensorSupported()Z
    .locals 6

    .prologue
    .line 220
    const/4 v3, 0x1

    .line 221
    .local v3, "isrgbsensor":Z
    const-string v4, "HW_REVISION"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 223
    .local v2, "hwrevision":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 224
    const-string v4, "NO_RGBSENSOR_SUPPORT_REV"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 227
    .local v1, "hwrevString":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "count":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 228
    const-string v4, "-1"

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 230
    :cond_0
    const/4 v3, 0x0

    .line 236
    .end local v0    # "count":I
    .end local v1    # "hwrevString":[Ljava/lang/String;
    :cond_1
    return v3

    .line 227
    .restart local v0    # "count":I
    .restart local v1    # "hwrevString":[Ljava/lang/String;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setLevelRange()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x6

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const-string v2, "LIGHT_SENSOR_UP_LEVEL_1"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v7

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const-string v2, "LIGHT_SENSOR_UP_LEVEL_2"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v8

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const-string v2, "LIGHT_SENSOR_UP_LEVEL_3"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v9

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const-string v2, "LIGHT_SENSOR_UP_LEVEL_4"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v10

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const/4 v2, 0x4

    const-string v3, "LIGHT_SENSOR_UP_LEVEL_5"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const/4 v2, 0x5

    const-string v3, "LIGHT_SENSOR_UP_LEVEL_6"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    const-string v2, "LIGHT_SENSOR_UP_LEVEL_7"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v6

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const-string v2, "LIGHT_SENSOR_DOWN_LEVEL_1"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v7

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const-string v2, "LIGHT_SENSOR_DOWN_LEVEL_2"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v8

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const-string v2, "LIGHT_SENSOR_DOWN_LEVEL_3"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v9

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const-string v2, "LIGHT_SENSOR_DOWN_LEVEL_4"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v10

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const/4 v2, 0x4

    const-string v3, "LIGHT_SENSOR_DOWN_LEVEL_5"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const/4 v2, 0x5

    const-string v3, "LIGHT_SENSOR_DOWN_LEVEL_6"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    aput-wide v4, v1, v2

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    const-string v2, "LIGHT_SENSOR_DOWN_LEVEL_7"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    aput-wide v2, v1, v6

    .line 255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_0

    .line 256
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLevelRange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UP["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J

    aget-wide v4, v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLevelRange"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DOWN["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J

    aget-wide v4, v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x8

    .line 105
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v0, 0x7f03004e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->setContentView(I)V

    .line 108
    const v0, 0x7f0b0156

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLuxText:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0b0157

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcTitle:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0b0158

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcText:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0b015a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelText:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0b0159

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mRgbwText:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0b0072

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mBackButton:Landroid/widget/Button;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mBackButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mTimer:Ljava/util/Timer;

    .line 121
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensor:Landroid/hardware/Sensor;

    .line 123
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;-><init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->start()V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->checkFile()V

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->isReadFromManager()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->isRgbSensorSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsReadFromManager="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIsRGBSensor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z

    if-nez v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :cond_0
    const-string v0, "IS_DISPLAY_LIGHT_SENSOR_ADC_ONLY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z

    if-nez v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mRgbwText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->setLevelRange()V

    .line 145
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->interrupt()V

    .line 170
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 161
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->setEnable(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 164
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 149
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 151
    const-string v1, "IS_DISPLAY_LIGHT_SENSOR_SLOW"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    const/4 v0, 0x3

    .line 155
    .local v0, "mDelay":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensor:Landroid/hardware/Sensor;

    invoke-virtual {v1, v2, v3, v0}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->setEnable(Z)V

    .line 157
    return-void

    .line 154
    .end local v0    # "mDelay":I
    :cond_0
    const/4 v0, 0x2

    .restart local v0    # "mDelay":I
    goto :goto_0
.end method

.method public setBrightness()V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setBrightness"

    const-string v2, "Do nothing"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public setBrightnessMode()V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setBrightnessMode"

    const-string v2, "Set AUTO_BRIGHTNESS_MODE_ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-object v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setScreenBrightnessMode(I)V

    .line 182
    return-void
.end method
