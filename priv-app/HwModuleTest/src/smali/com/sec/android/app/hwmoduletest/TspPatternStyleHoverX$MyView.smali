.class public Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;
.super Landroid/view/View;
.source "TspPatternStyleHoverX.java"

# interfaces
.implements Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mNonClickPaint:Landroid/graphics/Paint;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 263
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    .line 264
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 246
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    .line 247
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    .line 248
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    .line 249
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    .line 265
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->setKeepScreenOn(Z)V

    .line 267
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Screen size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " x "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 271
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 273
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 274
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 276
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "factory"

    const-string v1, "BINARY_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView$1;-><init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->setPaint()V

    .line 288
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->initRect()V

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    .line 290
    return-void
.end method

.method private check10RectRegion(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v11, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    .line 527
    const/4 v6, 0x0

    .line 528
    .local v6, "count10x":I
    const/4 v7, 0x0

    .line 529
    .local v7, "count10y":I
    const/4 v8, 0x0

    .line 530
    .local v8, "startRect10x":I
    const/4 v9, 0x0

    .line 532
    .local v9, "startRect10y":I
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p2, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v7, v0

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    float-to-int v8, v0

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v9, v0

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    sub-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v4, v10

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 540
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, v10

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    aput-boolean v11, v0, v7

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p2, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v7, v0

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v8, v0

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v9, v0

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    sub-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v4, v10

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 555
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, v10

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/2addr v1, v7

    aput-boolean v11, v0, v1

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 564
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    float-to-int v9, v0

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v8, v0

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    sub-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v4, v10

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 570
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, v10

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/2addr v1, v6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v2

    add-int/2addr v1, v2

    aput-boolean v11, v0, v1

    .line 577
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_3

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v9, v0

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v8, v0

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    sub-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    sub-float/2addr v4, v10

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 585
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    add-float/2addr v3, v10

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v4, v10

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw10:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/2addr v1, v6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x2

    aput-boolean v11, v0, v1

    .line 592
    :cond_3
    return-void
.end method

.method private check6RectRegion(FFLandroid/graphics/Paint;)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 595
    const/4 v6, 0x0

    .line 596
    .local v6, "count6x":I
    const/4 v7, 0x0

    .line 597
    .local v7, "count6y":I
    const/4 v8, 0x0

    .line 598
    .local v8, "startRect6x":I
    const/4 v9, 0x0

    .line 600
    .local v9, "startRect6y":I
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p2, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v7, v0

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v8, v0

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v9, v0

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 608
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    const/4 v1, 0x1

    aput-boolean v1, v0, v7

    .line 615
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    cmpg-float v0, p2, v0

    if-gez v0, :cond_1

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    sub-float/2addr v0, v1

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    sub-float v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v9, v0

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v8, v0

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v8, 0x1

    int-to-float v1, v1

    add-int/lit8 v2, v9, 0x1

    int-to-float v2, v2

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 623
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x1

    add-int/lit8 v2, v9, -0x1

    int-to-float v3, v8

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v4

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v5

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->draw6:[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/2addr v1, v6

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    .line 630
    :cond_1
    return-void
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 426
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 428
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 429
    float-to-int v6, p1

    .line 430
    float-to-int v8, p3

    .line 436
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 437
    float-to-int v7, p2

    .line 438
    float-to-int v9, p4

    .line 444
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 445
    return-void

    .line 432
    :cond_0
    float-to-int v6, p3

    .line 433
    float-to-int v8, p1

    goto :goto_0

    .line 440
    :cond_1
    float-to-int v7, p4

    .line 441
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 448
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 449
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 450
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->getIsHoverToolType()Z

    move-result v0

    if-nez v0, :cond_1

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-gtz v0, :cond_2

    cmpg-float v0, p2, v1

    if-ltz v0, :cond_2

    cmpg-float v0, p1, v1

    if-gez v0, :cond_3

    .line 499
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "You are out of bounds!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 503
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->check10RectRegion(FFLandroid/graphics/Paint;)V

    .line 504
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->check6RectRegion(FFLandroid/graphics/Paint;)V

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # invokes: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass10()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # invokes: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isPass6()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # setter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->successTest:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1902(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;Z)Z

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->remoteCall:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$2000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setResult(I)V

    .line 515
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setResult(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->finish()V

    goto :goto_0

    .line 520
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->setResult(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->finish()V

    goto :goto_0
.end method

.method private draw_down(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    .line 383
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    .line 384
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    .line 386
    return-void
.end method

.method private draw_move(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 389
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    if-eqz v1, :cond_1

    .line 390
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 391
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    .line 392
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    .line 393
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    .line 394
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    .line 395
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 396
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawLine(FFFF)V

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 399
    :cond_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    .line 400
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    .line 401
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    .line 402
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    .line 403
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 404
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawLine(FFFF)V

    .line 405
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    .line 407
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private draw_up(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 410
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 411
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    .line 412
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    .line 413
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    .line 414
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    .line 416
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 417
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->drawPoint(FF)V

    .line 420
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->isTouchDown:Z

    .line 422
    :cond_1
    return-void
.end method

.method private initRect()V
    .locals 18

    .prologue
    .line 453
    const/4 v13, 0x0

    .line 454
    .local v13, "ColX":I
    const/4 v14, 0x0

    .line 455
    .local v14, "ColY":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 456
    .local v6, "mRectPaint":Landroid/graphics/Paint;
    new-instance v17, Landroid/graphics/Paint;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Paint;-><init>()V

    .line 457
    .local v17, "mRectPaintCross":Landroid/graphics/Paint;
    const/high16 v1, -0x1000000

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 458
    const/high16 v1, -0x1000000

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 459
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 461
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_10:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-ge v15, v1, :cond_1

    .line 462
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v15

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    float-to-int v2, v2

    add-int v14, v1, v2

    .line 464
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_10:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    move/from16 v0, v16

    if-ge v0, v1, :cond_0

    .line 465
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    move/from16 v0, v16

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    float-to-int v2, v2

    add-int v13, v1, v2

    .line 466
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v14

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 467
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    int-to-float v4, v13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 464
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 461
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 471
    .end local v16    # "j":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 472
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_10:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_10:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_10:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 476
    const/4 v15, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->HEIGHT_BASIS_6:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    if-ge v15, v1, :cond_2

    .line 477
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenWidth:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v13, v1

    .line 478
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v15

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_y_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    float-to-int v2, v2

    add-int v14, v1, v2

    .line 479
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v8, v13

    int-to-float v9, v14

    int-to-float v1, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float v10, v1, v2

    int-to-float v1, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float v11, v1, v2

    move-object/from16 v12, v17

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 476
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 483
    :cond_2
    const/4 v15, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->WIDTH_BASIS_6:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    if-ge v15, v1, :cond_3

    .line 484
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->mScreenHeight:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v14, v1

    .line 485
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v1

    int-to-float v2, v15

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_start_x_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    float-to-int v2, v2

    add-int v13, v1, v2

    .line 486
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v8, v13

    int-to-float v9, v14

    int-to-float v1, v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_width_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float v10, v1, v2

    int-to-float v1, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->col_height_6:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)F

    move-result v2

    add-float v11, v1, v2

    move-object/from16 v12, v17

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 483
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 489
    :cond_3
    return-void
.end method

.method private setPaint()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, -0x1

    const/high16 v4, -0x1000000

    const/4 v3, 0x0

    .line 293
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 301
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 302
    .local v0, "e":Landroid/graphics/PathEffect;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 304
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 308
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 311
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 314
    return-void

    .line 301
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 320
    return-void
.end method

.method public onGloveEnableChanged(Z)V
    .locals 1
    .param p1, "isEnable"    # Z

    .prologue
    .line 635
    if-eqz p1, :cond_0

    .line 636
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;)V

    .line 640
    :goto_0
    return-void

    .line 638
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 324
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 326
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_HOVER:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    .line 330
    packed-switch v0, :pswitch_data_0

    .line 345
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 332
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 335
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 338
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 330
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 350
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 352
    .local v0, "action":I
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 378
    :goto_0
    return v3

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 357
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->getEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 358
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    .line 364
    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 366
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 360
    :cond_2
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 369
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 372
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 364
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
