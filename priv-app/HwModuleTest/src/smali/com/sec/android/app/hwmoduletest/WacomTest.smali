.class public Lcom/sec/android/app/hwmoduletest/WacomTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "WacomTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;
    }
.end annotation


# static fields
.field private static final AMETA_PEN_ON:I = 0x2000000


# instance fields
.field private HEIGHT_BASIS:I

.field protected KEY_TIMEOUT:I

.field protected KEY_TIMER_EXPIRED:I

.field protected MILLIS_IN_SEC:I

.field private WIDTH_BASIS:I

.field private click:[[Z

.field private draw:[[Z

.field private isDrawArea:[[Z

.field private mBottommostOfMatrix:I

.field private mCenterOfHorizontalOfMatrix:I

.field private mCenterOfVerticalOfMatrix:I

.field private mCurrentTime:J

.field private mIsPressedBackkey:Z

.field private mIsWacom:Z

.field private mLeftmostOfMatrix:I

.field private mRightmostOfMatrix:I

.field protected mTimerHandler:Landroid/os/Handler;

.field private mTopmostOfMatrix:I

.field private passFlag:I

.field private remoteCall:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 59
    const-string v0, "WacomTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I

    .line 36
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    .line 37
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->remoteCall:Z

    .line 45
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mTopmostOfMatrix:I

    .line 46
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mBottommostOfMatrix:I

    .line 47
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfVerticalOfMatrix:I

    .line 48
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mLeftmostOfMatrix:I

    .line 49
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mRightmostOfMatrix:I

    .line 50
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfHorizontalOfMatrix:I

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsPressedBackkey:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsWacom:Z

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCurrentTime:J

    .line 54
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->KEY_TIMER_EXPIRED:I

    .line 55
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->MILLIS_IN_SEC:I

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->KEY_TIMEOUT:I

    .line 168
    new-instance v0, Lcom/sec/android/app/hwmoduletest/WacomTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/WacomTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/WacomTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mTimerHandler:Landroid/os/Handler;

    .line 60
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/WacomTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/WacomTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/WacomTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I

    return v0
.end method

.method static synthetic access$1008(Lcom/sec/android/app/hwmoduletest/WacomTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->remoteCall:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/WacomTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsWacom:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->click:[[Z

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/WacomTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/WacomTest;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->isPass()Z

    move-result v0

    return v0
.end method

.method private decideRemote()V
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 99
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RemoteCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->remoteCall:Z

    .line 100
    return-void
.end method

.method private fillUpMatrix()V
    .locals 4

    .prologue
    .line 103
    const/4 v1, 0x0

    .local v1, "row":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    if-ge v1, v2, :cond_2

    .line 104
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    if-ge v0, v2, :cond_1

    .line 105
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->isNeededCheck(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    .line 104
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    goto :goto_2

    .line 103
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    .end local v0    # "column":I
    :cond_2
    return-void
.end method

.method private initGridSettings()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->click:[[Z

    .line 87
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z

    .line 88
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z

    .line 89
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mTopmostOfMatrix:I

    .line 90
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mBottommostOfMatrix:I

    .line 91
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfVerticalOfMatrix:I

    .line 92
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mLeftmostOfMatrix:I

    .line 93
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mRightmostOfMatrix:I

    .line 94
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfHorizontalOfMatrix:I

    .line 95
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 115
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfVerticalOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mRightmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCenterOfHorizontalOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPass()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 126
    const/4 v1, 0x1

    .line 128
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    if-ge v0, v4, :cond_3

    .line 129
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    if-ge v2, v4, :cond_2

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-ne v4, v3, :cond_0

    .line 131
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_1

    move v1, v3

    .line 129
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 131
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 128
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    .end local v2    # "j":I
    :cond_3
    return v1
.end method

.method private setGridSizebyModel()V
    .locals 1

    .prologue
    .line 74
    const-string v0, "WACOM_HEIGHT_BASIS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    .line 75
    const-string v0, "WACOM_WIDTH_BASIS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    .line 77
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    if-nez v0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    const-string v0, "WACOM_HEIGHT_BASIS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I

    .line 81
    const-string v0, "WACOM_WIDTH_BASIS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->setGridSizebyModel()V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->initGridSettings()V

    .line 67
    new-instance v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/WacomTest;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->setContentView(Landroid/view/View;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->fillUpMatrix()V

    .line 70
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 140
    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v1

    .line 142
    :cond_1
    const/4 v2, 0x4

    if-ne p1, v2, :cond_3

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onKeyDown"

    const-string v4, "This is back_key"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsPressedBackkey:Z

    if-nez v2, :cond_2

    .line 146
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 147
    .local v0, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCurrentTime:J

    .line 148
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsPressedBackkey:Z

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->startTimer()V

    goto :goto_0

    .line 151
    .end local v0    # "rightNow":Ljava/util/Calendar;
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsPressedBackkey:Z

    .line 152
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 153
    .restart local v0    # "rightNow":Ljava/util/Calendar;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onKeyDown"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rightNow.getTimeInMillis() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mCurrentTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCurrentTime:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mCurrentTime:J

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->KEY_TIMEOUT:I

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->MILLIS_IN_SEC:I

    mul-int/2addr v6, v7

    int-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->finish()V

    goto :goto_0

    .line 164
    .end local v0    # "rightNow":Ljava/util/Calendar;
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onStop()V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->finish()V

    .line 123
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->mTimerHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->KEY_TIMER_EXPIRED:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->KEY_TIMEOUT:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest;->MILLIS_IN_SEC:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 183
    return-void
.end method
