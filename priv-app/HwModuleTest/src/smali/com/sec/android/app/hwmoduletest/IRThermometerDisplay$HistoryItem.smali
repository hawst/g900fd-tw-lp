.class Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;
.super Ljava/lang/Object;
.source "IRThermometerDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryItem"
.end annotation


# instance fields
.field private body_temp:Ljava/lang/String;

.field private countvalue:Ljava/lang/String;

.field private rawta_data:Ljava/lang/String;

.field private rawto_data:Ljava/lang/String;

.field private ta_data:Ljava/lang/String;

.field private to_data:Ljava/lang/String;


# direct methods
.method public constructor <init>(I[Ljava/lang/String;)V
    .locals 2
    .param p1, "count"    # I
    .param p2, "bodyData"    # [Ljava/lang/String;

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    .line 273
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->body_temp:Ljava/lang/String;

    .line 274
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->to_data:Ljava/lang/String;

    .line 275
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->ta_data:Ljava/lang/String;

    .line 276
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawto_data:Ljava/lang/String;

    .line 277
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawta_data:Ljava/lang/String;

    .line 280
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    .line 283
    const-string v0, "PARTRON"

    sget-object v1, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const/4 v0, 0x3

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->body_temp:Ljava/lang/String;

    .line 285
    const/4 v0, 0x4

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->to_data:Ljava/lang/String;

    .line 286
    const/4 v0, 0x2

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->ta_data:Ljava/lang/String;

    .line 287
    const/4 v0, 0x1

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawto_data:Ljava/lang/String;

    .line 288
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawta_data:Ljava/lang/String;

    .line 290
    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getRawta_data()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawta_data:Ljava/lang/String;

    return-object v0
.end method

.method public getRawto_data()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->rawto_data:Ljava/lang/String;

    return-object v0
.end method

.method public getValueBodyTemp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->body_temp:Ljava/lang/String;

    return-object v0
.end method

.method public getValueTaData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->ta_data:Ljava/lang/String;

    return-object v0
.end method

.method public getValueToData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->to_data:Ljava/lang/String;

    return-object v0
.end method
