.class Lcom/sec/android/app/hwmoduletest/GestureTestMode2$1;
.super Ljava/lang/Object;
.source "GestureTestMode2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 68
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->access$002(Z)Z

    .line 70
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 75
    :goto_0
    return-void

    .line 72
    :cond_0
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->access$002(Z)Z

    .line 73
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
