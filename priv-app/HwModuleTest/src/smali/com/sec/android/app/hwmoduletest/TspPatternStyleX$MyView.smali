.class public Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;
.super Landroid/view/View;
.source "TspPatternStyleX.java"

# interfaces
.implements Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private col_height:F

.field private col_width:F

.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mHeightCross:F

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mNonClickPaint:Landroid/graphics/Paint;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field private mWidthCross:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;Landroid/content/Context;)V
    .locals 8
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 245
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    .line 246
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 224
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    .line 225
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    .line 226
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    .line 227
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    .line 228
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    .line 229
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    .line 230
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    .line 231
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    .line 247
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->setKeepScreenOn(Z)V

    .line 249
    const-string v3, "window"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 251
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 252
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 253
    const-string v3, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    iget v3, v2, Landroid/graphics/Point;->x:I

    add-int/lit16 v3, v3, -0xa0

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 256
    :cond_0
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    .line 257
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    .line 258
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Screen size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " x "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 262
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    invoke-static {v0, v3, v4, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 263
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 267
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v3, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    .line 268
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v3, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    .line 270
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 271
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    .line 276
    :goto_0
    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "factory"

    const-string v4, "BINARY_TYPE"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 279
    new-instance v3, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView$1;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView$1;-><init>(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 287
    :cond_1
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    .line 288
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->setPaint()V

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->initRect()V

    .line 290
    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    .line 291
    return-void

    .line 273
    :cond_2
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    goto :goto_0
.end method

.method private checkCrossRectRegion(FFIILandroid/graphics/Paint;)V
    .locals 13
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "countX"    # I
    .param p4, "countY"    # I
    .param p5, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 589
    const/4 v7, 0x0

    .line 590
    .local v7, "countCrossY":I
    const/4 v10, 0x0

    .line 591
    .local v10, "startRectTopX":I
    const/4 v11, 0x0

    .line 592
    .local v11, "startRectTopY":I
    const/4 v8, 0x0

    .line 593
    .local v8, "startRectBottomX":I
    const/4 v9, 0x0

    .line 595
    .local v9, "startRectBottomY":I
    if-lez p4, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move/from16 v0, p4

    if-ge v0, v1, :cond_3

    .line 596
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    sub-float v1, p2, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    div-float/2addr v1, v2

    float-to-int v7, v1

    .line 597
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    int-to-float v3, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v11, v1

    .line 598
    move v9, v11

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 601
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v7, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    float-to-int v2, v2

    add-int v10, v1, v2

    .line 602
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v3, v7, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v8, v1

    .line 608
    :goto_0
    int-to-float v1, v10

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    int-to-float v1, v10

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v1, v2

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    aget-boolean v1, v1, v7

    if-nez v1, :cond_0

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v2, v10, 0x1

    int-to-float v2, v2

    add-int/lit8 v3, v11, 0x1

    int-to-float v3, v3

    int-to-float v4, v10

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    int-to-float v5, v11

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v6, v12

    add-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 618
    new-instance v1, Landroid/graphics/Rect;

    add-int/lit8 v2, v10, -0x1

    add-int/lit8 v3, v11, -0x1

    int-to-float v4, v10

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v5, v11

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    add-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 623
    :cond_0
    if-ltz v7, :cond_1

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    const/4 v2, 0x1

    aput-boolean v2, v1, v7

    .line 628
    :cond_1
    int-to-float v1, v8

    cmpl-float v1, p1, v1

    if-lez v1, :cond_3

    int-to-float v1, v8

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v1, v2

    cmpg-float v1, p1, v1

    if-gez v1, :cond_3

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v7

    aget-boolean v1, v1, v2

    if-nez v1, :cond_2

    .line 635
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v2, v8, 0x1

    int-to-float v2, v2

    add-int/lit8 v3, v9, 0x1

    int-to-float v3, v3

    int-to-float v4, v8

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    int-to-float v5, v9

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v6, v12

    add-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v5, v5

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 638
    new-instance v1, Landroid/graphics/Rect;

    add-int/lit8 v2, v8, -0x1

    add-int/lit8 v3, v9, -0x1

    int-to-float v4, v8

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    int-to-float v5, v9

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    add-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 642
    :cond_2
    if-ltz v7, :cond_3

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v7

    const/4 v3, 0x1

    aput-boolean v3, v1, v2

    .line 647
    :cond_3
    return-void

    .line 604
    :cond_4
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v7, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v10, v1

    .line 605
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v3, v7, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v8, v1

    goto/16 :goto_0
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 430
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 432
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 433
    float-to-int v6, p1

    .line 434
    float-to-int v8, p3

    .line 440
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 441
    float-to-int v7, p2

    .line 442
    float-to-int v9, p4

    .line 448
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 449
    return-void

    .line 436
    :cond_0
    float-to-int v6, p3

    .line 437
    float-to-int v8, p1

    goto :goto_0

    .line 444
    :cond_1
    float-to-int v7, p4

    .line 445
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 454
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 455
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->getIsHoverToolType()Z

    move-result v0

    if-nez v0, :cond_1

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 537
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 538
    .local v9, "col_width":F
    div-float v0, p1, v9

    float-to-int v10, v0

    .line 539
    .local v10, "countX":I
    div-float v0, p2, v8

    float-to-int v11, v0

    .line 540
    .local v11, "countY":I
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 541
    .local v6, "ColX":F
    int-to-float v0, v11

    mul-float v7, v8, v0

    .line 544
    .local v7, "ColY":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v11, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v10, v0, :cond_2

    if-ltz v10, :cond_2

    if-gez v11, :cond_3

    .line 545
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$800(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "You are out of bounds!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 549
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-nez v0, :cond_4

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$900(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-eqz v0, :cond_4

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 555
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->invalidate(Landroid/graphics/Rect;)V

    :cond_4
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, v10

    move v4, v11

    move-object v5, p3

    .line 565
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->checkCrossRectRegion(FFIILandroid/graphics/Paint;)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # invokes: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPass()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$1000(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # invokes: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isPassCross()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$1100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$1200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->successTest:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$1202(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;Z)Z

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->remoteCall:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$1300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setResult(I)V

    .line 576
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setResult(I)V

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->finish()V

    goto/16 :goto_0

    .line 581
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->setResult(I)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->finish()V

    goto/16 :goto_0
.end method

.method private draw_down(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 385
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    .line 386
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    .line 387
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 388
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    .line 389
    return-void
.end method

.method private draw_move(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 392
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    if-eqz v1, :cond_1

    .line 393
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 394
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    .line 395
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    .line 396
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    .line 397
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    .line 398
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 399
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawLine(FFFF)V

    .line 393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 402
    :cond_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    .line 403
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    .line 404
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    .line 405
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    .line 406
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 407
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawLine(FFFF)V

    .line 408
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    .line 410
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private draw_up(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 413
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 414
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    .line 415
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    .line 416
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    .line 417
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    .line 419
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 420
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->drawPoint(FF)V

    .line 423
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->isTouchDown:Z

    .line 425
    :cond_1
    return-void
.end method

.method private initRect()V
    .locals 18

    .prologue
    .line 458
    const/4 v13, 0x0

    .line 459
    .local v13, "ColX":I
    const/4 v14, 0x0

    .line 460
    .local v14, "ColY":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 461
    .local v6, "mRectPaint":Landroid/graphics/Paint;
    new-instance v17, Landroid/graphics/Paint;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Paint;-><init>()V

    .line 462
    .local v17, "mRectPaintCross":Landroid/graphics/Paint;
    const/high16 v1, -0x1000000

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 463
    const/high16 v1, -0x1000000

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 464
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 466
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    if-ge v15, v1, :cond_1

    .line 467
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    int-to-float v2, v15

    mul-float/2addr v1, v2

    float-to-int v14, v1

    .line 469
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    move/from16 v0, v16

    if-ge v0, v1, :cond_0

    .line 470
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    move/from16 v0, v16

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v13, v1

    .line 471
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v4, v4

    int-to-float v5, v14

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 472
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    int-to-float v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->draw:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$500(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v1

    aget-object v1, v1, v15

    const/4 v2, 0x0

    aput-boolean v2, v1, v16

    .line 474
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->click:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$600(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[[Z

    move-result-object v1

    aget-object v1, v1, v15

    const/4 v2, 0x0

    aput-boolean v2, v1, v16

    .line 469
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 466
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 492
    .end local v16    # "j":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 493
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 494
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    sub-float v8, v1, v2

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenWidth:I

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 496
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 503
    :goto_2
    const/4 v15, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    if-ge v15, v1, :cond_4

    .line 504
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 505
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v15, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    float-to-int v2, v2

    add-int v13, v1, v2

    .line 510
    :goto_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    int-to-float v3, v15

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v14, v1

    .line 511
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v8, v13

    int-to-float v9, v14

    int-to-float v1, v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float v10, v1, v2

    int-to-float v1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float v11, v1, v2

    move-object/from16 v12, v17

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 513
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    const/4 v2, 0x0

    aput-boolean v2, v1, v15

    .line 503
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 499
    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$200(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$100(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 507
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v15, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v13, v1

    goto/16 :goto_4

    .line 516
    :cond_4
    const/4 v15, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v1

    if-ge v15, v1, :cond_6

    .line 517
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 518
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v15, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    float-to-int v2, v2

    add-int v13, v1, v2

    .line 523
    :goto_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mScreenHeight:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mHeightCross:F

    add-int/lit8 v4, v15, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v14, v1

    .line 524
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v8, v13

    int-to-float v9, v14

    int-to-float v1, v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_width:F

    add-float v10, v1, v2

    int-to-float v1, v14

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->col_height:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float v11, v1, v2

    move-object/from16 v12, v17

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->drawCross:[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$700(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)[Z

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->HEIGHT_BASIS_CROSS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$400(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)I

    move-result v2

    add-int/2addr v2, v15

    const/4 v3, 0x0

    aput-boolean v3, v1, v2

    .line 516
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 520
    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mWidthCross:F

    add-int/lit8 v2, v15, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v13, v1

    goto :goto_6

    .line 528
    :cond_6
    return-void
.end method

.method private setPaint()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/high16 v3, -0x1000000

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 295
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 303
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 304
    .local v0, "e":Landroid/graphics/PathEffect;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 306
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mClickPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/AvoidXfermode;

    const/16 v3, 0xff

    sget-object v4, Landroid/graphics/AvoidXfermode$Mode;->TARGET:Landroid/graphics/AvoidXfermode$Mode;

    invoke-direct {v2, v5, v3, v4}, Landroid/graphics/AvoidXfermode;-><init>(IILandroid/graphics/AvoidXfermode$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 311
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 314
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 317
    return-void

    .line 303
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 323
    return-void
.end method

.method public onGloveEnableChanged(Z)V
    .locals 1
    .param p1, "isEnable"    # Z

    .prologue
    .line 651
    if-eqz p1, :cond_0

    .line 652
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;)V

    .line 656
    :goto_0
    return-void

    .line 654
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 329
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_HOVER:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    .line 333
    packed-switch v0, :pswitch_data_0

    .line 348
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 335
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 338
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 341
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 353
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 355
    .local v0, "action":I
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 381
    :goto_0
    return v3

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    # getter for: Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;->access$300(Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->getEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 361
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    .line 367
    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 369
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 363
    :cond_2
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 372
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 375
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
