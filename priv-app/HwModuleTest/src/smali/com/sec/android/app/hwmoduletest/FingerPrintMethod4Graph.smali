.class public Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
.super Landroid/view/View;
.source "FingerPrintMethod4Graph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    }
.end annotation


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private GRAPH_MAX_VALUE:F

.field private GRAPH_X_SCALING:F

.field private GRAPH_Y_SCALING:F

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private SCLAING:I

.field private final TEXTSIZE_RATIO:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mCol:I

.field private mColWidth:I

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mDataResult:Ljava/lang/String;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mNoTermCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermPixelFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermPixelFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNumberPaint:Landroid/graphics/Paint;

.field private mPathSpecMax:Landroid/graphics/Path;

.field private mPathSpecMin:Landroid/graphics/Path;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mPrimaryEndPixel:I

.field private mPrimaryStartPixel:I

.field private mRect:Landroid/graphics/Rect;

.field private mResultFailPaint:Landroid/graphics/Paint;

.field private mResultPassPaint:Landroid/graphics/Paint;

.field private mRow:I

.field private mRowHeight:I

.field private mScreenHeight:I

.field private mScreenHeight_Max:I

.field private mScreenTitleHeight:I

.field private mScreenTitleHeight_Max:I

.field private mScreenWidth:I

.field private mSecondaryEndPixel:I

.field private mSecondaryStartPixel:I

.field private mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossBoundarySecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossPixenlFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossPixenlFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSpecPaint:Landroid/graphics/Paint;

.field private mSpecRow:I

.field private mSpecRowHeight:I

.field private mTableTexts:[[Ljava/lang/String;

.field private mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTextCenterPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValuePaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private final paddingHeight:I

.field private final paddingWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v2, 0x1f4

    const/16 v3, 0xf

    const/16 v1, 0xa

    .line 116
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 27
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_X:I

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    .line 29
    const/high16 v0, 0x44be0000    # 1520.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_MAX_VALUE:F

    .line 30
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    .line 31
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    .line 32
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->TEXTSIZE_RATIO:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    .line 38
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingWidth:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingHeight:I

    .line 40
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    .line 41
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    .line 42
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    .line 43
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight:I

    .line 44
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    .line 45
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    .line 49
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRowHeight:I

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    .line 66
    const-string v0, "FingerPrintMethod4Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->CLASS_NAME:Ljava/lang/String;

    .line 68
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    .line 69
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    .line 70
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    .line 71
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    .line 103
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataResult:Ljava/lang/String;

    .line 117
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->init(Landroid/content/Context;)V

    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v2, 0x1f4

    const/16 v3, 0xf

    const/16 v1, 0xa

    .line 126
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_X:I

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    .line 29
    const/high16 v0, 0x44be0000    # 1520.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_MAX_VALUE:F

    .line 30
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    .line 31
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    .line 32
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->TEXTSIZE_RATIO:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    .line 38
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingWidth:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingHeight:I

    .line 40
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    .line 41
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    .line 42
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    .line 43
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight:I

    .line 44
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    .line 45
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    .line 49
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRowHeight:I

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    .line 66
    const-string v0, "FingerPrintMethod4Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->CLASS_NAME:Ljava/lang/String;

    .line 68
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    .line 69
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    .line 70
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    .line 71
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    .line 103
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataResult:Ljava/lang/String;

    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->init(Landroid/content/Context;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/16 v2, 0x1f4

    const/16 v3, 0xf

    const/16 v1, 0xa

    .line 121
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_X:I

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    .line 29
    const/high16 v0, 0x44be0000    # 1520.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_MAX_VALUE:F

    .line 30
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    .line 31
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    .line 32
    const/16 v0, 0x24

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->TEXTSIZE_RATIO:I

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    .line 38
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingWidth:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->paddingHeight:I

    .line 40
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    .line 41
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    .line 42
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    .line 43
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight:I

    .line 44
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    .line 45
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    sub-int/2addr v1, v2

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    .line 49
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenTitleHeight_Max:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRowHeight:I

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    .line 55
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    .line 56
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 57
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    .line 66
    const-string v0, "FingerPrintMethod4Graph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->CLASS_NAME:Ljava/lang/String;

    .line 68
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    .line 69
    const/16 v0, 0x7a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    .line 70
    const/16 v0, 0x7b

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    .line 71
    const/16 v0, 0xbf

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    .line 103
    const-string v0, "Fail"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataResult:Ljava/lang/String;

    .line 122
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->init(Landroid/content/Context;)V

    .line 123
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x2

    const/high16 v9, -0x1000000

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    .line 131
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mContext:Landroid/content/Context;

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mContext:Landroid/content/Context;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 133
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 134
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 135
    const-string v3, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 136
    iget v3, v1, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Point;->x:I

    .line 138
    :cond_0
    iget v3, v1, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    .line 139
    iget v3, v1, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight:I

    .line 141
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v2, v3, 0x24

    .line 142
    .local v2, "textSize":I
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight:I

    div-int/lit8 v3, v3, 0x24

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRowHeight:I

    .line 143
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight:I

    div-int/lit8 v3, v3, 0x24

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    .line 146
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v3, v3, -0x14

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    .line 147
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    div-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mColWidth:I

    .line 149
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x5

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    .line 150
    new-instance v3, Landroid/graphics/CornerPathEffect;

    const/high16 v4, 0x41200000    # 10.0f

    invoke-direct {v3, v4}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mEffects:Landroid/graphics/PathEffect;

    .line 151
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40c00000    # 6.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    const v4, -0xffff01

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 156
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40800000    # 4.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    const v4, -0xff0100

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 161
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    const v4, -0xff0001

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 166
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 171
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40400000    # 3.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 176
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 181
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 186
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mLinePaint:Landroid/graphics/Paint;

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mLinePaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mLinePaint:Landroid/graphics/Paint;

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 190
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextPaint:Landroid/graphics/Paint;

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 195
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    .line 196
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 199
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 201
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 207
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    const v4, -0xffff01

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    add-int/lit8 v4, v2, 0xa

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 213
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    add-int/lit8 v4, v2, 0xa

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 221
    const-string v3, "FINGERPRINT_PRIMARY_PIXEL_START"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    .line 222
    const-string v3, "FINGERPRINT_PRIMARY_PIXEL_END"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    .line 223
    const-string v3, "FINGERPRINT_SECONDARY_PIXEL_START"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    .line 224
    const-string v3, "FINGERPRINT_SECONDARY_PIXEL_END"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    .line 232
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x1c2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x514

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 233
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xaa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 234
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x190

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x44c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 235
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 236
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 237
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x140

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x384

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 238
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xaa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 239
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x12c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x2ee

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 240
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x82

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x172

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 241
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 242
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 243
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xe1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 245
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x1f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x578

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 246
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xaa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 247
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x1c2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x41a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 248
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 249
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 250
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x12c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 251
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0xaa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 252
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x12c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x320

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 253
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x78

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x190

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 254
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 255
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 256
    new-instance v3, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/16 v4, 0x6e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x12c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->invalidate()V

    .line 259
    return-void
.end method

.method public static isNumeric(Ljava/lang/String;)Z
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 543
    const-string v0, "-?\\d+(\\.\\d+)?"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private round(FI)F
    .locals 6
    .param p1, "t"    # F
    .param p2, "decimal"    # I

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 638
    float-to-double v0, p1

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    int-to-double v2, p2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public addValue(III)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 553
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 263
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 265
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenHeight:I

    int-to-float v0, v0

    const/high16 v1, 0x41f00000    # 30.0f

    div-float v6, v0, v1

    .line 268
    .local v6, "baseYpx":F
    const-string v0, "No Term Signal :"

    const/high16 v1, 0x41200000    # 10.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 269
    const/high16 v1, 0x437a0000    # 250.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    const/high16 v3, 0x43960000    # 300.0f

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 270
    const-string v0, "Term Signal :"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit16 v1, v1, -0x96

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 271
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x32

    int-to-float v1, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x64

    int-to-float v3, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 272
    const-string v0, "Signal Loss Rate :"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit16 v1, v1, 0xb4

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 273
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0x1ae

    int-to-float v1, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v2, v6, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/lit16 v0, v0, 0x1e0

    int-to-float v3, v0

    const/high16 v0, 0x41200000    # 10.0f

    sub-float v4, v6, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 276
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v3, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 277
    const/high16 v1, 0x42480000    # 50.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 279
    const-string v0, "300"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x43960000    # 300.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 280
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x43960000    # 300.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x43960000    # 300.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 282
    const-string v0, "600"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x44160000    # 600.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 283
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x44160000    # 600.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x44160000    # 600.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 285
    const-string v0, "900"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x44610000    # 900.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 286
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x44610000    # 900.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x44610000    # 900.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 288
    const-string v0, "1200"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const/high16 v3, 0x44960000    # 1200.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 289
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v2, 0x44960000    # 1200.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const/high16 v4, 0x44960000    # 1200.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 291
    const-string v0, "1500"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v2, v2

    const v3, 0x44bb8000    # 1500.0f

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 292
    const/high16 v1, 0x42200000    # 40.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const v2, 0x44bb8000    # 1500.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    const v4, 0x44bb8000    # 1500.0f

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 295
    const/16 v8, 0x14

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 296
    const/high16 v0, 0x42480000    # 50.0f

    int-to-float v1, v8

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_MAX_VALUE:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->SCLAING:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    const/high16 v0, 0x42480000    # 50.0f

    int-to-float v3, v8

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0xa

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 298
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    int-to-float v2, v8

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    add-int/lit8 v2, v2, 0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValuePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 295
    add-int/lit8 v8, v8, 0x14

    goto :goto_0

    .line 301
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    add-int/lit8 v0, v0, 0x1e

    int-to-float v6, v0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 309
    const/4 v8, 0x0

    :goto_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    if-ge v8, v0, :cond_9

    .line 310
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    if-ge v9, v0, :cond_8

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    if-eqz v0, :cond_2

    .line 312
    add-int/lit8 v10, v9, 0x1

    .line 313
    .local v10, "k":I
    :goto_3
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    if-ge v10, v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v10

    if-eqz v0, :cond_3

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    mul-int/2addr v2, v8

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0xa

    add-int/lit8 v2, v8, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mColWidth:I

    mul-int/2addr v1, v9

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mColWidth:I

    mul-int/2addr v1, v10

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 330
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRow:I

    if-ge v8, v0, :cond_4

    .line 331
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSpecRowHeight:I

    .line 334
    .local v7, "height":I
    :goto_4
    const-string v0, "PASS"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultPassPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 343
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .line 310
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 313
    .restart local v10    # "k":I
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 332
    :cond_4
    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRowHeight:I

    .restart local v7    # "height":I
    goto :goto_4

    .line 336
    :cond_5
    const-string v0, "FAIL"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v1, v1, v8

    aget-object v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mResultFailPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 338
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->isNumeric(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x4

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNumberPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_5

    .line 341
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, v8

    aget-object v0, v0, v9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, v7, 0x4

    mul-int/lit8 v3, v3, 0x3

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTextCenterPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_5

    .line 309
    .end local v7    # "height":I
    .end local v10    # "k":I
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 348
    .end local v9    # "j":I
    :cond_9
    return-void
.end method

.method public setDataResult([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 32
    .param p1, "primary"    # [Ljava/lang/String;
    .param p2, "secondary"    # [Ljava/lang/String;

    .prologue
    .line 351
    const/16 v28, 0x0

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 352
    .local v12, "m_NoTermSignal1":I
    const/16 v28, 0x1

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 353
    .local v6, "m_NoTermNoise1":I
    const/16 v28, 0x2

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 354
    .local v10, "m_NoTermSNR1":I
    const/16 v28, 0x3

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 355
    .local v8, "m_NoTermPixelFail1":I
    const/16 v28, 0x4

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 356
    .local v4, "m_NoTermCSTVFail1":I
    const/16 v28, 0x5

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 357
    .local v26, "m_TermSignal1":I
    const/16 v28, 0x6

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v22

    .line 358
    .local v22, "m_TermNoise1":I
    const/16 v28, 0x7

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 359
    .local v24, "m_TermSNR1":I
    const/16 v28, 0x8

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 360
    .local v18, "m_SignalLossRate1":I
    const/16 v28, 0x9

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 361
    .local v16, "m_SignalLossPixelFail1":I
    const/16 v28, 0xa

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 362
    .local v14, "m_SignalLossCSTCFail1":I
    const/16 v28, 0xb

    aget-object v28, p1, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 364
    .local v20, "m_SignalLossSNR1":I
    const/16 v28, 0x0

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 365
    .local v13, "m_NoTermSignal2":I
    const/16 v28, 0x1

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 366
    .local v7, "m_NoTermNoise2":I
    const/16 v28, 0x2

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 367
    .local v11, "m_NoTermSNR2":I
    const/16 v28, 0x3

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 368
    .local v9, "m_NoTermPixelFail2":I
    const/16 v28, 0x4

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 369
    .local v5, "m_NoTermCSTVFail2":I
    const/16 v28, 0x5

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v27

    .line 370
    .local v27, "m_TermSignal2":I
    const/16 v28, 0x6

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 371
    .local v23, "m_TermNoise2":I
    const/16 v28, 0x7

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v25

    .line 372
    .local v25, "m_TermSNR2":I
    const/16 v28, 0x8

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 373
    .local v19, "m_SignalLossRate2":I
    const/16 v28, 0x9

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 374
    .local v17, "m_SignalLossPixelFail2":I
    const/16 v28, 0xa

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 375
    .local v15, "m_SignalLossCSTCFail2":I
    const/16 v28, 0xb

    aget-object v28, p2, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v21

    .line 377
    .local v21, "m_SignalLossSNR2":I
    const/16 v28, 0x0

    const/16 v29, 0x0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Pixel Section Primary : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " ~ "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", Secondary : "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " ~ "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    move/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 380
    const/16 v28, 0x1

    const/16 v29, 0x0

    const-string v30, "Test Items"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 381
    const/16 v28, 0x1

    const/16 v29, 0x2

    const-string v30, "Primary"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 382
    const/16 v28, 0x1

    const/16 v29, 0x4

    const-string v30, "Secondary"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 383
    const/16 v28, 0x1

    const/16 v29, 0x6

    const-string v30, "Result"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 385
    const/16 v28, 0x2

    const/16 v29, 0x0

    const-string v30, ""

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 386
    const/16 v28, 0x3

    const/16 v29, 0x0

    const-string v30, "No Term"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 387
    const/16 v28, 0x8

    const/16 v29, 0x0

    const-string v30, "Term"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 388
    const/16 v28, 0xb

    const/16 v29, 0x0

    const-string v30, "Signal Loss"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 390
    const/16 v28, 0x2

    const/16 v29, 0x2

    const-string v30, "Spec"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 391
    const/16 v28, 0x2

    const/16 v29, 0x3

    const-string v30, "Data"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 392
    const/16 v28, 0x2

    const/16 v29, 0x4

    const-string v30, "Spec"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 393
    const/16 v28, 0x2

    const/16 v29, 0x5

    const-string v30, "Data"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 394
    const/16 v28, 0x2

    const/16 v29, 0x6

    const-string v30, ""

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 396
    const/16 v28, 0x3

    const/16 v29, 0x1

    const-string v30, "Signal"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 397
    const/16 v28, 0x3

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 398
    const/16 v28, 0x3

    const/16 v29, 0x3

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 399
    const/16 v28, 0x3

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 400
    const/16 v28, 0x3

    const/16 v29, 0x5

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v12, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v12, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v13, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v13, v0, :cond_0

    .line 403
    const/16 v28, 0x3

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 408
    :goto_0
    const/16 v28, 0x4

    const/16 v29, 0x1

    const-string v30, "Noise"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 409
    const/16 v28, 0x4

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 410
    const/16 v28, 0x4

    const/16 v29, 0x3

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 411
    const/16 v28, 0x4

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 412
    const/16 v28, 0x4

    const/16 v29, 0x5

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v6, v0, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v6, v0, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v7, v0, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v7, v0, :cond_1

    .line 415
    const/16 v28, 0x4

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 420
    :goto_1
    const/16 v28, 0x5

    const/16 v29, 0x1

    const-string v30, "SNR"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 421
    const/16 v28, 0x5

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 422
    const/16 v28, 0x5

    const/16 v29, 0x3

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 423
    const/16 v28, 0x5

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 424
    const/16 v28, 0x5

    const/16 v29, 0x5

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v10, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v10, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-lt v11, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v11, v0, :cond_2

    .line 427
    const/16 v28, 0x5

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 432
    :goto_2
    const/16 v28, 0x6

    const/16 v29, 0x1

    const-string v30, "Pixel Fail"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 433
    const/16 v28, 0x6

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 434
    const/16 v28, 0x6

    const/16 v29, 0x3

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 435
    const/16 v28, 0x6

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 436
    const/16 v28, 0x6

    const/16 v29, 0x5

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v8, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermPixelFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v9, v0, :cond_3

    .line 439
    const/16 v28, 0x6

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 444
    :goto_3
    const/16 v28, 0x7

    const/16 v29, 0x1

    const-string v30, "CSTV Fail"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 445
    const/16 v28, 0x7

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 446
    const/16 v28, 0x7

    const/16 v29, 0x3

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 447
    const/16 v28, 0x7

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 448
    const/16 v28, 0x7

    const/16 v29, 0x5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v4, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mNoTermCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v5, v0, :cond_4

    .line 451
    const/16 v28, 0x7

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 456
    :goto_4
    const/16 v28, 0x8

    const/16 v29, 0x1

    const-string v30, "Signal"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 457
    const/16 v28, 0x8

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 458
    const/16 v28, 0x8

    const/16 v29, 0x3

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 459
    const/16 v28, 0x8

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 460
    const/16 v28, 0x8

    const/16 v29, 0x5

    invoke-static/range {v27 .. v27}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v26

    move/from16 v1, v28

    if-lt v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v26

    move/from16 v1, v28

    if-gt v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-lt v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSignalSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-gt v0, v1, :cond_5

    .line 463
    const/16 v28, 0x8

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 468
    :goto_5
    const/16 v28, 0x9

    const/16 v29, 0x1

    const-string v30, "Noise"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 469
    const/16 v28, 0x9

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 470
    const/16 v28, 0x9

    const/16 v29, 0x3

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 471
    const/16 v28, 0x9

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 472
    const/16 v28, 0x9

    const/16 v29, 0x5

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v22

    move/from16 v1, v28

    if-lt v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoisePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v22

    move/from16 v1, v28

    if-gt v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v23

    move/from16 v1, v28

    if-lt v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermNoiseSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v23

    move/from16 v1, v28

    if-gt v0, v1, :cond_6

    .line 475
    const/16 v28, 0x9

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 480
    :goto_6
    const/16 v28, 0xa

    const/16 v29, 0x1

    const-string v30, "SNR"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 481
    const/16 v28, 0xa

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 482
    const/16 v28, 0xa

    const/16 v29, 0x3

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 483
    const/16 v28, 0xa

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 484
    const/16 v28, 0xa

    const/16 v29, 0x5

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v24

    move/from16 v1, v28

    if-lt v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v24

    move/from16 v1, v28

    if-gt v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v25

    move/from16 v1, v28

    if-lt v0, v1, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTermSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v25

    move/from16 v1, v28

    if-gt v0, v1, :cond_7

    .line 487
    const/16 v28, 0xa

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 492
    :goto_7
    const/16 v28, 0xb

    const/16 v29, 0x1

    const-string v30, "Rate"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 493
    const/16 v28, 0xb

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 494
    const/16 v28, 0xb

    const/16 v29, 0x3

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 495
    const/16 v28, 0xb

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 496
    const/16 v28, 0xb

    const/16 v29, 0x5

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v18

    move/from16 v1, v28

    if-lt v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRatePrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v18

    move/from16 v1, v28

    if-gt v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v19

    move/from16 v1, v28

    if-lt v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossRateSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v19

    move/from16 v1, v28

    if-gt v0, v1, :cond_8

    .line 499
    const/16 v28, 0xb

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 504
    :goto_8
    const/16 v28, 0xc

    const/16 v29, 0x1

    const-string v30, "Pixel Fail"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 505
    const/16 v28, 0xc

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 506
    const/16 v28, 0xc

    const/16 v29, 0x3

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 507
    const/16 v28, 0xc

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 508
    const/16 v28, 0xc

    const/16 v29, 0x5

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v16

    move/from16 v1, v28

    if-gt v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossPixenlFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v17

    move/from16 v1, v28

    if-gt v0, v1, :cond_9

    .line 511
    const/16 v28, 0xc

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 516
    :goto_9
    const/16 v28, 0xd

    const/16 v29, 0x1

    const-string v30, "CSTV Fail"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 517
    const/16 v28, 0xd

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 518
    const/16 v28, 0xd

    const/16 v29, 0x3

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 519
    const/16 v28, 0xd

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 520
    const/16 v28, 0xd

    const/16 v29, 0x5

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v14, v0, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCSTVFailSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v28

    if-gt v15, v0, :cond_a

    .line 523
    const/16 v28, 0xd

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 528
    :goto_a
    const/16 v28, 0xe

    const/16 v29, 0x1

    const-string v30, "Cal SNR"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 529
    const/16 v28, 0xe

    const/16 v29, 0x2

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 530
    const/16 v28, 0xe

    const/16 v29, 0x3

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 531
    const/16 v28, 0xe

    const/16 v29, 0x4

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "~"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 532
    const/16 v28, 0xe

    const/16 v29, 0x5

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v20

    move/from16 v1, v28

    if-lt v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v20

    move/from16 v1, v28

    if-gt v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v21

    move/from16 v1, v28

    if-lt v0, v1, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossCalSNRSecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Ljava/lang/Integer;

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move/from16 v0, v21

    move/from16 v1, v28

    if-gt v0, v1, :cond_b

    .line 535
    const/16 v28, 0xe

    const/16 v29, 0x6

    const-string v30, "PASS"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    .line 540
    :goto_b
    return-void

    .line 405
    :cond_0
    const/16 v28, 0x3

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 417
    :cond_1
    const/16 v28, 0x4

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_1

    .line 429
    :cond_2
    const/16 v28, 0x5

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_2

    .line 441
    :cond_3
    const/16 v28, 0x6

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_3

    .line 453
    :cond_4
    const/16 v28, 0x7

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_4

    .line 465
    :cond_5
    const/16 v28, 0x8

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_5

    .line 477
    :cond_6
    const/16 v28, 0x9

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_6

    .line 489
    :cond_7
    const/16 v28, 0xa

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_7

    .line 501
    :cond_8
    const/16 v28, 0xb

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_8

    .line 513
    :cond_9
    const/16 v28, 0xc

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_9

    .line 525
    :cond_a
    const/16 v28, 0xd

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_a

    .line 537
    :cond_b
    const/16 v28, 0xe

    const/16 v29, 0x6

    const-string v30, "FAIL"

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setTextData(IILjava/lang/String;)V

    goto/16 :goto_b
.end method

.method public setGraphXScale(F)V
    .locals 0
    .param p1, "xScale"    # F

    .prologue
    .line 625
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    .line 626
    return-void
.end method

.method public setGraphYScale(F)V
    .locals 0
    .param p1, "yScale"    # F

    .prologue
    .line 629
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    .line 630
    return-void
.end method

.method public setPixelResult(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "primary"    # Ljava/lang/String;
    .param p2, "secondary"    # Ljava/lang/String;

    .prologue
    .line 559
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 560
    .local v1, "priPixel":[Ljava/lang/String;
    const-string v3, ","

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 561
    .local v2, "secPixel":[Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 562
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    aget-object v4, v1, v0

    const-string v5, "\\s"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 564
    :cond_0
    const/4 v0, 0x1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 565
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    aget-object v4, v2, v0

    const-string v5, "\\s"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    div-int/lit8 v4, v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 567
    :cond_1
    return-void
.end method

.method public setSignalLossBoundary([Ljava/lang/String;)V
    .locals 3
    .param p1, "mValue"    # [Ljava/lang/String;

    .prologue
    .line 633
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/4 v1, 0x1

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 634
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    const/4 v1, 0x3

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundarySecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    .line 635
    return-void
.end method

.method public setTextData(IILjava/lang/String;)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 547
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mRow:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mCol:I

    if-ge p2, v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mTableTexts:[[Ljava/lang/String;

    aget-object v0, v0, p1

    aput-object p3, v0, p2

    .line 550
    :cond_0
    return-void
.end method

.method public showGraph()V
    .locals 7

    .prologue
    const/high16 v6, 0x42480000    # 50.0f

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 571
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    .line 572
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 573
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    .line 574
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 575
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    .line 578
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 579
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    .line 580
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 581
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    .line 590
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 591
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 592
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 594
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 595
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    int-to-float v1, v1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 596
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryStartPixel:I

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPrimaryEndPixel:I

    if-gt v0, v1, :cond_0

    .line 597
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 598
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 599
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 601
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 602
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundaryPrimarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 596
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 611
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryStartPixel:I

    :goto_1
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSecondaryEndPixel:I

    if-gt v0, v1, :cond_1

    .line 612
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathX:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 613
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathY:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 614
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathZ:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 616
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMin:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundarySecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->lower:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 617
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mPathSpecMax:Landroid/graphics/Path;

    int-to-float v1, v0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_X_SCALING:F

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->mSignalLossBoundarySecondarySpec:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph$Spec;->upper:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->GRAPH_Y_SCALING:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 611
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 621
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->invalidate()V

    .line 622
    return-void
.end method
