.class Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;
.super Ljava/util/TimerTask;
.source "HrmTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HrmTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HRMSensorTask"
.end annotation


# instance fields
.field private mHRMSensorValues:[F

.field private mIsRunningTask:Z

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/HrmTest;)V
    .locals 1

    .prologue
    .line 281
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mIsRunningTask:Z

    .line 314
    const/4 v0, 0x5

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/HrmTest;Lcom/sec/android/app/hwmoduletest/HrmTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/HrmTest$1;

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HrmTest;)V

    return-void
.end method

.method private StoreHRMSensorValue(I)F
    .locals 1
    .param p1, "ArrayIndex"    # I

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    array-length v0, v0

    if-le v0, p1, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v0, v0, p1

    .line 396
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->resume()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->pause()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getHeartRateValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getSignalQulityValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getSNRValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .prologue
    .line 281
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getRRIValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHeartRateValueString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMRvalue:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "HR(Bpm) : --"

    .line 344
    :goto_0
    return-object v0

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2802(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 336
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # += operator for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2916(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3100(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3108(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mIsRvalue:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2100(Lcom/sec/android/app/hwmoduletest/HrmTest;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 342
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HR(Bpm) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mThreeFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3200(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMRvalue:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 344
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HR(Bpm) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getRRIValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "RRI(ms) : --"

    .line 377
    :goto_0
    return-object v0

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Min:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Min:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Max:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Max:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 373
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # += operator for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4616(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4800(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Avg:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4808(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RRI(ms) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getSNRValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "SNR(db) : --"

    .line 366
    :goto_0
    return-object v0

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3902(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # += operator for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4116(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4300(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Avg:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4202(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4308(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    .line 366
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SNR(db) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Avg:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getSignalQulityValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "Signal Qty(%) : --"

    .line 388
    :goto_0
    return-object v0

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4902(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5102(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # += operator for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5216(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5300(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5308(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Signal Qty(%) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$4900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$5100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private getSpO2ValueString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "SpO2(%) : --"

    .line 355
    :goto_0
    return-object v0

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    # += operator for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3616(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3800(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Avg:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3808(Lcom/sec/android/app/hwmoduletest/HrmTest;)I

    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SpO2(%) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Avg:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$3500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private pause()V
    .locals 2

    .prologue
    const/16 v1, 0xb

    .line 307
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mIsRunningTask:Z

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 312
    :cond_0
    return-void
.end method

.method private readToHRMSensor()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1900(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "readToHRMSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHRMSensorValues :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v1, v1, v5

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mIsRvalue:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2100(Lcom/sec/android/app/hwmoduletest/HrmTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    invoke-direct {p0, v7}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->StoreHRMSensorValue(I)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMRvalue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2202(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 323
    :goto_0
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1700(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v1, v1, v4

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 329
    :cond_0
    :goto_1
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    invoke-direct {p0, v7}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->StoreHRMSensorValue(I)F

    move-result v1

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2302(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    goto :goto_0

    .line 325
    :cond_2
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1700(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v1, v1, v6

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    aget v1, v1, v4

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$2602(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    goto :goto_1
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mIsRunningTask:Z

    .line 304
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 285
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 287
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 292
    :goto_0
    return-void

    .line 289
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mHRMSensorValues:[F

    goto :goto_0

    .line 287
    nop

    :pswitch_data_0
    .packed-switch 0x1001a
        :pswitch_0
    .end packed-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->readToHRMSensor()V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 300
    :cond_0
    return-void
.end method
