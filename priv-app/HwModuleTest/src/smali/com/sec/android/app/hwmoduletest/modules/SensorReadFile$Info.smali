.class Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
.super Ljava/lang/Object;
.source "SensorReadFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Info"
.end annotation


# instance fields
.field public mData:[Ljava/lang/String;

.field public mFileID:Ljava/lang/String;

.field public mIsExistFile:Z

.field public mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "fileid"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mFileID:Ljava/lang/String;

    .line 21
    invoke-static {p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;

    .line 23
    return-void
.end method
