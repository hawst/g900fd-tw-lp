.class Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;
.super Ljava/lang/Object;
.source "TempHumidDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryItem"
.end annotation


# instance fields
.field private compCheckValue:Ljava/lang/String;

.field private countvalue:Ljava/lang/String;

.field private humidCompValue:Ljava/lang/String;

.field private humidRawValue:Ljava/lang/String;

.field private tempCompValue:Ljava/lang/String;

.field private tempRawValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(IFFFFFF)V
    .locals 2
    .param p1, "count"    # I
    .param p2, "tempRaw"    # F
    .param p3, "humidRaw"    # F
    .param p4, "tempComp"    # F
    .param p5, "humidComp"    # F
    .param p6, "TempCheck"    # F
    .param p7, "HumidCheck"    # F

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    .line 176
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->tempRawValue:Ljava/lang/String;

    .line 177
    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->humidRawValue:Ljava/lang/String;

    .line 178
    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->tempCompValue:Ljava/lang/String;

    .line 179
    invoke-static {p5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->humidCompValue:Ljava/lang/String;

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->compCheckValue:Ljava/lang/String;

    .line 182
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueCompCheck()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->compCheckValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueHumidComp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->humidCompValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueHumidRaw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->humidRawValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueTempComp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->tempCompValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueTempRaw()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->tempRawValue:Ljava/lang/String;

    return-object v0
.end method
