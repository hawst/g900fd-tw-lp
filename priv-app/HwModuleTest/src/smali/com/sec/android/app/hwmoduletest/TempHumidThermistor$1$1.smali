.class Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;
.super Ljava/lang/Object;
.source "TempHumidThermistor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 84
    const-string v0, "Temp_Humid"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 85
    .local v12, "testcase":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$100(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "displayValue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "testcase = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v0, "THERMISTOR_AP_EXTERNAL"

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    const-string v0, "SEC_EXT_THERMISTER_TEMP"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 89
    .local v7, "mTempAp":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$202(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 97
    .end local v7    # "mTempAp":Ljava/lang/String;
    :goto_0
    const-string v0, "HUMITEMP_THERMISTER_PAM"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 98
    .local v11, "mTempPam":Ljava/lang/String;
    const-string v0, "BATTERY_TEMP"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 99
    .local v8, "mTempBatt":Ljava/lang/String;
    if-eqz v11, :cond_3

    if-eqz v8, :cond_3

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$402(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$502(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 108
    :goto_1
    const-string v0, "IS_SUPPORT_SENSOR_HUB_THERMS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "HUMITEMP_THERMISTER_BATT_CELCIUS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 110
    .local v10, "mTempHubCHG":Ljava/lang/String;
    const-string v0, "HUMITEMP_THERMISTER_S_HUB_BATT_CELCIUS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 111
    .local v9, "mTempHubBatt":Ljava/lang/String;
    if-eqz v10, :cond_4

    if-eqz v9, :cond_4

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_CHG:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$602(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    # invokes: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_BATT:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$702(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 120
    .end local v9    # "mTempHubBatt":Ljava/lang/String;
    .end local v10    # "mTempHubCHG":Ljava/lang/String;
    :cond_0
    :goto_2
    const-string v0, "IS_SUPPORT_SENSOR_HUB_THERMS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$800(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/util/List;

    move-result-object v13

    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$004(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$200(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$400(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_CHG:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$600(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$500(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v6, v6, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_BATT:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$700(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$900(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;->notifyDataSetChanged()V

    .line 127
    return-void

    .line 92
    .end local v8    # "mTempBatt":Ljava/lang/String;
    .end local v11    # "mTempPam":Ljava/lang/String;
    .restart local v7    # "mTempAp":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, ""

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$202(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 95
    .end local v7    # "mTempAp":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, "APCHIP_TEMP_ADC"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$202(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 103
    .restart local v8    # "mTempBatt":Ljava/lang/String;
    .restart local v11    # "mTempPam":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, ""

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$402(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, ""

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$502(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_1

    .line 115
    .restart local v9    # "mTempHubBatt":Ljava/lang/String;
    .restart local v10    # "mTempHubCHG":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, ""

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_CHG:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$602(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    const-string v1, ""

    # setter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_BATT:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$702(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    .line 123
    .end local v9    # "mTempHubBatt":Ljava/lang/String;
    .end local v10    # "mTempHubCHG":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$800(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$004(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$200(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$400(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->access$500(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method
