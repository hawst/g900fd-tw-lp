.class Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;
.super Ljava/lang/Object;
.source "GyroscopeDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$700(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mCount:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$304(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mX:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mY:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mZ:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;-><init>(IFFF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$800(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;->notifyDataSetChanged()V

    .line 159
    return-void
.end method
