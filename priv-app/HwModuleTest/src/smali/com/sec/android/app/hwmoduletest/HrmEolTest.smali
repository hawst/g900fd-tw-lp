.class public Lcom/sec/android/app/hwmoduletest/HrmEolTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HrmEolTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private Measure:[Ljava/lang/String;

.field private Result:[Ljava/lang/String;

.field private WHAT_UPDATE_CLOUD_TEST:I

.field private WHAT_UPDATE_STATUS:I

.field private dataCloud:[I

.field private isCloudTest:Z

.field private isEolOn:Z

.field private isUvOn:Z

.field private itemName:[Ljava/lang/String;

.field private itemName_ADI:[Ljava/lang/String;

.field private itemName_MAXIM:[Ljava/lang/String;

.field private itemName_MAXIM_Cloud:[Ljava/lang/String;

.field private itemName_MAXIM_SPO2:[Ljava/lang/String;

.field private mBIOSensor:Landroid/hardware/Sensor;

.field private mBufferCloud:[Ljava/lang/String;

.field private mButton_Exit:Landroid/widget/Button;

.field private mButton_Start:Landroid/widget/Button;

.field private mCloud_Status:Z

.field private mCpuCoreNumHelper:Landroid/os/DVFSHelper;

.field private mCpuHelper:Landroid/os/DVFSHelper;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mHRMSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field private mHrm_Vendor:Ljava/lang/String;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSpecResult:Z

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_Frequency_DC_IR:Landroid/widget/TextView;

.field private mText_Frequency_DC_RED:Landroid/widget/TextView;

.field private mText_Frequency_DC_Result:Landroid/widget/TextView;

.field private mText_Frequency_Noise_IR:Landroid/widget/TextView;

.field private mText_Frequency_Noise_RED:Landroid/widget/TextView;

.field private mText_Frequency_Noise_Result:Landroid/widget/TextView;

.field private mText_Frequency_Result:Landroid/widget/TextView;

.field private mText_Frequency_Sample_IR:Landroid/widget/TextView;

.field private mText_Frequency_Sample_RED:Landroid/widget/TextView;

.field private mText_Hr_Spo2:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Peak_DC_IR:Landroid/widget/TextView;

.field private mText_Peak_DC_RED:Landroid/widget/TextView;

.field private mText_Peak_DC_Result:Landroid/widget/TextView;

.field private mText_Peak_Peak_IR:Landroid/widget/TextView;

.field private mText_Peak_Peak_RED:Landroid/widget/TextView;

.field private mText_Peak_Ratio_IR:Landroid/widget/TextView;

.field private mText_Peak_Ratio_RED:Landroid/widget/TextView;

.field private mText_Peak_Ratio_Result:Landroid/widget/TextView;

.field private mText_Peak_result:Landroid/widget/TextView;

.field private mText_Temperature:Landroid/widget/TextView;

.field private mThreeFormat:Ljava/text/DecimalFormat;

.field private mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

.field private mhtm_Status:Z

.field private txt_itemName:[Landroid/widget/TextView;

.field private txt_measure:[Landroid/widget/TextView;

.field private txt_result:[Landroid/widget/TextView;

.field private waitPopup:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    const-string v0, "UIHeartRateMeasure"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "IR Frequency"

    aput-object v1, v0, v3

    const-string v1, "IR AC Level"

    aput-object v1, v0, v4

    const-string v1, "IR DC Level"

    aput-object v1, v0, v5

    const-string v1, "IR Noise Level"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "RED DC Level"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_ADI:[Ljava/lang/String;

    .line 45
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "P_IR AC Level"

    aput-object v1, v0, v3

    const-string v1, "P_IR DC Level"

    aput-object v1, v0, v4

    const-string v1, "P_IR AC/DC Ratio"

    aput-object v1, v0, v5

    const-string v1, "F_IR Frequency"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "F_IR DC Level"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "F_IR Noise Level"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "P_RED AC Level"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "P_RED DC Level"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "P_RED AC/DC Ratio"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "F_RED Frequency"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "F_RED DC Level"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "F_RED Noise Level"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM:[Ljava/lang/String;

    .line 46
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SPO2"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_SPO2:[Ljava/lang/String;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Cloud UV Ratio"

    aput-object v1, v0, v3

    const-string v1, "IR/RED R Ratio"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_Cloud:[Ljava/lang/String;

    .line 71
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I

    .line 72
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I

    .line 73
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z

    .line 75
    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->waitPopup:Landroid/app/ProgressDialog;

    .line 76
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    .line 77
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    .line 84
    const-string v0, "UIHeartRateMeasure"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    .line 89
    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    .line 90
    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    .line 173
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    .line 97
    return-void
.end method

.method private HRM_OnOff(Z)V
    .locals 6
    .param p1, "OnOff"    # Z

    .prologue
    const v5, 0x10019

    const/4 v4, 0x2

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "HRM_OnOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HRM Sensor OnOff : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    if-eqz p1, :cond_2

    .line 431
    const-string v0, "HRM_VENDOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHrm_Vendor:Ljava/lang/String;

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHrm_Vendor:Ljava/lang/String;

    const-string v1, "ADI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBIOSensor:Landroid/hardware/Sensor;

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBIOSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v5}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBIOSensor:Landroid/hardware/Sensor;

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x1001a

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHRMSensor:Landroid/hardware/Sensor;

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBIOSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHRMSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 447
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method private ReadValueFromListener(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 773
    if-eqz p1, :cond_0

    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 774
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    .line 776
    :cond_0
    return-void
.end method

.method private StoreCloudValue()V
    .locals 4

    .prologue
    .line 468
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "StoreCloudValue"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->dataCloud:[I

    .line 472
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->dataCloud:[I

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v0

    .line 472
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 476
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private UV_OnOff(Z)V
    .locals 4
    .param p1, "OnOff"    # Z

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "UV_OnOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Sensor OnOff : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    if-eqz p1, :cond_1

    .line 457
    new-instance v0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOn()V

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOff()V

    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkCloudValue()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs_UV(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->UV_OnOff(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->StoreCloudValue()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkStatus()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->HRM_OnOff(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->runCloudTest()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->updateUI()V

    return-void
.end method

.method private checkCloudValue()V
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkCloudValue"

    const-string v2, "Cloud test values are not ready"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :goto_0
    return-void

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "checkCloudValue"

    const-string v2, "Cloud test values are ready"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z

    goto :goto_0
.end method

.method private checkPassFail()V
    .locals 2

    .prologue
    .line 739
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 746
    :goto_0
    return-void

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private checkSpec(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "mOriginalVal"    # Ljava/lang/String;
    .param p2, "mSysfsNode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 722
    const/4 v0, 0x0

    .line 723
    .local v0, "mSpecVal":[Ljava/lang/String;
    const-string v5, ","

    invoke-virtual {p2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 725
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 726
    .local v1, "mTempVal":F
    array-length v5, v0

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_0

    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    cmpg-float v5, v1, v5

    if-gtz v5, :cond_0

    .line 728
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "checkSpec"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "val : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    .end local v1    # "mTempVal":F
    :goto_0
    return v3

    .line 732
    :catch_0
    move-exception v2

    .line 733
    .local v2, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .end local v2    # "ne":Ljava/lang/NumberFormatException;
    :cond_0
    move v3, v4

    .line 735
    goto :goto_0
.end method

.method private checkStatus()V
    .locals 4

    .prologue
    .line 479
    const/4 v0, 0x0

    .line 480
    .local v0, "mStatus":Ljava/lang/String;
    const-string v1, "HRM_EOL_TEST_STATUS"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    if-eqz v0, :cond_0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "checkStatus"

    const-string v3, "Sysfs is ready"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z

    .line 487
    :goto_0
    return-void

    .line 485
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "checkStatus"

    const-string v3, "Sysfs is not ready"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initUI()V
    .locals 17

    .prologue
    .line 253
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 254
    .local v12, "testList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v1, 0x14

    .line 256
    .local v1, "Font_Size":I
    const-string v13, "HRM_VENDOR"

    invoke-static {v13}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    const-string v14, "MAXIM"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 257
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM:[Ljava/lang/String;

    array-length v13, v13

    if-ge v5, v13, :cond_0

    .line 258
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM:[Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 261
    :cond_0
    const-string v13, "SUPPORT_HRM_SPO2"

    invoke-static {v13}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 262
    const/4 v5, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_SPO2:[Ljava/lang/String;

    array-length v13, v13

    if-ge v5, v13, :cond_1

    .line 263
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_SPO2:[Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 267
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    if-eqz v13, :cond_2

    .line 268
    const/16 v1, 0x11

    .line 269
    const/4 v5, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_Cloud:[Ljava/lang/String;

    array-length v13, v13

    if-ge v5, v13, :cond_2

    .line 270
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_MAXIM_Cloud:[Ljava/lang/String;

    aget-object v13, v13, v5

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 274
    :cond_2
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v13, v13, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    .line 275
    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v5, v13, :cond_4

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "initUI"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "testItem : "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v14, v15, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    invoke-virtual {v12, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    aput-object v13, v14, v5

    .line 275
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 280
    .end local v5    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_ADI:[Ljava/lang/String;

    array-length v13, v13

    new-array v13, v13, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    .line 281
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_ADI:[Ljava/lang/String;

    array-length v13, v13

    if-ge v5, v13, :cond_4

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName_ADI:[Ljava/lang/String;

    aget-object v14, v14, v5

    aput-object v14, v13, v5

    .line 281
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 286
    :cond_4
    new-instance v8, Landroid/widget/TableLayout$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v8, v13, v14}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 287
    .local v8, "layoutParams_upper":Landroid/widget/TableLayout$LayoutParams;
    new-instance v6, Landroid/widget/TableLayout$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v6, v13, v14}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 288
    .local v6, "layoutParams":Landroid/widget/TableLayout$LayoutParams;
    new-instance v7, Landroid/widget/TableRow$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v7, v13, v14}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 290
    .local v7, "layoutParams_txt":Landroid/widget/TableRow$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    array-length v13, v13

    new-array v13, v13, [Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    .line 291
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    array-length v13, v13

    new-array v13, v13, [Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    .line 292
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    array-length v13, v13

    new-array v13, v13, [Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    .line 294
    const v13, 0x7f0b0063

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TableLayout;

    .line 296
    .local v9, "tableLayout":Landroid/widget/TableLayout;
    new-instance v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 297
    .local v2, "classification1":Landroid/widget/TextView;
    new-instance v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 298
    .local v3, "classification2":Landroid/widget/TextView;
    new-instance v4, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 300
    .local v4, "classification3":Landroid/widget/TextView;
    new-instance v11, Landroid/widget/TableRow;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 302
    .local v11, "tableRow_classification":Landroid/widget/TableRow;
    const/4 v13, 0x2

    const/4 v14, 0x2

    const/4 v15, 0x2

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v8, v13, v14, v15, v0}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 303
    invoke-virtual {v11, v8}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    const-string v13, "Item"

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    const-string v13, "Measure"

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    const-string v13, "Result"

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    const/4 v13, 0x1

    int-to-float v14, v1

    invoke-virtual {v2, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 310
    const/4 v13, 0x1

    int-to-float v14, v1

    invoke-virtual {v3, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 311
    const/4 v13, 0x1

    int-to-float v14, v1

    invoke-virtual {v4, v13, v14}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 313
    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 314
    const/4 v13, 0x1

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 315
    const/4 v13, 0x1

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setGravity(I)V

    .line 317
    const/4 v13, -0x1

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 318
    const/4 v13, -0x1

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 319
    const/4 v13, -0x1

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 321
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 322
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 323
    sget-object v13, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v14, 0x1

    invoke-static {v13, v14}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 325
    const/high16 v13, -0x1000000

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 326
    const/high16 v13, -0x1000000

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 327
    const/high16 v13, -0x1000000

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 329
    const/16 v13, -0x100

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 330
    const/16 v13, -0x100

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 331
    const/16 v13, -0x100

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 333
    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v7, v13, v14, v15, v0}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 334
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 336
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338
    invoke-virtual {v11, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 339
    invoke-virtual {v11, v3}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 340
    invoke-virtual {v11, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 342
    invoke-virtual {v9, v11}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 344
    const/4 v5, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    array-length v13, v13

    if-ge v5, v13, :cond_5

    .line 346
    const/4 v13, 0x2

    const/4 v14, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v6, v13, v14, v15, v0}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 348
    new-instance v10, Landroid/widget/TableRow;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 349
    .local v10, "tableRow":Landroid/widget/TableRow;
    invoke-virtual {v10, v6}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    new-instance v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v14, v13, v5

    .line 353
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    aget-object v14, v14, v5

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    int-to-float v15, v1

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 355
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 356
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 357
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/high16 v14, -0x1000000

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 358
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    sget-object v14, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v15, 0x1

    invoke-static {v14, v15}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 359
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v13, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_itemName:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v10, v13}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 362
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    new-instance v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v14, v13, v5

    .line 363
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    int-to-float v15, v1

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 364
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 365
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 366
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/high16 v14, -0x1000000

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 367
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v13, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v10, v13}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    new-instance v14, Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v14, v13, v5

    .line 371
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    int-to-float v15, v1

    invoke-virtual {v13, v14, v15}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 372
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setGravity(I)V

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const/4 v14, -0x1

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 374
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    const v14, -0xffff01

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 375
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v13, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v13, v13, v5

    invoke-virtual {v10, v13}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 378
    invoke-virtual {v9, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 344
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_5

    .line 382
    .end local v10    # "tableRow":Landroid/widget/TableRow;
    :cond_5
    const v13, 0x7f0b0065

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;

    .line 383
    const v13, 0x7f0b0066

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Exit:Landroid/widget/Button;

    .line 384
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Exit:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    const v13, 0x7f0b0067

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    .line 389
    return-void
.end method

.method private registerSysfs(Z)V
    .locals 3
    .param p1, "mStatus"    # Z

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "registerSysfs"

    const-string v2, "registerSysfs Node"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    if-eqz p1, :cond_0

    .line 394
    const-string v0, "HRM_HR_RANGE2"

    const-string v1, "48"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 395
    const-string v0, "HRM_LED_CURRENT"

    const-string v1, "119"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 396
    const-string v0, "HRM_MODE_IR"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 397
    const-string v0, "HRM_MODE_R"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 398
    const-string v0, "HRM_EOL_TEST"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isEolOn:Z

    .line 404
    :goto_0
    return-void

    .line 401
    :cond_0
    const-string v0, "HRM_EOL_TEST"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 402
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isEolOn:Z

    goto :goto_0
.end method

.method private registerSysfs_UV(Z)V
    .locals 4
    .param p1, "mStatus"    # Z

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "registerSysfs_UV"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerSysfs UV node - mStatus:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    if-eqz p1, :cond_0

    .line 409
    const-string v0, "HRM_LED_CURRENT2"

    const-string v1, "255"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 410
    const-string v0, "UV_SENSOR_EOL_TEST"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 411
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isUvOn:Z

    .line 416
    :goto_0
    return-void

    .line 413
    :cond_0
    const-string v0, "UV_SENSOR_EOL_TEST"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isUvOn:Z

    goto :goto_0
.end method

.method private runCloudTest()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "runCloudTest"

    const-string v2, "start cloud detection test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->UV_OnOff(Z)V

    .line 219
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs_UV(Z)V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mBufferCloud:[Ljava/lang/String;

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 223
    return-void
.end method

.method private setPreCondition()V
    .locals 4

    .prologue
    .line 208
    const-string v1, "HRM_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "HRMSensorName":Ljava/lang/String;
    const-string v1, "MAX86902"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setPreCondition"

    const-string v3, "Enable to Cloud Detection Test"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_0
    return-void
.end method

.method private updateUI()V
    .locals 12

    .prologue
    .line 490
    const/4 v1, 0x0

    .line 491
    .local v1, "data":[Ljava/lang/String;
    const-string v3, ""

    .line 493
    .local v3, "mTempData":Ljava/lang/String;
    const-string v7, "HRM_EOL_TEST_RESULT"

    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 494
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 495
    const/16 v7, 0x64

    new-array v4, v7, [F

    .line 497
    .local v4, "mfloatData":[F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    array-length v7, v1

    if-ge v2, v7, :cond_0

    .line 498
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "updateUI"

    aget-object v9, v1, v2

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    aget-object v7, v1, v2

    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v7

    aput v7, v4, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 501
    :catch_0
    move-exception v5

    .line 502
    .local v5, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 505
    .end local v5    # "ne":Ljava/lang/NumberFormatException;
    :cond_0
    const-string v7, "HRM_VENDOR"

    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "MAXIM"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 506
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/4 v9, 0x0

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 507
    const/4 v7, 0x0

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_PEAK_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 508
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 509
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 516
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const/4 v8, 0x2

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    const/4 v7, 0x2

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_DC_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 518
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 526
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/4 v9, 0x4

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    const/4 v7, 0x4

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_RATIO_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 528
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 536
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const/4 v8, 0x6

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    const/4 v8, 0x7

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 538
    const/4 v7, 0x6

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_SAMPLE_NO"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 539
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 540
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 542
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 551
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const/16 v8, 0x8

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    const/16 v7, 0x8

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_DC_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 553
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 554
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 561
    :goto_5
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/16 v9, 0xa

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 562
    const/16 v7, 0xa

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_NOISE_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 563
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 564
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 571
    :goto_6
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    const/4 v8, 0x1

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 572
    const/4 v7, 0x1

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_PEAK_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 573
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 574
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 581
    :goto_7
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    const/4 v8, 0x3

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 582
    const/4 v7, 0x3

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_DC_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 583
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 591
    :goto_8
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/4 v9, 0x5

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    const/4 v7, 0x5

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_RATIO_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 593
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 601
    :goto_9
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    const/16 v8, 0x9

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    const/16 v7, 0x9

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_DC_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 603
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 611
    :goto_a
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/16 v9, 0xb

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 612
    const/16 v7, 0xb

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_NOISE_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 613
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 614
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 621
    :goto_b
    const/16 v0, 0xc

    .line 622
    .local v0, "Index":I
    const-string v7, "SUPPORT_HRM_SPO2"

    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 623
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/16 v9, 0xc

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 624
    const/16 v7, 0xc

    aget-object v7, v1, v7

    const-string v8, "HRM_HR_SPO2"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 625
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 632
    :goto_c
    add-int/lit8 v0, v0, 0x1

    .line 635
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    if-eqz v7, :cond_2

    .line 636
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->dataCloud:[I

    const/4 v8, 0x4

    aget v7, v7, v8

    int-to-float v7, v7

    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->dataCloud:[I

    const/4 v9, 0x5

    aget v8, v8, v9

    int-to-float v8, v8

    div-float v6, v7, v8

    .line 637
    .local v6, "uv_ratio":F
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    float-to-double v10, v6

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 639
    invoke-static {v6}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    const-string v8, "HRM_CLOUD_UV_RATIO"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 640
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 641
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 647
    :goto_d
    add-int/lit8 v0, v0, 0x1

    .line 649
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mThreeFormat:Ljava/text/DecimalFormat;

    const/16 v9, 0xc

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    const/16 v7, 0xc

    aget-object v7, v1, v7

    const-string v8, "HRM_IR_RED_R_RATIO"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 651
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 658
    :goto_e
    add-int/lit8 v0, v0, 0x1

    .line 661
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "updateUI"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cloud uv ratio:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 718
    .end local v0    # "Index":I
    .end local v6    # "uv_ratio":F
    :cond_2
    :goto_f
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkPassFail()V

    .line 719
    return-void

    .line 511
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 512
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 513
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_1

    .line 521
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 523
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_2

    .line 531
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 533
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_3

    .line 544
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 546
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 547
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 548
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_4

    .line 556
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 557
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 558
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_5

    .line 566
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 568
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_6

    .line 576
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x6

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 578
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_7

    .line 586
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x7

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 588
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_8

    .line 596
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0x8

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 598
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_9

    .line 606
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 608
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_a

    .line 616
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 617
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 618
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_b

    .line 628
    .restart local v0    # "Index":I
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 629
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 630
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_c

    .line 643
    .restart local v6    # "uv_ratio":F
    :cond_f
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 644
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 645
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_d

    .line 654
    :cond_10
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v7, v7, v0

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 656
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_e

    .line 662
    :catch_1
    move-exception v5

    .line 663
    .restart local v5    # "ne":Ljava/lang/NumberFormatException;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_f

    .line 667
    .end local v0    # "Index":I
    .end local v5    # "ne":Ljava/lang/NumberFormatException;
    .end local v6    # "uv_ratio":F
    :cond_11
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const/4 v8, 0x0

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 668
    const/4 v7, 0x0

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_SAMPLE_NO"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 669
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 670
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 677
    :goto_10
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const/4 v8, 0x1

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 678
    const/4 v7, 0x1

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_PEAK_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 679
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 687
    :goto_11
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const/4 v8, 0x2

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    const/4 v7, 0x2

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_DC_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 689
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 690
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 697
    :goto_12
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    const/4 v9, 0x3

    aget v9, v4, v9

    float-to-double v10, v9

    invoke-virtual {v8, v10, v11}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 698
    const/4 v7, 0x3

    aget-object v7, v1, v7

    const-string v8, "HRM_FREQUENCY_NOISE_IR"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 699
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 707
    :goto_13
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const/4 v8, 0x4

    aget v8, v4, v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    const/4 v7, 0x4

    aget-object v7, v1, v7

    const-string v8, "HRM_PEAK_TO_PEAK_DC_RED"

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkSpec(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 709
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const-string v8, "PASS"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 710
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_f

    .line 672
    :cond_12
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 674
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_10

    .line 682
    :cond_13
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 683
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 684
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_11

    .line 692
    :cond_14
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 694
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_12

    .line 702
    :cond_15
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 703
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 704
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_13

    .line 712
    :cond_16
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const-string v8, "FAIL"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 713
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 714
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    goto/16 :goto_f
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 758
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 250
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onClick"

    const-string v3, "start_button Click"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->HRM_OnOff(Z)V

    .line 230
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs(Z)V

    .line 232
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->itemName:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_measure:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->txt_result:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 237
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const-string v2, "Do not Move & please wait a moment"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mTextResult:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 240
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSpecResult:Z

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 244
    .end local v0    # "i":I
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onClick"

    const-string v3, "exit_button Click"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->finish()V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x7f0b0065
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 100
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    const v3, 0x7f030043

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->setContentView(I)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 103
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 104
    .local v0, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 105
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "#.##"

    invoke-direct {v3, v4, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mFormat:Ljava/text/DecimalFormat;

    .line 106
    new-instance v3, Ljava/text/DecimalFormat;

    const-string v4, "#.###"

    invoke-direct {v3, v4, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mThreeFormat:Ljava/text/DecimalFormat;

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->setPreCondition()V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->initUI()V

    .line 111
    new-instance v3, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xc

    invoke-direct {v3, v4, v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v3}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v2

    .line 113
    .local v2, "supportedCpuFreqTable":[I
    if-eqz v2, :cond_0

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    const-string v4, "CPU"

    aget v5, v2, v8

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 117
    :cond_0
    new-instance v3, Landroid/os/DVFSHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xe

    invoke-direct {v3, v4, v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v3}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v1

    .line 119
    .local v1, "supportedCpuCoreNum":[I
    if-eqz v1, :cond_1

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    const-string v4, "CORE_NUM"

    aget v5, v1, v8

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    .line 124
    :cond_1
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isUvOn:Z

    .line 125
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isEolOn:Z

    .line 126
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 166
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "CPU & Core Num LOCK release!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 143
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isEolOn:Z

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "HRM_EOL_TEST"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 145
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isEolOn:Z

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 153
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z

    if-eqz v0, :cond_3

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isSensorOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mUVSensor:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOff()V

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 158
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isUvOn:Z

    if-eqz v0, :cond_3

    .line 159
    const-string v0, "UV_SENSOR_EOL_TEST"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 160
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isUvOn:Z

    .line 163
    :cond_3
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 129
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "CPU LOCK acquire ready"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "CPU & Core Num LOCK acquire!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    .line 138
    :cond_0
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 752
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 4
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 762
    packed-switch p1, :pswitch_data_0

    .line 770
    :goto_0
    return-void

    .line 764
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->ReadValueFromListener(Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_UV_CLOUD : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 762
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
    .end packed-switch
.end method
