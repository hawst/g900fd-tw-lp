.class public Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ECGGraphActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mButton_Start:Landroid/widget/Button;

.field private mECGGraph:Lcom/sec/android/app/hwmoduletest/view/ECGGraph;

.field mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mTextView_HeartRate:Landroid/widget/TextView;

.field private registered:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 111
    const-string v0, "ECGGraphActivity"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->registered:Z

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 58
    new-instance v0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;-><init>(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;[F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;
    .param p1, "x1"    # [F

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->drawGraph([F)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mTextView_HeartRate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private drawGraph([F)V
    .locals 3
    .param p1, "input"    # [F

    .prologue
    .line 53
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mECGGraph:Lcom/sec/android/app/hwmoduletest/view/ECGGraph;

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->addValue(F)V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method private initializeSensorManager()V
    .locals 3

    .prologue
    .line 42
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    .line 43
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    .line 44
    return-void
.end method


# virtual methods
.method public deinitializeSensorManager()V
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    .line 49
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->deinitialize()V

    .line 50
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 108
    :goto_0
    return-void

    .line 93
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onClick"

    const-string v3, "start_button Click"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "Graph"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->startReceivingData(ZLjava/lang/String;)I

    move-result v0

    .line 104
    .local v0, "startResult":I
    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0065
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->setContentView(I)V

    .line 118
    const v0, 0x7f0b0068

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mECGGraph:Lcom/sec/android/app/hwmoduletest/view/ECGGraph;

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->startReceiver()V

    .line 121
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v1, "ECG"

    const-string v2, "ECG"

    const/4 v3, 0x1

    const/16 v4, 0x2729

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->initializeSensorManager()V

    .line 123
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->registered:Z

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener registered : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->registered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const v0, 0x7f0b0065

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mButton_Start:Landroid/widget/Button;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mButton_Start:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    const v0, 0x7f0b0069

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mTextView_HeartRate:Landroid/widget/TextView;

    .line 129
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 148
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 137
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 138
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->registered:Z

    if-eqz v0, :cond_0

    .line 139
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)Z

    .line 140
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->deinitializeSensorManager()V

    .line 143
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 134
    return-void
.end method

.method public startReceiver()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 33
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.app.factorytest.ECG_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    const-string v1, "com.sec.factory.app.factorytest.ECG_ALL_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 35
    const-string v1, "com.sec.factory.app.factorytest.HEART_RATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 36
    const-string v1, "com.sec.factory.app.factorytest.DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 38
    return-void
.end method
