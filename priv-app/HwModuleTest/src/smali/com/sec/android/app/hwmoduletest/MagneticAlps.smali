.class public Lcom/sec/android/app/hwmoduletest/MagneticAlps;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "MagneticAlps.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_DAC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Released:I

.field private mSensorID_Self:I

.field private mSensorID_Status:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_DAC:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTableRow_Offset_H:Landroid/widget/TableRow;

.field private mTableRow_SX:Landroid/widget/TableRow;

.field private mTableRow_SY:Landroid/widget/TableRow;

.field private mTableRow_Status:Landroid/widget/TableRow;

.field private mTableRow_Temp:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_DAC_X:Landroid/widget/TextView;

.field private mText_DAC_Y:Landroid/widget/TextView;

.field private mText_DAC_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Offset_H_X:Landroid/widget/TextView;

.field private mText_Offset_H_Y:Landroid/widget/TextView;

.field private mText_Offset_H_Z:Landroid/widget/TextView;

.field private mText_SX:Landroid/widget/TextView;

.field private mText_SY:Landroid/widget/TextView;

.field private mText_Status:Landroid/widget/TextView;

.field private mText_Temp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    const-string v0, "MagneticAlps"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSenserID:[I

    .line 60
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    .line 61
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Initialized:I

    .line 62
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Status:I

    .line 63
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_DAC:I

    .line 64
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_ADC:I

    .line 65
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Self:I

    .line 67
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Released:I

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->WHAT_UPDATE:I

    .line 115
    new-instance v0, Lcom/sec/android/app/hwmoduletest/MagneticAlps$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps$1;-><init>(Lcom/sec/android/app/hwmoduletest/MagneticAlps;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mHandler:Landroid/os/Handler;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/MagneticAlps;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticAlps;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/MagneticAlps;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticAlps;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->update()V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 125
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 126
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Initialized:Landroid/widget/TextView;

    .line 128
    const v0, 0x7f0b0137

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_Status:Landroid/widget/TableRow;

    .line 129
    const v0, 0x7f0b0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Status:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f0b016a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_SX:Landroid/widget/TableRow;

    .line 136
    const v0, 0x7f0b016b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SX:Landroid/widget/TextView;

    .line 138
    const v0, 0x7f0b016c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_SY:Landroid/widget/TableRow;

    .line 139
    const v0, 0x7f0b016d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SY:Landroid/widget/TextView;

    .line 141
    const v0, 0x7f0b016e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_DAC:Landroid/widget/TableRow;

    .line 142
    const v0, 0x7f0b016f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_X:Landroid/widget/TextView;

    .line 143
    const v0, 0x7f0b0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Y:Landroid/widget/TextView;

    .line 144
    const v0, 0x7f0b0171

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Z:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_ADC:Landroid/widget/TableRow;

    .line 147
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_X:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f0b0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Y:Landroid/widget/TextView;

    .line 149
    const v0, 0x7f0b0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Z:Landroid/widget/TextView;

    .line 151
    const v0, 0x7f0b0176

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_Offset_H:Landroid/widget/TableRow;

    .line 152
    const v0, 0x7f0b0177

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Offset_H_X:Landroid/widget/TextView;

    .line 153
    const v0, 0x7f0b0178

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Offset_H_Y:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Offset_H_Z:Landroid/widget/TextView;

    .line 156
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTextResult:Landroid/widget/TextView;

    .line 157
    return-void
.end method

.method private update()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x0

    .line 162
    .local v0, "data":[Ljava/lang/String;
    const/4 v1, 0x1

    .line 165
    .local v1, "isPass":Z
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Initialized:I

    if-ge v2, v3, :cond_0

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Initialized:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_7

    .line 170
    aget-object v2, v0, v7

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Initialized:Landroid/widget/TextView;

    aget-object v3, v0, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Initialized Return : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    :goto_1
    if-ne v1, v6, :cond_1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Status:I

    if-ge v2, v3, :cond_1

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_Status:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Status:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_9

    .line 192
    aget-object v2, v0, v6

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Status:Landroid/widget/TextView;

    aget-object v3, v0, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_1
    :goto_3
    if-ne v1, v6, :cond_2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_DAC:I

    if-ge v2, v3, :cond_2

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_DAC:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_DAC:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_b

    .line 215
    aget-object v2, v0, v6

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_X:Landroid/widget/TextView;

    aget-object v3, v0, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [DAC]X:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Z:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_2
    :goto_5
    if-ne v1, v6, :cond_3

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_ADC:I

    if-ge v2, v3, :cond_3

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_ADC:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_d

    .line 242
    aget-object v2, v0, v6

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :goto_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_X:Landroid/widget/TextView;

    aget-object v3, v0, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [ADC]X:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Z:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_3
    :goto_7
    if-ne v1, v6, :cond_4

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Self:I

    if-ge v2, v3, :cond_4

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_SX:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTableRow_SY:Landroid/widget/TableRow;

    invoke-virtual {v2, v8}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Self:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_f

    .line 270
    aget-object v2, v0, v6

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :goto_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SX:Landroid/widget/TextView;

    aget-object v3, v0, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SY:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_4
    :goto_9
    if-ne v1, v6, :cond_5

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Released:I

    if-ge v2, v3, :cond_5

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Released:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_11

    .line 306
    aget-object v2, v0, v7

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :goto_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Released Return : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_5
    :goto_b
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_12

    const-string v2, "PASS"

    :goto_c
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_13

    const v2, -0xffff01

    :goto_d
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 323
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "update"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Result:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_14

    const-string v2, "PASS"

    :goto_e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void

    .line 173
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 180
    :cond_7
    const/4 v1, 0x0

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 195
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 202
    :cond_9
    const/4 v1, 0x0

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_Status:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Status - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 218
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 228
    :cond_b
    const/4 v1, 0x0

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_DAC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "DAC - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 245
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 255
    :cond_d
    const/4 v1, 0x0

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 273
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 282
    :cond_f
    const/4 v1, 0x0

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SX:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mText_SY:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 309
    :cond_10
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 315
    :cond_11
    const/4 v1, 0x0

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Released - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 321
    :cond_12
    const-string v2, "FAIL"

    goto/16 :goto_c

    .line 322
    :cond_13
    const/high16 v2, -0x10000

    goto/16 :goto_d

    .line 323
    :cond_14
    const-string v2, "FAIL"

    goto/16 :goto_e
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f030053

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->setContentView(I)V

    .line 78
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->init()V

    .line 81
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 113
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    const-string v1, "HSCDTD004"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    const-string v1, "HSCDTD004A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    const-string v1, "HSCDTD006A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mFeature:Ljava/lang/String;

    const-string v1, "HSCDTD008A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Initialized:I

    .line 91
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Status:I

    .line 92
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_ADC:I

    .line 98
    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Initialized:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_Status:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSensorID_ADC:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSenserID:[I

    .line 104
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticAlps;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 108
    return-void
.end method
