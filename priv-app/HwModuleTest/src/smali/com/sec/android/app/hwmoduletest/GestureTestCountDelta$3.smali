.class Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;
.super Ljava/lang/Object;
.source "GestureTestCountDelta.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->deleteLogFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    # invokes: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->createArrayList()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_1

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v1, v3, [Ljava/lang/String;

    .line 127
    .local v1, "fileList":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Z

    .line 129
    .local v0, "fileChk":[Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v1, v2

    .line 131
    aput-boolean v5, v0, v2

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 134
    :cond_0
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v4, "Delete log files"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$2;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$2;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;[Z)V

    invoke-virtual {v3, v1, v0, v4}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "OK"

    new-instance v5, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;

    invoke-direct {v5, p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;[Z[Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "Cancel"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 179
    .end local v0    # "fileChk":[Z
    .end local v1    # "fileList":[Ljava/lang/String;
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 177
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "There is not any log file."

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
