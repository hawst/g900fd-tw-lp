.class Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;
.super Ljava/lang/Object;
.source "SensorReadManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$1;

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;-><init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 259
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 262
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 284
    :goto_0
    :pswitch_0
    return-void

    .line 264
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Accelerometer:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$102(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 267
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Barometer:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$202(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 270
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Gyro:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$302(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 273
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Light:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$402(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 276
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Magnetic:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$502(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 279
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$SensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->mOriginalData_Proximity:[F
    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->access$602(Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;[F)[F

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
