.class Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
.super Ljava/lang/Object;
.source "SensorCalculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Range"
.end annotation


# instance fields
.field private mIsSupportRange1_X:Z

.field private mIsSupportRange1_Y:Z

.field private mIsSupportRange1_Z:Z

.field private mIsSupportRange2_X:Z

.field private mIsSupportRange2_Y:Z

.field private mIsSupportRange2_Z:Z

.field private mRange1_X_Max:I

.field private mRange1_X_Min:I

.field private mRange1_Y_Max:I

.field private mRange1_Y_Min:I

.field private mRange1_Z_Max:I

.field private mRange1_Z_Min:I

.field private mRange2_X_Max:I

.field private mRange2_X_Min:I

.field private mRange2_Y_Max:I

.field private mRange2_Y_Min:I

.field private mRange2_Z_Max:I

.field private mRange2_Z_Min:I

.field private mRangeCount:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 3
    .param p1, "xMin"    # I
    .param p2, "xMax"    # I
    .param p3, "yMin"    # I
    .param p4, "yMax"    # I
    .param p5, "zMin"    # I
    .param p6, "zMax"    # I

    .prologue
    const/4 v2, 0x1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I

    .line 39
    iput p2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I

    .line 40
    iput p3, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I

    .line 41
    iput p4, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I

    .line 42
    iput p5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I

    .line 43
    iput p6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I

    .line 44
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRangeCount:I

    .line 46
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I

    if-eq v0, v1, :cond_0

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z

    .line 50
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I

    if-eq v0, v1, :cond_1

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z

    .line 54
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I

    if-eq v0, v1, :cond_2

    .line 55
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z

    .line 57
    :cond_2
    return-void
.end method

.method public constructor <init>(IIIIIIIIIIII)V
    .locals 3
    .param p1, "range1_X_Min"    # I
    .param p2, "range1_X_Max"    # I
    .param p3, "range1_Y_Min"    # I
    .param p4, "range1_Y_Max"    # I
    .param p5, "range1_Z_Min"    # I
    .param p6, "range1_Z_Max"    # I
    .param p7, "range2_X_Min"    # I
    .param p8, "range2_X_Max"    # I
    .param p9, "range2_Y_Min"    # I
    .param p10, "range2_Y_Max"    # I
    .param p11, "range2_Z_Min"    # I
    .param p12, "range2_Z_Max"    # I

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    .line 63
    iput p7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Min:I

    .line 64
    iput p8, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Max:I

    .line 65
    iput p9, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Min:I

    .line 66
    iput p10, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Max:I

    .line 67
    iput p11, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Min:I

    .line 68
    iput p12, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Max:I

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRangeCount:I

    .line 71
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Max:I

    if-eq v0, v1, :cond_0

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_X:Z

    .line 75
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Max:I

    if-eq v0, v1, :cond_1

    .line 76
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Y:Z

    .line 79
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Min:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Max:I

    if-eq v0, v1, :cond_2

    .line 80
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Z:Z

    .line 82
    :cond_2
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRangeCount:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_X:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Min:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Max:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Y:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Min:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Max:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Z:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Min:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Max:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I

    return v0
.end method
