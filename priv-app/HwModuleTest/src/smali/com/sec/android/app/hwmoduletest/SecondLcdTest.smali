.class public Lcom/sec/android/app/hwmoduletest/SecondLcdTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SecondLcdTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "2NDLCDTEST"


# instance fields
.field private HEIGHT_BASIS:I

.field private HEIGHT_BASIS_CROSS:I

.field protected KEY_TIMEOUT:I

.field protected KEY_TIMER_EXPIRED:I

.field protected MILLIS_IN_SEC:I

.field private final REQUEST_GHOST_TEST:I

.field private SIZE_RECT:I

.field private WIDTH_BASIS:I

.field private WIDTH_BASIS_CROSS:I

.field private dialog_showing:Z

.field private draw:[[Z

.field private drawCross:[Z

.field private isDrawArea:[[Z

.field private isNotGhostCheck:Z

.field private isTouchAction_pass_hovering:Z

.field private issideDrawDone:Z

.field private mBottommostOfMatrix:I

.field private mCurrentTime:J

.field private mHandler:Landroid/os/Handler;

.field private mIsPressedBackkey:Z

.field private mLeftmostOfMatrix:I

.field private mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

.field private mOldHoveringSetting:I

.field private mRightmostOfMatrix:I

.field private mTSPHover:Ljava/lang/String;

.field private mTestCase:Ljava/lang/String;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mTopmostOfMatrix:I

.field private passFlag:I

.field private remoteCall:Z

.field private sidedraw:[[Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    const-string v0, "SecondLcdTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 39
    const/16 v0, 0x18

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    .line 40
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    .line 41
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS_CROSS:I

    .line 42
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS_CROSS:I

    .line 43
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->REQUEST_GHOST_TEST:I

    .line 44
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I

    .line 45
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->SIZE_RECT:I

    .line 60
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mCurrentTime:J

    .line 62
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->KEY_TIMER_EXPIRED:I

    .line 63
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->MILLIS_IN_SEC:I

    .line 64
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->KEY_TIMEOUT:I

    .line 67
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mHandler:Landroid/os/Handler;

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->remoteCall:Z

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mIsPressedBackkey:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->dialog_showing:Z

    .line 72
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isTouchAction_pass_hovering:Z

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isNotGhostCheck:Z

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z

    .line 76
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mOldHoveringSetting:I

    .line 187
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mTimerHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isPassSide()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I

    return v0
.end method

.method static synthetic access$808(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->remoteCall:Z

    return v0
.end method

.method private decideRemote()V
    .locals 3

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 126
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RemoteCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->remoteCall:Z

    .line 127
    return-void
.end method

.method private fillUpMatrix()V
    .locals 4

    .prologue
    .line 130
    const/4 v1, 0x0

    .local v1, "row":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    if-ge v1, v2, :cond_2

    .line 131
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    if-ge v0, v2, :cond_1

    .line 132
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isNeededCheck(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x1

    aput-boolean v3, v2, v0

    .line 131
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 135
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isDrawArea:[[Z

    aget-object v2, v2, v1

    const/4 v3, 0x0

    aput-boolean v3, v2, v0

    goto :goto_2

    .line 130
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "column":I
    :cond_2
    return-void
.end method

.method private initGridSettings()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->draw:[[Z

    .line 116
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isDrawArea:[[Z

    .line 117
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mTopmostOfMatrix:I

    .line 118
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mBottommostOfMatrix:I

    .line 119
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mLeftmostOfMatrix:I

    .line 120
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mRightmostOfMatrix:I

    .line 121
    const/4 v0, 0x2

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z

    .line 122
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mRightmostOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPassSide()Z
    .locals 4

    .prologue
    .line 152
    const/4 v1, 0x1

    .line 153
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    if-ge v0, v2, :cond_3

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z

    const/4 v3, 0x0

    aget-object v2, v2, v3

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z

    const/4 v3, 0x1

    aget-object v2, v2, v3

    aget-boolean v2, v2, v0

    if-nez v2, :cond_2

    .line 155
    :cond_0
    if-eqz v1, :cond_1

    :cond_1
    const/4 v1, 0x0

    .line 153
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_3
    return v1
.end method

.method private setGridSizebyModel()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 99
    .local v3, "r":Landroid/content/res/Resources;
    const/4 v4, 0x5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->SIZE_RECT:I

    int-to-float v5, v5

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 100
    .local v2, "pxsize":F
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "JYP LOG======before WIDTH_BASIS=HEIGHT_BASIS====>,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v4, "TSP_X_AXIS_CHANNEL"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    .line 102
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    if-gez v4, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 104
    .local v1, "nScreenWidth":I
    int-to-float v4, v1

    div-float/2addr v4, v2

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    .line 106
    .end local v1    # "nScreenWidth":I
    :cond_0
    const-string v4, "TSP_Y_AXIS_CHANNEL"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    .line 107
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    if-gez v4, :cond_1

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 109
    .local v0, "nScreenHeight":I
    int-to-float v4, v0

    div-float/2addr v4, v2

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    .line 111
    .end local v0    # "nScreenHeight":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "JYP LOG======after WIDTH_BASIS=HEIGHT_BASIS====>,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->decideRemote()V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "UISpenAccuracyTest is created"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->setGridSizebyModel()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->initGridSettings()V

    .line 90
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;Landroid/content/Context;)V

    invoke-static {p0, v0}, Lcom/sec/android/app/dummy/SlookCocktailSubWindow;->setSubContentView(Landroid/app/Activity;Landroid/view/View;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->fillUpMatrix()V

    .line 94
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 162
    const/4 v2, 0x3

    if-ne p1, v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v1

    .line 164
    :cond_1
    const/4 v2, 0x4

    if-ne p1, v2, :cond_3

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onKeyDown"

    const-string v4, "This is back_key"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mIsPressedBackkey:Z

    if-nez v2, :cond_2

    .line 167
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 168
    .local v0, "rightNow":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mCurrentTime:J

    .line 169
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mIsPressedBackkey:Z

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->startTimer()V

    goto :goto_0

    .line 172
    .end local v0    # "rightNow":Ljava/util/Calendar;
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mIsPressedBackkey:Z

    .line 173
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 174
    .restart local v0    # "rightNow":Ljava/util/Calendar;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onKeyDown"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rightNow.getTimeInMillis() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mCurrentTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mCurrentTime:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mCurrentTime:J

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->KEY_TIMEOUT:I

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->MILLIS_IN_SEC:I

    mul-int/2addr v6, v7

    int-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->finish()V

    goto :goto_0

    .line 183
    .end local v0    # "rightNow":Ljava/util/Calendar;
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onStop()V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->finish()V

    .line 149
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->mTimerHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->KEY_TIMER_EXPIRED:I

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->KEY_TIMEOUT:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->MILLIS_IN_SEC:I

    mul-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 198
    return-void
.end method
