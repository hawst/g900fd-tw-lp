.class Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;
.super Landroid/os/Handler;
.source "HrmEolTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HrmEolTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v4, 0x64

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$100(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkStatus()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$200(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$300(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_STATUS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I

    move-result v1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mhtm_Status:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$102(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)Z

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$400(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->HRM_OnOff(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$500(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->isCloudTest:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$600(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->runCloudTest()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$700(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    goto :goto_0

    .line 186
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$800(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->updateUI()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$900(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    goto :goto_0

    .line 190
    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1100(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->checkCloudValue()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1200(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$300(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->WHAT_UPDATE_CLOUD_TEST:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1000(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)I

    move-result v1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 195
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->registerSysfs_UV(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->UV_OnOff(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1400(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mCloud_Status:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1102(Lcom/sec/android/app/hwmoduletest/HrmEolTest;Z)Z

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->StoreCloudValue()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$1500(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->mButton_Start:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$800(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmEolTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HrmEolTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmEolTest;->updateUI()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmEolTest;->access$900(Lcom/sec/android/app/hwmoduletest/HrmEolTest;)V

    goto :goto_0
.end method
