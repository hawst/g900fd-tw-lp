.class Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;
.super Ljava/lang/Object;
.source "GyroscopeGraphActivity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$1;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 51
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 54
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 59
    :goto_0
    return-void

    .line 56
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mGyroGraph:Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;->addValue(FFF)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
