.class public Lcom/sec/android/app/hwmoduletest/ColorTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ColorTest.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "ColorTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ColorTest;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "color"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 23
    .local v1, "color":I
    const/4 v2, 0x0

    .line 24
    .local v2, "hasCocktailBar":Z
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ColorTest;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.feature.cocktailbar"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    .line 25
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ColorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "onCreate"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "hasCocktailBar="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ColorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "onCreate"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Color="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    new-instance v3, Landroid/view/View;

    invoke-direct {v3, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 28
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 29
    if-eqz v2, :cond_0

    .line 30
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 31
    .local v0, "background2ndLayout":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/sec/android/app/dummy/SlookCocktailSubWindow;->setSubContentView(Landroid/app/Activity;Landroid/view/View;)V

    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 34
    .end local v0    # "background2ndLayout":Landroid/view/View;
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 35
    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/ColorTest;->setContentView(Landroid/view/View;)V

    .line 36
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ColorTest;->finish()V

    .line 41
    const/4 v0, 0x1

    return v0
.end method
