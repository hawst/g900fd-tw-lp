.class final Lcom/sec/android/app/hwmoduletest/AirMotionTest$4;
.super Ljava/lang/Object;
.source "AirMotionTest.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AirMotionTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 184
    # getter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mIsTesting:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$600()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    # getter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MAX88922"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    # invokes: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->setTextViewAndLogAsMAX88922(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$800(Lcom/samsung/android/sensorhub/SensorHubEvent;)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    # getter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mSensorName:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$700()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TMG3992"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    # invokes: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->setTextViewAndLogAsTMG3992(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$900(Lcom/samsung/android/sensorhub/SensorHubEvent;)V

    goto :goto_0
.end method
