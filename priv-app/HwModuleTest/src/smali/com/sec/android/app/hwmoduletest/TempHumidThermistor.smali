.class public Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TempHumidThermistor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;,
        Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;
    }
.end annotation


# instance fields
.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mTimer:Ljava/util/Timer;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private thermistor_SensorHub_BATT:Ljava/lang/String;

.field private thermistor_SensorHub_CHG:Ljava/lang/String;

.field private thermistor_ap:Ljava/lang/String;

.field private thermistor_batt:Ljava/lang/String;

.field private thermistor_pam:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const-string v0, "TempHumidThermistor"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mHandler:Landroid/os/Handler;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I

    .line 37
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mTimer:Ljava/util/Timer;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I

    return v0
.end method

.method static synthetic access$004(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_ap:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->fixedTemp(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_pam:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_batt:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_CHG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_CHG:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_BATT:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->thermistor_SensorHub_BATT:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

    return-object v0
.end method

.method private displayValue()V
    .locals 6

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$1;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2710

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 132
    return-void
.end method

.method private fixedTemp(I)Ljava/lang/String;
    .locals 4
    .param p1, "temp"    # I

    .prologue
    .line 74
    div-int/lit8 v0, p1, 0xa

    .line 75
    .local v0, "tens":I
    new-instance v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0xa

    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 135
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

    const v1, 0x7f03007b

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

    .line 136
    const v0, 0x7f0b024e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mListView:Landroid/widget/ListView;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    return-void
.end method

.method private startTempHumidDisplay()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->displayValue()V

    .line 66
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v2, 0x7f03007a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->setContentView(I)V

    .line 48
    const-string v2, "IS_SUPPORT_SENSOR_HUB_THERMS"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 49
    const v2, 0x7f0b024c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 50
    .local v0, "tv4":Landroid/widget/TextView;
    const v2, 0x7f0b024d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 51
    .local v1, "tv5":Landroid/widget/TextView;
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    .end local v0    # "tv4":Landroid/widget/TextView;
    .end local v1    # "tv5":Landroid/widget/TextView;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->initTextView()V

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->startTempHumidDisplay()V

    .line 57
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 71
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 61
    return-void
.end method
