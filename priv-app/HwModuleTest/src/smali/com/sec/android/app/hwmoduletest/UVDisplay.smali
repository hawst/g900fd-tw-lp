.class public Lcom/sec/android/app/hwmoduletest/UVDisplay;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "UVDisplay.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;,
        Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;
    }
.end annotation


# static fields
.field private static final ADI:Ljava/lang/String; = "ADI"

.field private static final CLASS_NAME:Ljava/lang/String; = "UVDisplay"

.field private static final INTERVAL:I = 0x3e8

.field private static final MAXIM:Ljava/lang/String; = "MAXIM"

.field private static final STM:Ljava/lang/String; = "STMicroelectronics"


# instance fields
.field private Count:I

.field private OnOff:Z

.field private TableColumn:[Ljava/lang/String;

.field private btnOff:Landroid/widget/Button;

.field private btnOn:Landroid/widget/Button;

.field private mDisplayAdapter:Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mHandler:Landroid/os/Handler;

.field private mItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

.field private mTimer:Ljava/util/Timer;

.field private mValue:[Ljava/lang/String;

.field private mVendor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    const-string v0, "UVDisplay"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;

    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->OnOff:Z

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mHandler:Landroid/os/Handler;

    .line 56
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I

    .line 63
    return-void
.end method

.method private Calculate_UVcal(F)Ljava/lang/String;
    .locals 4
    .param p1, "index"    # F

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mFormat:Ljava/text/DecimalFormat;

    const/high16 v1, 0x41c80000    # 25.0f

    mul-float/2addr v1, p1

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "index"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mFormat:Ljava/text/DecimalFormat;

    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    const-wide/high16 v4, 0x4039000000000000L    # 25.0

    mul-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private Initialize()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;

    const-string v1, "MAXIM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    const-string v1, "ADC"

    aput-object v1, v0, v3

    const-string v1, "UV calc"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    .line 143
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;

    const-string v1, "ADI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    const-string v1, "UV A"

    aput-object v1, v0, v3

    const-string v1, "UV B"

    aput-object v1, v0, v4

    const-string v1, "UV calc"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;

    const-string v1, "STMicroelectronics"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    const-string v1, "ADC"

    aput-object v1, v0, v3

    const-string v1, "UV calc"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    goto :goto_0

    .line 141
    :cond_2
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    goto :goto_0
.end method

.method private SetTableUpper()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 159
    const v5, 0x7f0b02d2

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 160
    .local v3, "tableLayout":Landroid/widget/LinearLayout;
    new-instance v2, Landroid/widget/TableRow$LayoutParams;

    const/4 v5, -0x2

    invoke-direct {v2, v8, v5}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 162
    .local v2, "layoutParams_txt":Landroid/widget/TableRow$LayoutParams;
    invoke-virtual {v2, v7, v6, v7, v6}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 163
    new-instance v4, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 166
    .local v4, "tableRow_classification":Landroid/widget/TableRow;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    array-length v5, v5

    new-array v0, v5, [Landroid/widget/TextView;

    .line 167
    .local v0, "classification":[Landroid/widget/TextView;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 168
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v5, v0, v1

    .line 169
    aget-object v5, v0, v1

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->TableColumn:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    aget-object v5, v0, v1

    const/high16 v6, 0x41600000    # 14.0f

    invoke-virtual {v5, v7, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 171
    aget-object v5, v0, v1

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 172
    aget-object v5, v0, v1

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 173
    aget-object v5, v0, v1

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 174
    aget-object v5, v0, v1

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    aget-object v5, v0, v1

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 178
    return-void
.end method

.method private SetUI()V
    .locals 4

    .prologue
    .line 146
    const v1, 0x7f0b02d1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->btnOn:Landroid/widget/Button;

    .line 147
    const v1, 0x7f0b02d0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->btnOff:Landroid/widget/Button;

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->btnOn:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->btnOff:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->SetTableUpper()V

    .line 153
    const v1, 0x7f0b02d3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 154
    .local v0, "lv":Landroid/widget/ListView;
    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

    const v2, 0x7f030089

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mDisplayAdapter:Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mDisplayAdapter:Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I

    return v0
.end method

.method static synthetic access$004(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->OnOff:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/UVDisplay;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/UVDisplay;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/UVDisplay;F)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;
    .param p1, "x1"    # F

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mDisplayAdapter:Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private startDisplay()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 93
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mTimer:Ljava/util/Timer;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 131
    return-void
.end method


# virtual methods
.method public ReadValue(Ljava/lang/String;)V
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 222
    if-eqz p1, :cond_1

    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "arrayValue":[Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_2

    .line 225
    const-string v2, "UVDisplay"

    const-string v3, "ReadValue"

    const-string v4, "arrayValue is null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_0
    return-void

    .line 222
    .end local v0    # "arrayValue":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    .restart local v0    # "arrayValue":[Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 230
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;

    aget-object v3, v0, v1

    aput-object v3, v2, v1

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 199
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    return-void

    .line 184
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->OnOff:Z

    .line 185
    const-string v0, "UVDisplay"

    const-string v1, "onClick"

    const-string v2, "Click button on"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->OnOff:Z

    .line 189
    const-string v0, "UVDisplay"

    const-string v1, "onClick"

    const-string v2, "Click button off"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x7f0b02d0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v1, 0x7f030088

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->setContentView(I)V

    .line 69
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 70
    .local v0, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 71
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v1, v2, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mFormat:Ljava/text/DecimalFormat;

    .line 73
    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getVendorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;

    .line 75
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOff()V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 89
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 90
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOn()V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->Initialize()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->SetUI()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->startDisplay()V

    .line 84
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 204
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 0
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 208
    packed-switch p1, :pswitch_data_0

    .line 215
    :goto_0
    return-void

    .line 210
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->ReadValue(Ljava/lang/String;)V

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method
