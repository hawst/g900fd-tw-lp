.class Lcom/sec/android/app/hwmoduletest/support/Support$Values;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Values"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 855
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/lang/String;Ljava/lang/String;)F
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method static synthetic access$300(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 855
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 855
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Ljava/lang/String;Ljava/lang/String;)B
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getByte(Ljava/lang/String;Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method static synthetic access$600(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Ljava/lang/String;Ljava/lang/String;)D
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getDouble(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$800(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 855
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Ljava/lang/String;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 855
    invoke-static {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getLong(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 857
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getBoolean(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;
    .param p2, "logLevel"    # I

    .prologue
    .line 861
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;ZI)Z

    move-result v0

    return v0
.end method

.method private static getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 865
    move v1, p2

    .line 868
    .local v1, "value":Z
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 870
    const-string v2, "Support"

    const-string v3, "Values.getBoolean"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 875
    :goto_0
    return v1

    .line 871
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getBoolean(Ljava/lang/String;Ljava/lang/String;ZI)Z
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z
    .param p3, "logLevel"    # I

    .prologue
    .line 879
    move v1, p2

    .line 882
    .local v1, "value":Z
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 885
    if-lez p3, :cond_0

    .line 886
    const-string v2, "Support"

    const-string v3, "Values.getBoolean"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 892
    :cond_0
    :goto_0
    return v1

    .line 888
    :catch_0
    move-exception v0

    .line 889
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getByte(Ljava/lang/String;Ljava/lang/String;)B
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 896
    const/4 v1, 0x0

    .line 899
    .local v1, "value":B
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v1

    .line 901
    const-string v2, "Support"

    const-string v3, "Values.getByte"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 906
    :goto_0
    return v1

    .line 902
    :catch_0
    move-exception v0

    .line 903
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getDouble(Ljava/lang/String;Ljava/lang/String;)D
    .locals 7
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 1009
    const-wide/16 v2, 0x0

    .line 1012
    .local v2, "value":D
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1014
    const-string v1, "Support"

    const-string v4, "Values.getDouble"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", value="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1019
    :goto_0
    return-wide v2

    .line 1015
    :catch_0
    move-exception v0

    .line 1016
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getFloat(Ljava/lang/String;Ljava/lang/String;)F
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 995
    const/4 v1, 0x0

    .line 998
    .local v1, "value":F
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 1000
    const-string v2, "Support"

    const-string v3, "Values.getFloat"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1005
    :goto_0
    return v1

    .line 1001
    :catch_0
    move-exception v0

    .line 1002
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getInt(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 967
    const/4 v1, 0x0

    .line 970
    .local v1, "value":I
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 972
    const-string v2, "Support"

    const-string v3, "Values.getInt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 977
    :goto_0
    return v1

    .line 973
    :catch_0
    move-exception v0

    .line 974
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getLong(Ljava/lang/String;Ljava/lang/String;)J
    .locals 7
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 981
    const-wide/16 v2, 0x0

    .line 984
    .local v2, "value":J
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 986
    const-string v1, "Support"

    const-string v4, "Values.getFloat"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", value="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 991
    :goto_0
    return-wide v2

    .line 987
    :catch_0
    move-exception v0

    .line 988
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;

    .prologue
    .line 930
    const-string v1, "Unknown"

    .line 933
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 934
    const-string v2, "Support"

    const-string v3, "Values.getString"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 939
    :goto_0
    if-nez v1, :cond_0

    .line 940
    const-string v1, "Unknown"

    .line 943
    :cond_0
    return-object v1

    .line 935
    :catch_0
    move-exception v0

    .line 936
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;
    .param p2, "logLevel"    # I

    .prologue
    .line 947
    const-string v1, "Unknown"

    .line 950
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 952
    if-lez p2, :cond_0

    .line 953
    const-string v2, "Support"

    const-string v3, "Values.getString"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 959
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 960
    const-string v1, "Unknown"

    .line 963
    :cond_1
    return-object v1

    .line 955
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private static getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "resultField"    # Ljava/lang/String;
    .param p2, "isLog"    # Z

    .prologue
    .line 910
    const-string v1, "Unknown"

    .line 913
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 915
    if-eqz p2, :cond_0

    .line 916
    const-string v2, "Support"

    const-string v3, "Values.getString"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 922
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 923
    const-string v1, "Unknown"

    .line 926
    :cond_1
    return-object v1

    .line 918
    :catch_0
    move-exception v0

    .line 919
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method
