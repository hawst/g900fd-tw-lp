.class public Lcom/sec/android/app/dummy/SlookCocktailSubWindow;
.super Ljava/lang/Object;
.source "SlookCocktailSubWindow.java"


# static fields
.field public static final cocktail_bar_shift_size:I = 0x11

.field public static final cocktail_bar_size:I = 0x28


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setSubContentView(Landroid/app/Activity;I)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "layoutResID"    # I

    .prologue
    .line 11
    const-string v0, "SlookCocktailSubWindow"

    const-string v1, "setSubContentView"

    invoke-static {v0, v1}, Lcom/sec/android/app/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public static setSubContentView(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 15
    const-string v0, "SlookCocktailSubWindow"

    const-string v1, "setSubContentView"

    invoke-static {v0, v1}, Lcom/sec/android/app/dummy/GedDummyMonitor;->warnLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    return-void
.end method
