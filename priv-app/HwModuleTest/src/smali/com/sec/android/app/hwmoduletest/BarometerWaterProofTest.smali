.class public Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "BarometerWaterProofTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;,
        Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "BarometerWaterProofTest"

.field private static final FINISH_ONE_CYCLE:I = 0x2

.field private static final GET_INIT_REF:I = 0x3

.field private static final GET_READ:I = 0x0

.field private static final L_LEAK_TEXT:Ljava/lang/String; = "L-Leak : "

.field private static final S_LEAK_TEXT:Ljava/lang/String; = "S-Leak : "

.field private static final UPDATE_CURRENT:I = 0x1


# instance fields
.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

.field private mBaromSensorValues:[F

.field private mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

.field private mControlButton:Landroid/widget/Button;

.field private mCount:I

.field private mCurrentSensorValue:Landroid/widget/TextView;

.field private mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

.field private mHandler:Landroid/os/Handler;

.field private mLLeak:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field private mPressureSensor:Landroid/hardware/Sensor;

.field private mRefValues:[F

.field private mSLeak:Landroid/widget/TextView;

.field private mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

.field private mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private needToResetUart:Z

.field private runWithUartSetting:Z

.field private supportBarometer:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 458
    const-string v0, "BarometerWaterProofTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    .line 63
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F

    .line 65
    sget-object v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runWithUartSetting:Z

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->needToResetUart:Z

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->supportBarometer:Z

    .line 314
    new-instance v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    .line 459
    return-void
.end method

.method private NPixelForOneMM()F
    .locals 6

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 596
    .local v1, "r":Landroid/content/res/Resources;
    const/4 v2, 0x5

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 598
    .local v0, "nPixelForOneMM":F
    const-string v2, "BarometerWaterProofTest"

    const-string v3, "NPixelForOneMM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nPixelForOneMM = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    return v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # [F

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mRefValues:[F

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # [F

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I

    return v0
.end method

.method static synthetic access$1108(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentSensorValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runMeasureStage()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p1, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    return-object v0
.end method

.method private disableUart()V
    .locals 2

    .prologue
    .line 530
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->needToResetUart:Z

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "UART_ON_OFF"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 533
    :cond_0
    return-void
.end method

.method private enableUart()V
    .locals 2

    .prologue
    .line 523
    const-string v0, "0"

    const-string v1, "UART_ON_OFF"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    const-string v0, "UART_ON_OFF"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 525
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->needToResetUart:Z

    .line 527
    :cond_0
    return-void
.end method

.method private getWindowSize()Landroid/graphics/Point;
    .locals 6

    .prologue
    .line 585
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 587
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 588
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 589
    const-string v2, "BarometerWaterProofTest"

    const-string v3, "onCreate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCREEN_WIDTH : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", SCREEN_HEIGHT : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    return-object v1
.end method

.method private initSensor()V
    .locals 2

    .prologue
    .line 549
    new-instance v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;

    .line 550
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mPressureSensor:Landroid/hardware/Sensor;

    .line 552
    return-void
.end method

.method private initUI()V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/16 v7, 0x8

    .line 555
    new-instance v4, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    const v5, 0x7f03000a

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    invoke-direct {v4, p0, v5, v6}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    .line 557
    const v4, 0x7f0b003a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mListView:Landroid/widget/ListView;

    .line 558
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 559
    const v4, 0x7f0b0033

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentSensorValue:Landroid/widget/TextView;

    .line 560
    const v4, 0x7f0b0034

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    .line 561
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 562
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->getStageNameID()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 563
    const v4, 0x7f0b0035

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 564
    const v4, 0x7f0b0037

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;

    .line 565
    const v4, 0x7f0b0038

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSLeak:Landroid/widget/TextView;

    .line 566
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v4

    if-nez v4, :cond_0

    .line 567
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentSensorValue:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 568
    const v4, 0x7f0b0039

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableLayout;

    invoke-virtual {v4, v7}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 569
    const v4, 0x7f0b0036

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableLayout;

    invoke-virtual {v4, v7}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 570
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 572
    const/high16 v0, 0x41c80000    # 25.0f

    .line 573
    .local v0, "RADIUS":F
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->NPixelForOneMM()F

    move-result v4

    const/high16 v5, 0x41c80000    # 25.0f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 574
    .local v3, "radius":I
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getWindowSize()Landroid/graphics/Point;

    move-result-object v2

    .line 576
    .local v2, "point":Landroid/graphics/Point;
    const v4, 0x7f0b0032

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/hwmoduletest/view/Circle;

    .line 577
    .local v1, "circle":Lcom/sec/android/app/hwmoduletest/view/Circle;
    iget v4, v2, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    div-float/2addr v4, v8

    iget v5, v2, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    div-float/2addr v5, v8

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/hwmoduletest/view/Circle;->setPoint(FF)V

    .line 578
    int-to-float v4, v3

    invoke-virtual {v1, v4}, Lcom/sec/android/app/hwmoduletest/view/Circle;->setRadius(F)V

    .line 579
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/android/app/hwmoduletest/view/Circle;->setVisibility(I)V

    .line 580
    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/hwmoduletest/view/Circle;->setColor(I)V

    .line 582
    .end local v0    # "RADIUS":F
    .end local v1    # "circle":Lcom/sec/android/app/hwmoduletest/view/Circle;
    .end local v2    # "point":Landroid/graphics/Point;
    .end local v3    # "radius":I
    :cond_0
    return-void
.end method

.method private runBarometerTest()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 542
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCount:I

    .line 543
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x16

    if-ge v0, v1, :cond_0

    .line 544
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    mul-int/lit16 v2, v0, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 543
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 546
    :cond_0
    return-void
.end method

.method private runMeasureStage()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 712
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 713
    new-instance v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    const-string v1, "Release"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F

    aget v2, v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v6, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;-><init>(ILjava/lang/String;FLjava/lang/String;)V

    .line 715
    .local v0, "releaseItem":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    if-eqz v1, :cond_0

    .line 717
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F

    aget v3, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->write(IF)Z

    .line 719
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    if-eqz v1, :cond_1

    .line 720
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getAllData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->write(Ljava/lang/String;)Z

    .line 722
    :cond_1
    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->MEASURING_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 723
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runBarometerTest()V

    .line 725
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->notifyDataSetChanged()V

    .line 726
    const-string v1, "BarometerWaterProofTest"

    const-string v2, "runMeasureStage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CurrentStage = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->getStageNameID()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 728
    return-void
.end method

.method private startPressureSensor()V
    .locals 4

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mPressureSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 539
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x3

    const/4 v3, 0x0

    .line 674
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 706
    const-string v1, "BarometerWaterProofTest"

    const-string v2, "onClick"

    const-string v3, "default"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 676
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSLeak:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 680
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    if-eqz v1, :cond_1

    .line 681
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->close()Z

    .line 682
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .line 684
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 685
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 686
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 687
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 688
    sget-object v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-boolean v3, v1, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    .line 690
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mAdaptor:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->notifyDataSetChanged()V

    .line 691
    const-string v1, "BarometerWaterProofTest"

    const-string v2, "onClick"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CurrentStage = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->getStageNameID()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 695
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    if-ne v1, v2, :cond_2

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 697
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    const-string v2, "Getting Ref..."

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 698
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v6, :cond_0

    .line 699
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v3, -0x1

    invoke-virtual {v2, v6, v0, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    mul-int/lit16 v3, v0, 0x3e8

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 698
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 701
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    if-ne v1, v2, :cond_0

    .line 702
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runMeasureStage()V

    goto/16 :goto_0

    .line 674
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0034
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 463
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 466
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 469
    const-string v0, "uart_setting"

    const-string v1, "MLC"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runWithUartSetting:Z

    .line 471
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->enableUart()V

    .line 473
    :cond_1
    const-string v0, "false"

    const-string v1, "Barometer"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getEnabled(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 474
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->supportBarometer:Z

    .line 477
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->supportBarometer:Z

    if-eqz v0, :cond_3

    .line 478
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->setContentView(I)V

    .line 479
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->initUI()V

    .line 480
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->initSensor()V

    .line 482
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 502
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runWithUartSetting:Z

    if-eqz v0, :cond_0

    .line 503
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->disableUart()V

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    if-eqz v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->close()Z

    .line 513
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .line 515
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    if-eqz v0, :cond_2

    .line 516
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    .line 519
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 520
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->supportBarometer:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 497
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 498
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 486
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 487
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->supportBarometer:Z

    if-eqz v0, :cond_0

    .line 488
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->startPressureSensor()V

    .line 490
    :cond_0
    return-void
.end method

.method public runFinishStage(I)V
    .locals 6
    .param p1, "waitingTime"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 431
    const-string v0, "BarometerWaterProofTest"

    const-string v1, "runFinishStage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "waitingTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    sget-object v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->INIT_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;->close()Z

    .line 435
    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSaveData:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SaveData;

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    if-eqz v0, :cond_1

    .line 438
    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSavePreference:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 443
    if-lez p1, :cond_2

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 448
    :goto_0
    return-void

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mControlButton:Landroid/widget/Button;

    const v1, 0x7f08028b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public setLeakageResult()V
    .locals 4

    .prologue
    const v3, 0x7f070018

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->getLLeak()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mLLeak:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSLeak:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCalculatePressure:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->getSLeak()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mSLeak:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 455
    return-void
.end method
