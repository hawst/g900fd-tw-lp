.class public Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HwTestMenu"
.end annotation


# static fields
.field public static final HDMI_TYPE:Ljava/lang/String; = "18"

.field public static final LCD_TEST_GRIP:Ljava/lang/String; = "15"

.field public static final LCD_TEST_RECEIVER:Ljava/lang/String; = "07"

.field public static final LCD_TEST_TOUCH:Ljava/lang/String; = "04"

.field public static final TAG:Ljava/lang/String; = "HwTestMenu"

.field public static final TSP_CMD_X_COUNT:Ljava/lang/String; = "get_x_num"

.field public static final TSP_CMD_Y_COUNT:Ljava/lang/String; = "get_y_num"

.field public static final TSP_RETURN_VALUE_NA:Ljava/lang/String; = "NA"

.field public static final TSP_RETURN_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final TSP_RETURN_VALUE_OK:Ljava/lang/String; = "OK"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEnabled(I)Ljava/lang/String;
    .locals 4
    .param p0, "id"    # I

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    const-string v1, "action"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "enable"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueByAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTestCase(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "action"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "testcase"

    invoke-virtual {v0, v1, p0, v2}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueByAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTestMenu()[Ljava/lang/String;
    .locals 14

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v10

    const-string v11, "HwTestMenu"

    invoke-virtual {v10, v11}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getChildElementSet(Ljava/lang/String;)[Lorg/w3c/dom/Element;

    move-result-object v3

    .line 38
    .local v3, "items":[Lorg/w3c/dom/Element;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 40
    .local v9, "testList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .local v7, "parent_ID":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "parent_Name":Ljava/lang/String;
    const/4 v6, 0x0

    .line 42
    .local v6, "parent_Extra":Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Lorg/w3c/dom/Element;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_2

    aget-object v2, v0, v1

    .line 43
    .local v2, "item":Lorg/w3c/dom/Element;
    const-string v10, "enable"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 44
    .local v5, "parent_Enable":Z
    const-string v10, "Support"

    const-string v11, "getFact"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "name="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "name"

    invoke-interface {v2, v13}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", enable="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    if-eqz v5, :cond_0

    .line 48
    const-string v10, "name"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 49
    const-string v10, "action"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 50
    const-string v10, "extra"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    const-string v10, "extra"

    invoke-interface {v2, v10}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 52
    :goto_1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "null"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 50
    :cond_1
    const-string v6, "default"

    goto :goto_1

    .line 56
    .end local v2    # "item":Lorg/w3c/dom/Element;
    .end local v5    # "parent_Enable":Z
    :cond_2
    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    return-object v10
.end method

.method public static getUIRate(I)F
    .locals 7
    .param p0, "action"    # I

    .prologue
    .line 70
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v1

    const-string v2, "action"

    const-string v3, "%02d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "uirate"

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->getAttributeValueByAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "rate":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
