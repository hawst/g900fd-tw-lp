.class public Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "LowFrequencyTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final AUDIO_PATH_EAR:I = 0x2

.field public static final AUDIO_PATH_HDMI:I = 0x3

.field public static final AUDIO_PATH_OFF:I = 0x4

.field public static final AUDIO_PATH_RCV:I = 0x1

.field public static final AUDIO_PATH_SPK:I


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private m100Hz:Landroid/widget/ToggleButton;

.field private m200Hz:Landroid/widget/ToggleButton;

.field private m300Hz:Landroid/widget/ToggleButton;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    const-string v0, "LowFrequencyTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->AUDIO_PATH:[Ljava/lang/String;

    .line 36
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 131
    :cond_0
    return-void
.end method

.method private startMedia(I)V
    .locals 2
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x1

    .line 110
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->setAudioPath(I)V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->release()V

    .line 112
    invoke-static {p0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 117
    return-void
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->release()V

    .line 124
    :cond_0
    return-void
.end method

.method private updateMedia(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    move-object v1, p1

    check-cast v1, Landroid/widget/ToggleButton;

    .line 88
    .local v1, "toggle":Landroid/widget/ToggleButton;
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->stopMedia()V

    .line 90
    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 93
    .local v0, "resID":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 105
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->startMedia(I)V

    .line 107
    .end local v0    # "resID":I
    :cond_0
    return-void

    .line 95
    .restart local v0    # "resID":I
    :pswitch_0
    const v0, 0x7f050001

    .line 96
    goto :goto_0

    .line 98
    :pswitch_1
    const v0, 0x7f050002

    .line 99
    goto :goto_0

    .line 101
    :pswitch_2
    const v0, 0x7f050003

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x7f0b0165
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateToggle(I)V
    .locals 3
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m100Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0b0165

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m100Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m200Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0b0166

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m300Hz:Landroid/widget/ToggleButton;

    const v2, 0x7f0b0167

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m300Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 84
    return-void

    :cond_1
    move v0, v1

    .line 81
    goto :goto_0

    :cond_2
    move v0, v1

    .line 82
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->updateToggle(I)V

    .line 77
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->updateMedia(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->setContentView(I)V

    .line 42
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mAudioManager:Landroid/media/AudioManager;

    .line 43
    const v0, 0x7f0b0165

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m100Hz:Landroid/widget/ToggleButton;

    .line 44
    const v0, 0x7f0b0166

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m200Hz:Landroid/widget/ToggleButton;

    .line 45
    const v0, 0x7f0b0167

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m300Hz:Landroid/widget/ToggleButton;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m100Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->m300Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 61
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->stopMedia()V

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->setAudioPath(I)V

    .line 72
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 53
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->setAudioPath(I)V

    .line 57
    return-void
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 136
    return-void
.end method
