.class Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;
.super Ljava/util/TimerTask;
.source "HealthSensorTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HealthSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTask"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mIRThermometerVer:F

.field private mIsRunningTask:Z

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)V
    .locals 1

    .prologue
    .line 308
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 309
    const-string v0, "SensorTask"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->TAG:Ljava/lang/String;

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIsRunningTask:Z

    .line 366
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIRThermometerVer:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;

    .prologue
    .line 308
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->getIRThermometerSensorVersionString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->resume()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    .prologue
    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->pause()V

    return-void
.end method

.method private getIRThermometerSensorVersionString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VER : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$800(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIRThermometerVer:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private pause()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 348
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIsRunningTask:Z

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$700(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$700(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 353
    :cond_0
    return-void
.end method

.method private readToIRThermometerSensor()V
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIRThermometerVer:F

    .line 370
    return-void
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIsRunningTask:Z

    .line 345
    return-void
.end method

.method private declared-synchronized updateUI()V
    .locals 2

    .prologue
    .line 356
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$700(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 357
    monitor-exit p0

    return-void

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 318
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 329
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$000(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)B

    move-result v0

    if-nez v0, :cond_1

    .line 335
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->readToIRThermometerSensor()V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$002(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;B)B

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->updateUI()V

    goto :goto_0
.end method
