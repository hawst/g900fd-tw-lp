.class Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;
.super Ljava/lang/Object;
.source "GyroscopeDisplay.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    .prologue
    .line 241
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 243
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const v5, 0x42654ca3

    .line 246
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 251
    :goto_0
    return-void

    .line 248
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-float/2addr v1, v5

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-float/2addr v2, v5

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v3, v5

    # invokes: Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->getValueFromSensor(FFF)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->access$1000(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;FFF)V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method
