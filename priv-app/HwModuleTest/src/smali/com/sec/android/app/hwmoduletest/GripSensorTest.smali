.class public Lcom/sec/android/app/hwmoduletest/GripSensorTest;
.super Landroid/app/Activity;
.source "GripSensorTest.java"


# static fields
.field private static IS_GRIP_COUNT:I = 0x0

.field protected static final KEY_TIMEOUT:I = 0x2

.field protected static final KEY_TIMER_EXPIRED:I = 0x1

.field private static final MASK_SENSING_GRIP_OFF:Ljava/lang/String; = "000000000000"

.field private static final MASK_SENSING_GRIP_SIDE_ON:Ljava/lang/String; = "000001000000"

.field private static final MASK_SENSING_GRIP_TOP_ON:Ljava/lang/String; = "010000000000"

.field private static final MASK_SENSING_GRIP_TOP_SIDE_ON:Ljava/lang/String; = "010001000000"

.field protected static final MILLIS_IN_SEC:I = 0x3e8

.field private static final VIB_STRONG:I = 0x1

.field private static final VIB_WEAK:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final VIBRATE_TIME:I

.field private info1:Landroid/widget/TextView;

.field private info2:Landroid/widget/TextView;

.field private mBackgroudLayout1:Landroid/widget/LinearLayout;

.field private mBackgroudLayout2:Landroid/widget/LinearLayout;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentTime:J

.field private mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

.field private mIsPressedBackkey:Z

.field private mVibrator:Landroid/os/Vibrator;

.field mWl:Landroid/os/PowerManager$WakeLock;

.field private pass1:Z

.field private pass2:Z

.field private phone:Lcom/android/internal/telephony/Phone;

.field private txtgripsensor1:Landroid/widget/TextView;

.field private txtgripsensor2:Landroid/widget/TextView;

.field private working:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->working:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->pass1:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->pass2:Z

    .line 64
    const v0, 0xffff

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->VIBRATE_TIME:I

    .line 66
    const-string v0, "GripSensor Test"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->CLASS_NAME:Ljava/lang/String;

    .line 68
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mIsPressedBackkey:Z

    .line 70
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mCurrentTime:J

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->phone:Lcom/android/internal/telephony/Phone;

    .line 238
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private ActiveVibrate(I)V
    .locals 4
    .param p1, "intensity"    # I

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->startVibration(I)V

    .line 231
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 232
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/android/app/hwmoduletest/GripSensorTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 236
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->stopVibration()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;
    .param p1, "x1"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->sendToRilGripSensorStop()V

    return-void
.end method

.method private init()V
    .locals 7

    .prologue
    const v4, 0x7f08008f

    const/16 v6, 0x8

    .line 162
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 163
    .local v1, "pm":Landroid/os/PowerManager;
    const/16 v2, 0x1a

    const-string v3, "My Lock Tag"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 164
    const v2, 0x7f0b00b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->txtgripsensor1:Landroid/widget/TextView;

    .line 165
    const v2, 0x7f0b00b5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->txtgripsensor2:Landroid/widget/TextView;

    .line 166
    const v2, 0x7f0b00b3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;

    .line 167
    const v2, 0x7f0b00b6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 170
    const v2, 0x7f0b00b1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    .line 171
    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    .line 172
    const-string v2, "15"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, "gripCount":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 174
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sput v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I

    .line 176
    :cond_0
    const-string v2, "GripSensor Test"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IS_GRIP_COUNT : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    sget v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->txtgripsensor1:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->txtgripsensor2:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :cond_1
    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    .line 188
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 136
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 137
    .local v0, "gripSensorData":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    const-string v1, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    return-void
.end method

.method private sendToRilGripSensorStart()V
    .locals 3

    .prologue
    .line 143
    const-string v0, "GripSensor Test"

    const-string v1, "sendToRilGripSensorStart"

    const-string v2, "sendToRilGripSensorStart="

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 145
    return-void
.end method

.method private sendToRilGripSensorStop()V
    .locals 3

    .prologue
    .line 148
    const-string v0, "GripSensor Test"

    const-string v1, "sendToRilGripSensorStop"

    const-string v2, "sendToRilGripSensorStop="

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 150
    return-void
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 208
    const-string v1, "GripSensor Test"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 213
    .local v0, "pattern":[J
    if-nez p1, :cond_1

    .line 214
    aput-wide v6, v0, v4

    .line 215
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 221
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 222
    return-void

    .line 216
    :cond_1
    if-ne p1, v5, :cond_0

    .line 217
    aput-wide v6, v0, v4

    .line 218
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 209
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 226
    const-string v0, "GripSensor Test"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 100
    const v0, 0x7f030030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setContentView(I)V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->init()V

    .line 102
    const-string v0, "GripSensor Test"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->bindSecPhoneService()V

    .line 106
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->unbindSecPhoneService()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .line 133
    :cond_0
    return-void
.end method

.method public onExit()V
    .locals 3

    .prologue
    .line 201
    const-string v0, "GripSensor Test"

    const-string v1, "onExit"

    const-string v2, "finish"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 203
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setResult(I)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->finish()V

    .line 205
    return-void
.end method

.method public onFinish()V
    .locals 3

    .prologue
    .line 195
    const-string v0, "GripSensor Test"

    const-string v1, "onFinish"

    const-string v2, "GripSensor Test finish"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->finish()V

    .line 198
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->sendToRilGripSensorStop()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 121
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 122
    const-string v0, "GripSensor Test"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 110
    const-string v0, "GripSensor Test"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->registerReceiver()V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->sendToRilGripSensorStart()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 114
    return-void
.end method

.method public setBackgroundColor(Landroid/widget/LinearLayout;I)V
    .locals 1
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "id"    # I

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 192
    return-void
.end method
