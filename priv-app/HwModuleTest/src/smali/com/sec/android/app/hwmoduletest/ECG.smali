.class public Lcom/sec/android/app/hwmoduletest/ECG;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ECG.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static CLASS_NAME:Ljava/lang/String;


# instance fields
.field private ECG_VENDOR:Ljava/lang/String;

.field private final FONT_SIZE:I

.field private Measure:[Ljava/lang/String;

.field private Result:[Ljava/lang/String;

.field private ecg_init_progress:I

.field private itemName:[Ljava/lang/String;

.field private itemName_TI:[Ljava/lang/String;

.field item_selected:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mButton_Exit:Landroid/widget/Button;

.field private mButton_Start:Landroid/widget/Button;

.field private mECG_IMPEDANCE_Spec_Max:F

.field private mECG_IMPEDANCE_Spec_Min:F

.field mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mFormat2:Ljava/text/DecimalFormat;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSpecResult:Z

.field private mTextResult:Landroid/widget/TextView;

.field mode_num:I

.field private registered:Z

.field test_state:Ljava/lang/String;

.field private txt_itemName:[Landroid/widget/TextView;

.field private txt_measure:[Landroid/widget/TextView;

.field private txt_result:[Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-string v0, "ECG"

    sput-object v0, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 304
    const-string v0, "ECG"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 40
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->registered:Z

    .line 42
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I

    .line 43
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->FONT_SIZE:I

    .line 45
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F

    .line 46
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 52
    const/16 v0, 0x15

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "D_range(0mv)"

    aput-object v1, v0, v3

    const-string v1, "D_range(+300mV)"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "D_range(-300mV)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Frequency(0.5Hz)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Frequency(1Hz)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Frequency(2Hz)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Frequency(5Hz)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Frequency(10Hz)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Frequency(20Hz)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Frequency(40Hz)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "HR(50bpm)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "HR(70bpm)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "HR(100bpm)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "HR(200bpm)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "System noise"

    aput-object v2, v0, v1

    const-string v1, "Impedance_B(0mV)"

    aput-object v1, v0, v5

    const/16 v1, 0x10

    const-string v2, "Impedance_A(0mV)"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Impedance_B(+300mV)"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Impedance_A(+300mV)"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Impedance_B(-300mV)"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Impedance_A(-300mV)"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName_TI:[Ljava/lang/String;

    .line 58
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    .line 59
    const-string v0, "READY"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    .line 60
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    .line 72
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z

    .line 75
    const-string v0, "TI"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ECG_VENDOR:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/sec/android/app/hwmoduletest/ECG$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/ECG$1;-><init>(Lcom/sec/android/app/hwmoduletest/ECG;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 305
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/ECG;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/hwmoduletest/ECG;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;
    .param p1, "x1"    # F

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Max:F

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/ECG;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I

    return v0
.end method

.method static synthetic access$1108(Lcom/sec/android/app/hwmoduletest/ECG;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ecg_init_progress:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/ECG;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat2:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/ECG;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/ECG;)[Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/hwmoduletest/ECG;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/ECG;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/ECG;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/hwmoduletest/ECG;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ECG;
    .param p1, "x1"    # F

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mECG_IMPEDANCE_Spec_Min:F

    return p1
.end method

.method private checkPassFail()V
    .locals 2

    .prologue
    .line 594
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z

    if-eqz v0, :cond_0

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 601
    :goto_0
    return-void

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private initUI()V
    .locals 15

    .prologue
    .line 385
    :try_start_0
    const-string v11, "SENSOR_VENDOR_ECG"

    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ECG_VENDOR:Ljava/lang/String;

    .line 386
    sget-object v11, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "initUI"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "VENDOR : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ECG_VENDOR:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :goto_0
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->ECG_VENDOR:Ljava/lang/String;

    const-string v12, "TI"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 391
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName_TI:[Ljava/lang/String;

    array-length v11, v11

    new-array v11, v11, [Ljava/lang/String;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    .line 392
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName_TI:[Ljava/lang/String;

    array-length v11, v11

    if-ge v3, v11, :cond_0

    .line 393
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    iget-object v12, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName_TI:[Ljava/lang/String;

    aget-object v12, v12, v3

    aput-object v12, v11, v3

    .line 392
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 387
    .end local v3    # "i":I
    :catch_0
    move-exception v7

    .line 388
    .local v7, "ne":Ljava/lang/NullPointerException;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 397
    .end local v7    # "ne":Ljava/lang/NullPointerException;
    :cond_0
    new-instance v6, Landroid/widget/TableLayout$LayoutParams;

    const/4 v11, -0x1

    const/4 v12, -0x2

    invoke-direct {v6, v11, v12}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 398
    .local v6, "layoutParams_upper":Landroid/widget/TableLayout$LayoutParams;
    new-instance v4, Landroid/widget/TableLayout$LayoutParams;

    const/4 v11, -0x1

    const/4 v12, -0x2

    invoke-direct {v4, v11, v12}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 399
    .local v4, "layoutParams":Landroid/widget/TableLayout$LayoutParams;
    new-instance v5, Landroid/widget/TableRow$LayoutParams;

    const/4 v11, -0x1

    const/4 v12, -0x2

    invoke-direct {v5, v11, v12}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 401
    .local v5, "layoutParams_txt":Landroid/widget/TableRow$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    array-length v11, v11

    new-array v11, v11, [Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    .line 402
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    array-length v11, v11

    new-array v11, v11, [Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    .line 403
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    array-length v11, v11

    new-array v11, v11, [Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    .line 405
    const v11, 0x7f0b0063

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/ECG;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TableLayout;

    .line 407
    .local v8, "tableLayout":Landroid/widget/TableLayout;
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 408
    .local v0, "classification1":Landroid/widget/TextView;
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 409
    .local v1, "classification2":Landroid/widget/TextView;
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 411
    .local v2, "classification3":Landroid/widget/TextView;
    new-instance v10, Landroid/widget/TableRow;

    invoke-direct {v10, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 413
    .local v10, "tableRow_classification":Landroid/widget/TableRow;
    const/4 v11, 0x2

    const/4 v12, 0x2

    const/4 v13, 0x2

    const/4 v14, 0x2

    invoke-virtual {v6, v11, v12, v13, v14}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 414
    invoke-virtual {v10, v6}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 416
    const-string v11, "Item"

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    const-string v11, "Measure"

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 418
    const-string v11, "Result"

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 420
    const/4 v11, 0x1

    const/high16 v12, 0x41700000    # 15.0f

    invoke-virtual {v0, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 421
    const/4 v11, 0x1

    const/high16 v12, 0x41700000    # 15.0f

    invoke-virtual {v1, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 422
    const/4 v11, 0x1

    const/high16 v12, 0x41700000    # 15.0f

    invoke-virtual {v2, v11, v12}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 424
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 425
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 426
    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setGravity(I)V

    .line 428
    const/4 v11, -0x1

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 429
    const/4 v11, -0x1

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 430
    const/4 v11, -0x1

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 432
    sget-object v11, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 433
    sget-object v11, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 434
    sget-object v11, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v12, 0x1

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 436
    const/high16 v11, -0x1000000

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 437
    const/high16 v11, -0x1000000

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 438
    const/high16 v11, -0x1000000

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 440
    const/16 v11, -0x100

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 441
    const/16 v11, -0x100

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 442
    const/16 v11, -0x100

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 444
    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    invoke-virtual {v5, v11, v12, v13, v14}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 445
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 446
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 447
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    invoke-virtual {v10, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 450
    invoke-virtual {v10, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 451
    invoke-virtual {v10, v2}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 453
    invoke-virtual {v8, v10}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 455
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    array-length v11, v11

    if-ge v3, v11, :cond_1

    .line 457
    const/4 v11, 0x2

    const/4 v12, 0x0

    const/4 v13, 0x2

    const/4 v14, 0x2

    invoke-virtual {v4, v11, v12, v13, v14}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 459
    new-instance v9, Landroid/widget/TableRow;

    invoke-direct {v9, p0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 460
    .local v9, "tableRow":Landroid/widget/TableRow;
    invoke-virtual {v9, v4}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 462
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    new-instance v12, Landroid/widget/TextView;

    invoke-direct {v12, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v12, v11, v3

    .line 463
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    iget-object v12, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    aget-object v12, v12, v3

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    const/high16 v13, 0x41700000    # 15.0f

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 465
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setGravity(I)V

    .line 466
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 467
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/high16 v12, -0x1000000

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 468
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    sget-object v12, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 469
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 470
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v9, v11}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 472
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    new-instance v12, Landroid/widget/TextView;

    invoke-direct {v12, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v12, v11, v3

    .line 473
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    const/high16 v13, 0x41700000    # 15.0f

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 474
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setGravity(I)V

    .line 475
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 476
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/high16 v12, -0x1000000

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 477
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 478
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_measure:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v9, v11}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 480
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    new-instance v12, Landroid/widget/TextView;

    invoke-direct {v12, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v12, v11, v3

    .line 481
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    const/high16 v13, 0x41700000    # 15.0f

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 482
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setGravity(I)V

    .line 483
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 484
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    const v12, -0xffff01

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 485
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 486
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_result:[Landroid/widget/TextView;

    aget-object v11, v11, v3

    invoke-virtual {v9, v11}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 488
    invoke-virtual {v8, v9}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 455
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 492
    .end local v9    # "tableRow":Landroid/widget/TableRow;
    :cond_1
    const v11, 0x7f0b0065

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/ECG;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    .line 493
    const v11, 0x7f0b0066

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/ECG;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Exit:Landroid/widget/Button;

    .line 494
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 495
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v11, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 498
    const v11, 0x7f0b0067

    invoke-virtual {p0, v11}, Lcom/sec/android/app/hwmoduletest/ECG;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    .line 499
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 500
    return-void
.end method

.method private initializeSensorManager()V
    .locals 3

    .prologue
    .line 309
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    .line 310
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->initialize(Landroid/content/Context;Lcom/sec/android/service/health/sensor/manager/ISensorListener;)V

    .line 311
    return-void
.end method


# virtual methods
.method public deinitializeSensorManager()V
    .locals 1

    .prologue
    .line 315
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    .line 316
    .local v0, "abs":Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;
    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/BaseSensorManager;->deinitialize()V

    .line 317
    return-void
.end method

.method public getXY(Landroid/view/View;Ljava/lang/String;)I
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 503
    const-string v9, "window"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/hwmoduletest/ECG;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 504
    .local v3, "mWm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getWidth()I

    move-result v2

    .line 505
    .local v2, "mScreenWidth":I
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 507
    .local v1, "mScreenHeight":I
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    .line 508
    .local v4, "parentView":Landroid/view/View;
    const/4 v5, 0x0

    .local v5, "x1":I
    const/4 v6, 0x0

    .line 509
    .local v6, "x2":I
    const/4 v7, 0x0

    .local v7, "y1":I
    const/4 v8, 0x0

    .line 510
    .local v8, "y2":I
    const/4 v0, 0x0

    .line 512
    .local v0, "chk":Z
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    sub-int v6, v2, v9

    .line 513
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    .line 515
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 516
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v9

    add-int/2addr v5, v9

    .line 517
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v9

    add-int/2addr v7, v9

    .line 519
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "view":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .line 520
    .restart local p1    # "view":Landroid/view/View;
    if-ne v4, p1, :cond_0

    .line 521
    const/4 v0, 0x1

    goto :goto_0

    .line 524
    :cond_1
    add-int/2addr v8, v7

    .line 526
    sget-object v9, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "onTouchEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "x1 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " / y : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    sget-object v9, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "onTouchEvent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "x2 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " / y2 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v9, "x1"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 539
    .end local v5    # "x1":I
    :goto_1
    return v5

    .line 531
    .restart local v5    # "x1":I
    :cond_2
    const-string v9, "x2"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move v5, v6

    .line 532
    goto :goto_1

    .line 533
    :cond_3
    const-string v9, "y1"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    move v5, v7

    .line 534
    goto :goto_1

    .line 535
    :cond_4
    const-string v9, "y2"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    move v5, v8

    .line 536
    goto :goto_1

    .line 539
    :cond_5
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 360
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 381
    :goto_0
    return-void

    .line 362
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onClick"

    const-string v3, "start_button Click"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const-string v2, "Please wait a moment"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mTextResult:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 367
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mSpecResult:Z

    .line 369
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->startReceivingData(ZLjava/lang/String;)I

    move-result v0

    .line 370
    .local v0, "startResult":I
    const-string v1, "WAIT"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    goto :goto_0

    .line 374
    .end local v0    # "startResult":I
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onClick"

    const-string v3, "exit_button Click"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->finish()V

    goto :goto_0

    .line 360
    :pswitch_data_0
    .packed-switch 0x7f0b0065
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 320
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 322
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat:Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 325
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat2:Ljava/text/DecimalFormat;

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mFormat2:Ljava/text/DecimalFormat;

    const-string v1, "0.000"

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->startReceiver()V

    .line 329
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    const-string v1, "ECG"

    const-string v2, "ECG"

    const/4 v3, 0x1

    const/16 v4, 0x2729

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;-><init>(Ljava/lang/String;Ljava/lang/String;IIILandroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    .line 330
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->initializeSensorManager()V

    .line 331
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->registerListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorDataListener;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->registered:Z

    .line 332
    sget-object v0, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registerListener registered : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/ECG;->registered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECG;->setContentView(I)V

    .line 336
    const v0, 0x7f0b0194

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ECG;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mProgressBar:Landroid/widget/ProgressBar;

    .line 337
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->initUI()V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 339
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 355
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 356
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 346
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 347
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ECG;->registered:Z

    if-eqz v0, :cond_0

    .line 348
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mEcgDevice:Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->unregisterListener(Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorDevice;Ljava/lang/Integer;)Z

    .line 349
    invoke-static {}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->getInstance()Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/sensor/manager/InternalAndroidSensorManager;->stopReceivingData()I

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ECG;->deinitializeSensorManager()V

    .line 352
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 342
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 343
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 544
    sget-object v5, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onTouchEvent"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    const/4 v3, 0x0

    .line 547
    .local v3, "x":F
    const/4 v4, 0x0

    .line 548
    .local v4, "y":F
    const/4 v2, 0x0

    .line 549
    .local v2, "index":I
    const/4 v0, 0x0

    .line 551
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 553
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_2

    .line 556
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 557
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 559
    sget-object v5, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onTouchEvent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / y : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->test_state:Ljava/lang/String;

    const-string v6, "READY"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 563
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    invoke-virtual {v5, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 564
    iput-boolean v10, p0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    .line 566
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->itemName:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 567
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 568
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 570
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const-string v6, "x1"

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/hwmoduletest/ECG;->getXY(Landroid/view/View;Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const-string v6, "x2"

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/hwmoduletest/ECG;->getXY(Landroid/view/View;Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    cmpg-float v5, v3, v5

    if-gez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const-string v6, "y1"

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/hwmoduletest/ECG;->getXY(Landroid/view/View;Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const-string v6, "y2"

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/hwmoduletest/ECG;->getXY(Landroid/view/View;Ljava/lang/String;)I

    move-result v5

    int-to-float v5, v5

    cmpg-float v5, v4, v5

    if-gez v5, :cond_0

    .line 575
    sget-object v5, Lcom/sec/android/app/hwmoduletest/ECG;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onTouchEvent"

    const-string v7, "Item have been chosen"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    const v6, -0xffff01

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 577
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->txt_itemName:[Landroid/widget/TextView;

    aget-object v5, v5, v1

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 579
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mode_num:I

    .line 580
    iput-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    .line 566
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 584
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->item_selected:Z

    if-eqz v5, :cond_2

    .line 585
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mButton_Start:Landroid/widget/Button;

    invoke-virtual {v5, v11}, Landroid/widget/Button;->setEnabled(Z)V

    .line 590
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    return v5
.end method

.method public startReceiver()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 81
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.app.factorytest.ECG_DATA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    const-string v1, "com.sec.factory.app.factorytest.HEART_RATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 83
    const-string v1, "com.sec.factory.app.factorytest.DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ECG;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/ECG;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 86
    return-void
.end method
