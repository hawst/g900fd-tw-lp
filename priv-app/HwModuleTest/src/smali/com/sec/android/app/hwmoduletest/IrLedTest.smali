.class public Lcom/sec/android/app/hwmoduletest/IrLedTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "IrLedTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "IrEDTest"

.field private static final INTERVAL:I = 0x320

.field private static final IrED_SYSFS_PATH:Ljava/lang/String;

.field private static final IrED_SYSFS_PATH_FOR_ON_OFF:Ljava/lang/String;


# instance fields
.field private CHANNEL_DOWN:Ljava/lang/String;

.field private CHANNEL_UP:Ljava/lang/String;

.field private OFF:Ljava/lang/String;

.field private ON:Ljava/lang/String;

.field private ON_OFF_REPEAT:Ljava/lang/String;

.field private POWER_CMD:Ljava/lang/String;

.field private REPEAT_VOLUME_UP_START:Ljava/lang/String;

.field private REPEAT_VOLUME_UP_STOP:Ljava/lang/String;

.field private START_CMD:Ljava/lang/String;

.field private UNKNOWN_CMD:Ljava/lang/String;

.field private VOLUME_DOWN:Ljava/lang/String;

.field private VOLUME_UP:Ljava/lang/String;

.field private freqRange:[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

.field private irManager:Landroid/hardware/ConsumerIrManager;

.field private mConcept:Ljava/lang/String;

.field private mCounter:I

.field private mCounterView:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mRemoteCall:Z

.field private mRunnable:Ljava/lang/Runnable;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "IR_LED_SEND"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->IrED_SYSFS_PATH:Ljava/lang/String;

    .line 79
    const-string v0, "IR_LED_SEND_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->IrED_SYSFS_PATH_FOR_ON_OFF:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    const-string v0, "IrLedTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRemoteCall:Z

    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mHandler:Landroid/os/Handler;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mTimer:Ljava/util/Timer;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRunnable:Ljava/lang/Runnable;

    .line 93
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounter:I

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounterView:Landroid/widget/TextView;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->irManager:Landroid/hardware/ConsumerIrManager;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    .line 103
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/IrLedTest;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/IrLedTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_UP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/IrLedTest;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/IrLedTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounter:I

    return v0
.end method

.method static synthetic access$308(Lcom/sec/android/app/hwmoduletest/IrLedTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounter:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/IrLedTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounterView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/IrLedTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IrLedTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private controlIrED(Ljava/lang/String;)V
    .locals 7
    .param p1, "control"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    const-string v3, "PEEL"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->irManager:Landroid/hardware/ConsumerIrManager;

    const v4, 0x9470

    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->convertPattern(Ljava/lang/String;)[I

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/hardware/ConsumerIrManager;->transmit(I[I)V

    .line 352
    :goto_0
    const-string v3, "IrEDTest"

    const-string v4, "controlIrED"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "controlIrED - write bytes : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    return-void

    .line 341
    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    sget-object v3, Lcom/sec/android/app/hwmoduletest/IrLedTest;->IrED_SYSFS_PATH:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 343
    .local v2, "out":Ljava/io/FileOutputStream;
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 345
    .local v0, "data":[B
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .line 346
    :catch_0
    move-exception v1

    .line 347
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v3
.end method

.method private convertPattern(Ljava/lang/String;)[I
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 371
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "arr":[Ljava/lang/String;
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    new-array v1, v3, [I

    .line 374
    .local v1, "converted":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    .line 375
    add-int/lit8 v3, v2, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    .line 374
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 377
    :cond_0
    return-object v1
.end method

.method private onoffIrED(Ljava/lang/String;)V
    .locals 7
    .param p1, "control"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 358
    .local v0, "data":[B
    const-string v3, "IrEDTest"

    const-string v4, "onoffIrED"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onoffIrED - write bytes : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    new-instance v2, Ljava/io/FileOutputStream;

    sget-object v3, Lcom/sec/android/app/hwmoduletest/IrLedTest;->IrED_SYSFS_PATH_FOR_ON_OFF:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 362
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v2, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 368
    :goto_0
    return-void

    .line 363
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v3
.end method


# virtual methods
.method public finishOperation()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRunnable:Ljava/lang/Runnable;

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 215
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 324
    :goto_0
    :pswitch_0
    return-void

    .line 220
    :pswitch_1
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "channel_up"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_UP:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 232
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_2
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "channel_down"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_DOWN:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 236
    :catch_1
    move-exception v0

    .line 238
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_3
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "volume_up"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_UP:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 247
    :catch_2
    move-exception v0

    .line 249
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 254
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_4
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "volume_down"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_DOWN:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 258
    :catch_3
    move-exception v0

    .line 260
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 265
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_5
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "button_on"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IRLED_CONCEPT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :try_start_4
    const-string v1, "NEW"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->START_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 276
    :catch_4
    move-exception v0

    .line 278
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 271
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_5
    const-string v1, "PEEL"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "NEWTIME"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "HESTIA"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 272
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 274
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->onoffIrED(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 283
    :pswitch_6
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "button_off"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :try_start_6
    const-string v1, "NEW"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->START_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_0

    .line 293
    :catch_5
    move-exception v0

    .line 295
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 288
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_7
    const-string v1, "PEEL"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "NEWTIME"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "HESTIA"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 289
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->OFF:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 291
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->OFF:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->onoffIrED(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto/16 :goto_0

    .line 300
    :pswitch_7
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "on_off_repeat"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :try_start_8
    const-string v1, "NEW"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->START_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->controlIrED(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_0

    .line 310
    :catch_6
    move-exception v0

    .line 311
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 308
    .end local v0    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_9
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON_OFF_REPEAT:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->onoffIrED(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_0

    .line 316
    :pswitch_8
    const-string v1, "IrEDTest"

    const-string v2, "onClick"

    const-string v3, "Finished IR LED Test in 15 mode"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->setResult(I)V

    .line 318
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->finish()V

    goto/16 :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x7f0b0127
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 108
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    const v2, 0x7f030047

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->setContentView(I)V

    .line 110
    const-string v2, "IrEDTest"

    const-string v3, "onCreate"

    const-string v4, "IrLedTest onCreate"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->getIntent()Landroid/content/Intent;

    move-result-object v12

    .line 112
    .local v12, "i":Landroid/content/Intent;
    const-string v2, "REMOTE_CALL"

    const/4 v3, 0x0

    invoke-virtual {v12, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRemoteCall:Z

    .line 113
    const v2, 0x7f0b012a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 114
    .local v10, "channelUp":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    const v2, 0x7f0b012b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 116
    .local v8, "channelDown":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v2, 0x7f0b012d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 118
    .local v20, "volumeUp":Landroid/view/View;
    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    const v2, 0x7f0b012e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .line 120
    .local v18, "volumeDown":Landroid/view/View;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    const v2, 0x7f0b0130

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 122
    .local v14, "iRLedOn":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v2, 0x7f0b0131

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 124
    .local v13, "iRLedOff":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const v2, 0x7f0b0132

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 126
    .local v15, "iRLedRepeat":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const/16 v2, 0x8

    invoke-virtual {v15, v2}, Landroid/view/View;->setVisibility(I)V

    .line 129
    const-string v2, "IRLED_CONCEPT"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    .line 130
    const-string v2, "PEEL"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "NEWTIME"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 131
    :cond_0
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,552,578,552,578,1683,578,1683,578,552,578,552,578,1683,578,552,578,1683,578,1683,578,552,578,552,578,1683,578,1683,578,552,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON:Ljava/lang/String;

    .line 132
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,552,578,552,578,1683,578,1683,578,1683,578,1683,578,552,578,552,578,1683,578,1683,578,552,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->OFF:Ljava/lang/String;

    .line 133
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,552,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,552,578,1683,578,1683,578,1683,578,1683,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_DOWN:Ljava/lang/String;

    .line 134
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,1683,578,1683,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_UP:Ljava/lang/String;

    .line 135
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,1683,578,552,578,1683,578,1683,578,1683,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_DOWN:Ljava/lang/String;

    .line 136
    const-string v2, "38000,4499,4499,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,1683,578,1683,578,1683,578,552,578,552,578,552,578,552,578,552,578,552,578,1683,578,552,578,552,578,1683,578,552,578,552,578,552,578,1683,578,552,578,1683,578,1683,578,552,578,1683,578,1683,578,1683,578,23047"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_UP:Ljava/lang/String;

    .line 156
    :goto_0
    const-string v2, "PEEL"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 157
    const-string v2, "consumer_ir"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/ConsumerIrManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->irManager:Landroid/hardware/ConsumerIrManager;

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->irManager:Landroid/hardware/ConsumerIrManager;

    invoke-virtual {v2}, Landroid/hardware/ConsumerIrManager;->getCarrierFrequencies()[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->freqRange:[Landroid/hardware/ConsumerIrManager$CarrierFrequencyRange;

    .line 161
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mRemoteCall:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 162
    const-string v2, "IrEDTest"

    const-string v3, "onCreate"

    const-string v4, "mRemoteCall == true"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const v2, 0x7f0b0126

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 166
    .local v16, "layout2":Landroid/view/View;
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 167
    const v2, 0x7f0b0127

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 168
    .local v11, "clrButton":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Landroid/view/View;->setVisibility(I)V

    .line 170
    const v2, 0x7f0b0128

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounterView:Landroid/widget/TextView;

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mCounterView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    const/16 v2, 0x8

    invoke-virtual {v10, v2}, Landroid/view/View;->setVisibility(I)V

    .line 174
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/view/View;->setVisibility(I)V

    .line 175
    const/16 v2, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 176
    const/16 v2, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 177
    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Landroid/view/View;->setVisibility(I)V

    .line 178
    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/view/View;->setVisibility(I)V

    .line 179
    const/16 v2, 0x8

    invoke-virtual {v15, v2}, Landroid/view/View;->setVisibility(I)V

    .line 180
    const v2, 0x7f0b0129

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 181
    .local v9, "channelTitle":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v9, v2}, Landroid/view/View;->setVisibility(I)V

    .line 182
    const v2, 0x7f0b012c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 183
    .local v19, "volumeTitle":Landroid/view/View;
    const/16 v2, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 184
    const v2, 0x7f0b012f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .line 185
    .local v17, "onoffTitle":Landroid/view/View;
    const/16 v2, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 187
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mTimer:Ljava/util/Timer;

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mTimer:Ljava/util/Timer;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/IrLedTest$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/hwmoduletest/IrLedTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/IrLedTest;)V

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x320

    invoke-virtual/range {v2 .. v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 204
    .end local v9    # "channelTitle":Landroid/view/View;
    .end local v11    # "clrButton":Landroid/view/View;
    .end local v16    # "layout2":Landroid/view/View;
    .end local v17    # "onoffTitle":Landroid/view/View;
    .end local v19    # "volumeTitle":Landroid/view/View;
    :cond_2
    return-void

    .line 137
    :cond_3
    const-string v2, "HESTIA"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->mConcept:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 138
    const-string v2, "38400,4531,4479,625,1588,625,1614,625,1588,625,442,651,442,625,442,625,442,625,468,625,1588,625,1614,625,1588,625,468,625,442,625,442,625,468,625,442,625,442,625,1614,625,442,625,442,625,468,625,442,625,442,625,468,625,1588,625,442,625,1614,625,1588,651,1588,625,1614,625,1588,625,1614,625,48932"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON:Ljava/lang/String;

    .line 139
    const-string v2, "38400,4531,4479,625,1588,625,1614,625,1588,625,442,651,442,625,442,625,442,625,468,625,1588,625,1614,625,1588,625,468,625,442,625,442,625,468,625,442,625,442,625,1614,625,442,625,442,625,468,625,442,625,442,625,468,625,1588,625,442,625,1614,625,1588,651,1588,625,1614,625,1588,625,1614,625,48932"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->OFF:Ljava/lang/String;

    .line 140
    const-string v2, "38000,4526,4447,578,1631,578,1631,578,1631,578,526,578,526,578,526,578,526,578,526,578,1631,578,1631,578,1631,578,526,578,526,578,526,578,526,578,526,578,1631,578,1631,578,526,578,1631,578,526,578,526,578,526,578,526,578,526,578,526,578,1631,578,526,578,1631,578,1631,578,1631,578,1631,552,46447"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_DOWN:Ljava/lang/String;

    .line 141
    const-string v2, "38000,4526,4447,578,1631,578,1631,578,1631,578,500,578,500,578,500,578,500,578,500,578,1631,578,1631,578,1631,578,500,578,500,578,500,578,500,578,500,578,1631,578,1631,578,1631,578,500,578,500,578,500,578,500,578,500,578,500,578,500,578,500,578,1631,578,1631,578,1631,578,1631,578,1631,578,46447"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_UP:Ljava/lang/String;

    .line 142
    const-string v2, "38000,4526,4526,578,1657,578,1657,578,1657,578,526,578,526,578,526,578,526,578,526,578,1657,578,1657,578,1657,578,526,578,526,578,526,578,526,578,526,578,526,578,526,578,526,578,526,578,1657,578,526,578,526,578,526,578,1657,578,1657,578,1657,578,1657,578,526,578,1657,578,1657,578,1657,578,46736"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_DOWN:Ljava/lang/String;

    .line 143
    const-string v2, "38000,4526,4447,578,1657,578,1657,578,1657,552,500,552,500,552,500,552,500,552,500,578,1657,578,1657,578,1657,552,500,552,500,552,500,552,500,552,500,552,500,578,1657,552,500,552,500,578,1657,552,500,552,500,552,500,578,1657,552,500,578,1657,578,1657,552,500,578,1657,578,1657,578,1657,578,46447"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_UP:Ljava/lang/String;

    goto/16 :goto_0

    .line 145
    :cond_4
    const-string v2, "38400,10"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON:Ljava/lang/String;

    .line 146
    const-string v2, "38400,5"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->OFF:Ljava/lang/String;

    .line 147
    const-string v2, "38400,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5,10,5"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->ON_OFF_REPEAT:Ljava/lang/String;

    .line 148
    const-string v2, "38400,173,171,24,62,24,61,24,62,24,17,24,17,24,18,24,17,24,18,23,62,24,61,24,62,24,18,23,17,25,17,24,17,24,17,24,62,24,61,25,17,24,61,24,18,24,17,24,17,24,18,24,17,24,17,24,62,24,17,24,62,24,61,24,62,24,61,24,1880,0"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_DOWN:Ljava/lang/String;

    .line 149
    const-string v2, "38400,173,171,24,62,24,61,24,62,24,17,24,17,24,18,24,17,24,19,22,62,24,61,24,62,24,19,22,17,25,17,24,17,24,17,24,62,24,61,25,61,24,17,24,19,23,17,24,17,24,20,22,17,24,17,24,17,25,61,24,62,24,61,24,62,24,61,24,1880,0"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->VOLUME_UP:Ljava/lang/String;

    .line 150
    const-string v2, "38400,173,171,24,62,24,61,24,62,24,17,24,17,24,18,24,17,24,18,23,62,24,61,24,62,24,18,23,17,25,17,24,17,24,17,24,19,23,17,24,17,24,18,24,61,24,17,25,17,24,17,24,62,24,61,24,62,24,61,24,19,23,61,24,62,24,61,24,1880,0"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_DOWN:Ljava/lang/String;

    .line 151
    const-string v2, "38400,173,171,24,62,24,61,24,62,24,17,24,17,24,18,24,17,24,18,23,62,24,61,24,62,24,17,24,17,25,17,24,17,24,17,24,19,23,61,24,18,24,17,24,61,24,19,23,17,24,17,24,62,24,17,24,62,24,61,24,19,23,61,24,62,24,61,24,1880,0"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->CHANNEL_UP:Ljava/lang/String;

    .line 152
    const-string v2, "38400"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->START_CMD:Ljava/lang/String;

    .line 153
    const-string v2, ",174,172,24,61,24,62,24,61,24,17,25,17,24,17,24,17,24,18,24,61,24,62,24,61,24,18,24,17,24,17,24,18,24,17,24,17,24,62,24,17,24,17,24,18,24,17,24,17,24,18,24,61,24,17,24,62,24,61,25,61,24,62,24,61,24,62,24,1879"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/IrLedTest;->POWER_CMD:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->finishOperation()V

    .line 334
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 335
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 328
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/IrLedTest;->finishOperation()V

    .line 330
    return-void
.end method
