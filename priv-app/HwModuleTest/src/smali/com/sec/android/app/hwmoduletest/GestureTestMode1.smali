.class public Lcom/sec/android/app/hwmoduletest/GestureTestMode1;
.super Landroid/app/Activity;
.source "GestureTestMode1.java"


# static fields
.field private static GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment; = null

.field private static final TAG:Ljava/lang/String; = "GestureTestMode1"

.field private static mCheckbox_logging:Landroid/widget/CheckBox;

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mIsTesting:Z

.field private static mLoggingOn:Z


# instance fields
.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBtn_start:Landroid/widget/ToggleButton;

.field private mStartBtn:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    sput-boolean v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mLoggingOn:Z

    .line 32
    sput-boolean v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z

    .line 34
    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 35
    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 43
    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mBtn_start:Landroid/widget/ToggleButton;

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 27
    sput-boolean p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mLoggingOn:Z

    return p0
.end method

.method static synthetic access$100()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200()Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)Landroid/widget/ToggleButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mBtn_start:Landroid/widget/ToggleButton;

    return-object v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 27
    sput-boolean p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z

    return p0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->createArrayList()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->removeFile(Ljava/lang/String;)V

    return-void
.end method

.method private createArrayList()V
    .locals 7

    .prologue
    .line 223
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mArrayList:Ljava/util/ArrayList;

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mArrayList:Ljava/util/ArrayList;

    const-string v6, "Select All"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    const-string v4, "/storage/sdcard0/"

    .line 226
    .local v4, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v1, "dir":Ljava/io/File;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 228
    .local v2, "filename":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 231
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_1

    .line 232
    aget-object v2, v0, v3

    .line 234
    const-string v5, ".txt"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 235
    const-string v5, "GestureZmean"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 236
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 242
    .end local v3    # "i":I
    :cond_1
    const-string v5, "GestureTestMode1"

    const-string v6, "createArrayList() : end!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    return-void
.end method

.method private deleteLogFiles()V
    .locals 5

    .prologue
    .line 151
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 152
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v2, "Gesture sensor data is stored in \'/storage/sdcard0/\'.\nDo you want to delete log files?"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Yes"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$4;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "No"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$3;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 217
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 218
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 219
    const-string v2, "GestureTestMode1"

    const-string v3, "deleteLogFiles() : end!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    return-void
.end method

.method private removeFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 246
    const-string v1, "GestureZmean"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage/sdcard0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 249
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 254
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    const-string v1, "GestureTestMode1"

    const-string v2, "removeFile() : end!"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    sput-boolean v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mLoggingOn:Z

    .line 74
    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 75
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 76
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    sput-boolean v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z

    .line 94
    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mBtn_start:Landroid/widget/ToggleButton;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mBtn_start:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$2;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const-string v0, "GestureTestMode1"

    const-string v1, "onCreate"

    const-string v2, "onCreate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->setContentView(I)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->initialize()V

    .line 51
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 67
    const-string v0, "GestureTestMode1"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 69
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 132
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 134
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->deleteLogFiles()V

    .line 135
    const/4 v0, 0x1

    goto :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b02e9
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "GestureTestMode1"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 64
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 143
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 55
    const-string v0, "GestureTestMode1"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0b009f

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    .line 59
    return-void
.end method
