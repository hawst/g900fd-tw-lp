.class Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;
.super Ljava/lang/Object;
.source "SensorUV.java"

# interfaces
.implements Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Maxim"
.end annotation


# instance fields
.field private ADC:I

.field private HR:I

.field private SUM:I

.field private UVIndex:D

.field private VB0:I

.field private VB1:I

.field private VB2:I

.field private VB3:I

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

.field uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;Landroid/content/Context;)V
    .locals 3
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281
    const-string v0, "SensorUV"

    const-string v1, "SensorUV"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->mContext:Landroid/content/Context;

    .line 283
    return-void
.end method


# virtual methods
.method public SensorOff()V
    .locals 3

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->isSensorOn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    invoke-virtual {v0, p0}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->unregisterListener(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;)V

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    .line 307
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->ADC:I

    .line 308
    const-string v0, "SensorUV"

    const-string v1, "SensorOn"

    const-string v2, "unregister-UVSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    :cond_0
    return-void
.end method

.method public SensorOn()Z
    .locals 4

    .prologue
    .line 286
    const/4 v0, 0x1

    .line 288
    .local v0, "res":Z
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    if-nez v1, :cond_1

    .line 289
    new-instance v1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->mContext:Landroid/content/Context;

    const v3, 0x1001d

    invoke-direct {v1, v2, v3}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    .line 290
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-nez v1, :cond_0

    .line 291
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    invoke-virtual {v1, p0}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->registerListener(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolEventListener;)V

    .line 295
    const-string v1, "SensorUV"

    const-string v2, "SensorOn"

    const-string v3, "register-UVSensor"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/4 v0, 0x1

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->isUVMGROn:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->access$002(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;Z)Z

    .line 299
    :cond_1
    return v0
.end method

.method public isSensorOn()Z
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->uvEolSensor:Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;

    invoke-virtual {v0}, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEol;->getSensor()Landroid/hardware/Sensor;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 314
    const/4 v0, 0x1

    .line 316
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMaximUVSensorEolIRChanged(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;)V
    .locals 4
    .param p1, "event"    # Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;

    .prologue
    .line 321
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb0:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB0:I

    .line 322
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb1:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB1:I

    .line 323
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb2:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB2:I

    .line 324
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->vb3:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB3:I

    .line 325
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->sum:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->SUM:I

    .line 326
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolIREvent;->hr:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->HR:I

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB0:I

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB1:I

    aput v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB2:I

    aput v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->VB3:I

    aput v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->SUM:I

    aput v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->HR:I

    aput v3, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->access$102(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;[I)[I

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    # getter for: Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer_SensorValue_Cloud:[I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->access$100(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->returnCloudValues([I)V

    .line 330
    return-void
.end method

.method public onMaximUVSensorEolUVChanged(Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;

    .prologue
    .line 334
    iget v0, p1, Lcom/maximintegrated/bio/uv/eol/MaximUVSensorEolUVEvent;->adccount:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->ADC:I

    .line 335
    const-wide v0, 0x3f5f212d77318fc5L    # 0.0019

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->ADC:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    const-wide v2, 0x3f73404ea4a8c155L    # 0.0047

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->UVIndex:D

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->UVIndex:D

    invoke-static {v4, v5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->ADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->mBuffer:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->access$202(Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;[Ljava/lang/String;)[Ljava/lang/String;

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV$Maxim;->this$0:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->returnSensorValues()V

    .line 339
    return-void
.end method
