.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;
.super Ljava/lang/Object;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SavePreference"
.end annotation


# static fields
.field private static final PRIVATE_FILE:Ljava/lang/String; = "barometerData"


# instance fields
.field private mData:[F

.field private mFormat:Ljava/text/DecimalFormat;

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V
    .locals 1

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    const/16 v0, 0x17

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mData:[F

    .line 160
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mFormat:Ljava/text/DecimalFormat;

    return-void

    .line 159
    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V

    return-void
.end method

.method private saveToPrivateFiles(Ljava/lang/String;)Z
    .locals 6
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 179
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    const-string v3, "barometerData"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 180
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 181
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "path":Ljava/lang/String;
    const-string v2, "BarometerWaterProofTest"

    const-string v3, "SavePreference:saveToPrivateFiles"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->writeToPath(Ljava/lang/String;[B)Z

    move-result v2

    .line 185
    .end local v1    # "path":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected write(IF)Z
    .locals 10
    .param p1, "index"    # I
    .param p2, "value"    # F

    .prologue
    const/4 v5, 0x0

    .line 163
    const-string v6, "BarometerWaterProofTest"

    const-string v7, "SavePreference:write"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "index["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mFormat:Ljava/text/DecimalFormat;

    const-string v7, "0.00"

    invoke-virtual {v6, v7}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 165
    const/4 v6, -0x1

    if-le p1, v6, :cond_1

    const/16 v6, 0x17

    if-ge p1, v6, :cond_1

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mData:[F

    aput p2, v6, p1

    .line 167
    const-string v4, ""

    .line 168
    .local v4, "stringData":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mData:[F

    .local v0, "arr$":[F
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, v0, v2

    .line 169
    .local v1, "f":F
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->mFormat:Ljava/text/DecimalFormat;

    float-to-double v8, v1

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    .end local v1    # "f":F
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 172
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SavePreference;->saveToPrivateFiles(Ljava/lang/String;)Z

    move-result v5

    .line 175
    .end local v0    # "arr$":[F
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "stringData":Ljava/lang/String;
    :cond_1
    return v5
.end method
