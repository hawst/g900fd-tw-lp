.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

.field final synthetic val$operation:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)V
    .locals 0

    .prologue
    .line 763
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iput p2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/high16 v13, 0x10000000

    const/4 v12, 0x0

    const/high16 v11, 0x10000

    const/16 v10, 0xb

    .line 765
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3302(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    .line 767
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    const-wide/16 v8, 0x1f4

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v8, v9, v7}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 768
    const-string v6, "FingerPrintTest"

    const-string v7, "mLock.tryAcquire( fail!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1011
    :goto_0
    return-void

    .line 771
    :catch_0
    move-exception v2

    .line 772
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 776
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    if-ne v6, v14, :cond_2

    .line 777
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    const-string v8, "Test"

    invoke-virtual {v7, v8, v10}, Lcom/validity/fingerprint/Fingerprint;->removeEnrolledFinger(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    .line 1009
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/util/concurrent/Semaphore;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 778
    :cond_2
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 779
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    const-string v8, "Test"

    const-string v9, "test appdata"

    const/4 v10, 0x7

    const/4 v11, 0x2

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/validity/fingerprint/Fingerprint;->enroll(Ljava/lang/String;Ljava/lang/String;II)I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto :goto_1

    .line 781
    :cond_3
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_4

    .line 782
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/validity/fingerprint/Fingerprint;->cancel()I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto :goto_1

    .line 783
    :cond_4
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x65

    if-ne v6, v7, :cond_b

    .line 785
    new-instance v5, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v5}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 786
    .local v5, "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v3, 0x0

    .line 787
    .local v3, "opt":I
    const/4 v1, 0x0

    .line 790
    .local v1, "dataLogopt":I
    or-int/2addr v3, v11

    .line 793
    or-int/lit8 v1, v1, 0x1

    .line 794
    or-int/lit8 v1, v1, 0x40

    .line 795
    or-int/lit16 v1, v1, 0x80

    .line 796
    or-int/lit16 v1, v13, 0xc1

    .line 799
    const-string v6, "factory"

    const-string v7, "BINARY_TYPE"

    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "TEMPORARY_USE_NON_TZ"

    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 801
    const/16 v6, 0xfe6

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 811
    :cond_5
    :goto_2
    iput v3, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 812
    iput v1, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 813
    iput v12, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 815
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorTest["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v6

    invoke-virtual {v6, v10, v5}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v4

    .line 823
    .local v4, "result":I
    if-nez v4, :cond_a

    .line 824
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_NORMALSCAN_DATA Success "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 802
    .end local v4    # "result":I
    :cond_6
    const-string v6, "60"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 803
    const/16 v6, 0x80c

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto :goto_2

    .line 804
    :cond_7
    const-string v6, "65"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "68A"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 805
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v6

    if-nez v6, :cond_9

    .line 806
    const/16 v6, 0x816

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_2

    .line 808
    :cond_9
    const/16 v6, 0x820

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_2

    .line 826
    .restart local v4    # "result":I
    :cond_a
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_NORMALSCAN_DATA FAIL!! "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 828
    .end local v1    # "dataLogopt":I
    .end local v3    # "opt":I
    .end local v4    # "result":I
    .end local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    :cond_b
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x66

    if-ne v6, v7, :cond_d

    .line 830
    new-instance v5, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v5}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 831
    .restart local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v3, 0x0

    .line 832
    .restart local v3    # "opt":I
    const/4 v1, 0x0

    .line 833
    .restart local v1    # "dataLogopt":I
    const/high16 v0, 0x4000000

    .line 836
    .local v0, "VCS_SNSR_TEST_USE_SET_EVENT_WITH_CB_FLAG":I
    const/high16 v6, 0x8000000

    or-int/2addr v3, v6

    .line 837
    or-int/2addr v3, v11

    .line 838
    or-int/2addr v3, v0

    .line 841
    or-int/lit8 v1, v1, 0x1

    .line 842
    or-int/lit8 v1, v1, 0x2

    .line 843
    or-int/lit8 v1, v1, 0x20

    .line 844
    or-int/lit16 v1, v1, 0x100

    .line 845
    or-int/lit16 v1, v1, 0x200

    .line 846
    or-int/lit16 v1, v1, 0x400

    .line 847
    or-int/lit16 v1, v1, 0x1000

    .line 848
    or-int/lit16 v1, v1, 0x2000

    .line 852
    iput v3, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 853
    iput v1, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 854
    iput v12, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 856
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorTest["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v6

    invoke-virtual {v6, v10, v5}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v4

    .line 865
    .restart local v4    # "result":I
    if-nez v4, :cond_c

    .line 866
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest VCS_REQUEST_COMMAND_SENSOR_TEST Success "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 868
    :cond_c
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest VCS_REQUEST_COMMAND_SENSOR_TEST FAIL!! "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 870
    .end local v0    # "VCS_SNSR_TEST_USE_SET_EVENT_WITH_CB_FLAG":I
    .end local v1    # "dataLogopt":I
    .end local v3    # "opt":I
    .end local v4    # "result":I
    .end local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    :cond_d
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x67

    if-ne v6, v7, :cond_f

    .line 872
    new-instance v5, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v5}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 873
    .restart local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v3, 0x0

    .line 874
    .restart local v3    # "opt":I
    const/4 v1, 0x0

    .line 877
    .restart local v1    # "dataLogopt":I
    or-int/2addr v3, v11

    .line 880
    or-int/lit8 v1, v1, 0x1

    .line 881
    or-int/lit8 v1, v1, 0x2

    .line 882
    or-int/lit8 v1, v1, 0x20

    .line 883
    or-int/lit16 v1, v1, 0x100

    .line 884
    or-int/lit16 v1, v1, 0x200

    .line 885
    or-int/lit16 v1, v1, 0x400

    .line 886
    or-int/lit16 v1, v1, 0x4000

    .line 887
    or-int/lit16 v1, v13, 0x4723

    .line 889
    const/16 v6, 0x7d6

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 897
    iput v3, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 898
    iput v1, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 899
    iput v12, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 901
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorTest["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v6

    invoke-virtual {v6, v10, v5}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v4

    .line 910
    .restart local v4    # "result":I
    if-nez v4, :cond_e

    .line 911
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest VCS_REQUEST_COMMAND_SENSOR_TEST Success "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 914
    :cond_e
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest VCS_REQUEST_COMMAND_SENSOR_TEST FAIL!! "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 917
    .end local v1    # "dataLogopt":I
    .end local v3    # "opt":I
    .end local v4    # "result":I
    .end local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    :cond_f
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x68

    if-ne v6, v7, :cond_15

    .line 918
    new-instance v5, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v5}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 919
    .restart local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v3, 0x0

    .line 920
    .restart local v3    # "opt":I
    const/4 v1, 0x0

    .line 923
    .restart local v1    # "dataLogopt":I
    or-int/2addr v3, v11

    .line 926
    or-int/lit8 v1, v1, 0x1

    .line 927
    const v6, 0x8000

    or-int/lit8 v1, v6, 0x1

    .line 928
    or-int/2addr v1, v13

    .line 930
    const-string v6, "60"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 931
    const/16 v6, 0x813

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 939
    :cond_10
    :goto_3
    iput v3, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 940
    iput v1, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 941
    iput v12, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 943
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorTest["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v6

    invoke-virtual {v6, v10, v5}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v4

    .line 951
    .restart local v4    # "result":I
    if-nez v4, :cond_14

    .line 952
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_METHOD3_DATA Success "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 932
    .end local v4    # "result":I
    :cond_11
    const-string v6, "65"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    const-string v6, "68A"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 933
    :cond_12
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v6

    if-nez v6, :cond_13

    .line 934
    const/16 v6, 0x81d

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_3

    .line 936
    :cond_13
    const/16 v6, 0x827

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_3

    .line 954
    .restart local v4    # "result":I
    :cond_14
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_METHOD3_DATA FAIL!! "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 956
    .end local v1    # "dataLogopt":I
    .end local v3    # "opt":I
    .end local v4    # "result":I
    .end local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    :cond_15
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x69

    if-ne v6, v7, :cond_1b

    .line 957
    new-instance v5, Lcom/validity/fingerprint/SensorTest;

    invoke-direct {v5}, Lcom/validity/fingerprint/SensorTest;-><init>()V

    .line 958
    .restart local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    const/4 v3, 0x0

    .line 959
    .restart local v3    # "opt":I
    const/4 v1, 0x0

    .line 962
    .restart local v1    # "dataLogopt":I
    or-int/2addr v3, v11

    .line 965
    or-int/lit8 v1, v1, 0x1

    .line 966
    or-int/lit8 v1, v11, 0x1

    .line 967
    const/high16 v6, 0x20000

    or-int/2addr v1, v6

    .line 968
    or-int/2addr v1, v13

    .line 970
    const-string v6, "60"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 971
    const/16 v6, 0x813

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    .line 979
    :cond_16
    :goto_4
    iput v3, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    .line 980
    iput v1, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    .line 981
    iput v12, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    .line 983
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorTest["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->options:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->dataLogOpt:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v5, Lcom/validity/fingerprint/SensorTest;->unitId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v6

    invoke-virtual {v6, v10, v5}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v4

    .line 991
    .restart local v4    # "result":I
    if-nez v4, :cond_1a

    .line 992
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_METHOD4_DATA Success "

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 972
    .end local v4    # "result":I
    :cond_17
    const-string v6, "65"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_18

    const-string v6, "68A"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 973
    :cond_18
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v6

    if-nez v6, :cond_19

    .line 974
    const/16 v6, 0x81e

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_4

    .line 976
    :cond_19
    const/16 v6, 0x828

    iput v6, v5, Lcom/validity/fingerprint/SensorTest;->scriptId:I

    goto/16 :goto_4

    .line 994
    .restart local v4    # "result":I
    :cond_1a
    const-string v6, "FingerPrintTest"

    const-string v7, "SensorTest OPERATION_REQUEST_METHOD4_DATA FAIL!! "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 996
    .end local v1    # "dataLogopt":I
    .end local v3    # "opt":I
    .end local v4    # "result":I
    .end local v5    # "snrTest":Lcom/validity/fingerprint/SensorTest;
    :cond_1b
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/4 v7, 0x6

    if-ne v6, v7, :cond_1c

    .line 997
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/validity/fingerprint/Fingerprint;->getSensorStatus()I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto/16 :goto_1

    .line 998
    :cond_1c
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_1d

    .line 999
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    const-string v8, "Test"

    invoke-virtual {v7, v8}, Lcom/validity/fingerprint/Fingerprint;->identify(Ljava/lang/String;)I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto/16 :goto_1

    .line 1000
    :cond_1d
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_1e

    .line 1001
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/validity/fingerprint/Fingerprint;->cleanUp()I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto/16 :goto_1

    .line 1002
    :cond_1e
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    const/16 v7, 0x8

    if-ne v6, v7, :cond_1f

    .line 1003
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/validity/fingerprint/Fingerprint;->request(ILjava/lang/Object;)I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto/16 :goto_1

    .line 1005
    :cond_1f
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->val$operation:I

    if-ne v6, v10, :cond_1

    .line 1006
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v14, v8}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v7

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I

    goto/16 :goto_1
.end method
