.class public Lcom/sec/android/app/hwmoduletest/HdmiTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HdmiTest.java"


# instance fields
.field private final HDMI_MSG:Ljava/lang/String;

.field private mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "HdmiTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 11
    const-string v0, "HDMI Pattern Display On"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->HDMI_MSG:Ljava/lang/String;

    .line 16
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 66
    :cond_0
    return-void
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->release()V

    .line 59
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v0, 0x7f030041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->setContentView(I)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 23
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 40
    const/16 v0, 0x18

    if-ne p1, v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->finish()V

    .line 44
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->stopMedia()V

    .line 35
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 36
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 27
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 28
    const v0, 0x7f050006

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->playMedia(IZ)V

    .line 29
    const-string v0, "HDMI Pattern Display On"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 30
    return-void
.end method

.method public playMedia(IZ)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->release()V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HdmiTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HdmiTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 52
    return-void
.end method
