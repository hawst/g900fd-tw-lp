.class public Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;
.super Ljava/lang/Object;
.source "SensorBase.java"


# static fields
.field public static final ID_FILE____LIGHT:I = 0x6

.field public static final ID_FILE____LIGHT_ADC:I = 0x8

.field public static final ID_FILE____LIGHT_CCT:I = 0x7

.field public static final ID_FILE____PROXIMITY_ADC:I = 0x2

.field public static final ID_FILE____PROXIMITY_AVG:I = 0x3

.field public static final ID_FILE____PROXIMITY_OFFSET:I = 0x4

.field public static final ID_MANAGER_BIO:I = 0xc

.field public static final ID_MANAGER_BIO_HRM:I = 0xd

.field public static final ID_MANAGER_HUMID:I = 0xa

.field public static final ID_MANAGER_LIGHT:I = 0x5

.field public static final ID_MANAGER_PROXIMITY:I = 0x1

.field public static final ID_MANAGER_TEMP:I = 0x9

.field public static final ID_MANAGER_UV:I = 0xb

.field public static final ID_MANAGER_UV_CLOUD:I = 0xe

.field public static final NOTI:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "SensorBase"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;->CLASS_NAME:Ljava/lang/String;

    .line 31
    const-string v0, "SensorBase"

    const-string v1, "SensorBase"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void
.end method


# virtual methods
.method public SensorOff()V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public SensorOn([I)V
    .locals 0
    .param p1, "sensorID"    # [I

    .prologue
    .line 37
    return-void
.end method

.method public SensorOn([ILandroid/os/Handler;I)V
    .locals 0
    .param p1, "sensorID"    # [I
    .param p2, "notiHandler"    # Landroid/os/Handler;
    .param p3, "messageDelay_SensorUpdate_millisecond"    # I

    .prologue
    .line 41
    return-void
.end method

.method public isSensorOn(I)Z
    .locals 1
    .param p1, "sensorID"    # I

    .prologue
    .line 49
    const/4 v0, 0x1

    return v0
.end method
