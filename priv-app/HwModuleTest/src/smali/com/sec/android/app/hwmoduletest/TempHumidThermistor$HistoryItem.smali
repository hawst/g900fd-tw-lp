.class Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;
.super Ljava/lang/Object;
.source "TempHumidThermistor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryItem"
.end annotation


# instance fields
.field private countvalue:Ljava/lang/String;

.field private thermistor_SensorHub_BATT_Value:Ljava/lang/String;

.field private thermistor_SensorHub_CHG_Value:Ljava/lang/String;

.field private thermistor_apValue:Ljava/lang/String;

.field private thermistor_battValue:Ljava/lang/String;

.field private thermistor_pamValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "count"    # I
    .param p2, "thermistor_ap"    # Ljava/lang/String;
    .param p3, "thermistor_pam"    # Ljava/lang/String;
    .param p4, "thermistor_batt"    # Ljava/lang/String;

    .prologue
    .line 149
    const-string v5, " "

    const-string v6, " "

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "thermistor_ap"    # Ljava/lang/String;
    .param p3, "thermistor_pam"    # Ljava/lang/String;
    .param p4, "thermistor_SensorHub_CHG"    # Ljava/lang/String;
    .param p5, "thermistor_batt"    # Ljava/lang/String;
    .param p6, "thermistor_SensorHub_BATT"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->countvalue:Ljava/lang/String;

    .line 154
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_apValue:Ljava/lang/String;

    .line 155
    iput-object p3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_pamValue:Ljava/lang/String;

    .line 156
    iput-object p5, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_battValue:Ljava/lang/String;

    .line 157
    iput-object p4, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_SensorHub_CHG_Value:Ljava/lang/String;

    .line 158
    iput-object p6, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_SensorHub_BATT_Value:Ljava/lang/String;

    .line 159
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue_thermistor_ap()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_apValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue_thermistor_batt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_battValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue_thermistor_pam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_pamValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue_thermistor_sensorhub_batt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_SensorHub_BATT_Value:Ljava/lang/String;

    return-object v0
.end method

.method public getValue_thermistor_sensorhub_chg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->thermistor_SensorHub_CHG_Value:Ljava/lang/String;

    return-object v0
.end method
