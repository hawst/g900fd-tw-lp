.class public Lcom/sec/android/app/hwmoduletest/HwModuleTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HwModuleTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/HwModuleTest$EmptyListener;
    }
.end annotation


# static fields
.field public static final ID_2ND_LCD:B = 0x21t

.field public static final ID_BARCODE_EMUL:B = 0x16t

.field public static final ID_BLACK:B = 0x18t

.field public static final ID_BLUE:B = 0x2t

.field public static final ID_DIMMING:B = 0x3t

.field public static final ID_FRONT_CAM:B = 0x6t

.field public static final ID_GREEN:B = 0x1t

.field public static final ID_GRIP:B = 0xft

.field public static final ID_HALL_IC:B = 0x19t

.field public static final ID_HDMI:B = 0x12t

.field public static final ID_HEALTH_SENSOR:B = 0x20t

.field public static final ID_IR_LED:B = 0x11t

.field public static final ID_LED:B = 0xdt

.field public static final ID_LOW_FREQUENCY:B = 0x13t

.field public static final ID_MEGA_CAM:B = 0x5t

.field public static final ID_MLC:B = 0x1ft

.field public static final ID_RECEIVER:B = 0x7t

.field public static final ID_RED:B = 0x0t

.field public static final ID_SENSOR:B = 0x9t

.field public static final ID_SENSORHUB:B = 0x17t

.field public static final ID_SIDE_TOUCH:B = 0x1et

.field public static final ID_SLEEP:B = 0xbt

.field public static final ID_SPEAKER:B = 0x8t

.field public static final ID_SPEAKER_R:B = 0x10t

.field public static final ID_SPEN_HOVERING:B = 0x1dt

.field public static final ID_SUB_KEY:B = 0xct

.field public static final ID_SVC_LED:B = 0x14t

.field public static final ID_TOUCH:B = 0x4t

.field public static final ID_TOUCHSELF:B = 0x68t

.field public static final ID_TSP_DATA:B = 0x15t

.field public static final ID_TSP_HOVERING:B = 0x1at

.field public static final ID_VIBRATION:B = 0xat

.field public static final ID_VIBRATION_L:B = 0x1bt

.field public static final ID_VIBRATION_R:B = 0x1ct

.field public static final ID_WACOM:B = 0xet

.field private static final LEFT:I = 0x2

.field private static final RIGHT:I = 0x1

.field private static final START_SAVE_TSP_DATA:I = 0x3e9

.field protected static mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

.field public static preVolume:I


# instance fields
.field private final BACK_KEY_EVENT_TIMELAG:J

.field PassSavingInspection:Z

.field PassSavingReference:Z

.field private TSPFile:Ljava/lang/String;

.field private TSPFilePath:Ljava/lang/String;

.field private TspNotiDialog:Landroid/app/AlertDialog;

.field private mAccRotationValue:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContainer:Landroid/view/ViewGroup;

.field private mDeviceTypeAlertDialog:Landroid/app/AlertDialog;

.field public mHandler:Landroid/os/Handler;

.field private mIsLongPress:Z

.field private mIsPlayingSound:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

.field private mPrevBackKeyEventTime:J

.field mProduct:Ljava/lang/String;

.field private mTspManufacture:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    .line 130
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->preVolume:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 136
    const-string v0, "HwModuleTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 99
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mProduct:Ljava/lang/String;

    .line 101
    const-string v0, "mnt/extSdCard/"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFilePath:Ljava/lang/String;

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    .line 119
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->BACK_KEY_EVENT_TIMELAG:J

    .line 121
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsLongPress:Z

    .line 123
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mPrevBackKeyEventTime:J

    .line 129
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    .line 139
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 780
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;-><init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mHandler:Landroid/os/Handler;

    .line 137
    return-void
.end method

.method private CreateFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "tsptype"    # Ljava/lang/String;

    .prologue
    .line 722
    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFilePath:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 723
    .local v3, "dir":Ljava/io/File;
    move-object v6, p1

    .line 724
    .local v6, "fileName":Ljava/lang/String;
    const-string v4, ".txt"

    .line 725
    .local v4, "exr":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFilePath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->SubStringProduct()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 726
    .local v5, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 727
    .local v2, "count":I
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    const-string v12, ""

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 730
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "arr":[Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "CreateFile"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "arr : "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    :goto_0
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v12, v10}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 734
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "file : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    if-eqz v0, :cond_2

    .line 737
    move-object v1, v0

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_2

    aget-object v9, v1, v7

    .line 738
    .local v9, "str":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "str : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ",fileName"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x5

    if-le v10, v11, :cond_0

    .line 742
    const/4 v10, 0x0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x4

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->SubStringProduct()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 744
    add-int/lit8 v2, v2, 0x1

    .line 737
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 731
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "str":Ljava/lang/String;
    :cond_1
    const-string v10, "null"

    goto/16 :goto_0

    .line 756
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFilePath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->SubStringProduct()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 764
    .end local v0    # "arr":[Ljava/lang/String;
    :goto_2
    return-object v10

    .line 760
    .restart local v0    # "arr":[Ljava/lang/String;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFilePath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->SubStringProduct()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    .line 764
    .end local v0    # "arr":[Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    goto :goto_2
.end method

.method private StartTspdataTest()V
    .locals 4

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->isExternalMemoryExist()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    const-string v0, "Wait..."

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->showProgressDialog(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "StartTspdataTest"

    const-string v2, "StartTspdata Test!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 778
    :goto_0
    return-void

    .line 775
    :cond_0
    const-string v0, "Please insert SD card."

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 776
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "StartTspdataTest"

    const-string v2, "SD Card is not Detect"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private SubStringProduct()Ljava/lang/String;
    .locals 3

    .prologue
    .line 567
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mProduct:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mProduct:Ljava/lang/String;

    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mProduct:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->writeSOC()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->makeTspDataReference()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->makeTspDataInspection()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->hideProgressDialog()V

    return-void
.end method

.method private checkDeviceType()V
    .locals 4

    .prologue
    .line 192
    const-string v2, "dongle"

    const-string v3, "DEVICE_TYPE"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 194
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v2, "Check Device Type"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 195
    const-string v2, "We don\'t support devices like a Dongle or Setopbox."

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 196
    new-instance v1, Lcom/sec/android/app/hwmoduletest/HwModuleTest$EmptyListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest$EmptyListener;-><init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    .line 197
    .local v1, "pl":Landroid/content/DialogInterface$OnClickListener;
    const-string v2, "OK"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 198
    new-instance v2, Lcom/sec/android/app/hwmoduletest/HwModuleTest$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 204
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mDeviceTypeAlertDialog:Landroid/app/AlertDialog;

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mDeviceTypeAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 207
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "pl":Landroid/content/DialogInterface$OnClickListener;
    :cond_0
    return-void
.end method

.method private createUI()V
    .locals 13

    .prologue
    .line 532
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mContainer:Landroid/view/ViewGroup;

    if-nez v9, :cond_0

    .line 533
    const v9, 0x7f0b0124

    invoke-virtual {p0, v9}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mContainer:Landroid/view/ViewGroup;

    .line 536
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 537
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestMenu()[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 540
    .local v7, "menuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v1, v9, :cond_3

    .line 541
    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 542
    .local v6, "layout":Landroid/widget/LinearLayout;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 544
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 545
    const/high16 v9, 0x40400000    # 3.0f

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 546
    add-int/lit8 v9, v1, 0x3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v9, v10, :cond_1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    :goto_1
    invoke-virtual {v7, v1, v9}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    .line 549
    .local v5, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 550
    .local v4, "item":Ljava/lang/String;
    const-string v9, ","

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v3, v9, v10

    .line 551
    .local v3, "id":Ljava/lang/String;
    const-string v9, ","

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v8, v9, v10

    .line 552
    .local v8, "text":Ljava/lang/String;
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 553
    .local v0, "button":Landroid/widget/Button;
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 555
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setId(I)V

    .line 556
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 557
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 558
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUI"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Create Button. id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", name="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 546
    .end local v0    # "button":Landroid/widget/Button;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "item":Ljava/lang/String;
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "text":Ljava/lang/String;
    :cond_1
    add-int/lit8 v9, v1, 0x3

    goto :goto_1

    .line 562
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v5    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 540
    add-int/lit8 v1, v1, 0x3

    goto/16 :goto_0

    .line 564
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "layout":Landroid/widget/LinearLayout;
    :cond_3
    return-void
.end method

.method private get_Inspection_ReadValue()Ljava/lang/String;
    .locals 7

    .prologue
    .line 670
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "get_Inspection_ReadValue()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " mTspManufacture :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    const-string v2, ""

    .line 673
    .local v2, "value":Ljava/lang/String;
    const-string v1, ""

    .line 675
    .local v1, "result":Ljava/lang/String;
    const-string v3, "MELFAS"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 678
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "run_cm_abs_read"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 695
    :cond_0
    :goto_0
    const/16 v0, 0xa

    .line 697
    .local v0, "X_AXIS":I
    const-string v3, "MELFAS"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 698
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "get_cm_abs"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 717
    :cond_1
    :goto_1
    return-object v1

    .line 679
    .end local v0    # "X_AXIS":I
    :cond_2
    const-string v3, "SYNAPTICS"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 685
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "NA"

    .line 686
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "get_Inspection_ReadValue()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " result :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 687
    :cond_3
    const-string v3, "STM"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 690
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "run_raw_read"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 691
    :cond_4
    const-string v3, "ATMEL"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 692
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "run_delta_read"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 701
    .restart local v0    # "X_AXIS":I
    :cond_5
    const-string v3, "SYNAPTICS"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 705
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "NA"

    .line 707
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "get_Inspection_ReadValue(), "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SYNAPTICS : GET_INSPECTION  - result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 709
    :cond_6
    const-string v3, "STM"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 712
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "get_raw"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 713
    :cond_7
    const-string v3, "ATMEL"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 714
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v4, "get_delta"

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method private get_Reference_ReadValue()Ljava/lang/String;
    .locals 6

    .prologue
    .line 625
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "get_Reference_ReadValue()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mTspManufacture :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v1, ""

    .line 629
    .local v1, "result":Ljava/lang/String;
    const-string v2, "MELFAS"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 630
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "run_cm_abs_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 648
    :cond_0
    :goto_0
    const/16 v0, 0xa

    .line 650
    .local v0, "X_AXIS":I
    const-string v2, "MELFAS"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 651
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "get_cm_abs"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 666
    :cond_1
    :goto_1
    return-object v1

    .line 633
    .end local v0    # "X_AXIS":I
    :cond_2
    const-string v2, "SYNAPTICS"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 636
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "get_x_num"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 637
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "run_rawcap_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 640
    :cond_3
    const-string v2, "STM"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 641
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "run_reference_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 644
    :cond_4
    const-string v2, "ATMEL"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 645
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "run_reference_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 654
    .restart local v0    # "X_AXIS":I
    :cond_5
    const-string v2, "SYNAPTICS"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 655
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "get_rawcap"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 658
    :cond_6
    const-string v2, "STM"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 659
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "get_reference"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 662
    :cond_7
    const-string v2, "ATMEL"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 663
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v3, "get_reference"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private hideProgressDialog()V
    .locals 3

    .prologue
    .line 814
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "hideProgressDialog"

    const-string v2, "hide Dialog"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 819
    :cond_0
    return-void
.end method

.method private isApplicableToCamTest()Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 867
    const/16 v4, 0xf

    .line 868
    .local v4, "threshold":I
    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v9, v5}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 869
    .local v1, "bat":Landroid/content/Intent;
    const-string v5, "level"

    const/4 v6, -0x1

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 870
    .local v2, "battLevel":I
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "checkCapacity()"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    const/16 v5, 0xf

    if-ge v2, v5, :cond_0

    .line 873
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 874
    .local v3, "builder":Landroid/app/AlertDialog$Builder;
    const-string v5, "Battery Low"

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 875
    const-string v5, "Need to Charge 15% Over"

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 876
    const-string v5, "OK"

    invoke-virtual {v3, v5, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 877
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 878
    .local v0, "ad":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 879
    const/4 v5, 0x0

    .line 882
    .end local v0    # "ad":Landroid/app/AlertDialog;
    .end local v3    # "builder":Landroid/app/AlertDialog$Builder;
    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private makeTspDataInspection()V
    .locals 8

    .prologue
    .line 598
    const/4 v3, 0x0

    .line 599
    .local v3, "fos":Ljava/io/FileOutputStream;
    const-string v1, "TSP_Delta"

    .line 600
    .local v1, "TSP_TYPE":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CreateFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 603
    .local v0, "FileInspection":Ljava/io/File;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "makeFileTspdata"

    const-string v7, ""

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v0, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->get_Inspection_ReadValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 606
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingInspection:Z

    .line 607
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 614
    if-eqz v4, :cond_2

    .line 616
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 622
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 617
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 618
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 619
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 608
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 609
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 614
    if-eqz v3, :cond_0

    .line 616
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 617
    :catch_2
    move-exception v2

    .line 618
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 610
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 612
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 614
    if-eqz v3, :cond_0

    .line 616
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 617
    :catch_4
    move-exception v2

    .line 618
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 614
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v3, :cond_1

    .line 616
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 619
    :cond_1
    :goto_4
    throw v5

    .line 617
    :catch_5
    move-exception v2

    .line 618
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 614
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 610
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 608
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method private makeTspDataReference()V
    .locals 9

    .prologue
    .line 571
    const/4 v3, 0x0

    .line 572
    .local v3, "fos":Ljava/io/FileOutputStream;
    const-string v1, "TSP_Reference"

    .line 573
    .local v1, "TSP_TYPE":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CreateFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, "FileReference":Ljava/io/File;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "makeFileTspdata"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TSPFile"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->TSPFile:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v0, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->get_Reference_ReadValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/FileOutputStream;->write([B)V

    .line 579
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingReference:Z

    .line 580
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 587
    if-eqz v4, :cond_2

    .line 589
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 595
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 590
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 591
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 592
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 581
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 582
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 587
    if-eqz v3, :cond_0

    .line 589
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 590
    :catch_2
    move-exception v2

    .line 591
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 583
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 585
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 587
    if-eqz v3, :cond_0

    .line 589
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 590
    :catch_4
    move-exception v2

    .line 591
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 587
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v3, :cond_1

    .line 589
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 592
    :cond_1
    :goto_4
    throw v5

    .line 590
    :catch_5
    move-exception v2

    .line 591
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 587
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 583
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 581
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method private showProgressDialog(Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 805
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "showProgressDialog"

    const-string v3, "Display Dialog"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 807
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 808
    const-string v1, "TSP Node Data entering to file."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 809
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 810
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private startSpeakerTest()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x3

    .line 847
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 848
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->preVolume:I

    .line 849
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 851
    const/high16 v1, 0x7f050000

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 852
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 854
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    .line 855
    return-void
.end method

.method private startSpeakerTest(I)V
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x3

    .line 823
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 824
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->preVolume:I

    .line 825
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 828
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 829
    const v1, 0x7f050007

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 837
    :goto_0
    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    div-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v2, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 839
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 840
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 841
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    .line 842
    return-void

    .line 830
    :cond_0
    if-ne p1, v3, :cond_1

    .line 831
    const v1, 0x7f050008

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 833
    :cond_1
    const/high16 v1, 0x7f050000

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method private stopSpeakerTest()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 858
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 859
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 860
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 861
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    .line 862
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 863
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x3

    sget v2, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->preVolume:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 864
    return-void
.end method

.method private writeSOC()V
    .locals 5

    .prologue
    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "isresetneeded":Z
    const-string v3, "AT_BATTEST_RESET_WHEN_READ"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 157
    if-eqz v0, :cond_1

    .line 158
    sget-object v3, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->resetFuelGaugeIC()Z

    .line 163
    :goto_0
    sget-object v3, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->readBatteryVoltage()Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "result":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    const-string v3, "efs/FactoryApp/SOC_Data"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    .local v2, "socDataFile":Ljava/io/File;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 169
    :cond_0
    return-void

    .line 160
    .end local v1    # "result":Ljava/lang/String;
    .end local v2    # "socDataFile":Ljava/io/File;
    :cond_1
    const-string v3, "BATTERY_UPDATE_BEFORE_READ"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 251
    sparse-switch p1, :sswitch_data_0

    .line 280
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 281
    return-void

    .line 254
    :sswitch_0
    const/4 v4, 0x6

    if-ne p1, v4, :cond_1

    .line 256
    .local v2, "isFrontCamera":Z
    :goto_1
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    .line 257
    const-string v3, "data_filepath"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "filePath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 260
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/hwmoduletest/CameraImageView;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "bg_filepath"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "frontcam"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .end local v0    # "filePath":Ljava/lang/String;
    .end local v2    # "isFrontCamera":Z
    :cond_1
    move v2, v3

    .line 254
    goto :goto_1

    .line 267
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "onActivityResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resultCode = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/hwmoduletest/TouchTest;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 270
    .local v1, "i":Landroid/content/Intent;
    if-nez p2, :cond_2

    .line 271
    const-string v4, "TEST_TSP_SELF"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 276
    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 273
    :cond_2
    const-string v3, "TEST_TSP_SELF"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x68 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x2

    const/16 v8, 0x10

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 286
    .local v0, "id":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "onClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 289
    .local v1, "intent":Landroid/content/Intent;
    packed-switch v0, :pswitch_data_0

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 291
    :pswitch_0
    const-class v2, Lcom/sec/android/app/hwmoduletest/ColorTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 292
    const-string v2, "color"

    const/high16 v3, -0x10000

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 296
    :pswitch_1
    const-class v2, Lcom/sec/android/app/hwmoduletest/ColorTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 297
    const-string v2, "color"

    const v3, -0xff0100

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 301
    :pswitch_2
    const-class v2, Lcom/sec/android/app/hwmoduletest/ColorTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 302
    const-string v2, "color"

    const v3, -0xffff01

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 303
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 306
    :pswitch_3
    const-class v2, Lcom/sec/android/app/hwmoduletest/DimmingTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 307
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 310
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "startTSP"

    const-string v5, "Start TSP"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v3, "isHovering"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 313
    const-string v2, "Style_X"

    const-string v3, "04"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315
    const-class v2, Lcom/sec/android/app/hwmoduletest/TspPatternStyleX;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 316
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 319
    :cond_1
    const-class v2, Lcom/sec/android/app/hwmoduletest/TouchTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 320
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 325
    :pswitch_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "startTSP"

    const-string v5, "Start TSP Hovering"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v2, "isHovering"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 328
    const-string v2, "Style_X"

    const-string v3, "04"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 330
    const-class v2, Lcom/sec/android/app/hwmoduletest/TspPatternStyleHoverX;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 331
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 334
    :cond_2
    const-class v2, Lcom/sec/android/app/hwmoduletest/TouchTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 335
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 340
    :pswitch_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startSPEN"

    const-string v4, "Start SPEN Hovering"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-class v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 343
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 347
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->isApplicableToCamTest()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 352
    const-string v4, "camera"

    const-string v5, "DEVICE_TYPE"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 353
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "run "

    const-string v6, "com.samsung.difactorycamera"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v4, "com.samsung.difactorycamera"

    const-string v5, "com.samsung.difactorycamera.Camera"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    const-string v4, "camera_id"

    if-ne v0, v10, :cond_3

    :goto_1
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 356
    const-string v2, "camcorder_preview_test"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 357
    const-string v2, "postview_test"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 358
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_3
    move v2, v3

    .line 355
    goto :goto_1

    .line 363
    :cond_4
    :pswitch_8
    const-string v4, "com.sec.factory.camera"

    const-string v5, "com.sec.android.app.camera.Camera"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    const-string v4, "camera_id"

    if-ne v0, v10, :cond_5

    :goto_2
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 365
    const-string v2, "ois_test"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 366
    const-string v2, "camcorder_preview_test"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 367
    const-string v2, "postview_test"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 368
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 364
    goto :goto_2

    .line 371
    :pswitch_9
    const-string v2, "Dual_Receiver"

    const-string v3, "07"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 373
    const-class v2, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 374
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 375
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 376
    :cond_6
    const-string v2, "MULTI_CNT"

    const-string v3, "07"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 378
    const-class v2, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 379
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 380
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 382
    :cond_7
    const-class v2, Lcom/sec/android/app/hwmoduletest/ReceiverTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 383
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 389
    :pswitch_a
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    if-eqz v2, :cond_8

    .line 390
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->stopSpeakerTest()V

    goto/16 :goto_0

    .line 392
    :cond_8
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getEnabled(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getEnabled(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 393
    invoke-direct {p0, v9}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startSpeakerTest(I)V

    goto/16 :goto_0

    .line 395
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startSpeakerTest()V

    goto/16 :goto_0

    .line 402
    :pswitch_b
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    if-eqz v2, :cond_a

    .line 403
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->stopSpeakerTest()V

    goto/16 :goto_0

    .line 405
    :cond_a
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getEnabled(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getEnabled(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "true"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 406
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startSpeakerTest(I)V

    goto/16 :goto_0

    .line 408
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startSpeakerTest()V

    goto/16 :goto_0

    .line 415
    :pswitch_c
    const-class v3, Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 416
    const-string v3, "isSensorHubTest"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 417
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 420
    :pswitch_d
    const-class v2, Lcom/sec/android/app/hwmoduletest/VibrationTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 421
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 424
    :pswitch_e
    const-string v2, "Direction"

    invoke-virtual {v1, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 425
    const-class v2, Lcom/sec/android/app/hwmoduletest/VibrationTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 426
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 429
    :pswitch_f
    const-string v2, "Direction"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 430
    const-class v2, Lcom/sec/android/app/hwmoduletest/VibrationTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 431
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 434
    :pswitch_10
    sget-object v3, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doWakeLock(Z)V

    .line 435
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->sleep(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 438
    :pswitch_11
    const-class v2, Lcom/sec/android/app/hwmoduletest/SubKeyTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 439
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 442
    :pswitch_12
    const-class v2, Lcom/sec/android/app/hwmoduletest/LedTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 443
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 448
    :pswitch_13
    const-class v2, Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 449
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 453
    :pswitch_14
    const-string v2, "SUPPORT_GRIPSENSOR_CAL_UI"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 454
    const-class v2, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 464
    :goto_3
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 457
    :cond_c
    const-string v2, "GRIPSENSOR_TYPE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "AP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 458
    const-class v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_3

    .line 461
    :cond_d
    const-class v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_3

    .line 468
    :pswitch_15
    const-class v2, Lcom/sec/android/app/hwmoduletest/IrLedTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 469
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 472
    :pswitch_16
    const-string v2, "LAND"

    const-string v3, "18"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 474
    const-class v2, Lcom/sec/android/app/hwmoduletest/HDMI_Landscape;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 480
    :goto_4
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 478
    :cond_e
    const-class v2, Lcom/sec/android/app/hwmoduletest/HDMI_Portrait;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_4

    .line 484
    :pswitch_17
    const-class v2, Lcom/sec/android/app/hwmoduletest/LowFrequencyTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 485
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 488
    :pswitch_18
    const-class v2, Lcom/sec/android/app/hwmoduletest/SvcLedTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 489
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 492
    :pswitch_19
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->StartTspdataTest()V

    goto/16 :goto_0

    .line 495
    :pswitch_1a
    const-class v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 496
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 499
    :pswitch_1b
    const-class v2, Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 500
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 503
    :pswitch_1c
    const-class v2, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 504
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 507
    :pswitch_1d
    const-class v2, Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 508
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 511
    :pswitch_1e
    const-class v2, Lcom/sec/android/app/hwmoduletest/SideTouchTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 512
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 515
    :pswitch_1f
    const-class v2, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 516
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 519
    :pswitch_20
    const-class v2, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 520
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 523
    :pswitch_21
    const-class v2, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 524
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_d
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_b
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_5
        :pswitch_e
        :pswitch_f
        :pswitch_6
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 173
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 174
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 177
    :cond_0
    const v0, 0x7f030046

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->setContentView(I)V

    .line 178
    const v0, 0x7f0b0124

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mContainer:Landroid/view/ViewGroup;

    .line 179
    sget-object v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    .line 180
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    const-string v1, "get_chip_vendor"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mTspManufacture:Ljava/lang/String;

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mAccRotationValue:I

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 187
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->checkDeviceType()V

    .line 188
    return-void

    .line 179
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mAccRotationValue:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 246
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 247
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mIsPlayingSound:Z

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopSpeakerTest "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->stopSpeakerTest()V

    .line 235
    :cond_0
    const-string v0, "factory"

    const-string v1, "BINARY_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "INBATT_SAVE_SOC"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 239
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 240
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 217
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onResume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resume "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->createUI()V

    .line 221
    const-string v1, "factory"

    const-string v2, "BINARY_TYPE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "INBATT_SAVE_SOC"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 224
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 227
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method
