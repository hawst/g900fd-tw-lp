.class public Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;
.super Landroid/view/View;
.source "DigitizerTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/DigitizerTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mDefectFlag:Z

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mNonClickPaint:Landroid/graphics/Paint;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/DigitizerTest;Landroid/content/Context;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 319
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .line 320
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 300
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    .line 301
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    .line 302
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    .line 303
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    .line 315
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mDefectFlag:Z

    .line 321
    const-string v3, "window"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 323
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 324
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 325
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    .line 326
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    .line 327
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 328
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    invoke-static {v0, v3, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 330
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 334
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->setPaint()V

    .line 335
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->initRect()V

    .line 336
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    .line 337
    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;

    .prologue
    .line 296
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->initializeView()V

    return-void
.end method

.method private checkDefect(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 427
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 428
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    const-string v3, "+++++++++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$900(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    const-string v3, "MotionEvent.ACTION_DOWN 0"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1000(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    const-string v3, "MotionEvent.ACTION_MOVE 2"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    const-string v3, "MotionEvent.ACTION_UP 1"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "checkDefect"

    const-string v3, "+++++++++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    packed-switch v0, :pswitch_data_0

    .line 457
    :goto_0
    :pswitch_0
    return-void

    .line 442
    :pswitch_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mDefectFlag:Z

    goto :goto_0

    .line 447
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mDefectFlag:Z

    if-nez v1, :cond_0

    .line 448
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mDefectFlag:Z

    goto :goto_0

    .line 451
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->finishTestFail()V

    goto :goto_0

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private checkPreviousRect(FF)Z
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 638
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v7

    int-to-float v7, v7

    div-float v0, v6, v7

    .line 639
    .local v0, "col_height":F
    iget v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v7

    int-to-float v7, v7

    div-float v1, v6, v7

    .line 640
    .local v1, "col_width":F
    div-float v6, p1, v1

    float-to-int v2, v6

    .line 641
    .local v2, "countX":I
    div-float v6, p2, v0

    float-to-int v3, v6

    .line 645
    .local v3, "countY":I
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-gt v3, v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-le v2, v6, :cond_2

    .line 646
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$2200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "checkPreviousRect"

    const-string v7, "You are out of bounds!"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    :cond_1
    :goto_0
    return v4

    .line 650
    :cond_2
    add-int/lit8 v6, v3, -0x1

    if-ltz v6, :cond_3

    add-int/lit8 v6, v2, -0x1

    if-gez v6, :cond_4

    :cond_3
    move v4, v5

    .line 651
    goto :goto_0

    .line 654
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v6

    add-int/lit8 v7, v3, -0x1

    aget-object v6, v6, v7

    add-int/lit8 v7, v2, -0x1

    aget-boolean v6, v6, v7

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v6

    add-int/lit8 v7, v3, -0x1

    aget-object v6, v6, v7

    add-int/lit8 v7, v2, -0x1

    aget-boolean v6, v6, v7

    if-eqz v6, :cond_1

    :cond_5
    move v4, v5

    .line 658
    goto :goto_0
.end method

.method private drawByEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 460
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 462
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 464
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    .line 465
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    .line 466
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 467
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 478
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 479
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 480
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    .line 481
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    .line 482
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    .line 483
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    .line 484
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 485
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawLine(FFFF)V

    .line 479
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 488
    :cond_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    .line 489
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    .line 490
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    .line 491
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    .line 492
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 493
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawLine(FFFF)V

    .line 494
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 500
    .end local v1    # "i":I
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "drawByEvent"

    const-string v4, "ACTION_UP!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_3

    .line 503
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    .line 504
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    .line 505
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    .line 506
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    .line 508
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 509
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawPoint(FF)V

    .line 512
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    .line 515
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isPass()Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 516
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->finishTestFail()V

    goto/16 :goto_0

    .line 462
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 528
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 530
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 531
    float-to-int v6, p1

    .line 532
    float-to-int v8, p3

    .line 538
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 539
    float-to-int v7, p2

    .line 540
    float-to-int v9, p4

    .line 546
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 547
    return-void

    .line 534
    :cond_0
    float-to-int v6, p3

    .line 535
    float-to-int v8, p1

    goto :goto_0

    .line 542
    :cond_1
    float-to-int v7, p4

    .line 543
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 551
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 552
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 553
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 662
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 663
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 664
    .local v9, "col_width":F
    div-float v0, p1, v9

    float-to-int v10, v0

    .line 665
    .local v10, "countX":I
    div-float v0, p2, v8

    float-to-int v11, v0

    .line 666
    .local v11, "countY":I
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 667
    .local v6, "ColX":F
    int-to-float v0, v11

    mul-float v7, v8, v0

    .line 670
    .local v7, "ColY":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v11, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v10, v0, :cond_2

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$2300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "You are out of bounds!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    :cond_1
    :goto_0
    return-void

    .line 675
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-nez v0, :cond_3

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-eqz v0, :cond_5

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 684
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 688
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isPass()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$000(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-nez v0, :cond_1

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$008(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->remoteCall:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$2400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 691
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->setResult(I)V

    .line 693
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->finishTestPass()V

    goto/16 :goto_0

    .line 681
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private finishTestFail()V
    .locals 5

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$000(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-nez v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$002(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)I

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    const/16 v1, 0x64

    # invokes: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->receiverStart(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1900(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const-string v1, "FAIL"

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x43960000    # 300.0f

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 628
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate()V

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$2100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView$2;-><init>(Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 635
    :cond_0
    return-void
.end method

.method private finishTestPass()V
    .locals 5

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    const/16 v1, 0x65

    # invokes: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->receiverStart(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1900(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const-string v1, "PASS"

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x43960000    # 300.0f

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate()V

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$2100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView$1;-><init>(Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 619
    return-void
.end method

.method private initRect()V
    .locals 14

    .prologue
    .line 556
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 557
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 558
    .local v9, "col_width":F
    const/4 v6, 0x0

    .line 559
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 560
    .local v7, "ColY":I
    const/4 v10, 0x0

    .line 561
    .local v10, "count":I
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 562
    .local v13, "mRectPaint":Landroid/graphics/Paint;
    const/high16 v0, -0x1000000

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 564
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-ge v11, v0, :cond_2

    .line 565
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-ge v12, v0, :cond_1

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v12

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v12

    mul-float/2addr v1, v9

    int-to-float v2, v11

    mul-float/2addr v2, v8

    add-int/lit8 v3, v12, 0x1

    int-to-float v3, v3

    mul-float/2addr v3, v9

    add-int/lit8 v4, v11, 0x1

    int-to-float v4, v4

    mul-float/2addr v4, v8

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 565
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 564
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 575
    .end local v12    # "j":I
    :cond_2
    const/4 v11, 0x0

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-ge v11, v0, :cond_4

    .line 576
    int-to-float v0, v11

    mul-float/2addr v0, v8

    float-to-int v7, v0

    .line 577
    const/4 v12, 0x0

    .restart local v12    # "j":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I

    move-result v0

    if-ge v12, v0, :cond_3

    .line 578
    int-to-float v0, v12

    mul-float/2addr v0, v9

    float-to-int v6, v0

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v7

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    int-to-float v4, v4

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v10

    mul-float/2addr v1, v9

    int-to-float v2, v10

    mul-float/2addr v2, v8

    add-int/lit8 v3, v10, 0x1

    int-to-float v3, v3

    mul-float/2addr v3, v9

    int-to-float v4, v10

    mul-float/2addr v4, v8

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v10

    mul-float/2addr v1, v9

    int-to-float v2, v10

    mul-float/2addr v2, v8

    int-to-float v3, v10

    mul-float/2addr v3, v9

    add-int/lit8 v4, v10, 0x1

    int-to-float v4, v4

    mul-float/2addr v4, v8

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v10

    mul-float/2addr v1, v9

    add-int/lit8 v2, v10, 0x1

    int-to-float v2, v2

    mul-float/2addr v2, v8

    add-int/lit8 v3, v10, 0x1

    int-to-float v3, v3

    mul-float/2addr v3, v9

    add-int/lit8 v4, v10, 0x1

    int-to-float v4, v4

    mul-float/2addr v4, v8

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    add-int/lit8 v1, v10, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v9

    int-to-float v2, v10

    mul-float/2addr v2, v8

    add-int/lit8 v3, v10, 0x1

    int-to-float v3, v3

    mul-float/2addr v3, v9

    add-int/lit8 v4, v10, 0x1

    int-to-float v4, v4

    mul-float/2addr v4, v8

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    const/4 v1, 0x0

    aput-boolean v1, v0, v12

    .line 577
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 594
    :cond_3
    add-int/lit8 v10, v10, 0x1

    .line 575
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_2

    .line 605
    .end local v12    # "j":I
    :cond_4
    return-void
.end method

.method private initializeView()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 391
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$002(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)I

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->receiverStop()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenWidth:I

    int-to-float v3, v2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mScreenHeight:I

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 397
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->initRect()V

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->invalidate()V

    .line 399
    return-void
.end method

.method private setPaint()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    .line 340
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 351
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 357
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 360
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 364
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 370
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 374
    sparse-switch p1, :sswitch_data_0

    .line 387
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 376
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->initializeView()V

    goto :goto_0

    .line 379
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->finish()V

    goto :goto_0

    .line 382
    :sswitch_2
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    .line 374
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 403
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 404
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event.getButtonState(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event.getToolType(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isTouchDown: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->isTouchDown:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsWacom:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->access$600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v6, :cond_1

    .line 417
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->drawByEvent(Landroid/view/MotionEvent;)V

    .line 423
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 418
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 419
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;->checkDefect(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
