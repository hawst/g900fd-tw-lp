.class public Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "PowerNoiseGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;
    }
.end annotation


# static fields
.field private static period:I

.field private static sampling:I


# instance fields
.field private fail:I

.field private mMagneticSensor:Landroid/hardware/Sensor;

.field private mOriginalData_Magnetic:[F

.field private mPowerNoiseGraph:Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private pass:I

.field private power_noise:[Ljava/lang/String;

.field private power_nosie_spec_x:D

.field private power_nosie_spec_y:D

.field private power_nosie_spec_z:D

.field private raw_data:[F

.field private raw_datas:[[F

.field private retSelftest:I

.field private run_num:I

.field private sptimer:Ljava/util/Timer;

.field private task:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x82

    sput v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I

    .line 40
    const/16 v0, 0x2710

    sput v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->period:I

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    const/4 v1, 0x0

    .line 50
    const-string v0, "PowerNoiseGraphActivity"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 30
    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    .line 31
    new-array v0, v4, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    move-object v0, v1

    .line 32
    check-cast v0, [[F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->retSelftest:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->pass:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->fail:I

    .line 41
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 42
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 43
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mOriginalData_Magnetic:[F

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->retSelftest:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mPowerNoiseGraph:Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    return v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->getRawData(Z)V

    return-void
.end method

.method private getRawData(Z)V
    .locals 7
    .param p1, "isSampling"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 166
    const/4 v1, 0x0

    .line 167
    .local v1, "str":Ljava/lang/String;
    const-string v2, "MAGNT_SENSOR_RAW"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 168
    const/4 v0, 0x0

    .line 170
    .local v0, "RawDatas":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 172
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 177
    aget-object v2, v0, v4

    if-nez v2, :cond_0

    .line 178
    const-string v2, "0.0"

    aput-object v2, v0, v4

    .line 181
    :cond_0
    aget-object v2, v0, v5

    if-nez v2, :cond_1

    .line 182
    const-string v2, "0.0"

    aput-object v2, v0, v5

    .line 185
    :cond_1
    aget-object v2, v0, v6

    if-nez v2, :cond_2

    .line 186
    const-string v2, "0.0"

    aput-object v2, v0, v6

    .line 189
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget-object v3, v0, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aput v3, v2, v4

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget-object v3, v0, v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aput v3, v2, v5

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget-object v3, v0, v6

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    aput v3, v2, v6

    .line 193
    if-eqz p1, :cond_3

    .line 194
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    sget v3, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I

    if-ge v2, v3, :cond_5

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget v3, v3, v4

    aput v3, v2, v4

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget v3, v3, v5

    aput v3, v2, v5

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F

    aget v3, v3, v6

    aput v3, v2, v6

    .line 203
    :cond_3
    :goto_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    .line 204
    :cond_4
    return-void

    .line 199
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->standardDeviation([[F)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->retSelftest:I

    goto :goto_0
.end method

.method public static mean([F)D
    .locals 6
    .param p0, "array"    # [F

    .prologue
    .line 241
    const-wide/16 v2, 0x0

    .line 243
    .local v2, "sum":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 244
    aget v1, p0, v0

    float-to-double v4, v1

    add-double/2addr v2, v4

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_0
    array-length v1, p0

    int-to-double v4, v1

    div-double v4, v2, v4

    return-wide v4
.end method

.method private sensorRegister()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "sensorRegister"

    const-string v2, "MAGNETIC"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;-><init>(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    .line 147
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mMagneticSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 151
    return-void
.end method

.method private sensorUnregister()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "sensorUnregister"

    const-string v2, "MAGNETIC"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 160
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 161
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$SensorListener;

    .line 162
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mMagneticSensor:Landroid/hardware/Sensor;

    .line 163
    return-void
.end method

.method public static standardDeviation([F)D
    .locals 12
    .param p0, "array"    # [F

    .prologue
    .line 251
    array-length v3, p0

    const/4 v10, 0x2

    if-ge v3, v10, :cond_0

    .line 252
    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    .line 266
    :goto_0
    return-wide v6

    .line 255
    :cond_0
    const-wide/16 v8, 0x0

    .line 256
    .local v8, "sum":D
    const-wide/16 v6, 0x0

    .line 258
    .local v6, "sd":D
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mean([F)D

    move-result-wide v4

    .line 260
    .local v4, "meanValue":D
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, p0

    if-ge v2, v3, :cond_1

    .line 261
    aget v3, p0, v2

    float-to-double v10, v3

    sub-double v0, v10, v4

    .line 262
    .local v0, "diff":D
    mul-double v10, v0, v0

    add-double/2addr v8, v10

    .line 260
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 265
    .end local v0    # "diff":D
    :cond_1
    array-length v3, p0

    int-to-double v10, v3

    div-double v10, v8, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 266
    goto :goto_0
.end method

.method private standardDeviation([[F)I
    .locals 18
    .param p1, "value"    # [[F

    .prologue
    .line 207
    const/4 v10, 0x0

    .local v10, "x":[F
    const/4 v11, 0x0

    .local v11, "y":[F
    const/4 v12, 0x0

    .line 210
    .local v12, "z":[F
    sget-object v13, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-static {v13}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    check-cast v2, Ljava/text/DecimalFormat;

    .line 211
    .local v2, "df":Ljava/text/DecimalFormat;
    const-string v13, "0.000"

    invoke-virtual {v2, v13}, Ljava/text/DecimalFormat;->applyPattern(Ljava/lang/String;)V

    .line 212
    move-object/from16 v0, p1

    array-length v13, v0

    new-array v10, v13, [F

    .line 213
    move-object/from16 v0, p1

    array-length v13, v0

    new-array v11, v13, [F

    .line 214
    move-object/from16 v0, p1

    array-length v13, v0

    new-array v12, v13, [F

    .line 216
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v13, v0

    if-ge v3, v13, :cond_0

    .line 217
    aget-object v13, p1, v3

    const/4 v14, 0x0

    aget v13, v13, v14

    aput v13, v10, v3

    .line 218
    aget-object v13, p1, v3

    const/4 v14, 0x1

    aget v13, v13, v14

    aput v13, v11, v3

    .line 219
    aget-object v13, p1, v3

    const/4 v14, 0x2

    aget v13, v13, v14

    aput v13, v12, v3

    .line 216
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 222
    :cond_0
    invoke-static {v10}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->standardDeviation([F)D

    move-result-wide v4

    .line 223
    .local v4, "dx":D
    invoke-static {v11}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->standardDeviation([F)D

    move-result-wide v6

    .line 224
    .local v6, "dy":D
    invoke-static {v12}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->standardDeviation([F)D

    move-result-wide v8

    .line 225
    .local v8, "dz":D
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 226
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    const/4 v14, 0x1

    invoke-virtual {v2, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 227
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    const/4 v14, 0x2

    invoke-virtual {v2, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    .line 228
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "standardDeviation"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "power noise x="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", y="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    aget-object v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", z="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x2

    aget-object v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    cmpg-double v13, v4, v14

    if-gtz v13, :cond_1

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    cmpg-double v13, v6, v14

    if-gtz v13, :cond_1

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    cmpg-double v13, v8, v14

    if-gtz v13, :cond_1

    .line 234
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->pass:I

    .line 236
    :goto_1
    return v13

    :cond_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->fail:I

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/high16 v8, 0x4042000000000000L    # 36.0

    const-wide/high16 v6, 0x4041000000000000L    # 34.0

    const-wide/high16 v4, 0x400c000000000000L    # 3.5

    const-wide/high16 v2, 0x402c000000000000L    # 14.0

    .line 55
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f030061

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->setContentView(I)V

    .line 57
    const v0, 0x7f0b01ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mPowerNoiseGraph:Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;

    .line 59
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AK8963C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AK8963"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HSCDTD008A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    :cond_0
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 63
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 64
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    .line 85
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "YAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 66
    iput-wide v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 67
    const-wide/high16 v0, 0x403a000000000000L    # 26.0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 68
    const-wide/high16 v0, 0x4026000000000000L    # 11.0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    goto :goto_0

    .line 70
    :cond_3
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 71
    iput-wide v6, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 72
    iput-wide v6, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 73
    iput-wide v6, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    goto :goto_0

    .line 75
    :cond_4
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AK09911C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 76
    iput-wide v4, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 77
    iput-wide v4, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 78
    iput-wide v4, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    goto :goto_0

    .line 80
    :cond_5
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "STMICRO_K303C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iput-wide v8, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_x:D

    .line 82
    iput-wide v8, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_y:D

    .line 83
    iput-wide v8, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_nosie_spec_z:D

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 141
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 142
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "YAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "IS_ASAHI_USING_SYSFS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    :cond_0
    const-string v0, "MAGNT_SENSOR_RAW"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 131
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sptimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sptimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 136
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 137
    return-void

    .line 127
    :cond_3
    const-string v0, "IS_USING_SENSOR_MANAGER_IN_POWER_NOISE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sensorUnregister()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 7

    .prologue
    .line 88
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 90
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "YAS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "IS_ASAHI_USING_SYSFS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    :cond_0
    const-string v0, "MAGNT_SENSOR_RAW"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 97
    :cond_1
    :goto_0
    new-instance v6, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;-><init>(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)V

    .line 103
    .local v6, "handler":Landroid/os/Handler;
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I

    .line 104
    sget v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I

    const/4 v1, 0x3

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_datas:[[F

    .line 105
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sptimer:Ljava/util/Timer;

    .line 106
    new-instance v0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;

    invoke-direct {v0, p0, v6}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;-><init>(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->task:Ljava/util/TimerTask;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sptimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->task:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    sget v4, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->period:I

    sget v5, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I

    div-int/2addr v4, v5

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 120
    return-void

    .line 93
    .end local v6    # "handler":Landroid/os/Handler;
    :cond_2
    const-string v0, "IS_USING_SENSOR_MANAGER_IN_POWER_NOISE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sensorRegister()V

    goto :goto_0
.end method
