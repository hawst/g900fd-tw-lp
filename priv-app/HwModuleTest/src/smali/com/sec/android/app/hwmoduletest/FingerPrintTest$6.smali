.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setVisibility(I)V

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "as":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setDataResult([Ljava/lang/String;[Ljava/lang/String;)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v3

    add-int/lit8 v3, v3, -0x32

    int-to-float v3, v3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, 0x14

    int-to-float v4, v4

    div-float v1, v3, v4

    .line 269
    .local v1, "scaleX":F
    const-string v3, "FingerPrintTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Graph X Scale : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setGraphXScale(F)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40400000    # 3.0f

    div-float/2addr v3, v4

    const/high16 v4, 0x437f0000    # 255.0f

    div-float v2, v3, v4

    .line 272
    .local v2, "scaleY":F
    const-string v3, "FingerPrintTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Graph Y Scale : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setGraphYScale(F)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->setPixelResult(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;->showGraph()V

    .line 283
    .end local v0    # "as":[Ljava/lang/String;
    .end local v1    # "scaleX":F
    .end local v2    # "scaleY":F
    :goto_0
    return-void

    .line 279
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    const-string v4, "Cannot found Data log!!"

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$202(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Ljava/lang/String;)Ljava/lang/String;

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
