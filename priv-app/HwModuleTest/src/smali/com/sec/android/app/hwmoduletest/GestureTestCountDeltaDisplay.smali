.class public Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;
.super Landroid/app/Activity;
.source "GestureTestCountDeltaDisplay.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureTestCountDeltaDisplay"

.field private static final TYPE_AIRMOTION:I = 0x2

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mLoggingOn:Z

.field private static mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItemCD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 66
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 68
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z

    .line 69
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 71
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mValueList:Ljava/util/List;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mHandler:Landroid/os/Handler;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mPowerManager:Landroid/os/PowerManager;

    .line 134
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z

    return v0
.end method

.method static synthetic access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    return-object v0
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

    const v1, 0x7f030022

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

    .line 124
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mListView:Landroid/widget/ListView;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 128
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z

    if-eqz v0, :cond_0

    .line 129
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    const-string v1, "GestureCountDelta"

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "onCreate"

    const-string v2, "onCreate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->setContentView(I)V

    .line 79
    const-string v0, "sensorhub"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 80
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 81
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mPowerManager:Landroid/os/PowerManager;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "GestureTestCountDeltaDisplay"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "logging"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z

    .line 84
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logging flag value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->initTextView()V

    .line 86
    return-void
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    const-string v1, "GestureTestCountDeltaDisplay"

    const-string v2, "onDestroy"

    const-string v3, "onDestroy start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 108
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 109
    sput-object v4, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 112
    :cond_0
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v1, :cond_1

    .line 113
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 114
    sput-object v4, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 115
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 117
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->sendBroadcast(Landroid/content/Intent;)V

    .line 119
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 120
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 97
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 101
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 102
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 89
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 90
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 93
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 94
    return-void
.end method
