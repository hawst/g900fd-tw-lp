.class public Lcom/sec/android/app/hwmoduletest/ReceiverTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ReceiverTest.java"


# static fields
.field public static final AUDIO_PATH_EAR:I = 0x2

.field public static final AUDIO_PATH_HDMI:I = 0x3

.field public static final AUDIO_PATH_OFF:I = 0x4

.field public static final AUDIO_PATH_RCV:I = 0x1

.field public static final AUDIO_PATH_SPK:I

.field public static preVolume:I


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->preVolume:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 30
    const-string v0, "ReceiverTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->AUDIO_PATH:[Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    .local v0, "view":Landroid/view/View;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 38
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->setContentView(Landroid/view/View;)V

    .line 39
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    .line 40
    const v1, 0x7f050006

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 42
    return-void
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 68
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 72
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onDestroy"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x3

    sget v3, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->preVolume:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 78
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->setAudioPath(I)V

    .line 79
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "Media stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 46
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->preVolume:I

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v4, v1, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->setAudioPath(I)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->setMode(I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "Media start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->finish()V

    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 90
    return-void
.end method
