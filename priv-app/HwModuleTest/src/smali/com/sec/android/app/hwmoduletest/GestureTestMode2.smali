.class public Lcom/sec/android/app/hwmoduletest/GestureTestMode2;
.super Landroid/app/Activity;
.source "GestureTestMode2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureTestMode2"

.field private static mCheckbox_logging:Landroid/widget/CheckBox;

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mLoggingOn:Z


# instance fields
.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStartBtn:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z

    .line 32
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 33
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 26
    sput-boolean p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z

    return p0
.end method

.method static synthetic access$100()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->createArrayList()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->removeFile(Ljava/lang/String;)V

    return-void
.end method

.method private createArrayList()V
    .locals 7

    .prologue
    .line 186
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mArrayList:Ljava/util/ArrayList;

    .line 187
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mArrayList:Ljava/util/ArrayList;

    const-string v6, "Select All"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    const-string v4, "/storage/sdcard0/"

    .line 189
    .local v4, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 190
    .local v1, "dir":Ljava/io/File;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 191
    .local v2, "filename":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 194
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_1

    .line 195
    aget-object v2, v0, v3

    .line 197
    const-string v5, ".txt"

    invoke-virtual {v2, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 198
    const-string v5, "GestureData"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 199
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 205
    .end local v3    # "i":I
    :cond_1
    const-string v5, "GestureTestMode2"

    const-string v6, "createArrayList() : end!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-void
.end method

.method private deleteLogFiles()V
    .locals 5

    .prologue
    .line 114
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    .local v1, "alt_bld":Landroid/app/AlertDialog$Builder;
    const-string v2, "Gesture sensor data is stored in \'/storage/sdcard0/\'.\nDo you want to delete log files?"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Yes"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$3;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "No"

    new-instance v4, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$2;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 180
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 181
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 182
    const-string v2, "GestureTestMode2"

    const-string v3, "deleteLogFiles() : end!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void
.end method

.method private removeFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string v1, "GestureData"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage/sdcard0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 212
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 217
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    const-string v1, "GestureTestMode2"

    const-string v2, "removeFile() : end!"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    return-void
.end method


# virtual methods
.method public initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    sput-boolean v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z

    .line 64
    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;

    .line 65
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mCheckbox_logging:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v0, 0x7f0b008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mStartBtn:Landroid/widget/Button;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 84
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0b008f

    if-ne v1, v2, :cond_0

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "logging"

    sget-boolean v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->mLoggingOn:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->startActivity(Landroid/content/Intent;)V

    .line 89
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const-string v0, "GestureTestMode2"

    const-string v1, "onCreate"

    const-string v2, "onCreate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->setContentView(I)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->initialize()V

    .line 43
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 57
    const-string v0, "GestureTestMode2"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 59
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 99
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 101
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;->deleteLogFiles()V

    .line 102
    const/4 v0, 0x1

    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b02e9
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 51
    const-string v0, "GestureTestMode2"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 54
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 47
    const-string v0, "GestureTestMode2"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    return-void
.end method
