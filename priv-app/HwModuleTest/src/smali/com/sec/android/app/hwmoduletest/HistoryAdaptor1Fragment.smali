.class Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;
.super Landroid/widget/ArrayAdapter;
.source "GestureTestMode1Fragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HistoryAdaptor1Fragment"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 197
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 199
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;

    .line 202
    .local v4, "item":Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;
    if-nez p2, :cond_0

    .line 203
    const-string v7, "HistoryAdaptor1Fragment"

    const-string v8, "getView()"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "convertView = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 205
    .local v6, "li":Landroid/view/LayoutInflater;
    const v7, 0x7f030028

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 211
    .end local v6    # "li":Landroid/view/LayoutInflater;
    .local v5, "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;->getCount()Ljava/lang/String;

    move-result-object v2

    .line 212
    .local v2, "countString":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;->getValueRawDataA()Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "Zmean":Ljava/lang/String;
    const v7, 0x7f0b00a0

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 214
    .local v3, "countView":Landroid/widget/TextView;
    const v7, 0x7f0b00a1

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 215
    .local v1, "ZmeanView":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const/high16 v7, -0x10000

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 218
    return-object v5

    .line 207
    .end local v0    # "Zmean":Ljava/lang/String;
    .end local v1    # "ZmeanView":Landroid/widget/TextView;
    .end local v2    # "countString":Ljava/lang/String;
    .end local v3    # "countView":Landroid/widget/TextView;
    .end local v5    # "layout":Landroid/widget/LinearLayout;
    :cond_0
    const-string v7, "HistoryAdaptor1Fragment"

    const-string v8, "getView()"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "convertView = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, p2

    .line 208
    check-cast v5, Landroid/widget/LinearLayout;

    .restart local v5    # "layout":Landroid/widget/LinearLayout;
    goto :goto_0
.end method
