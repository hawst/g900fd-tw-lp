.class Lcom/sec/android/app/hwmoduletest/SensorTest$2;
.super Landroid/content/BroadcastReceiver;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 442
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 443
    .local v0, "action":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "BroadcaseReceiver onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Action :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v2, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 446
    const-string v2, "COMMAND"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 447
    .local v1, "cmdData":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest;->catchCPsAccelerometerData(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5400(Lcom/sec/android/app/hwmoduletest/SensorTest;Ljava/lang/String;)V

    .line 452
    .end local v1    # "cmdData":Ljava/lang/String;
    :cond_0
    return-void
.end method
