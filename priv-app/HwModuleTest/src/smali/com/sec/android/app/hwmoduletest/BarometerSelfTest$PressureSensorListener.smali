.class Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;
.super Ljava/lang/Object;
.source "BarometerSelfTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PressureSensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$1;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 63
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 66
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 80
    :goto_0
    return-void

    .line 68
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mPressureValue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->access$102(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mTempValue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->access$202(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mAltitudeValue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->access$302(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;F)F

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;->access$700(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener$1;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerSelfTest$PressureSensorListener;)V

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
