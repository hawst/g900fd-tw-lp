.class Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;
.super Ljava/lang/Thread;
.source "LightSensorReadTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LightSensorThread"
.end annotation


# static fields
.field private static final SLEEP_DURATION:I = 0x32


# instance fields
.field private mAdc:F

.field private mBlueValue:I

.field private mCctValue:F

.field private mGreenValue:I

.field private mIsRunningTask:Z

.field private mLevel:I

.field private mLighSensortValues:[F

.field private mLightSensorValues_File:Ljava/lang/String;

.field private mLux:F

.field private mPreviousLux:F

.field private mRedValue:I

.field private mSupportRawLux:Z

.field mTestcase:Ljava/lang/String;

.field private mWhiteValue:I

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 261
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 263
    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mIsRunningTask:Z

    .line 266
    const/4 v1, 0x3

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    .line 270
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    .line 271
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    .line 272
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    .line 273
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    .line 274
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    .line 275
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mCctValue:F

    .line 276
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mPreviousLux:F

    .line 277
    const-string v1, "Light_LUX"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mTestcase:Ljava/lang/String;

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mTestcase:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mTestcase:Ljava/lang/String;

    const-string v1, "RAW_LUX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mSupportRawLux:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;

    .prologue
    .line 261
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;-><init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getLuxString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getRgbwString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getAdcString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getLevelString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getAlsWString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private computeForCctValue(Ljava/lang/String;)I
    .locals 18
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    .line 461
    if-nez p1, :cond_1

    .line 462
    const/4 v3, -0x1

    .line 539
    :cond_0
    :goto_0
    return v3

    .line 465
    :cond_1
    const-string v11, ","

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 468
    .local v10, "rgbwString":[Ljava/lang/String;
    const/4 v11, 0x0

    :try_start_0
    aget-object v11, v10, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    .line 469
    const/4 v11, 0x1

    aget-object v11, v10, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    .line 470
    const/4 v11, 0x2

    aget-object v11, v10, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    .line 471
    const/4 v11, 0x3

    aget-object v11, v10, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 477
    const/4 v3, -0x1

    .line 478
    .local v3, "cctValue":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    if-eqz v11, :cond_2

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    int-to-float v12, v12

    div-float v2, v11, v12

    .line 481
    .local v2, "I_cf":F
    :goto_1
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    const v12, 0xf230

    if-lt v11, v12, :cond_3

    .line 482
    const/16 v3, 0xa8c

    goto :goto_0

    .line 472
    .end local v2    # "I_cf":F
    .end local v3    # "cctValue":I
    :catch_0
    move-exception v9

    .line 473
    .local v9, "e":Ljava/lang/Exception;
    invoke-static {v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 474
    const/4 v3, -0x1

    goto :goto_0

    .line 478
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v3    # "cctValue":I
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-float v2, v11

    goto :goto_1

    .line 484
    .restart local v2    # "I_cf":F
    :cond_3
    float-to-double v12, v2

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    cmpg-double v11, v12, v14

    if-gtz v11, :cond_7

    .line 485
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    if-eqz v11, :cond_4

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    sub-int/2addr v11, v12

    int-to-double v12, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-double v14, v11

    div-double/2addr v12, v14

    const-wide v14, 0x3feae147ae147ae1L    # 0.84

    add-double v4, v12, v14

    .line 488
    .local v4, "ccti":D
    :goto_2
    const-wide v12, 0x3fc3d70a3d70a3d7L    # 0.155

    cmpg-double v11, v4, v12

    if-gtz v11, :cond_5

    .line 489
    const/16 v3, 0x1bb8

    goto/16 :goto_0

    .line 485
    .end local v4    # "ccti":D
    :cond_4
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    sub-int/2addr v11, v12

    int-to-double v12, v11

    const-wide v14, 0x3feae147ae147ae1L    # 0.84

    add-double v4, v12, v14

    goto :goto_2

    .line 490
    .restart local v4    # "ccti":D
    :cond_5
    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpl-double v11, v4, v12

    if-ltz v11, :cond_6

    .line 491
    const/16 v3, 0x9a3

    goto/16 :goto_0

    .line 493
    :cond_6
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    goto/16 :goto_0

    .line 496
    .end local v4    # "ccti":D
    :cond_7
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    if-eqz v11, :cond_8

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    sub-int/2addr v11, v12

    int-to-double v12, v11

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-double v14, v11

    div-double/2addr v12, v14

    const-wide v14, 0x3fd28f5c28f5c28fL    # 0.29

    add-double v4, v12, v14

    .line 499
    .restart local v4    # "ccti":D
    :goto_3
    const-wide v12, 0x3fc3d70a3d70a3d7L    # 0.155

    cmpg-double v11, v4, v12

    if-gtz v11, :cond_9

    .line 500
    const/16 v3, 0x1bb8

    goto/16 :goto_0

    .line 496
    .end local v4    # "ccti":D
    :cond_8
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    sub-int/2addr v11, v12

    int-to-double v12, v11

    const-wide v14, 0x3fd28f5c28f5c28fL    # 0.29

    add-double v4, v12, v14

    goto :goto_3

    .line 501
    .restart local v4    # "ccti":D
    :cond_9
    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpl-double v11, v4, v12

    if-ltz v11, :cond_a

    .line 502
    const/16 v3, 0x9a3

    goto/16 :goto_0

    .line 504
    :cond_a
    float-to-double v12, v2

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v12, v14

    if-lez v11, :cond_c

    float-to-double v12, v2

    const-wide v14, 0x3feccccccccccccdL    # 0.9

    cmpg-double v11, v12, v14

    if-gez v11, :cond_c

    .line 505
    const-wide v12, 0x3fd999999999999aL    # 0.4

    cmpl-double v11, v4, v12

    if-lez v11, :cond_b

    const-wide v12, 0x3ff199999999999aL    # 1.1

    cmpg-double v11, v4, v12

    if-gez v11, :cond_b

    .line 506
    const-wide v12, 0x40a5180000000000L    # 2700.0

    const-wide v14, -0x400dfbe76c8b4396L    # -1.126

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    goto/16 :goto_0

    .line 507
    :cond_b
    const-wide v12, 0x3ff199999999999aL    # 1.1

    cmpl-double v11, v4, v12

    if-lez v11, :cond_0

    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpg-double v11, v4, v12

    if-gez v11, :cond_0

    .line 508
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    goto/16 :goto_0

    .line 510
    :cond_c
    float-to-double v12, v2

    const-wide v14, 0x3feccccccccccccdL    # 0.9

    cmpl-double v11, v12, v14

    if-ltz v11, :cond_d

    float-to-double v12, v2

    const-wide v14, 0x3ffa666666666666L    # 1.65

    cmpg-double v11, v12, v14

    if-gez v11, :cond_d

    .line 511
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    goto/16 :goto_0

    .line 512
    :cond_d
    float-to-double v12, v2

    const-wide v14, 0x3ffa666666666666L    # 1.65

    cmpl-double v11, v12, v14

    if-ltz v11, :cond_f

    float-to-double v12, v2

    const-wide v14, 0x3ffe666666666666L    # 1.9

    cmpg-double v11, v12, v14

    if-gtz v11, :cond_f

    .line 513
    const-wide v12, 0x3fb1eb851eb851ecL    # 0.07

    float-to-double v14, v2

    mul-double v6, v12, v14

    .line 515
    .local v6, "cctixlamp":D
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    if-eq v11, v12, :cond_e

    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    sub-int/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    mul-float/2addr v11, v2

    float-to-double v12, v11

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    :goto_4
    double-to-float v8, v12

    .line 520
    .local v8, "cctxlamp_gain":F
    const v11, 0x45354000    # 2900.0f

    mul-float/2addr v11, v8

    float-to-double v12, v11

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v6, v7, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    .line 521
    goto/16 :goto_0

    .line 515
    .end local v8    # "cctxlamp_gain":F
    :cond_e
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    int-to-float v11, v11

    mul-float/2addr v11, v2

    float-to-double v12, v11

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    goto :goto_4

    .line 522
    .end local v6    # "cctixlamp":D
    :cond_f
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, 0x3fbf8a0902de00d2L    # 0.1232

    const-wide v16, -0x402147ae147ae148L    # -0.48

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v3, v12

    goto/16 :goto_0
.end method

.method private computeForLux(Ljava/lang/String;)I
    .locals 10
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 411
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 412
    .local v2, "rgbwString":[Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    .line 413
    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    .line 414
    const/4 v1, -0x1

    .line 415
    .local v1, "luxValue":I
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 418
    .local v0, "I_cf":F
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    .line 419
    const/4 v3, 0x0

    .line 432
    :goto_0
    return v3

    .line 422
    :cond_0
    float-to-double v4, v0

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_1

    .line 423
    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-double v6, v3

    const-wide v8, 0x3ff570a3d70a3d71L    # 1.34

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v1, v4

    :goto_1
    move v3, v1

    .line 432
    goto :goto_0

    .line 425
    :cond_1
    float-to-double v4, v0

    cmpg-double v3, v6, v4

    if-gez v3, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_2

    .line 426
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-double v4, v3

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    float-to-double v8, v0

    mul-double/2addr v6, v8

    const-wide v8, 0x3fdccccccccccccdL    # 0.45

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_1

    .line 429
    :cond_2
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    int-to-double v4, v3

    const-wide v6, 0x3fc70a3d70a3d70aL    # 0.18

    mul-double/2addr v4, v6

    const-wide v6, 0x401999999999999aL    # 6.4

    mul-double/2addr v4, v6

    float-to-double v6, v0

    div-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_1
.end method

.method private getAdcString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 557
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 558
    .local v0, "df":Ljava/text/DecimalFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mAdc:F

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Adc(from sysfs)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getAlsWString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 583
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ALS: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nW: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method private getLevelString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LEVEL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLuxString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 543
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 544
    .local v0, "df":Ljava/text/DecimalFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " lux(from manager)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getRGBWFromSysfs(Ljava/lang/String;)Z
    .locals 5
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 437
    if-nez p1, :cond_0

    .line 456
    :goto_0
    return v2

    .line 441
    :cond_0
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 444
    .local v1, "rgbwString":[Ljava/lang/String;
    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    .line 445
    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    .line 446
    const/4 v4, 0x2

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    .line 448
    const-string v4, "RGBSENSOR_SUPPORT_WHITE"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 449
    const/4 v4, 0x3

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v2, v3

    .line 456
    goto :goto_0

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private getRawLuxString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    array-length v1, v1

    if-le v1, v3, :cond_0

    .line 549
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 550
    .local v0, "df":Ljava/text/DecimalFormat;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Raw lux : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    aget v2, v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 553
    .end local v0    # "df":Ljava/text/DecimalFormat;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getRgbwString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 563
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "R: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mRedValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nG: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mGreenValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mBlueValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 566
    .local v0, "result":Ljava/lang/String;
    const-string v1, "RGBSENSOR_SUPPORT_WHITE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nW: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mWhiteValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 571
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mSupportRawLux:Z

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getRawLuxString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 572
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getRawLuxString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nCCT: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mCctValue:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 578
    return-object v0
.end method

.method private readToLightSensor()V
    .locals 7

    .prologue
    const v6, -0xffff01

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mReadTargetFileID:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 321
    const-string v3, "IS_DISPLAY_LIGHT_SENSOR_ADC_ONLY"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    .line 327
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$800(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    aget v3, v3, v2

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mCctValue:F

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getRGBWFromSysfs(Ljava/lang/String;)Z

    .line 340
    :cond_0
    :goto_1
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    if-le v3, v2, :cond_1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    add-int/lit8 v1, v3, -0x1

    .line 342
    .local v1, "tempLevel":I
    :cond_1
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mPreviousLux:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_8

    .line 343
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelDownTable:[J
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1700(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)[J

    move-result-object v4

    aget-wide v4, v4, v1

    long-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    .line 344
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    if-le v3, v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    add-int/lit8 v2, v2, -0x1

    :cond_2
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    .line 352
    :cond_3
    :goto_2
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    packed-switch v2, :pswitch_data_0

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v6}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    .line 372
    :goto_3
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mPreviousLux:F

    .line 378
    .end local v1    # "tempLevel":I
    :cond_4
    :goto_4
    return-void

    .line 324
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    aget v3, v3, v1

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    goto :goto_0

    .line 330
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$200(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v0, v3, v1

    .line 333
    .local v0, "mLightSensorValues_File_split":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mAdc:F

    goto :goto_1

    .line 335
    .end local v0    # "mLightSensorValues_File_split":Ljava/lang/String;
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLightSensorValues_File:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mAdc:F

    goto :goto_1

    .line 347
    .restart local v1    # "tempLevel":I
    :cond_8
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLux:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelUpTable:[J
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1800(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)[J

    move-result-object v3

    aget-wide v4, v3, v1

    long-to-float v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 348
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLevel:I

    goto :goto_2

    .line 354
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    const/high16 v3, -0x10000

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    goto :goto_3

    .line 357
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    const/16 v3, -0x100

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    goto :goto_3

    .line 360
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    const v3, -0xff0100

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    goto :goto_3

    .line 363
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    const v3, -0xff0001

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    goto :goto_3

    .line 366
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v2, v6}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$702(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;I)I

    goto/16 :goto_3

    .line 375
    .end local v1    # "tempLevel":I
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1900(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "readToLightSensor"

    const-string v4, "File Not Found => Value : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setBrightness(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    const/16 v1, 0xff

    .line 382
    packed-switch p1, :pswitch_data_0

    .line 405
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    .line 407
    :goto_0
    return-void

    .line 384
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 387
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 390
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0x5a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 393
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 396
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 399
    :pswitch_5
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 402
    :pswitch_6
    sget-object v0, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    goto :goto_0

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 282
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 286
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 291
    :goto_0
    return-void

    .line 288
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mLighSensortValues:[F

    goto :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method

.method public declared-synchronized run()V
    .locals 4

    .prologue
    .line 296
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mIsRunningTask:Z

    if-eqz v1, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->readToLightSensor()V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1300(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    :cond_0
    const-wide/16 v2, 0x32

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "ie":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1400(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "run"

    const-string v3, "Thread interrupted"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 308
    monitor-exit p0

    return-void

    .line 296
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public setEnable(Z)V
    .locals 4
    .param p1, "e"    # Z

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1500(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "setEnable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->mIsRunningTask:Z

    .line 313
    return-void
.end method
