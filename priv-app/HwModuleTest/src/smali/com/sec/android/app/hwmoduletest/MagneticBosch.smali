.class public Lcom/sec/android/app/hwmoduletest/MagneticBosch;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "MagneticBosch.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mReadTarget:Ljava/lang/String;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Self:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_BmZ:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_BmZ:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "MagneticBosch"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSenserID:[I

    .line 42
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    .line 43
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    .line 45
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    .line 48
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->WHAT_UPDATE:I

    .line 114
    new-instance v0, Lcom/sec/android/app/hwmoduletest/MagneticBosch$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch$1;-><init>(Lcom/sec/android/app/hwmoduletest/MagneticBosch;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mHandler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/MagneticBosch;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticBosch;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/MagneticBosch;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticBosch;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->update()V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 124
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 125
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    .line 127
    const v0, 0x7f0b0180

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_BmZ:Landroid/widget/TableRow;

    .line 128
    const v0, 0x7f0b0181

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_BmZ:Landroid/widget/TextView;

    .line 130
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_ADC:Landroid/widget/TableRow;

    .line 131
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_X:Landroid/widget/TextView;

    .line 132
    const v0, 0x7f0b0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Y:Landroid/widget/TextView;

    .line 133
    const v0, 0x7f0b0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Z:Landroid/widget/TextView;

    .line 135
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTextResult:Landroid/widget/TextView;

    .line 136
    return-void
.end method

.method private update()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "data":[Ljava/lang/String;
    const/4 v1, 0x1

    .line 144
    .local v1, "isPass":Z
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v3, "BMC150_NEWEST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_BmZ:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_3

    .line 152
    aget-object v2, v0, v7

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "1"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    :goto_0
    const/4 v2, 0x5

    aget-object v2, v0, v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_BmZ:Landroid/widget/TextView;

    aget-object v3, v0, v8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_X:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_d

    const-string v2, "PASS"

    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_e

    const v2, -0xffff01

    :goto_4
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "update"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Result:"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v1, :cond_f

    const-string v2, "PASS"

    :goto_5
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-void

    .line 155
    :cond_1
    const/4 v1, 0x0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "-1"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 162
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 170
    :cond_3
    const/4 v1, 0x0

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_BmZ:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 181
    :cond_4
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    if-ge v2, v3, :cond_5

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_8

    .line 185
    aget-object v2, v0, v6

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :goto_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Initialized Return : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_5
    :goto_7
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    if-ge v2, v3, :cond_6

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_BmZ:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_a

    .line 205
    aget-object v2, v0, v8

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_BmZ:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [Self]BmZ : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_6
    :goto_9
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_None:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    if-ge v2, v3, :cond_0

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v2, v7}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_c

    .line 225
    aget-object v2, v0, v8

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 226
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Pass"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_a
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_X:Landroid/widget/TextView;

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Y:Landroid/widget/TextView;

    aget-object v3, v0, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Z:Landroid/widget/TextView;

    aget-object v3, v0, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retuen : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", [ADC]X:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Z:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 188
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 194
    :cond_8
    const/4 v1, 0x0

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_Initialized:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Initialized - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 208
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 214
    :cond_a
    const/4 v1, 0x0

    .line 215
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_BmZ:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "Self - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 228
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v1, 0x0

    goto/16 :goto_a

    .line 237
    :cond_c
    const/4 v1, 0x0

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_X:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v3, "NONE"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "update"

    const-string v4, "ADC - Fail : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247
    :cond_d
    const-string v2, "FAIL"

    goto/16 :goto_3

    .line 248
    :cond_e
    const/high16 v2, -0x10000

    goto/16 :goto_4

    .line 249
    :cond_f
    const-string v2, "FAIL"

    goto/16 :goto_5
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const v0, 0x7f030055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->setContentView(I)V

    .line 58
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v0, "READ_TARGET_GEOMAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mReadTarget:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "readTarget : ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mReadTarget:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->init()V

    .line 65
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 112
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 68
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v1, "BMC150_COMBINATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v1, "BMC150_NEWEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mReadTarget:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mFeature:Ljava/lang/String;

    const-string v1, "BMC150_NEWEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    .line 77
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    .line 78
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    .line 97
    :cond_1
    :goto_0
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSenserID:[I

    .line 103
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 107
    return-void

    .line 80
    :cond_2
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    .line 81
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    .line 82
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    goto :goto_0

    .line 85
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mReadTarget:Ljava/lang/String;

    const-string v1, "MANAGER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 86
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    .line 87
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    .line 88
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    goto :goto_0

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mReadTarget:Ljava/lang/String;

    const-string v1, "FILE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Initialized:I

    .line 91
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_Self:I

    .line 92
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticBosch;->mSensorID_ADC:I

    goto :goto_0
.end method
