.class Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;
.super Ljava/lang/Object;
.source "GyroscopeBosch.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mGyroSelfTestLayout:Landroid/widget/TableLayout;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Landroid/widget/TableLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 64
    const-string v0, "BMG160"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSubType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BMI055"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSubType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BMI160"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSubType:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;->this$0:Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    # getter for: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSelfTestResults:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)[Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->showTestResults_BMG160([Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->access$300(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;[Ljava/lang/String;)V

    .line 72
    :cond_1
    return-void
.end method
