.class Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;
.super Ljava/lang/Object;
.source "AccGraphActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AccGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 116
    check-cast p1, Landroid/widget/Button;

    .end local p1    # "arg0":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 118
    .local v0, "text":Ljava/lang/String;
    const-string v1, "LPF Status : OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const-string v1, "MOTOR_LPF"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$500(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "LPF Status : ON"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    return-void

    .line 123
    :cond_0
    const-string v1, "MOTOR_LPF"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/AccGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->btnLPFOnOff:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccGraphActivity;->access$500(Lcom/sec/android/app/hwmoduletest/AccGraphActivity;)Landroid/widget/Button;

    move-result-object v1

    const-string v2, "LPF Status : OFF"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
