.class Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;
.super Ljava/lang/Object;
.source "TempHumidGraphActivity.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$1;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 55
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 66
    :goto_0
    return-void

    .line 60
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempHumidGraph:Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->addValueTemp(FF)V

    goto :goto_0

    .line 63
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempHumidGraph:Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

    move-result-object v0

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->addValueHumid(FF)V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
