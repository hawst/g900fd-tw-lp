.class public Lcom/sec/android/app/hwmoduletest/view/AccGraph;
.super Landroid/view/View;
.source "AccGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private GRAPH_SCALING_COOR:F

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private final TEXT_SCALING_COOR:F

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mFeature_Accelerometer:Ljava/lang/String;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mPathZ:Landroid/graphics/Path;

.field private mRawDataMax:[F

.field private mRawDataMin:[F

.field private mRawDataRange:Ljava/lang/String;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueZ:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;

.field private rate:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 57
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INCREASING_COOR:I

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->TEXT_SCALING_COOR:F

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    .line 42
    const-string v0, "AccSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    .line 44
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    .line 47
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->DEBUG:Z

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->init(Landroid/content/Context;)V

    .line 59
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 47
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INCREASING_COOR:I

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->TEXT_SCALING_COOR:F

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    .line 42
    const-string v0, "AccSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    .line 44
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    .line 47
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->DEBUG:Z

    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->init(Landroid/content/Context;)V

    .line 69
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 47
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INCREASING_COOR:I

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->TEXT_SCALING_COOR:F

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    .line 33
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    .line 42
    const-string v0, "AccSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    .line 44
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    .line 47
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->DEBUG:Z

    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->init(Landroid/content/Context;)V

    .line 64
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 47
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method private calRawData()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 197
    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    .line 198
    .local v1, "tempX":F
    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    .line 199
    .local v2, "tempY":F
    const/high16 v3, -0x800000    # Float.NEGATIVE_INFINITY

    .line 202
    .local v3, "tempZ":F
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 213
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aget v4, v4, v8

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 214
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aput v1, v4, v8

    .line 217
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aget v4, v4, v9

    cmpg-float v4, v2, v4

    if-gez v4, :cond_1

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aput v2, v4, v9

    .line 221
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aget v4, v4, v10

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    aput v3, v4, v10

    .line 226
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aget v4, v4, v8

    cmpl-float v4, v1, v4

    if-lez v4, :cond_3

    .line 227
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aput v1, v4, v8

    .line 230
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aget v4, v4, v9

    cmpl-float v4, v2, v4

    if-lez v4, :cond_4

    .line 231
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aput v2, v4, v9

    .line 234
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aget v4, v4, v10

    cmpl-float v4, v3, v4

    if-lez v4, :cond_5

    .line 235
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    aput v3, v4, v10

    .line 237
    :cond_5
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "calRawData"

    const-string v6, "get value fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "calRawData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Size of X, Y, Z : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "calRawData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LIST_SIZE value : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->LIST_SIZE:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v8, -0x1000000

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mContext:Landroid/content/Context;

    .line 73
    const-string v1, "Accelerometer"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getUIRate(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "init"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 76
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mScreenWidth:I

    .line 77
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mScreenHeight:I

    .line 78
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mScreenHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    .line 79
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    div-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->LIST_SIZE:I

    .line 80
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 81
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 86
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 91
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 96
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 98
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 103
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 105
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    cmpl-float v1, v1, v6

    if-nez v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 112
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 114
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->setSpecGraphScale()V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->invalidate()V

    .line 121
    return-void

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_1
.end method

.method private setPath()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x41200000    # 10.0f

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 241
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 243
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 245
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    invoke-virtual {v2, v5, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    invoke-virtual {v2, v5, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    invoke-virtual {v2, v5, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 250
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->invalidate()V

    .line 260
    return-void
.end method

.method private setSpecGraphScale()V
    .locals 4

    .prologue
    .line 263
    const-string v0, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mFeature_Accelerometer:Ljava/lang/String;

    .line 264
    const-string v0, "ACCSENSOR_DATA_RANGE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataRange:Ljava/lang/String;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataRange:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataRange:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 267
    :cond_0
    const v0, 0x3ba3d70a    # 0.005f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    .line 272
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setSpecGraphScale"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GRAPH_SCALING_COOR :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void

    .line 269
    :cond_1
    const/high16 v0, 0x43480000    # 200.0f

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataRange:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->GRAPH_SCALING_COOR:F

    goto :goto_0
.end method


# virtual methods
.method public addValue(FFF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->calRawData()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->setPath()V

    .line 194
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 125
    const/4 v7, 0x0

    .line 126
    .local v7, "textx":F
    const/4 v8, 0x0

    .line 127
    .local v8, "texty":F
    const/4 v9, 0x0

    .line 130
    .local v9, "textz":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 140
    :goto_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->rate:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 141
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 142
    const-string v0, "x :"

    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 143
    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x43020000    # 130.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 144
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 145
    const-string v0, "y :"

    const/high16 v1, 0x43200000    # 160.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 146
    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x438c0000    # 280.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 147
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 148
    const-string v0, "z :"

    const/high16 v1, 0x439b0000    # 310.0f

    const/high16 v2, 0x42480000    # 50.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 149
    const/high16 v1, 0x43af0000    # 350.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x43d70000    # 430.0f

    const/high16 v4, 0x42200000    # 40.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 150
    invoke-static {v9}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43af0000    # 350.0f

    const/high16 v2, 0x428c0000    # 70.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 151
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, -0xc8

    int-to-float v2, v0

    const/high16 v3, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, 0xc8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 153
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mPathZ:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mZPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 158
    const-string v0, "Raw Data"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x19

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 159
    const-string v0, "MIN"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x32

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 160
    const-string v0, "X :"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 163
    const-string v0, "Y :"

    const/high16 v1, 0x43200000    # 160.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 166
    const-string v0, "Z :"

    const/high16 v1, 0x439b0000    # 310.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMin:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43af0000    # 350.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x4b

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 169
    const-string v0, "MAX"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit8 v2, v2, 0x7d

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 170
    const-string v0, "X :"

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42480000    # 50.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 173
    const-string v0, "Y :"

    const/high16 v1, 0x43200000    # 160.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43480000    # 200.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 176
    const-string v0, "Z :"

    const/high16 v1, 0x439b0000    # 310.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mRawDataMax:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43af0000    # 350.0f

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->INIT_COOR_Y:I

    add-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 179
    return-void

    .line 133
    :catch_0
    move-exception v6

    .line 134
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    const-string v2, "get value fail"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Size of X, Y, Z : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->mValueZ:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LIST_SIZE value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/view/AccGraph;->LIST_SIZE:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
