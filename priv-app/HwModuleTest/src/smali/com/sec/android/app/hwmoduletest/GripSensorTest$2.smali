.class Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;
.super Landroid/content/BroadcastReceiver;
.source "GripSensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GripSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f080092

    const v9, 0x7f070021

    const v8, 0x7f08008f

    const v7, 0x7f070002

    .line 240
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "action":Ljava/lang/String;
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Action :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v3, "com.sec.android.app.factorytest"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 244
    const-string v3, "COMMAND"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "cmdData":Ljava/lang/String;
    const-string v2, "null"

    .line 246
    .local v2, "sensorData":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 247
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 249
    :cond_0
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cmdData=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sensorData=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->IS_GRIP_COUNT:I
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$000()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 253
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "IS_GRIP_COUNT == 1"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v3, "0100"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 260
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "________ 0100 ________"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    .end local v1    # "cmdData":Ljava/lang/String;
    .end local v2    # "sensorData":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 261
    .restart local v1    # "cmdData":Ljava/lang/String;
    .restart local v2    # "sensorData":Ljava/lang/String;
    :cond_2
    const-string v3, "0000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V

    .line 265
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "________ 0000 ________"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_3
    const-string v3, "010001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "010000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "000001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 271
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V

    goto :goto_0

    .line 277
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 282
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 285
    :cond_6
    const-string v3, "010001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V

    .line 292
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_SIDE_BACK_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 293
    :cond_7
    const-string v3, "010000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 296
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 297
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 298
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 299
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V

    .line 301
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_SIDE_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 302
    :cond_8
    const-string v3, "000001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 306
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v9}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 307
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 308
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 309
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest;I)V

    .line 310
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_BACK_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    :cond_9
    const-string v3, "000000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 317
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 318
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 320
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v3, v4, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 323
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 325
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF else"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 328
    .end local v1    # "cmdData":Ljava/lang/String;
    .end local v2    # "sensorData":Ljava/lang/String;
    :cond_b
    const-string v3, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest;->sendToRilGripSensorStop()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest;)V

    .line 331
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "GRIPGRIP *******   GripSensorTest - onReceive - get Stop GripTest ******* GRIPGRIP"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest;->finish()V

    goto/16 :goto_0
.end method
