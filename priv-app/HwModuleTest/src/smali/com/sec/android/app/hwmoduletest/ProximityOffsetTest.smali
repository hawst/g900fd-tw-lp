.class public Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ProximityOffsetTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;
    }
.end annotation


# static fields
.field private static final MSG_GET_ADC:B = 0x0t

.field private static final MSG_GET_OFFSET:B = 0x1t

.field private static final MSG_RESET:B = 0x2t

.field private static final PATH_PROX_OFFSET:Ljava/lang/String; = "/sys/class/sensors/proximity_sensor/prox_cal"

.field private static final TAG:Ljava/lang/String; = "Proximityoffset"


# instance fields
.field private mAdcView:Landroid/widget/TextView;

.field private mBackview:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

.field private mOffsetBtn:Landroid/widget/Button;

.field private mOffsetResult:Landroid/widget/TextView;

.field private mOffsetValue:I

.field private mOffsetView:Landroid/widget/TextView;

.field private mResetBtn:Landroid/widget/Button;

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTimer:Ljava/util/Timer;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "ProximityOffsetTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetValue:I

    .line 52
    new-instance v0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mAdcView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->stopVibrate()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mBackview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->startVibrate()V

    return-void
.end method

.method private startVibrate()V
    .locals 3

    .prologue
    .line 123
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 126
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 127
    return-void

    .line 123
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibrate()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 131
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 168
    .local v1, "fw":Ljava/io/FileWriter;
    const/4 v3, 0x0

    .line 171
    .local v3, "result":Z
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 173
    const/4 v3, 0x1

    .line 177
    if-eqz v2, :cond_2

    .line 179
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 186
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return v3

    .line 180
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 182
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 174
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 175
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "writeFile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOExceptionfilepath : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " value : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    if-eqz v1, :cond_0

    .line 179
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 180
    :catch_2
    move-exception v0

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_1

    .line 179
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 182
    :cond_1
    :goto_3
    throw v4

    .line 180
    :catch_3
    move-exception v0

    .line 181
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "writeFile"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 177
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 174
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0b01b8

    if-ne v3, v4, :cond_2

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "onClick"

    const-string v5, "offset"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v3, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v4, "0"

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 138
    const-string v3, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v4, "1"

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->readOffset()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "OffsetValue":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetValue:I

    .line 141
    aget-object v3, v0, v8

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "temp":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    int-to-double v4, v3

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 143
    .local v2, "threshold":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 145
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetValue:I

    if-le v3, v2, :cond_1

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetResult:Landroid/widget/TextView;

    const-string v4, "Fail"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetResult:Landroid/widget/TextView;

    const/high16 v4, -0x10000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 157
    .end local v0    # "OffsetValue":[Ljava/lang/String;
    .end local v1    # "temp":Ljava/lang/String;
    .end local v2    # "threshold":I
    :cond_0
    :goto_0
    return-void

    .line 149
    .restart local v0    # "OffsetValue":[Ljava/lang/String;
    .restart local v1    # "temp":Ljava/lang/String;
    .restart local v2    # "threshold":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetResult:Landroid/widget/TextView;

    const-string v4, "Pass"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetResult:Landroid/widget/TextView;

    const v4, -0xffff01

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 152
    .end local v0    # "OffsetValue":[Ljava/lang/String;
    .end local v1    # "temp":Ljava/lang/String;
    .end local v2    # "threshold":I
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    const v4, 0x7f0b01b9

    if-ne v3, v4, :cond_0

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "onClick"

    const-string v5, "reset"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v3, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v4, "0"

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->setContentView(I)V

    .line 79
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mVibrator:Landroid/os/Vibrator;

    .line 80
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensor:Landroid/hardware/Sensor;

    .line 82
    const v0, 0x7f0b01ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mBackview:Landroid/view/View;

    .line 83
    const v0, 0x7f0b01b7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0b01af

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mAdcView:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0b01c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetView:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0b01b3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetResult:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0b01b8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetBtn:Landroid/widget/Button;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v0, 0x7f0b01b9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mResetBtn:Landroid/widget/Button;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mTimer:Ljava/util/Timer;

    .line 92
    new-instance v0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;-><init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 94
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 120
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->stopVibrate()V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    # invokes: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->access$500(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;)V

    .line 113
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    # invokes: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->access$400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 104
    return-void
.end method

.method public readOffset()Ljava/lang/String;
    .locals 9

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 192
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 195
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "sys/class/sensors/proximity_sensor/prox_cal"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 201
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readOffset"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 205
    if-eqz v3, :cond_4

    .line 207
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 214
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    const-string v0, "NONE"

    .end local v0    # "data":Ljava/lang/String;
    :cond_2
    return-object v0

    .line 208
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 209
    .local v1, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readOffset"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 210
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 202
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 203
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readOffset"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ProximitySensor IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 205
    if-eqz v2, :cond_1

    .line 207
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 208
    :catch_2
    move-exception v1

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "readOffset"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 205
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_3

    .line 207
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 210
    :cond_3
    :goto_3
    throw v4

    .line 208
    :catch_3
    move-exception v1

    .line 209
    .restart local v1    # "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "readOffset"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 205
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 202
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_4
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method
