.class Lcom/sec/android/app/hwmoduletest/HDMI$1;
.super Landroid/content/BroadcastReceiver;
.source "HDMI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HDMI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HDMI;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HDMI;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    .line 28
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 31
    .local v0, "action":Ljava/lang/String;
    :try_start_1
    const-string v3, "com.android.samsungtest.HDMITEST_STOP"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 32
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 33
    :cond_1
    :try_start_2
    const-string v3, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 34
    const-string v3, "state"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 35
    .local v2, "state":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # getter for: Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$000(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AudioManager.ACTION_HDMI_AUDIO_PLUG - state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 39
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # getter for: Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$100(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "onReceive"

    const-string v5, "Already UIHDMI finished"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_2
    if-ne v2, v7, :cond_3

    .line 43
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # invokes: Lcom/sec/android/app/hwmoduletest/HDMI;->stopMedia()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$200(Lcom/sec/android/app/hwmoduletest/HDMI;)V

    .line 44
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # getter for: Lcom/sec/android/app/hwmoduletest/HDMI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$300(Lcom/sec/android/app/hwmoduletest/HDMI;)Landroid/media/AudioManager;

    move-result-object v3

    const-string v4, "factory_test_route=hdmi"

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 45
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    const v4, 0x7f050006

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/HDMI;->playMedia(IZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 70
    .end local v2    # "state":I
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # getter for: Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$600(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mBroadcastReceiver.onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 28
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 46
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v2    # "state":I
    :cond_3
    if-nez v2, :cond_0

    .line 47
    :try_start_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # invokes: Lcom/sec/android/app/hwmoduletest/HDMI;->stopMedia()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$200(Lcom/sec/android/app/hwmoduletest/HDMI;)V

    .line 48
    const-wide/16 v4, 0x12c

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 49
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$402(Lcom/sec/android/app/hwmoduletest/HDMI;Z)Z

    .line 50
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    # invokes: Lcom/sec/android/app/hwmoduletest/HDMI;->turnOffAudioPath()V
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->access$500(Lcom/sec/android/app/hwmoduletest/HDMI;)V

    .line 51
    const-wide/16 v4, 0x12c

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 53
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/HDMI;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 54
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI$1;->this$0:Lcom/sec/android/app/hwmoduletest/HDMI;

    const v4, 0x7f050006

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/HDMI;->playMedia(IZ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method
