.class public Lcom/sec/android/app/hwmoduletest/HallICTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HallICTest.java"


# static fields
.field protected static final CLASS_NAME:Ljava/lang/String; = "HallICTest"

.field public static final FOLDER_STATE_CLOSE:I = 0x1

.field public static final FOLDER_STATE_ERROR:I = 0x2

.field public static final FOLDER_STATE_OPEN:I = 0x0

.field public static final FONT_COLOR_FAILD:I = -0x10000

.field public static final FONT_COLOR_PASS:I = -0xffff01

.field protected static final KEY_TIMER_EXPIRED:I = 0x1

.field protected static final MILLIS_IN_SEC:I

.field public static final TESTCASE_CLOSE_CHECK:I = 0x3

.field public static final TESTCASE_ONCREATE_RELEASE_CHECK:I = 0x0

.field public static final TESTCASE_OPEN_CHECK:I = 0x4

.field public static final TESTCASE_RELEASE_CHECK:I = 0x2

.field public static final TESTCASE_RELEASE_CHECK2:I = 0x7

.field public static final TESTCASE_START_RELEASE_CHECK2:I = 0x5

.field public static final TESTCASE_WORKING_CHECK:I = 0x1

.field public static final TESTCASE_WORKING_CHECK2:I = 0x6


# instance fields
.field private final KEYCODE_FOLDER_CLOSE:I

.field private final KEYCODE_FOLDER_OPEN:I

.field private _timer:Landroid/os/CountDownTimer;

.field private mCurrentTime:J

.field private mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

.field private mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

.field private mHallIcTestIndex1:Landroid/widget/TextView;

.field private mHallIcTestIndex2:Landroid/widget/TextView;

.field private mHallIcTestTable:Landroid/widget/TableLayout;

.field private mHallIcTestView:Landroid/widget/TextView;

.field private mHallIcTestView2:Landroid/widget/TextView;

.field private mIsHallICTestAllPass:Z

.field private mIsPressedBackkey:Z

.field private mIsReleasePass:Z

.field private mIsReleasePass2:Z

.field private mIsWorkingPass:Z

.field private mIsWorkingPass2:Z

.field private mReleaseTextView:Landroid/widget/TextView;

.field private mReleaseTextView2:Landroid/widget/TextView;

.field private mSYSFS_FolderChcekingTimer:Ljava/util/Timer;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkingTextView:Landroid/widget/TextView;

.field private mWorkingTextView2:Landroid/widget/TextView;

.field private mmyTask:Ljava/util/TimerTask;

.field private nHallICTestState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    const-string v0, "HALLIC_TEST_MILLIS_SEC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/sec/android/app/hwmoduletest/HallICTest;->MILLIS_IN_SEC:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    const-string v0, "HallICTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    .line 76
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsHallICTestAllPass:Z

    .line 97
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 100
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsPressedBackkey:Z

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mCurrentTime:J

    .line 110
    const/16 v0, 0xea

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->KEYCODE_FOLDER_OPEN:I

    .line 111
    const/16 v0, 0xeb

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->KEYCODE_FOLDER_CLOSE:I

    .line 169
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HallICTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/HallICTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mTimerHandler:Landroid/os/Handler;

    .line 51
    return-void
.end method

.method private DisplayResult(I)V
    .locals 10
    .param p1, "nCurrentTest"    # I

    .prologue
    const-wide/16 v8, 0x2bc

    const/high16 v6, -0x10000

    const v5, -0xffff01

    const/4 v4, 0x1

    .line 436
    const-string v0, "HallICTest"

    const-string v1, "DisplayResult"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mIsWorkingPass = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIsReleasePass : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsWorkingPass2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mIsReleasePass2 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v0, "HallICTest"

    const-string v1, "DisplayResult"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nCurrentTest = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsHallICTestAllPass:Z

    if-ne v0, v4, :cond_1

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    if-ne p1, v4, :cond_2

    .line 445
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    if-eqz v0, :cond_6

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, v8, v9}, Landroid/os/Vibrator;->vibrate(J)V

    .line 456
    :cond_2
    :goto_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 457
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    if-eqz v0, :cond_7

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, v8, v9}, Landroid/os/Vibrator;->vibrate(J)V

    .line 468
    :cond_3
    :goto_2
    const/4 v0, 0x6

    if-ne p1, v0, :cond_4

    .line 469
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    if-eqz v0, :cond_8

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, v8, v9}, Landroid/os/Vibrator;->vibrate(J)V

    .line 480
    :cond_4
    :goto_3
    const/4 v0, 0x7

    if-ne p1, v0, :cond_5

    .line 481
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    if-eqz v0, :cond_9

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, v8, v9}, Landroid/os/Vibrator;->vibrate(J)V

    .line 491
    :cond_5
    :goto_4
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    if-ne v0, v4, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    if-ne v0, v4, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    if-ne v0, v4, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    if-ne v0, v4, :cond_0

    .line 492
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsHallICTestAllPass:Z

    .line 493
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->setResult(I)V

    .line 495
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->onExit()V

    goto :goto_0

    .line 450
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 462
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 474
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 486
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4
.end method

.method private HallICTestInit()V
    .locals 14

    .prologue
    const v3, 0x7f0b0105

    const/4 v13, 0x1

    const/4 v12, 0x5

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 310
    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    const-string v2, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 313
    const v0, 0x7f0b0106

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f080251

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    const v0, 0x7f0b0108

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f080250

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    :cond_0
    const v0, 0x7f0b0103

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestTable:Landroid/widget/TableLayout;

    .line 319
    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestView:Landroid/widget/TextView;

    .line 320
    const v0, 0x7f0b0107

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView:Landroid/widget/TextView;

    .line 321
    const v0, 0x7f0b0109

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView:Landroid/widget/TextView;

    .line 323
    const v0, 0x7f0b010b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestView2:Landroid/widget/TextView;

    .line 324
    const v0, 0x7f0b010d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    .line 325
    const v0, 0x7f0b010f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    .line 327
    const v0, 0x7f0b0104

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestIndex1:Landroid/widget/TextView;

    .line 328
    const v0, 0x7f0b010a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestIndex2:Landroid/widget/TextView;

    .line 330
    const-string v0, "HALL_IC_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 332
    .local v11, "viewStartY":I
    if-eqz v11, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestTable:Landroid/widget/TableLayout;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setGravity(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestView:Landroid/widget/TextView;

    invoke-virtual {v0, v5, v11, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 338
    :cond_1
    const-string v0, "HALL_IC_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    const v0, 0x7f0b0110

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/HallICPoint;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setVisibility(I)V

    .line 342
    const-string v0, "HALL_IC_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getViewPointF(Ljava/lang/String;)Landroid/graphics/PointF;

    move-result-object v6

    .line 344
    .local v6, "point":Landroid/graphics/PointF;
    iget v7, v6, Landroid/graphics/PointF;->x:F

    .line 345
    .local v7, "pxX":F
    iget v8, v6, Landroid/graphics/PointF;->y:F

    .line 347
    .local v8, "pxY":F
    const-string v0, "HALL_IC_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getKeyTextSize(Ljava/lang/String;)F

    move-result v9

    .line 349
    .local v9, "radius":F
    const-string v0, "HALL_IC_TEST"

    const-string v1, "unit"

    const-string v2, "mm"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 350
    .local v10, "unit":Ljava/lang/String;
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unit: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v0, "mm"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 352
    iget v0, v6, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v12, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 354
    iget v0, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v12, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v12, v9, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    .line 361
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setRadius(F)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setPoint(FF)V

    .line 364
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "radius = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pxX = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pxY = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    .end local v6    # "point":Landroid/graphics/PointF;
    .end local v7    # "pxX":F
    .end local v8    # "pxY":F
    .end local v9    # "radius":F
    .end local v10    # "unit":Ljava/lang/String;
    :cond_3
    const-string v0, "HALL_IC_TEST_2ND"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 370
    const v0, 0x7f0b0111

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/HallICPoint;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setVisibility(I)V

    .line 373
    const-string v0, "HALL_IC_TEST_2ND"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getViewPointF(Ljava/lang/String;)Landroid/graphics/PointF;

    move-result-object v6

    .line 375
    .restart local v6    # "point":Landroid/graphics/PointF;
    iget v7, v6, Landroid/graphics/PointF;->x:F

    .line 376
    .restart local v7    # "pxX":F
    iget v8, v6, Landroid/graphics/PointF;->y:F

    .line 378
    .restart local v8    # "pxY":F
    const-string v0, "HALL_IC_TEST_2ND"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getKeyTextSize(Ljava/lang/String;)F

    move-result v9

    .line 380
    .restart local v9    # "radius":F
    const-string v0, "HALL_IC_TEST_2ND"

    const-string v1, "unit"

    const-string v2, "mm"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 381
    .restart local v10    # "unit":Ljava/lang/String;
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unit2: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v0, "mm"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 383
    iget v0, v6, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v12, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 385
    iget v0, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v12, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v8

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v12, v9, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v9}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setRadius(F)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v0, v7, v8}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setPoint(FF)V

    .line 395
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "radius2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pxX2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v0, "HallICTest"

    const-string v1, "HallICTestInit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pxY2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    .end local v6    # "point":Landroid/graphics/PointF;
    .end local v7    # "pxX":F
    .end local v8    # "pxY":F
    .end local v9    # "radius":F
    .end local v10    # "unit":Ljava/lang/String;
    :goto_0
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    .line 413
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 415
    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "SUPPORT_BOOK_COVER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 417
    :cond_5
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 421
    :cond_6
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HallICTest$3;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/HallICTest$3;-><init>(Lcom/sec/android/app/hwmoduletest/HallICTest;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->_timer:Landroid/os/CountDownTimer;

    .line 433
    return-void

    .line 399
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestIndex1:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestIndex2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallIcTestView2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mWorkingTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mReleaseTextView2:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    const v0, 0x7f0b010c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    const v0, 0x7f0b010e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    iput-boolean v13, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    .line 407
    iput-boolean v13, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/HallICTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HallICTest;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsPressedBackkey:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HallICTest;)Landroid/os/Vibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HallICTest;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;

    return-object v0
.end method

.method private hallIc_by_sysfs()I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 544
    const-string v5, "HallICTest"

    const-string v6, "hallIc_by_sysfs"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    const/4 v2, 0x0

    .line 548
    .local v2, "sysfsVal":Ljava/lang/String;
    :try_start_0
    const-string v5, "PATH_HALLIC_STATE"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/HallICTest;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 550
    move-object v1, v2

    .line 551
    .local v1, "state":Ljava/lang/String;
    const-string v5, "HallICTest"

    const-string v6, "hallIc_by_sysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "state value ( "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " )"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const-string v5, "SUPPORT_BOOK_COVER"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 554
    const-string v5, "OPEN"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 570
    .end local v1    # "state":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 556
    .restart local v1    # "state":Ljava/lang/String;
    :cond_1
    const-string v3, "CLOSE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    .line 557
    goto :goto_0

    .line 560
    :cond_2
    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 561
    goto :goto_0

    .line 562
    :cond_3
    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 570
    .end local v1    # "state":Ljava/lang/String;
    :cond_4
    :goto_1
    const/4 v3, 0x2

    goto :goto_0

    .line 566
    :catch_0
    move-exception v0

    .line 567
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HallICTest"

    const-string v4, "hallIc_by_sysfs"

    const-string v5, "Exception"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private hallIc_by_sysfs2()I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 574
    const-string v5, "HallICTest"

    const-string v6, "hallIc_by_sysfs"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const/4 v2, 0x0

    .line 578
    .local v2, "sysfsVal":Ljava/lang/String;
    :try_start_0
    const-string v5, "PATH_HALLIC_STATE2"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/HallICTest;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 580
    move-object v1, v2

    .line 581
    .local v1, "state":Ljava/lang/String;
    const-string v5, "HallICTest"

    const-string v6, "hallIc_by_sysfs2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "state value ( "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " )"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v5, "SUPPORT_BOOK_COVER"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 584
    const-string v5, "OPEN"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 600
    .end local v1    # "state":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 586
    .restart local v1    # "state":Ljava/lang/String;
    :cond_1
    const-string v3, "CLOSE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    .line 587
    goto :goto_0

    .line 590
    :cond_2
    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v3, v4

    .line 591
    goto :goto_0

    .line 592
    :cond_3
    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 600
    .end local v1    # "state":Ljava/lang/String;
    :cond_4
    :goto_1
    const/4 v3, 0x2

    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    .line 597
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HallICTest"

    const-string v4, "hallIc_by_sysfs"

    const-string v5, "Exception"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 510
    const-string v3, ""

    .line 511
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 512
    .local v0, "buf":Ljava/io/BufferedReader;
    const-string v4, "HallICTest"

    const-string v5, "readOneLine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "filepath: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1fa0

    invoke-direct {v1, v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 518
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 519
    if-eqz v3, :cond_0

    .line 520
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 527
    :cond_0
    if-eqz v1, :cond_4

    .line 529
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 536
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    .line 537
    const-string v3, ""

    .line 539
    .end local v3    # "result":Ljava/lang/String;
    :cond_2
    return-object v3

    .line 530
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v3    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 531
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 532
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 523
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 524
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 527
    if-eqz v0, :cond_1

    .line 529
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 530
    :catch_2
    move-exception v2

    .line 531
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 527
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v0, :cond_3

    .line 529
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 532
    :cond_3
    :goto_3
    throw v4

    .line 530
    :catch_3
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 527
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 523
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method


# virtual methods
.method public CheckingFolderState()V
    .locals 11

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x2

    const/high16 v8, -0x10000

    const/high16 v7, -0x1000000

    const/4 v6, 0x1

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->hallIc_by_sysfs()I

    move-result v0

    .line 213
    .local v0, "nCheckingCurrentFolderState":I
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->hallIc_by_sysfs2()I

    move-result v1

    .line 214
    .local v1, "nCheckingCurrentFolderState2":I
    const-string v2, "HallICTest"

    const-string v3, "CheckingFolderState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nHallICTestState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    packed-switch v2, :pswitch_data_0

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 219
    :pswitch_0
    if-eqz v0, :cond_0

    .line 222
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 223
    const-string v2, "HALL_IC_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto :goto_0

    .line 228
    :pswitch_1
    if-nez v0, :cond_0

    .line 229
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    .line 230
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 231
    iput v9, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 232
    const-string v2, "HALL_IC_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto :goto_0

    .line 238
    :pswitch_2
    if-ne v0, v6, :cond_0

    .line 239
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    .line 240
    invoke-direct {p0, v9}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 241
    const-string v2, "HALL_IC_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 242
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    .line 243
    :cond_1
    const-string v2, "HALL_IC_TEST_2ND"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    iput v10, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    goto :goto_0

    .line 250
    :pswitch_3
    if-ne v0, v6, :cond_0

    .line 251
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass:Z

    .line 252
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 253
    const/4 v2, 0x4

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 254
    const-string v2, "HALL_IC_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto :goto_0

    .line 261
    :pswitch_4
    if-nez v0, :cond_0

    .line 262
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass:Z

    .line 263
    invoke-direct {p0, v9}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 264
    const-string v2, "HALL_IC_TEST"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    .line 266
    :cond_2
    const-string v2, "HALL_IC_TEST_2ND"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    iput v10, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    goto :goto_0

    .line 273
    :pswitch_5
    if-eq v0, v6, :cond_0

    .line 276
    const/4 v2, 0x6

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 277
    const-string v2, "HALL_IC_TEST_2ND"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto/16 :goto_0

    .line 283
    :pswitch_6
    if-ne v1, v6, :cond_0

    .line 284
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsWorkingPass2:Z

    .line 285
    const/4 v2, 0x6

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 286
    const/4 v2, 0x7

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 287
    const-string v2, "HALL_IC_TEST_2ND"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto/16 :goto_0

    .line 294
    :pswitch_7
    if-nez v1, :cond_0

    .line 295
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsReleasePass2:Z

    .line 296
    const/4 v2, 0x7

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/HallICTest;->DisplayResult(I)V

    .line 297
    const-string v2, "HALL_IC_TEST_2ND"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mHallICPoint2:Lcom/sec/android/app/hwmoduletest/HallICPoint;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/hwmoduletest/HallICPoint;->setColor(I)V

    goto/16 :goto_0

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 504
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 505
    const-string v0, "HallICTest"

    const-string v1, "onConfigurationChanged"

    const-string v2, "onConfigurationChanged"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->CheckingFolderState()V

    .line 507
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    const-string v0, "HallICTest"

    const-string v1, "onCreate"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->setContentView(I)V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->HallICTestInit()V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->CheckingFolderState()V

    .line 122
    const-string v0, "SUPPORT_BOOK_COVER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->startTimer()V

    .line 127
    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 140
    const-string v0, "SUPPORT_BOOK_COVER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->stopTimer()V

    .line 144
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 148
    return-void
.end method

.method public onExit()V
    .locals 4

    .prologue
    .line 197
    const-string v0, "HallICTest"

    const-string v1, "onExit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsHallICTestAllPass: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsHallICTestAllPass:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsHallICTestAllPass:Z

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->setResult(I)V

    .line 203
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/hwmoduletest/HallICTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/HallICTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/HallICTest;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 209
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 152
    const-string v0, "HallICTest"

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keycode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 155
    const/4 v0, 0x1

    .line 166
    :goto_0
    return v0

    .line 158
    :cond_0
    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    if-eqz p1, :cond_1

    const/16 v0, 0xea

    if-eq p1, v0, :cond_1

    const/16 v0, 0xeb

    if-ne p1, v0, :cond_2

    .line 161
    :cond_1
    const-string v0, "HallICTest"

    const-string v1, "SUPPORT_DUAL_LCD_FOLDER"

    const-string v2, "Temporary checking flip status"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->CheckingFolderState()V

    .line 166
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 137
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 612
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 613
    const-string v0, "HallICTest"

    const-string v1, "onRestoreInstanceState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "now State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    const-string v0, "nHallICTestState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    .line 615
    const-string v0, "HallICTest"

    const-string v1, "onRestoreInstanceState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInt State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 132
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 133
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 605
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 606
    const-string v0, "HallICTest"

    const-string v1, "onSaveInstanceState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "putInt State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    const-string v0, "nHallICTestState"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->nHallICTestState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 608
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    .line 187
    const-string v0, "HallICTest"

    const-string v1, "startTimer"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/app/hwmoduletest/HallICTest;->MILLIS_IN_SEC:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 189
    return-void
.end method

.method protected stopTimer()V
    .locals 3

    .prologue
    .line 192
    const-string v0, "HallICTest"

    const-string v1, "stopTimer"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 194
    return-void
.end method
