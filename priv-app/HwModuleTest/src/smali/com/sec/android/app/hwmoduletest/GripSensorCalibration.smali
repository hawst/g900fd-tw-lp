.class public Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;
.super Landroid/app/Activity;
.source "GripSensorCalibration.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "GripSensorCalibration"

.field private static final MSG_UPDATE_UI:B


# instance fields
.field private final CALDATA_SYSFS_PATH:Ljava/lang/String;

.field private final CALIBRATION:B

.field private final CALIBRATION_ERASE:B

.field private final CAL_SYSFS_PATH:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH:Ljava/lang/String;

.field private final GRIP_OFF:B

.field private final GRIP_ON:B

.field private final ONOFF_SYSFS_PATH:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

.field private isTimerFinish:Z

.field private isgrip_On:Z

.field private ispass:Z

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mButton_Skip:Landroid/widget/Button;

.field private mButton_onoff:Landroid/widget/Button;

.field private mCaldata_data:Ljava/lang/String;

.field private mCspercent_data:Ljava/lang/String;

.field private mProxpercent_data:Ljava/lang/String;

.field private mText_Caldata:Landroid/widget/TextView;

.field private mText_RAW_Count:Landroid/widget/TextView;

.field private mText_RAW_Count_crcount:Landroid/widget/TextView;

.field private mText_Result:Landroid/widget/TextView;

.field private mText_SPEC:Landroid/widget/TextView;

.field private mTimerHandler:Landroid/os/Handler;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x31

    const/16 v0, 0x30

    const/4 v1, 0x1

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->CALIBRATION:B

    .line 44
    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->CALIBRATION_ERASE:B

    .line 46
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->GRIP_ON:B

    .line 47
    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->GRIP_OFF:B

    .line 49
    const-string v0, "/sys/class/sensors/grip_sensor/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->CSPERSENT_SYSFS_PATH:Ljava/lang/String;

    .line 50
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

    .line 51
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->CALDATA_SYSFS_PATH:Ljava/lang/String;

    .line 52
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->CAL_SYSFS_PATH:Ljava/lang/String;

    .line 53
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ONOFF_SYSFS_PATH:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isTimerFinish:Z

    .line 296
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    return-void
.end method

.method private UpdateCSPercent()V
    .locals 8

    .prologue
    const/4 v6, 0x7

    .line 196
    const/4 v3, 0x0

    .line 197
    .local v3, "temp":Ljava/lang/String;
    const/4 v0, 0x0

    .line 198
    .local v0, "RAWCount":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 199
    .local v2, "StrCS_Percent":Ljava/lang/String;
    const/4 v1, 0x0

    .line 201
    .local v1, "StrCR_Count":Ljava/lang/String;
    const-string v4, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->readsysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 206
    :cond_0
    if-eqz v0, :cond_1

    array-length v4, v0

    if-le v4, v6, :cond_1

    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CS Percent("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x3

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CR Percent ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x4

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x5

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x6

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v0, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CS Percent("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "CR Percent ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    const-string v5, "exception"

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "   "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const-string v4, "GripSensorCalibration"

    const-string v5, "UpdateCSPercent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mCspercent_data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_2
    const-string v4, "GripSensorCalibration"

    const-string v5, "UpdateCSPercent"

    const-string v6, "mCspercent_data null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private UpdateGripdata()V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;

    const-string v1, "wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 189
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->readsysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    .line 190
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->readsysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;

    return-object v0
.end method

.method private calibration(B)Z
    .locals 9
    .param p1, "data"    # B

    .prologue
    .line 224
    const-string v4, "GripSensorCalibration"

    const-string v5, "calibration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calibration data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    const/4 v3, 0x0

    .line 226
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 228
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const-string v4, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 229
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 230
    const/4 v3, 0x1

    .line 235
    if-eqz v2, :cond_0

    .line 236
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 242
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibration"

    const-string v5, "calibration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "calibration result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    return v3

    .line 238
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibration"

    const-string v5, "calibration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 241
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 232
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibration"

    const-string v5, "calibration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 235
    if-eqz v1, :cond_1

    .line 236
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 238
    :catch_2
    move-exception v0

    .line 239
    const-string v4, "GripSensorCalibration"

    const-string v5, "calibration"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 234
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 235
    :goto_2
    if-eqz v1, :cond_2

    .line 236
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 240
    :cond_2
    :goto_3
    throw v4

    .line 238
    :catch_3
    move-exception v0

    .line 239
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibration"

    const-string v6, "calibration"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 234
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 231
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private gripOnOff(B)Z
    .locals 9
    .param p1, "data"    # B

    .prologue
    .line 246
    const-string v4, "GripSensorCalibration"

    const-string v5, "gripOnOff"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "gripOnOff data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v3, 0x0

    .line 248
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 250
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const-string v4, "/sys/class/sensors/grip_sensor/onoff"

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 252
    const/4 v3, 0x1

    .line 257
    if-eqz v2, :cond_0

    .line 258
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 264
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibration"

    const-string v5, "gripOnOff"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "gripOnOff result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    return v3

    .line 260
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibration"

    const-string v5, "gripOnOff"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 263
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 253
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 254
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibration"

    const-string v5, "gripOnOff"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 257
    if-eqz v1, :cond_1

    .line 258
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 260
    :catch_2
    move-exception v0

    .line 261
    const-string v4, "GripSensorCalibration"

    const-string v5, "gripOnOff"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 256
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 257
    :goto_2
    if-eqz v1, :cond_2

    .line 258
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 262
    :cond_2
    :goto_3
    throw v4

    .line 260
    :catch_3
    move-exception v0

    .line 261
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibration"

    const-string v6, "gripOnOff"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 256
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 253
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private readsysfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 269
    const/4 v2, 0x0

    .line 270
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v1, ""

    .line 272
    .local v1, "readData":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 274
    if-eqz v1, :cond_0

    .line 275
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 277
    :cond_0
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sysfs Data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 284
    if-eqz v3, :cond_1

    .line 285
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    move-object v2, v3

    .line 293
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    return-object v1

    .line 287
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 288
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v1, "exception"

    .line 290
    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z

    move-object v2, v3

    .line 292
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 278
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 279
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    const-string v1, "exception"

    .line 281
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 284
    if-eqz v2, :cond_2

    .line 285
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 287
    :catch_2
    move-exception v0

    .line 288
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v1, "exception"

    .line 290
    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z

    goto :goto_0

    .line 283
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 284
    :goto_2
    if-eqz v2, :cond_3

    .line 285
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 291
    :cond_3
    :goto_3
    throw v4

    .line 287
    :catch_3
    move-exception v0

    .line 288
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibration"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const-string v1, "exception"

    .line 290
    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z

    goto :goto_3

    .line 283
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 278
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x31

    const/16 v6, 0x30

    const/4 v5, 0x1

    .line 134
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0b00bf

    if-ne v2, v3, :cond_2

    .line 135
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_cal_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->calibration(B)Z

    move-result v1

    .line 137
    .local v1, "result":Z
    if-nez v1, :cond_0

    .line 138
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_cal_fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->UpdateGripdata()V

    .line 181
    .end local v1    # "result":Z
    :cond_1
    :goto_0
    return-void

    .line 142
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0b00c0

    if-ne v2, v3, :cond_4

    .line 143
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_calerase_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->calibration(B)Z

    move-result v1

    .line 145
    .restart local v1    # "result":Z
    if-nez v1, :cond_3

    .line 146
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_cal_erase_fail"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->UpdateGripdata()V

    goto :goto_0

    .line 150
    .end local v1    # "result":Z
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0b00c2

    if-ne v2, v3, :cond_6

    .line 151
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_skip_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v2, "GRIPSENSOR_TYPE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "AP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 154
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    .local v0, "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->startActivity(Landroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->finish()V

    goto :goto_0

    .line 157
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 162
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0b00c1

    if-ne v2, v3, :cond_1

    .line 163
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "gripsensor_onoff_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    if-ne v2, v5, :cond_7

    .line 165
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->gripOnOff(B)Z

    move-result v1

    .line 166
    .restart local v1    # "result":Z
    if-ne v1, v5, :cond_1

    .line 167
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    const-string v3, "On"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "grip off"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 172
    .end local v1    # "result":Z
    :cond_7
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    if-nez v2, :cond_1

    .line 173
    invoke-direct {p0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->gripOnOff(B)Z

    move-result v1

    .line 174
    .restart local v1    # "result":Z
    if-ne v1, v5, :cond_1

    .line 175
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    const-string v3, "Off"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const-string v2, "GripSensorCalibration"

    const-string v3, "onClick"

    const-string v4, "grip on"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const-string v0, "GripSensorCalibration"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->setContentView(I)V

    .line 71
    const v0, 0x7f0b00b8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0b00ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0b00bc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0b00be

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0b00bf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f0b00c0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v0, 0x7f0b00c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_Skip:Landroid/widget/Button;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_Skip:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v0, 0x7f0b00c1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 128
    const-string v0, "GripSensorCalibration"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 130
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 119
    const-string v0, "GripSensorCalibration"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isTimerFinish:Z

    .line 124
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 91
    const-string v1, "GripSensorCalibration"

    const-string v2, "onResume"

    const-string v3, "onResume"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 93
    const/16 v1, 0x31

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->gripOnOff(B)Z

    move-result v0

    .line 94
    .local v0, "result":Z
    if-ne v0, v5, :cond_0

    .line 95
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    const-string v2, "Off"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 97
    const-string v1, "GripSensorCalibration"

    const-string v2, "onResume"

    const-string v3, "grip on success"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->UpdateGripdata()V

    .line 105
    new-instance v1, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$1;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    .line 113
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isTimerFinish:Z

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 115
    return-void

    .line 100
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->isgrip_On:Z

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mButton_onoff:Landroid/widget/Button;

    const-string v2, "On"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 102
    const-string v1, "GripSensorCalibration"

    const-string v2, "onResume"

    const-string v3, "grip on fail"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
