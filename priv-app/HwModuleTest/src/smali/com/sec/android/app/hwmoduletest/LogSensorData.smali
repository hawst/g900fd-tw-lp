.class public Lcom/sec/android/app/hwmoduletest/LogSensorData;
.super Ljava/lang/Object;
.source "LogSensorData.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LogSensorData"


# instance fields
.field mFileName:Ljava/lang/String;

.field mFilePreFix:Ljava/lang/String;

.field private mFos:Ljava/io/FileOutputStream;

.field private mIsStarting:Z

.field mSystemTime:[J

.field private mWriteLine:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFileName:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFilePreFix:Ljava/lang/String;

    .line 24
    const-string v0, "AirMotion"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFilePreFix:Ljava/lang/String;

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->createLogFile()V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    .line 18
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 20
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFileName:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFilePreFix:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFilePreFix:Ljava/lang/String;

    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->createLogFile()V

    .line 31
    return-void
.end method

.method private createLogFile()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 34
    iput-boolean v10, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 35
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 36
    .local v0, "c":Ljava/util/Calendar;
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 37
    .local v3, "form":Ljava/util/Formatter;
    const-string v6, "-%04d%02d%02d-%02d%02d%02d"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v0, v12}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    const/4 v8, 0x3

    const/16 v9, 0xb

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    const/16 v9, 0xc

    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0xd

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v12

    invoke-virtual {v3, v6, v7}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 40
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 41
    .local v5, "sbfileName":Ljava/lang/StringBuffer;
    const-string v6, "/storage/sdcard0/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->getFilePrefix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, ".txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 43
    const-string v6, "LogSensorData"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sbfileName "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v4, Ljava/io/File;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 46
    .local v4, "fp":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 48
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :cond_0
    :goto_0
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    const/4 v7, 0x1

    invoke-direct {v6, v4, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 62
    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFileName:Ljava/lang/String;

    .line 63
    return-void

    .line 49
    :catch_0
    move-exception v2

    .line 51
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 57
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 59
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private getFilePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFilePreFix:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method CalculateSpace(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    rsub-int/lit8 v2, v3, 0x14

    .line 256
    .local v2, "spaceCount":I
    move-object v1, p1

    .line 258
    .local v1, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 259
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_0
    return-object v1
.end method

.method getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 349
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 351
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 359
    return-void

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 355
    const-string v1, "LogSensorData"

    const-string v2, "file close error"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method writeBodyTempSensorData(I[Ljava/lang/String;)V
    .locals 6
    .param p1, "count"    # I
    .param p2, "value"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 70
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 72
    .local v1, "strData":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v3, :cond_1

    .line 73
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Count"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Body temp."

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "To"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Ta"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Raw To"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Raw Ta\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 74
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 79
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v4, p2, v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, p2, v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, p2, v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p2, v5

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, p2, v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 80
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 84
    .local v2, "theByteArray":[B
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v3, :cond_0

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v3, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_0
    :goto_1
    return-void

    .line 76
    .end local v2    # "theByteArray":[B
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 87
    .restart local v2    # "theByteArray":[B
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method writeSensorData(F)V
    .locals 14
    .param p1, "value1"    # F

    .prologue
    .line 267
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 268
    .local v6, "strData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 270
    .local v4, "intervalTime":J
    iget-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v9, :cond_1

    .line 271
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Interval"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Zmean\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 272
    const-string v9, "gesture sensor test mode 1"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mIsStarting"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "strData"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 278
    :goto_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 279
    .local v8, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 280
    .local v0, "curTime":J
    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 281
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 282
    .local v3, "form":Ljava/util/Formatter;
    const-string v9, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v8, Landroid/text/format/Time;->second:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 283
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aput-wide v0, v9, v10

    .line 285
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 286
    const-wide/16 v4, 0x0

    .line 291
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    aput-wide v12, v9, v10

    .line 292
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 293
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 294
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 297
    .local v7, "theByteArray":[B
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_0

    .line 298
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v9, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :cond_0
    :goto_2
    return-void

    .line 275
    .end local v0    # "curTime":J
    .end local v3    # "form":Ljava/util/Formatter;
    .end local v7    # "theByteArray":[B
    .end local v8    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v6, ""

    goto/16 :goto_0

    .line 288
    .restart local v0    # "curTime":J
    .restart local v3    # "form":Ljava/util/Formatter;
    .restart local v8    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    sub-long v4, v10, v12

    goto :goto_1

    .line 300
    .restart local v7    # "theByteArray":[B
    :catch_0
    move-exception v2

    .line 301
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method writeSensorData(FFFF)V
    .locals 14
    .param p1, "value1"    # F
    .param p2, "value2"    # F
    .param p3, "value3"    # F
    .param p4, "value4"    # F

    .prologue
    .line 308
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 309
    .local v6, "strData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 311
    .local v4, "intervalTime":J
    iget-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v9, :cond_1

    .line 312
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Interval"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Ch A"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Ch B"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Ch C"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Ch D\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 313
    const-string v9, "gesture sensor count delta"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mIsStarting"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "strData"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 319
    :goto_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 320
    .local v8, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 321
    .local v0, "curTime":J
    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 322
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 323
    .local v3, "form":Ljava/util/Formatter;
    const-string v9, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v8, Landroid/text/format/Time;->second:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 324
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aput-wide v0, v9, v10

    .line 326
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 327
    const-wide/16 v4, 0x0

    .line 332
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    aput-wide v12, v9, v10

    .line 333
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 335
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 336
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 339
    .local v7, "theByteArray":[B
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_0

    .line 340
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v9, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :cond_0
    :goto_2
    return-void

    .line 316
    .end local v0    # "curTime":J
    .end local v3    # "form":Ljava/util/Formatter;
    .end local v7    # "theByteArray":[B
    .end local v8    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v6, ""

    goto/16 :goto_0

    .line 329
    .restart local v0    # "curTime":J
    .restart local v3    # "form":Ljava/util/Formatter;
    .restart local v8    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    sub-long v4, v10, v12

    goto/16 :goto_1

    .line 342
    .restart local v7    # "theByteArray":[B
    :catch_0
    move-exception v2

    .line 343
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method writeSensorData(FFFI)V
    .locals 14
    .param p1, "value1"    # F
    .param p2, "value2"    # F
    .param p3, "value3"    # F
    .param p4, "value4"    # I

    .prologue
    .line 171
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 172
    .local v6, "strData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 174
    .local v4, "intervalTime":J
    iget-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v9, :cond_1

    .line 175
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Interval"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Zdelta"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Peak to Peak"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Valid cnt"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Angle\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 177
    const-string v9, "gesture sensor test mode 2"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mIsStarting"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "strData"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 183
    :goto_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 184
    .local v8, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 185
    .local v0, "curTime":J
    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 186
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 187
    .local v3, "form":Ljava/util/Formatter;
    const-string v9, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v8, Landroid/text/format/Time;->second:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 188
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aput-wide v0, v9, v10

    .line 190
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 191
    const-wide/16 v4, 0x0

    .line 196
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    aput-wide v12, v9, v10

    .line 197
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 199
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 200
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 203
    .local v7, "theByteArray":[B
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_0

    .line 204
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v9, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_0
    :goto_2
    return-void

    .line 180
    .end local v0    # "curTime":J
    .end local v3    # "form":Ljava/util/Formatter;
    .end local v7    # "theByteArray":[B
    .end local v8    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v6, ""

    goto/16 :goto_0

    .line 193
    .restart local v0    # "curTime":J
    .restart local v3    # "form":Ljava/util/Formatter;
    .restart local v8    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    sub-long v4, v10, v12

    goto/16 :goto_1

    .line 206
    .restart local v7    # "theByteArray":[B
    :catch_0
    move-exception v2

    .line 207
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method writeSensorData(Ljava/lang/String;I)V
    .locals 14
    .param p1, "value1"    # Ljava/lang/String;
    .param p2, "value2"    # I

    .prologue
    .line 132
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 133
    .local v6, "strData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 135
    .local v4, "intervalTime":J
    iget-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v9, :cond_1

    .line 136
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Interval"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Direction"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Angle\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 137
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 142
    :goto_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 143
    .local v8, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 144
    .local v0, "curTime":J
    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 145
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 146
    .local v3, "form":Ljava/util/Formatter;
    const-string v9, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v8, Landroid/text/format/Time;->second:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 147
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aput-wide v0, v9, v10

    .line 149
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 150
    const-wide/16 v4, 0x0

    .line 155
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    aput-wide v12, v9, v10

    .line 156
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 157
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 158
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 161
    .local v7, "theByteArray":[B
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_0

    .line 162
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v9, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :cond_0
    :goto_2
    return-void

    .line 139
    .end local v0    # "curTime":J
    .end local v3    # "form":Ljava/util/Formatter;
    .end local v7    # "theByteArray":[B
    .end local v8    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v6, ""

    goto/16 :goto_0

    .line 152
    .restart local v0    # "curTime":J
    .restart local v3    # "form":Ljava/util/Formatter;
    .restart local v8    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    sub-long v4, v10, v12

    goto :goto_1

    .line 164
    .restart local v7    # "theByteArray":[B
    :catch_0
    move-exception v2

    .line 165
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method writeSensorData(Ljava/lang/String;II)V
    .locals 14
    .param p1, "value1"    # Ljava/lang/String;
    .param p2, "value2"    # I
    .param p3, "value3"    # I

    .prologue
    .line 93
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    .line 94
    .local v6, "strData":Ljava/lang/String;
    const-wide/16 v4, 0x0

    .line 96
    .local v4, "intervalTime":J
    iget-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v9, :cond_1

    .line 97
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Time"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Interval"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Direction"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Scale"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Angle\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 98
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 103
    :goto_0
    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    .line 104
    .local v8, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 105
    .local v0, "curTime":J
    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 106
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 107
    .local v3, "form":Ljava/util/Formatter;
    const-string v9, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Landroid/text/format/Time;->hour:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Landroid/text/format/Time;->minute:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    iget v12, v8, Landroid/text/format/Time;->second:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v3, v9, v10}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 108
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aput-wide v0, v9, v10

    .line 110
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 111
    const-wide/16 v4, 0x0

    .line 116
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    aput-wide v12, v9, v10

    .line 117
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 119
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 120
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v7

    .line 123
    .local v7, "theByteArray":[B
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v9, :cond_0

    .line 124
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v9, v7}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_0
    :goto_2
    return-void

    .line 100
    .end local v0    # "curTime":J
    .end local v3    # "form":Ljava/util/Formatter;
    .end local v7    # "theByteArray":[B
    .end local v8    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v6, ""

    goto/16 :goto_0

    .line 113
    .restart local v0    # "curTime":J
    .restart local v3    # "form":Ljava/util/Formatter;
    .restart local v8    # "time":Landroid/text/format/Time;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    sub-long v4, v10, v12

    goto/16 :goto_1

    .line 126
    .restart local v7    # "theByteArray":[B
    :catch_0
    move-exception v2

    .line 127
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method writeSensorData(Ljava/lang/String;IIIIIIILjava/lang/String;)V
    .locals 16
    .param p1, "value1"    # Ljava/lang/String;
    .param p2, "value2"    # I
    .param p3, "value3"    # I
    .param p4, "value4"    # I
    .param p5, "value5"    # I
    .param p6, "value6"    # I
    .param p7, "value7"    # I
    .param p8, "value8"    # I
    .param p9, "gesture_count"    # Ljava/lang/String;

    .prologue
    .line 213
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8}, Ljava/lang/String;-><init>()V

    .line 214
    .local v8, "strData":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 216
    .local v6, "intervalTime":J
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    if-nez v11, :cond_1

    .line 217
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Time"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Interval"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Direction"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Cross_Angle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Count"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Max.sum_NSWE"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Enter_Angle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Exit_Angle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Enter_Mag"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Exit_Mag"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "Gesture Count\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 219
    const-string v11, "gesture sensor test mode 2"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "mIsStarting"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "strData"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput-boolean v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mIsStarting:Z

    .line 225
    :goto_0
    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    .line 226
    .local v10, "time":Landroid/text/format/Time;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 227
    .local v2, "curTime":J
    invoke-virtual {v10, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 228
    new-instance v5, Ljava/util/Formatter;

    invoke-direct {v5}, Ljava/util/Formatter;-><init>()V

    .line 229
    .local v5, "form":Ljava/util/Formatter;
    const-string v11, "%02d:%02d:%02d"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget v14, v10, Landroid/text/format/Time;->hour:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    iget v14, v10, Landroid/text/format/Time;->minute:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    iget v14, v10, Landroid/text/format/Time;->second:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v5, v11, v12}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 230
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aput-wide v2, v11, v12

    .line 232
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    aget-wide v12, v11, v12

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-nez v11, :cond_2

    .line 233
    const-wide/16 v6, 0x0

    .line 238
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v14, 0x1

    aget-wide v14, v13, v14

    aput-wide v14, v11, v12

    .line 239
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p8 .. p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->CalculateSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p9

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 242
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    .line 243
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mWriteLine:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    .line 246
    .local v9, "theByteArray":[B
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    if-eqz v11, :cond_0

    .line 247
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v11, v9}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_0
    :goto_2
    return-void

    .line 222
    .end local v2    # "curTime":J
    .end local v5    # "form":Ljava/util/Formatter;
    .end local v9    # "theByteArray":[B
    .end local v10    # "time":Landroid/text/format/Time;
    :cond_1
    const-string v8, ""

    goto/16 :goto_0

    .line 235
    .restart local v2    # "curTime":J
    .restart local v5    # "form":Ljava/util/Formatter;
    .restart local v10    # "time":Landroid/text/format/Time;
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v12, 0x1

    aget-wide v12, v11, v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;->mSystemTime:[J

    const/4 v14, 0x0

    aget-wide v14, v11, v14

    sub-long v6, v12, v14

    goto/16 :goto_1

    .line 249
    .restart local v9    # "theByteArray":[B
    :catch_0
    move-exception v4

    .line 250
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
