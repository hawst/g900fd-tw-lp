.class public Lcom/sec/android/app/hwmoduletest/UVTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "UVTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# static fields
.field private static final ADI_TESTCASE:I = 0x2

.field private static final INTERVAL:I = 0x1f4

.field private static final MAXIM_TESTCASE:I = 0x1

.field private static final MSG_UPDATE_UI:I = 0x0

.field private static final STM_TESTCASE:I = 0x1


# instance fields
.field private ColumnNum:I

.field private SeparatedTest_Number:I

.field private Table_ColumnItem:[Ljava/lang/String;

.field private Table_RowItem:[Ljava/lang/String;

.field private calc:D

.field private mFormat:Ljava/text/DecimalFormat;

.field private mHandler:Landroid/os/Handler;

.field private mHoldButton:Landroid/widget/ToggleButton;

.field private mIsHold:Z

.field private mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

.field private mTimer:Ljava/util/Timer;

.field private mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

.field private mValue:[F

.field private mVendor:Ljava/lang/String;

.field private txt_ItemName:[Landroid/widget/TextView;

.field private txt_ItemValue:[Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    const-string v0, "UVTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 46
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    .line 47
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    .line 48
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mIsHold:Z

    .line 52
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    .line 58
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mTimer:Ljava/util/Timer;

    .line 296
    new-instance v0, Lcom/sec/android/app/hwmoduletest/UVTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/UVTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/UVTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHandler:Landroid/os/Handler;

    .line 68
    return-void
.end method

.method private Calculate_UVcal(F)D
    .locals 2
    .param p1, "index"    # F

    .prologue
    .line 352
    const/high16 v0, 0x41c80000    # 25.0f

    mul-float/2addr v0, p1

    float-to-double v0, v0

    return-wide v0
.end method

.method private SetUI()V
    .locals 22

    .prologue
    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "SetUI"

    const-string v20, ""

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->setTestItems()V

    .line 147
    new-instance v11, Landroid/widget/TableLayout$LayoutParams;

    const/16 v18, -0x1

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 148
    .local v11, "layoutParams_upper":Landroid/widget/TableLayout$LayoutParams;
    new-instance v9, Landroid/widget/TableLayout$LayoutParams;

    const/16 v18, -0x1

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v9, v0, v1}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 149
    .local v9, "layoutParams":Landroid/widget/TableLayout$LayoutParams;
    new-instance v10, Landroid/widget/TableRow$LayoutParams;

    const/16 v18, -0x1

    const/16 v19, -0x2

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    .line 151
    .local v10, "layoutParams_txt":Landroid/widget/TableRow$LayoutParams;
    const v18, 0x7f0b02df

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/UVTest;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TableLayout;

    .line 152
    .local v14, "tableLayout":Landroid/widget/TableLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v5, v18, -0x1

    .line 153
    .local v5, "TableContent_Num":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    move/from16 v19, v0

    sub-int v4, v18, v19

    .line 155
    .local v4, "CommonTest_Num":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v0, v0, [Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    .line 156
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    move/from16 v18, v0

    mul-int v18, v18, v5

    add-int v18, v18, v4

    move/from16 v0, v18

    new-array v0, v0, [Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    .line 160
    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x1

    const/16 v21, 0x0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 161
    const/16 v18, 0x2

    const/16 v19, 0x2

    const/16 v20, 0x2

    const/16 v21, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 162
    new-instance v16, Landroid/widget/TableRow;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 163
    .local v16, "tableRow_classification":Landroid/widget/TableRow;
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v6, v0, [Landroid/widget/TextView;

    .line 167
    .local v6, "classification":[Landroid/widget/TextView;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_0

    .line 168
    new-instance v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v18, v6, v7

    .line 169
    aget-object v18, v6, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v7

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    aget-object v18, v6, v7

    const/16 v19, 0x1

    const/high16 v20, 0x41a00000    # 20.0f

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 171
    aget-object v18, v6, v7

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setGravity(I)V

    .line 172
    aget-object v18, v6, v7

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 173
    aget-object v18, v6, v7

    const/high16 v19, -0x1000000

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setTextColor(I)V

    .line 174
    aget-object v18, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    aget-object v18, v6, v7

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 167
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 177
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 180
    const/16 v17, 0x0

    .line 181
    .local v17, "valueIndex":I
    const/4 v12, 0x0

    .line 183
    .local v12, "nameIndex":I
    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_2

    .line 184
    const/16 v18, 0x2

    const/16 v19, 0x2

    const/16 v20, 0x2

    const/16 v21, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 185
    new-instance v15, Landroid/widget/TableRow;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 186
    .local v15, "tableRow":Landroid/widget/TableRow;
    invoke-virtual {v15, v9}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v19, v18, v12

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v12

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, 0x1

    const/high16 v20, 0x41a00000    # 20.0f

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setGravity(I)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/high16 v19, -0x1000000

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setTextColor(I)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 196
    add-int/lit8 v12, v12, 0x1

    .line 198
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_2
    if-ge v8, v5, :cond_1

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v19, v18, v17

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, 0x1

    const/high16 v20, 0x41a00000    # 20.0f

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setGravity(I)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/high16 v19, -0x1000000

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setTextColor(I)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 206
    add-int/lit8 v17, v17, 0x1

    .line 198
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 209
    :cond_1
    invoke-virtual {v14, v15}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 183
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 212
    .end local v8    # "j":I
    .end local v15    # "tableRow":Landroid/widget/TableRow;
    :cond_2
    const/4 v7, 0x0

    :goto_3
    if-ge v7, v4, :cond_4

    .line 213
    const/16 v18, 0x2

    const/16 v19, 0x2

    const/16 v20, 0x2

    const/16 v21, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 214
    new-instance v15, Landroid/widget/TableRow;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 215
    .restart local v15    # "tableRow":Landroid/widget/TableRow;
    invoke-virtual {v15, v9}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v19, v18, v12

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    move-object/from16 v19, v0

    aget-object v19, v19, v12

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, 0x1

    const/high16 v20, 0x41a00000    # 20.0f

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setGravity(I)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    const/high16 v19, -0x1000000

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setTextColor(I)V

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemName:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v12

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 225
    add-int/lit8 v12, v12, 0x1

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Landroid/widget/TextView;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v19, v18, v17

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, 0x1

    const/high16 v20, 0x41a00000    # 20.0f

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setGravity(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/16 v19, -0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    const/high16 v19, -0x1000000

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setTextColor(I)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    const-string v18, "ADI"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 234
    new-instance v13, Landroid/widget/TableRow$LayoutParams;

    invoke-direct {v13}, Landroid/widget/TableRow$LayoutParams;-><init>()V

    .line 235
    .local v13, "params":Landroid/widget/TableRow$LayoutParams;
    const/16 v18, 0x2

    move/from16 v0, v18

    iput v0, v13, Landroid/widget/TableRow$LayoutParams;->span:I

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v13}, Landroid/widget/TableRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 240
    .end local v13    # "params":Landroid/widget/TableRow$LayoutParams;
    :goto_4
    add-int/lit8 v17, v17, 0x1

    .line 242
    invoke-virtual {v14, v15}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 212
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 238
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    move-object/from16 v18, v0

    aget-object v18, v18, v17

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    goto :goto_4

    .line 244
    .end local v15    # "tableRow":Landroid/widget/TableRow;
    :cond_4
    return-void
.end method

.method private UpdateGraph()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 286
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->addValue1(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->addValue2(I)V

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->addValue1(I)V

    goto :goto_0

    .line 291
    :cond_2
    const-string v0, "STMicroelectronics"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->addValue1(I)V

    goto :goto_0
.end method

.method private UpdateTable()V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 263
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v4

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v5

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v2, v2, v6

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "UpdateTable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV A:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV B:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v5

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v4, v4, v6

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV Index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v1, v1, v4

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v2, v2, v6

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "UpdateTable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV ADC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v4, v4, v6

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV Index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_2
    const-string v0, "STMicroelectronics"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v6

    const-string v1, "%.1f"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v5

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->txt_ItemValue:[Landroid/widget/TextView;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v2, v2, v6

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "UpdateTable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV ADC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v4, v4, v6

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/UVTest;->Calculate_UVcal(F)D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV Index:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/UVTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVTest;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/UVTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVTest;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mIsHold:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/UVTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVTest;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->UpdateTable()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/UVTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/UVTest;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->UpdateGraph()V

    return-void
.end method

.method private setTestItems()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    const-string v1, "UV A"

    aput-object v1, v0, v3

    const-string v1, "UV B"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    .line 126
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "UV ADC"

    aput-object v1, v0, v2

    const-string v1, "UV calc"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    .line 127
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    .line 141
    :goto_0
    return-void

    .line 128
    :cond_0
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    .line 130
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "UV ADC"

    aput-object v1, v0, v2

    const-string v1, "UV calc"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    .line 131
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    goto :goto_0

    .line 132
    :cond_1
    const-string v0, "STMicroelectronics"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    .line 134
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "UV ADC"

    aput-object v1, v0, v2

    const-string v1, "UV calc"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    .line 135
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    goto :goto_0

    .line 137
    :cond_2
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_ColumnItem:[Ljava/lang/String;

    .line 138
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->Table_RowItem:[Ljava/lang/String;

    .line 139
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->SeparatedTest_Number:I

    goto :goto_0
.end method


# virtual methods
.method public Initialize()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "initialize"

    const-string v2, "Initialize values"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const v0, 0x7f0b02e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mUVGraph:Lcom/sec/android/app/hwmoduletest/view/UVGraph;

    .line 109
    const v0, 0x7f0b02de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHoldButton:Landroid/widget/ToggleButton;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHoldButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    .line 121
    :goto_0
    return-void

    .line 114
    :cond_0
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    goto :goto_0

    .line 116
    :cond_1
    const-string v0, "STMicroelectronics"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    goto :goto_0

    .line 119
    :cond_2
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->ColumnNum:I

    goto :goto_0
.end method

.method public ReadValue(Ljava/lang/String;)V
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 337
    if-eqz p1, :cond_1

    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "arrayValue":[Ljava/lang/String;
    :goto_0
    if-nez v0, :cond_2

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "ReadValue"

    const-string v4, "arrayValue is null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_0
    return-void

    .line 337
    .end local v0    # "arrayValue":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    .restart local v0    # "arrayValue":[Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 345
    aget-object v2, v0, v1

    if-eqz v2, :cond_3

    .line 346
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mValue:[F

    aget-object v3, v0, v1

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    aput v3, v2, v1

    .line 344
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 314
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 260
    :goto_0
    return-void

    .line 251
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHoldButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mIsHold:Z

    goto :goto_0

    .line 254
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mIsHold:Z

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x7f0b02de
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v1, 0x7f03008b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/UVTest;->setContentView(I)V

    .line 74
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 75
    .local v0, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 76
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.#"

    invoke-direct {v1, v2, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mFormat:Ljava/text/DecimalFormat;

    .line 78
    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getVendorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mVendor:Ljava/lang/String;

    .line 80
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOff()V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 102
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 103
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->SensorOn()V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->Initialize()V

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVTest;->SetUI()V

    .line 89
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mTimer:Ljava/util/Timer;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVTest;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/UVTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/UVTest;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 96
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 319
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 0
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 323
    packed-switch p1, :pswitch_data_0

    .line 330
    :goto_0
    return-void

    .line 325
    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/sec/android/app/hwmoduletest/UVTest;->ReadValue(Ljava/lang/String;)V

    goto :goto_0

    .line 323
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method
