.class Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;
.super Ljava/lang/Object;
.source "GestureTestMode2Display.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 17
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 198
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    .line 199
    const-string v11, "N"

    .line 200
    .local v11, "direction":Ljava/lang/String;
    const-string v12, "R L U D"

    .line 202
    .local v12, "gesture_count":Ljava/lang/String;
    const-string v1, "MAXIM"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    const-string v1, "GestureTestMode2Display"

    const-string v3, "listenner: getSensorData()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "count No = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", zdelta = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x4

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", peaktopeak = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", valid_cnt = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", angle = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/HistoryItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v9, 0x2

    aget v6, v6, v9

    float-to-int v6, v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/hwmoduletest/HistoryItem;-><init>(IFFFI)V

    invoke-interface {v7, v8, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object v2, v11

    .line 226
    .end local v11    # "direction":Ljava/lang/String;
    .local v2, "direction":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$800(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->notifyDataSetChanged()V

    .line 228
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$900()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 229
    const-string v1, "MAXIM"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 230
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$1000()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v7, 0x2

    aget v6, v6, v7

    float-to-int v6, v6

    invoke-virtual {v1, v3, v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(FFFI)V

    move-object v10, v12

    .line 236
    .end local v12    # "gesture_count":Ljava/lang/String;
    .local v10, "gesture_count":Ljava/lang/String;
    :goto_1
    const-string v1, "GestureTestMode2Display"

    const-string v3, "onGetSensorHubData"

    const-string v4, "write to log file "

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :goto_2
    return-void

    .line 205
    .end local v2    # "direction":Ljava/lang/String;
    .end local v10    # "gesture_count":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    .restart local v12    # "gesture_count":Ljava/lang/String;
    :cond_0
    const-string v1, "AMS"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 206
    const-string v1, "GestureTestMode2Display"

    const-string v3, "listenner: getSensorData()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "count No = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", NSWE_Gesture = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Max_Sum_NSWE = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Cross_Angle = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Enter_Angle = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Exit_Angle = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x4

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Enter_Mag = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Exit_Mag = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    float-to-int v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/HistoryItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-int v5, v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    float-to-int v6, v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    float-to-int v7, v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    float-to-int v8, v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v15, 0x6

    aget v9, v9, v15

    float-to-int v9, v9

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/16 v16, 0x7

    aget v15, v15, v16

    float-to-int v10, v15

    invoke-direct/range {v1 .. v10}, Lcom/sec/android/app/hwmoduletest/HistoryItem;-><init>(IIIIIIIII)V

    invoke-interface {v13, v14, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 209
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v1, v1

    const/4 v3, 0x5

    if-ne v1, v3, :cond_1

    .line 210
    const-string v2, "Up"

    .line 211
    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$308(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    .line 224
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_direction:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$700(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "R:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$600(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "   L:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$500(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "   U:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "   D:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$400(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 212
    .end local v2    # "direction":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v1, v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 213
    const-string v2, "Down"

    .line 214
    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$408(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    goto :goto_3

    .line 215
    .end local v2    # "direction":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v1, v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 216
    const-string v2, "Left"

    .line 217
    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$508(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    goto/16 :goto_3

    .line 218
    .end local v2    # "direction":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v1, v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 219
    const-string v2, "Right"

    .line 220
    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$608(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    goto/16 :goto_3

    .line 222
    .end local v2    # "direction":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    :cond_4
    const-string v2, "N"

    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    goto/16 :goto_3

    .line 231
    :cond_5
    const-string v1, "AMS"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "R"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$600(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "L"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$500(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "U"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "D"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$400(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 234
    .end local v12    # "gesture_count":Ljava/lang/String;
    .restart local v10    # "gesture_count":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->access$1000()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v1

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-int v5, v5

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    float-to-int v6, v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    float-to-int v7, v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    float-to-int v8, v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v13, 0x6

    aget v9, v9, v13

    float-to-int v9, v9

    invoke-virtual/range {v1 .. v10}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(Ljava/lang/String;IIIIIIILjava/lang/String;)V

    goto/16 :goto_1

    .end local v10    # "gesture_count":Ljava/lang/String;
    .restart local v12    # "gesture_count":Ljava/lang/String;
    :cond_6
    move-object v10, v12

    .end local v12    # "gesture_count":Ljava/lang/String;
    .restart local v10    # "gesture_count":Ljava/lang/String;
    goto/16 :goto_1

    .end local v10    # "gesture_count":Ljava/lang/String;
    .restart local v12    # "gesture_count":Ljava/lang/String;
    :cond_7
    move-object v10, v12

    .end local v12    # "gesture_count":Ljava/lang/String;
    .restart local v10    # "gesture_count":Ljava/lang/String;
    goto/16 :goto_2

    .end local v2    # "direction":Ljava/lang/String;
    .end local v10    # "gesture_count":Ljava/lang/String;
    .restart local v11    # "direction":Ljava/lang/String;
    .restart local v12    # "gesture_count":Ljava/lang/String;
    :cond_8
    move-object v2, v11

    .end local v11    # "direction":Ljava/lang/String;
    .restart local v2    # "direction":Ljava/lang/String;
    goto/16 :goto_0
.end method
