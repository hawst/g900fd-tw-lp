.class Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;
.super Ljava/lang/Object;
.source "FactoryTestPhone.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ServiceConnection"

    const-string v2, "onServiceConnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$102(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$202(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Z)Z

    .line 163
    :goto_0
    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$300()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mData:[B
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$400(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mResponse:Landroid/os/Message;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$500(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Landroid/os/Message;

    move-result-object v2

    # invokes: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->sendMessageToRil([BLandroid/os/Message;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$600(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;[BLandroid/os/Message;)V

    goto :goto_0

    .line 166
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected"

    const-string v2, "onServiceDisconnedted()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$202(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Z)Z

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$102(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 171
    return-void
.end method
