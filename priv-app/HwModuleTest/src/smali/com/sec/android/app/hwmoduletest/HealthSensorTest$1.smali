.class Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;
.super Landroid/os/Handler;
.source "HealthSensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HealthSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 91
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 95
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$000(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)B

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$300(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    move-result-object v1

    # invokes: Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->getIRThermometerSensorVersionString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->access$200(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->access$002(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;B)B

    goto :goto_0

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
