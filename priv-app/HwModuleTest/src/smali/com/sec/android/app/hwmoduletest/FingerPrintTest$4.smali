.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/high16 v11, 0x41200000    # 10.0f

    .line 168
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 173
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setVisibility(I)V

    .line 174
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "normalDataArray":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 177
    .local v4, "stdevDataArray":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 178
    .local v5, "stdevSection1Array":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 180
    .local v6, "stdevSection2Array":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v7

    add-int/lit8 v7, v7, -0x32

    int-to-float v7, v7

    array-length v8, v1

    add-int/lit8 v8, v8, -0x1

    add-int/lit8 v8, v8, 0xa

    int-to-float v8, v8

    div-float v2, v7, v8

    .line 181
    .local v2, "scaleX":F
    const-string v7, "FingerPrintTest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Graph X Scale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setGraphXScale(F)V

    .line 184
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x40400000    # 3.0f

    div-float/2addr v7, v8

    const/high16 v8, 0x437f0000    # 255.0f

    div-float v3, v7, v8

    .line 185
    .local v3, "scaleY":F
    const-string v7, "FingerPrintTest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Graph Y Scale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->setGraphYScale(F)V

    .line 188
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v7, v1

    if-ge v0, v7, :cond_0

    .line 189
    aget-object v7, v1, v0

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v0

    .line 190
    aget-object v7, v4, v0

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v0

    .line 191
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    aget-object v8, v1, v0

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    div-int/lit8 v8, v8, 0xa

    aget-object v9, v4, v0

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v11

    const/16 v10, 0xa

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->addValue(IFI)V

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_0
    const/4 v0, 0x1

    :goto_1
    array-length v7, v5

    if-ge v0, v7, :cond_1

    .line 195
    aget-object v7, v5, v0

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 196
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    aget-object v8, v5, v0

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v11

    invoke-virtual {v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->addStdev1Value(F)V

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 199
    :cond_1
    const/4 v0, 0x1

    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 200
    aget-object v7, v6, v0

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    .line 201
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    aget-object v8, v6, v0

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v11

    invoke-virtual {v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->addStdev2Value(F)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 204
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg2:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 205
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->showGraph()V

    .line 211
    .end local v0    # "i":I
    .end local v1    # "normalDataArray":[Ljava/lang/String;
    .end local v2    # "scaleX":F
    .end local v3    # "scaleY":F
    .end local v4    # "stdevDataArray":[Ljava/lang/String;
    .end local v5    # "stdevSection1Array":[Ljava/lang/String;
    .end local v6    # "stdevSection2Array":[Ljava/lang/String;
    :goto_3
    return-void

    .line 207
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    const-string v8, "Cannot found Data log!!"

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$202(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Ljava/lang/String;)Ljava/lang/String;

    .line 208
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 209
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
