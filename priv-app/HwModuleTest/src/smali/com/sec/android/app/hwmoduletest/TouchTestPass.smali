.class public Lcom/sec/android/app/hwmoduletest/TouchTestPass;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TouchTestPass.java"


# instance fields
.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "TouchTestPass"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TouchTestPass$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/TouchTestPass$1;-><init>(Lcom/sec/android/app/hwmoduletest/TouchTestPass;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->mHandler:Landroid/os/Handler;

    .line 18
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 28
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->requestWindowFeature(I)Z

    .line 30
    const v0, 0x7f030082

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTestPass;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 33
    return-void
.end method
