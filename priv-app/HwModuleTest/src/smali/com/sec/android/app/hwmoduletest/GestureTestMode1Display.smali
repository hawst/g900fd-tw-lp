.class public Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;
.super Landroid/app/Activity;
.source "GestureTestMode1Display.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureTestMode1Display"

.field private static final TYPE_AIRMOTION:I = 0x2

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mLoggingOn:Z

.field private static mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItem1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 64
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 66
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z

    .line 67
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 69
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mValueList:Ljava/util/List;

    .line 61
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I

    .line 62
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mHandler:Landroid/os/Handler;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mPowerManager:Landroid/os/PowerManager;

    .line 124
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 56
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z

    return v0
.end method

.method static synthetic access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    return-object v0
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 119
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

    const v1, 0x7f030026

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

    .line 120
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mListView:Landroid/widget/ListView;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 122
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    const-string v0, "GestureTestMode1Display"

    const-string v1, "onCreate"

    const-string v2, "onCreate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    const v0, 0x7f030025

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->setContentView(I)V

    .line 76
    const-string v0, "sensorhub"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 77
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 78
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mPowerManager:Landroid/os/PowerManager;

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "GestureTestMode2Display"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "logging"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z

    .line 81
    const-string v0, "GestureTestMode1Display"

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "logging flag value = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->initTextView()V

    .line 83
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 114
    const-string v0, "GestureTestMode1Display"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 116
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 98
    const-string v0, "GestureTestMode1Display"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 100
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 102
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 103
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 106
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_1

    .line 107
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 111
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 86
    const-string v0, "GestureTestMode1Display"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 89
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 90
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 92
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z

    if-eqz v0, :cond_0

    .line 93
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    const-string v1, "GestureZmean"

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 95
    :cond_0
    return-void
.end method
