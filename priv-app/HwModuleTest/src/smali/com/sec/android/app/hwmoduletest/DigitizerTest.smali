.class public Lcom/sec/android/app/hwmoduletest/DigitizerTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "DigitizerTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;
    }
.end annotation


# static fields
.field private static final AMETA_PEN_ON:I = 0x2000000

.field private static final PLAY_1000_HZ:I = 0x65

.field private static final PLAY_300_HZ:I = 0x64


# instance fields
.field private HEIGHT_BASIS:I

.field private SIZE_RECT:I

.field private WIDTH_BASIS:I

.field private draw:[[Z

.field private isDrawArea:[[Z

.field private mBottommostOfMatrix:I

.field private mCenterOfHorizontalOfMatrix:I

.field private mCenterOfVerticalOfMatrix:I

.field private mHandler:Landroid/os/Handler;

.field private mIsReceiverPlaying:Z

.field private mIsWacom:Z

.field private mLeftmostOfMatrix:I

.field private mRcvMediaPlayer:Landroid/media/MediaPlayer;

.field private mRightmostOfMatrix:I

.field private mTopmostOfMatrix:I

.field private mView:Landroid/view/View;

.field private mVolume:Landroid/media/AudioManager;

.field private passFlag:I

.field private remoteCall:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    const-string v0, "DigitizerTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 36
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I

    .line 41
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->SIZE_RECT:I

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->remoteCall:Z

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mHandler:Landroid/os/Handler;

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsReceiverPlaying:Z

    .line 58
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mTopmostOfMatrix:I

    .line 59
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mBottommostOfMatrix:I

    .line 60
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfVerticalOfMatrix:I

    .line 61
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mLeftmostOfMatrix:I

    .line 62
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRightmostOfMatrix:I

    .line 63
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfHorizontalOfMatrix:I

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsWacom:Z

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;
    .param p1, "x1"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I

    return p1
.end method

.method static synthetic access$008(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->passFlag:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->receiverStop()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/DigitizerTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->receiverStart(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->remoteCall:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsWacom:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/DigitizerTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/DigitizerTest;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private decideRemote()V
    .locals 3

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 120
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RemoteCall"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->remoteCall:Z

    .line 121
    return-void
.end method

.method private fillUpMatrix()V
    .locals 11

    .prologue
    const/16 v10, 0x17

    const/16 v9, 0x14

    const/16 v8, 0x8

    const/16 v6, 0xe

    const/4 v7, 0x1

    .line 132
    const/4 v3, 0x0

    .local v3, "row":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    if-ge v3, v4, :cond_1

    .line 133
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    if-ge v0, v4, :cond_0

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v3

    const/4 v5, 0x0

    aput-boolean v5, v4, v0

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 132
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "column":I
    :cond_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    const/16 v5, 0x34

    if-ne v4, v5, :cond_3

    .line 150
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x2

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aput-boolean v7, v4, v5

    .line 151
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x3

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aput-boolean v7, v4, v5

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x4

    aget-object v4, v4, v5

    aput-boolean v7, v4, v7

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x5

    aget-object v4, v4, v5

    aput-boolean v7, v4, v7

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x5

    aget-object v4, v4, v5

    const/4 v5, 0x2

    aput-boolean v7, v4, v5

    .line 155
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x6

    aget-object v4, v4, v5

    const/4 v5, 0x2

    aput-boolean v7, v4, v5

    .line 156
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/4 v5, 0x7

    aget-object v4, v4, v5

    const/4 v5, 0x3

    aput-boolean v7, v4, v5

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v8

    const/4 v5, 0x3

    aput-boolean v7, v4, v5

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v8

    const/4 v5, 0x4

    aput-boolean v7, v4, v5

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x9

    aget-object v4, v4, v5

    const/4 v5, 0x4

    aput-boolean v7, v4, v5

    .line 160
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0xa

    aget-object v4, v4, v5

    const/4 v5, 0x5

    aput-boolean v7, v4, v5

    .line 161
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0xb

    aget-object v4, v4, v5

    const/4 v5, 0x6

    aput-boolean v7, v4, v5

    .line 162
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0xc

    aget-object v4, v4, v5

    const/4 v5, 0x6

    aput-boolean v7, v4, v5

    .line 163
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0xd

    aget-object v4, v4, v5

    const/4 v5, 0x7

    aput-boolean v7, v4, v5

    .line 164
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v6

    const/4 v5, 0x7

    aput-boolean v7, v4, v5

    .line 165
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v6

    aput-boolean v7, v4, v8

    .line 166
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0xf

    aget-object v4, v4, v5

    aput-boolean v7, v4, v8

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x10

    aget-object v4, v4, v5

    const/16 v5, 0x9

    aput-boolean v7, v4, v5

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x11

    aget-object v4, v4, v5

    const/16 v5, 0x9

    aput-boolean v7, v4, v5

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x11

    aget-object v4, v4, v5

    const/16 v5, 0xa

    aput-boolean v7, v4, v5

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x12

    aget-object v4, v4, v5

    const/16 v5, 0xa

    aput-boolean v7, v4, v5

    .line 171
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x13

    aget-object v4, v4, v5

    const/16 v5, 0xb

    aput-boolean v7, v4, v5

    .line 172
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v9

    const/16 v5, 0xb

    aput-boolean v7, v4, v5

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v9

    const/16 v5, 0xc

    aput-boolean v7, v4, v5

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x15

    aget-object v4, v4, v5

    const/16 v5, 0xc

    aput-boolean v7, v4, v5

    .line 175
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x16

    aget-object v4, v4, v5

    const/16 v5, 0xd

    aput-boolean v7, v4, v5

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v10

    const/16 v5, 0xd

    aput-boolean v7, v4, v5

    .line 177
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v10

    aput-boolean v7, v4, v6

    .line 178
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x18

    aget-object v4, v4, v5

    aput-boolean v7, v4, v6

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x18

    aget-object v4, v4, v5

    aput-boolean v7, v4, v6

    .line 180
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x19

    aget-object v4, v4, v5

    aput-boolean v7, v4, v6

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x19

    aget-object v4, v4, v5

    const/16 v5, 0xf

    aput-boolean v7, v4, v5

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1a

    aget-object v4, v4, v5

    const/16 v5, 0xf

    aput-boolean v7, v4, v5

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1b

    aget-object v4, v4, v5

    const/16 v5, 0x10

    aput-boolean v7, v4, v5

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1c

    aget-object v4, v4, v5

    const/16 v5, 0x10

    aput-boolean v7, v4, v5

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1d

    aget-object v4, v4, v5

    const/16 v5, 0x11

    aput-boolean v7, v4, v5

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1e

    aget-object v4, v4, v5

    const/16 v5, 0x12

    aput-boolean v7, v4, v5

    .line 187
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x1f

    aget-object v4, v4, v5

    const/16 v5, 0x12

    aput-boolean v7, v4, v5

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x20

    aget-object v4, v4, v5

    const/16 v5, 0x13

    aput-boolean v7, v4, v5

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x21

    aget-object v4, v4, v5

    const/16 v5, 0x13

    aput-boolean v7, v4, v5

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x21

    aget-object v4, v4, v5

    aput-boolean v7, v4, v9

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x22

    aget-object v4, v4, v5

    aput-boolean v7, v4, v9

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x23

    aget-object v4, v4, v5

    const/16 v5, 0x15

    aput-boolean v7, v4, v5

    .line 193
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x24

    aget-object v4, v4, v5

    const/16 v5, 0x15

    aput-boolean v7, v4, v5

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x24

    aget-object v4, v4, v5

    const/16 v5, 0x16

    aput-boolean v7, v4, v5

    .line 195
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x25

    aget-object v4, v4, v5

    const/16 v5, 0x16

    aput-boolean v7, v4, v5

    .line 196
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x26

    aget-object v4, v4, v5

    aput-boolean v7, v4, v10

    .line 197
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x27

    aget-object v4, v4, v5

    aput-boolean v7, v4, v10

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x27

    aget-object v4, v4, v5

    const/16 v5, 0x18

    aput-boolean v7, v4, v5

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x28

    aget-object v4, v4, v5

    const/16 v5, 0x18

    aput-boolean v7, v4, v5

    .line 200
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x28

    aget-object v4, v4, v5

    const/16 v5, 0x19

    aput-boolean v7, v4, v5

    .line 201
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x29

    aget-object v4, v4, v5

    const/16 v5, 0x19

    aput-boolean v7, v4, v5

    .line 202
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2a

    aget-object v4, v4, v5

    const/16 v5, 0x1a

    aput-boolean v7, v4, v5

    .line 203
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2b

    aget-object v4, v4, v5

    const/16 v5, 0x1a

    aput-boolean v7, v4, v5

    .line 204
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2c

    aget-object v4, v4, v5

    const/16 v5, 0x1b

    aput-boolean v7, v4, v5

    .line 205
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2d

    aget-object v4, v4, v5

    const/16 v5, 0x1b

    aput-boolean v7, v4, v5

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2e

    aget-object v4, v4, v5

    const/16 v5, 0x1c

    aput-boolean v7, v4, v5

    .line 207
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2f

    aget-object v4, v4, v5

    const/16 v5, 0x1c

    aput-boolean v7, v4, v5

    .line 208
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x2f

    aget-object v4, v4, v5

    const/16 v5, 0x1d

    aput-boolean v7, v4, v5

    .line 209
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x30

    aget-object v4, v4, v5

    const/16 v5, 0x1d

    aput-boolean v7, v4, v5

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x31

    aget-object v4, v4, v5

    const/16 v5, 0x1e

    aput-boolean v7, v4, v5

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x32

    aget-object v4, v4, v5

    const/16 v5, 0x1f

    aput-boolean v7, v4, v5

    .line 212
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    const/16 v5, 0x33

    aget-object v4, v4, v5

    const/16 v5, 0x1f

    aput-boolean v7, v4, v5

    .line 227
    :cond_2
    return-void

    .line 215
    :cond_3
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    sub-float/2addr v5, v6

    div-float v2, v4, v5

    .line 217
    .local v2, "rate":F
    const/4 v3, 0x1

    :goto_2
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_2

    .line 218
    add-int/lit8 v4, v3, -0x1

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v0, v4

    .line 219
    .restart local v0    # "column":I
    int-to-float v4, v3

    mul-float/2addr v4, v2

    float-to-int v1, v4

    .line 220
    .local v1, "nextcolumn":I
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v3

    aput-boolean v7, v4, v0

    .line 222
    if-eq v0, v1, :cond_4

    .line 223
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    add-int/lit8 v5, v3, -0x1

    aget-object v4, v4, v5

    aput-boolean v7, v4, v0

    .line 217
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private initGridSettings()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z

    .line 109
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    .line 110
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mTopmostOfMatrix:I

    .line 111
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mBottommostOfMatrix:I

    .line 112
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfVerticalOfMatrix:I

    .line 113
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mLeftmostOfMatrix:I

    .line 114
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRightmostOfMatrix:I

    .line 115
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfHorizontalOfMatrix:I

    .line 116
    return-void
.end method

.method private isNeededCheck(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 230
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mTopmostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mBottommostOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfVerticalOfMatrix:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mLeftmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRightmostOfMatrix:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mCenterOfHorizontalOfMatrix:I

    if-ne p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPass()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 241
    const/4 v1, 0x1

    .line 243
    .local v1, "isPass":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    if-ge v0, v4, :cond_3

    .line 244
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    if-ge v2, v4, :cond_2

    .line 245
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->isDrawArea:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-ne v4, v3, :cond_0

    .line 246
    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->draw:[[Z

    aget-object v4, v4, v0

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_1

    move v1, v3

    .line 244
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 246
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 243
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    .end local v2    # "j":I
    :cond_3
    return v1
.end method

.method private receiverStart(I)V
    .locals 5
    .param p1, "hertz"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 254
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsReceiverPlaying:Z

    if-nez v0, :cond_0

    .line 255
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsReceiverPlaying:Z

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mVolume:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mVolume:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 261
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 262
    const v0, 0x7f050003

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    .line 267
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 270
    const-wide/16 v0, 0x82

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 276
    :cond_0
    return-void

    .line 264
    :cond_1
    const v0, 0x7f050006

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private receiverStop()V
    .locals 1

    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsReceiverPlaying:Z

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mRcvMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mIsReceiverPlaying:Z

    .line 285
    :cond_0
    return-void
.end method

.method private setGridSizebyModel()V
    .locals 7

    .prologue
    .line 90
    const-string v4, "DIGITIZER_HEIGHT_BASIS"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    .line 91
    const-string v4, "DIGITIZER_WIDTH_BASIS"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    .line 93
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    if-nez v4, :cond_1

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 95
    .local v1, "nScreenWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 97
    .local v0, "nScreenHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 98
    .local v3, "r":Landroid/content/res/Resources;
    const/4 v4, 0x5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->SIZE_RECT:I

    int-to-float v5, v5

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 99
    .local v2, "pxsize":F
    int-to-float v4, v1

    div-float/2addr v4, v2

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    .line 100
    int-to-float v4, v0

    div-float/2addr v4, v2

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    .line 105
    .end local v0    # "nScreenHeight":I
    .end local v1    # "nScreenWidth":I
    .end local v2    # "pxsize":F
    .end local v3    # "r":Landroid/content/res/Resources;
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v4, "DIGITIZER_HEIGHT_BASIS"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->HEIGHT_BASIS:I

    .line 103
    const-string v4, "DIGITIZER_WIDTH_BASIS"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->WIDTH_BASIS:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 71
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "TouchTest is created"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->setRemoveSystemUI(Landroid/view/Window;Z)V

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->decideRemote()V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->setGridSizebyModel()V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->initGridSettings()V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->fillUpMatrix()V

    .line 79
    new-instance v0, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest$MyView;-><init>(Lcom/sec/android/app/hwmoduletest/DigitizerTest;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mView:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->setContentView(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 84
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->mVolume:Landroid/media/AudioManager;

    .line 86
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 289
    const/16 v0, 0x18

    if-ne p1, v0, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->finish()V

    .line 293
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 236
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onStop()V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DigitizerTest;->finish()V

    .line 238
    return-void
.end method
