.class public Lcom/sec/android/app/hwmoduletest/view/UVGraph;
.super Landroid/view/View;
.source "UVGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private final GRAPH_SCALING_VALUE:F

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private MAX_VALUE_LIST:F

.field private MIN_VALUE_LIST:F

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPath1:Landroid/graphics/Path;

.field private mPath2:Landroid/graphics/Path;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mUV1_Paint:Landroid/graphics/Paint;

.field private mUV2_Paint:Landroid/graphics/Paint;

.field private mValue1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mValue2:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mVendor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_X:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    .line 26
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INCREASING_COOR:I

    .line 29
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->GRAPH_SCALING_VALUE:F

    .line 36
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    .line 44
    const-string v0, "UVGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->CLASS_NAME:Ljava/lang/String;

    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->init(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_X:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    .line 26
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INCREASING_COOR:I

    .line 29
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->GRAPH_SCALING_VALUE:F

    .line 36
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    .line 44
    const-string v0, "UVGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->CLASS_NAME:Ljava/lang/String;

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->init(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_X:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    .line 26
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INCREASING_COOR:I

    .line 29
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->GRAPH_SCALING_VALUE:F

    .line 36
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    .line 44
    const-string v0, "UVGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->CLASS_NAME:Ljava/lang/String;

    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->init(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method private checkValue()V
    .locals 3

    .prologue
    .line 147
    const v1, 0x469c4000    # 20000.0f

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    .line 148
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    .line 150
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    .line 150
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    goto :goto_1

    .line 158
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    .line 158
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    goto :goto_3

    .line 165
    :cond_5
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v9, -0x1000000

    const/high16 v8, 0x41f00000    # 30.0f

    const/high16 v7, 0x40c00000    # 6.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 62
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mContext:Landroid/content/Context;

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 65
    .local v1, "mWm":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenWidth:I

    .line 66
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    .line 67
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    mul-int/lit8 v2, v2, 0x9

    div-int/lit8 v2, v2, 0xa

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    .line 68
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenWidth:I

    add-int/lit8 v2, v2, -0xa

    div-int/lit8 v2, v2, 0x5

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->LIST_SIZE:I

    .line 69
    new-instance v2, Landroid/graphics/CornerPathEffect;

    const/high16 v3, 0x41200000    # 10.0f

    invoke-direct {v2, v3}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 70
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 73
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 75
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 80
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 85
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mRectPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mRectPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mRectPaint:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 89
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->invalidate()V

    .line 97
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    const v3, 0x1001d

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getVendor()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mVendor:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 101
    const-string v2, "UV"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mVendor:Ljava/lang/String;

    goto :goto_0
.end method

.method private setPath1()V
    .locals 8

    .prologue
    const v7, 0x3f4ccccd    # 0.8f

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 169
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 173
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x5

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->invalidate()V

    .line 178
    return-void
.end method

.method private setPath2()V
    .locals 8

    .prologue
    const v7, 0x3f4ccccd    # 0.8f

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 182
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 186
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x5

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->invalidate()V

    .line 191
    return-void
.end method


# virtual methods
.method public addValue1(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue1:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->checkValue()V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->setPath1()V

    .line 133
    return-void
.end method

.method public addValue2(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mValue2:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 141
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->checkValue()V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->setPath2()V

    .line 144
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x40400000    # 3.0f

    const/high16 v6, 0x42480000    # 50.0f

    const/high16 v2, 0x42200000    # 40.0f

    .line 109
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mVendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "UV A"

    const/high16 v1, 0x43520000    # 210.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 111
    const/high16 v1, 0x43910000    # 290.0f

    const/high16 v3, 0x43dc0000    # 440.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 112
    const-string v0, "UV B"

    const/high16 v1, 0x43eb0000    # 470.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 113
    const v1, 0x44098000    # 550.0f

    const v3, 0x442c8000    # 690.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 116
    :cond_0
    const-string v0, "ADC"

    const/high16 v1, 0x41f00000    # 30.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v6, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 117
    const-string v0, "Time"

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0x64

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    add-int/lit8 v2, v2, -0x14

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath1:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV1_Paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mPath2:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mUV2_Paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 121
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0x3

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mScreenHeight:I

    add-int/lit8 v0, v0, -0x3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/UVGraph;->mRectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v7

    move v2, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 122
    return-void
.end method
