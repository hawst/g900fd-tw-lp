.class Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;
.super Ljava/lang/Object;
.source "GripSensorTest2.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Lcom/sec/android/app/hwmoduletest/GripSensorTest2$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2$1;

    .prologue
    .line 298
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 301
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f08008f

    const v7, 0x7f070002

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 305
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor1 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 308
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor1 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    cmpl-float v0, v0, v9

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f070021

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 312
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip2:ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # setter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$202(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z

    .line 321
    :cond_0
    :goto_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    cmpl-float v0, v0, v9

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f070021

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 324
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip2:ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # setter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$502(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z

    .line 333
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-eq v0, v5, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-ne v0, v5, :cond_5

    .line 334
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V
    invoke-static {v0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;I)V

    .line 354
    :goto_2
    return-void

    .line 314
    :cond_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 317
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ release Grip1:OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # setter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$202(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z

    goto/16 :goto_0

    .line 326
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 329
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ release Grip2:OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # setter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$502(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z

    goto/16 :goto_1

    .line 336
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$900(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V

    goto :goto_2

    .line 339
    :cond_6
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v6

    cmpl-float v0, v0, v9

    if-nez v0, :cond_7

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f070021

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V
    invoke-static {v0, v6}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;I)V

    .line 343
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 345
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # invokes: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$900(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 350
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
