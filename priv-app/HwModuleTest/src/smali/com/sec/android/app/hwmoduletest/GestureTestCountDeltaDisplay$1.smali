.class Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;
.super Ljava/lang/Object;
.source "GestureTestCountDeltaDisplay.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 137
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v1, 0x5

    aget v2, v0, v1

    .line 138
    .local v2, "a_Delta":F
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v1, 0x6

    aget v3, v0, v1

    .line 139
    .local v3, "b_Delta":F
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v1, 0x7

    aget v4, v0, v1

    .line 140
    .local v4, "c_Delta":F
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/16 v1, 0x8

    aget v5, v0, v1

    .line 141
    .local v5, "d_Delta":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$008(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)I

    .line 142
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "listenner: getSensorData()"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "count = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Ch A = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Ch B = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Ch C = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Ch D = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v1, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)I

    move-result v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;-><init>(IFFFF)V

    invoke-interface {v6, v7, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;->notifyDataSetChanged()V

    .line 151
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mLoggingOn:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$300()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDeltaDisplay;->access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(FFFF)V

    .line 153
    const-string v0, "GestureTestCountDeltaDisplay"

    const-string v1, "onGetSensorHubData"

    const-string v6, "write to log file "

    invoke-static {v0, v1, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_0
    return-void
.end method
