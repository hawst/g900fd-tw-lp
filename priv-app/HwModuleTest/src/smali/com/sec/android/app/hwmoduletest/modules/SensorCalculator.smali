.class public Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;
.super Ljava/lang/Object;
.source "SensorCalculator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;,
        Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "SensorCalculator"

.field private static final DEBUG:Z = true

.field private static RANGE_MAX_INTEGER:I

.field private static RANGE_MIN_INTEGER:I

.field private static RESULT_VALUE_NG:Ljava/lang/String;

.field private static RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

.field private static RESULT_VALUE_OK:Ljava/lang/String;

.field private static mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

.field private static mFeature_Accelerometer:Ljava/lang/String;

.field private static mFeature_Magnetic:Ljava/lang/String;

.field private static mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

.field private static mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

.field private static mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

.field private static mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    const v0, 0x7fffffff

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    .line 89
    const/high16 v0, -0x80000000

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MIN_INTEGER:I

    .line 91
    const-string v0, "OK"

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    .line 92
    const-string v0, "NG"

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    .line 93
    const-string v0, "None"

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    .line 95
    sput-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    .line 96
    sput-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 97
    sput-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 98
    sput-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 99
    sput-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static accelerometerAngle([I)[Ljava/lang/String;
    .locals 11
    .param p0, "data"    # [I

    .prologue
    const/high16 v10, -0x40800000    # -1.0f

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x42652ee1

    .line 309
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    .line 310
    .local v0, "angle":[Ljava/lang/String;
    aget v2, p0, v7

    aget v3, p0, v7

    mul-int/2addr v2, v3

    aget v3, p0, v8

    aget v4, p0, v8

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aget v3, p0, v9

    aget v4, p0, v9

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 312
    .local v1, "realg":F
    const-string v2, "IS_SIMPLE_TEST_ACC_SYSFS"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v7

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    mul-float/2addr v3, v10

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 315
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v8

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    mul-float/2addr v3, v10

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v9

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    const/high16 v4, 0x42b40000    # 90.0f

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    .line 326
    :goto_0
    return-object v0

    .line 320
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v7

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v8

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v8

    .line 322
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p0, v9

    int-to-float v3, v3

    div-float/2addr v3, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    const/high16 v4, 0x42b40000    # 90.0f

    sub-float/2addr v3, v4

    mul-float/2addr v3, v10

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v9

    goto :goto_0
.end method

.method public static checkSpecMagneticADC(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 522
    const-string v0, "SensorCalculator"

    const-string v1, "checkSpecMagneticADC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Feature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticADC"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 526
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->getResult(IIILcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static checkSpecMagneticADC2(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 532
    const-string v0, "SensorCalculator"

    const-string v1, "checkSpecMagneticADC2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Feature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticADC2"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 536
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->getResult(IIILcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static checkSpecMagneticDAC(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 512
    const-string v0, "SensorCalculator"

    const-string v1, "checkSpecMagneticDAC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Feature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticDAC"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 516
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->getResult(IIILcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static checkSpecMagneticSelf(III)Ljava/lang/String;
    .locals 4
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 542
    const-string v0, "SensorCalculator"

    const-string v1, "checkSpecMagneticSelf"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Feature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticSelf"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 546
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->getResult(IIILcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getResult(IIILcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Ljava/lang/String;
    .locals 3
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I
    .param p3, "range"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .prologue
    .line 412
    if-eqz p3, :cond_e

    .line 413
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRangeCount:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 414
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : 1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 419
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    .line 505
    :goto_0
    return-object v0

    .line 424
    :cond_0
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto :goto_0

    .line 431
    :cond_1
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_2

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_2

    .line 433
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto :goto_0

    .line 437
    :cond_2
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    goto :goto_0

    .line 438
    :cond_3
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRangeCount:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 439
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : 2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 443
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 445
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 448
    :cond_4
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_5

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 449
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 450
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 453
    :cond_5
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_6

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_X:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 454
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_X_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_6

    .line 455
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - X"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 461
    :cond_6
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_7

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 462
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 464
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 467
    :cond_7
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_8

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 468
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 469
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 472
    :cond_8
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_9

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Y:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 473
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Y_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_9

    .line 474
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - Y"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 480
    :cond_9
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_a

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 481
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 483
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[All] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 486
    :cond_a
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_b

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 487
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 488
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range1] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 491
    :cond_b
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-nez v0, :cond_c

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange2_Z:Z
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 492
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Min:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v0

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange2_Z_Max:I
    invoke-static {p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$1900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    invoke-static {p2, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->isSpecIn(III)Z

    move-result v0

    if-nez v0, :cond_c

    .line 493
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "[Range2] Fail - Z"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NG:Ljava/lang/String;

    goto/16 :goto_0

    .line 498
    :cond_c
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_OK:Ljava/lang/String;

    goto/16 :goto_0

    .line 500
    :cond_d
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "mRangeCount : Unknown"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    goto/16 :goto_0

    .line 504
    :cond_e
    const-string v0, "SensorCalculator"

    const-string v1, "getResult"

    const-string v2, "Fail - Spec null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RESULT_VALUE_NOTSUPPORTED:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static getResultAccelerometerSelf(III)Ljava/lang/String;
    .locals 5
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "z"    # I

    .prologue
    .line 331
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Feature : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const-string v2, "AccelSelf"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;Ljava/lang/String;)V

    .line 336
    const-string v0, ""

    .line 338
    .local v0, "returnValue":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    if-eqz v1, :cond_9

    .line 339
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 340
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-lt p0, v1, :cond_0

    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-ge v1, p0, :cond_3

    .line 342
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    :goto_0
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :goto_1
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 355
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-lt p1, v1, :cond_1

    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-ge v1, p1, :cond_5

    .line 357
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 362
    :goto_2
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :goto_3
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 370
    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-lt p2, v1, :cond_2

    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v1

    if-ge v1, p2, :cond_7

    .line 372
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 377
    :goto_4
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v4

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_5
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 388
    :goto_6
    return-object v1

    .line 344
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 351
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 359
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 366
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 374
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 381
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "P"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 387
    :cond_9
    const-string v1, "SensorCalculator"

    const-string v2, "getResultAccelerometerSelf"

    const-string v3, "FFF - Spec null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const-string v1, "FFF"

    goto/16 :goto_6
.end method

.method public static initialize()V
    .locals 4

    .prologue
    .line 104
    const-string v0, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    .line 106
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    .line 107
    const-string v0, "SensorCalculator"

    const-string v1, "initialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Accelerometer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "SensorCalculator"

    const-string v1, "initialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature_Magnetic : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->setSpecAccel()V

    .line 110
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->setSpecMagnetic()V

    .line 111
    return-void
.end method

.method private static isSpecIn(III)Z
    .locals 4
    .param p0, "value"    # I
    .param p1, "rangeMin"    # I
    .param p2, "rangeMax"    # I

    .prologue
    .line 397
    if-gt p1, p0, :cond_0

    if-gt p0, p2, :cond_0

    .line 398
    const-string v0, "SensorCalculator"

    const-string v1, "isSpecIn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Pass => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const/4 v0, 0x1

    .line 404
    :goto_0
    return v0

    .line 402
    :cond_0
    const-string v0, "SensorCalculator"

    const-string v1, "isSpecIn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setSpecAccel()V
    .locals 11

    .prologue
    .line 115
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_SMB380"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xc0

    const/16 v7, 0x140

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    .line 177
    :goto_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const-string v1, "AccelSelf"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;Ljava/lang/String;)V

    .line 179
    return-void

    .line 119
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xbe

    const/16 v7, 0x142

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto :goto_0

    .line 123
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA022"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x34

    const/16 v3, 0x34

    const/16 v4, -0x34

    const/16 v5, 0x34

    const/16 v6, 0xb9

    const/16 v7, 0x147

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto :goto_0

    .line 127
    :cond_2
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA023"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 128
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xa

    const/16 v2, -0x38

    const/16 v3, 0x38

    const/16 v4, -0x38

    const/16 v5, 0x38

    const/16 v6, 0xb5

    const/16 v7, 0x14b

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA222"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 132
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0x8

    const/16 v2, -0xf

    const/16 v3, 0xf

    const/16 v4, -0xf

    const/16 v5, 0xf

    const/16 v6, 0x2c

    const/16 v7, 0x54

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 135
    :cond_4
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "BOSCH_BMA220"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 136
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/4 v1, 0x6

    const/4 v2, -0x6

    const/4 v3, 0x6

    const/4 v4, -0x6

    const/4 v5, 0x6

    const/16 v6, 0x9

    const/16 v7, 0x17

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 139
    :cond_5
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_KR3DH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 140
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x66

    const/16 v3, 0x66

    const/16 v4, -0x66

    const/16 v5, 0x66

    const/16 v6, 0x351

    const/16 v7, 0x4af

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 143
    :cond_6
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_K3DH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 144
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, 0x31e

    const/16 v7, 0x4e2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 147
    :cond_7
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_KR3DM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 148
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0x8

    const/16 v2, -0xc

    const/16 v3, 0xc

    const/16 v4, -0xc

    const/16 v5, 0xc

    const/16 v6, 0x30

    const/16 v7, 0x50

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 151
    :cond_8
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "STMICRO_LSM330DLC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 153
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x9a

    const/16 v3, 0x9a

    const/16 v4, -0x9a

    const/16 v5, 0x9a

    const/16 v6, 0x31e

    const/16 v7, 0x4e2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 156
    :cond_9
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "KIONIX_KXUD9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 157
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x97

    const/16 v3, 0x97

    const/16 v4, -0x97

    const/16 v5, 0x97

    const/16 v6, 0x282

    const/16 v7, 0x3e4

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 160
    :cond_a
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "KIONIX_KXTF9"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 161
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0xc

    const/16 v2, -0x85

    const/16 v3, 0x85

    const/16 v4, -0x85

    const/16 v5, 0x85

    const/16 v6, 0x34e

    const/16 v7, 0x4b2

    const/4 v8, 0x0

    const/16 v9, 0x64

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 164
    :cond_b
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6050"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6051"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 166
    :cond_c
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 169
    :cond_d
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Accelerometer:Ljava/lang/String;

    const-string v1, "INVENSENSE_MPU6515M"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 170
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    const/16 v1, 0x10

    const/16 v2, -0x806

    const/16 v3, 0x806

    const/16 v4, -0x806

    const/16 v5, 0x806

    const/16 v6, 0x33f6

    const/16 v7, 0x4c0a

    const/4 v8, 0x0

    const/16 v9, 0x640

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;-><init>(IIIIIIIIIZ)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0

    .line 173
    :cond_e
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mAccelerometer:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    goto/16 :goto_0
.end method

.method private static setSpecMagnetic()V
    .locals 15

    .prologue
    const/16 v1, -0x1964

    const/16 v8, 0x1e

    const/16 v7, -0x1e

    const/4 v14, 0x0

    const/4 v13, 0x0

    .line 183
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 185
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x1964

    const/16 v4, 0x1964

    const/16 v6, 0x1964

    move v3, v1

    move v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 186
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 187
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 299
    :goto_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticDAC"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 300
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticADC"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 301
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticADC2"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 302
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const-string v1, "MagneticSelf"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 304
    return-void

    .line 190
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 192
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x1964

    const/16 v4, 0x1964

    const/16 v6, 0x1964

    move v3, v1

    move v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 193
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 194
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto :goto_0

    .line 196
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 198
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 199
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x1964

    const/16 v4, 0x1964

    const/16 v6, 0x1964

    move v3, v1

    move v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 200
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 201
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0xc8

    const/16 v2, 0xc8

    const/16 v3, -0xc8

    const/16 v4, 0xc8

    const/16 v5, -0xc80

    const/16 v6, -0x320

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto :goto_0

    .line 204
    :cond_2
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "AK8973"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x7e

    const/16 v4, 0x7e

    const/16 v6, 0x7e

    const/16 v7, 0x80

    const/16 v8, 0xfe

    const/16 v9, 0x80

    const/16 v10, 0xfe

    const/16 v11, 0x80

    const/16 v12, 0xfe

    move v1, v13

    move v3, v13

    move v5, v13

    invoke-direct/range {v0 .. v12}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIIIIIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 206
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x58

    const/16 v2, 0xa8

    const/16 v3, 0x58

    const/16 v4, 0xa8

    const/16 v5, 0x58

    const/16 v6, 0xa8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 207
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 208
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 211
    :cond_3
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "AK8975"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 212
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 213
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x7d0

    const/16 v2, 0x7d0

    const/16 v3, -0x7d0

    const/16 v4, 0x7d0

    const/16 v5, -0x7d0

    const/16 v6, 0x7d0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 214
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 215
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x64

    const/16 v2, 0x64

    const/16 v3, -0x64

    const/16 v4, 0x64

    const/16 v5, -0x3e8

    const/16 v6, -0x12c

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 218
    :cond_4
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "AK09911"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "AK09911C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 220
    :cond_5
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 221
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x640

    const/16 v2, 0x640

    const/16 v3, -0x640

    const/16 v4, 0x640

    const/16 v5, -0x640

    const/16 v6, 0x640

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 222
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 223
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v5, -0x190

    const/16 v6, -0x32

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 226
    :cond_6
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS529"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 227
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/4 v1, 0x5

    const/16 v2, 0x20

    const/4 v3, 0x5

    const/16 v4, 0x20

    const/4 v5, 0x5

    const/16 v6, 0x20

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 228
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 229
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 230
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x50

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0x6b

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 233
    :cond_7
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS530"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 234
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    move v5, v7

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 235
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 236
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 237
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x85

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0xa0

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 240
    :cond_8
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS530A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 241
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    move v5, v7

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 242
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 243
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 244
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x85

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0xa0

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 247
    :cond_9
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS530C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 248
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    move v5, v7

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 249
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 250
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x190

    const/16 v2, 0x190

    const/16 v3, -0x190

    const/16 v4, 0x190

    const/16 v5, -0x190

    const/16 v6, 0x190

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 251
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x85

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0xa0

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 254
    :cond_a
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS532"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 255
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    move v5, v7

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 256
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 257
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x258

    const/16 v2, 0x258

    const/16 v3, -0x258

    const/16 v4, 0x258

    const/16 v5, -0x258

    const/16 v6, 0x258

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 258
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x11

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0x16

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 261
    :cond_b
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "YAS532B"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 262
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v1, v7

    move v2, v8

    move v3, v7

    move v4, v8

    move v5, v7

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 263
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v2, 0x167

    move v1, v13

    move v3, v13

    move v4, v13

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 264
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x258

    const/16 v2, 0x258

    const/16 v3, -0x258

    const/16 v4, 0x258

    const/16 v5, -0x258

    const/16 v6, 0x258

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 265
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, 0x2c

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    const/16 v3, 0x3a

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->RANGE_MAX_INTEGER:I

    move v5, v13

    move v6, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 267
    :cond_c
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 268
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 270
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x800

    const/16 v2, 0x800

    const/16 v3, -0x800

    const/16 v4, 0x800

    const/16 v5, -0x2000

    const/16 v6, 0x2000

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 271
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 272
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v1, v13

    move v2, v13

    move v3, v13

    move v4, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 274
    :cond_d
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 275
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 277
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x28a0

    const/16 v2, 0x28a0

    const/16 v3, -0x28a0

    const/16 v4, 0x28a0

    const/16 v5, -0x4e20

    const/16 v6, 0x4e20

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 278
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 279
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v1, v13

    move v2, v13

    move v3, v13

    move v4, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 280
    :cond_e
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BMC150_COMBINATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 281
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 283
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x28a0

    const/16 v2, 0x28a0

    const/16 v3, -0x28a0

    const/16 v4, 0x28a0

    const/16 v5, -0x4e20

    const/16 v6, 0x4e20

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 284
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 285
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v5, 0xb40

    const/16 v6, 0xf00

    move v1, v13

    move v2, v13

    move v3, v13

    move v4, v13

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 286
    :cond_f
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "STMICRO_K303C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 287
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 288
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x4000

    const/16 v2, 0x4000

    const/16 v3, -0x4000

    const/16 v4, 0x4000

    const/16 v5, -0x4000

    const/16 v6, 0x4000

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 289
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 290
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    const/16 v1, -0x140d

    const/16 v2, -0x6af

    const/16 v3, -0x140d

    const/16 v4, -0x6af

    const/16 v5, -0x6af

    const/16 v6, -0xab

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0

    .line 292
    :cond_10
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticDAC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 293
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 294
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticADC2:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 295
    sput-object v14, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->mRangeMagneticSelf:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    goto/16 :goto_0
.end method

.method private static specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;Ljava/lang/String;)V
    .locals 1
    .param p0, "data"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 580
    if-eqz p0, :cond_0

    .line 581
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    .line 585
    :goto_0
    return-void

    .line 583
    :cond_0
    const/4 v0, 0x0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    invoke-static {v0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static specLog(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;Ljava/lang/String;)V
    .locals 3
    .param p0, "data"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 552
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 554
    .local v0, "message":Ljava/lang/String;
    if-eqz p0, :cond_3

    .line 555
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_X:Z
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "X("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Min:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_X_Max:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 561
    :goto_0
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Y:Z
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Y("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Min:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$500(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Y_Max:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$600(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 567
    :goto_1
    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mIsSupportRange1_Z:Z
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$700(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 568
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Z("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Min:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$800(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->mRange1_Z_Max:I
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;->access$900(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 576
    :goto_2
    const-string v1, "SensorCalculator"

    const-string v2, "specLog"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    return-void

    .line 558
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "X(not supported) , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 564
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Y(not supported) , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 570
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Z(not supported)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 573
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
