.class Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;
.super Ljava/lang/Thread;
.source "SensorReadFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SensorReadThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 269
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 272
    const/4 v3, -0x1

    .line 274
    .local v3, "infoArrayIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[I

    move-result-object v5

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 275
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mModuleSensorIDArray:[I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$100(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[I

    move-result-object v6

    aget v6, v6, v2

    # invokes: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->ConverterID(I)I
    invoke-static {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$200(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;I)I

    move-result v3

    .line 277
    const/4 v5, -0x1

    if-ge v5, v3, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v5

    aget-object v5, v5, v3

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v5

    aget-object v5, v5, v3

    iget-boolean v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mIsExistFile:Z

    if-eqz v5, :cond_0

    .line 280
    const-string v5, "SensorReadFile"

    const-string v6, "read"

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v7

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mName:Ljava/lang/String;

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v5

    aget-object v5, v5, v3

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mFileID:Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 286
    .local v4, "mTemp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 287
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInfo:[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$300(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)[Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;

    move-result-object v5

    aget-object v5, v5, v3

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$Info;->mData:[Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    .end local v4    # "mTemp":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 289
    .restart local v4    # "mTemp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v5, "SensorReadFile"

    const-string v6, "SensorReadThread-run"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "execption : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 296
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "mTemp":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 300
    :try_start_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 301
    :catch_2
    move-exception v1

    .line 302
    .local v1, "e1":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 295
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_5
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 300
    :try_start_6
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)I

    move-result v5

    int-to-long v6, v5

    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 301
    :catch_3
    move-exception v1

    .line 302
    .restart local v1    # "e1":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 299
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    .line 300
    :try_start_7
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->LOOP_DELAY:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$400(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)I

    move-result v6

    int-to-long v6, v6

    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_4

    .line 303
    :goto_3
    throw v5

    .line 301
    :catch_4
    move-exception v1

    .line 302
    .restart local v1    # "e1":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 311
    .end local v1    # "e1":Ljava/lang/InterruptedException;
    .end local v2    # "i":I
    .end local v3    # "infoArrayIndex":I
    :cond_2
    const-string v5, "SensorReadFile"

    const-string v6, "SensorReadThread-run"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LoopStop-mInterrupted : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile$SensorReadThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->mInterrupted:Z
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    return-void
.end method
