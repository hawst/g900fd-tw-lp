.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;
.super Ljava/lang/Object;
.source "BarometerWaterProofTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V
    .locals 0

    .prologue
    .line 730
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;

    .prologue
    .line 730
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 732
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 735
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 755
    :goto_0
    return-void

    .line 737
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    if-ne v0, v3, :cond_1

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    if-nez v0, :cond_0

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getRawValue()F

    move-result v0

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v4, v4, v2

    sub-float/2addr v0, v4

    const/high16 v4, 0x40400000    # 3.0f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mCurrentStage:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$800(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v0

    aget v0, v0, v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    .line 743
    const-string v3, "BarometerWaterProofTest"

    const-string v4, "onSensorChanged"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ref = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$200(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getRawValue()F

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "Curr = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", Past = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$100(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)[F

    move-result-object v5

    aget v5, v5, v2

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->runMeasureStage()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1500(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)V

    .line 746
    sget-object v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->RELASE_STAGE:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;

    iput-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$TestStage;->mTouchedTHD:Z

    .line 749
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    # setter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mBaromSensorValues:[F
    invoke-static {v2, v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$102(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;[F)[F

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;->access$1600(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 739
    goto/16 :goto_1

    .line 735
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
