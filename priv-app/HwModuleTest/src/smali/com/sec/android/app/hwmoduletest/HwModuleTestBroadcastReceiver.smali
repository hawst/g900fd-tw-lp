.class public Lcom/sec/android/app/hwmoduletest/HwModuleTestBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HwModuleTestBroadcastReceiver.java"


# static fields
.field public static final SECRET_CODE_ACTION:Ljava/lang/String; = "android.provider.Telephony.SECRET_CODE"

.field private static final TAG:Ljava/lang/String; = "TestingBroadcastReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 20
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "host":Ljava/lang/String;
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get the Secret code action. Host="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 26
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 29
    :cond_0
    if-nez v0, :cond_2

    .line 30
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "host is null."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 34
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v1    # "i":Landroid/content/Intent;
    :cond_2
    const-string v2, "0*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 35
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 36
    const-class v2, Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 59
    :cond_3
    :goto_1
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 38
    :cond_4
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "Lcdtest XML data parsing was not completed."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 40
    :cond_5
    const-string v2, "0482"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 41
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->instance()Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 42
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "SenorHub Start!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "isSensorHubTest"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    const-class v2, Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 46
    :cond_6
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "Lcdtest XML data parsing was not completed."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :cond_7
    const-string v2, "43788731"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 49
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "Gesture test mode1 start!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-class v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 51
    :cond_8
    const-string v2, "43788732"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 52
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "Gesture test mode2 start!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-class v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_1

    .line 54
    :cond_9
    const-string v2, "0437"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 55
    const-string v2, "TestingBroadcastReceiver"

    const-string v3, "onReceive"

    const-string v4, "Gesture test main start!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-class v2, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_1
.end method
