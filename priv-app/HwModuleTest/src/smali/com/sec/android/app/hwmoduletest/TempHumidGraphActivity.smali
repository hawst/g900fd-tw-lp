.class public Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "TempHumidGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$1;,
        Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;
    }
.end annotation


# instance fields
.field private mHumidSensor:Landroid/hardware/Sensor;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTempHumidGraph:Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

.field private mTempSensor:Landroid/hardware/Sensor;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    const-string v0, "TempHumidGraphActivity"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

    .line 23
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempHumidGraph:Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f030079

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->setContentView(I)V

    .line 29
    const v0, 0x7f0b024b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempHumidGraph:Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;

    .line 30
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempSensor:Landroid/hardware/Sensor;

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mHumidSensor:Landroid/hardware/Sensor;

    .line 33
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 51
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 45
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 46
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 36
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mTempSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidGraphActivity;->mHumidSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 41
    return-void
.end method
