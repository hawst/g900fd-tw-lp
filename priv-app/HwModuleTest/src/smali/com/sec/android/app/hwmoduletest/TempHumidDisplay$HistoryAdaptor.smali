.class Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;
.super Landroid/widget/ArrayAdapter;
.source "TempHumidDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryAdaptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 212
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 216
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;

    .line 219
    .local v10, "item":Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;
    if-nez p2, :cond_0

    .line 220
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 221
    .local v12, "li":Landroid/view/LayoutInflater;
    const v18, 0x7f030078

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v12, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 226
    .end local v12    # "li":Landroid/view/LayoutInflater;
    .local v11, "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getCount()Ljava/lang/String;

    move-result-object v4

    .line 227
    .local v4, "countString":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getValueTempRaw()Ljava/lang/String;

    move-result-object v16

    .line 228
    .local v16, "tempRawVal":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getValueHumidRaw()Ljava/lang/String;

    move-result-object v8

    .line 229
    .local v8, "humidRawVal":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getValueTempComp()Ljava/lang/String;

    move-result-object v14

    .line 230
    .local v14, "tempCompVal":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getValueHumidComp()Ljava/lang/String;

    move-result-object v6

    .line 231
    .local v6, "humidCompVal":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;->getValueCompCheck()Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "CompckeckVal":Ljava/lang/String;
    const v18, 0x7f0b00d3

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 233
    .local v5, "countView":Landroid/widget/TextView;
    const v18, 0x7f0b0246

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 234
    .local v17, "tempRawView":Landroid/widget/TextView;
    const v18, 0x7f0b0247

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 235
    .local v9, "humidRawView":Landroid/widget/TextView;
    const v18, 0x7f0b0248

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 236
    .local v15, "tempCompView":Landroid/widget/TextView;
    const v18, 0x7f0b0249

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 237
    .local v7, "humidCompView":Landroid/widget/TextView;
    const v18, 0x7f0b024a

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 238
    .local v13, "resultView":Landroid/widget/TextView;
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    invoke-virtual {v15, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {v13, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    const/high16 v18, -0x10000

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    .line 245
    const/high16 v18, -0x10000

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 246
    const v18, -0xffff01

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 247
    const v18, -0xffff01

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 248
    const v18, -0xff0001

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    return-object v11

    .end local v3    # "CompckeckVal":Ljava/lang/String;
    .end local v4    # "countString":Ljava/lang/String;
    .end local v5    # "countView":Landroid/widget/TextView;
    .end local v6    # "humidCompVal":Ljava/lang/String;
    .end local v7    # "humidCompView":Landroid/widget/TextView;
    .end local v8    # "humidRawVal":Ljava/lang/String;
    .end local v9    # "humidRawView":Landroid/widget/TextView;
    .end local v11    # "layout":Landroid/widget/LinearLayout;
    .end local v13    # "resultView":Landroid/widget/TextView;
    .end local v14    # "tempCompVal":Ljava/lang/String;
    .end local v15    # "tempCompView":Landroid/widget/TextView;
    .end local v16    # "tempRawVal":Ljava/lang/String;
    .end local v17    # "tempRawView":Landroid/widget/TextView;
    :cond_0
    move-object/from16 v11, p2

    .line 223
    check-cast v11, Landroid/widget/LinearLayout;

    .restart local v11    # "layout":Landroid/widget/LinearLayout;
    goto/16 :goto_0
.end method
