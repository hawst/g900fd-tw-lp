.class public Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "IRThermometerDisplay.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;,
        Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;,
        Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;
    }
.end annotation


# static fields
.field public static final vendor:Ljava/lang/String;


# instance fields
.field public final MSG_GET_DATA:I

.field public final MSG_UPDATE_UI:I

.field public final PARTRON_ITEMS:I

.field private avg_length:I

.field private bodytempData_result:[Ljava/lang/String;

.field private getCount:I

.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;

.field private mAmbientTemp:F

.field private mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private mBodyTempSensor:Landroid/hardware/Sensor;

.field private mBodyTempTitleText:Landroid/widget/TextView;

.field private mCount:I

.field private mFormat:Ljava/text/DecimalFormat;

.field private final mHandler:Landroid/os/Handler;

.field private mHumiTempValue:Ljava/lang/String;

.field private mIRThermometerOffButton:Landroid/widget/Button;

.field private mIRThermometerOnButton:Landroid/widget/Button;

.field private mIRThermometerTempText:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field private mLoggingOn:Z

.field private mRawTaTitleText:Landroid/widget/TextView;

.field private mRawToTitleText:Landroid/widget/TextView;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

.field private mTaDataTitleText:Landroid/widget/TextView;

.field private mToDataTitleText:Landroid/widget/TextView;

.field private final mUIHandler:Landroid/os/Handler;

.field private final mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private result_to:Ljava/lang/String;

.field private showBodyTemp:Z

.field private sumCount:I

.field private sum_to:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-string v0, "IR_THERMOMETER_SENSOR_VENDOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->vendor:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 83
    const-string v0, "IRThermometerDisplay"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 37
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->MSG_GET_DATA:I

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->result_to:Ljava/lang/String;

    .line 39
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    .line 42
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->avg_length:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mValueList:Ljava/util/List;

    .line 53
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    .line 54
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->showBodyTemp:Z

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mLoggingOn:Z

    .line 57
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 58
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->PARTRON_ITEMS:I

    .line 60
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mHandler:Landroid/os/Handler;

    .line 65
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mHumiTempValue:Ljava/lang/String;

    .line 67
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->MSG_UPDATE_UI:I

    .line 180
    new-instance v0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$1;-><init>(Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mUIHandler:Landroid/os/Handler;

    .line 84
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;[F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;
    .param p1, "x1"    # [F

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getValueFromSensor([F)V

    return-void
.end method

.method private getValueFromSensor([F)V
    .locals 9
    .param p1, "rawdata"    # [F

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 219
    const-string v2, "PARTRON"

    sget-object v3, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->vendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->showBodyTemp:Z

    if-eqz v2, :cond_4

    .line 220
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    if-ge v2, v8, :cond_0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    .line 222
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bodyTemp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x3

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", toData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", taData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rawTo="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rawTa="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    if-ne v2, v8, :cond_4

    .line 226
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    .line 227
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    aget v3, p1, v7

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    .line 229
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_4

    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, "averageTemp_to":F
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->avg_length:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mFormat:Ljava/text/DecimalFormat;

    float-to-double v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->result_to:Ljava/lang/String;

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Averaged To : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result_to : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->result_to:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_2

    .line 237
    if-ne v1, v7, :cond_1

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->result_to:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 236
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mFormat:Ljava/text/DecimalFormat;

    aget v4, p1, v1

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    goto :goto_1

    .line 244
    :cond_2
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    const/16 v3, 0x78

    if-ge v2, v3, :cond_3

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mValueList:Ljava/util/List;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;-><init>(I[Ljava/lang/String;)V

    invoke-interface {v2, v6, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;->notifyDataSetChanged()V

    .line 250
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mLoggingOn:Z

    if-eqz v2, :cond_3

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeBodyTempSensorData(I[Ljava/lang/String;)V

    .line 256
    :cond_3
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    .line 257
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    .line 263
    .end local v0    # "averageTemp_to":F
    .end local v1    # "i":I
    :cond_4
    return-void
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 266
    new-instance v0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;

    const v1, 0x7f030049

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;

    .line 267
    const v0, 0x7f0b0143

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mListView:Landroid/widget/ListView;

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 102
    const v0, 0x7f0b0139

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerTempText:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0b0134

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    .line 104
    const v0, 0x7f0b0133

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    .line 105
    const v0, 0x7f0b013d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempTitleText:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0b013e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mToDataTitleText:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0b013f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mTaDataTitleText:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0b0140

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mRawToTitleText:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0b0141

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mRawTaTitleText:Landroid/widget/TextView;

    .line 111
    const-string v0, "PARTRON"

    sget-object v1, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->bodytempData_result:[Ljava/lang/String;

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerTempText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 122
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 178
    :goto_0
    return-void

    .line 151
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click to ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mLoggingOn:Z

    if-eqz v0, :cond_0

    .line 153
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    const-string v1, "BodyTempData"

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 155
    :cond_0
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mCount:I

    .line 156
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getCount:I

    .line 157
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sumCount:I

    .line 158
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->sum_to:F

    .line 159
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->showBodyTemp:Z

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setClickable(Z)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 168
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click to OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->showBodyTemp:Z

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setClickable(Z)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOnButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTextColor(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 148
    :pswitch_data_0
    .packed-switch 0x7f0b0133
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 89
    const v1, 0x7f030048

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->setContentView(I)V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->initTextView()V

    .line 91
    new-instance v1, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$1;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;

    .line 92
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1001e

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempSensor:Landroid/hardware/Sensor;

    .line 95
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 96
    .local v0, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 97
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.##"

    invoke-direct {v1, v2, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mFormat:Ljava/text/DecimalFormat;

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->initialize()V

    .line 99
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 140
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOff()V

    .line 142
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 126
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 127
    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0x9

    aput v2, v0, v1

    .line 128
    .local v0, "mSensorTempID":[I
    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    const/16 v2, 0x3e8

    invoke-virtual {v1, p0, v0, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mBodyTempSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 131
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 4
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    sparse-switch p1, :sswitch_data_0

    .line 215
    :goto_0
    return-void

    .line 205
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mUIHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 209
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_TEMP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mHumiTempValue:Ljava/lang/String;

    goto :goto_0

    .line 203
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public updateUI()V
    .locals 6

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mHumiTempValue:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "TempCompRaw":[Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mAmbientTemp:F

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mIRThermometerTempText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mFormat:Ljava/text/DecimalFormat;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->mAmbientTemp:F

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    return-void
.end method
