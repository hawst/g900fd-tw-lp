.class public Lcom/sec/android/app/hwmoduletest/LedTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "LedTest.java"


# instance fields
.field private final COLORSTATE_BLUE:I

.field private final COLORSTATE_END:I

.field private final COLORSTATE_FINISH:I

.field private final COLORSTATE_GREEN:I

.field private final COLORSTATE_INIT:I

.field private final COLORSTATE_RED:I

.field private LED_OFF:Ljava/lang/String;

.field private LED_ON:Ljava/lang/String;

.field private LED_ON_BRIGHTNESS:I

.field private final LED_POWER_LOW:Ljava/lang/String;

.field private nColorState:I

.field private txtcolorstate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 17
    const-string v0, "LedTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 20
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_INIT:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_RED:I

    .line 22
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_GREEN:I

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_BLUE:I

    .line 24
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_END:I

    .line 25
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->COLORSTATE_FINISH:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->nColorState:I

    .line 28
    const-string v0, "LED_POWER_MAX"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_ON:Ljava/lang/String;

    .line 29
    const-string v0, "LED_POWER_MIN"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_OFF:Ljava/lang/String;

    .line 30
    const-string v0, "LED_POWER_MAX"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_ON_BRIGHTNESS:I

    .line 31
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_POWER_LOW:Ljava/lang/String;

    .line 18
    return-void
.end method

.method private setLED(III)V
    .locals 9
    .param p1, "setR"    # I
    .param p2, "setG"    # I
    .param p3, "setB"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLED()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setMix : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "setMix":Ljava/lang/String;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v1, v2, :cond_0

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 60000 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLED"

    const-string v3, "VERSION.SDK_INT is higher than KITKAT"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setLED"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "LED_BLINK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 109
    return-void
.end method

.method private setcolorstate(I)V
    .locals 5
    .param p1, "colorstate"    # I

    .prologue
    const/4 v4, 0x0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setcolorstate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->nColorState:I

    .line 60
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->nColorState:I

    packed-switch v0, :pswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 62
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const-string v1, "RED"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 64
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_ON_BRIGHTNESS:I

    invoke-direct {p0, v0, v4, v4}, Lcom/sec/android/app/hwmoduletest/LedTest;->setLED(III)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    const-string v2, "LED_RED On!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const-string v1, "GREEN"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 70
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_ON_BRIGHTNESS:I

    invoke-direct {p0, v4, v0, v4}, Lcom/sec/android/app/hwmoduletest/LedTest;->setLED(III)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    const-string v2, "LED_GREEN On!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const-string v1, "BLUE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->LED_ON_BRIGHTNESS:I

    invoke-direct {p0, v4, v4, v0}, Lcom/sec/android/app/hwmoduletest/LedTest;->setLED(III)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    const-string v2, "LED_BLUE On!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const-string v1, "END"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 82
    invoke-direct {p0, v4, v4, v4}, Lcom/sec/android/app/hwmoduletest/LedTest;->setLED(III)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    const-string v2, "LED Off!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    :pswitch_4
    invoke-direct {p0, v4, v4, v4}, Lcom/sec/android/app/hwmoduletest/LedTest;->setLED(III)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setcolorState"

    const-string v2, "FINISH!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f03004d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LedTest;->setContentView(I)V

    .line 39
    const v0, 0x7f0b0155

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/LedTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->txtcolorstate:Landroid/widget/TextView;

    .line 40
    const-string v0, "APQ8084"

    const-string v1, "APCHIP_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "LED_LOWPOWER"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 43
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/LedTest;->setcolorstate(I)V

    .line 44
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 112
    packed-switch p1, :pswitch_data_0

    .line 121
    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 112
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/LedTest;->setcolorstate(I)V

    .line 53
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 54
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 48
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 126
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 128
    .local v0, "action":I
    if-ne v0, v3, :cond_0

    .line 129
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->nColorState:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/LedTest;->setcolorstate(I)V

    .line 131
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/LedTest;->nColorState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LedTest;->finish()V

    .line 136
    :cond_0
    return v3
.end method
