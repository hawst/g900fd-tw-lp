.class public Lcom/sec/android/app/hwmoduletest/CameraImageView;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "CameraImageView.java"


# static fields
.field private static mRotatedBitmap:Landroid/graphics/Bitmap;

.field private static vgaDispBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private Isfrontcam:Z

.field private SCREEN_HEIGHT:I

.field private SCREEN_WIDTH:I

.field private image:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "CameraImageView"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->Isfrontcam:Z

    .line 30
    return-void
.end method

.method private FlipVerticalBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 203
    if-nez p1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "FlipVerticalBitmap"

    const-string v2, "originalBitmap is null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v7, 0x0

    .line 213
    :goto_0
    return-object v7

    .line 209
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 210
    .local v5, "matrix":Landroid/graphics/Matrix;
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 211
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 213
    .local v7, "flipedBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method private calculateSampleSize(I)I
    .locals 4
    .param p1, "scale"    # I

    .prologue
    .line 135
    const/4 v0, 0x2

    if-lt p1, v0, :cond_0

    .line 136
    div-int/lit8 v0, p1, 0x2

    mul-int/lit8 p1, v0, 0x2

    .line 141
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "calculateSampleSize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "scale = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    return p1

    .line 138
    :cond_0
    const/4 p1, 0x1

    goto :goto_0
.end method

.method private deleteCameraFile(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 159
    if-nez p1, :cond_0

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "deleteCameraFile"

    const-string v4, "path=null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 164
    :cond_0
    const/4 v1, 0x0

    .line 165
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 167
    .local v0, "delFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 173
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "deleteCameraFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private getBitmapFromFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 101
    if-nez p1, :cond_0

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getBitmapFromFile"

    const-string v8, "path=null"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-object v5

    .line 105
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getBitmapFromFile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "path="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 111
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getBitmapFromFile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Filt is not exist. / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_1
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 116
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 117
    const/4 v5, 0x1

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 118
    invoke-static {p1, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 119
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getBitmapFromFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "options.outWidth="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , options.outHeight="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_WIDTH:I

    div-int v4, v5, v6

    .line 122
    .local v4, "widthScale":I
    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_HEIGHT:I

    div-int v1, v5, v6

    .line 123
    .local v1, "heightScale":I
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getBitmapFromFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "widthScale="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , heightScale="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    if-le v4, v1, :cond_2

    move v3, v4

    .line 126
    .local v3, "scale":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getBitmapFromFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "scale="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->calculateSampleSize(I)I

    move-result v5

    iput v5, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 129
    const/4 v5, 0x0

    iput-boolean v5, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 130
    invoke-static {p1, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto/16 :goto_0

    .end local v3    # "scale":I
    :cond_2
    move v3, v1

    .line 125
    goto :goto_1
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "originalBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 177
    if-nez p1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "rotateBitmap"

    const-string v2, "originalBitmap is null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 v7, 0x0

    .line 199
    :goto_0
    return-object v7

    .line 183
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 185
    .local v5, "matrix":Landroid/graphics/Matrix;
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->Isfrontcam:Z

    if-eqz v0, :cond_1

    .line 188
    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 195
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 197
    .local v7, "rotatedBitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "rotateBitmap"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "width="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 192
    .end local v7    # "rotatedBitmap":Landroid/graphics/Bitmap;
    :cond_1
    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const v5, 0x7f030013

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->setContentView(I)V

    .line 36
    const v5, 0x7f0b005f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->image:Landroid/widget/ImageView;

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "bg_filepath"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 39
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 40
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onCreate"

    const-string v7, "File path is null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->finish()V

    .line 44
    :cond_0
    const-string v5, "window"

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 46
    .local v1, "display":Landroid/view/Display;
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 47
    .local v4, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v4}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 48
    iget v5, v4, Landroid/graphics/Point;->x:I

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_WIDTH:I

    .line 49
    iget v5, v4, Landroid/graphics/Point;->y:I

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_HEIGHT:I

    .line 50
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onCreate"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SCREEN_WIDTH X SCREEN_HEIGHT = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_WIDTH:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " X "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->SCREEN_HEIGHT:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 54
    .local v3, "myExtras":Landroid/os/Bundle;
    if-eqz v3, :cond_1

    .line 55
    const-string v5, "frontcam"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->Isfrontcam:Z

    .line 59
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getBitmapFromFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 61
    .local v0, "bgPic":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 62
    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/hwmoduletest/CameraImageView;->mRotatedBitmap:Landroid/graphics/Bitmap;

    .line 63
    sget-object v5, Lcom/sec/android/app/hwmoduletest/CameraImageView;->mRotatedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->FlipVerticalBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    sput-object v5, Lcom/sec/android/app/hwmoduletest/CameraImageView;->vgaDispBitmap:Landroid/graphics/Bitmap;

    .line 65
    iget-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->Isfrontcam:Z

    if-eqz v5, :cond_3

    .line 66
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onCreate"

    const-string v7, "Front Camera"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->image:Landroid/widget/ImageView;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/hwmoduletest/CameraImageView;->vgaDispBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    :cond_2
    :goto_0
    return-void

    .line 69
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "onCreate"

    const-string v7, "Mega Camera"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->image:Landroid/widget/ImageView;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/hwmoduletest/CameraImageView;->mRotatedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 88
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 89
    sget-object v0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->vgaDispBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "Remove vgaDispBitmap"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sput-object v3, Lcom/sec/android/app/hwmoduletest/CameraImageView;->vgaDispBitmap:Landroid/graphics/Bitmap;

    .line 94
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->mRotatedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/CameraImageView;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDestroy"

    const-string v2, "Remove mRotatedBitmap"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sput-object v3, Lcom/sec/android/app/hwmoduletest/CameraImageView;->mRotatedBitmap:Landroid/graphics/Bitmap;

    .line 98
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/CameraImageView;->finish()V

    .line 82
    const/4 v0, 0x1

    return v0
.end method
