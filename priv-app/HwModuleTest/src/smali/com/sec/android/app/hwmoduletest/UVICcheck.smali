.class public Lcom/sec/android/app/hwmoduletest/UVICcheck;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "UVICcheck.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private btn_Exit:Landroid/widget/Button;

.field private btn_Test:Landroid/widget/Button;

.field private mSpecID:Ljava/lang/String;

.field private mTestResult:Ljava/lang/Boolean;

.field private readProdID:Ljava/lang/String;

.field private txt_ProdID:Landroid/widget/TextView;

.field private txt_Spec:Landroid/widget/TextView;

.field private txt_TestResult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "UVICcheck"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method private InitValues()V
    .locals 4

    .prologue
    .line 51
    const-string v0, "UV_PROD_ID_SPEC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mSpecID:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "InitValues"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UV Prod ID Spec : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mSpecID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readProdID:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mTestResult:Ljava/lang/Boolean;

    .line 55
    return-void
.end method

.method private InitWidgets()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "InitWidgets"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const v0, 0x7f0b02d9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_ProdID:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0b02db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0b02da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_Spec:Landroid/widget/TextView;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_Spec:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(Spec : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mSpecID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const v0, 0x7f0b02dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->btn_Test:Landroid/widget/Button;

    .line 67
    const v0, 0x7f0b02dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->btn_Exit:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->btn_Test:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->btn_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method

.method private Start()V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->InitValues()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_ProdID:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readID()V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->isPass()V

    .line 95
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->displayResult()V

    .line 96
    return-void
.end method

.method private displayResult()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mTestResult:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_TestResult:Landroid/widget/TextView;

    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private isPass()V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mSpecID:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readProdID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->mTestResult:Ljava/lang/Boolean;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "isPass"

    const-string v2, "test pass!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    return-void
.end method

.method private readID()V
    .locals 4

    .prologue
    .line 99
    const-string v0, "UV_PROD_ID"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readProdID:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->txt_ProdID:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readProdID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "readID"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read UV Prod ID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->readProdID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 87
    :goto_0
    return-void

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click start button"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->Start()V

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click exit button"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->finish()V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x7f0b02dc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f03008a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->setContentView(I)V

    .line 33
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVICcheck;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->InitValues()V

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/UVICcheck;->InitWidgets()V

    .line 42
    return-void
.end method
