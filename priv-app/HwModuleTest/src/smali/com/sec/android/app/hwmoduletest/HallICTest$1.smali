.class Lcom/sec/android/app/hwmoduletest/HallICTest$1;
.super Landroid/os/Handler;
.source "HallICTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HallICTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HallICTest;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 171
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 173
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsPressedBackkey:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HallICTest;->access$002(Lcom/sec/android/app/hwmoduletest/HallICTest;Z)Z

    .line 174
    const-string v0, "HallICTest"

    const-string v1, "handleMessage"

    const-string v2, "KEY_TIMER_EXPIRED"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->CheckingFolderState()V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HallICTest;->mTimerHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/app/hwmoduletest/HallICTest;->MILLIS_IN_SEC:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
