.class Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;
.super Landroid/content/BroadcastReceiver;
.source "HwModuleTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HwModuleTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$000(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mBroadcastReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action= = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "JIG_CONNECTION_CHECK"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "JIG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->writeSOC()V
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$100(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    :cond_0
    monitor-exit p0

    return-void

    .line 142
    .end local v0    # "action":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
