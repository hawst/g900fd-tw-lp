.class Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;
.super Landroid/os/Handler;
.source "FactoryTestPhone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 393
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 411
    :goto_0
    return-void

    .line 395
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage"

    const-string v2, "Received Test NV Values from RIL"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 398
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage"

    const-string v2, "Received History NV Values from RIL"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 401
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage"

    const-string v2, "Item NV update success"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;->this$0:Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    # getter for: Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage"

    const-string v2, "MSG_DATA_CP_ACCELEROMETER"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 393
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0xce -> :sswitch_3
    .end sparse-switch
.end method
