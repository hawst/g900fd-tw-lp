.class public Lcom/sec/android/app/hwmoduletest/HealthSensorTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HealthSensorTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;
    }
.end annotation


# instance fields
.field private final MSG_UPDATE_SENSOR_VALUE:B

.field private final TIMER_TASK_PERIOD:I

.field private final WORK_HANDLER_THREAD:B

.field private final WORK_SENSOR_THREAD:B

.field private mBackground:Landroid/widget/LinearLayout;

.field private mECGSensorFwVerText:Landroid/widget/TextView;

.field private mECGSensorGraphButton:Landroid/widget/Button;

.field private mECGSensorTestButton:Landroid/widget/Button;

.field private mECGSensorTitleText:Landroid/widget/TextView;

.field private mECGSeparator:Landroid/view/View;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mGlucoseCheckButton:Landroid/widget/Button;

.field private mGlucoseSelfTestButton:Landroid/widget/Button;

.field private mGlucoseSeparator:Landroid/view/View;

.field private mGlucoseTitleText:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mIRThermometerDisplayButton:Landroid/widget/Button;

.field private mIRThermometerSensor:Landroid/hardware/Sensor;

.field private mIRThermometerSeparator:Landroid/view/View;

.field private mIRThermometerTestButton:Landroid/widget/Button;

.field private mIRThermometerTitleText:Landroid/widget/TextView;

.field private mIRThermometerVerText:Landroid/widget/TextView;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

.field mThreadGetData:Ljava/lang/Thread;

.field private final mTimer:Ljava/util/Timer;

.field private use_ECG:Z

.field private use_Glucose:Z

.field private use_IR_Thermometer:Z

.field private work_thread:B


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 108
    const-string v0, "HealthSensorTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 38
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->TIMER_TASK_PERIOD:I

    .line 41
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->MSG_UPDATE_SENSOR_VALUE:B

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_IR_Thermometer:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_Glucose:Z

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_ECG:Z

    .line 72
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mTimer:Ljava/util/Timer;

    .line 81
    iput-byte v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->WORK_SENSOR_THREAD:B

    .line 82
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->WORK_HANDLER_THREAD:B

    .line 83
    iput-byte v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mThreadGetData:Ljava/lang/Thread;

    .line 88
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mHandler:Landroid/os/Handler;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "HealthSensorTest()"

    const-string v2, "HealthSensorTest"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    .prologue
    .line 36
    iget-byte v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;B)B
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;
    .param p1, "x1"    # B

    .prologue
    .line 36
    iput-byte p1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->work_thread:B

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HealthSensorTest;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method private initialize()V
    .locals 15

    .prologue
    const v14, 0x7f0b0114

    const/4 v10, 0x1

    const/4 v13, 0x0

    const/16 v12, 0x8

    .line 198
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/Support$SensorTestMenu;->getSensorTest()[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 201
    .local v5, "menuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v0, v8, :cond_5

    .line 202
    add-int/lit8 v8, v0, 0x3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v8, v9, :cond_3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    :goto_1
    invoke-virtual {v5, v0, v8}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 205
    .local v4, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 206
    .local v3, "item":Ljava/lang/String;
    const-string v8, ","

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v2, v8, v13

    .line 207
    .local v2, "id":Ljava/lang/String;
    const-string v8, ","

    invoke-virtual {v3, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v7, v8, v10

    .line 209
    .local v7, "text":Ljava/lang/String;
    const-string v8, "IR_Thermometer"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 210
    iput-boolean v10, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_IR_Thermometer:Z

    .line 213
    :cond_1
    const-string v8, "Glucose"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 214
    iput-boolean v10, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_Glucose:Z

    .line 217
    :cond_2
    const-string v8, "ECG"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 218
    iput-boolean v10, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_ECG:Z

    goto :goto_2

    .line 202
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "item":Ljava/lang/String;
    .end local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "text":Ljava/lang/String;
    :cond_3
    add-int/lit8 v8, v0, 0x3

    goto :goto_1

    .line 201
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 223
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "initialize"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[IR Thermometer=>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_IR_Thermometer:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "[Glucose=>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_Glucose:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const v8, 0x7f0b000c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mBackground:Landroid/widget/LinearLayout;

    .line 229
    const v8, 0x7f0b0113

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTitleText:Landroid/widget/TextView;

    .line 230
    invoke-virtual {p0, v14}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerSeparator:Landroid/view/View;

    .line 231
    const v8, 0x7f0b0115

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;

    .line 232
    const v8, 0x7f0b0116

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTestButton:Landroid/widget/Button;

    .line 233
    const v8, 0x7f0b0117

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerDisplayButton:Landroid/widget/Button;

    .line 235
    iget-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_IR_Thermometer:Z

    if-nez v8, :cond_6

    .line 236
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTitleText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerSeparator:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTestButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 240
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerDisplayButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 244
    :cond_6
    const v8, 0x7f0b011c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTitleText:Landroid/widget/TextView;

    .line 245
    const v8, 0x7f0b0120

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorFwVerText:Landroid/widget/TextView;

    .line 246
    const v8, 0x7f0b011d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSeparator:Landroid/view/View;

    .line 247
    const v8, 0x7f0b011e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTestButton:Landroid/widget/Button;

    .line 248
    const v8, 0x7f0b011f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorGraphButton:Landroid/widget/Button;

    .line 250
    iget-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_ECG:Z

    if-nez v8, :cond_7

    .line 251
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTitleText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorFwVerText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSeparator:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 254
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTestButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 255
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorGraphButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 259
    :cond_7
    const v8, 0x7f0b0118

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    .line 260
    invoke-virtual {p0, v14}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSeparator:Landroid/view/View;

    .line 261
    const v8, 0x7f0b011a

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSelfTestButton:Landroid/widget/Button;

    .line 262
    const v8, 0x7f0b011b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseCheckButton:Landroid/widget/Button;

    .line 265
    iget-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->use_Glucose:Z

    if-nez v8, :cond_8

    .line 266
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSeparator:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->setVisibility(I)V

    .line 268
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSelfTestButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 269
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseCheckButton:Landroid/widget/Button;

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 273
    :cond_8
    new-instance v8, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HealthSensorTest;Lcom/sec/android/app/hwmoduletest/HealthSensorTest$1;)V

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    .line 274
    const-string v8, "sensor"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/hardware/SensorManager;

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 276
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v9, 0x1001e

    invoke-virtual {v8, v9}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerSensor:Landroid/hardware/Sensor;

    .line 279
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTestButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerDisplayButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSelfTestButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseCheckButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTestButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorGraphButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    const/16 v8, 0x9

    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getUIRate(I)F

    move-result v6

    .line 288
    .local v6, "rate":F
    const/4 v8, 0x0

    cmpl-float v8, v6, v8

    if-eqz v8, :cond_9

    const/high16 v8, 0x3f800000    # 1.0f

    cmpl-float v8, v6, v8

    if-eqz v8, :cond_9

    .line 290
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTitleText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 291
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerVerText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTestButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerTestButton:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 293
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerDisplayButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mIRThermometerDisplayButton:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 295
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 296
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSelfTestButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseSelfTestButton:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 297
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseCheckButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseCheckButton:Landroid/widget/Button;

    invoke-virtual {v9}, Landroid/widget/Button;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 300
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTitleText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 301
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorFwVerText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 302
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorTestButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 303
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorGraphButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mGlucoseTitleText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextSize()F

    move-result v9

    mul-float/2addr v9, v6

    invoke-virtual {v8, v13, v9}, Landroid/widget/Button;->setTextSize(IF)V

    .line 306
    :cond_9
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 195
    :goto_0
    :pswitch_0
    return-void

    .line 166
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 167
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 171
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 176
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 181
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/GlucoseCheck;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 182
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 185
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_5
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/ECG;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 186
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 189
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_6
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 163
    :pswitch_data_0
    .packed-switch 0x7f0b0116
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 115
    const v0, 0x7f030042

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->setContentView(I)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->initialize()V

    .line 120
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v6}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 121
    .local v6, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v0, 0x2e

    invoke-virtual {v6, v0}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 122
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mFormat:Ljava/text/DecimalFormat;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 125
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 157
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->access$500(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)V

    .line 151
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 129
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 131
    const-string v1, "ECG_SENSOR_FW_VERSION"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "fw_ver":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mECGSensorFwVerText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "VER : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mBackground:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HealthSensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->resume()V
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;->access$400(Lcom/sec/android/app/hwmoduletest/HealthSensorTest$SensorTask;)V

    .line 142
    return-void
.end method
