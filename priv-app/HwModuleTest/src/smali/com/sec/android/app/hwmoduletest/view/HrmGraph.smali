.class public Lcom/sec/android/app/hwmoduletest/view/HrmGraph;
.super Landroid/view/View;
.source "HrmGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private final GRAPH_SCALING_VALUE:F

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private MAX_VALUE_LIST:F

.field private MIN_VALUE_LIST:F

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mIR:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mIRPaint:Landroid/graphics/Paint;

.field private mPathIR:Landroid/graphics/Path;

.field private mPathRED:Landroid/graphics/Path;

.field private mRED:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mREDPaint:Landroid/graphics/Paint;

.field private mRectPaint:Landroid/graphics/Paint;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INCREASING_COOR:I

    .line 24
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->GRAPH_SCALING_VALUE:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    .line 38
    const-string v0, "HrmGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->init(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INCREASING_COOR:I

    .line 24
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->GRAPH_SCALING_VALUE:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    .line 38
    const-string v0, "HrmGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->init(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INCREASING_COOR:I

    .line 24
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->GRAPH_SCALING_VALUE:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    .line 38
    const-string v0, "HrmGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->init(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method private checkValue()V
    .locals 3

    .prologue
    .line 146
    const v1, 0x469c4000    # 20000.0f

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    .line 147
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    .line 148
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    .line 148
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    goto :goto_1

    .line 152
    :cond_2
    const/4 v0, 0x1

    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 153
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    .line 152
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 154
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    goto :goto_3

    .line 156
    :cond_5
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v6, 0x41f00000    # 30.0f

    const/high16 v5, -0x1000000

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mContext:Landroid/content/Context;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 59
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenWidth:I

    .line 60
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    .line 61
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    mul-int/lit8 v1, v1, 0x9

    div-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    .line 62
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->LIST_SIZE:I

    .line 63
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 64
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 69
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 74
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 79
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 84
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mDataPaint:Landroid/graphics/Paint;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mDataPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 89
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRectPaint:Landroid/graphics/Paint;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRectPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRectPaint:Landroid/graphics/Paint;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->invalidate()V

    .line 94
    return-void
.end method

.method private setPathIR()V
    .locals 8

    .prologue
    const v7, 0x3f4ccccd    # 0.8f

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 160
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 164
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x3

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->invalidate()V

    .line 169
    return-void
.end method

.method private setPathRED()V
    .locals 8

    .prologue
    const v7, 0x3f4ccccd    # 0.8f

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 173
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 176
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x3

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MAX_VALUE_LIST:F

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->MIN_VALUE_LIST:F

    sub-float/2addr v5, v6

    div-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    mul-float/2addr v1, v7

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->invalidate()V

    .line 181
    return-void
.end method


# virtual methods
.method public addValueIR(F)V
    .locals 2
    .param p1, "comp"    # F

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->checkValue()V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->setPathIR()V

    .line 133
    return-void
.end method

.method public addValueRED(F)V
    .locals 2
    .param p1, "comp"    # F

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->checkValue()V

    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->setPathRED()V

    .line 143
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v11, 0x428c0000    # 70.0f

    const/high16 v10, 0x42480000    # 50.0f

    const/high16 v9, 0x40400000    # 3.0f

    const/16 v5, 0xb4

    const/high16 v2, 0x42200000    # 40.0f

    .line 98
    const/4 v7, 0x0

    .line 99
    .local v7, "testIR":F
    const/4 v8, 0x0

    .line 102
    .local v8, "testRED":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRED:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 113
    :goto_0
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 114
    const-string v0, "IR :"

    const/high16 v1, 0x41200000    # 10.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v10, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 115
    const/high16 v1, 0x42dc0000    # 110.0f

    const/high16 v3, 0x43700000    # 240.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 116
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42dc0000    # 110.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 117
    const-string v0, "RED :"

    const/high16 v1, 0x43820000    # 260.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v10, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 118
    const/high16 v1, 0x43b90000    # 370.0f

    const/high16 v3, 0x43fa0000    # 500.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 119
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43b90000    # 370.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathIR:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIRPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mPathRED:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mREDPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 122
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0x3

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mScreenHeight:I

    add-int/lit8 v0, v0, -0x3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mRectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    move v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 123
    return-void

    .line 104
    :catch_0
    move-exception v6

    .line 105
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    const-string v3, "get value fail"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size of IR, RED : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->mIR:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LIST_SIZE value : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->LIST_SIZE:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
