.class Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;
.super Landroid/widget/ArrayAdapter;
.source "GestureTestMode2Display.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/HistoryItem;",
        ">;"
    }
.end annotation


# instance fields
.field private ISWVGA800:Z

.field txt_item1:Landroid/widget/TextView;

.field txt_item2:Landroid/widget/TextView;

.field txt_item3:Landroid/widget/TextView;

.field txt_item4:Landroid/widget/TextView;

.field txt_item5:Landroid/widget/TextView;

.field txt_item6:Landroid/widget/TextView;

.field txt_item7:Landroid/widget/TextView;

.field txt_item8:Landroid/widget/TextView;

.field private vendor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->ISWVGA800:Z

    .line 318
    const-string v0, "GESTURE_SENSOR_VENDOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->vendor:Ljava/lang/String;

    .line 335
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 338
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->getItem(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/hwmoduletest/HistoryItem;

    .line 341
    .local v16, "item":Lcom/sec/android/app/hwmoduletest/HistoryItem;
    if-nez p2, :cond_2

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->getContext()Landroid/content/Context;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v18

    .line 343
    .local v18, "li":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->ISWVGA800:Z

    move/from16 v22, v0

    if-eqz v22, :cond_1

    .line 344
    const v22, 0x7f03002d

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, p3

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 352
    .end local v18    # "li":Landroid/view/LayoutInflater;
    .local v17, "layout":Landroid/widget/LinearLayout;
    :goto_0
    const v22, 0x7f0b00a7

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item1:Landroid/widget/TextView;

    .line 353
    const v22, 0x7f0b00a8

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item2:Landroid/widget/TextView;

    .line 354
    const v22, 0x7f0b00a9

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item3:Landroid/widget/TextView;

    .line 355
    const v22, 0x7f0b00aa

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item4:Landroid/widget/TextView;

    .line 356
    const v22, 0x7f0b00ab

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item5:Landroid/widget/TextView;

    .line 357
    const v22, 0x7f0b00ac

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item6:Landroid/widget/TextView;

    .line 358
    const v22, 0x7f0b00ad

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item7:Landroid/widget/TextView;

    .line 359
    const v22, 0x7f0b00ae

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item8:Landroid/widget/TextView;

    .line 361
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getCount()Ljava/lang/String;

    move-result-object v8

    .line 362
    .local v8, "countString":Ljava/lang/String;
    const v22, 0x7f0b00a6

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 363
    .local v9, "countView":Landroid/widget/TextView;
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    const-string v22, "MAXIM"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->vendor:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item5:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item6:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item7:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 369
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item8:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    .line 371
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataA()Ljava/lang/String;

    move-result-object v6

    .line 372
    .local v6, "Zdelta":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataB()Ljava/lang/String;

    move-result-object v21

    .line 373
    .local v21, "peak2peak":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataC()Ljava/lang/String;

    move-result-object v5

    .line 374
    .local v5, "Validcnt":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataD()Ljava/lang/String;

    move-result-object v4

    .line 376
    .local v4, "Angle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item1:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item2:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item3:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item4:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item1:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/high16 v23, -0x10000

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item2:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/high16 v23, -0x10000

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item3:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item4:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 422
    .end local v4    # "Angle":Ljava/lang/String;
    .end local v5    # "Validcnt":Ljava/lang/String;
    .end local v6    # "Zdelta":Ljava/lang/String;
    .end local v21    # "peak2peak":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v17

    .line 346
    .end local v8    # "countString":Ljava/lang/String;
    .end local v9    # "countView":Landroid/widget/TextView;
    .end local v17    # "layout":Landroid/widget/LinearLayout;
    .restart local v18    # "li":Landroid/view/LayoutInflater;
    :cond_1
    const v22, 0x7f03002c

    const/16 v23, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, p3

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .restart local v17    # "layout":Landroid/widget/LinearLayout;
    goto/16 :goto_0

    .end local v17    # "layout":Landroid/widget/LinearLayout;
    .end local v18    # "li":Landroid/view/LayoutInflater;
    :cond_2
    move-object/from16 v17, p2

    .line 349
    check-cast v17, Landroid/widget/LinearLayout;

    .restart local v17    # "layout":Landroid/widget/LinearLayout;
    goto/16 :goto_0

    .line 385
    .restart local v8    # "countString":Ljava/lang/String;
    .restart local v9    # "countView":Landroid/widget/TextView;
    :cond_3
    const-string v22, "AMS"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->vendor:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 387
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataA()Ljava/lang/String;

    move-result-object v20

    .line 388
    .local v20, "nswe_gesture":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataC()Ljava/lang/String;

    move-result-object v10

    .line 389
    .local v10, "cross_angle":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataH()Ljava/lang/String;

    move-result-object v7

    .line 390
    .local v7, "count":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataB()Ljava/lang/String;

    move-result-object v19

    .line 391
    .local v19, "max_sum_nswe":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataD()Ljava/lang/String;

    move-result-object v12

    .line 392
    .local v12, "enter_angle":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataE()Ljava/lang/String;

    move-result-object v14

    .line 393
    .local v14, "exit_angle":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataF()Ljava/lang/String;

    move-result-object v13

    .line 394
    .local v13, "enter_mag":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/hwmoduletest/HistoryItem;->getValueRawDataG()Ljava/lang/String;

    move-result-object v15

    .line 396
    .local v15, "exit_mag":Ljava/lang/String;
    const-string v22, "5"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    const-string v11, "U"

    .line 403
    .local v11, "directionStr":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item1:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item2:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item3:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item4:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item5:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item6:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item7:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item8:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item1:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/high16 v23, -0x10000

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item2:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/high16 v23, -0x10000

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item3:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/high16 v23, -0x10000

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item4:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item5:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item6:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item7:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->txt_item8:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const v23, -0xffff01

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 396
    .end local v11    # "directionStr":Ljava/lang/String;
    :cond_4
    const-string v22, "4"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    const-string v11, "D"

    goto/16 :goto_2

    :cond_5
    const-string v22, "3"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    const-string v11, "L"

    goto/16 :goto_2

    :cond_6
    const-string v22, "2"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    const-string v11, "R"

    goto/16 :goto_2

    :cond_7
    const-string v11, "N"

    goto/16 :goto_2
.end method

.method public setISWVGA800(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 330
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->ISWVGA800:Z

    .line 331
    return-void
.end method
