.class public final Lcom/sec/android/app/hwmoduletest/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final acc_cal:I = 0x7f030000

.field public static final acc_graph_activity:I = 0x7f030001

.field public static final acc_image_test:I = 0x7f030002

.field public static final accelerometer_sensor:I = 0x7f030003

.field public static final activity_beam:I = 0x7f030004

.field public static final airmotion_test:I = 0x7f030005

.field public static final autoanswering:I = 0x7f030006

.field public static final barcode_emul_test:I = 0x7f030007

.field public static final barometer_self_test:I = 0x7f030008

.field public static final barometer_waterproof_test:I = 0x7f030009

.field public static final barometer_waterproof_test_list_item:I = 0x7f03000a

.field public static final battery_status:I = 0x7f03000b

.field public static final battery_status_kor:I = 0x7f03000c

.field public static final battery_status_type2:I = 0x7f03000d

.field public static final bluetooth_test:I = 0x7f03000e

.field public static final brightness:I = 0x7f03000f

.field public static final brightness_custom_dialog:I = 0x7f030010

.field public static final build_info:I = 0x7f030011

.field public static final cabc_test:I = 0x7f030012

.field public static final camera_image_view:I = 0x7f030013

.field public static final cpu_version:I = 0x7f030014

.field public static final ecg:I = 0x7f030015

.field public static final ecg_graph_activity:I = 0x7f030016

.field public static final epen_test:I = 0x7f030017

.field public static final factory_reset:I = 0x7f030018

.field public static final failhist:I = 0x7f030019

.field public static final fingerprint_test:I = 0x7f03001a

.field public static final fragment_fingerprint:I = 0x7f03001b

.field public static final fta_hw_version:I = 0x7f03001c

.field public static final fta_sw_version:I = 0x7f03001d

.field public static final fuel_gauge_register:I = 0x7f03001e

.field public static final gesture_sensor:I = 0x7f03001f

.field public static final gesture_test_count_delta:I = 0x7f030020

.field public static final gesture_test_count_delta_display:I = 0x7f030021

.field public static final gesture_test_count_delta_display_item:I = 0x7f030022

.field public static final gesture_test_main:I = 0x7f030023

.field public static final gesture_test_mode1:I = 0x7f030024

.field public static final gesture_test_mode1_display:I = 0x7f030025

.field public static final gesture_test_mode1_display_item:I = 0x7f030026

.field public static final gesture_test_mode1_fragment:I = 0x7f030027

.field public static final gesture_test_mode1_fragment_item:I = 0x7f030028

.field public static final gesture_test_mode2:I = 0x7f030029

.field public static final gesture_test_mode2_display:I = 0x7f03002a

.field public static final gesture_test_mode2_display_480:I = 0x7f03002b

.field public static final gesture_test_mode2_display_item:I = 0x7f03002c

.field public static final gesture_test_mode2_display_item_480:I = 0x7f03002d

.field public static final glucose_check:I = 0x7f03002e

.field public static final glucose_selftest:I = 0x7f03002f

.field public static final gripsensor:I = 0x7f030030

.field public static final gripsensor_cal:I = 0x7f030031

.field public static final gyroscope_bosch:I = 0x7f030032

.field public static final gyroscope_display:I = 0x7f030033

.field public static final gyroscope_display_item:I = 0x7f030034

.field public static final gyroscope_graph_activity:I = 0x7f030035

.field public static final gyroscope_icstmicro:I = 0x7f030036

.field public static final gyroscope_icstmicro_tablet:I = 0x7f030037

.field public static final gyroscope_invensense:I = 0x7f030038

.field public static final gyroscope_maxim:I = 0x7f030039

.field public static final gyroscope_ois:I = 0x7f03003a

.field public static final gyroscope_self_test:I = 0x7f03003b

.field public static final gyroscope_sensor_test_m:I = 0x7f03003c

.field public static final hallictest:I = 0x7f03003d

.field public static final hdcpcheckdialog:I = 0x7f03003e

.field public static final hdmi:I = 0x7f03003f

.field public static final hdmi_capri:I = 0x7f030040

.field public static final hdmitest:I = 0x7f030041

.field public static final health_sensor_test:I = 0x7f030042

.field public static final hrm_eoltest:I = 0x7f030043

.field public static final hrmtest:I = 0x7f030044

.field public static final hw_id:I = 0x7f030045

.field public static final hwmoduletest:I = 0x7f030046

.field public static final irled_test:I = 0x7f030047

.field public static final irthermometer_display:I = 0x7f030048

.field public static final irthermometer_display_item:I = 0x7f030049

.field public static final irthermometer_test:I = 0x7f03004a

.field public static final item:I = 0x7f03004b

.field public static final lcd_type_info:I = 0x7f03004c

.field public static final led_test:I = 0x7f03004d

.field public static final light_sensor_read_test:I = 0x7f03004e

.field public static final lightsensor:I = 0x7f03004f

.field public static final lightsensor_main:I = 0x7f030050

.field public static final loopback_test:I = 0x7f030051

.field public static final low_frequency_test:I = 0x7f030052

.field public static final magnetic_alps:I = 0x7f030053

.field public static final magnetic_asahi:I = 0x7f030054

.field public static final magnetic_bosch:I = 0x7f030055

.field public static final magnetic_stmicro:I = 0x7f030056

.field public static final magnetic_yamaha:I = 0x7f030057

.field public static final main_version:I = 0x7f030058

.field public static final melody_test:I = 0x7f030059

.field public static final pcm_dump:I = 0x7f03005a

.field public static final phoneutil_c1via:I = 0x7f03005b

.field public static final phoneutil_esc:I = 0x7f03005c

.field public static final phoneutil_eur:I = 0x7f03005d

.field public static final phoneutil_prevail2spr:I = 0x7f03005e

.field public static final phoneutil_sglte:I = 0x7f03005f

.field public static final phoneutil_td:I = 0x7f030060

.field public static final power_noise_graph_activity:I = 0x7f030061

.field public static final power_status:I = 0x7f030062

.field public static final proximity:I = 0x7f030063

.field public static final proximity_offset:I = 0x7f030064

.field public static final proximity_offset_sharp:I = 0x7f030065

.field public static final proximity_offset_test:I = 0x7f030066

.field public static final proximity_offset_tmd27723:I = 0x7f030067

.field public static final proximity_test:I = 0x7f030068

.field public static final receiver_dual:I = 0x7f030069

.field public static final receiver_multicnt:I = 0x7f03006a

.field public static final rtc:I = 0x7f03006b

.field public static final securejt:I = 0x7f03006c

.field public static final self_test_listview_item:I = 0x7f03006d

.field public static final self_test_main:I = 0x7f03006e

.field public static final sensor_test:I = 0x7f03006f

.field public static final sensorhub_test:I = 0x7f030070

.field public static final shutdown:I = 0x7f030071

.field public static final simple_version:I = 0x7f030072

.field public static final spen_detection_test:I = 0x7f030073

.field public static final svc_led_test:I = 0x7f030074

.field public static final sys_dump_main:I = 0x7f030075

.field public static final temp_humid_test:I = 0x7f030076

.field public static final temphumid_display:I = 0x7f030077

.field public static final temphumid_display_item:I = 0x7f030078

.field public static final temphumid_graph_activity:I = 0x7f030079

.field public static final temphumid_thermistor:I = 0x7f03007a

.field public static final temphumid_thermistor_item:I = 0x7f03007b

.field public static final touch_firmware:I = 0x7f03007c

.field public static final touch_key_sensitivity:I = 0x7f03007d

.field public static final touch_key_sensitivity_graph:I = 0x7f03007e

.field public static final touch_key_sensitivity_view:I = 0x7f03007f

.field public static final touch_selftest:I = 0x7f030080

.field public static final touch_test_fail:I = 0x7f030081

.field public static final touch_test_pass:I = 0x7f030082

.field public static final tsp_fail_pop:I = 0x7f030083

.field public static final ui_heartratemeasure:I = 0x7f030084

.field public static final usblogging:I = 0x7f030085

.field public static final usbpathswitch:I = 0x7f030086

.field public static final usbsetting:I = 0x7f030087

.field public static final uv_display:I = 0x7f030088

.field public static final uv_display_item:I = 0x7f030089

.field public static final uv_ic_check:I = 0x7f03008a

.field public static final uv_test:I = 0x7f03008b

.field public static final vibration_test:I = 0x7f03008c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
