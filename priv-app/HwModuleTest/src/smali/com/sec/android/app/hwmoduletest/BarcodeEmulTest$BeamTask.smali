.class public Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;
.super Ljava/lang/Object;
.source "BarcodeEmulTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BeamTask"
.end annotation


# static fields
.field private static fromHex:[B

.field private static toHex:[C


# instance fields
.field data:Ljava/lang/String;

.field seq:[B

.field symbology:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-string v0, "0123456789abcdef"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->toHex:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "symbology"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "seqHex"    # Ljava/lang/String;

    .prologue
    .line 90
    const-string v0, "\\s"

    const-string v1, ""

    invoke-virtual {p3, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->parseHexString(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "symbology"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .param p3, "seq"    # [B

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->symbology:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->data:Ljava/lang/String;

    .line 86
    iput-object p3, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->seq:[B

    .line 87
    return-void
.end method

.method private static declared-synchronized getHexTable()[B
    .locals 9

    .prologue
    .line 114
    const-class v7, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    monitor-enter v7

    :try_start_0
    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B

    if-nez v6, :cond_1

    .line 115
    const/16 v6, 0x100

    new-array v6, v6, [B

    sput-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B

    .line 116
    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B

    const/4 v8, -0x1

    invoke-static {v6, v8}, Ljava/util/Arrays;->fill([BB)V

    .line 117
    const/4 v4, 0x0

    .line 119
    .local v4, "pos":I
    sget-object v0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->toHex:[C

    .local v0, "arr$":[C
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v5, v4

    .end local v4    # "pos":I
    .local v5, "pos":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-char v1, v0, v2

    .line 120
    .local v1, "c":C
    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "pos":I
    .restart local v4    # "pos":I
    int-to-byte v8, v5

    aput-byte v8, v6, v1

    .line 119
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "pos":I
    .restart local v5    # "pos":I
    goto :goto_0

    .line 123
    .end local v1    # "c":C
    :cond_0
    const/16 v4, 0xa

    .line 125
    .end local v5    # "pos":I
    .restart local v4    # "pos":I
    const-string v6, "ABCDEF"

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    move v5, v4

    .end local v4    # "pos":I
    .restart local v5    # "pos":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget-char v1, v0, v2

    .line 126
    .restart local v1    # "c":C
    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "pos":I
    .restart local v4    # "pos":I
    int-to-byte v8, v5

    aput-byte v8, v6, v1

    .line 125
    add-int/lit8 v2, v2, 0x1

    move v5, v4

    .end local v4    # "pos":I
    .restart local v5    # "pos":I
    goto :goto_1

    .line 130
    .end local v1    # "c":C
    :cond_1
    sget-object v6, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->fromHex:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-object v6

    .line 114
    .end local v5    # "pos":I
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6
.end method

.method public static parseHexString(Ljava/lang/String;)[B
    .locals 11
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 134
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_0

    .line 135
    new-instance v8, Ljava/lang/RuntimeException;

    const-string v9, "Invalid hex string - odd length."

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 138
    :cond_0
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->getHexTable()[B

    move-result-object v3

    .line 139
    .local v3, "hexTable":[B
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 140
    .local v7, "x":[C
    array-length v8, v7

    ushr-int/lit8 v8, v8, 0x1

    new-array v2, v8, [B

    .line 141
    .local v2, "buf":[B
    const/4 v4, 0x0

    .line 143
    .local v4, "i":I
    const/4 v6, 0x0

    .local v6, "pos":I
    :goto_0
    array-length v8, v2

    if-ge v6, v8, :cond_3

    .line 144
    add-int/lit8 v5, v4, 0x1

    .end local v4    # "i":I
    .local v5, "i":I
    aget-char v8, v7, v4

    aget-byte v0, v3, v8

    .local v0, "a":B
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    aget-char v8, v7, v5

    aget-byte v1, v3, v8

    .line 146
    .local v1, "b":B
    if-ltz v0, :cond_1

    if-gez v1, :cond_2

    .line 147
    :cond_1
    new-instance v8, Ljava/lang/NumberFormatException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid hex characters: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v4, -0x2

    aget-char v10, v7, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v10, v4, -0x1

    aget-char v10, v7, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 150
    :cond_2
    shl-int/lit8 v8, v0, 0x4

    or-int/2addr v8, v1

    int-to-byte v8, v8

    aput-byte v8, v2, v6

    .line 143
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 153
    .end local v0    # "a":B
    .end local v1    # "b":B
    :cond_3
    return-object v2
.end method


# virtual methods
.method public toHexString([B)Ljava/lang/String;
    .locals 1
    .param p1, "b"    # [B

    .prologue
    .line 99
    array-length v0, p1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->toHexString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toHexString([BI)Ljava/lang/String;
    .locals 6
    .param p1, "b"    # [B
    .param p2, "size"    # I

    .prologue
    .line 103
    shl-int/lit8 v4, p2, 0x1

    new-array v0, v4, [C

    .line 105
    .local v0, "c":[C
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "pos":I
    move v3, v2

    .end local v2    # "pos":I
    .local v3, "pos":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 106
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "pos":I
    .restart local v2    # "pos":I
    sget-object v4, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->toHex:[C

    aget-byte v5, p1, v1

    shr-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v3

    .line 107
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "pos":I
    .restart local v3    # "pos":I
    sget-object v4, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->toHex:[C

    aget-byte v5, p1, v1

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    aput-char v4, v0, v2

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;->symbology:Ljava/lang/String;

    return-object v0
.end method
