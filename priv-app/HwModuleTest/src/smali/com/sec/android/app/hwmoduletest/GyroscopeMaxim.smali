.class public Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeMaxim.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mGyroSpec_Max:I

.field private mGyroSpec_Min:I

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_HW_Self_X:Landroid/widget/TextView;

.field private mText_HW_Self_Y:Landroid/widget/TextView;

.field private mText_HW_Self_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Noise_Bias_X:Landroid/widget/TextView;

.field private mText_Noise_Bias_Y:Landroid/widget/TextView;

.field private mText_Noise_Bias_Z:Landroid/widget/TextView;

.field private mText_Temperature:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "GyroscopeMaxim"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mSenserID:[I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->WHAT_UPDATE:I

    .line 32
    const-string v0, "GyroscopeMaxim"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mHandler:Landroid/os/Handler;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->update()V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 72
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Initialized:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 75
    const v0, 0x7f0b00f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Temperature:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0b00f2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_X:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0b00f3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0b00f8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_X:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0b00f9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_Y:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0b00fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_Z:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTextResult:Landroid/widget/TextView;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTableRow_Initialized:Landroid/widget/TableRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 87
    return-void
.end method

.method private update()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x7

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 90
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, v5, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    .line 91
    .local v1, "feature":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "feature : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "data":[Ljava/lang/String;
    const-string v4, ""

    .line 94
    .local v4, "tempdata":Ljava/lang/String;
    const/4 v2, 0x1

    .line 97
    .local v2, "isPass":Z
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    sget v6, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_4

    .line 100
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Temperature : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Temperature:Landroid/widget/TextView;

    aget-object v6, v0, v10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :goto_0
    const-string v5, "GYRO_SENSOR_SELFTEST"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 108
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_7

    .line 111
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Noise Bias : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_X:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    aget-object v6, v0, v10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    aget-object v6, v0, v11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HW Self Test(%) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x9

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_X:Landroid/widget/TextView;

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_Y:Landroid/widget/TextView;

    aget-object v6, v0, v13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_HW_Self_Z:Landroid/widget/TextView;

    const/16 v6, 0x9

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ReturnValue : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v5, "GYROSCOPE_SELFTEST_MIN"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Min:I

    .line 128
    const-string v5, "GYROSCOPE_SELFTEST_MAX"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Max:I

    .line 130
    if-ne v2, v9, :cond_0

    aget-object v5, v0, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    aget-object v5, v0, v10

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    aget-object v5, v0, v11

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_0

    aget-object v5, v0, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_0

    aget-object v5, v0, v10

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_0

    aget-object v5, v0, v11

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-gez v5, :cond_1

    .line 134
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "spec out"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Noise Bias : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v2, 0x0

    .line 145
    :cond_1
    const/16 v3, 0x32

    .line 146
    .local v3, "specHwSelfTest":I
    if-ne v2, v9, :cond_3

    int-to-float v5, v3

    aget-object v6, v0, v12

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-lez v5, :cond_2

    int-to-float v5, v3

    aget-object v6, v0, v13

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-lez v5, :cond_2

    int-to-float v5, v3

    const/16 v6, 0x9

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_3

    .line 150
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "spec out"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HW Self Test : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x9

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v2, 0x0

    .line 155
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    const-string v5, "PASS"

    :goto_1
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    const v5, -0xffff01

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 164
    .end local v3    # "specHwSelfTest":I
    :goto_3
    return-void

    .line 103
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Temperature:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 155
    .restart local v3    # "specHwSelfTest":I
    :cond_5
    const-string v5, "FAIL"

    goto :goto_1

    .line 156
    :cond_6
    const/high16 v5, -0x10000

    goto :goto_2

    .line 158
    .end local v3    # "specHwSelfTest":I
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_X:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTextResult:Landroid/widget/TextView;

    const-string v6, "FAIL"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mTextResult:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 40
    const v0, 0x7f030039

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->setContentView(I)V

    .line 41
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mFeature:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->init()V

    .line 44
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 60
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 47
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 48
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mSenserID:[I

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeMaxim;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 55
    return-void
.end method
