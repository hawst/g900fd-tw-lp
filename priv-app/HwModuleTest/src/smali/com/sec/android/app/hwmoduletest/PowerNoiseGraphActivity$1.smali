.class Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;
.super Landroid/os/Handler;
.source "PowerNoiseGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->mPowerNoiseGraph:Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$300(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[F

    move-result-object v2

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[F

    move-result-object v3

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->power_noise:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->raw_data:[F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)[F

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->retSelftest:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$200(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/hwmoduletest/view/PowerNoiseGraph;->addValueWithPowerNoise(FFF[Ljava/lang/String;[FI)V

    .line 101
    return-void
.end method
