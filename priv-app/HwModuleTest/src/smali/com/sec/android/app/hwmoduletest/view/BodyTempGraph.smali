.class public Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;
.super Landroid/view/View;
.source "BodyTempGraph.java"


# static fields
.field private static final vendor:Ljava/lang/String;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final GRAPH_SCALING_COOR:F

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private final mComp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mCompPaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPathComp:Landroid/graphics/Path;

.field private mPathTa:Landroid/graphics/Path;

.field private mPathTemp:Landroid/graphics/Path;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private final mTa:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mTaPaint:Landroid/graphics/Paint;

.field private final mTemp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mTempPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "IR_THERMOMETER_SENSOR_VENDOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->vendor:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_X:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INCREASING_COOR:I

    .line 24
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->GRAPH_SCALING_COOR:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    .line 41
    const-string v0, "BodyTempGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->CLASS_NAME:Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->init(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_X:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INCREASING_COOR:I

    .line 24
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->GRAPH_SCALING_COOR:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    .line 41
    const-string v0, "BodyTempGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->CLASS_NAME:Ljava/lang/String;

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->init(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_X:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    .line 23
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INCREASING_COOR:I

    .line 24
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->GRAPH_SCALING_COOR:F

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    .line 32
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    .line 41
    const-string v0, "BodyTempGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->CLASS_NAME:Ljava/lang/String;

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x40800000    # 4.0f

    const/4 v3, 0x1

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 62
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mScreenWidth:I

    .line 63
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mScreenHeight:I

    .line 64
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mScreenHeight:I

    div-int/lit8 v1, v1, 0x2

    add-int/lit16 v1, v1, 0xc8

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    .line 65
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    div-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->LIST_SIZE:I

    .line 66
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 67
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 72
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 77
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 82
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 87
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->invalidate()V

    .line 93
    return-void
.end method

.method private setPath()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v5, 0x41000000    # 8.0f

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 156
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 158
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 160
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v3, v1

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v3, v1

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v3, v1

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 166
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->invalidate()V

    .line 176
    return-void
.end method


# virtual methods
.method public addValueTemp(FFF)V
    .locals 3
    .param p1, "ta"    # F
    .param p2, "tocomp"    # F
    .param p3, "bodytemp"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 137
    cmpl-float v0, p3, v1

    if-ltz v0, :cond_1

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 151
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->setPath()V

    .line 152
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v13, 0x44098000    # 550.0f

    const v1, 0x44048000    # 530.0f

    const/high16 v12, 0x41200000    # 10.0f

    const v11, 0x7f0800a9

    const/high16 v10, 0x439b0000    # 310.0f

    .line 97
    const/4 v9, 0x0

    .line 98
    .local v9, "textTemp":F
    const/4 v7, 0x0

    .line 99
    .local v7, "textComp":F
    const/4 v8, 0x0

    .line 102
    .local v8, "textTa":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 112
    :goto_0
    const/high16 v0, -0x1000000

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Body temp.("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x43160000    # 150.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 114
    const/high16 v2, 0x430c0000    # 140.0f

    const/high16 v3, 0x44250000    # 660.0f

    const/high16 v4, 0x430c0000    # 140.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 115
    invoke-static {v9}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x432a0000    # 170.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v13, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 116
    const-string v0, "PARTRON"

    sget-object v2, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->vendor:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "To.("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x43520000    # 210.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 122
    :goto_1
    const/high16 v2, 0x43480000    # 200.0f

    const/high16 v3, 0x44250000    # 660.0f

    const/high16 v4, 0x43480000    # 200.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 123
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x43660000    # 230.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v13, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ta.("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x43870000    # 270.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 125
    const/high16 v2, 0x43820000    # 260.0f

    const/high16 v3, 0x44250000    # 660.0f

    const/high16 v4, 0x43820000    # 260.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 126
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43910000    # 290.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v13, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 127
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, -0x1f4

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, 0xc8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v12

    move v3, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 129
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTemp:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTempPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathComp:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mCompPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mPathTa:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTaPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 134
    return-void

    .line 105
    :catch_0
    move-exception v6

    .line 106
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "BodyTempGraph"

    const-string v2, "onDraw"

    const-string v3, "get value fail"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "BodyTempGraph"

    const-string v2, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size of Temp, Comp, Ta : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mComp:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mTa:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v0, "BodyTempGraph"

    const-string v2, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LIST_SIZE value : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->LIST_SIZE:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "To comp.("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v2, 0x43520000    # 210.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v10, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method
