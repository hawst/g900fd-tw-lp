.class Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;
.super Ljava/lang/Object;
.source "GestureTestMode1Fragment.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 161
    iget-object v1, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x3

    aget v0, v1, v2

    .line 162
    .local v0, "zmean":F
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)I

    .line 163
    const-string v1, "GestureTestMode1Fragment"

    const-string v2, "listenner: getSensorData()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ", zmean = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mValueList:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)I

    move-result v4

    invoke-direct {v3, v4, v0}, Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;-><init>(IF)V

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;->notifyDataSetChanged()V

    .line 167
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$300()Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 168
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(F)V

    .line 169
    const-string v1, "GestureTestMode1Fragment"

    const-string v2, "onGetSensorHubData"

    const-string v3, "write to log file "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_0
    return-void
.end method
