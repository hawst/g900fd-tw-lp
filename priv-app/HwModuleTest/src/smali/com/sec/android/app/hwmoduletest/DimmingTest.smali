.class public Lcom/sec/android/app/hwmoduletest/DimmingTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "DimmingTest.java"


# instance fields
.field private isDimming:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    const-string v0, "DimmingTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->isDimming:Z

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->mPowerManager:Landroid/os/PowerManager;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->resources:Landroid/content/res/Resources;

    .line 23
    return-void
.end method

.method private setBrightness(I)V
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->mPowerManager:Landroid/os/PowerManager;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f02000e

    .line 33
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->resources:Landroid/content/res/Resources;

    .line 35
    const/4 v1, 0x0

    .line 36
    .local v1, "hasCocktailBar":Z
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.feature.cocktailbar"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 37
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "hasCocktailBar="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    if-eqz v1, :cond_0

    .line 39
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "background2ndLayout":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/sec/android/app/dummy/SlookCocktailSubWindow;->setSubContentView(Landroid/app/Activity;Landroid/view/View;)V

    .line 41
    invoke-virtual {v0, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 43
    .end local v0    # "background2ndLayout":Landroid/view/View;
    :cond_0
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 44
    .local v2, "view":Landroid/view/View;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 45
    invoke-virtual {v2, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 46
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->setContentView(Landroid/view/View;)V

    .line 47
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->setBrightness(I)V

    .line 65
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 66
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 51
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->isDimming:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "screen_brightness"

    const/16 v4, 0xff

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 54
    .local v0, "brightness":I
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->isDimming:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->isDimming:Z

    .line 55
    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/DimmingTest;->setBrightness(I)V

    .line 56
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "onTouchEvent"

    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->isDimming:Z

    if-eqz v1, :cond_3

    const-string v1, "DIM"

    :goto_2
    invoke-static {v3, v4, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .end local v0    # "brightness":I
    :cond_0
    return v2

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/DimmingTest;->resources:Landroid/content/res/Resources;

    const v3, 0x10e0050

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_0

    .line 54
    .restart local v0    # "brightness":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 56
    :cond_3
    const-string v1, "NO DIM"

    goto :goto_2
.end method
