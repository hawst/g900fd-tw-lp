.class public Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;
.super Landroid/view/View;
.source "TouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mNonClickPaint:Landroid/graphics/Paint;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/TouchTest;Landroid/content/Context;)V
    .locals 6
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 192
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    .line 193
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 175
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    .line 176
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    .line 177
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    .line 178
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    .line 194
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->setKeepScreenOn(Z)V

    .line 195
    const-string v2, "window"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 197
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 198
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 199
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    .line 200
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    .line 201
    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$000(Lcom/sec/android/app/hwmoduletest/TouchTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "MyView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Screen size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 205
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 207
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 208
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->setPaint()V

    .line 210
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->initRect()V

    .line 211
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    .line 212
    return-void
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 340
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 342
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 343
    float-to-int v6, p1

    .line 344
    float-to-int v8, p3

    .line 350
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 351
    float-to-int v7, p2

    .line 352
    float-to-int v9, p4

    .line 358
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 359
    return-void

    .line 346
    :cond_0
    float-to-int v6, p3

    .line 347
    float-to-int v8, p1

    goto :goto_0

    .line 354
    :cond_1
    float-to-int v7, p4

    .line 355
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 363
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 364
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 402
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 403
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 404
    .local v9, "col_width":F
    div-float v0, p1, v9

    float-to-int v10, v0

    .line 405
    .local v10, "countX":I
    div-float v0, p2, v8

    float-to-int v11, v0

    .line 406
    .local v11, "countY":I
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 407
    .local v6, "ColX":F
    int-to-float v0, v11

    mul-float v7, v8, v0

    .line 410
    .local v7, "ColY":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v11, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v10, v0, :cond_2

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$600(Lcom/sec/android/app/hwmoduletest/TouchTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "Out of bounds"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :cond_1
    :goto_0
    return-void

    .line 415
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$400(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-nez v0, :cond_3

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$400(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$400(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$700(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-eqz v0, :cond_4

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 426
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 430
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/TouchTest;->isPass()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$800(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->iscompleted:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$900(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->iscompleted:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$902(Lcom/sec/android/app/hwmoduletest/TouchTest;Z)Z

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    const-class v3, Lcom/sec/android/app/hwmoduletest/TouchTestPass;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TouchTest;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 422
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private draw_down(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    .line 297
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    .line 298
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    .line 300
    return-void
.end method

.method private draw_move(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 303
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    if-eqz v1, :cond_1

    .line 304
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 305
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    .line 306
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    .line 307
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    .line 308
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    .line 309
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 310
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawLine(FFFF)V

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 313
    :cond_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    .line 314
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    .line 315
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    .line 316
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    .line 317
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 318
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawLine(FFFF)V

    .line 319
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    .line 321
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private draw_up(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 324
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 325
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    .line 326
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    .line 328
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    .line 330
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 331
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->drawPoint(FF)V

    .line 334
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->isTouchDown:Z

    .line 336
    :cond_1
    return-void
.end method

.method private initRect()V
    .locals 19

    .prologue
    .line 367
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v2

    int-to-float v2, v2

    div-float v15, v1, v2

    .line 368
    .local v15, "col_height":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v2

    int-to-float v2, v2

    div-float v16, v1, v2

    .line 369
    .local v16, "col_width":F
    const/4 v13, 0x0

    .line 370
    .local v13, "ColX":I
    const/4 v14, 0x0

    .line 371
    .local v14, "ColY":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 372
    .local v6, "mRectPaint":Landroid/graphics/Paint;
    const/high16 v1, -0x1000000

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 374
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    move/from16 v0, v17

    if-ge v0, v1, :cond_1

    .line 375
    move/from16 v0, v17

    int-to-float v1, v0

    mul-float/2addr v1, v15

    float-to-int v14, v1

    .line 377
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    move/from16 v0, v18

    if-ge v0, v1, :cond_0

    .line 378
    move/from16 v0, v18

    int-to-float v1, v0

    mul-float v1, v1, v16

    float-to-int v13, v1

    .line 379
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    int-to-float v4, v4

    int-to-float v5, v14

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 380
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    int-to-float v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 381
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->draw:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$400(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v1

    aget-object v1, v1, v17

    const/4 v2, 0x0

    aput-boolean v2, v1, v18

    .line 382
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->click:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$500(Lcom/sec/android/app/hwmoduletest/TouchTest;)[[Z

    move-result-object v1

    aget-object v1, v1, v17

    const/4 v2, 0x0

    aput-boolean v2, v1, v18

    .line 377
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 374
    :cond_0
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 386
    .end local v18    # "j":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$100(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 387
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v10, v16, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 388
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    int-to-float v1, v1

    sub-float v8, v1, v16

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenWidth:I

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mScreenHeight:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 389
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 390
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 399
    :goto_2
    return-void

    .line 394
    :cond_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v8, v16, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 396
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v8, v16, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 397
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$300(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$200(Lcom/sec/android/app/hwmoduletest/TouchTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2
.end method

.method private setPaint()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, -0x1

    const/high16 v4, -0x1000000

    const/4 v3, 0x0

    .line 215
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 223
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 226
    .local v0, "e":Landroid/graphics/PathEffect;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 228
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 231
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 234
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 237
    return-void

    .line 223
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 247
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 249
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$100(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 250
    packed-switch v0, :pswitch_data_0

    .line 265
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 252
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 255
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 258
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 270
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 272
    .local v0, "action":I
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v3

    .line 276
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/TouchTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/TouchTest;->isHovering:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TouchTest;->access$100(Lcom/sec/android/app/hwmoduletest/TouchTest;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 277
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 279
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 282
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 285
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/TouchTest$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
