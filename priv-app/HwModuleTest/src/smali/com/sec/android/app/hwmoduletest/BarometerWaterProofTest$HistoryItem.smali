.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
.super Ljava/lang/Object;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryItem"
.end annotation


# instance fields
.field private mRawValue:F

.field private mTag:Ljava/lang/String;

.field private mTime:Ljava/lang/String;

.field private mTimeStamp:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;FLjava/lang/String;)V
    .locals 2
    .param p1, "time"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "value"    # F
    .param p4, "timeStamp"    # Ljava/lang/String;

    .prologue
    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "sec"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTime:Ljava/lang/String;

    .line 611
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTag:Ljava/lang/String;

    .line 612
    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mValue:Ljava/lang/String;

    .line 613
    iput p3, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mRawValue:F

    .line 614
    iput-object p4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTimeStamp:Ljava/lang/String;

    .line 615
    return-void
.end method


# virtual methods
.method public getAllData()Ljava/lang/String;
    .locals 2

    .prologue
    .line 638
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTime()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTimeStamp()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRawValue()F
    .locals 1

    .prologue
    .line 618
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mRawValue:F

    return v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTime:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mTimeStamp:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->mValue:Ljava/lang/String;

    return-object v0
.end method
