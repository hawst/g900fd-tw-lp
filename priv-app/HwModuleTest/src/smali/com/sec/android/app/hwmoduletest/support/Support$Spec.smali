.class public Lcom/sec/android/app/hwmoduletest/support/Support$Spec;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Spec"
.end annotation


# static fields
.field public static final DIGITIZER_HEIGHT_BASIS:Ljava/lang/String; = "DIGITIZER_HEIGHT_BASIS"

.field public static final DIGITIZER_WIDTH_BASIS:Ljava/lang/String; = "DIGITIZER_WIDTH_BASIS"

.field public static final ECG_DYNAMIC_RANGE_MAX:Ljava/lang/String; = "ECG_DYNAMIC_RANGE_MAX"

.field public static final ECG_DYNAMIC_RANGE_MIN:Ljava/lang/String; = "ECG_DYNAMIC_RANGE_MIN"

.field public static final ECG_FREQUENCY_MAX:Ljava/lang/String; = "ECG_FREQUENCY_MAX"

.field public static final ECG_FREQUENCY_MIN:Ljava/lang/String; = "ECG_FREQUENCY_MIN"

.field public static final ECG_HR_100_MAX:Ljava/lang/String; = "ECG_HR_100_MAX"

.field public static final ECG_HR_100_MIN:Ljava/lang/String; = "ECG_HR_100_MIN"

.field public static final ECG_HR_200_MAX:Ljava/lang/String; = "ECG_HR_200_MAX"

.field public static final ECG_HR_200_MIN:Ljava/lang/String; = "ECG_HR_200_MIN"

.field public static final ECG_HR_50_MAX:Ljava/lang/String; = "ECG_HR_50_MAX"

.field public static final ECG_HR_50_MIN:Ljava/lang/String; = "ECG_HR_50_MIN"

.field public static final ECG_HR_70_MAX:Ljava/lang/String; = "ECG_HR_70_MAX"

.field public static final ECG_HR_70_MIN:Ljava/lang/String; = "ECG_HR_70_MIN"

.field public static final ECG_SYSTEM_NOISE_MAX:Ljava/lang/String; = "ECG_SYSTEM_NOISE_MAX"

.field public static final FINGERPRINT_MRM_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_MRM_PIXEL_END"

.field public static final FINGERPRINT_MRM_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_MRM_PIXEL_START"

.field public static final FINGERPRINT_MRM_SPEC_AVG_MIN:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_AVG_MIN"

.field public static final FINGERPRINT_MRM_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_MAX"

.field public static final FINGERPRINT_MRM_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_MRM_SPEC_MIN"

.field public static final FINGERPRINT_PRIMARY_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_PRIMARY_PIXEL_END"

.field public static final FINGERPRINT_PRIMARY_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_PRIMARY_PIXEL_START"

.field public static final FINGERPRINT_PRIMARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_AVG"

.field public static final FINGERPRINT_PRIMARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_MAX"

.field public static final FINGERPRINT_PRIMARY_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_PRIMARY_SPEC_MIN"

.field public static final FINGERPRINT_SECONDARY_PIXEL_END:Ljava/lang/String; = "FINGERPRINT_SECONDARY_PIXEL_END"

.field public static final FINGERPRINT_SECONDARY_PIXEL_START:Ljava/lang/String; = "FINGERPRINT_SECONDARY_PIXEL_START"

.field public static final FINGERPRINT_SECONDARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_AVG"

.field public static final FINGERPRINT_SECONDARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_MAX"

.field public static final FINGERPRINT_SECONDARY_SPEC_MIN:Ljava/lang/String; = "FINGERPRINT_SECONDARY_SPEC_MIN"

.field public static final FINGERPRINT_STDEV_PRIMARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_PRIMARY_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_PRIMARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_PRIMARY_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECONDARY_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECONDARY_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECONDARY_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECONDARY_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECTION1_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION1_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECTION1_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION1_SPEC_MAX"

.field public static final FINGERPRINT_STDEV_SECTION2_SPEC_AVG:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION2_SPEC_AVG"

.field public static final FINGERPRINT_STDEV_SECTION2_SPEC_MAX:Ljava/lang/String; = "FINGERPRINT_STDEV_SECTION2_SPEC_MAX"

.field public static final GESTURE_CROSSTALK_RAW:Ljava/lang/String; = "GESTURE_CROSSTALK_RAW"

.field public static final GYROSCOPE_SELFTEST_MAX:Ljava/lang/String; = "GYROSCOPE_SELFTEST_MAX"

.field public static final GYROSCOPE_SELFTEST_MIN:Ljava/lang/String; = "GYROSCOPE_SELFTEST_MIN"

.field public static final HRM_CLOUD_UV1_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV1_DC_LEVEL"

.field public static final HRM_CLOUD_UV2_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV2_DC_LEVEL"

.field public static final HRM_CLOUD_UV3_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV3_DC_LEVEL"

.field public static final HRM_CLOUD_UV4_DC_LEVEL:Ljava/lang/String; = "HRM_CLOUD_UV4_DC_LEVEL"

.field public static final HRM_CLOUD_UV_RATIO:Ljava/lang/String; = "HRM_CLOUD_UV_RATIO"

.field public static final HRM_CLOUD_UV_SUM_RATIO:Ljava/lang/String; = "HRM_CLOUD_UV_SUM_RATIO"

.field public static final HRM_FREQUENCY_DC_IR:Ljava/lang/String; = "HRM_FREQUENCY_DC_IR"

.field public static final HRM_FREQUENCY_DC_RED:Ljava/lang/String; = "HRM_FREQUENCY_DC_RED"

.field public static final HRM_FREQUENCY_NOISE_IR:Ljava/lang/String; = "HRM_FREQUENCY_NOISE_IR"

.field public static final HRM_FREQUENCY_NOISE_RED:Ljava/lang/String; = "HRM_FREQUENCY_NOISE_RED"

.field public static final HRM_FREQUENCY_SAMPLE_NO:Ljava/lang/String; = "HRM_FREQUENCY_SAMPLE_NO"

.field public static final HRM_HR_SPO2:Ljava/lang/String; = "HRM_HR_SPO2"

.field public static final HRM_IR_RED_R_RATIO:Ljava/lang/String; = "HRM_IR_RED_R_RATIO"

.field public static final HRM_PEAK_TO_PEAK_DC_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_DC_IR"

.field public static final HRM_PEAK_TO_PEAK_DC_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_DC_RED"

.field public static final HRM_PEAK_TO_PEAK_PEAK_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_PEAK_IR"

.field public static final HRM_PEAK_TO_PEAK_PEAK_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_PEAK_RED"

.field public static final HRM_PEAK_TO_PEAK_RATIO_IR:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_RATIO_IR"

.field public static final HRM_PEAK_TO_PEAK_RATIO_RED:Ljava/lang/String; = "HRM_PEAK_TO_PEAK_RATIO_RED"

.field public static final LED_POWER_MAX:Ljava/lang/String; = "LED_POWER_MAX"

.field public static final LED_POWER_MIN:Ljava/lang/String; = "LED_POWER_MIN"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_1:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_1"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_2:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_2"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_3:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_3"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_4:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_4"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_5:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_5"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_6:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_6"

.field public static final LIGHT_SENSOR_DOWN_LEVEL_7:Ljava/lang/String; = "LIGHT_SENSOR_DOWN_LEVEL_7"

.field public static final LIGHT_SENSOR_LEVEL_1_MAX:Ljava/lang/String; = "LIGHT_SENSOR_LEVEL_1_MAX"

.field public static final LIGHT_SENSOR_LEVEL_2_MAX:Ljava/lang/String; = "LIGHT_SENSOR_LEVEL_2_MAX"

.field public static final LIGHT_SENSOR_LEVEL_3_MAX:Ljava/lang/String; = "LIGHT_SENSOR_LEVEL_3_MAX"

.field public static final LIGHT_SENSOR_LEVEL_4_MAX:Ljava/lang/String; = "LIGHT_SENSOR_LEVEL_4_MAX"

.field public static final LIGHT_SENSOR_UP_LEVEL_1:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_1"

.field public static final LIGHT_SENSOR_UP_LEVEL_2:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_2"

.field public static final LIGHT_SENSOR_UP_LEVEL_3:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_3"

.field public static final LIGHT_SENSOR_UP_LEVEL_4:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_4"

.field public static final LIGHT_SENSOR_UP_LEVEL_5:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_5"

.field public static final LIGHT_SENSOR_UP_LEVEL_6:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_6"

.field public static final LIGHT_SENSOR_UP_LEVEL_7:Ljava/lang/String; = "LIGHT_SENSOR_UP_LEVEL_7"

.field public static final MAGNETIC_SENSOR_SELFTEST_X_MAX:Ljava/lang/String; = "MAGNETIC_SENSOR_SELFTEST_X_MAX"

.field public static final MAGNETIC_SENSOR_SELFTEST_X_MIN:Ljava/lang/String; = "MAGNETIC_SENSOR_SELFTEST_X_MIN"

.field public static final NO_RGBSENSOR_SUPPORT_REV:Ljava/lang/String; = "NO_RGBSENSOR_SUPPORT_REV"

.field public static final OIS_INITIAL_SPEC_MAX:Ljava/lang/String; = "OIS_INITIAL_SPEC_MAX"

.field public static final OIS_INITIAL_SPEC_MIN:Ljava/lang/String; = "OIS_INITIAL_SPEC_MIN"

.field public static final RGBSENSOR_SUPPORT_WHITE:Ljava/lang/String; = "RGBSENSOR_SUPPORT_WHITE"

.field public static final SPEN_NODE_COUNT_HEIGHT:Ljava/lang/String; = "SPEN_NODE_COUNT_HEIGHT"

.field public static final SPEN_NODE_COUNT_WIDTH:Ljava/lang/String; = "SPEN_NODE_COUNT_WIDTH"

.field public static final TAG:Ljava/lang/String; = "HardwareSpec"

.field public static final TOUCH_KEY_GRAPH_REDUCTION:Ljava/lang/String; = "TOUCH_KEY_GRAPH_REDUCTION"

.field public static final TSP_X_AXIS_CHANNEL:Ljava/lang/String; = "TSP_X_AXIS_CHANNEL"

.field public static final TSP_Y_AXIS_CHANNEL:Ljava/lang/String; = "TSP_Y_AXIS_CHANNEL"

.field public static final ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:Ljava/lang/String; = "ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE"

.field public static final UV_PROD_ID_SPEC:Ljava/lang/String; = "UV_PROD_ID_SPEC"

.field public static final WACOM_HEIGHT_BASIS:Ljava/lang/String; = "WACOM_HEIGHT_BASIS"

.field public static final WACOM_WIDTH_BASIS:Ljava/lang/String; = "WACOM_WIDTH_BASIS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 409
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getBoolean(Ljava/lang/String;I)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "logLevel"    # I

    .prologue
    .line 413
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;I)Z
    invoke-static {p0, v0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$800(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public static getByte(Ljava/lang/String;)B
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 417
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getByte(Ljava/lang/String;Ljava/lang/String;)B
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method public static getDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 437
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getDouble(Ljava/lang/String;Ljava/lang/String;)D
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$700(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 433
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$200(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 425
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$600(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getLong(Ljava/lang/String;)J
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 429
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getLong(Ljava/lang/String;Ljava/lang/String;)J
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$900(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 421
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
