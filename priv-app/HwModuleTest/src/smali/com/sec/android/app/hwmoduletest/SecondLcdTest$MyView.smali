.class public Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;
.super Landroid/view/View;
.source "SecondLcdTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SecondLcdTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private col_height:F

.field private col_width:F

.field private isPen:Z

.field private isTouchDown:Z

.field private m2ndScreenWidth:I

.field private mClickPaint:Landroid/graphics/Paint;

.field private mDualPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mHeightCross:F

.field private mLinePaint:Landroid/graphics/Paint;

.field private mLinePaint_Fail:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mSPenLinePaint:Landroid/graphics/Paint;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchPaint:Landroid/graphics/Paint;

.field private mTouchedX:F

.field private mTouchedY:F

.field private mWidthBetween:F

.field private mWidthCross:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;Landroid/content/Context;)V
    .locals 7
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 229
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    .line 230
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 205
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 206
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 207
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 208
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 221
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->col_height:F

    .line 222
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->col_width:F

    .line 223
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mWidthCross:F

    .line 224
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mWidthBetween:F

    .line 225
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mHeightCross:F

    .line 231
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->setKeepScreenOn(Z)V

    .line 232
    const-string v3, "window"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 234
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 235
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 236
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/16 v4, 0x28

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    .line 237
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    .line 238
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    .line 239
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 240
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    invoke-static {v0, v3, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 241
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 242
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 243
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    int-to-float v3, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->col_height:F

    .line 244
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    int-to-float v3, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$300(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->col_width:F

    .line 245
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->col_width:F

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mWidthBetween:F

    .line 246
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->setPaint()V

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->initRect()V

    .line 248
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    .line 249
    return-void
.end method

.method private drawByEvent(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 319
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 320
    .local v6, "action":I
    packed-switch v6, :pswitch_data_0

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 322
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 323
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 324
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 325
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 328
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 329
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 330
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 331
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 332
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 333
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 334
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 335
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 329
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 337
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 338
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 339
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 340
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 341
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 342
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 343
    iput-boolean v8, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 347
    .end local v7    # "i":I
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 348
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 349
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 350
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 351
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 352
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 353
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 355
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    goto/16 :goto_0

    .line 320
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private drawLine(FFFFLandroid/graphics/Paint;)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 404
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 405
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 406
    float-to-int v6, p1

    .line 407
    float-to-int v8, p3

    .line 412
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 413
    float-to-int v7, p2

    .line 414
    float-to-int v9, p4

    .line 419
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 420
    return-void

    .line 409
    :cond_0
    float-to-int v6, p3

    .line 410
    float-to-int v8, p1

    goto :goto_0

    .line 416
    :cond_1
    float-to-int v7, p4

    .line 417
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FFLandroid/graphics/Paint;)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 422
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 423
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 424
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 444
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v7, v0, v1

    .line 445
    .local v7, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$300(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 446
    .local v8, "col_width":F
    div-float v0, p1, v8

    float-to-int v9, v0

    .line 447
    .local v9, "countX":I
    div-float v0, p2, v7

    float-to-int v10, v0

    .line 448
    .local v10, "countY":I
    int-to-float v0, v10

    mul-float v6, v7, v0

    .line 449
    .local v6, "ColY":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v10, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$300(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v9, v0, :cond_2

    .line 450
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$400(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "You are out of bounds!,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_1
    :goto_0
    return-void

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$500(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    if-nez v0, :cond_a

    if-lez v9, :cond_a

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    if-nez v0, :cond_5

    .line 455
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isPen:Z

    if-eqz v0, :cond_4

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aput-boolean v1, v0, v10

    .line 463
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$500(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    float-to-int v2, v6

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v3, v3

    add-float v4, v6, v7

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 465
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v6, v2

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    add-float v4, v6, v7

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_0

    .line 460
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aput-boolean v1, v0, v10

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    goto :goto_1

    .line 468
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 469
    :cond_6
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isPen:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 471
    iget-object p3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    .line 472
    new-instance v0, Landroid/graphics/AvoidXfermode;

    const/16 v1, -0x100

    const/16 v2, 0xff

    sget-object v3, Landroid/graphics/AvoidXfermode$Mode;->TARGET:Landroid/graphics/AvoidXfermode$Mode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/AvoidXfermode;-><init>(IILandroid/graphics/AvoidXfermode$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 479
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$500(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    float-to-int v2, v6

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v3, v3

    add-float v4, v6, v7

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 481
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v6, v2

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    add-float v4, v6, v7

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 483
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->isPassSide()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$700(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$502(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;Z)Z

    goto/16 :goto_0

    .line 474
    :cond_9
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isPen:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->sidedraw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$600(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)[[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 476
    iget-object p3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    .line 477
    new-instance v0, Landroid/graphics/AvoidXfermode;

    const v1, -0xff01

    const/16 v2, 0xff

    sget-object v3, Landroid/graphics/AvoidXfermode$Mode;->TARGET:Landroid/graphics/AvoidXfermode$Mode;

    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/AvoidXfermode;-><init>(IILandroid/graphics/AvoidXfermode$Mode;)V

    invoke-virtual {p3, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto/16 :goto_2

    .line 488
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->issideDrawDone:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$500(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$800(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v0

    if-nez v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$808(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->remoteCall:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$900(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->setResult(I)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "2ndScreen: Pass"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->finish()V

    goto/16 :goto_0
.end method

.method private draw_down(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 363
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 364
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 365
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    .line 367
    return-void
.end method

.method private draw_move(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 371
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 372
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 373
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 374
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 375
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 376
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 377
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 371
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 379
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 380
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 381
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 382
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 383
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 384
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    .line 387
    .end local v6    # "i":I
    :cond_1
    return-void
.end method

.method private draw_up(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 391
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    .line 392
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    .line 393
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    .line 394
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    .line 395
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 396
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 398
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isTouchDown:Z

    .line 400
    :cond_1
    return-void
.end method

.method private initRect()V
    .locals 11

    .prologue
    const/high16 v2, -0x1000000

    .line 427
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v7, v0, v1

    .line 428
    .local v7, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$300(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 429
    .local v8, "col_width":F
    const/4 v6, 0x0

    .line 430
    .local v6, "ColY":I
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 431
    .local v5, "mRectPaint":Landroid/graphics/Paint;
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 432
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 433
    .local v10, "mRectPaintCross":Landroid/graphics/Paint;
    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 434
    invoke-virtual {v10, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 435
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 436
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/SecondLcdTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest;->access$200(Lcom/sec/android/app/hwmoduletest/SecondLcdTest;)I

    move-result v0

    if-gt v9, v0, :cond_0

    .line 437
    int-to-float v0, v9

    mul-float/2addr v0, v7

    float-to-int v6, v0

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    int-to-float v2, v6

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 436
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v1, v1

    sub-float/2addr v1, v8

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->m2ndScreenWidth:I

    int-to-float v3, v3

    sub-float/2addr v3, v8

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mScreenHeight:I

    add-int/lit8 v4, v4, 0x0

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 441
    return-void
.end method

.method private setPaint()V
    .locals 7

    .prologue
    const/16 v6, 0xff

    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, -0x1000000

    .line 253
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setDither(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 262
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setDither(Z)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mSPenLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 271
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mDualPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 275
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 278
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mTouchPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/AvoidXfermode;

    sget-object v2, Landroid/graphics/AvoidXfermode$Mode;->TARGET:Landroid/graphics/AvoidXfermode$Mode;

    invoke-direct {v1, v5, v6, v2}, Landroid/graphics/AvoidXfermode;-><init>(IILandroid/graphics/AvoidXfermode$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 283
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v1, -0xff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/AvoidXfermode;

    sget-object v2, Landroid/graphics/AvoidXfermode$Mode;->TARGET:Landroid/graphics/AvoidXfermode$Mode;

    invoke-direct {v1, v5, v6, v2}, Landroid/graphics/AvoidXfermode;-><init>(IILandroid/graphics/AvoidXfermode$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 288
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 293
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 297
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 298
    .local v0, "action":I
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isPen:Z

    .line 299
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 300
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->isPen:Z

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->drawByEvent(Landroid/view/MotionEvent;)V

    .line 303
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 315
    :goto_0
    return v3

    .line 305
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 308
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 311
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SecondLcdTest$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 303
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
