.class Lcom/sec/android/app/hwmoduletest/HistoryItem;
.super Ljava/lang/Object;
.source "GestureTestMode2Display.java"


# instance fields
.field private RawDataAValue:Ljava/lang/String;

.field private RawDataBValue:Ljava/lang/String;

.field private RawDataCValue:Ljava/lang/String;

.field private RawDataDValue:Ljava/lang/String;

.field private RawDataEValue:Ljava/lang/String;

.field private RawDataFValue:Ljava/lang/String;

.field private RawDataGValue:Ljava/lang/String;

.field private RawDataHValue:Ljava/lang/String;

.field private countvalue:Ljava/lang/String;


# direct methods
.method public constructor <init>(IFFFI)V
    .locals 4
    .param p1, "count"    # I
    .param p2, "RawDataA"    # F
    .param p3, "RawDataB"    # F
    .param p4, "RawDataC"    # F
    .param p5, "RawDataD"    # I

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->countvalue:Ljava/lang/String;

    .line 259
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.0000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double v2, p2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataAValue:Ljava/lang/String;

    .line 260
    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataBValue:Ljava/lang/String;

    .line 261
    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataCValue:Ljava/lang/String;

    .line 262
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataDValue:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public constructor <init>(IIIIIIIII)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "RawDataA"    # I
    .param p3, "RawDataB"    # I
    .param p4, "RawDataC"    # I
    .param p5, "RawDataD"    # I
    .param p6, "RawDataE"    # I
    .param p7, "RawDataF"    # I
    .param p8, "RawDataG"    # I
    .param p9, "RawDataH"    # I

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->countvalue:Ljava/lang/String;

    .line 268
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataAValue:Ljava/lang/String;

    .line 269
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataBValue:Ljava/lang/String;

    .line 270
    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataCValue:Ljava/lang/String;

    .line 271
    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataDValue:Ljava/lang/String;

    .line 272
    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataEValue:Ljava/lang/String;

    .line 273
    invoke-static {p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataFValue:Ljava/lang/String;

    .line 274
    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataGValue:Ljava/lang/String;

    .line 275
    invoke-static {p9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataHValue:Ljava/lang/String;

    .line 276
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataAValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataBValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataCValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataDValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataE()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataEValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataF()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataFValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataG()Ljava/lang/String;
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataGValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataH()Ljava/lang/String;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItem;->RawDataHValue:Ljava/lang/String;

    return-object v0
.end method
