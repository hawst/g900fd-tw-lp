.class public Lcom/sec/android/app/hwmoduletest/Gesture_test_main;
.super Landroid/app/Activity;
.source "Gesture_test_main.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "Gesture_test_main"


# instance fields
.field private vendor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 54
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 76
    :goto_0
    return-void

    .line 56
    :pswitch_0
    const-string v1, "Gesture_test_main"

    const-string v2, "onClick"

    const-string v3, "Gesture_Data"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-class v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    :pswitch_1
    const-string v1, "Gesture_test_main"

    const-string v2, "onClick"

    const-string v3, "Gesture_Zmean"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-class v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 63
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 66
    :pswitch_2
    const-string v1, "Gesture_test_main"

    const-string v2, "onClick"

    const-string v3, "Gesture_Crosstalk"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-class v1, Lcom/sec/android/app/hwmoduletest/GestureSensor;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 71
    :pswitch_3
    const-string v1, "Gesture_test_main"

    const-string v2, "onClick"

    const-string v3, "Gesture_Crosstalk"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-class v1, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b009a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const-string v5, "GESTURE_SENSOR_VENDOR"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->vendor:Ljava/lang/String;

    .line 25
    const v5, 0x7f030023

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->setContentView(I)V

    .line 26
    const v5, 0x7f0b0099

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 27
    .local v4, "title_vendor":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->vendor:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    const v5, 0x7f0b009a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 29
    .local v2, "button_Gesture_Data":Landroid/widget/Button;
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    const v5, 0x7f0b009b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 31
    .local v3, "button_Gesture_Zmean":Landroid/widget/Button;
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 33
    const v5, 0x7f0b009c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 34
    .local v1, "button_Gesture_Crosstalk":Landroid/widget/Button;
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    const v5, 0x7f0b009d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 36
    .local v0, "button_Gesture_Countdelta":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const-string v5, "AMS"

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/Gesture_test_main;->vendor:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 39
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 41
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 49
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 45
    return-void
.end method
