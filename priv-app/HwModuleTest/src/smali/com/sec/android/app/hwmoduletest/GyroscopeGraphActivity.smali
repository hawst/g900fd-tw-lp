.class public Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$1;,
        Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;
    }
.end annotation


# instance fields
.field private mGyroGraph:Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;

.field private mGyroSensor:Landroid/hardware/Sensor;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    const-string v0, "GyroscopeGraphActivity"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;

    .line 22
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;)Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mGyroGraph:Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v0, 0x7f030035

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->setContentView(I)V

    .line 28
    const v0, 0x7f0b00d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mGyroGraph:Lcom/sec/android/app/hwmoduletest/view/GyroscopeGraph;

    .line 29
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    .line 30
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mGyroSensor:Landroid/hardware/Sensor;

    .line 31
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 47
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 41
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 42
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 34
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 35
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeGraphActivity;->mGyroSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 37
    return-void
.end method
