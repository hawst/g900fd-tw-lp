.class public Lcom/sec/android/app/hwmoduletest/MagneticAsahi;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "MagneticAsahi.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_DAC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Self:I

.field private mSensorID_Temperature:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_DAC:Landroid/widget/TableRow;

.field private mTableRow_HX:Landroid/widget/TableRow;

.field private mTableRow_HY:Landroid/widget/TableRow;

.field private mTableRow_HZ:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTableRow_Temp:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_DAC_X:Landroid/widget/TextView;

.field private mText_DAC_Y:Landroid/widget/TextView;

.field private mText_DAC_Z:Landroid/widget/TextView;

.field private mText_HX:Landroid/widget/TextView;

.field private mText_HY:Landroid/widget/TextView;

.field private mText_HZ:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Temp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "MagneticAsahi"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSenserID:[I

    .line 58
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    .line 59
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    .line 60
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Temperature:I

    .line 61
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    .line 62
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    .line 63
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    .line 65
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->WHAT_UPDATE:I

    .line 123
    new-instance v0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi$1;-><init>(Lcom/sec/android/app/hwmoduletest/MagneticAsahi;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mHandler:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/MagneticAsahi;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticAsahi;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/MagneticAsahi;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/MagneticAsahi;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->update()V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 133
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 134
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Initialized:Landroid/widget/TextView;

    .line 136
    const v0, 0x7f0b0168

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_Temp:Landroid/widget/TableRow;

    .line 137
    const v0, 0x7f0b0136

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Temp:Landroid/widget/TextView;

    .line 139
    const v0, 0x7f0b017a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HX:Landroid/widget/TableRow;

    .line 140
    const v0, 0x7f0b017b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HX:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f0b017c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HY:Landroid/widget/TableRow;

    .line 143
    const v0, 0x7f0b017d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HY:Landroid/widget/TextView;

    .line 145
    const v0, 0x7f0b017e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HZ:Landroid/widget/TableRow;

    .line 146
    const v0, 0x7f0b017f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HZ:Landroid/widget/TextView;

    .line 148
    const v0, 0x7f0b016e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_DAC:Landroid/widget/TableRow;

    .line 149
    const v0, 0x7f0b016f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_X:Landroid/widget/TextView;

    .line 150
    const v0, 0x7f0b0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Y:Landroid/widget/TextView;

    .line 151
    const v0, 0x7f0b0171

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Z:Landroid/widget/TextView;

    .line 153
    const v0, 0x7f0b0172

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_ADC:Landroid/widget/TableRow;

    .line 154
    const v0, 0x7f0b0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_X:Landroid/widget/TextView;

    .line 155
    const v0, 0x7f0b0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Y:Landroid/widget/TextView;

    .line 156
    const v0, 0x7f0b0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Z:Landroid/widget/TextView;

    .line 158
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTextResult:Landroid/widget/TextView;

    .line 159
    return-void
.end method

.method private update()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x2

    .line 162
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "data":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 165
    .local v4, "tempdata":Ljava/lang/String;
    const/4 v3, 0x0

    .line 166
    .local v3, "retData":Ljava/lang/String;
    const/4 v1, 0x1

    .line 167
    .local v1, "isPass":Z
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 169
    :cond_0
    const-string v5, "GEOMAGNETIC_SENSOR_SELFTEST"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 170
    if-eqz v4, :cond_1

    .line 171
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 176
    :cond_1
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    if-ge v5, v6, :cond_4

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_Initialized:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 183
    :cond_2
    if-eqz v0, :cond_12

    .line 184
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 186
    :cond_3
    aget-object v5, v0, v10

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 187
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Initialized - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v3, "1"

    .line 194
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Initialized:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Initialized Return : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_4
    :goto_1
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Temperature:I

    if-ge v5, v6, :cond_5

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_Temp:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 216
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Temperature:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_14

    .line 219
    aget-object v5, v0, v11

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Temperature - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Temp:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Retuen : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Temperature : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_5
    :goto_3
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    if-ge v5, v6, :cond_8

    .line 238
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HX:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 239
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HY:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 240
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_HZ:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 241
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 243
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 246
    :cond_6
    if-eqz v0, :cond_18

    .line 247
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 250
    :cond_7
    const/4 v5, 0x1

    :try_start_0
    aget-object v5, v0, v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 251
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "HX/HY/HZ - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :goto_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HX:Landroid/widget/TextView;

    const/4 v6, 0x2

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HY:Landroid/widget/TextView;

    const/4 v6, 0x3

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HZ:Landroid/widget/TextView;

    const/4 v6, 0x4

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_8
    :goto_5
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    if-ge v5, v6, :cond_b

    .line 287
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_DAC:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 288
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 290
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 293
    :cond_9
    if-eqz v0, :cond_1c

    .line 294
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 297
    :cond_a
    const/4 v5, 0x5

    :try_start_1
    aget-object v5, v0, v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 298
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "DAC - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v3, "1"

    .line 305
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_X:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Y:Landroid/widget/TextView;

    const-string v6, "0"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Z:Landroid/widget/TextView;

    const-string v6, "0"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 335
    :cond_b
    :goto_7
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_None:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    if-ge v5, v6, :cond_e

    .line 336
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTableRow_ADC:Landroid/widget/TableRow;

    invoke-virtual {v5, v10}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 337
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 339
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 342
    :cond_c
    if-eqz v0, :cond_20

    .line 343
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v6, "AK09911C"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 346
    :cond_d
    const/4 v5, 0x6

    :try_start_2
    aget-object v5, v0, v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 347
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "ADC - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :goto_8
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_X:Landroid/widget/TextView;

    const/4 v6, 0x7

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Y:Landroid/widget/TextView;

    const/16 v6, 0x8

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Z:Landroid/widget/TextView;

    const/16 v6, 0x9

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    .line 381
    :cond_e
    :goto_9
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_21

    const-string v5, "PASS"

    :goto_a
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mTextResult:Landroid/widget/TextView;

    if-eqz v1, :cond_22

    const v5, -0xffff01

    :goto_b
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 383
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "update"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Result:"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz v1, :cond_23

    const-string v5, "PASS"

    :goto_c
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    return-void

    .line 190
    :cond_f
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Initialized - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v1, 0x0

    .line 192
    const-string v3, "-1"

    goto/16 :goto_0

    .line 197
    :cond_10
    aget-object v5, v0, v9

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 198
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Initialized - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :goto_d
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Initialized:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Initialized Return : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 200
    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Initialized - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/4 v1, 0x0

    goto :goto_d

    .line 207
    :cond_12
    const/4 v1, 0x0

    .line 208
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Initialized:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Initialized - Fail : null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 222
    :cond_13
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Temperature - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 230
    :cond_14
    const/4 v1, 0x0

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_Temp:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "Temperature - Fail : null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 253
    :cond_15
    :try_start_3
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "HX/HY/HZ - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    .line 254
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 259
    :catch_0
    move-exception v2

    .line 260
    .local v2, "ne":Ljava/lang/NullPointerException;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 263
    .end local v2    # "ne":Ljava/lang/NullPointerException;
    :cond_16
    aget-object v5, v0, v11

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 264
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "HX/HY/HZ - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_e
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HX:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HY:Landroid/widget/TextView;

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HZ:Landroid/widget/TextView;

    aget-object v6, v0, v13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Retuen : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", HX:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", HY:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", HZ:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 266
    :cond_17
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "HX/HY/HZ - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v1, 0x0

    goto :goto_e

    .line 277
    :cond_18
    const/4 v1, 0x0

    .line 278
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HX:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HY:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_HZ:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "HX/HY/HZ - Fail : null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 301
    :cond_19
    :try_start_4
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "DAC - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v3, "-1"
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_1

    .line 303
    const/4 v1, 0x0

    goto/16 :goto_6

    .line 308
    :catch_1
    move-exception v2

    .line 309
    .restart local v2    # "ne":Ljava/lang/NullPointerException;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_7

    .line 312
    .end local v2    # "ne":Ljava/lang/NullPointerException;
    :cond_1a
    aget-object v5, v0, v11

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 313
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "DAC - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :goto_f
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_X:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Y:Landroid/widget/TextView;

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Z:Landroid/widget/TextView;

    aget-object v6, v0, v13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Retuen : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", [DAC]X:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Y:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Z:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 315
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "DAC - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/4 v1, 0x0

    goto :goto_f

    .line 326
    :cond_1c
    const/4 v1, 0x0

    .line 327
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_X:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Y:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_DAC_Z:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "DAC - Fail : null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 349
    :cond_1d
    :try_start_5
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "ADC - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2

    .line 350
    const/4 v1, 0x0

    goto/16 :goto_8

    .line 355
    :catch_2
    move-exception v2

    .line 356
    .restart local v2    # "ne":Ljava/lang/NullPointerException;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_9

    .line 359
    .end local v2    # "ne":Ljava/lang/NullPointerException;
    :cond_1e
    aget-object v5, v0, v11

    const-string v6, "OK"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 360
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "ADC - Pass"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :goto_10
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_X:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Y:Landroid/widget/TextView;

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Z:Landroid/widget/TextView;

    aget-object v6, v0, v13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Retuen : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", [ADC]X:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Y:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", Z:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 362
    :cond_1f
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "ADC - Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const/4 v1, 0x0

    goto :goto_10

    .line 372
    :cond_20
    const/4 v1, 0x0

    .line 373
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_X:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Y:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mText_ADC_Z:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    const-string v7, "ADC - Fail : null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 381
    :cond_21
    const-string v5, "FAIL"

    goto/16 :goto_a

    .line 382
    :cond_22
    const/high16 v5, -0x10000

    goto/16 :goto_b

    .line 383
    :cond_23
    const-string v5, "FAIL"

    goto/16 :goto_c
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->setContentView(I)V

    .line 74
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->init()V

    .line 77
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 121
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 80
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK8963"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK8963C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    .line 85
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    .line 86
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    .line 87
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    .line 108
    :cond_1
    :goto_0
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Temperature:I

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSenserID:[I

    .line 112
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 116
    return-void

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK09911"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK09911C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 90
    :cond_3
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    .line 91
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    .line 92
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    .line 93
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    goto :goto_0

    .line 94
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK8975"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK8963C_MANAGER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96
    :cond_5
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Initialized:I

    .line 97
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_DAC:I

    .line 98
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_ADC:I

    .line 99
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mSensorID_Self:I

    goto :goto_0

    .line 100
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/MagneticAsahi;->mFeature:Ljava/lang/String;

    const-string v1, "AK8973"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto/16 :goto_0
.end method
