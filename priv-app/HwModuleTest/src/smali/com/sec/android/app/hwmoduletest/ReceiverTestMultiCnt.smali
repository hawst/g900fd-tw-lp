.class public Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ReceiverTestMultiCnt.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final AUDIO_PATH_EAR:I = 0x2

.field public static final AUDIO_PATH_HDMI:I = 0x3

.field public static final AUDIO_PATH_OFF:I = 0x4

.field public static final AUDIO_PATH_RCV:I = 0x1

.field public static final AUDIO_PATH_SPK:I


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private m300Hz:Landroid/widget/ToggleButton;

.field private m500Hz:Landroid/widget/ToggleButton;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    const-string v0, "LowFrequencyTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv_lf"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->AUDIO_PATH:[Ljava/lang/String;

    .line 40
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 133
    :cond_0
    return-void
.end method

.method private startMedia(I)V
    .locals 2
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x1

    .line 112
    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->setAudioPath(I)V

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->release()V

    .line 114
    invoke-static {p0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 119
    return-void
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->release()V

    .line 126
    :cond_0
    return-void
.end method

.method private updateMedia(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 92
    move-object v1, p1

    check-cast v1, Landroid/widget/ToggleButton;

    .line 93
    .local v1, "toggle":Landroid/widget/ToggleButton;
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->stopMedia()V

    .line 95
    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "resID":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 107
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->startMedia(I)V

    .line 109
    .end local v0    # "resID":I
    :cond_0
    return-void

    .line 100
    .restart local v0    # "resID":I
    :sswitch_0
    const v0, 0x7f050004

    .line 101
    goto :goto_0

    .line 103
    :sswitch_1
    const v0, 0x7f050005

    goto :goto_0

    .line 98
    :sswitch_data_0
    .sparse-switch
        0x7f0b0167 -> :sswitch_0
        0x7f0b01c6 -> :sswitch_1
    .end sparse-switch
.end method

.method private updateToggle(I)V
    .locals 3
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x0

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m300Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0b0167

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m300Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m500Hz:Landroid/widget/ToggleButton;

    const v2, 0x7f0b01c6

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 89
    return-void

    :cond_1
    move v0, v1

    .line 87
    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->updateToggle(I)V

    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->updateMedia(Landroid/view/View;)V

    .line 84
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f03006a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->setContentView(I)V

    .line 46
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mAudioManager:Landroid/media/AudioManager;

    .line 47
    const v0, 0x7f0b0167

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m300Hz:Landroid/widget/ToggleButton;

    .line 48
    const v0, 0x7f0b01c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m500Hz:Landroid/widget/ToggleButton;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m300Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setSoundEffectsEnabled(Z)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setSoundEffectsEnabled(Z)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m300Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->m500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 67
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->stopMedia()V

    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->setAudioPath(I)V

    .line 78
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 59
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->setAudioPath(I)V

    .line 63
    return-void
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestMultiCnt;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 138
    return-void
.end method
