.class public Lcom/sec/android/app/hwmoduletest/HDMI;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HDMI.java"


# instance fields
.field private final HDMI_MSG:Ljava/lang/String;

.field private MAX_VOLUME:F

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mIsTurnOffAudioPath:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "UIHDMI"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 19
    const-string v0, "HDMI Pattern Display On"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->HDMI_MSG:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    .line 23
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->MAX_VOLUME:F

    .line 25
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HDMI$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HDMI$1;-><init>(Lcom/sec/android/app/hwmoduletest/HDMI;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 19
    const-string v0, "HDMI Pattern Display On"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->HDMI_MSG:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    .line 23
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->MAX_VOLUME:F

    .line 25
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HDMI$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HDMI$1;-><init>(Lcom/sec/android/app/hwmoduletest/HDMI;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/HDMI;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->stopMedia()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/HDMI;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/hwmoduletest/HDMI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/HDMI;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->turnOffAudioPath()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/HDMI;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HDMI;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private release()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 161
    :cond_0
    return-void
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->release()V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->turnOffAudioPath()V

    .line 154
    :cond_0
    return-void
.end method

.method private declared-synchronized turnOffAudioPath()V
    .locals 4

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "turnOffAudioPath"

    const-string v2, "Already UIHDMI finished"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_route=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 133
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_route=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "turnOffAudioPath()"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsTurnOffAudioPath = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 88
    const-string v1, "ro.board.platform"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "platform":Ljava/lang/String;
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HDMI;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mAudioManager:Landroid/media/AudioManager;

    .line 91
    if-eqz v0, :cond_0

    const-string v1, "capri"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    const v1, 0x7f030040

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HDMI;->setContentView(I)V

    .line 104
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onCreate"

    const-string v3, "mMediaPlayer.setVolume => MAX_VOLUME"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void

    .line 94
    :cond_0
    const v1, 0x7f03003f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HDMI;->setContentView(I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 123
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HDMI;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onPause"

    const-string v2, "stopMedia() ..."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->stopMedia()V

    .line 127
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 109
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 111
    .local v0, "mIntentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.android.samsungtest.HDMITEST_STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    const-string v1, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/hwmoduletest/HDMI;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 115
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onResume()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsTurnOffAudioPath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mIsTurnOffAudioPath:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const v1, 0x7f050006

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/HDMI;->playMedia(IZ)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "HDMI Pattern Display On"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 119
    return-void
.end method

.method public playMedia(IZ)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "isLoop"    # Z

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->release()V

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/HDMI;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HDMI;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 146
    return-void
.end method
