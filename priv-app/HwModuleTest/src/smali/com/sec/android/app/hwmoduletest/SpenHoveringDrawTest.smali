.class public Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SpenHoveringDrawTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;,
        Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenHoveringDrawTest"


# instance fields
.field private final MARGINE_X:F

.field private final MARGINE_Y:F

.field private final SPEC_HEIGHT:I

.field private final SPEC_WIDTH:I

.field private isTestFinish:Z

.field private isTestPass:Z

.field private isTestStart:Z

.field private mNPixelForOneMM:F

.field private mRectList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    const-string v0, "SpenHoveringDrawTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->MARGINE_X:F

    .line 78
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->MARGINE_Y:F

    .line 79
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->SPEC_HEIGHT:I

    .line 80
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->SPEC_WIDTH:I

    .line 82
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z

    .line 85
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestPass:Z

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isPassCross()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;

    return-object v0
.end method

.method private isPassCross()Z
    .locals 4

    .prologue
    .line 135
    const/4 v1, 0x1

    .line 137
    .local v1, "isPassCross":Z
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;

    .line 138
    .local v2, "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    iget-boolean v3, v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->isTouched:Z

    if-nez v3, :cond_0

    .line 139
    const/4 v1, 0x0

    .line 144
    .end local v2    # "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    :cond_1
    return v1
.end method

.method private setTSP()V
    .locals 5

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 96
    .local v0, "r":Landroid/content/res/Resources;
    const/4 v1, 0x5

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "HoverView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mNPixelForOneMM = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    const-string v0, "SpenHoveringDrawTest"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->setTSP()V

    .line 107
    new-instance v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;-><init>(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->setContentView(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 125
    const/16 v0, 0x18

    if-ne p1, v0, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isPassCross()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->setResult(I)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->finish()V

    .line 131
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 114
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 119
    return-void
.end method
