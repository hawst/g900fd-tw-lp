.class Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;
.super Ljava/lang/Object;
.source "TempHumidDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempRaw:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$100(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidRaw:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempComp:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidComp:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v0

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_1

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mValueList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$700(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)Ljava/util/List;

    move-result-object v8

    new-instance v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$004(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempRaw:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$100(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidRaw:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempComp:F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidComp:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v6, v6, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mTempCheck:F
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v7, v7, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mHumidCheck:F
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)F

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryItem;-><init>(IFFFFFF)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2$1;->this$1:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$2;->this$0:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;->access$800(Lcom/sec/android/app/hwmoduletest/TempHumidDisplay;)Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/TempHumidDisplay$HistoryAdaptor;->notifyDataSetChanged()V

    .line 153
    :cond_1
    return-void
.end method
