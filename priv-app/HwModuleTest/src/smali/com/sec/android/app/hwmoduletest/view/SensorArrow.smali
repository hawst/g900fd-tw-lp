.class public Lcom/sec/android/app/hwmoduletest/view/SensorArrow;
.super Landroid/view/View;
.source "SensorArrow.java"


# static fields
.field private static final CENTER_X:I = 0x5a

.field private static final COMPASS_RADIUS:I = 0x46


# instance fields
.field private final COMPLETED_CAL:I

.field private final REAL_COMPLETED_CAL:I

.field private direction:F

.field private mArrowPaint:Landroid/graphics/Paint;

.field private mBGPaint:Landroid/graphics/Paint;

.field private mCurrentCal:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTextPaintCal:Landroid/graphics/Paint;

.field private mX:I

.field private needCalMsg:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 12
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->COMPLETED_CAL:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->REAL_COMPLETED_CAL:I

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->initCompassView()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->COMPLETED_CAL:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->REAL_COMPLETED_CAL:I

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->initCompassView()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 12
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->COMPLETED_CAL:I

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->REAL_COMPLETED_CAL:I

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->initCompassView()V

    .line 44
    return-void
.end method

.method private draw_arrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "p"    # Landroid/graphics/Paint;
    .param p3, "x"    # I

    .prologue
    const/4 v1, 0x2

    const/high16 v4, 0x428c0000    # 70.0f

    .line 93
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    if-ge v0, v1, :cond_1

    .line 94
    const/high16 v0, -0x10000

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    neg-float v0, v0

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 102
    int-to-float v1, p3

    const/4 v2, 0x0

    int-to-float v3, p3

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 103
    return-void

    .line 95
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    if-ne v0, v1, :cond_2

    .line 96
    const v0, -0xff0100

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 97
    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 98
    const v0, -0xffff01

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private initCompassView()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 52
    const/16 v0, 0x5a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    .line 53
    const-string v0, "Need for calibration"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->needCalMsg:Ljava/lang/String;

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mArrowPaint:Landroid/graphics/Paint;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mArrowPaint:Landroid/graphics/Paint;

    const v1, -0xff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mArrowPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mArrowPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mBGPaint:Landroid/graphics/Paint;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mBGPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaint:Landroid/graphics/Paint;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setCurrentCal(I)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->invalidate()V

    .line 70
    return-void
.end method


# virtual methods
.method public getDirection()F
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "cv"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v2, 0x428c0000    # 70.0f

    .line 81
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mBGPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mArrowPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->draw_arrow(Landroid/graphics/Canvas;Landroid/graphics/Paint;I)V

    .line 84
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->needCalMsg:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 86
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 90
    :goto_0
    return-void

    .line 88
    :cond_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mX:I

    add-int/lit8 v2, v2, 0x1e

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mTextPaintCal:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 75
    const/16 v0, 0xaa

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 76
    return-void
.end method

.method public setCurrentCal(I)V
    .locals 0
    .param p1, "cal"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->mCurrentCal:I

    .line 25
    return-void
.end method

.method public setDirection(F)V
    .locals 0
    .param p1, "direction"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->direction:F

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->invalidate()V

    .line 34
    return-void
.end method
