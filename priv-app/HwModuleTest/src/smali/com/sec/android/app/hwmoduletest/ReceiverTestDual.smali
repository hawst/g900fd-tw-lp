.class public Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "ReceiverTestDual.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final AUDIO_PATH_EAR:I = 0x3

.field public static final AUDIO_PATH_HDMI:I = 0x4

.field public static final AUDIO_PATH_OFF:I = 0x5

.field public static final AUDIO_PATH_RCV:I = 0x1

.field public static final AUDIO_PATH_RCV_PIEZO:I = 0x2

.field public static final AUDIO_PATH_SPK:I


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field public mAudioPath:I

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNormalRcvButton:Landroid/widget/ToggleButton;

.field private mPiezoRcvButton:Landroid/widget/ToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 48
    const-string v0, "LowFrequencyTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "rcv_piezo"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->AUDIO_PATH:[Ljava/lang/String;

    .line 49
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 174
    :cond_0
    return-void
.end method

.method private startMedia(I)V
    .locals 2
    .param p1, "audioPath"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    .line 154
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setAudioPath(I)V

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->release()V

    .line 156
    const v0, 0x7f050006

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 160
    return-void
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->release()V

    .line 167
    :cond_0
    return-void
.end method

.method private updateMedia(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    move-object v0, p1

    check-cast v0, Landroid/widget/ToggleButton;

    .line 136
    .local v0, "toggle":Landroid/widget/ToggleButton;
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->stopMedia()V

    .line 138
    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 139
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 148
    :goto_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->startMedia(I)V

    .line 150
    :cond_0
    return-void

    .line 141
    :pswitch_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    goto :goto_0

    .line 144
    :pswitch_1
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x7f0b01c4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateToggle(I)V
    .locals 3
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    const v0, 0x7f0b01c4

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    const v2, 0x7f0b01c5

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 132
    return-void

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method


# virtual methods
.method public isFolderOpen()Z
    .locals 4

    .prologue
    .line 110
    const/4 v0, 0x1

    .line 111
    .local v0, "bRet":Z
    const-string v2, "PATH_HALLIC_STATE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "strVal":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "isFolderOpen"

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    if-eqz v1, :cond_0

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 118
    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->updateToggle(I)V

    .line 124
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->updateMedia(Landroid/view/View;)V

    .line 125
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onConfigurationChanged"

    const-string v2, "set ButtonAlpa"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setButtonEnableAtHallIC()V

    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 95
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v0, 0x7f030069

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setContentView(I)V

    .line 55
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioManager:Landroid/media/AudioManager;

    .line 56
    const v0, 0x7f0b01c4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    .line 57
    const v0, 0x7f0b01c5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setButtonEnableAtHallIC()V

    .line 62
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->stopMedia()V

    .line 81
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setAudioPath(I)V

    .line 88
    return-void

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 66
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    .line 71
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioPath:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->setAudioPath(I)V

    .line 73
    return-void
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public setButtonEnableAtHallIC()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->isFolderOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setAlpha(F)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setAlpha(F)V

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mPiezoRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setAlpha(F)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ReceiverTestDual;->mNormalRcvButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setAlpha(F)V

    goto :goto_0
.end method
