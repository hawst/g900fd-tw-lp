.class Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;
.super Ljava/lang/Object;
.source "IRThermometerTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/IRThermometerTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/IRThermometerTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/IRThermometerTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;Lcom/sec/android/app/hwmoduletest/IRThermometerTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/IRThermometerTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/IRThermometerTest$1;

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 278
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 287
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 294
    :goto_0
    return-void

    .line 289
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;->this$0:Lcom/sec/android/app/hwmoduletest/IRThermometerTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    # invokes: Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getValueFromSensor([F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->access$100(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;[F)V

    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x1001e
        :pswitch_0
    .end packed-switch
.end method
