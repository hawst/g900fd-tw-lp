.class public Lcom/sec/android/app/hwmoduletest/BeamActivity;
.super Landroid/app/Activity;
.source "BeamActivity.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "BeamActivity"

.field public static final POSITION_KEY:Ljava/lang/String; = "position-task"

.field public static final TASK_KEY:Ljava/lang/String; = "beam-task"

.field private static beamFactory:Landroid/app/BarBeamFactory;


# instance fields
.field private barcodeByte:[[B

.field private barcodeImage:Landroid/widget/ImageView;

.field private barcodeTemp:[B

.field private beamTask:Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

.field private dataId:[Ljava/lang/String;

.field hadError:Z

.field handler:Landroid/os/Handler;

.field private hopSequenceArray:[Landroid/app/Hop;

.field private imageId:[I

.field private position:I

.field private textId:[Ljava/lang/String;

.field private viewStatus:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->beamFactory:Landroid/app/BarBeamFactory;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->handler:Landroid/os/Handler;

    .line 33
    const/16 v0, 0xa

    new-array v6, v0, [Landroid/app/Hop;

    const/4 v7, 0x0

    new-instance v0, Landroid/app/Hop;

    const/4 v1, 0x0

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x14

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    new-instance v0, Landroid/app/Hop;

    const/16 v1, 0x14

    const/16 v2, 0x14

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    new-instance v0, Landroid/app/Hop;

    const/4 v1, 0x2

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0xc

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    new-instance v0, Landroid/app/Hop;

    const/16 v1, 0x8

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    new-instance v0, Landroid/app/Hop;

    const/16 v1, 0xc

    const/16 v2, 0x1e

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    new-instance v0, Landroid/app/Hop;

    const/4 v1, 0x1

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0x10

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    new-instance v0, Landroid/app/Hop;

    const/16 v1, 0xe

    const/16 v2, 0x1e

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    new-instance v0, Landroid/app/Hop;

    const/4 v1, 0x4

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    new-instance v0, Landroid/app/Hop;

    const/16 v1, 0xa

    const/16 v2, 0x28

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    new-instance v0, Landroid/app/Hop;

    const/4 v1, 0x6

    const/16 v2, 0x32

    const/16 v3, 0x32

    const/16 v4, 0xa

    const/16 v5, 0x1f

    invoke-direct/range {v0 .. v5}, Landroid/app/Hop;-><init>(IIIII)V

    aput-object v0, v6, v7

    iput-object v6, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->hopSequenceArray:[Landroid/app/Hop;

    .line 46
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->imageId:[I

    .line 51
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "UPC-A"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EAN-13"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EAN-8"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Code 39"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Code 128"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Interleaved 2 of 5"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Codebar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GS1 Databar"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->textId:[Ljava/lang/String;

    .line 56
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "123456789012"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "1234567890128"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "12345670"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Code 39"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Code 128"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "12345678"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "A1234567890A"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "(01)00614141999996"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->dataId:[Ljava/lang/String;

    .line 61
    const/16 v0, 0x8

    new-array v0, v0, [[B

    const/4 v1, 0x0

    const/16 v2, 0xe

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0xe

    new-array v2, v2, [B

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xb

    new-array v2, v2, [B

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x15

    new-array v2, v2, [B

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x12

    new-array v2, v2, [B

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xb

    new-array v2, v2, [B

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x12

    new-array v2, v2, [B

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x11

    new-array v2, v2, [B

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeByte:[[B

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    return-void

    .line 46
    :array_0
    .array-data 4
        0x7f02000f
        0x7f020006
        0x7f020007
        0x7f020004
        0x7f020003
        0x7f02000c
        0x7f020005
        0x7f02000a
    .end array-data

    .line 61
    :array_1
    .array-data 1
        -0x1t
        -0x54t
        -0x25t
        0x21t
        0x5ct
        -0x63t
        0x42t
        -0x52t
        -0x25t
        -0x75t
        0x1at
        0x64t
        -0x66t
        -0x2t
    .end array-data

    nop

    :array_2
    .array-data 1
        -0x1t
        -0x53t
        -0x70t
        -0x4ft
        0x4et
        -0xbt
        -0x46t
        -0x53t
        -0x3bt
        -0x73t
        0x32t
        0x4dt
        -0x46t
        -0x1t
    .end array-data

    nop

    :array_3
    .array-data 1
        -0x1t
        -0x15t
        0x36t
        -0x38t
        0x57t
        0x2at
        -0x3bt
        0x7bt
        -0x4ft
        -0x55t
        -0x1t
    .end array-data

    :array_4
    .array-data 1
        -0x1t
        -0x9t
        0x44t
        0x51t
        0x17t
        0x51t
        0x45t
        -0x2bt
        0x1dt
        0x11t
        0x47t
        0x57t
        0x51t
        0x11t
        0x1dt
        0x54t
        0x74t
        0x57t
        0x44t
        0x5ft
        -0x1t
    .end array-data

    nop

    :array_5
    .array-data 1
        -0x1t
        -0x35t
        0x7bt
        -0x47t
        0x70t
        -0x51t
        0x65t
        0x37t
        -0x4et
        0x36t
        0x32t
        0x63t
        0x45t
        -0x68t
        -0x27t
        0x38t
        -0x59t
        -0x1t
    .end array-data

    nop

    :array_6
    .array-data 1
        -0x1t
        -0xbt
        0x2dt
        0x4ct
        -0x6at
        -0x4et
        -0x33t
        0x5at
        -0x67t
        0x2ft
        -0x1t
    .end array-data

    :array_7
    .array-data 1
        -0x1t
        -0x17t
        -0x4bt
        0x4dt
        0x5at
        0x4dt
        0x54t
        -0x4ct
        -0x53t
        0x6at
        0x5at
        0x56t
        0x54t
        -0x4bt
        0x56t
        0x53t
        0x6ft
        -0x1t
    .end array-data

    nop

    :array_8
    .array-data 1
        -0x22t
        -0x45t
        -0x38t
        0x7t
        -0x5at
        0x7at
        0x11t
        -0x43t
        -0x26t
        0x1ft
        -0x74t
        -0xat
        -0x1ct
        -0x80t
        -0x5dt
        -0x7ft
        -0x15t
    .end array-data
.end method


# virtual methods
.method public StartBeaming()V
    .locals 6

    .prologue
    .line 153
    const/4 v1, 0x0

    .line 156
    .local v1, "result":Z
    :try_start_0
    sget-object v2, Lcom/sec/android/app/hwmoduletest/BeamActivity;->beamFactory:Landroid/app/BarBeamFactory;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeTemp:[B

    invoke-virtual {v2, v3}, Landroid/app/BarBeamFactory;->StartBarBeamFactory([B)Z
    :try_end_0
    .catch Landroid/app/BarBeamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 162
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->viewStatus:Landroid/widget/TextView;

    const-string v3, "Beaming..."

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    const-string v2, "BeamActivity"

    const-string v3, "StartBeaming"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StartBarBeamFactory-- : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Landroid/app/BarBeamException;
    invoke-virtual {v0}, Landroid/app/BarBeamException;->printStackTrace()V

    goto :goto_0
.end method

.method public StopBeaming()V
    .locals 6

    .prologue
    .line 167
    const/4 v1, 0x0

    .line 168
    .local v1, "result":Z
    const-string v2, "BeamActivity"

    const-string v3, "StopBeaming"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StopBarBeamFactory ++ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :try_start_0
    sget-object v2, Lcom/sec/android/app/hwmoduletest/BeamActivity;->beamFactory:Landroid/app/BarBeamFactory;

    invoke-virtual {v2}, Landroid/app/BarBeamFactory;->StopBarBeamFactory()Z
    :try_end_0
    .catch Landroid/app/BarBeamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 176
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->viewStatus:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const-string v2, "BeamActivity"

    const-string v3, "StopBeaming"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "StopBarBeamFactory -- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Landroid/app/BarBeamException;
    const-string v2, "BeamActivity"

    const-string v3, "StopBeaming"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception in StopBarBeamFactory"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 119
    const v1, 0x7f030004

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->setContentView(I)V

    .line 120
    const v1, 0x7f0b0010

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeImage:Landroid/widget/ImageView;

    .line 121
    const v1, 0x7f0b0016

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->viewStatus:Landroid/widget/TextView;

    .line 122
    const v1, 0x7f0b0014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BeamActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/BeamActivity$1;-><init>(Lcom/sec/android/app/hwmoduletest/BeamActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const v1, 0x7f0b0015

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BeamActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/BeamActivity$2;-><init>(Lcom/sec/android/app/hwmoduletest/BeamActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "beam-task"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->beamTask:Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "position-task"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    .line 134
    const v1, 0x7f0b0011

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->dataId:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const v1, 0x7f0b0012

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->textId:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeByte:[[B

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeTemp:[B

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->barcodeImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->imageId:[I

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->position:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 140
    :try_start_0
    new-instance v1, Landroid/app/BarBeamFactory;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BeamActivity;->hopSequenceArray:[Landroid/app/Hop;

    invoke-direct {v1, p0, v2}, Landroid/app/BarBeamFactory;-><init>(Landroid/content/Context;[Landroid/app/Hop;)V

    sput-object v1, Lcom/sec/android/app/hwmoduletest/BeamActivity;->beamFactory:Landroid/app/BarBeamFactory;
    :try_end_0
    .catch Landroid/app/BarBeamException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e1":Landroid/app/BarBeamException;
    invoke-virtual {v0}, Landroid/app/BarBeamException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BeamActivity;->StopBeaming()V

    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 150
    return-void
.end method
