.class public Lcom/sec/android/app/hwmoduletest/view/ECGGraph;
.super Landroid/view/View;
.source "ECGGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private GRAPH_SCALING_COOR:F

.field private final INCREASING_COOR:F

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private final TEXT_SCALING_COOR:F

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mDataRange:Ljava/lang/String;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPathX:Landroid/graphics/Path;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    .line 24
    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INCREASING_COOR:F

    .line 25
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->TEXT_SCALING_COOR:F

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    .line 41
    const-string v0, "ECGGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->CLASS_NAME:Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->init(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    .line 24
    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INCREASING_COOR:F

    .line 25
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->TEXT_SCALING_COOR:F

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    .line 41
    const-string v0, "ECGGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->CLASS_NAME:Ljava/lang/String;

    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->init(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_X:I

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    .line 24
    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INCREASING_COOR:F

    .line 25
    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    .line 26
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->TEXT_SCALING_COOR:F

    .line 34
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    .line 41
    const-string v0, "ECGGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->CLASS_NAME:Ljava/lang/String;

    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->init(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v5, -0x1000000

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 59
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mContext:Landroid/content/Context;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 61
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mScreenWidth:I

    .line 62
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mScreenHeight:I

    .line 63
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mScreenHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    .line 64
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    int-to-float v1, v1

    const v2, 0x3eb33333    # 0.35f

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->LIST_SIZE:I

    .line 65
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 66
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 72
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 77
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->setSpecGraphScale()V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->invalidate()V

    .line 85
    return-void
.end method

.method private setPath()V
    .locals 7

    .prologue
    const/high16 v6, 0x41200000    # 10.0f

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 121
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    invoke-virtual {v2, v6, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 125
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    int-to-float v1, v0

    const v3, 0x3eb33333    # 0.35f

    mul-float/2addr v1, v3

    add-float v3, v6, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->invalidate()V

    .line 131
    return-void
.end method

.method private setSpecGraphScale()V
    .locals 4

    .prologue
    .line 134
    const-string v0, "ECGSENSOR_DATA_RANGE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mDataRange:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setSpecGraphScale"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rmDataRange : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mDataRange:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mDataRange:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mDataRange:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    :cond_0
    const v0, 0x3ba3d70a    # 0.005f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    .line 142
    :goto_0
    return-void

    .line 141
    :cond_1
    const/high16 v0, 0x43480000    # 200.0f

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mDataRange:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->GRAPH_SCALING_COOR:F

    goto :goto_0
.end method


# virtual methods
.method public addValue(F)V
    .locals 2
    .param p1, "x"    # F

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 116
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->setPath()V

    .line 117
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v9, 0x42480000    # 50.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v8, 0x41200000    # 10.0f

    .line 89
    const/4 v7, 0x0

    .line 92
    .local v7, "textx":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const v1, 0x42654ca3

    mul-float v7, v0, v1

    .line 98
    :goto_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 99
    const-string v0, "ECG :"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 100
    const/high16 v1, 0x42b40000    # 90.0f

    const/high16 v3, 0x432a0000    # 170.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 101
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x428c0000    # 70.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 103
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, -0xc8

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, 0xc8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v8

    move v3, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 105
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 107
    return-void

    .line 94
    :catch_0
    move-exception v6

    .line 95
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/ECGGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    const-string v3, "get value fail"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
