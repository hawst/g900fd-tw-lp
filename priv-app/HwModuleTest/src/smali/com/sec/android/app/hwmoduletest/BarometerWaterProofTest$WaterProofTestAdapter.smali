.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;
.super Landroid/widget/ArrayAdapter;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WaterProofTestAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 644
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 645
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 649
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;

    .line 652
    .local v0, "item":Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;
    if-nez p2, :cond_0

    .line 653
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$WaterProofTestAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 654
    .local v2, "li":Landroid/view/LayoutInflater;
    const v7, 0x7f03000a

    const/4 v8, 0x0

    invoke-virtual {v2, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    .line 660
    .end local v2    # "li":Landroid/view/LayoutInflater;
    .local v1, "layout":Landroid/widget/TableLayout;
    :goto_0
    const v7, 0x7f0b003c

    invoke-virtual {v1, v7}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 661
    .local v6, "timeView":Landroid/widget/TextView;
    const v7, 0x7f0b003d

    invoke-virtual {v1, v7}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 662
    .local v4, "tagView":Landroid/widget/TextView;
    const v7, 0x7f0b003e

    invoke-virtual {v1, v7}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 663
    .local v3, "pressureView":Landroid/widget/TextView;
    const v7, 0x7f0b003f

    invoke-virtual {v1, v7}, Landroid/widget/TableLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 664
    .local v5, "timeStampView":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 665
    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 666
    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 667
    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$HistoryItem;->getTimeStamp()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 668
    return-object v1

    .end local v1    # "layout":Landroid/widget/TableLayout;
    .end local v3    # "pressureView":Landroid/widget/TextView;
    .end local v4    # "tagView":Landroid/widget/TextView;
    .end local v5    # "timeStampView":Landroid/widget/TextView;
    .end local v6    # "timeView":Landroid/widget/TextView;
    :cond_0
    move-object v1, p2

    .line 657
    check-cast v1, Landroid/widget/TableLayout;

    .restart local v1    # "layout":Landroid/widget/TableLayout;
    goto :goto_0
.end method
