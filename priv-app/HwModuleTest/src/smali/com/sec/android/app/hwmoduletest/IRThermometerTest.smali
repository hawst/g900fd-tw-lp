.class public Lcom/sec/android/app/hwmoduletest/IRThermometerTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "IRThermometerTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;
    }
.end annotation


# static fields
.field public static final vendor:Ljava/lang/String;


# instance fields
.field public DELAY:I

.field public final MSG_FINISH_ONETEST:I

.field public final MSG_UPDATE_ONETEST:I

.field public final MSG_UPDATE_UI:I

.field public final PARTRON_ITEMS:I

.field private avg_length:I

.field private bodytempData_result:[Ljava/lang/String;

.field private getCount:I

.field private mAmbientTemp:F

.field private mAmbientTempText:Landroid/widget/TextView;

.field private mBodyTempGraph:Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;

.field private mBodyTempSensor:Landroid/hardware/Sensor;

.field private mBodyTempText:Landroid/widget/TextView;

.field private mBodyTempTitleText:Landroid/widget/TextView;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mHumiTempValue:Ljava/lang/String;

.field private mIRThermometerOnOffButton:Landroid/widget/Button;

.field private mIRThermometerOneTestButton:Landroid/widget/Button;

.field private mRawTaText:Landroid/widget/TextView;

.field private mRawTaTitleText:Landroid/widget/TextView;

.field private mRawToText:Landroid/widget/TextView;

.field private mRawToTitleText:Landroid/widget/TextView;

.field private final mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

.field private mTaDataText:Landroid/widget/TextView;

.field private mTaDataTitleText:Landroid/widget/TextView;

.field private mToDataText:Landroid/widget/TextView;

.field private mToDataTitleText:Landroid/widget/TextView;

.field private final mUIHandler:Landroid/os/Handler;

.field private result_to:Ljava/lang/String;

.field private showBodyTemp:Z

.field private showBodyTemp_onetime:Z

.field private sumCount:I

.field private sum_to:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "IR_THERMOMETER_SENSOR_VENDOR"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->vendor:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 79
    const-string v0, "IRThermometerTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->result_to:Ljava/lang/String;

    .line 34
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    .line 35
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    .line 37
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->avg_length:I

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp_onetime:Z

    .line 43
    new-instance v0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;Lcom/sec/android/app/hwmoduletest/IRThermometerTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;

    .line 48
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    .line 50
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->PARTRON_ITEMS:I

    .line 56
    const-string v0, "0,0,0"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mHumiTempValue:Ljava/lang/String;

    .line 58
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->MSG_UPDATE_UI:I

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->MSG_UPDATE_ONETEST:I

    .line 60
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->MSG_FINISH_ONETEST:I

    .line 61
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->DELAY:I

    .line 204
    new-instance v0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mUIHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/IRThermometerTest;[F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/IRThermometerTest;
    .param p1, "x1"    # [F

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getValueFromSensor([F)V

    return-void
.end method

.method private getValueFromSensor([F)V
    .locals 11
    .param p1, "rawdata"    # [F

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 153
    const-string v2, "PARTRON"

    sget-object v3, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->vendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp_onetime:Z

    if-eqz v2, :cond_6

    .line 154
    :cond_0
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    const/16 v3, 0xa

    if-ge v2, v3, :cond_1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    .line 156
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bodyTemp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", toData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", taData="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rawTo="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rawTa="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, p1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_6

    .line 160
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    .line 161
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    aget v3, p1, v7

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    .line 163
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_6

    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "averageTemp_to":F
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->avg_length:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mFormat:Ljava/text/DecimalFormat;

    float-to-double v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->result_to:Ljava/lang/String;

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Averaged To : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getValueFromSensor"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result_to : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->result_to:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_3

    .line 171
    if-ne v1, v7, :cond_2

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->result_to:Ljava/lang/String;

    aput-object v3, v2, v1

    .line 170
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mFormat:Ljava/text/DecimalFormat;

    aget v4, p1, v1

    float-to-double v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    goto :goto_1

    .line 179
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempGraph:Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v4, v4, v7

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v5, v5, v10

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;->addValueTemp(FFF)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mToDataText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mTaDataText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawToText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawTaText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_4
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp_onetime:Z

    if-eqz v2, :cond_5

    .line 189
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp_onetime:Z

    .line 190
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    invoke-virtual {v2, v8}, Landroid/widget/Button;->setClickable(Z)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 195
    :cond_5
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    .line 196
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    .line 202
    .end local v0    # "averageTemp_to":F
    .end local v1    # "i":I
    :cond_6
    return-void
.end method

.method private initialize()V
    .locals 4

    .prologue
    .line 97
    const v0, 0x7f0b013d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempTitleText:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0b013e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mToDataTitleText:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0b013f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mTaDataTitleText:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0b0140

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawToTitleText:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0b0141

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawTaTitleText:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0b014d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempText:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0b014e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mToDataText:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0b014f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mTaDataText:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0b0150

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawToText:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0b0151

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawTaText:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0b0139

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mAmbientTempText:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0b014b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    .line 111
    const v0, 0x7f0b014a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "initialize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Vendor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->vendor:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "PARTRON"

    sget-object v1, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    const-string v0, "PARTRON"

    sget-object v1, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->bodytempData_result:[Ljava/lang/String;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mToDataText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mTaDataText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawToText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mRawTaText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mAmbientTempText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v5, -0x777778

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 248
    :pswitch_0
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    if-nez v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click to ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 253
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click to OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp:Z

    .line 256
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getCount:I

    .line 257
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sumCount:I

    .line 258
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->sum_to:F

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOneTestButton:Landroid/widget/Button;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 265
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onClick"

    const-string v2, "click to One Test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->showBodyTemp_onetime:Z

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mIRThermometerOnOffButton:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0

    .line 245
    :pswitch_data_0
    .packed-switch 0x7f0b014a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v1, 0x7f03004a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->setContentView(I)V

    .line 86
    const v1, 0x7f0b0152

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempGraph:Lcom/sec/android/app/hwmoduletest/view/BodyTempGraph;

    .line 87
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1001e

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempSensor:Landroid/hardware/Sensor;

    .line 90
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 91
    .local v0, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 92
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.##"

    invoke-direct {v1, v2, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mFormat:Ljava/text/DecimalFormat;

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->initialize()V

    .line 94
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 148
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 149
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 143
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 144
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    .line 133
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 134
    const/4 v1, 0x1

    new-array v0, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0x9

    aput v2, v0, v1

    .line 135
    .local v0, "mSensorTempID":[I
    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorTemp:Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->DELAY:I

    invoke-virtual {v1, p0, v0, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorTemp;->SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mSensorListener:Lcom/sec/android/app/hwmoduletest/IRThermometerTest$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mBodyTempSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 138
    return-void
.end method

.method public onSensorValueReceived(ILjava/lang/String;)V
    .locals 4
    .param p1, "mSensor"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    sparse-switch p1, :sswitch_data_0

    .line 239
    :goto_0
    return-void

    .line 229
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOTI : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mUIHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 233
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onSensorValueReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID_MANAGER_TEMP : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mHumiTempValue:Ljava/lang/String;

    goto :goto_0

    .line 227
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public updateTempUI()V
    .locals 6

    .prologue
    .line 218
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mHumiTempValue:Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "TempCompRaw":[Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mAmbientTemp:F

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mAmbientTempText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mFormat:Ljava/text/DecimalFormat;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/IRThermometerTest;->mAmbientTemp:F

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    return-void
.end method
