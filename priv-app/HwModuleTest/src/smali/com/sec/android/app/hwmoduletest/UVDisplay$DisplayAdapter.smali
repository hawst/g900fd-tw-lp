.class public Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "UVDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/UVDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DisplayAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p4, "mItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    .line 244
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 245
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x2

    const v10, 0x7f0b00d3

    const/4 v9, 0x1

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 250
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;

    .line 251
    .local v1, "item":Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;
    if-nez p2, :cond_0

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 253
    .local v3, "li":Landroid/view/LayoutInflater;
    const v5, 0x7f030089

    invoke-virtual {v3, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 259
    .end local v3    # "li":Landroid/view/LayoutInflater;
    .local v2, "layout":Landroid/widget/LinearLayout;
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "MAXIM"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 260
    const/4 v5, 0x3

    new-array v4, v5, [Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v7

    const v5, 0x7f0b02d5

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v9

    const v5, 0x7f0b02d8

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v11

    .line 264
    .local v4, "txt_Contents":[Landroid/widget/TextView;
    const v5, 0x7f0b02d6

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    const v5, 0x7f0b02d7

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v5, v4

    if-ge v0, v5, :cond_4

    .line 285
    aget-object v5, v4, v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;->getItem(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .end local v0    # "i":I
    .end local v2    # "layout":Landroid/widget/LinearLayout;
    .end local v4    # "txt_Contents":[Landroid/widget/TextView;
    :cond_0
    move-object v2, p2

    .line 255
    check-cast v2, Landroid/widget/LinearLayout;

    .restart local v2    # "layout":Landroid/widget/LinearLayout;
    goto :goto_0

    .line 266
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "ADI"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 267
    const/4 v5, 0x4

    new-array v4, v5, [Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v7

    const v5, 0x7f0b02d6

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v9

    const v5, 0x7f0b02d7

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v11

    const/4 v6, 0x3

    const v5, 0x7f0b02d8

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v6

    .line 272
    .restart local v4    # "txt_Contents":[Landroid/widget/TextView;
    const v5, 0x7f0b02d5

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 273
    .end local v4    # "txt_Contents":[Landroid/widget/TextView;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "STMicroelectronics"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 274
    const/4 v5, 0x3

    new-array v4, v5, [Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v7

    const v5, 0x7f0b02d5

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v9

    const v5, 0x7f0b02d8

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v11

    .line 278
    .restart local v4    # "txt_Contents":[Landroid/widget/TextView;
    const v5, 0x7f0b02d6

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    const v5, 0x7f0b02d7

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 281
    .end local v4    # "txt_Contents":[Landroid/widget/TextView;
    :cond_3
    new-array v4, v9, [Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    aput-object v5, v4, v7

    .restart local v4    # "txt_Contents":[Landroid/widget/TextView;
    goto/16 :goto_1

    .line 288
    .restart local v0    # "i":I
    :cond_4
    return-object v2
.end method
