.class Lcom/sec/android/app/hwmoduletest/HallICTest$3;
.super Landroid/os/CountDownTimer;
.source "HallICTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/HallICTest;->HallICTestInit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HallICTest;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 421
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 427
    const-string v0, "HallICTest"

    const-string v1, "onFinish"

    const-string v2, "onFinish Countdown Timer End"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/HallICTest;->mIsPressedBackkey:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HallICTest;->access$002(Lcom/sec/android/app/hwmoduletest/HallICTest;Z)Z

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HallICTest;->mVibrator:Landroid/os/Vibrator;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->access$100(Lcom/sec/android/app/hwmoduletest/HallICTest;)Landroid/os/Vibrator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HallICTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HallICTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/HallICTest;->finish()V

    .line 431
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 425
    return-void
.end method
