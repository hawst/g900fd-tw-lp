.class public Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;
.super Landroid/app/Fragment;
.source "GestureTestMode1Fragment.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureTestMode1Fragment"

.field private static final TYPE_AIRMOTION:I = 0x2

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mLoggingFlag:Z

.field private static mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItem1Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 66
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 67
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z

    .line 71
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 74
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mValueList:Ljava/util/List;

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mHandler:Landroid/os/Handler;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mPowerManager:Landroid/os/PowerManager;

    .line 159
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 57
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z

    return v0
.end method

.method static synthetic access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    return-object v0
.end method

.method private initTextView()V
    .locals 4

    .prologue
    .line 125
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f030028

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mValueList:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00a2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mListView:Landroid/widget/ListView;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1Fragment;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 129
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onActivityCreated"

    const-string v2, "onActivityCreated start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->initTextView()V

    .line 95
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onCreate"

    const-string v2, "onCreate start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "sensorhub"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 81
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mPowerManager:Landroid/os/PowerManager;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mPowerManager:Landroid/os/PowerManager;

    const/16 v1, 0x1a

    const-string v2, "GestureTestMode2Display"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 84
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z

    .line 85
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onCreateView"

    const-string v2, "onCreateView start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const v0, 0x7f030027

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->stopGestureZmeanTest()V

    .line 113
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 114
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 115
    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 118
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_1

    .line 119
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 120
    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 122
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 104
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 107
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 98
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 100
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 101
    return-void
.end method

.method public setLogingFlag(Z)V
    .locals 0
    .param p1, "setLoggingFlag"    # Z

    .prologue
    .line 156
    sput-boolean p1, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z

    .line 157
    return-void
.end method

.method public starGestureZmeanTest()V
    .locals 4

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 135
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mLoggingFlag:Z

    if-eqz v0, :cond_1

    .line 136
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    const-string v1, "GestureZmean"

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 137
    const-string v0, "GestureTestMode1Fragment"

    const-string v1, "resume()"

    const-string v2, "new LogSensorData()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_1
    return-void
.end method

.method public stopGestureZmeanTest()V
    .locals 3

    .prologue
    .line 142
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    if-eqz v0, :cond_0

    .line 143
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 146
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 147
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 150
    :cond_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_2

    .line 151
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 153
    :cond_2
    return-void
.end method
