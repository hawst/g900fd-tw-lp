.class public Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "GyroscopeDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlertDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;
    .locals 3
    .param p0, "title"    # I

    .prologue
    .line 110
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;

    invoke-direct {v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;-><init>()V

    .line 111
    .local v0, "alertDialogFragment":Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 112
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 114
    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 120
    .local v0, "title":I
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "Choose DPS"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "250 dps"

    new-instance v3, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$3;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "500 dps"

    new-instance v3, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$2;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "2000 dps"

    new-instance v3, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
