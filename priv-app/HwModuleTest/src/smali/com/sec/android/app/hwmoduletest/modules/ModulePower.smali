.class public Lcom/sec/android/app/hwmoduletest/modules/ModulePower;
.super Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;
.source "ModulePower.java"


# static fields
.field public static final AUTO_BRIGHTNESS_MODE_OFF:I = 0x0

.field public static final AUTO_BRIGHTNESS_MODE_ON:I = 0x1

.field public static final REBOOT_MODE_DOWNLOAD:B = 0x1t

.field public static final REBOOT_MODE_NOMAL:B

.field private static mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;


# instance fields
.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 24
    const-string v0, "ModulePower"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModulePower"

    const-string v2, "Create ModulePower"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModulePower;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    return-object v0
.end method


# virtual methods
.method public doPartialWakeLock(Z)V
    .locals 6
    .param p1, "wake"    # Z

    .prologue
    const/4 v5, 0x1

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doPartialWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    if-ne p1, v5, :cond_2

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 99
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 100
    .local v0, "pm":Landroid/os/PowerManager;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Partial"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 104
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 114
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 111
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public doWakeLock(Z)V
    .locals 5
    .param p1, "wake"    # Z

    .prologue
    .line 73
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "doWakeLock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wake="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 77
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 78
    .local v0, "pm":Landroid/os/PowerManager;
    const v1, 0x3000001a

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 92
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 89
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0
.end method

.method public getBrightness()I
    .locals 5

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 156
    .local v0, "brightness":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBrightness"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "brightness="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return v0
.end method

.method public getScreenBrightnessMode()I
    .locals 5

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 185
    .local v0, "mode":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getScreenBrightnessMode"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return v0
.end method

.method public isBatteryAuthPass()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 313
    const-string v1, "BATTERY_AUTH"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScreenOn()Z
    .locals 2

    .prologue
    .line 196
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 197
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    return v1
.end method

.method public readApChipTemp()Ljava/lang/String;
    .locals 5

    .prologue
    .line 275
    const-string v1, "APCHIP_TEMP_ADC"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 276
    .local v0, "adc":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readApChipTemp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    return-object v0
.end method

.method public readBatteryAdcCal()Ljava/lang/String;
    .locals 5

    .prologue
    .line 291
    const-string v1, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, "adc_cal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryAdcCal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc_cal="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-object v0
.end method

.method public readBatteryCal()Ljava/lang/String;
    .locals 5

    .prologue
    .line 302
    const-string v1, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 303
    .local v0, "cal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryCal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cal="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    return-object v0
.end method

.method public readBatteryStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string v0, ""

    return-object v0
.end method

.method public readBatteryTemp()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 237
    const-string v3, "BATTERY_TEMP"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "temp":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readBatteryTemp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sysfs temp="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x3

    if-lt v3, v4, :cond_1

    .line 243
    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 244
    .local v1, "decimal":I
    invoke-virtual {v2, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 246
    .local v0, "data":I
    const/4 v3, 0x5

    if-lt v1, v3, :cond_0

    .line 247
    add-int/lit8 v0, v0, 0x1

    .line 253
    .end local v1    # "decimal":I
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "readBatteryTemp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "return temp="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    return-object v2

    .line 250
    .end local v0    # "data":I
    :cond_1
    invoke-virtual {v2, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .restart local v0    # "data":I
    goto :goto_0
.end method

.method public readBatteryTempAdc()Ljava/lang/String;
    .locals 5

    .prologue
    .line 269
    const-string v1, "BATTERY_TEMP_ADC"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "adc":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryTempAdc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    return-object v0
.end method

.method public readBatteryTempAdcRF2Chip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 287
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempAdcRTRChip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 282
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempRF2Chip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 265
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryTempRTRChip()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 260
    const/4 v0, 0x0

    return-object v0
.end method

.method public readBatteryVoltage()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x3

    .line 209
    const-string v1, "BATTERY_VOLT"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "voltage":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 211
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readBatteryVoltage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "voltage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    .line 215
    :goto_1
    return-object v1

    .line 211
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public readFuelGaugeSOC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 226
    const-string v1, "BATTERY_CAPACITY"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    .local v0, "soc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 229
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readFuelGaugeSOC"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "soc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    return-object v0
.end method

.method public readLux()Ljava/lang/String;
    .locals 5

    .prologue
    .line 190
    const-string v1, "LIGHT_SENSOR_LUX"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "lux":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readLux"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lux="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    return-object v0
.end method

.method public reboot(B)V
    .locals 6
    .param p1, "mode"    # B

    .prologue
    .line 43
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "reboot"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 45
    .local v1, "pm":Landroid/os/PowerManager;
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower$1;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower$1;-><init>(Lcom/sec/android/app/hwmoduletest/modules/ModulePower;Landroid/os/Looper;)V

    .line 55
    .local v0, "handler":Landroid/os/Handler;
    invoke-virtual {v0, p1, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 56
    return-void
.end method

.method public resetFuelGaugeIC()Z
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "resetFuelGaugeIC"

    const-string v2, "Fuel Gauge IC reset"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "FUEL_GAUGE_RESET"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public sendAlarmManagerOnOff(Z)V
    .locals 6
    .param p1, "alarm"    # Z

    .prologue
    .line 117
    const-string v0, "android.intent.action.START_FACTORY_TEST"

    .line 118
    .local v0, "RTC_OFF":Ljava/lang/String;
    const-string v1, "android.intent.action.STOP_FACTORY_TEST"

    .line 120
    .local v1, "RTC_ON":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "sendAlarmOnOffIntentandroid.intent.action.START_FACTORY_TEST"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->sendBroadcast(Landroid/content/Intent;)V

    .line 131
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 124
    :cond_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "sendAlarmOnOffIntentandroid.intent.action.STOP_FACTORY_TEST"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 127
    .restart local v2    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 129
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "sendAlarmOnOffIntent"

    const-string v5, "Invalid parameter"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBrightness(I)V
    .locals 5
    .param p1, "brightness"    # I

    .prologue
    .line 145
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setBrightness"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "brightness="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 148
    .local v0, "mPowerManager":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    .line 149
    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 151
    :cond_0
    return-void
.end method

.method public setDisplayColor(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setDisplayColor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "color="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 142
    return-void
.end method

.method public setScreenBrightnessMode(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setScreenBrightnessMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 180
    return-void
.end method

.method public setScreenState(Z)V
    .locals 6
    .param p1, "b"    # Z

    .prologue
    const/4 v5, 0x0

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "setScreenState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 164
    .local v0, "pm":Landroid/os/PowerManager;
    if-eqz p1, :cond_0

    .line 165
    invoke-virtual {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 167
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, v5}, Landroid/os/PowerManager;->userActivity(JZ)V

    .line 175
    :goto_0
    return-void

    .line 169
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 171
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_0
.end method

.method public sleep()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sleep"

    const-string v3, "Power Sleep"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doWakeLock(Z)V

    .line 67
    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doPartialWakeLock(Z)V

    .line 68
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 69
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 70
    return-void
.end method

.method public writeBatteryAdcCal(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writeBatteryAdcCal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v0, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 299
    return-void
.end method

.method public writeBatteryCal(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "writeBatteryCal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const-string v0, "BATTERY_VOLT_ADC_CAL"

    invoke-static {v0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 310
    return-void
.end method
