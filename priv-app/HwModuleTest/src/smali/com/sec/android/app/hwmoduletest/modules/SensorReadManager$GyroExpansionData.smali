.class public Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
.super Ljava/lang/Object;
.source "SensorReadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GyroExpansionData"
.end annotation


# instance fields
.field public mData:[S

.field public mNoiseBias:[F

.field public mRMSValue:[F

.field public mReturnValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    .line 346
    new-array v0, v1, [S

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    .line 347
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    .line 348
    return-void
.end method
