.class Lcom/sec/android/app/hwmoduletest/SensorTest$1;
.super Landroid/os/Handler;
.source "SensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v5, 0x7f070004

    const v4, 0x7f070001

    const/4 v8, 0x0

    .line 308
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 312
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$000(Lcom/sec/android/app/hwmoduletest/SensorTest;)B

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mTemp_raw:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getTempSensorValueString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHumi_raw:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getHumiSensorValueString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$500(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mAcceSensorValueText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getAccelerometerSensorString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorStatusText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getProximitySensorStatusString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$900(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorAdcAvgText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getProximitySensorAdcAvgString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$1100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Barometer:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 332
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotalArray:[F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1400(Lcom/sec/android/app/hwmoduletest/SensorTest;)[F

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1500(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v4

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getBarometerSensorPressureFloat()F
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$1600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)F

    move-result v4

    aput v4, v2, v3

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotalArray:[F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1400(Lcom/sec/android/app/hwmoduletest/SensorTest;)[F

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1500(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    move-result v4

    aget v3, v3, v4

    # += operator for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1716(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1508(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1500(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_2

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1700(Lcom/sec/android/app/hwmoduletest/SensorTest;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1500(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1802(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorPressureText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BAROMETER: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1800(Lcom/sec/android/app/hwmoduletest/SensorTest;)F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hPa"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " Handler()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mBaromPressureAvg : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureAvg:F
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1800(Lcom/sec/android/app/hwmoduletest/SensorTest;)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->countValue:I
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1502(Lcom/sec/android/app/hwmoduletest/SensorTest;I)I

    .line 346
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromPressureTotal:F
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1702(Lcom/sec/android/app/hwmoduletest/SensorTest;F)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorAltitudeText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getBarometerSensorAltitudeString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$2300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorLuxText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getLightSensorLuxString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$2500(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorAdcText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getLightSensorAdcString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$2700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mGyroSensorValueText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getGyroscopeSensorValueString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$2900(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorValueText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getMagneticSensorValueString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$3100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 365
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mMagnSensorRawValueText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getMagneticSensorRawValueString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$3300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 367
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mUltraSensorAdcDistanceValueText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getUltrasonicSensorAdcDistanceString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$3600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorAzimuthText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorAzimuthString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$3800(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorPitchText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorPitchString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$4000(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mOrieSensorRollText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorRollString()Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$4200(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->DisableLogs()V

    .line 383
    const-string v2, "270"

    const-string v3, "MAGNETIC_ROTATE_DEGREE"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mProduct:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "GT-N8013"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 386
    :cond_6
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->EnableLogs()V

    .line 387
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, " Handler()"

    const-string v4, "Feature 270"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v2

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorAzimuth:F
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$4600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)F

    move-result v0

    .line 389
    .local v0, "azimuth":F
    const/high16 v2, 0x43870000    # 270.0f

    add-float/2addr v0, v2

    .line 390
    const/high16 v2, 0x43b40000    # 360.0f

    rem-float/2addr v0, v2

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setDirection(F)V

    .line 396
    .end local v0    # "azimuth":F
    :goto_2
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->EnableLogs()V

    .line 398
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B
    invoke-static {v2, v8}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$002(Lcom/sec/android/app/hwmoduletest/SensorTest;B)B

    goto/16 :goto_0

    .line 348
    :catch_0
    move-exception v1

    .line 349
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$2200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 393
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorTask:Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    move-result-object v3

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorAzimuth:F
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->access$4600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setDirection(F)V

    goto :goto_2

    .line 404
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 406
    const-string v2, "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/SystemVibrator;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->FEED_BACK_PATTERN:[J
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4900(Lcom/sec/android/app/hwmoduletest/SensorTest;)[J

    move-result-object v3

    invoke-virtual {v2, v3, v8}, Landroid/os/SystemVibrator;->vibrate([JI)V

    .line 411
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBackground:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setBackGroundColor(I)V

    goto/16 :goto_0

    .line 419
    :pswitch_2
    const-string v2, "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 421
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/SystemVibrator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/SystemVibrator;->cancel()V

    .line 424
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBackground:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 425
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest$FingerPrintFragment;->setBackGroundColor(I)V

    goto/16 :goto_0

    .line 429
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mBaromSensorSettingAltitudeText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
