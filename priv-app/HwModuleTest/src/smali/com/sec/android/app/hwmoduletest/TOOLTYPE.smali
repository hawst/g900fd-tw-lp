.class final enum Lcom/sec/android/app/hwmoduletest/TOOLTYPE;
.super Ljava/lang/Enum;
.source "TspPatternStyleX.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/TOOLTYPE$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/hwmoduletest/TOOLTYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

.field public static final enum TOOLTYPE_FINGER_HOVER:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

.field public static final enum TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

.field public static final enum TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

.field public static final enum TOOLTYPE_NONE:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

.field private static mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;


# instance fields
.field private mColor:I

.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    .line 661
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    const-string v1, "TOOLTYPE_NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_NONE:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    new-instance v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    const-string v1, "TOOLTYPE_FINGER_TOUCH"

    const-string v2, "finger touch"

    const/high16 v3, -0x10000

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    new-instance v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    const-string v1, "TOOLTYPE_FINGER_HOVER"

    const-string v2, "finger hover"

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_HOVER:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .line 662
    new-instance v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    const-string v1, "TOOLTYPE_GLOVE_TOUCH"

    const-string v2, "glove touch"

    const v3, -0xffff01

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .line 660
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_NONE:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_FINGER_HOVER:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_GLOVE_TOUCH:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    aput-object v1, v0, v8

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->$VALUES:[Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .line 664
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->TOOLTYPE_NONE:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    sput-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "color"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 669
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 670
    iput-object p3, p0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mName:Ljava/lang/String;

    .line 671
    iput p4, p0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mColor:I

    .line 672
    return-void
.end method

.method public static getIsHoverToolType()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 690
    sget-object v1, Lcom/sec/android/app/hwmoduletest/TOOLTYPE$1;->$SwitchMap$com$sec$android$app$hwmoduletest$TOOLTYPE:[I

    sget-object v2, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 698
    :goto_0
    :pswitch_0
    return v0

    .line 692
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 690
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getToolTypeColor()I
    .locals 1

    .prologue
    .line 686
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mColor:I

    return v0
.end method

.method public static setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;)V
    .locals 0
    .param p0, "type"    # Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .prologue
    .line 675
    sput-object p0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .line 676
    return-void
.end method

.method public static setToolType(Lcom/sec/android/app/hwmoduletest/TOOLTYPE;Landroid/graphics/Paint;)V
    .locals 1
    .param p0, "type"    # Lcom/sec/android/app/hwmoduletest/TOOLTYPE;
    .param p1, "p"    # Landroid/graphics/Paint;

    .prologue
    .line 679
    sput-object p0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    .line 680
    if-eqz p1, :cond_0

    .line 681
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mType:Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    iget v0, v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mColor:I

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 683
    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/hwmoduletest/TOOLTYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 660
    const-class v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/hwmoduletest/TOOLTYPE;
    .locals 1

    .prologue
    .line 660
    sget-object v0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->$VALUES:[Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/hwmoduletest/TOOLTYPE;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/TOOLTYPE;->mName:Ljava/lang/String;

    return-object v0
.end method
