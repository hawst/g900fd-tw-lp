.class Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
.super Ljava/util/TimerTask;
.source "SensorTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTask"
.end annotation


# static fields
.field private static final ULTRASONIC_ESCAPE_CLOSEMODE:I = 0x3


# instance fields
.field private final TAG:Ljava/lang/String;

.field public ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:I

.field private mAcceSensorString:Ljava/lang/String;

.field private mAcceSensorValues:[F

.field private mAngle:[I

.field private mBaromAltitude:F

.field private mBaromPressure:F

.field private mBaromSensorValues:[F

.field private mBlueValue:I

.field private mCctValue:I

.field private mGestureSensorValues:[F

.field private mGreenValue:I

.field private mGyroSensorValues:[F

.field private mGyroSensor_P:F

.field private mGyroSensor_R:F

.field private mGyroSensor_Y:F

.field private mHumiSensorValues:[F

.field private mIsRunningTask:Z

.field private mLightSensorAdcValue:Ljava/lang/String;

.field private mLightSensorLuxValue:F

.field private mLightSensorManagerValue:F

.field private mMagnSensorValues:[F

.field private mMagnSensor_X:F

.field private mMagnSensor_Y:F

.field private mMagnSensor_Z:F

.field private mOrieSensorAzimuth:F

.field private mOrieSensorPitch:F

.field private mOrieSensorRoll:F

.field private mOrieSensorValues:[F

.field private mProxAdc:I

.field private mProxAvg:Ljava/lang/String;

.field private mProxStatus:I

.field private mRawData:[I

.field private mRedValue:I

.field private mTempSensorValues:[F

.field private mWhiteValue:I

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

.field private ultraAdcDistanceData:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x3

    .line 2021
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 2022
    const-string v0, "SensorTask"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->TAG:Ljava/lang/String;

    .line 2023
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mIsRunningTask:Z

    .line 2172
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorValues:[F

    .line 2174
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    .line 2175
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    .line 2213
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxStatus:I

    .line 2244
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromSensorValues:[F

    .line 2289
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    .line 2290
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    .line 2291
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRedValue:I

    .line 2292
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBlueValue:I

    .line 2293
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mCctValue:I

    .line 2417
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensorValues:[F

    .line 2433
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mTempSensorValues:[F

    .line 2437
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mHumiSensorValues:[F

    .line 2441
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGestureSensorValues:[F

    .line 2448
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensorValues:[F

    .line 2475
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorValues:[F

    .line 2510
    const-string v0, "ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorTest;Lcom/sec/android/app/hwmoduletest/SensorTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/SensorTest$1;

    .prologue
    .line 2021
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/SensorTest;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getProximitySensorAdcAvgString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getBarometerSensorPressureFloat()F

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getBarometerSensorAltitudeString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getLightSensorLuxString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getLightSensorAdcString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getGyroscopeSensorValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getTempSensorValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getMagneticSensorValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getMagneticSensorRawValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getUltrasonicSensorAdcDistanceString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorAzimuthString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorPitchString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getOrientationSensorRollString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorAzimuth:F

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getHumiSensorValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->resume()V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->pause()V

    return-void
.end method

.method static synthetic access$6100(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;Ljava/lang/Double;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;
    .param p1, "x1"    # Ljava/lang/Double;

    .prologue
    .line 2021
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->setBarometerSensorPressure(Ljava/lang/Double;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getAccelerometerSensorString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;

    .prologue
    .line 2021
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->getProximitySensorStatusString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private computeForCctValue(Ljava/lang/String;)I
    .locals 14
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v10, -0x402147ae147ae148L    # -0.48

    .line 2376
    const-string v5, ","

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2377
    .local v4, "rgbwString":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRedValue:I

    .line 2378
    const/4 v5, 0x1

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    .line 2379
    const/4 v5, 0x2

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBlueValue:I

    .line 2380
    const/4 v5, 0x3

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    .line 2381
    const/4 v1, -0x1

    .line 2383
    .local v1, "cctValue":I
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    const v6, 0xf230

    if-lt v5, v6, :cond_0

    .line 2384
    const/16 v1, 0xa8c

    :goto_0
    move v5, v1

    .line 2407
    :goto_1
    return v5

    .line 2386
    :cond_0
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRedValue:I

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBlueValue:I

    sub-int/2addr v5, v6

    int-to-double v6, v5

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-double v8, v5

    div-double/2addr v6, v8

    const-wide v8, 0x3fe4cccccccccccdL    # 0.65

    add-double v2, v6, v8

    .line 2387
    .local v2, "ccti":D
    iget v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    int-to-float v6, v6

    div-float v0, v5, v6

    .line 2390
    .local v0, "I_cf":F
    const-wide v6, 0x3fc3d70a3d70a3d7L    # 0.155

    cmpg-double v5, v2, v6

    if-gtz v5, :cond_1

    .line 2391
    const/16 v5, 0x1bb8

    goto :goto_1

    .line 2392
    :cond_1
    const-wide v6, 0x3ff6666666666666L    # 1.4

    cmpl-double v5, v2, v6

    if-ltz v5, :cond_2

    .line 2393
    const/16 v5, 0x9a3

    goto :goto_1

    .line 2396
    :cond_2
    float-to-double v6, v0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    cmpg-double v5, v6, v8

    if-lez v5, :cond_3

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v0, v5

    if-ltz v5, :cond_4

    .line 2397
    :cond_3
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v6, v12

    double-to-int v1, v6

    goto :goto_0

    .line 2399
    :cond_4
    const-wide v6, 0x3fd999999999999aL    # 0.4

    cmpg-double v5, v6, v2

    if-gtz v5, :cond_5

    const-wide v6, 0x3ff199999999999aL    # 1.1

    cmpg-double v5, v2, v6

    if-gtz v5, :cond_5

    .line 2400
    const-wide v6, 0x40a5180000000000L    # 2700.0

    const-wide v8, -0x400dfbe76c8b4396L    # -1.126

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-int v1, v6

    goto :goto_0

    .line 2402
    :cond_5
    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v6, v12

    double-to-int v1, v6

    goto :goto_0
.end method

.method private computeForLux(Ljava/lang/String;)I
    .locals 10
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 2350
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2351
    .local v2, "rgbwString":[Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    .line 2352
    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    .line 2353
    const/4 v1, -0x1

    .line 2354
    .local v1, "luxValue":I
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mWhiteValue:I

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 2357
    .local v0, "I_cf":F
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    .line 2358
    const/4 v3, 0x0

    .line 2371
    :goto_0
    return v3

    .line 2361
    :cond_0
    float-to-double v4, v0

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_1

    .line 2362
    const-wide v4, 0x3f9eb851eb851eb8L    # 0.03

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-double v6, v3

    const-wide v8, 0x3ff570a3d70a3d71L    # 1.34

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v1, v4

    :goto_1
    move v3, v1

    .line 2371
    goto :goto_0

    .line 2364
    :cond_1
    float-to-double v4, v0

    cmpg-double v3, v6, v4

    if-gez v3, :cond_2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_2

    .line 2365
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-double v4, v3

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    float-to-double v8, v0

    mul-double/2addr v6, v8

    const-wide v8, 0x3fdccccccccccccdL    # 0.45

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_1

    .line 2368
    :cond_2
    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGreenValue:I

    int-to-double v4, v3

    const-wide v6, 0x3fc70a3d70a3d70aL    # 0.18

    mul-double/2addr v4, v6

    const-wide v6, 0x401999999999999aL    # 6.4

    mul-double/2addr v4, v6

    float-to-double v6, v0

    div-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_1
.end method

.method private getAccelerometerSensorString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2205
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorString:Ljava/lang/String;

    return-object v0
.end method

.method private getBarometerSensorAltitudeString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ALTITUDE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromAltitude:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBarometerSensorPressureFloat()F
    .locals 1

    .prologue
    .line 2276
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromPressure:F

    return v0
.end method

.method private getBarometerSensorPressureString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BAROMETER: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromPressure:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hPa"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getGyroscopeSensorValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GYROSCOPE: Y: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_Y:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", P: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_P:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", R: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_R:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHumiSensorValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TEMP_HUMI : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mHumiSensorValues:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLightSensorAdcString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2327
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsRGBSensor:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2328
    const-string v0, "RGBSENSOR_SUPPORT_WHITE"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getBoolean(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Light Sensor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorAdcValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (R,G,B,W)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2343
    :goto_0
    return-object v0

    .line 2332
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Light Sensor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorAdcValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (R,G,B)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2335
    :cond_1
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->DisableLogs()V

    .line 2337
    const-string v0, "IS_DISPLAY_LIGHT_SENSOR_ADC_ONLY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2338
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->EnableLogs()V

    .line 2339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Light Sensor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorAdcValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Lux"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2342
    :cond_2
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->EnableLogs()V

    .line 2343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Light Sensor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorAdcValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Adc"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getLightSensorLuxString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2323
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Light Sensor: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorLuxValue:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lux"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMagneticSensorRawValueString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2466
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-le v0, v3, :cond_0

    .line 2467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADC : x: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", z: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->magAdcData:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$5500(Lcom/sec/android/app/hwmoduletest/SensorTest;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2470
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method private getMagneticSensorValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAGNETIC: 2, x: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_X:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_Y:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", z: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_Z:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOrientationSensorAzimuthString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2495
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AZIMUTH: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorAzimuth:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOrientationSensorPitchString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2499
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PITCH: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorPitch:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOrientationSensorRollString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2503
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ROLL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorRoll:F

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getProximitySensorAdcAvgString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADC: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAdc:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAvg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getProximitySensorStatusString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PROXIMITY: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxStatus:I

    if-nez v0, :cond_0

    const-string v0, "1.0"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "0.0"

    goto :goto_0
.end method

.method private getTempSensorValueString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2435
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TEMP_RAW : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mFormat:Ljava/text/DecimalFormat;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$1900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/text/DecimalFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mTempSensorValues:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUltrasonicSensorAdcDistanceString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2536
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 2537
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ADC : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Distance : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2539
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "ADC : 0 Distance : 0"

    goto :goto_0
.end method

.method private pause()V
    .locals 7

    .prologue
    const/16 v6, 0xd

    const/16 v5, 0xc

    const/16 v4, 0xb

    const/16 v3, 0xa

    const/4 v2, 0x1

    .line 2125
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mIsRunningTask:Z

    .line 2127
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2128
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 2131
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2132
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2135
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2136
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2139
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2140
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2144
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-ne v1, v2, :cond_4

    .line 2145
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Pause"

    const-string v3, "Ultrasonic end 0 write"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2147
    :try_start_0
    const-string v1, "ULTRASONIC_ADC_DISTANCE"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2152
    :cond_4
    :goto_0
    return-void

    .line 2148
    :catch_0
    move-exception v0

    .line 2149
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7200(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "onResume"

    const-string v3, "Exception accessing ultrasonic file"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private readToAccelerometerSensor()V
    .locals 10

    .prologue
    .line 2179
    :try_start_0
    const-string v3, "ACCEL_SENSOR_RAW"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2182
    .local v1, "rawDatas":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 2183
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 2184
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x2

    const/4 v5, 0x2

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 2185
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v6, 0x2

    aget v5, v5, v6

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    int-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v2, v4

    .line 2187
    .local v2, "realg":F
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    mul-int/lit8 v5, v5, -0x1

    aput v5, v3, v4

    .line 2189
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    mul-int/lit8 v5, v5, -0x1

    aput v5, v3, v4

    .line 2191
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v6, 0x2

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    add-int/lit8 v5, v5, -0x5a

    aput v5, v3, v4

    .line 2192
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACC Raw Data - x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", y: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", z: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nx-angle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", y-angle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    mul-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", z-angle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAngle:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorString:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2202
    .end local v1    # "rawDatas":[Ljava/lang/String;
    .end local v2    # "realg":F
    :goto_0
    return-void

    .line 2195
    :catch_0
    move-exception v0

    .line 2196
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 2197
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 2198
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 2199
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACCELEROMETER: 8, x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", y: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", z: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mRawData:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorString:Ljava/lang/String;

    goto :goto_0
.end method

.method private readToBarometerSensor()V
    .locals 2

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromSensorValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromPressure:F

    .line 2250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromSensorValues:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromAltitude:F

    .line 2251
    return-void
.end method

.method private readToGyroscopeSensor()V
    .locals 3

    .prologue
    const v2, 0x42652ee0

    .line 2423
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensorValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_Y:F

    .line 2424
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensorValues:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_P:F

    .line 2425
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensorValues:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensor_R:F

    .line 2426
    return-void
.end method

.method private readToLightSensor()V
    .locals 3

    .prologue
    .line 2298
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorManagerValue:F

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorLuxValue:F

    .line 2301
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2302
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mLightSensorSysfsPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 2305
    .local v0, "lightValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2312
    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorAdcValue:Ljava/lang/String;

    .line 2320
    .end local v0    # "lightValue":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private readToMagneticSensor()V
    .locals 2

    .prologue
    .line 2454
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensorValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_X:F

    .line 2455
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensorValues:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_Y:F

    .line 2456
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensorValues:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensor_Z:F

    .line 2457
    return-void
.end method

.method private readToOrientationSensor()V
    .locals 2

    .prologue
    .line 2481
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorAzimuth:F

    .line 2482
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorValues:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorPitch:F

    .line 2483
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorValues:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorRoll:F

    .line 2492
    return-void
.end method

.method private readToProximitySensor()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2217
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mProxSensorValue:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6400(Lcom/sec/android/app/hwmoduletest/SensorTest;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxStatus:I

    .line 2218
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Proximity_ADC:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7400(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2220
    :try_start_0
    const-string v1, "PROXI_SENSOR_ADC"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAdc:I

    .line 2222
    const-string v1, "PROXI_SENSOR_ADC_AVG"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAvg:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2229
    :cond_0
    :goto_0
    return-void

    .line 2224
    :catch_0
    move-exception v0

    .line 2225
    .local v0, "e":Ljava/lang/Exception;
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAdc:I

    .line 2226
    const-string v1, "0,0,0"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxAvg:Ljava/lang/String;

    goto :goto_0
.end method

.method private readToUltrasonicSensor()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 2513
    const-string v1, "ULTRASONIC_ADC_DISTANCE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    .line 2515
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2516
    .local v0, "UltraDistance":I
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:I

    if-gt v0, v1, :cond_2

    if-eqz v0, :cond_2

    .line 2517
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7802(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z

    .line 2518
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I
    invoke-static {v1, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7902(Lcom/sec/android/app/hwmoduletest/SensorTest;I)I

    .line 2529
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2530
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 2531
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$8000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " readToUltrasonicSensor()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ultraAdcDistanceData : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ultraAdcDistanceData:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2534
    :cond_1
    return-void

    .line 2519
    :cond_2
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->ULTRASONIC_MINIMUM_DETECTABLE_DISTANCE:I

    if-gt v0, v1, :cond_3

    if-nez v0, :cond_0

    .line 2520
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2521
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7908(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    .line 2522
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7900(Lcom/sec/android/app/hwmoduletest/SensorTest;)I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    .line 2523
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->isCloseMode:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7802(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z

    .line 2524
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mCloseOutBoundCount:I
    invoke-static {v1, v3}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7902(Lcom/sec/android/app/hwmoduletest/SensorTest;I)I

    goto :goto_0
.end method

.method private resume()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2111
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mIsRunningTask:Z

    .line 2114
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 2115
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6800(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resume"

    const-string v3, "Ultrasonic start 1 write"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2117
    :try_start_0
    const-string v1, "ULTRASONIC_ADC_DISTANCE"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2122
    :cond_0
    :goto_0
    return-void

    .line 2118
    :catch_0
    move-exception v0

    .line 2119
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6900(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Resume"

    const-string v3, "Exception accessing ultrasonic file"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setBarometerSensorPressure(Ljava/lang/Double;)V
    .locals 8
    .param p1, "pressure"    # Ljava/lang/Double;

    .prologue
    .line 2254
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 2255
    .local v0, "iPressure":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "setBarometerSensorPressure"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBarometerSensorPressure : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xd

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/SensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08001d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2261
    const-string v1, "BROME_SENSOR_SEAR_LEVEL_PRESSURE"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2262
    return-void
.end method

.method private declared-synchronized updateUI()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2155
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxStatus:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2156
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7302(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z

    .line 2157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2163
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2164
    monitor-exit p0

    return-void

    .line 2158
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mProxStatus:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7300(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2159
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsVibrate:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7302(Lcom/sec/android/app/hwmoduletest/SensorTest;Z)Z

    .line 2160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$7000(Lcom/sec/android/app/hwmoduletest/SensorTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 4
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 2026
    invoke-virtual {p1}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2027
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onAccuracyChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TYPE_MAGNETIC_FIELD accuracy : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2028
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mSensorArrow:Lcom/sec/android/app/hwmoduletest/view/SensorArrow;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$4700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Lcom/sec/android/app/hwmoduletest/view/SensorArrow;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/hwmoduletest/view/SensorArrow;->setCurrentCal(I)V

    .line 2030
    :cond_0
    return-void
.end method

.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 6
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2076
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->mIsSensorHubTest:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$100(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2080
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGestureSensorValues:[F

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v2, v2, v3

    aput v2, v0, v1

    .line 2081
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGestureSensorValues:[F

    iget-object v1, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v1, v1, v4

    aput v1, v0, v3

    .line 2082
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGestureSensorValues:[F

    iget-object v1, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v1, v1, v5

    aput v1, v0, v4

    .line 2083
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGestureSensorValues:[F

    iget-object v1, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    aput v1, v0, v5

    .line 2087
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6600(Lcom/sec/android/app/hwmoduletest/SensorTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onGetSensorHubData"

    const-string v2, "received sensorHub data"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2088
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 2033
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2072
    :goto_0
    :pswitch_0
    return-void

    .line 2035
    :pswitch_1
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mAcceSensorValues:[F

    goto :goto_0

    .line 2038
    :pswitch_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mBaromSensorValues:[F

    goto :goto_0

    .line 2043
    :pswitch_3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mLightSensorManagerValue:F

    goto :goto_0

    .line 2047
    :pswitch_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mGyroSensorValues:[F

    goto :goto_0

    .line 2050
    :pswitch_5
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mMagnSensorValues:[F

    goto :goto_0

    .line 2055
    :pswitch_6
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mOrieSensorValues:[F

    goto :goto_0

    .line 2058
    :pswitch_7
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mTempSensorValues:[F

    goto :goto_0

    .line 2061
    :pswitch_8
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mHumiSensorValues:[F

    goto :goto_0

    .line 2033
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2092
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->mIsRunningTask:Z

    if-eqz v0, :cond_1

    .line 2093
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$000(Lcom/sec/android/app/hwmoduletest/SensorTest;)B

    move-result v0

    if-nez v0, :cond_2

    .line 2094
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToAccelerometerSensor()V

    .line 2095
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToProximitySensor()V

    .line 2096
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToBarometerSensor()V

    .line 2097
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToLightSensor()V

    .line 2098
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToGyroscopeSensor()V

    .line 2099
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToMagneticSensor()V

    .line 2100
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToOrientationSensor()V

    .line 2101
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->use_Ultrasonic:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$3500(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->bReadUltrasonicData:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$6700(Lcom/sec/android/app/hwmoduletest/SensorTest;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2102
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->readToUltrasonicSensor()V

    .line 2103
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/SensorTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorTest;->work_thread:B
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/SensorTest;->access$002(Lcom/sec/android/app/hwmoduletest/SensorTest;B)B

    .line 2108
    :cond_1
    :goto_0
    return-void

    .line 2105
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SensorTest$SensorTask;->updateUI()V

    goto :goto_0
.end method
