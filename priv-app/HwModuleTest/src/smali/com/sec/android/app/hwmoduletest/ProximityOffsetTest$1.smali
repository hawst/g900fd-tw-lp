.class Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;
.super Landroid/os/Handler;
.source "ProximityOffsetTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 54
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 67
    :goto_0
    return-void

    .line 56
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mAdcView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$100(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandlersensor:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$000(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->getADC()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$200(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->readOffset()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 62
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mOffsetView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$200(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->readOffset()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
