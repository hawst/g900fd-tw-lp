.class Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;
.super Landroid/widget/ArrayAdapter;
.source "TempHumidThermistor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/TempHumidThermistor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryAdaptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 189
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 193
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;

    .line 196
    .local v5, "item":Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;
    if-nez p2, :cond_0

    .line 197
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryAdaptor;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 198
    .local v7, "li":Landroid/view/LayoutInflater;
    const v18, 0x7f03007b

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v7, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 203
    .end local v7    # "li":Landroid/view/LayoutInflater;
    .local v6, "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getCount()Ljava/lang/String;

    move-result-object v3

    .line 204
    .local v3, "countString":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getValue_thermistor_ap()Ljava/lang/String;

    move-result-object v8

    .line 205
    .local v8, "thermistor_apValue":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getValue_thermistor_pam()Ljava/lang/String;

    move-result-object v12

    .line 206
    .local v12, "thermistor_pamValue":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getValue_thermistor_batt()Ljava/lang/String;

    move-result-object v10

    .line 207
    .local v10, "thermistor_battValue":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getValue_thermistor_sensorhub_chg()Ljava/lang/String;

    move-result-object v16

    .line 208
    .local v16, "thermistor_sensorhub_chg":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/TempHumidThermistor$HistoryItem;->getValue_thermistor_sensorhub_batt()Ljava/lang/String;

    move-result-object v14

    .line 209
    .local v14, "thermistor_sensorhub_batt":Ljava/lang/String;
    const v18, 0x7f0b00d3

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 210
    .local v4, "countView":Landroid/widget/TextView;
    const v18, 0x7f0b024f

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 211
    .local v9, "thermistor_apView":Landroid/widget/TextView;
    const v18, 0x7f0b0250

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 212
    .local v13, "thermistor_pamView":Landroid/widget/TextView;
    const v18, 0x7f0b0251

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 213
    .local v11, "thermistor_battView":Landroid/widget/TextView;
    const v18, 0x7f0b024c

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 214
    .local v15, "thermistor_sensorhub_battView":Landroid/widget/TextView;
    const v18, 0x7f0b024d

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 215
    .local v17, "thermistor_sensorhub_pmicView":Landroid/widget/TextView;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    const/high16 v18, -0x10000

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 222
    const/high16 v18, -0x10000

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 223
    const v18, -0xffff01

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 224
    const/high16 v18, -0x1000000

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 225
    const/high16 v18, -0x1000000

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    .line 226
    return-object v6

    .end local v3    # "countString":Ljava/lang/String;
    .end local v4    # "countView":Landroid/widget/TextView;
    .end local v6    # "layout":Landroid/widget/LinearLayout;
    .end local v8    # "thermistor_apValue":Ljava/lang/String;
    .end local v9    # "thermistor_apView":Landroid/widget/TextView;
    .end local v10    # "thermistor_battValue":Ljava/lang/String;
    .end local v11    # "thermistor_battView":Landroid/widget/TextView;
    .end local v12    # "thermistor_pamValue":Ljava/lang/String;
    .end local v13    # "thermistor_pamView":Landroid/widget/TextView;
    .end local v14    # "thermistor_sensorhub_batt":Ljava/lang/String;
    .end local v15    # "thermistor_sensorhub_battView":Landroid/widget/TextView;
    .end local v16    # "thermistor_sensorhub_chg":Ljava/lang/String;
    .end local v17    # "thermistor_sensorhub_pmicView":Landroid/widget/TextView;
    :cond_0
    move-object/from16 v6, p2

    .line 200
    check-cast v6, Landroid/widget/LinearLayout;

    .restart local v6    # "layout":Landroid/widget/LinearLayout;
    goto/16 :goto_0
.end method
