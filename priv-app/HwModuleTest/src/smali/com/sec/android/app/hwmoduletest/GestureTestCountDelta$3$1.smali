.class Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;
.super Ljava/lang/Object;
.source "GestureTestCountDelta.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;

.field final synthetic val$fileChk:[Z

.field final synthetic val$fileList:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;[Z[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->this$1:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;

    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->val$fileChk:[Z

    iput-object p3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->val$fileList:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 164
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->val$fileChk:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->val$fileChk:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->this$1:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->val$fileList:[Ljava/lang/String;

    aget-object v3, v3, v0

    # invokes: Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->removeFile(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->access$400(Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;Ljava/lang/String;)V

    .line 164
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 171
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3$1;->this$1:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta$3;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/hwmoduletest/GestureTestCountDelta;->sendBroadcast(Landroid/content/Intent;)V

    .line 172
    return-void
.end method
