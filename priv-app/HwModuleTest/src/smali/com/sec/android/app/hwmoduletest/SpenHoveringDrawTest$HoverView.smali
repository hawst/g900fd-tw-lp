.class public Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;
.super Landroid/view/View;
.source "SpenHoveringDrawTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HoverView"
.end annotation


# instance fields
.field private MILLIS_IN_SEC:J

.field private col_height:F

.field private col_width:F

.field private isHovering:Z

.field private isInputLock:Z

.field private isTesteShouldAtOnce:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mHeightCross:F

.field private mLineFailPaint:Landroid/graphics/Paint;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mResultText:Ljava/lang/String;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field private mWidthBetween:F

.field private mWidthCross:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Landroid/content/Context;)V
    .locals 20
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 175
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    .line 176
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 151
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 152
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 153
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 154
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 163
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->col_height:F

    .line 164
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->col_width:F

    .line 165
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mWidthCross:F

    .line 166
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mWidthBetween:F

    .line 167
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mHeightCross:F

    .line 170
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    .line 171
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isTesteShouldAtOnce:Z

    .line 173
    const-wide/16 v2, 0x64

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->MILLIS_IN_SEC:J

    .line 177
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->setKeepScreenOn(Z)V

    .line 178
    const-string v2, "window"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    .line 180
    .local v13, "mDisplay":Landroid/view/Display;
    new-instance v15, Landroid/graphics/Point;

    invoke-direct {v15}, Landroid/graphics/Point;-><init>()V

    .line 181
    .local v15, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v13, v15}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 182
    iget v2, v15, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    .line 183
    iget v2, v15, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    .line 185
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mScreenWidth = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$100(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mScreenHeight = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$200(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nRect(float) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    int-to-float v5, v5

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v6

    const/high16 v19, 0x40c00000    # 6.0f

    mul-float v6, v6, v19

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v6, v6, v19

    sub-float/2addr v5, v6

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v6

    const/high16 v19, 0x40000000    # 2.0f

    mul-float v6, v6, v19

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    int-to-float v2, v2

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    const/high16 v4, 0x40c00000    # 6.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-int v14, v2

    .line 190
    .local v14, "nRect":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x40c00000    # 6.0f

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    const/4 v4, 0x0

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    add-int/lit8 v3, v14, -0x1

    int-to-float v3, v3

    div-float v16, v2, v3

    .line 192
    .local v16, "overLapWidth":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    int-to-float v2, v2

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    const/high16 v4, 0x40c00000    # 6.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    int-to-float v3, v14

    div-float v11, v2, v3

    .line 194
    .local v11, "height":F
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$400(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "nRect = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$500(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "overLapWidth = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$600(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "height = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 199
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    const/4 v4, 0x0

    invoke-static {v8, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 200
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 202
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 203
    .local v7, "transPainter":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    int-to-float v6, v6

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 205
    const v2, 0x7f070001

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->setBackgroundResource(I)V

    .line 207
    const/4 v9, 0x0

    .line 208
    .local v9, "currentX":F
    const/4 v10, 0x0

    .line 211
    .local v10, "currentY":F
    const/4 v2, 0x0

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    mul-float v9, v2, v3

    .line 212
    const/high16 v2, 0x40c00000    # 6.0f

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    mul-float v10, v2, v3

    .line 214
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v14, :cond_0

    .line 215
    new-instance v17, Landroid/graphics/RectF;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/RectF;-><init>()V

    .line 217
    .local v17, "r":Landroid/graphics/RectF;
    move-object/from16 v0, v17

    iput v9, v0, Landroid/graphics/RectF;->left:F

    .line 218
    move-object/from16 v0, v17

    iput v10, v0, Landroid/graphics/RectF;->top:F

    .line 219
    const/high16 v2, 0x40c00000    # 6.0f

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mNPixelForOneMM:F
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v2, v9

    move-object/from16 v0, v17

    iput v2, v0, Landroid/graphics/RectF;->right:F

    .line 220
    add-float v2, v10, v11

    move-object/from16 v0, v17

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 222
    add-float v9, v9, v16

    .line 223
    add-float/2addr v10, v11

    .line 225
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$700(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HoverView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currentX = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", currentY = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$800(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/util/LinkedList;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 231
    .end local v17    # "r":Landroid/graphics/RectF;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->setPaint()V

    .line 232
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->initRect()V

    .line 233
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawTeachingPoint()V

    .line 234
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isHovering:Z

    .line 236
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 237
    new-instance v18, Ljava/util/Timer;

    invoke-direct/range {v18 .. v18}, Ljava/util/Timer;-><init>()V

    .line 238
    .local v18, "timer":Ljava/util/Timer;
    new-instance v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView$1;-><init>(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->MILLIS_IN_SEC:J

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 245
    .end local v18    # "timer":Ljava/util/Timer;
    :cond_1
    return-void
.end method

.method static synthetic access$902(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;
    .param p1, "x1"    # Z

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    return p1
.end method

.method private checkCrossRectRegion(FFLandroid/graphics/Paint;)V
    .locals 7
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 469
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$800(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;

    .line 470
    .local v2, "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    iget-boolean v3, v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->isTouched:Z

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 471
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->invalidate()V

    .line 473
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->isTouched:Z

    .line 474
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$800(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 475
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1400(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "checkCrossRectRegion"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Touched Area: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 478
    .end local v1    # "index":I
    .end local v2    # "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    :cond_1
    return-void
.end method

.method private checkPassNfinishSpenTest()V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isPassCross()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1200(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawResultText(Z)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->setResult(I)V

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->passAndFinish()V

    .line 460
    :cond_0
    return-void
.end method

.method private drawByEvent(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 330
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 332
    .local v6, "action":I
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-ne v0, v5, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 350
    :pswitch_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_4

    .line 351
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 352
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 353
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 354
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 355
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_2

    .line 356
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 357
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawLine(FFFFZ)V

    .line 358
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawTeachingPoint()V

    .line 350
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 336
    .end local v7    # "i":I
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 337
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 338
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_0

    .line 339
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 340
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    invoke-direct {p0, v0, v1, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawPoint(FFZ)V

    .line 341
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawHoverText(Z)V

    .line 342
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawTeachingPoint()V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1100(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1102(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z

    .line 346
    :cond_3
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isHovering:Z

    goto :goto_0

    .line 361
    .restart local v7    # "i":I
    :cond_4
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 362
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 363
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 364
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 365
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1100(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestStart:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1102(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z

    .line 369
    :cond_5
    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isHovering:Z

    .line 370
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 371
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawLine(FFFFZ)V

    .line 372
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawTeachingPoint()V

    goto/16 :goto_0

    .line 376
    .end local v7    # "i":I
    :pswitch_3
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isHovering:Z

    if-eqz v0, :cond_7

    .line 377
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 378
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 379
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 380
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 381
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_6

    .line 382
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    invoke-direct {p0, v0, v1, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawPoint(FFZ)V

    .line 384
    :cond_6
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isHovering:Z

    .line 386
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawHoverText(Z)V

    goto/16 :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawHoverText(Z)V
    .locals 6
    .param p1, "result"    # Z

    .prologue
    .line 481
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 483
    .local v5, "paint":Landroid/graphics/Paint;
    if-eqz p1, :cond_0

    .line 484
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->setBackgroundResource(I)V

    .line 485
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080264

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mResultText:Ljava/lang/String;

    .line 486
    const/high16 v0, -0x1000000

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 487
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 488
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 489
    const/high16 v0, 0x42a00000    # 80.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mResultText:Ljava/lang/String;

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    div-int/lit8 v2, v2, 0x8

    mul-int/lit8 v2, v2, 0x5

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    div-int/lit8 v3, v3, 0x8

    mul-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 499
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->invalidate()V

    .line 500
    return-void

    .line 492
    :cond_0
    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->setBackgroundResource(I)V

    .line 493
    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v1}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 494
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    div-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    div-int/lit8 v2, v2, 0x8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    const/high16 v2, 0x41200000    # 10.0f

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    div-int/lit8 v3, v3, 0x8

    mul-int/lit8 v3, v3, 0x8

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    div-int/lit8 v4, v4, 0x8

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawLine(FFFFZ)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "isHover"    # Z

    .prologue
    .line 392
    const/4 v0, 0x1

    if-ne p5, v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 397
    :goto_0
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 399
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_1

    .line 400
    float-to-int v6, p1

    .line 401
    float-to-int v8, p3

    .line 407
    :goto_1
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_2

    .line 408
    float-to-int v7, p2

    .line 409
    float-to-int v9, p4

    .line 415
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->invalidate()V

    .line 416
    return-void

    .line 395
    .end local v6    # "highX":I
    .end local v7    # "highY":I
    .end local v8    # "lowX":I
    .end local v9    # "lowY":I
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 403
    .restart local v6    # "highX":I
    .restart local v7    # "highY":I
    .restart local v8    # "lowX":I
    .restart local v9    # "lowY":I
    :cond_1
    float-to-int v6, p3

    .line 404
    float-to-int v8, p1

    goto :goto_1

    .line 411
    :cond_2
    float-to-int v7, p4

    .line 412
    float-to-int v9, p2

    goto :goto_2
.end method

.method private drawPoint(FFZ)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "isHover"    # Z

    .prologue
    .line 419
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 425
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->invalidate()V

    .line 426
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 449
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->checkCrossRectRegion(FFLandroid/graphics/Paint;)V

    .line 450
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->checkPassNfinishSpenTest()V

    .line 451
    return-void
.end method

.method private drawResultText(Z)V
    .locals 5
    .param p1, "result"    # Z

    .prologue
    const/4 v3, 0x1

    .line 503
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 505
    .local v0, "paint":Landroid/graphics/Paint;
    if-ne p1, v3, :cond_0

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mResultText:Ljava/lang/String;

    .line 507
    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 513
    :goto_0
    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 514
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 515
    const/high16 v1, 0x43160000    # 150.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 516
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mResultText:Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    div-int/lit8 v3, v3, 0x8

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    div-int/lit8 v4, v4, 0x8

    mul-int/lit8 v4, v4, 0x6

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->invalidate()V

    .line 519
    return-void

    .line 509
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mResultText:Ljava/lang/String;

    .line 510
    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private drawTeachingPoint()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 439
    const/4 v2, 0x5

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 440
    .local v1, "pxsize":F
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 441
    .local v0, "mCirclePaint":Landroid/graphics/Paint;
    const/high16 v2, 0x40800000    # 4.0f

    div-float v2, v1, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 442
    const/high16 v2, -0x10000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 443
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 444
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenWidth:I

    int-to-float v3, v3

    div-float/2addr v3, v5

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mScreenHeight:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 446
    return-void
.end method

.method private initRect()V
    .locals 5

    .prologue
    .line 429
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 430
    .local v1, "mRectPaintCross":Landroid/graphics/Paint;
    const/high16 v3, -0x1000000

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 431
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 433
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->mRectList:Ljava/util/LinkedList;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$800(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/util/LinkedList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;

    .line 434
    .local v2, "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 436
    .end local v2    # "r":Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$RectNode;
    :cond_0
    return-void
.end method

.method private setPaint()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 248
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 254
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mLineFailPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 258
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mClickPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 262
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 267
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 321
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 322
    .local v0, "action":I
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 326
    :goto_0
    return v3

    .line 325
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawByEvent(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 271
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 272
    .local v6, "action":I
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 316
    :cond_0
    :goto_0
    return v8

    .line 275
    :cond_1
    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 277
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 279
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    invoke-direct {p0, v0, v1, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawPoint(FFZ)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0, v8}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1002(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z

    .line 282
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawResultText(Z)V

    goto :goto_0

    .line 286
    :pswitch_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 287
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 288
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 289
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 290
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 291
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawLine(FFFFZ)V

    .line 286
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 293
    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    .line 294
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    .line 295
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 297
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawLine(FFFFZ)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0, v8}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1002(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z

    .line 300
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawResultText(Z)V

    goto/16 :goto_0

    .line 304
    .end local v7    # "i":I
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    .line 305
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    .line 306
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->mTouchedY:F

    invoke-direct {p0, v0, v1, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawPoint(FFZ)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->isInputLock:Z

    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # setter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->isTestFinish:Z
    invoke-static {v0, v8}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1002(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;Z)Z

    .line 309
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->drawResultText(Z)V

    goto/16 :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public passAndFinish()V
    .locals 3

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->access$1300(Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "passAndFinish"

    const-string v2, "Test Passed!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest$HoverView;->this$0:Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/SpenHoveringDrawTest;->finish()V

    .line 466
    return-void
.end method
