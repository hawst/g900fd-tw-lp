.class public Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TestCase"
.end annotation


# static fields
.field public static final ACCSENSOR_DATA_RANGE:Ljava/lang/String; = "ACCSENSOR_DATA_RANGE"

.field public static final AT_BATTEST_RESET_WHEN_READ:Ljava/lang/String; = "AT_BATTEST_RESET_WHEN_READ"

.field public static final CROSSTALK_VALUE_DIRECTION:Ljava/lang/String; = "CROSSTALK_VALUE_DIRECTION"

.field public static final ECGSENSOR_DATA_RANGE:Ljava/lang/String; = "ECGSENSOR_DATA_RANGE"

.field public static final GRIPSENSOR_DUAL_MODE:Ljava/lang/String; = "GRIPSENSOR_DUAL_MODE"

.field public static final HALLIC_TEST_MILLIS_SEC:Ljava/lang/String; = "HALLIC_TEST_MILLIS_SEC"

.field public static final HALL_IC_TEST:Ljava/lang/String; = "HALL_IC_TEST"

.field public static final HALL_IC_TEST_2ND:Ljava/lang/String; = "HALL_IC_TEST_2ND"

.field public static final IS_ASAHI_USING_SYSFS:Ljava/lang/String; = "IS_ASAHI_USING_SYSFS"

.field public static final IS_ATMEL_TSP_IC_TOUCHKEY:Ljava/lang/String; = "IS_ATMEL_TSP_IC_TOUCHKEY"

.field public static final IS_BACKLIGHT_ON_FOR_GREEN_LCD:Ljava/lang/String; = "IS_BACKLIGHT_ON_FOR_GREEN_LCD"

.field public static final IS_DISPLAY_LIGHT_SENSOR_ADC_ONLY:Ljava/lang/String; = "IS_DISPLAY_LIGHT_SENSOR_ADC_ONLY"

.field public static final IS_DISPLAY_LIGHT_SENSOR_SLOW:Ljava/lang/String; = "IS_DISPLAY_LIGHT_SENSOR_SLOW"

.field public static final IS_DISPLAY_LUX_ADC:Ljava/lang/String; = "IS_DISPLAY_LUX_ADC"

.field public static final IS_DUMMYKEY_SAME_KEYCODE:Ljava/lang/String; = "IS_DUMMYKEY_SAME_KEYCODE"

.field public static final IS_PROXIMITY_TEST_MOTOR_FEEDBACK:Ljava/lang/String; = "IS_PROXIMITY_TEST_MOTOR_FEEDBACK"

.field public static final IS_SENSOR_TEST_ACC_REVERSE:Ljava/lang/String; = "IS_SENSOR_TEST_ACC_REVERSE"

.field public static final IS_SIMPLE_TEST_ACC_SYSFS:Ljava/lang/String; = "IS_SIMPLE_TEST_ACC_SYSFS"

.field public static final IS_TSP_SECOND_LCD_TEST:Ljava/lang/String; = "IS_TSP_SECOND_LCD_TEST"

.field public static final IS_USING_SENSOR_MANAGER_IN_POWER_NOISE:Ljava/lang/String; = "IS_USING_SENSOR_MANAGER_IN_POWER_NOISE"

.field public static final IS_VIBETONZ_UNSUPPORTED:Ljava/lang/String; = "IS_VIBETONZ_UNSUPPORTED"

.field public static final MAGNETIC_POWERNOISE_GRAPH_Y_RANGE:Ljava/lang/String; = "MAGNETIC_POWERNOISE_GRAPH_Y_RANGE"

.field public static final READ_TARGET_GEOMAGNETIC:Ljava/lang/String; = "READ_TARGET_GEOMAGNETIC"

.field public static final RVALUE_SPO2_IN_HRM_TEST:Ljava/lang/String; = "RVALUE_SPO2_IN_HRM_TEST"

.field public static final SENSOR_TEST_ACC_BIT:Ljava/lang/String; = "SENSOR_TEST_ACC_BIT"

.field public static final TAG:Ljava/lang/String; = "TestCase"

.field public static final TEMPORARY_USE_NON_TZ:Ljava/lang/String; = "TEMPORARY_USE_NON_TZ"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "attr"    # Ljava/lang/String;
    .param p2, "deafult"    # Ljava/lang/String;

    .prologue
    .line 187
    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    invoke-static {p0, p1, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$300(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "ret":Ljava/lang/String;
    const-string v1, "Unknown"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p2

    .line 191
    .end local v0    # "ret":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public static getEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 162
    const-string v0, "enable"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getKeyTextSize(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 183
    const-string v0, "size"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$200(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 166
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getViewPointF(Ljava/lang/String;)Landroid/graphics/PointF;
    .locals 4
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 170
    const-string v3, "point"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "position":Ljava/lang/String;
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 173
    .local v0, "point":Landroid/graphics/PointF;
    if-eqz v2, :cond_0

    .line 174
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "pointStr":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Landroid/graphics/PointF;->x:F

    .line 176
    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 179
    .end local v1    # "pointStr":[Ljava/lang/String;
    :cond_0
    return-object v0
.end method
