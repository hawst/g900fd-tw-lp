.class public Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;
.super Landroid/app/Activity;
.source "GyroscopeSensorTest_M.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "GyroscopeSensorTest"


# instance fields
.field private IS_Q1_MODEL:Z

.field private mGyroSelfTestLayout:Landroid/widget/TableLayout;

.field private mGyroZeroRateText:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mSelfTestResults:[Ljava/lang/String;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mZeroRatePass:Z

.field private txt_X:Landroid/widget/TextView;

.field private txt_Y:Landroid/widget/TextView;

.field private txt_Z:Landroid/widget/TextView;

.field private txt_diff_x:Landroid/widget/TextView;

.field private txt_diff_y:Landroid/widget/TextView;

.field private txt_diff_z:Landroid/widget/TextView;

.field private txt_fifo_result:Landroid/widget/TextView;

.field private txt_fifo_subject:Landroid/widget/TextView;

.field private txt_prime_x:Landroid/widget/TextView;

.field private txt_prime_y:Landroid/widget/TextView;

.field private txt_prime_z:Landroid/widget/TextView;

.field private txt_sub1:Landroid/widget/TextView;

.field private txt_sub2:Landroid/widget/TextView;

.field private txt_sub3:Landroid/widget/TextView;

.field private txt_xyz_add:Landroid/widget/TextView;

.field private txt_xyz_x:Landroid/widget/TextView;

.field private txt_xyz_y:Landroid/widget/TextView;

.field private txt_xyz_z:Landroid/widget/TextView;

.field private txt_xyzprime_add:Landroid/widget/TextView;

.field private txtresult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mZeroRatePass:Z

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mHandler:Landroid/os/Handler;

    .line 57
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->IS_Q1_MODEL:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;)Landroid/widget/TableLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mSelfTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->showTestResults([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0b00e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_add:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0b00e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyzprime_add:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0b00db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_X:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0b00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_Y:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_Z:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0b00c8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_sub1:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0b00ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_sub2:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0b00e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_sub3:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0b00de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_x:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0b00df

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_y:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0b00e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_z:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0b00e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_x:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_y:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0b00e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_z:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0b00e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_x:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0b00e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_y:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0b00e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_z:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0b00d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_fifo_subject:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0b00c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_fifo_result:Landroid/widget/TextView;

    .line 88
    return-void
.end method

.method private showTestResults([Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "results"    # [Ljava/lang/String;
    .param p2, "resultValue"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/high16 v4, -0x10000

    const/4 v3, 0x0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_x:Landroid/widget/TextView;

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_y:Landroid/widget/TextView;

    aget-object v1, p1, v5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_xyz_z:Landroid/widget/TextView;

    aget-object v1, p1, v6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_x:Landroid/widget/TextView;

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_prime_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_x:Landroid/widget/TextView;

    aget-object v1, p1, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txt_diff_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v6

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    if-eqz p2, :cond_0

    .line 150
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mZeroRatePass:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 153
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->setResult(I)V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 157
    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->setResult(I)V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->setContentView(I)V

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->init()V

    .line 64
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 91
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 92
    const/4 v1, 0x0

    .line 93
    .local v1, "resultValue":Ljava/lang/String;
    const/4 v2, 0x1

    .line 94
    .local v2, "resultValue_Int":I
    const v4, 0x7f0b00d9

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    .line 95
    const v4, 0x7f0b00c7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableLayout;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    invoke-virtual {v4, v6}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 98
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v6, "/sys/class/sensors/gyro_sensor/selftest"

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x2000

    const/4 v7, 0x0

    invoke-static {v4, v6, v7}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "selfTestResult":Ljava/lang/String;
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mSelfTestResults:[Ljava/lang/String;

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mSelfTestResults:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v4, v4, v6

    if-eqz v4, :cond_0

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mSelfTestResults:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v4, v4, v6

    const-string v6, "1"

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mZeroRatePass:Z

    .line 111
    :cond_0
    iget-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mZeroRatePass:Z

    if-eqz v4, :cond_2

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    const-string v5, "Zero Rate Level Check:  PASS"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    const v5, -0xffff01

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 114
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->txtresult:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    .end local v3    # "selfTestResult":Ljava/lang/String;
    :goto_1
    return-void

    .restart local v3    # "selfTestResult":Ljava/lang/String;
    :cond_1
    move v4, v5

    .line 108
    goto :goto_0

    .line 122
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    const-string v5, "Zero Rate Level Check:  FAIL"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mGyroZeroRateText:Landroid/widget/TextView;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 124
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M$2;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeSensorTest_M;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 130
    .end local v3    # "selfTestResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
