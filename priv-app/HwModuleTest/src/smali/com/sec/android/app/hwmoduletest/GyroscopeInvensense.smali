.class public Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeInvensense.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mGyroSpec_Max:I

.field private mGyroSpec_Min:I

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_HW_Self_X:Landroid/widget/TextView;

.field private mText_HW_Self_Y:Landroid/widget/TextView;

.field private mText_HW_Self_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Noise_Bias_X:Landroid/widget/TextView;

.field private mText_Noise_Bias_Y:Landroid/widget/TextView;

.field private mText_Noise_Bias_Z:Landroid/widget/TextView;

.field private mText_Noise_Power_X:Landroid/widget/TextView;

.field private mText_Noise_Power_Y:Landroid/widget/TextView;

.field private mText_Noise_Power_Z:Landroid/widget/TextView;

.field private mText_Temperature:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "GyroscopeInvensense"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mSenserID:[I

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->WHAT_UPDATE:I

    .line 33
    const-string v0, "GyroscopeInvensense"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    .line 63
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->update()V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 73
    const v0, 0x7f0b00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Initialized:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0b00ef

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTableRow_Initialized:Landroid/widget/TableRow;

    .line 76
    const v0, 0x7f0b00f1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Temperature:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0b00f2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_X:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0b00f3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0b00f4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0b00f5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_X:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0b00f6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Y:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0b00f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Z:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0b00f8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_X:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0b00f9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_Y:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0b00fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_Z:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTextResult:Landroid/widget/TextView;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTableRow_Initialized:Landroid/widget/TableRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 92
    return-void
.end method

.method private update()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, v5, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    .line 96
    .local v1, "feature":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "feature : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "data":[Ljava/lang/String;
    const-string v4, ""

    .line 99
    .local v4, "tempdata":Ljava/lang/String;
    const/4 v2, 0x1

    .line 102
    .local v2, "isPass":Z
    const-string v5, "INVENSENSE_MPU6050"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "INVENSENSE_MPU6051"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "INVENSENSE_MPU6051M"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Initialized:Landroid/widget/TextView;

    const-string v6, "1"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    sget v6, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    invoke-virtual {v5, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getData(I)[Ljava/lang/String;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_9

    .line 113
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Temperature : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Temperature:Landroid/widget/TextView;

    aget-object v6, v0, v10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_0
    const-string v5, "GYRO_SENSOR_SELFTEST"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 121
    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_c

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Noise Bias : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_X:Landroid/widget/TextView;

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    aget-object v6, v0, v10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    aget-object v6, v0, v11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Noise Power(RMS) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v13

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x6

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_X:Landroid/widget/TextView;

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Y:Landroid/widget/TextView;

    aget-object v6, v0, v13

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Z:Landroid/widget/TextView;

    const/4 v6, 0x6

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HW Self Test(%) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x7

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x8

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x9

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_X:Landroid/widget/TextView;

    const/4 v6, 0x7

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_Y:Landroid/widget/TextView;

    const/16 v6, 0x8

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_HW_Self_Z:Landroid/widget/TextView;

    const/16 v6, 0x9

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "update"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ReturnValue : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v5, 0x0

    aget-object v5, v0, v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 142
    const/4 v2, 0x0

    .line 145
    :cond_2
    const-string v5, "GYROSCOPE_SELFTEST_MIN"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Min:I

    .line 146
    const-string v5, "GYROSCOPE_SELFTEST_MAX"

    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Max:I

    .line 148
    if-ne v2, v9, :cond_3

    aget-object v5, v0, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_3

    aget-object v5, v0, v10

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_3

    aget-object v5, v0, v11

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Max:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_3

    aget-object v5, v0, v9

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    aget-object v5, v0, v10

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    aget-object v5, v0, v11

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mGyroSpec_Min:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-gez v5, :cond_4

    .line 152
    :cond_3
    const/4 v2, 0x0

    .line 155
    :cond_4
    if-ne v2, v9, :cond_5

    aget-object v5, v0, v12

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, 0x40a00000    # 5.0f

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_5

    aget-object v5, v0, v13

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, 0x40a00000    # 5.0f

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_5

    const/4 v5, 0x6

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, 0x40a00000    # 5.0f

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_5

    aget-object v5, v0, v12

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, -0x3f600000    # -5.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    aget-object v5, v0, v13

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, -0x3f600000    # -5.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_5

    const/4 v5, 0x6

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    const/high16 v6, -0x3f600000    # -5.0f

    cmpl-float v5, v5, v6

    if-gez v5, :cond_6

    .line 159
    :cond_5
    const/4 v2, 0x0

    .line 162
    :cond_6
    const/16 v3, 0x32

    .line 163
    .local v3, "specHwSelfTest":I
    if-ne v2, v9, :cond_8

    int-to-float v5, v3

    const/4 v6, 0x7

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-lez v5, :cond_7

    int-to-float v5, v3

    const/16 v6, 0x8

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-lez v5, :cond_7

    int-to-float v5, v3

    const/16 v6, 0x9

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gtz v5, :cond_8

    .line 167
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "spec out"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HW Self Test : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x7

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x8

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x9

    aget-object v8, v0, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v2, 0x0

    .line 172
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_a

    const-string v5, "PASS"

    :goto_1
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTextResult:Landroid/widget/TextView;

    if-eqz v2, :cond_b

    const v5, -0xffff01

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    .end local v3    # "specHwSelfTest":I
    :goto_3
    return-void

    .line 116
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Temperature:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 172
    .restart local v3    # "specHwSelfTest":I
    :cond_a
    const-string v5, "FAIL"

    goto :goto_1

    .line 173
    :cond_b
    const/high16 v5, -0x10000

    goto :goto_2

    .line 175
    .end local v3    # "specHwSelfTest":I
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_X:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Y:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Bias_Z:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_X:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Y:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mText_Noise_Power_Z:Landroid/widget/TextView;

    const-string v6, "NONE"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTextResult:Landroid/widget/TextView;

    const-string v6, "FAIL"

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mTextResult:Landroid/widget/TextView;

    const/high16 v6, -0x10000

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f030038

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->setContentView(I)V

    .line 42
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mFeature:Ljava/lang/String;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->init()V

    .line 45
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOff()V

    .line 61
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 48
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 49
    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mSenserID:[I

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mSenserID:[I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeInvensense;->WHAT_UPDATE:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 56
    return-void
.end method
