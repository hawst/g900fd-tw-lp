.class Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;
.super Ljava/util/TimerTask;
.source "ProximityOffsetTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field private data:Ljava/lang/String;

.field private mIsRunningTask:Z

.field private mValue:I

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V
    .locals 1

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 220
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mIsRunningTask:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$1;

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;-><init>(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->resume()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->pause()V

    return-void
.end method

.method private pause()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mIsRunningTask:Z

    .line 285
    return-void
.end method

.method private readToProximitySensor()V
    .locals 8

    .prologue
    .line 250
    const/4 v1, 0x0

    .line 253
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    const-string v4, "sys/class/sensors/proximity_sensor/state"

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 255
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->data:Ljava/lang/String;

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->data:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->data:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mValue:I

    .line 261
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1200(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToProximitySensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ADC: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 265
    if-eqz v2, :cond_3

    .line 267
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v1, v2

    .line 273
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    return-void

    .line 259
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    const/4 v3, 0x0

    :try_start_3
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mValue:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 262
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 263
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v0, "e":Ljava/io/IOException;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1300(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToProximitySensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ProximitySensor IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 265
    if-eqz v1, :cond_0

    .line 267
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 268
    :catch_1
    move-exception v0

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToProximitySensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 268
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v0

    .line 269
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToProximitySensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 270
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 265
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v1, :cond_2

    .line 267
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 270
    :cond_2
    :goto_4
    throw v3

    .line 268
    :catch_3
    move-exception v0

    .line 269
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1400(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "readToProximitySensor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 265
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 262
    :catch_4
    move-exception v0

    goto/16 :goto_2

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mIsRunningTask:Z

    .line 281
    return-void
.end method


# virtual methods
.method public getADC()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mValue:I

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 223
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$600(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSensorChanged"

    const-string v2, "SensorChanged!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$700(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Working"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$700(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$800(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->startVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$900(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V

    .line 239
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$700(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$700(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$800(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->stopVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1000(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)V

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->readToProximitySensor()V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest$handlersensor;->this$0:Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;->access$1100(Lcom/sec/android/app/hwmoduletest/ProximityOffsetTest;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 247
    :cond_0
    return-void
.end method
