.class Lcom/sec/android/app/hwmoduletest/HistoryItemCD;
.super Ljava/lang/Object;
.source "GestureTestCountDeltaDisplay.java"


# instance fields
.field private RawDataAValue:Ljava/lang/String;

.field private RawDataBValue:Ljava/lang/String;

.field private RawDataCValue:Ljava/lang/String;

.field private RawDataDValue:Ljava/lang/String;

.field private countvalue:Ljava/lang/String;


# direct methods
.method public constructor <init>(IFFFF)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "RawDataA"    # F
    .param p3, "RawDataB"    # F
    .param p4, "RawDataC"    # F
    .param p5, "RawDataD"    # F

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->countvalue:Ljava/lang/String;

    .line 172
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataAValue:Ljava/lang/String;

    .line 173
    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataBValue:Ljava/lang/String;

    .line 174
    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataCValue:Ljava/lang/String;

    .line 175
    invoke-static {p5}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataDValue:Ljava/lang/String;

    .line 176
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataA()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataAValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataBValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataCValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueRawDataD()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->RawDataDValue:Ljava/lang/String;

    return-object v0
.end method
