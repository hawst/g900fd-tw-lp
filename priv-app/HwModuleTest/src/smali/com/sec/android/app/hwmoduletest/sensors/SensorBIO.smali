.class public Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;
.super Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;
.source "SensorBIO.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final DEBUG:Z

.field private isBIOMGROn:Z

.field private mBIOSensor:Landroid/hardware/Sensor;

.field private mBuffer_SensorValue_BIO:[F

.field private mContext:Landroid/content/Context;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorValues:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorBase;-><init>()V

    .line 17
    const-string v0, "SensorBIO"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->CLASS_NAME:Ljava/lang/String;

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->DEBUG:Z

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->isBIOMGROn:Z

    .line 24
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorValues:[Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    .line 34
    const-string v0, "SensorBIO"

    const-string v1, "SensorBIO"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method private dataCheck([F)Ljava/lang/String;
    .locals 4
    .param p1, "data"    # [F

    .prologue
    .line 135
    const-string v1, ""

    .line 137
    .local v1, "result":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 138
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 141
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v0    # "i":I
    :cond_1
    const-string v1, "Data : null"

    .line 149
    :cond_2
    return-object v1
.end method


# virtual methods
.method public SensorOff()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    const-string v0, "SensorBIO"

    const-string v1, "sensorOff"

    const-string v2, "Sensor Off"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 85
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    .line 86
    const-string v0, "SensorBIO"

    const-string v1, "mTimer canceled"

    const-string v2, "..."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 93
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->isBIOMGROn:Z

    .line 94
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    .line 95
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    .line 96
    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    .line 97
    return-void
.end method

.method public SensorOn(Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;[II)V
    .locals 7
    .param p1, "mSensorListener"    # Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;
    .param p2, "sensorID"    # [I
    .param p3, "interval"    # I

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mContext:Landroid/content/Context;

    const-string v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_2

    .line 43
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p2

    if-ge v6, v0, :cond_3

    .line 44
    aget v0, p2, v6

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x10019

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 48
    const-string v0, "SensorBIO"

    const-string v1, "SensorOn"

    const-string v2, "register-BIOSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 51
    :cond_1
    const-string v0, "SensorBIO"

    const-string v1, "SensorOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregistered: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p2, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 55
    .end local v6    # "i":I
    :cond_2
    const-string v0, "SensorBIO"

    const-string v1, "SensorOn"

    const-string v2, "SensorManager null !!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_3
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO$1;-><init>(Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;)V

    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 64
    return-void
.end method

.method public isSensorOn(I)Z
    .locals 2
    .param p1, "sensorID"    # I

    .prologue
    const/4 v0, 0x0

    .line 111
    const/16 v1, 0xc

    if-ne p1, v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBIOSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 99
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 102
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 108
    :goto_0
    return-void

    .line 104
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->isBIOMGROn:Z

    .line 105
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    goto :goto_0

    .line 102
    :pswitch_data_0
    .packed-switch 0x10019
        :pswitch_0
    .end packed-switch
.end method

.method public returnHRM()[F
    .locals 3

    .prologue
    .line 124
    const-string v0, "SensorBIO"

    const-string v1, "returnBIO"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->dataCheck([F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    return-object v0
.end method

.method public returnSensorValues()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->isBIOMGROn:Z

    if-ne v0, v3, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mBuffer_SensorValue_BIO:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v5, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 75
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v1, ""

    invoke-interface {v0, v4, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    .line 76
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/sensors/SensorBIO;->mSensorListener:Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;

    const-string v1, "null"

    invoke-interface {v0, v5, v1}, Lcom/sec/android/app/hwmoduletest/sensors/SensorListener;->onSensorValueReceived(ILjava/lang/String;)V

    goto :goto_0
.end method
