.class Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;
.super Ljava/lang/Object;
.source "GyroscopeDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryItem"
.end annotation


# instance fields
.field private countvalue:Ljava/lang/String;

.field private xvalue:Ljava/lang/String;

.field private yvalue:Ljava/lang/String;

.field private zvalue:Ljava/lang/String;


# direct methods
.method public constructor <init>(IFFF)V
    .locals 1
    .param p1, "count"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    .line 183
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->xvalue:Ljava/lang/String;

    .line 184
    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->yvalue:Ljava/lang/String;

    .line 185
    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->zvalue:Ljava/lang/String;

    .line 186
    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->countvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueX()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->xvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->yvalue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueZ()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->zvalue:Ljava/lang/String;

    return-object v0
.end method
