.class Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;
.super Ljava/lang/Object;
.source "GestureTestMode1Display.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 9
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    .line 127
    iget-object v5, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-int v1, v5

    .line 128
    .local v1, "scale":I
    iget-object v5, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-int v0, v5

    .line 129
    .local v0, "angle":I
    iget-object v5, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x3

    aget v4, v5, v6

    .line 130
    .local v4, "zmean":F
    iget-object v5, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x4

    aget v2, v5, v6

    .line 131
    .local v2, "valid_cnt":F
    iget-object v5, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    const/4 v6, 0x5

    aget v3, v5, v6

    .line 133
    .local v3, "zdelta":F
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)I

    .line 134
    const-string v5, "GestureTestMode1Display"

    const-string v6, "listenner: getSensorData()"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ", zmean = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mValueList:Ljava/util/List;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)Ljava/util/List;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/hwmoduletest/HistoryItem1;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mCount:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)I

    move-result v7

    invoke-direct {v6, v7, v4}, Lcom/sec/android/app/hwmoduletest/HistoryItem1;-><init>(IF)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display$1;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor1;->notifyDataSetChanged()V

    .line 138
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mLoggingOn:Z
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$300()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 139
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Display;->access$400()Lcom/sec/android/app/hwmoduletest/LogSensorData;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->writeSensorData(F)V

    .line 140
    const-string v5, "GestureTestMode1Display"

    const-string v6, "onGetSensorHubData"

    const-string v7, "write to log file "

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    return-void
.end method
