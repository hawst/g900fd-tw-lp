.class Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;
.super Landroid/os/Handler;
.source "LightSensorReadTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 74
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 76
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLuxText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$100(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    # invokes: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getLuxString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->access$000(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsReadFromManager:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$200(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mAdcText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$400(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    # invokes: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getAdcString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->access$300(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    # invokes: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getLevelString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->access$500(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$600(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLevelColor:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$700(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mIsRGBSensor:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$800(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "LIGHT_SENSOR_NAME"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CM36686"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mRgbwText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1000(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    # invokes: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getAlsWString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->access$900(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 90
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mRgbwText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->access$1000(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest;->mLightSensorThread:Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;

    # invokes: Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->getRgbwString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;->access$1100(Lcom/sec/android/app/hwmoduletest/LightSensorReadTest$LightSensorThread;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
