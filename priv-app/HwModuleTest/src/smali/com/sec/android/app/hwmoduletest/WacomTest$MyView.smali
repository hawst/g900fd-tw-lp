.class public Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;
.super Landroid/view/View;
.source "WacomTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/WacomTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mNonClickPaint:Landroid/graphics/Paint;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/WacomTest;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 206
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    .line 207
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 189
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    .line 190
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    .line 191
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    .line 192
    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    .line 208
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->setKeepScreenOn(Z)V

    .line 209
    invoke-virtual {p1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    .line 210
    invoke-virtual {p1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    .line 211
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 213
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 215
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 216
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->setPaint()V

    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->initRect()V

    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    .line 220
    return-void
.end method

.method private drawByEvent(Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 268
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 270
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    .line 273
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    .line 274
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 275
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 279
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 280
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 281
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    .line 282
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    .line 283
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    .line 284
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    .line 285
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 286
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawLine(FFFF)V

    .line 280
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 289
    :cond_1
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    .line 290
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    .line 291
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    .line 292
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    .line 293
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 294
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawLine(FFFF)V

    .line 295
    iput-boolean v6, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 301
    .end local v1    # "i":I
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 302
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    .line 303
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    .line 304
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    .line 305
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    .line 307
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 308
    iget v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawPoint(FF)V

    .line 311
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->isTouchDown:Z

    goto/16 :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 320
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 322
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 323
    float-to-int v6, p1

    .line 324
    float-to-int v8, p3

    .line 330
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 331
    float-to-int v7, p2

    .line 332
    float-to-int v9, p4

    .line 338
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 339
    return-void

    .line 326
    :cond_0
    float-to-int v6, p3

    .line 327
    float-to-int v8, p1

    goto :goto_0

    .line 334
    :cond_1
    float-to-int v7, p4

    .line 335
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 343
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 344
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 381
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 382
    .local v9, "col_width":F
    div-float v0, p1, v9

    float-to-int v10, v0

    .line 383
    .local v10, "countX":I
    div-float v0, p2, v8

    float-to-int v11, v0

    .line 384
    .local v11, "countY":I
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 385
    .local v6, "ColX":F
    int-to-float v0, v11

    mul-float v7, v8, v0

    .line 388
    .local v7, "ColY":F
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v11, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v10, v0, :cond_2

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$700(Lcom/sec/android/app/hwmoduletest/WacomTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "You are out of bounds!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_1
    :goto_0
    return-void

    .line 393
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$500(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-nez v0, :cond_3

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$500(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    const/4 v1, 0x1

    aput-boolean v1, v0, v10

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$500(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$800(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v0

    aget-object v0, v0, v11

    aget-boolean v0, v0, v10

    if-eqz v0, :cond_5

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 404
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 408
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/WacomTest;->isPass()Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$900(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$1000(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v0

    if-nez v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # operator++ for: Lcom/sec/android/app/hwmoduletest/WacomTest;->passFlag:I
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$1008(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->remoteCall:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$1100(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->setResult(I)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$1200(Lcom/sec/android/app/hwmoduletest/WacomTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "drawRect"

    const-string v2, "Wacom Test: Pass"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->finish()V

    goto/16 :goto_0

    .line 400
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private initRect()V
    .locals 19

    .prologue
    .line 347
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v2

    int-to-float v2, v2

    div-float v15, v1, v2

    .line 348
    .local v15, "col_height":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v2

    int-to-float v2, v2

    div-float v16, v1, v2

    .line 349
    .local v16, "col_width":F
    const/4 v13, 0x0

    .line 350
    .local v13, "ColX":I
    const/4 v14, 0x0

    .line 351
    .local v14, "ColY":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 352
    .local v6, "mRectPaint":Landroid/graphics/Paint;
    const/high16 v1, -0x1000000

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 354
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    move/from16 v0, v17

    if-ge v0, v1, :cond_1

    .line 355
    move/from16 v0, v17

    int-to-float v1, v0

    mul-float/2addr v1, v15

    float-to-int v14, v1

    .line 357
    const/16 v18, 0x0

    .local v18, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    move/from16 v0, v18

    if-ge v0, v1, :cond_0

    .line 358
    move/from16 v0, v18

    int-to-float v1, v0

    mul-float v1, v1, v16

    float-to-int v13, v1

    .line 359
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenWidth:I

    int-to-float v4, v4

    int-to-float v5, v14

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 360
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v13

    int-to-float v3, v14

    int-to-float v4, v13

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mScreenHeight:I

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 361
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->draw:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$500(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v1

    aget-object v1, v1, v17

    const/4 v2, 0x0

    aput-boolean v2, v1, v18

    .line 362
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->click:[[Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$600(Lcom/sec/android/app/hwmoduletest/WacomTest;)[[Z

    move-result-object v1

    aget-object v1, v1, v17

    const/4 v2, 0x0

    aput-boolean v2, v1, v18

    .line 357
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 354
    :cond_0
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 366
    .end local v18    # "j":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v8, v16, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 369
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v9, v15, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/high16 v1, 0x3f800000    # 1.0f

    add-float v8, v16, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 374
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v8, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    add-float v9, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->WIDTH_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$400(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float v1, v1, v16

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v10, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->HEIGHT_BASIS:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$300(Lcom/sec/android/app/hwmoduletest/WacomTest;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v15

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v11, v1, v2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 377
    return-void
.end method

.method private setPaint()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    .line 223
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 234
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 237
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mNonClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 240
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 243
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 249
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v1, 0x2000000

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/WacomTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/WacomTest;->mIsWacom:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/WacomTest;->access$200(Lcom/sec/android/app/hwmoduletest/WacomTest;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 256
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawByEvent(Landroid/view/MotionEvent;)V

    .line 264
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 259
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 260
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/WacomTest$MyView;->drawByEvent(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method
