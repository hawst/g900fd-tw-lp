.class public Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;
.super Landroid/app/ListActivity;
.source "BarcodeEmulTest.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;
    }
.end annotation


# instance fields
.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;",
            ">;"
        }
    .end annotation
.end field

.field private mBarcodeEmulFWText:Landroid/widget/TextView;

.field private mBarcodeEmulTitleText:Landroid/widget/TextView;

.field private mListView:Landroid/widget/ListView;

.field private tasks:[Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 22
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "UPC-A"

    const-string v4, "123456789012"

    const-string v5, "FF AC DB 21 5C 9D 42 AE DB 8B 1A 64 9A FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "EAN-13"

    const-string v4, "1234567890128"

    const-string v5, "FF AD 90 B1 4E F5 BA AD C5 8D 32 4D BA FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "EAN-8"

    const-string v4, "12345670"

    const-string v5, "FF EB 36 C8 57 2A C5 7B B1 AB FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "Code 39"

    const-string v4, "Code-39"

    const-string v5, "FF F7 44 51 17 51 45 D5 1D 11 47 57 51 11 1D 54 74 57 44 5F FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "Code 128"

    const-string v4, "Code-128"

    const-string v5, "FF CB 7B B9 70 AF 65 37 B2 36 32 63 45 98 D9 38 A7 FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "Interleaved 2 of 5"

    const-string v4, "12345678"

    const-string v5, "FF F5 2D 4C 96 B2 CD 5A 99 2F FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "Codabar"

    const-string v4, "A1234567890A"

    const-string v5, "FF E9 B5 4D 5A 4D 54 B4 AD 6A 5A 56 54 B5 56 53 6F FF"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    const-string v3, "GS1 Databar"

    const-string v4, "(01)00614141999996"

    const-string v5, "DE BB C8 07 A6 7A 11 BD DA 1F 8C F6 E4 80 A3 81 EB"

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->tasks:[Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    .line 70
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v2, 0x7f030007

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->setContentView(I)V

    .line 45
    const v2, 0x102000a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mListView:Landroid/widget/ListView;

    .line 46
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mListView:Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x7f03004b

    const v5, 0x7f0b0153

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->tasks:[Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest$BeamTask;

    invoke-direct {v3, p0, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 47
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 49
    const v2, 0x7f0b002a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mBarcodeEmulTitleText:Landroid/widget/TextView;

    .line 50
    const v2, 0x7f0b002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mBarcodeEmulFWText:Landroid/widget/TextView;

    .line 53
    :try_start_0
    const-string v2, "BARCODE_EMUL_FIRMWARE_VERSION"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "BarcodeFW":Ljava/lang/String;
    const-string v2, "BarcodeEmulTest"

    const-string v3, "onCreate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Barcode Emul FW is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mBarcodeEmulTitleText:Landroid/widget/TextView;

    const-string v3, "Barcode Emulator"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->mBarcodeEmulFWText:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FW Version : 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    .end local v0    # "BarcodeFW":Ljava/lang/String;
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v1

    .line 58
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "BarcodeEmulTest"

    const-string v3, "onCreate"

    const-string v4, "Barcode Emul FW read is fail!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/hwmoduletest/BeamActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "position-task"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/BarcodeEmulTest;->startActivity(Landroid/content/Intent;)V

    .line 68
    return-void
.end method
