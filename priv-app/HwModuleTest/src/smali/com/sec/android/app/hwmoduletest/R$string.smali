.class public final Lcom/sec/android/app/hwmoduletest/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ACCELEROMETER_Sensor:I = 0x7f08011f

.field public static final AP:I = 0x7f0800fe

.field public static final Auto:I = 0x7f080141

.field public static final BIST:I = 0x7f080071

.field public static final BUILDTIME:I = 0x7f080106

.field public static final Bin_Firm_Version:I = 0x7f0801fe

.field public static final Button01:I = 0x7f08018e

.field public static final Button02:I = 0x7f08018f

.field public static final Button03:I = 0x7f080190

.field public static final Button04:I = 0x7f080191

.field public static final Button05:I = 0x7f080192

.field public static final CALIB:I = 0x7f08006c

.field public static final CHANGELIST:I = 0x7f080107

.field public static final CP:I = 0x7f0800ff

.field public static final CSC:I = 0x7f080100

.field public static final Cabc_Test:I = 0x7f080142

.field public static final Count:I = 0x7f08018a

.field public static final ESC:I = 0x7f080199

.field public static final EXIT:I = 0x7f08014c

.field public static final Error1:I = 0x7f0800db

.field public static final Error10:I = 0x7f0800df

.field public static final Error13:I = 0x7f0800e0

.field public static final Error17:I = 0x7f0800ed

.field public static final Error2:I = 0x7f0800dc

.field public static final Error3:I = 0x7f0800dd

.field public static final Error7:I = 0x7f0800de

.field public static final Etc:I = 0x7f0800fa

.field public static final FAIL:I = 0x7f08007b

.field public static final FIFO_Test:I = 0x7f080002

.field public static final FirmwareCheck:I = 0x7f0800d1

.field public static final GYROSCOPE_Sensor:I = 0x7f08005f

.field public static final Gesture_Countdelta:I = 0x7f08024b

.field public static final Gesture_Countdelta_item_ChA:I = 0x7f08024c

.field public static final Gesture_Countdelta_item_ChB:I = 0x7f08024d

.field public static final Gesture_Countdelta_item_ChC:I = 0x7f08024e

.field public static final Gesture_Countdelta_item_ChD:I = 0x7f08024f

.field public static final Gesture_Crosstalk:I = 0x7f08024a

.field public static final Gesture_Data:I = 0x7f080248

.field public static final Gesture_Sensor:I = 0x7f080184

.field public static final Gesture_Sensor_Start:I = 0x7f080187

.field public static final Gesture_Zmean:I = 0x7f080249

.field public static final Gesture_count_direction:I = 0x7f080186

.field public static final Gesture_count_title:I = 0x7f080185

.field public static final Gesture_test:I = 0x7f080247

.field public static final HW:I = 0x7f080101

.field public static final HWREV:I = 0x7f080104

.field public static final Headset_on:I = 0x7f080149

.field public static final INT_Check:I = 0x7f0801fb

.field public static final Initialized:I = 0x7f080060

.field public static final LOTID:I = 0x7f080188

.field public static final MAX:I = 0x7f0800d0

.field public static final MCU:I = 0x7f0801ff

.field public static final MIN:I = 0x7f0800cf

.field public static final Mcu_Firm_Version:I = 0x7f0801fd

.field public static final Mode1:I = 0x7f08016c

.field public static final Mode2:I = 0x7f08016d

.field public static final Mode_check:I = 0x7f08016f

.field public static final Music_volume:I = 0x7f08025c

.field public static final OFF:I = 0x7f08014b

.field public static final OK:I = 0x7f0800ee

.field public static final PASS:I = 0x7f08007a

.field public static final PROXIMITY_Sensor:I = 0x7f08002a

.field public static final Panel_Firm_Version:I = 0x7f0800d3

.field public static final Phone_Firm_Version:I = 0x7f0800d2

.field public static final QuickStart:I = 0x7f08011e

.field public static final RATE:I = 0x7f080072

.field public static final RESULT:I = 0x7f080073

.field public static final RETRY:I = 0x7f08007c

.field public static final RFCAL:I = 0x7f080103

.field public static final Receiver_Headset_on:I = 0x7f08014a

.field public static final Receiver_on:I = 0x7f080148

.field public static final Recording_mainmic_rcv:I = 0x7f080257

.field public static final Recording_mainmic_spk:I = 0x7f080256

.field public static final Recording_off:I = 0x7f08025a

.field public static final Recording_submic_rcv:I = 0x7f080259

.field public static final Recording_submic_spk:I = 0x7f080258

.field public static final Reference_Test_Fail:I = 0x7f0800ce

.field public static final Reset:I = 0x7f08016e

.field public static final SensorHubFirmwareCheck:I = 0x7f0801fc

.field public static final SensorHub_Test:I = 0x7f0801fa

.field public static final Speaker_on:I = 0x7f080147

.field public static final State:I = 0x7f080189

.field public static final THDmode:I = 0x7f08016b

.field public static final TSP:I = 0x7f0800d4

.field public static final Temperature:I = 0x7f080061

.field public static final VALUE:I = 0x7f080070

.field public static final Vibration_Test:I = 0x7f08013e

.field public static final Voice_call_volume:I = 0x7f08025b

.field public static final X:I = 0x7f080077

.field public static final XYZ:I = 0x7f080065

.field public static final XYZ_DIFF:I = 0x7f080067

.field public static final XYZ_PRIME:I = 0x7f080066

.field public static final XYZ_ZERO_RATE:I = 0x7f08006b

.field public static final X_PRIME:I = 0x7f080062

.field public static final Y:I = 0x7f080078

.field public static final Y_PRIME:I = 0x7f080063

.field public static final Z:I = 0x7f080079

.field public static final Z_PRIME:I = 0x7f080064

.field public static final acc_graph_button:I = 0x7f08001c

.field public static final acc_sensor_calibration:I = 0x7f080120

.field public static final acc_sensor_calibration_data_erase:I = 0x7f080121

.field public static final acc_sensor_test_NTT:I = 0x7f0801de

.field public static final acc_sensor_test_apollo:I = 0x7f0801e9

.field public static final acc_sensor_test_aries:I = 0x7f0801d1

.field public static final acc_value:I = 0x7f080216

.field public static final acce_image_test_button:I = 0x7f08001b

.field public static final acce_sensor:I = 0x7f080014

.field public static final adc_value_11:I = 0x7f080034

.field public static final advanced:I = 0x7f0800ef

.field public static final aponly_name:I = 0x7f0801a0

.field public static final app_name:I = 0x7f080000

.field public static final app_name_brightness:I = 0x7f08018b

.field public static final app_title:I = 0x7f080001

.field public static final auto_answeringmode:I = 0x7f0801a2

.field public static final autofocus_failed:I = 0x7f0801f2

.field public static final automode:I = 0x7f0801a8

.field public static final back:I = 0x7f0801b6

.field public static final band_setting:I = 0x7f0801a7

.field public static final baro_ref_altitude:I = 0x7f08001d

.field public static final baro_ref_altitude_button:I = 0x7f08001e

.field public static final baro_self_test_NA:I = 0x7f080038

.field public static final baro_self_test_button:I = 0x7f08001f

.field public static final baro_self_test_title:I = 0x7f080037

.field public static final baro_sensor:I = 0x7f080017

.field public static final baro_waterproof_test_button:I = 0x7f080020

.field public static final barometer:I = 0x7f080284

.field public static final barometer_waterproof_title:I = 0x7f080283

.field public static final basic:I = 0x7f0800f0

.field public static final bat_back:I = 0x7f080118

.field public static final bat_get:I = 0x7f080117

.field public static final bat_level:I = 0x7f08011b

.field public static final bat_type:I = 0x7f08011c

.field public static final beamComplete:I = 0x7f080012

.field public static final beamError:I = 0x7f080013

.field public static final beaming:I = 0x7f080011

.field public static final bluetooth_test_NTT:I = 0x7f0801db

.field public static final bluetooth_test_apollo:I = 0x7f0801e6

.field public static final bluetooth_test_aries:I = 0x7f0801ce

.field public static final body_temp:I = 0x7f0800ab

.field public static final brightness:I = 0x7f08018c

.field public static final bt_turn_off:I = 0x7f0801f9

.field public static final bt_turn_on:I = 0x7f0801f8

.field public static final btn_start:I = 0x7f08000f

.field public static final btn_stop:I = 0x7f080010

.field public static final build_info_name:I = 0x7f080108

.field public static final cabc_off:I = 0x7f080144

.field public static final cabc_on:I = 0x7f080143

.field public static final cal_start:I = 0x7f08020b

.field public static final cal_stop:I = 0x7f08020c

.field public static final camera_fail:I = 0x7f0801f5

.field public static final camera_test_NTT:I = 0x7f0801da

.field public static final camera_test_apollo:I = 0x7f0801e5

.field public static final camera_test_aries:I = 0x7f0801cc

.field public static final cancel_button:I = 0x7f080007

.field public static final channel_up_down:I = 0x7f0800bf

.field public static final clear_history:I = 0x7f0800f9

.field public static final comp_value:I = 0x7f080218

.field public static final copy:I = 0x7f0800f2

.field public static final copy_all:I = 0x7f0800f1

.field public static final copy_to_sdcard:I = 0x7f080179

.field public static final cp_acce_sensor:I = 0x7f080015

.field public static final cp_ril:I = 0x7f08017e

.field public static final cpap_name:I = 0x7f0801a1

.field public static final cpchange:I = 0x7f0801a6

.field public static final cponly_name:I = 0x7f08019f

.field public static final csfbmode:I = 0x7f0801aa

.field public static final cut:I = 0x7f0800f4

.field public static final cut_all:I = 0x7f0800f3

.field public static final dataline_fail:I = 0x7f0801f3

.field public static final date:I = 0x7f0800ec

.field public static final dbg_state_enable:I = 0x7f080177

.field public static final default_string:I = 0x7f080005

.field public static final delete_dump:I = 0x7f080171

.field public static final dimming_test_NTT:I = 0x7f0801d9

.field public static final dimming_test_apollo:I = 0x7f0801e4

.field public static final dimming_test_aries:I = 0x7f0801cb

.field public static final disable_name:I = 0x7f08019d

.field public static final dm_modem_adb_mode:I = 0x7f0801b3

.field public static final dm_name:I = 0x7f080197

.field public static final down:I = 0x7f0800c1

.field public static final dualmode:I = 0x7f0801a3

.field public static final dualmodeoff:I = 0x7f0801a4

.field public static final dualmodeon:I = 0x7f0801a5

.field public static final ecg_graph_button:I = 0x7f080268

.field public static final ecg_test_button:I = 0x7f080267

.field public static final ecg_title:I = 0x7f080265

.field public static final ecg_version:I = 0x7f080266

.field public static final enable_name:I = 0x7f08019c

.field public static final error:I = 0x7f0800da

.field public static final errorparen:I = 0x7f0800fb

.field public static final exit:I = 0x7f08017c

.field public static final factory_format:I = 0x7f0801bb

.field public static final fail:I = 0x7f080004

.field public static final failhist_get:I = 0x7f080126

.field public static final fcc_id:I = 0x7f0800e7

.field public static final file_open:I = 0x7f0801b9

.field public static final find_text:I = 0x7f0801b8

.field public static final fingerprint_method3:I = 0x7f080260

.field public static final fingerprint_method4:I = 0x7f080261

.field public static final fingerprint_mrmtest:I = 0x7f08025f

.field public static final fingerprint_normalscan:I = 0x7f08025e

.field public static final fingerprint_sensorinfo:I = 0x7f080262

.field public static final fingerprint_snr:I = 0x7f080263

.field public static final fingerprint_test:I = 0x7f08025d

.field public static final finish_button:I = 0x7f08028b

.field public static final firmware_str:I = 0x7f080155

.field public static final format_caution:I = 0x7f0801bd

.field public static final format_caution2:I = 0x7f0801be

.field public static final format_info:I = 0x7f0801bc

.field public static final frequency_100:I = 0x7f0800ca

.field public static final frequency_200:I = 0x7f0800cb

.field public static final frequency_300:I = 0x7f0800cc

.field public static final frequency_500:I = 0x7f0800cd

.field public static final frequency_test:I = 0x7f0800c9

.field public static final front_dataline_fail:I = 0x7f0801f4

.field public static final fuel_gauge_current:I = 0x7f0801c3

.field public static final fuel_gauge_new1:I = 0x7f0801c5

.field public static final fuel_gauge_new2:I = 0x7f0801c6

.field public static final fuel_gauge_read:I = 0x7f0801c4

.field public static final fuel_gauge_title:I = 0x7f0801c2

.field public static final fuel_gauge_write:I = 0x7f0801c7

.field public static final function_name:I = 0x7f08019b

.field public static final gesture_crosstalk_button:I = 0x7f080201

.field public static final gesture_direction_button:I = 0x7f080202

.field public static final gesture_peak2peak:I = 0x7f080246

.field public static final gesture_sensor:I = 0x7f080200

.field public static final gesture_test_mode1:I = 0x7f08023e

.field public static final gesture_test_mode1_Angle:I = 0x7f080245

.field public static final gesture_test_mode1_Valid_cnt:I = 0x7f080244

.field public static final gesture_test_mode1_Zdelta:I = 0x7f080242

.field public static final gesture_test_mode1_Zmean:I = 0x7f080243

.field public static final gesture_test_mode1_item_count:I = 0x7f080240

.field public static final gesture_test_mode2:I = 0x7f08023f

.field public static final gesture_test_mode2_item_count:I = 0x7f080241

.field public static final glucose_check_button:I = 0x7f0800b4

.field public static final glucose_check_checking:I = 0x7f0800b8

.field public static final glucose_check_detect:I = 0x7f0800b7

.field public static final glucose_check_start:I = 0x7f0800b5

.field public static final glucose_check_stop:I = 0x7f0800b6

.field public static final glucose_current:I = 0x7f0800ba

.field public static final glucose_self_button:I = 0x7f0800b3

.field public static final glucose_sensor:I = 0x7f0800b2

.field public static final glucose_sn:I = 0x7f0800b9

.field public static final glucose_spec:I = 0x7f0800bb

.field public static final grip1:I = 0x7f080090

.field public static final grip2:I = 0x7f080091

.field public static final grip_title:I = 0x7f080096

.field public static final gripsensor_RAW_Count_str:I = 0x7f080097

.field public static final gripsensor_SPEC_str:I = 0x7f080098

.field public static final gripsensor_cal:I = 0x7f08009c

.field public static final gripsensor_cal_erase:I = 0x7f08009d

.field public static final gripsensor_cal_skip:I = 0x7f08009e

.field public static final gripsensor_caldata_str:I = 0x7f08009b

.field public static final gripsensor_cspercent_str:I = 0x7f080099

.field public static final gripsensor_on:I = 0x7f08009f

.field public static final gripsensor_proxpercent_str:I = 0x7f08009a

.field public static final gripsensor_result:I = 0x7f0800a0

.field public static final gyro_disp_title:I = 0x7f080046

.field public static final gyro_disp_title_sub:I = 0x7f080047

.field public static final gyro_disp_try_count:I = 0x7f080048

.field public static final gyro_disp_x_value:I = 0x7f080049

.field public static final gyro_disp_y_value:I = 0x7f08004a

.field public static final gyro_disp_z_value:I = 0x7f08004b

.field public static final gyro_display:I = 0x7f08006e

.field public static final gyro_display_button:I = 0x7f080024

.field public static final gyro_graph_button:I = 0x7f080025

.field public static final gyro_main_title:I = 0x7f08005e

.field public static final gyro_self:I = 0x7f08005d

.field public static final gyro_self_test_button:I = 0x7f080022

.field public static final gyro_self_test_diff:I = 0x7f080045

.field public static final gyro_self_test_normal:I = 0x7f080042

.field public static final gyro_self_test_prime:I = 0x7f080044

.field public static final gyro_self_test_retry:I = 0x7f080043

.field public static final gyro_self_test_x:I = 0x7f08003f

.field public static final gyro_self_test_y:I = 0x7f080040

.field public static final gyro_self_test_z:I = 0x7f080041

.field public static final gyro_sensor:I = 0x7f080019

.field public static final gyroscope_bias_dps:I = 0x7f080058

.field public static final gyroscope_diff:I = 0x7f080054

.field public static final gyroscope_initialized:I = 0x7f08004d

.field public static final gyroscope_intial_bias:I = 0x7f080053

.field public static final gyroscope_noise_bias:I = 0x7f080052

.field public static final gyroscope_packet_cnt:I = 0x7f080059

.field public static final gyroscope_result:I = 0x7f08005a

.field public static final gyroscope_rms:I = 0x7f080055

.field public static final gyroscope_temperature:I = 0x7f08004e

.field public static final gyroscope_title:I = 0x7f08004c

.field public static final gyroscope_value:I = 0x7f08006d

.field public static final gyroscope_x:I = 0x7f08004f

.field public static final gyroscope_y:I = 0x7f080050

.field public static final gyroscope_z:I = 0x7f080051

.field public static final hallic_1st:I = 0x7f080254

.field public static final hallic_2nd:I = 0x7f080255

.field public static final hallic_folder_close:I = 0x7f080251

.field public static final hallic_folder_open:I = 0x7f080250

.field public static final hallic_test_text:I = 0x7f080252

.field public static final hello:I = 0x7f0800e1

.field public static final hrm_btn_exit:I = 0x7f080282

.field public static final hrm_btn_press:I = 0x7f080273

.field public static final hrm_btn_start:I = 0x7f080281

.field public static final hrm_common_ir:I = 0x7f080276

.field public static final hrm_common_item:I = 0x7f080275

.field public static final hrm_common_red:I = 0x7f080277

.field public static final hrm_common_result:I = 0x7f080278

.field public static final hrm_common_title:I = 0x7f080274

.field public static final hrm_eoltest_button:I = 0x7f08026e

.field public static final hrm_eoltest_title:I = 0x7f08026f

.field public static final hrm_frequency_check:I = 0x7f08027d

.field public static final hrm_frequency_dc_level:I = 0x7f08027f

.field public static final hrm_frequency_noise:I = 0x7f080280

.field public static final hrm_frequency_sample_no:I = 0x7f08027e

.field public static final hrm_hr_range:I = 0x7f080272

.field public static final hrm_idac_range:I = 0x7f080270

.field public static final hrm_led_current:I = 0x7f080271

.field public static final hrm_peak_to_peak_check_title:I = 0x7f080279

.field public static final hrm_peak_to_peak_dc_level:I = 0x7f08027b

.field public static final hrm_peak_to_peak_peak:I = 0x7f08027a

.field public static final hrm_peak_to_peak_ratio:I = 0x7f08027c

.field public static final hrm_sensor:I = 0x7f08026b

.field public static final hrm_start_button:I = 0x7f08026c

.field public static final hrmtest_title:I = 0x7f08026d

.field public static final hw_self:I = 0x7f080057

.field public static final hw_self_dps:I = 0x7f080056

.field public static final ic_firmware_version:I = 0x7f080152

.field public static final imei:I = 0x7f0800fc

.field public static final ired_off:I = 0x7f0800c4

.field public static final ired_on:I = 0x7f0800c3

.field public static final ired_repeat:I = 0x7f0800c5

.field public static final ired_title:I = 0x7f0800bc

.field public static final ired_vendor:I = 0x7f0800bd

.field public static final irled_clr_button:I = 0x7f0800c6

.field public static final irled_repet_volume_up:I = 0x7f0800c7

.field public static final irthermometer_ambi_temp:I = 0x7f0800aa

.field public static final irthermometer_celius:I = 0x7f0800a9

.field public static final irthermometer_ctemp:I = 0x7f0800a8

.field public static final irthermometer_display_button:I = 0x7f0800a3

.field public static final irthermometer_display_title:I = 0x7f0800a5

.field public static final irthermometer_off:I = 0x7f0800a7

.field public static final irthermometer_on:I = 0x7f0800a6

.field public static final irthermometer_one_test:I = 0x7f0800b1

.field public static final irthermometer_sensor:I = 0x7f0800a1

.field public static final irthermometer_test_button:I = 0x7f0800a2

.field public static final irthermometer_test_title:I = 0x7f0800a4

.field public static final kernel_log:I = 0x7f080175

.field public static final l_leak:I = 0x7f080290

.field public static final lcd_Type:I = 0x7f080122

.field public static final ligh_read_test_adc:I = 0x7f08003b

.field public static final ligh_read_test_adc_value:I = 0x7f08003e

.field public static final ligh_read_test_back_button:I = 0x7f08003d

.field public static final ligh_read_test_lux:I = 0x7f08003a

.field public static final ligh_read_test_notset:I = 0x7f08003c

.field public static final ligh_read_test_title:I = 0x7f080039

.field public static final ligh_sensor:I = 0x7f080018

.field public static final ligh_test_button:I = 0x7f080021

.field public static final light:I = 0x7f0801ed

.field public static final light_get:I = 0x7f080127

.field public static final light_sensor_test_NTT:I = 0x7f0801e0

.field public static final light_sensor_test_aries:I = 0x7f0801d3

.field public static final logging:I = 0x7f080205

.field public static final loopback_ear:I = 0x7f080182

.field public static final loopback_off:I = 0x7f080183

.field public static final loopback_receiver:I = 0x7f080180

.field public static final loopback_speaker:I = 0x7f080181

.field public static final lux_value:I = 0x7f080128

.field public static final m0_sensitivity_minmax:I = 0x7f08013d

.field public static final magn_Sensor:I = 0x7f08001a

.field public static final magn_power_noise_button:I = 0x7f080027

.field public static final magn_self_test_button:I = 0x7f080026

.field public static final magnetic_adc:I = 0x7f08008a

.field public static final magnetic_bmz:I = 0x7f08008e

.field public static final magnetic_dac:I = 0x7f080089

.field public static final magnetic_hx:I = 0x7f080083

.field public static final magnetic_hy:I = 0x7f080084

.field public static final magnetic_hz:I = 0x7f080085

.field public static final magnetic_initialized:I = 0x7f08007e

.field public static final magnetic_offset_h:I = 0x7f08008b

.field public static final magnetic_result:I = 0x7f08008c

.field public static final magnetic_status:I = 0x7f08008d

.field public static final magnetic_sx:I = 0x7f080080

.field public static final magnetic_sy:I = 0x7f080081

.field public static final magnetic_sz:I = 0x7f080082

.field public static final magnetic_temp:I = 0x7f08007f

.field public static final magnetic_title:I = 0x7f08007d

.field public static final magnetic_x:I = 0x7f080086

.field public static final magnetic_y:I = 0x7f080087

.field public static final magnetic_z:I = 0x7f080088

.field public static final main_version_name:I = 0x7f080105

.field public static final manufacturer:I = 0x7f0800e9

.field public static final mdm_silent_reset:I = 0x7f0801b4

.field public static final measured_value_thermistor_ap:I = 0x7f08022a

.field public static final measured_value_thermistor_chg:I = 0x7f08022c

.field public static final measured_value_thermistor_pam:I = 0x7f08022b

.field public static final measured_value_thermistor_sensorhub_batt1:I = 0x7f08022d

.field public static final measured_value_thermistor_sensorhub_batt2:I = 0x7f08022e

.field public static final measuring:I = 0x7f080287

.field public static final meid:I = 0x7f0800fd

.field public static final melody_test_NTT:I = 0x7f0801d6

.field public static final melody_test_apollo:I = 0x7f0801e1

.field public static final melody_test_aries:I = 0x7f0801c8

.field public static final menu_setting1:I = 0x7f080203

.field public static final menu_setting2:I = 0x7f080204

.field public static final model_name:I = 0x7f0800e6

.field public static final model_name_label:I = 0x7f0800e4

.field public static final model_name_p:I = 0x7f0800e5

.field public static final modem_log:I = 0x7f080176

.field public static final modem_to_pda:I = 0x7f080195

.field public static final motor_blank:I = 0x7f080292

.field public static final move_to_end:I = 0x7f0801b7

.field public static final mtp_adb_mode:I = 0x7f0801ac

.field public static final mtp_mode:I = 0x7f0801ab

.field public static final no_sd:I = 0x7f0801f0

.field public static final normal_receiver:I = 0x7f08014e

.field public static final not_enough_space_sd:I = 0x7f0801f1

.field public static final not_set:I = 0x7f08011d

.field public static final nv_backup:I = 0x7f08017a

.field public static final nv_delete:I = 0x7f08017b

.field public static final off:I = 0x7f080140

.field public static final offset_Result:I = 0x7f080031

.field public static final ois_gyro_self_test:I = 0x7f080023

.field public static final ok:I = 0x7f080123

.field public static final ok_button:I = 0x7f080006

.field public static final on:I = 0x7f08013f

.field public static final on_off:I = 0x7f0800c2

.field public static final one:I = 0x7f0801ec

.field public static final panel_vendor_str:I = 0x7f080154

.field public static final pass:I = 0x7f080003

.field public static final paste:I = 0x7f0800f5

.field public static final pda_to_modem:I = 0x7f080194

.field public static final pda_to_td_modem:I = 0x7f080008

.field public static final pda_to_wcdma_modem:I = 0x7f080009

.field public static final phone_firmware_version:I = 0x7f080151

.field public static final piezo_receiver:I = 0x7f08014f

.field public static final preparing_sd:I = 0x7f0801ef

.field public static final pressure:I = 0x7f08028e

.field public static final prox_offset_button:I = 0x7f08002b

.field public static final prox_sensor:I = 0x7f080016

.field public static final proximity:I = 0x7f0801ee

.field public static final proximity_ADC:I = 0x7f08002d

.field public static final proximity_Offset:I = 0x7f080032

.field public static final proximity_Reset:I = 0x7f080033

.field public static final proximity_current_High:I = 0x7f080030

.field public static final proximity_current_Low:I = 0x7f08002f

.field public static final proximity_current_Offset:I = 0x7f08002e

.field public static final proximity_sensor_test_NTT:I = 0x7f0801df

.field public static final proximity_sensor_test_apollo:I = 0x7f0801ea

.field public static final proximity_sensor_test_aries:I = 0x7f0801d2

.field public static final proximity_test:I = 0x7f080029

.field public static final proximity_threshold_btn_text:I = 0x7f08016a

.field public static final proximity_title:I = 0x7f08002c

.field public static final ptp_adb_mode:I = 0x7f0801ae

.field public static final ptp_mode:I = 0x7f0801ad

.field public static final ramdump_mode_on:I = 0x7f080178

.field public static final ramdump_mode_str:I = 0x7f0801ba

.field public static final rawTa_value:I = 0x7f0800b0

.field public static final rawTo_value:I = 0x7f0800af

.field public static final raw_value:I = 0x7f080217

.field public static final receive_test:I = 0x7f080146

.field public static final receiver_test:I = 0x7f08014d

.field public static final reference_button:I = 0x7f080285

.field public static final release:I = 0x7f08008f

.field public static final release_button:I = 0x7f080286

.field public static final res_Count:I = 0x7f08020f

.field public static final res_Cross_Angle:I = 0x7f08020e

.field public static final res_Enter_Angle:I = 0x7f080211

.field public static final res_Enter_Mag:I = 0x7f080213

.field public static final res_Exit_Angle:I = 0x7f080212

.field public static final res_Exit_Mag:I = 0x7f080214

.field public static final res_Max_Sum_NSWE:I = 0x7f080210

.field public static final res_angle:I = 0x7f080208

.field public static final res_direction:I = 0x7f080206

.field public static final res_empty:I = 0x7f08020d

.field public static final res_peak2peak:I = 0x7f080207

.field public static final res_valid_cnt:I = 0x7f080209

.field public static final res_zmax_delta:I = 0x7f08020a

.field public static final result:I = 0x7f08005c

.field public static final ril_log:I = 0x7f0801b5

.field public static final rmnet_dm_modem_mode:I = 0x7f0801b2

.field public static final rndis_adb_mode:I = 0x7f0801b0

.field public static final rndis_dm_modem_mode:I = 0x7f0801b1

.field public static final rndis_mode:I = 0x7f0801af

.field public static final rtc_back:I = 0x7f080125

.field public static final rtc_get:I = 0x7f080124

.field public static final run_dump:I = 0x7f080172

.field public static final run_dump_all:I = 0x7f080173

.field public static final s_leak:I = 0x7f080291

.field public static final savenreset:I = 0x7f0801bf

.field public static final select_all:I = 0x7f0800f6

.field public static final sensor1:I = 0x7f080094

.field public static final sensor2:I = 0x7f080095

.field public static final sensor_test:I = 0x7f08005b

.field public static final sensorhub:I = 0x7f08000a

.field public static final sensorhub_crashed_firmware_button:I = 0x7f080168

.field public static final sensorhub_firmware_button:I = 0x7f080167

.field public static final sensorhub_firmware_str_bin:I = 0x7f080166

.field public static final sensorhub_firmware_str_mcu:I = 0x7f080165

.field public static final sensorhub_test_button:I = 0x7f08000c

.field public static final sensorhub_text:I = 0x7f08000b

.field public static final seperator:I = 0x7f080174

.field public static final serialnumber:I = 0x7f0800ea

.field public static final sgltemode:I = 0x7f0801a9

.field public static final shutdown:I = 0x7f0800e2

.field public static final shutdownSummary:I = 0x7f0800e3

.field public static final sn:I = 0x7f0800eb

.field public static final speaker_test:I = 0x7f080145

.field public static final speaker_test_NTT:I = 0x7f0801d8

.field public static final speaker_test_apollo:I = 0x7f0801e3

.field public static final speaker_test_aries:I = 0x7f0801ca

.field public static final speaker_test_aries_Left:I = 0x7f0801d4

.field public static final speaker_test_aries_Right:I = 0x7f0801d5

.field public static final spen_hovertest_result_hovering:I = 0x7f080264

.field public static final start_ril:I = 0x7f08017d

.field public static final start_selecting:I = 0x7f0800f7

.field public static final stop_selecting:I = 0x7f0800f8

.field public static final str_humid:I = 0x7f08021a

.field public static final str_temp:I = 0x7f080219

.field public static final str_warning:I = 0x7f08021c

.field public static final str_warning2:I = 0x7f08021d

.field public static final str_warning_title:I = 0x7f08021b

.field public static final svc_led_blue:I = 0x7f0800d7

.field public static final svc_led_magenta:I = 0x7f0800d8

.field public static final svc_led_off:I = 0x7f0800d9

.field public static final svc_led_red:I = 0x7f0800d6

.field public static final switchable_log_disabled:I = 0x7f0801c1

.field public static final sys_dump_label:I = 0x7f080170

.field public static final ta_value:I = 0x7f0800ae

.field public static final tag:I = 0x7f08028d

.field public static final tcpdump_Start:I = 0x7f08017f

.field public static final temp_humid_button:I = 0x7f08021e

.field public static final temp_humid_title:I = 0x7f080215

.field public static final temphumid_display:I = 0x7f08021f

.field public static final temphumid_display_comp_check:I = 0x7f080228

.field public static final temphumid_display_humid_comp:I = 0x7f080227

.field public static final temphumid_display_humid_raw:I = 0x7f080225

.field public static final temphumid_display_temp_comp:I = 0x7f080226

.field public static final temphumid_display_temp_raw:I = 0x7f080224

.field public static final temphumid_display_title:I = 0x7f080222

.field public static final temphumid_display_try_count:I = 0x7f080223

.field public static final temphumid_graph:I = 0x7f080220

.field public static final temphumid_thermistor:I = 0x7f080221

.field public static final temphumid_thermistor_title:I = 0x7f080229

.field public static final testTitle:I = 0x7f080253

.field public static final them:I = 0x7f080119

.field public static final threshold_high:I = 0x7f080036

.field public static final threshold_low:I = 0x7f080035

.field public static final threshold_str:I = 0x7f080156

.field public static final time:I = 0x7f08028c

.field public static final time_stamp:I = 0x7f08028f

.field public static final title_activity_beam:I = 0x7f08000e

.field public static final title_barcode_test:I = 0x7f08000d

.field public static final to_comp:I = 0x7f0800ac

.field public static final to_value:I = 0x7f0800ad

.field public static final touch_back:I = 0x7f080134

.field public static final touch_fail:I = 0x7f0800d5

.field public static final touch_home:I = 0x7f080133

.field public static final touch_idac:I = 0x7f080139

.field public static final touch_max:I = 0x7f08013b

.field public static final touch_menu:I = 0x7f080132

.field public static final touch_min:I = 0x7f08013a

.field public static final touch_rawdata:I = 0x7f080136

.field public static final touch_rawdata_max:I = 0x7f080138

.field public static final touch_rawdata_min:I = 0x7f080137

.field public static final touch_recent:I = 0x7f080135

.field public static final touch_title:I = 0x7f080150

.field public static final touchkey_back:I = 0x7f08012b

.field public static final touchkey_exit:I = 0x7f080131

.field public static final touchkey_fwupdate:I = 0x7f08012e

.field public static final touchkey_home:I = 0x7f08012c

.field public static final touchkey_info:I = 0x7f080129

.field public static final touchkey_menu:I = 0x7f08012a

.field public static final touchkey_recent:I = 0x7f08012d

.field public static final touchkey_sensitivity:I = 0x7f080130

.field public static final touchkey_sensitivity_minmax:I = 0x7f08013c

.field public static final touchkey_warning:I = 0x7f08012f

.field public static final try_count:I = 0x7f08006f

.field public static final tsk_part_firmware_str:I = 0x7f08015f

.field public static final tsk_phone_firmware_str:I = 0x7f08015e

.field public static final tsk_update:I = 0x7f080160

.field public static final tsp_dot_test_NTT:I = 0x7f0801dc

.field public static final tsp_dot_test_apollo:I = 0x7f0801e7

.field public static final tsp_dot_test_aries:I = 0x7f0801cf

.field public static final tsp_fw_version_str:I = 0x7f080153

.field public static final tsp_grid_test_NTT:I = 0x7f0801dd

.field public static final tsp_grid_test_apollo:I = 0x7f0801e8

.field public static final tsp_grid_test_aries:I = 0x7f0801d0

.field public static final tsp_part_firmware_str:I = 0x7f080159

.field public static final tsp_phone_firmware_str:I = 0x7f080158

.field public static final tsp_threshold_str:I = 0x7f08015a

.field public static final tsp_update:I = 0x7f08015b

.field public static final tsp_update_factory:I = 0x7f08015d

.field public static final tsp_update_ums:I = 0x7f08015c

.field public static final tspfirmware_str:I = 0x7f080157

.field public static final txt_dps_2000:I = 0x7f080076

.field public static final txt_dps_250:I = 0x7f080074

.field public static final txt_dps_500:I = 0x7f080075

.field public static final txt_ecg_fw_version:I = 0x7f080269

.field public static final txt_ecg_heart_rate:I = 0x7f08026a

.field public static final type_name:I = 0x7f08019e

.field public static final uart_name:I = 0x7f080193

.field public static final uart_to_lte:I = 0x7f080196

.field public static final ultra_sensor:I = 0x7f080028

.field public static final unknown:I = 0x7f0800c8

.field public static final up:I = 0x7f0800c0

.field public static final update_firmware:I = 0x7f0801f6

.field public static final usb_name:I = 0x7f080198

.field public static final usblogging_name:I = 0x7f08019a

.field public static final usbsettings:I = 0x7f0801c0

.field public static final user_measuring:I = 0x7f08028a

.field public static final user_reference_button:I = 0x7f080288

.field public static final user_release_button:I = 0x7f080289

.field public static final uv_adc:I = 0x7f080230

.field public static final uv_display:I = 0x7f080236

.field public static final uv_display_title:I = 0x7f080237

.field public static final uv_exposure_level:I = 0x7f080233

.field public static final uv_hold:I = 0x7f080235

.field public static final uv_ic_check:I = 0x7f08023d

.field public static final uv_ic_check_title:I = 0x7f08023a

.field public static final uv_index_number:I = 0x7f080232

.field public static final uv_intensity:I = 0x7f080231

.field public static final uv_off:I = 0x7f080239

.field public static final uv_on:I = 0x7f080238

.field public static final uv_prod_id:I = 0x7f08023b

.field public static final uv_start:I = 0x7f080234

.field public static final uv_test:I = 0x7f08023c

.field public static final uv_title:I = 0x7f08022f

.field public static final value:I = 0x7f08018d

.field public static final vbatt:I = 0x7f08011a

.field public static final ver_CSC:I = 0x7f080113

.field public static final ver_PDA:I = 0x7f080109

.field public static final ver_Phone:I = 0x7f08010a

.field public static final ver_buildinfo:I = 0x7f080116

.field public static final ver_changelist:I = 0x7f080114

.field public static final ver_fta_swver:I = 0x7f08010e

.field public static final ver_hwver:I = 0x7f08010f

.field public static final ver_hwversion:I = 0x7f080115

.field public static final ver_label_CPU_VERSION:I = 0x7f08010c

.field public static final ver_label_FTA:I = 0x7f08010d

.field public static final ver_label_HWID:I = 0x7f08010b

.field public static final ver_label_hidden:I = 0x7f080111

.field public static final ver_test_gsm:I = 0x7f080110

.field public static final ver_version:I = 0x7f080112

.field public static final ver_version_name:I = 0x7f080102

.field public static final vibration_test_NTT:I = 0x7f0801d7

.field public static final vibration_test_apollo:I = 0x7f0801e2

.field public static final vibration_test_aries:I = 0x7f0801c9

.field public static final volt:I = 0x7f0800e8

.field public static final volume_up_down:I = 0x7f0800be

.field public static final vt_camera_test_aries:I = 0x7f0801cd

.field public static final wacom_firmware_button_kernel:I = 0x7f080162

.field public static final wacom_firmware_button_ums:I = 0x7f080163

.field public static final wacom_firmware_str:I = 0x7f080161

.field public static final wacom_firmware_str_ums:I = 0x7f080164

.field public static final wacom_tuning_version_str:I = 0x7f080169

.field public static final wait:I = 0x7f080093

.field public static final warning_msg:I = 0x7f0801f7

.field public static final working:I = 0x7f080092

.field public static final x_value:I = 0x7f080068

.field public static final y_value:I = 0x7f080069

.field public static final z_value:I = 0x7f08006a

.field public static final zero:I = 0x7f0801eb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
