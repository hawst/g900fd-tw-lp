.class public Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GlucoseSelfTest.java"


# instance fields
.field private WHAT_UPDATE:I

.field private mFeature:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mModuleSensor:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

.field private mSenserID:[I

.field private mSensorID_ADC:I

.field private mSensorID_DAC:I

.field private mSensorID_Initialized:I

.field private mSensorID_None:I

.field private mSensorID_Released:I

.field private mSensorID_Self:I

.field private mSensorID_Status:I

.field private mTableRow_ADC:Landroid/widget/TableRow;

.field private mTableRow_DAC:Landroid/widget/TableRow;

.field private mTableRow_Initialized:Landroid/widget/TableRow;

.field private mTableRow_Offset_H:Landroid/widget/TableRow;

.field private mTableRow_SX:Landroid/widget/TableRow;

.field private mTableRow_SY:Landroid/widget/TableRow;

.field private mTableRow_SZ:Landroid/widget/TableRow;

.field private mTableRow_Status:Landroid/widget/TableRow;

.field private mTableRow_Temp:Landroid/widget/TableRow;

.field private mTextResult:Landroid/widget/TextView;

.field private mText_ADC_X:Landroid/widget/TextView;

.field private mText_ADC_Y:Landroid/widget/TextView;

.field private mText_ADC_Z:Landroid/widget/TextView;

.field private mText_DAC_X:Landroid/widget/TextView;

.field private mText_DAC_Y:Landroid/widget/TextView;

.field private mText_DAC_Z:Landroid/widget/TextView;

.field private mText_Initialized:Landroid/widget/TextView;

.field private mText_Offset_H_X:Landroid/widget/TextView;

.field private mText_Offset_H_Y:Landroid/widget/TextView;

.field private mText_Offset_H_Z:Landroid/widget/TextView;

.field private mText_SX:Landroid/widget/TextView;

.field private mText_SY:Landroid/widget/TextView;

.field private mText_SZ:Landroid/widget/TextView;

.field private mText_Status:Landroid/widget/TextView;

.field private mText_Temp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "GlucoseSelfTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSenserID:[I

    .line 49
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    .line 50
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_Initialized:I

    .line 51
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_Status:I

    .line 52
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_DAC:I

    .line 53
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_ADC:I

    .line 54
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_Self:I

    .line 56
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_None:I

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mSensorID_Released:I

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->WHAT_UPDATE:I

    .line 81
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mHandler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;

    .prologue
    .line 15
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->WHAT_UPDATE:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->update()V

    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method private update()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mFeature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->mFeature:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GlucoseSelfTest;->init()V

    .line 70
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 79
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 74
    return-void
.end method
