.class Lcom/sec/android/app/hwmoduletest/AccImageTest$1;
.super Landroid/os/Handler;
.source "AccImageTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AccImageTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 46
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 52
    :goto_0
    return-void

    .line 48
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->mRawDataText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$100(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->getRawdataString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->access$000(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAngleText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$300(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->getAngleString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->access$200(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
