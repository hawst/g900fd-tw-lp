.class public Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;
.super Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;
.source "ModuleSensor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$1;,
        Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;
    }
.end annotation


# static fields
.field private static DUMMY:I = 0x0

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA022:Ljava/lang/String; = "BOSCH_BMA022"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA023:Ljava/lang/String; = "BOSCH_BMA023"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA220:Ljava/lang/String; = "BOSCH_BMA220"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA222:Ljava/lang/String; = "BOSCH_BMA222"

.field public static final FEATURE_ACCELEROMETER_BOSCH_BMA250:Ljava/lang/String; = "BOSCH_BMA250"

.field public static final FEATURE_ACCELEROMETER_BOSCH_SMB380:Ljava/lang/String; = "BOSCH_SMB380"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6050:Ljava/lang/String; = "INVENSENSE_MPU6050"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6051:Ljava/lang/String; = "INVENSENSE_MPU6051"

.field public static final FEATURE_ACCELEROMETER_INVENSENSE_MPU6515M:Ljava/lang/String; = "INVENSENSE_MPU6515M"

.field public static final FEATURE_ACCELEROMETER_KIONIX_KXTF9:Ljava/lang/String; = "KIONIX_KXTF9"

.field public static final FEATURE_ACCELEROMETER_KIONIX_KXUD9:Ljava/lang/String; = "KIONIX_KXUD9"

.field public static final FEATURE_ACCELEROMETER_STMICRO_K3DH:Ljava/lang/String; = "STMICRO_K3DH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_KR3DH:Ljava/lang/String; = "STMICRO_KR3DH"

.field public static final FEATURE_ACCELEROMETER_STMICRO_KR3DM:Ljava/lang/String; = "STMICRO_KR3DM"

.field public static final FEATURE_ACCELEROMETER_STMICRO_LSM330DLC:Ljava/lang/String; = "STMICRO_LSM330DLC"

.field public static final FEATURE_GESTURE_MAX88922:Ljava/lang/String; = "MAX88922"

.field public static final FEATURE_GESTURE_TMG3992:Ljava/lang/String; = "TMG3992"

.field public static final FEATURE_GYROSCOP_BOSCH:Ljava/lang/String; = "BOSCH"

.field public static final FEATURE_GYROSCOP_INVENSENSE:Ljava/lang/String; = "INVENSENSE"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6050:Ljava/lang/String; = "INVENSENSE_MPU6050"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6051:Ljava/lang/String; = "INVENSENSE_MPU6051"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6051M:Ljava/lang/String; = "INVENSENSE_MPU6051M"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6500:Ljava/lang/String; = "INVENSENSE_MPU6500"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6515:Ljava/lang/String; = "INVENSENSE_MPU6515"

.field public static final FEATURE_GYROSCOP_INVENSENSE_MPU6515M:Ljava/lang/String; = "INVENSENSE_MPU6515M"

.field public static final FEATURE_GYROSCOP_MAXIM:Ljava/lang/String; = "MAXIM"

.field public static final FEATURE_GYROSCOP_STMICRO_SMARTPHONE:Ljava/lang/String; = "STMICRO_SMARTPHONE"

.field public static final FEATURE_GYROSCOP_STMICRO_TABLET:Ljava/lang/String; = "STMICRO_TABLET"

.field public static final FEATURE_MAGENTIC_AK09911:Ljava/lang/String; = "AK09911"

.field public static final FEATURE_MAGENTIC_AK09911C:Ljava/lang/String; = "AK09911C"

.field public static final FEATURE_MAGENTIC_AK8963:Ljava/lang/String; = "AK8963"

.field public static final FEATURE_MAGENTIC_AK8963C:Ljava/lang/String; = "AK8963C"

.field public static final FEATURE_MAGENTIC_AK8963C_MANAGER:Ljava/lang/String; = "AK8963C_MANAGER"

.field public static final FEATURE_MAGENTIC_AK8973:Ljava/lang/String; = "AK8973"

.field public static final FEATURE_MAGENTIC_AK8975:Ljava/lang/String; = "AK8975"

.field public static final FEATURE_MAGENTIC_BMC150:Ljava/lang/String; = "BOSCH_BMC150"

.field public static final FEATURE_MAGENTIC_BMC150_COMBINATION:Ljava/lang/String; = "BMC150_COMBINATION"

.field public static final FEATURE_MAGENTIC_BMC150_NEWEST:Ljava/lang/String; = "BMC150_NEWEST"

.field public static final FEATURE_MAGENTIC_BMC150_POWER_NOISE:Ljava/lang/String; = "BOSCH_BMC150_POWER_NOISE"

.field public static final FEATURE_MAGENTIC_HSCDTD004:Ljava/lang/String; = "HSCDTD004"

.field public static final FEATURE_MAGENTIC_HSCDTD004A:Ljava/lang/String; = "HSCDTD004A"

.field public static final FEATURE_MAGENTIC_HSCDTD006A:Ljava/lang/String; = "HSCDTD006A"

.field public static final FEATURE_MAGENTIC_HSCDTD008A:Ljava/lang/String; = "HSCDTD008A"

.field public static final FEATURE_MAGENTIC_STMICRO:Ljava/lang/String; = "STMICRO_K303C"

.field public static final FEATURE_MAGENTIC_YAS529:Ljava/lang/String; = "YAS529"

.field public static final FEATURE_MAGENTIC_YAS530:Ljava/lang/String; = "YAS530"

.field public static final FEATURE_MAGENTIC_YAS530A:Ljava/lang/String; = "YAS530A"

.field public static final FEATURE_MAGENTIC_YAS530C:Ljava/lang/String; = "YAS530C"

.field public static final FEATURE_MAGENTIC_YAS532:Ljava/lang/String; = "YAS532"

.field public static final FEATURE_MAGENTIC_YAS532B:Ljava/lang/String; = "YAS532B"

.field public static final GRIP_STATUS_DETECT:I = 0x1

.field public static final GRIP_STATUS_RELEASE:I = 0x0

.field public static final ID_FILE____ACCELEROMETER:I

.field public static final ID_FILE____ACCELEROMETER_CAL:I

.field public static final ID_FILE____ACCELEROMETER_INTPIN:I

.field public static final ID_FILE____ACCELEROMETER_N_ANGLE:I

.field public static final ID_FILE____ACCELEROMETER_SELF:I

.field public static final ID_FILE____BAROMETER_EEPROM:I

.field public static final ID_FILE____COUNT:I

.field public static final ID_FILE____GYRO_POWER:I

.field public static final ID_FILE____GYRO_SELFTEST:I

.field public static final ID_FILE____GYRO_TEMPERATURE:I

.field public static final ID_FILE____LIGHT:I

.field public static final ID_FILE____LIGHT_ADC:I

.field public static final ID_FILE____LIGHT_CCT:I

.field public static final ID_FILE____MAGNETIC_ADC:I

.field public static final ID_FILE____MAGNETIC_DAC:I

.field public static final ID_FILE____MAGNETIC_POWER_OFF:I

.field public static final ID_FILE____MAGNETIC_POWER_ON:I

.field public static final ID_FILE____MAGNETIC_SELF:I

.field public static final ID_FILE____MAGNETIC_STATUS:I

.field public static final ID_FILE____MAGNETIC_TEMPERATURE:I

.field public static final ID_FILE____PROXIMITY_ADC:I

.field public static final ID_FILE____PROXIMITY_AVG:I

.field public static final ID_FILE____PROXIMITY_OFFSET:I

.field public static final ID_INTENT__COUNT:I

.field public static final ID_INTENT__CP_ACCELEROMETER:I

.field public static final ID_INTENT__GRIP:I

.field public static final ID_MANAGER_ACCELEROMETER:I

.field public static final ID_MANAGER_ACCELEROMETER_N_ANGLE:I

.field public static final ID_MANAGER_ACCELEROMETER_SELF:I

.field public static final ID_MANAGER_BAROMETER:I

.field public static final ID_MANAGER_COUNT:I

.field public static final ID_MANAGER_GYRO:I

.field public static final ID_MANAGER_GYRO_EXPANSION:I

.field public static final ID_MANAGER_GYRO_POWER:I

.field public static final ID_MANAGER_GYRO_SELF:I

.field public static final ID_MANAGER_GYRO_TEMPERATURE:I

.field public static final ID_MANAGER_LIGHT:I

.field public static final ID_MANAGER_MAGNETIC:I

.field public static final ID_MANAGER_MAGNETIC_ADC:I

.field public static final ID_MANAGER_MAGNETIC_DAC:I

.field public static final ID_MANAGER_MAGNETIC_OFFSETH:I

.field public static final ID_MANAGER_MAGNETIC_POWER_OFF:I

.field public static final ID_MANAGER_MAGNETIC_POWER_ON:I

.field public static final ID_MANAGER_MAGNETIC_SELF:I

.field public static final ID_MANAGER_MAGNETIC_STATUS:I

.field public static final ID_MANAGER_MAGNETIC_TEMPERATURE:I

.field public static final ID_MANAGER_PROXIMITY:I

.field public static final ID_SCOPE_MAX:I

.field public static final ID_SCOPE_MIN:I

.field public static final RETURN_DATA_ARRAY_INDEX_1_NG:Ljava/lang/String; = "NG"

.field public static final RETURN_DATA_ARRAY_INDEX_1_NONE:Ljava/lang/String; = "None"

.field public static final RETURN_DATA_ARRAY_INDEX_1_OK:Ljava/lang/String; = "OK"

.field public static final TARGET_FILE:I = 0x2

.field public static final TARGET_INTENT:I = 0x3

.field public static final TARGET_MANAGER:I = 0x1

.field public static final WHAT_NOTI_SENSOR_UPDATAE:I

.field private static mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;


# instance fields
.field private final DEBUG:Z

.field private final RETURN_DATA_ARRAY_SIZE_MAX:I

.field private final STANDARD_GRAVITY:F

.field public mFeature_Accelerometer:Ljava/lang/String;

.field public mFeature_Gesture:Ljava/lang/String;

.field public mFeature_Gyroscope:Ljava/lang/String;

.field public mFeature_Magnetic:Ljava/lang/String;

.field private mInterrupted:Z

.field private mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

.field private mReturnData:[Ljava/lang/String;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

.field private mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

.field private mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

.field private mTemp_Float:[F

.field private mTemp_Int:[I

.field private mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

.field private mTemp_String:[Ljava/lang/String;

.field private magnitude:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    .line 99
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    .line 101
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    .line 102
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    .line 103
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    .line 104
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    .line 105
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    .line 106
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    .line 107
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    .line 108
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    .line 109
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    .line 110
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    .line 111
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    .line 112
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    .line 113
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    .line 114
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    .line 115
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    .line 116
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    .line 117
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    .line 118
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    .line 119
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    .line 120
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    .line 121
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    .line 123
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    .line 124
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    .line 125
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    .line 126
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    .line 127
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    .line 128
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    .line 129
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    .line 130
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    .line 131
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    .line 132
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    .line 133
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_CCT:I

    .line 134
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    .line 135
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    .line 136
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    .line 137
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    .line 138
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    .line 139
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    .line 140
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    .line 141
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    .line 142
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    .line 143
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    .line 144
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    .line 145
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    .line 148
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    .line 150
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    .line 151
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__COUNT:I

    .line 152
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DUMMY:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MAX:I

    .line 167
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x0

    .line 182
    const-string v0, "ModuleSensor"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->DEBUG:Z

    .line 19
    const-string v0, "SENSOR_NAME_ACCELEROMETER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Accelerometer:Ljava/lang/String;

    .line 37
    const-string v0, "SENSOR_NAME_GYROSCOPE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    .line 52
    const-string v0, "SENSOR_NAME_MAGNETIC"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    .line 77
    const-string v0, "SENSOR_NAME_GESTURE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gesture:Ljava/lang/String;

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    .line 155
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->RETURN_DATA_ARRAY_SIZE_MAX:I

    .line 156
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    .line 161
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    .line 162
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 163
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 164
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 168
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    .line 169
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .line 170
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    .line 173
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 176
    const v0, 0x411ce80a

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->STANDARD_GRAVITY:F

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleSensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 185
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->initialize()V

    .line 186
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    return v0
.end method

.method private computeForCCT([Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "input"    # [Ljava/lang/String;

    .prologue
    .line 2444
    const/4 v11, 0x0

    aget-object v11, p1, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 2445
    .local v9, "mRedValue":I
    const/4 v11, 0x1

    aget-object v11, p1, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 2446
    .local v8, "mGreenValue":I
    const/4 v11, 0x2

    aget-object v11, p1, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 2447
    .local v7, "mBlueValue":I
    const/4 v11, 0x3

    aget-object v11, p1, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 2448
    .local v10, "mWhiteValue":I
    const/4 v1, -0x1

    .line 2449
    .local v1, "cctValue":I
    if-eqz v10, :cond_1

    int-to-float v11, v8

    int-to-float v12, v10

    div-float v0, v11, v12

    .line 2452
    .local v0, "I_cf":F
    :goto_0
    const v11, 0xf230

    if-lt v10, v11, :cond_2

    .line 2453
    const/16 v1, 0xa8c

    .line 2507
    :cond_0
    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 2449
    .end local v0    # "I_cf":F
    :cond_1
    int-to-float v0, v8

    goto :goto_0

    .line 2455
    .restart local v0    # "I_cf":F
    :cond_2
    float-to-double v12, v0

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    cmpg-double v11, v12, v14

    if-gtz v11, :cond_6

    .line 2456
    if-eqz v8, :cond_3

    sub-int v11, v9, v7

    int-to-double v12, v11

    int-to-double v14, v8

    div-double/2addr v12, v14

    const-wide v14, 0x3feae147ae147ae1L    # 0.84

    add-double v2, v12, v14

    .line 2459
    .local v2, "ccti":D
    :goto_2
    const-wide v12, 0x3fc3d70a3d70a3d7L    # 0.155

    cmpg-double v11, v2, v12

    if-gtz v11, :cond_4

    .line 2460
    const/16 v1, 0x1bb8

    goto :goto_1

    .line 2456
    .end local v2    # "ccti":D
    :cond_3
    sub-int v11, v9, v7

    int-to-double v12, v11

    const-wide v14, 0x3feae147ae147ae1L    # 0.84

    add-double v2, v12, v14

    goto :goto_2

    .line 2461
    .restart local v2    # "ccti":D
    :cond_4
    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpl-double v11, v2, v12

    if-ltz v11, :cond_5

    .line 2462
    const/16 v1, 0x9a3

    goto :goto_1

    .line 2464
    :cond_5
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    goto :goto_1

    .line 2467
    .end local v2    # "ccti":D
    :cond_6
    if-eqz v8, :cond_7

    sub-int v11, v9, v7

    int-to-double v12, v11

    int-to-double v14, v8

    div-double/2addr v12, v14

    const-wide v14, 0x3fd28f5c28f5c28fL    # 0.29

    add-double v2, v12, v14

    .line 2470
    .restart local v2    # "ccti":D
    :goto_3
    const-wide v12, 0x3fc3d70a3d70a3d7L    # 0.155

    cmpg-double v11, v2, v12

    if-gtz v11, :cond_8

    .line 2471
    const/16 v1, 0x1bb8

    goto :goto_1

    .line 2467
    .end local v2    # "ccti":D
    :cond_7
    sub-int v11, v9, v7

    int-to-double v12, v11

    const-wide v14, 0x3fd28f5c28f5c28fL    # 0.29

    add-double v2, v12, v14

    goto :goto_3

    .line 2472
    .restart local v2    # "ccti":D
    :cond_8
    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpl-double v11, v2, v12

    if-ltz v11, :cond_9

    .line 2473
    const/16 v1, 0x9a3

    goto :goto_1

    .line 2475
    :cond_9
    float-to-double v12, v0

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    cmpl-double v11, v12, v14

    if-lez v11, :cond_b

    float-to-double v12, v0

    const-wide v14, 0x3feccccccccccccdL    # 0.9

    cmpg-double v11, v12, v14

    if-gez v11, :cond_b

    .line 2476
    const-wide v12, 0x3fd999999999999aL    # 0.4

    cmpl-double v11, v2, v12

    if-lez v11, :cond_a

    const-wide v12, 0x3ff199999999999aL    # 1.1

    cmpg-double v11, v2, v12

    if-gez v11, :cond_a

    .line 2477
    const-wide v12, 0x40a5180000000000L    # 2700.0

    const-wide v14, -0x400dfbe76c8b4396L    # -1.126

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    goto/16 :goto_1

    .line 2478
    :cond_a
    const-wide v12, 0x3ff199999999999aL    # 1.1

    cmpl-double v11, v2, v12

    if-lez v11, :cond_0

    const-wide v12, 0x3ff6666666666666L    # 1.4

    cmpg-double v11, v2, v12

    if-gez v11, :cond_0

    .line 2479
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    goto/16 :goto_1

    .line 2481
    :cond_b
    float-to-double v12, v0

    const-wide v14, 0x3feccccccccccccdL    # 0.9

    cmpl-double v11, v12, v14

    if-ltz v11, :cond_c

    float-to-double v12, v0

    const-wide v14, 0x3ffa666666666666L    # 1.65

    cmpg-double v11, v12, v14

    if-gez v11, :cond_c

    .line 2482
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v2, v3, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    goto/16 :goto_1

    .line 2483
    :cond_c
    float-to-double v12, v0

    const-wide v14, 0x3ffa666666666666L    # 1.65

    cmpl-double v11, v12, v14

    if-ltz v11, :cond_e

    float-to-double v12, v0

    const-wide v14, 0x3ffe666666666666L    # 1.9

    cmpg-double v11, v12, v14

    if-gtz v11, :cond_e

    .line 2484
    const-wide v12, 0x3fb1eb851eb851ecL    # 0.07

    float-to-double v14, v0

    mul-double v4, v12, v14

    .line 2486
    .local v4, "cctixlamp":D
    if-eq v9, v7, :cond_d

    int-to-float v11, v10

    sub-int v12, v9, v7

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    int-to-float v12, v12

    div-float/2addr v11, v12

    mul-float/2addr v11, v0

    float-to-double v12, v11

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    :goto_4
    double-to-float v6, v12

    .line 2489
    .local v6, "cctxlamp_gain":F
    const v11, 0x45354000    # 2900.0f

    mul-float/2addr v11, v6

    float-to-double v12, v11

    const-wide v14, -0x402147ae147ae148L    # -0.48

    invoke-static {v4, v5, v14, v15}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    .line 2490
    goto/16 :goto_1

    .line 2486
    .end local v6    # "cctxlamp_gain":F
    :cond_d
    int-to-float v11, v10

    mul-float/2addr v11, v0

    float-to-double v12, v11

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    goto :goto_4

    .line 2491
    .end local v4    # "cctixlamp":D
    :cond_e
    const-wide v12, 0x40a6a80000000000L    # 2900.0

    const-wide v14, 0x3fbf8a0902de00d2L    # 0.1232

    const-wide v16, -0x402147ae147ae148L    # -0.48

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    mul-double/2addr v12, v14

    double-to-int v1, v12

    goto/16 :goto_1
.end method

.method private computeForLux([Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "input"    # [Ljava/lang/String;

    .prologue
    const-wide v10, 0x3feccccccccccccdL    # 0.9

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 2418
    const/4 v6, 0x1

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2419
    .local v1, "green":I
    const/4 v6, 0x3

    aget-object v6, p1, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2420
    .local v3, "white":I
    const/4 v2, -0x1

    .line 2421
    .local v2, "luxValue":I
    if-eqz v3, :cond_0

    int-to-float v6, v1

    int-to-float v7, v3

    div-float v0, v6, v7

    .line 2422
    .local v0, "I_cf":F
    :goto_0
    const-wide v4, 0x3fc70a3d70a3d70aL    # 0.18

    .line 2424
    .local v4, "sensitivity":D
    const/4 v6, 0x5

    if-ge v1, v6, :cond_1

    .line 2425
    const-string v6, "0"

    .line 2439
    :goto_1
    return-object v6

    .line 2421
    .end local v0    # "I_cf":F
    .end local v4    # "sensitivity":D
    :cond_0
    int-to-float v0, v1

    goto :goto_0

    .line 2429
    .restart local v0    # "I_cf":F
    .restart local v4    # "sensitivity":D
    :cond_1
    float-to-double v6, v0

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_2

    .line 2430
    const-wide v6, 0x3fa3404ea4a8c155L    # 0.0376

    int-to-double v8, v1

    mul-double/2addr v8, v10

    const-wide v10, 0x3ff53f7ced916873L    # 1.328

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-int v2, v6

    .line 2439
    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 2432
    :cond_2
    float-to-double v6, v0

    cmpg-double v6, v8, v6

    if-gez v6, :cond_3

    float-to-double v6, v0

    cmpg-double v6, v6, v10

    if-gez v6, :cond_3

    .line 2433
    int-to-double v6, v1

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    float-to-double v10, v0

    mul-double/2addr v8, v10

    const-wide v10, 0x3fdd70a3d70a3d71L    # 0.46

    sub-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v2, v6

    goto :goto_2

    .line 2436
    :cond_3
    int-to-double v6, v1

    mul-double/2addr v6, v4

    const-wide v8, 0x4022800000000000L    # 9.25

    mul-double/2addr v6, v8

    float-to-double v8, v0

    div-double/2addr v6, v8

    double-to-int v2, v6

    goto :goto_2
.end method

.method private dataCheck([Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "data"    # [Ljava/lang/String;

    .prologue
    .line 2295
    const-string v2, ""

    .line 2296
    .local v2, "result":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2298
    .local v1, "length":I
    if-eqz p1, :cond_0

    .line 2299
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2302
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "dataCheck"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "length : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2304
    if-eqz p1, :cond_2

    .line 2305
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    add-int/lit8 v3, v1, 0x1

    if-ge v0, v3, :cond_2

    .line 2306
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2308
    if-ge v0, v1, :cond_1

    .line 2309
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2305
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2314
    .end local v0    # "i":I
    :cond_2
    return-object v2
.end method

.method private getAccelermeterCal(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 721
    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 755
    :cond_0
    :goto_0
    return-object v0

    .line 723
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v0, v0

    if-gt v0, v4, :cond_2

    .line 728
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v0, v0, v5

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 732
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterCal"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v5

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v4

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v4

    aput-object v2, v0, v1

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v1

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterCal"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 742
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getAccelermeterCal"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getAccelermeterIntpin(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 762
    const-string v1, "2"

    .line 763
    .local v1, "INTPIN_ENABLE":Ljava/lang/String;
    const-string v0, "0"

    .line 765
    .local v0, "INTPIN_DISABLE":Ljava/lang/String;
    if-ne p1, v8, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v4, :cond_1

    .line 803
    :cond_0
    :goto_0
    return-object v3

    .line 767
    :cond_1
    if-ne p1, v9, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v4, :cond_0

    .line 768
    const-string v4, "ACCEL_SENSOR_INTPIN"

    invoke-static {v4, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 770
    const-wide/16 v4, 0x12c

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 776
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v5, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 778
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 780
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAccelermeterIntpin"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "2"

    aput-object v4, v3, v7

    .line 784
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "None"

    aput-object v4, v3, v8

    .line 785
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v9

    .line 786
    const-string v3, "ACCEL_SENSOR_INTPIN"

    invoke-static {v3, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 800
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAccelermeterIntpin"

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 771
    :catch_0
    move-exception v2

    .line 773
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 789
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getAccelermeterIntpin"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAccelermeterSelf(I)[Ljava/lang/String;
    .locals 6
    .param p1, "target"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 692
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 695
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v3

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v5

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v4

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v3

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v1, v1, v3

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v3, v3, v4

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->getResultAccelerometerSelf(III)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterSelf"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :goto_0
    return-object v0

    .line 704
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterSelf"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAccelermeterXYZ(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 510
    if-ne p1, v6, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v3, :cond_4

    .line 511
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnAccelermeter()[F

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 513
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v3, :cond_3

    .line 515
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getAccelermeterXYZ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "4"

    aput-object v3, v2, v8

    .line 519
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "None"

    aput-object v3, v2, v6

    .line 520
    const-wide/16 v0, 0x0

    .line 522
    .local v0, "changeBit":D
    const-string v2, "12"

    const-string v3, "SENSOR_TEST_ACC_BIT"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 523
    const-wide v0, 0x405a1f58d0fac687L    # 104.48979591836734

    .line 528
    :goto_0
    const-string v2, "IS_SENSOR_TEST_ACC_REVERSE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 529
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v4, v4, v6

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    mul-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 530
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v4, v4, v8

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    mul-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    .line 536
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v5, v5, v7

    float-to-double v6, v5

    mul-double/2addr v6, v0

    double-to-int v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 569
    .end local v0    # "changeBit":D
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getAccelermeterXYZ"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_3
    return-object v2

    .line 525
    .restart local v0    # "changeBit":D
    :cond_1
    const-wide/high16 v0, 0x4038000000000000L    # 24.0

    goto :goto_0

    .line 532
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v4, v4, v8

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 533
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v4, v4, v6

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-int v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    goto :goto_1

    .line 539
    .end local v0    # "changeBit":D
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAccelermeterXYZ"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 544
    :cond_4
    if-ne p1, v7, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v3, :cond_0

    .line 545
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 547
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 549
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getAccelermeterXYZ"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "4"

    aput-object v3, v2, v8

    .line 553
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "None"

    aput-object v3, v2, v6

    .line 554
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    aput-object v3, v2, v7

    .line 555
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v6

    aput-object v3, v2, v9

    .line 556
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v2, v3

    goto/16 :goto_2

    .line 559
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAccelermeterXYZ"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method private getAccelermeterXYZnAngle(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x2

    .line 579
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v5

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v6

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v10

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aput v1, v0, v8

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->accelerometerAngle([I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 588
    :cond_0
    if-ne p1, v6, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v0, :cond_2

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnAccelermeter()[F

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "10"

    aput-object v1, v0, v5

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v6

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v0, v1

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v1

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v1

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v0, v0, v5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v1, v1, v5

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v1, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v6

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v1, v1, v8

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v8

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    const v4, 0x411ce80a

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v5

    float-to-double v4, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v6

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    neg-double v4, v4

    double-to-float v3, v4

    const v4, 0x42652ee1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "coordinates_x :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coordinates_y :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coordinates_z :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "X_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Y_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", z_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "magnitude :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0x8

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deviation :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0x9

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "XY_value :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :goto_1
    return-object v0

    .line 615
    :cond_2
    if-ne p1, v8, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v0, :cond_4

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "7"

    aput-object v1, v0, v5

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v6

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v0, v1

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v1

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v1

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterXYZNAngle"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 631
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterXYZnAngle"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 638
    :cond_4
    if-ne p1, v9, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    if-eqz v0, :cond_7

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->returnCPsAccelerometerData()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    .line 640
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    if-eqz v0, :cond_5

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;->accelerometerAngle([I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 644
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterXYZnAngle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mTemp_Float.length : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "10"

    aput-object v1, v0, v5

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v6

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v10

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v5

    aput-object v2, v0, v1

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v6

    aput-object v2, v0, v1

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v8

    aput-object v2, v0, v1

    .line 657
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v0, v0, v5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v1, v1, v5

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v1, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v6

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v1, v1, v8

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v8

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0x9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->magnitude:F

    const v4, 0x411ce80a

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v1, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v3, v3, v5

    int-to-double v4, v3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v3, v3, v6

    int-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    neg-double v4, v4

    double-to-float v3, v4

    const v4, 0x42652ee1

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CP: coordinates_x :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coordinates_y :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coordinates_z :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CP: X_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Y_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", z_Angle :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CP: magnitude :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0x8

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", deviation :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0x9

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterRawXYZ"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CP: XY_value :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 674
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAccelermeterXYZnAngle"

    const-string v2, "null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 682
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method private getBarometer(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 810
    if-ne p1, v5, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v2, :cond_2

    .line 811
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnBarometer()[F

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 813
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v2, :cond_1

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBarometer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Count : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 818
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 819
    .local v0, "format":Ljava/text/DecimalFormat;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v2, "4"

    aput-object v2, v1, v7

    .line 820
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v2, "None"

    aput-object v2, v1, v5

    .line 821
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v7

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 822
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v5

    float-to-double v4, v3

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 823
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v6

    float-to-double v4, v3

    invoke-virtual {v0, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 838
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBarometer"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    .end local v0    # "format":Ljava/text/DecimalFormat;
    :cond_0
    :goto_0
    return-object v1

    .line 826
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getBarometer"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 831
    :cond_2
    if-ne p1, v6, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private getBarometerEEPROM(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 848
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 876
    :cond_0
    :goto_0
    return-object v0

    .line 850
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 851
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 855
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getBarometerEEPROM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 859
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 860
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 873
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getBarometerEEPROM"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 863
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getBarometerEEPROM"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getGrip(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 883
    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 914
    :cond_0
    :goto_0
    return-object v0

    .line 885
    :cond_1
    if-ne p1, v5, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-nez v1, :cond_0

    .line 887
    :cond_2
    if-ne p1, v7, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    if-eqz v1, :cond_0

    .line 888
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->returnGrip()[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    .line 890
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    if-eqz v1, :cond_3

    .line 892
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGrip"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "3"

    aput-object v1, v0, v6

    .line 896
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 897
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Int:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 911
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyro"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto/16 :goto_0

    .line 901
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getGrip"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getGyro(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const v4, 0x42652ee0

    .line 921
    if-ne p1, v5, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_2

    .line 922
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnGyro()[F

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 924
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v1, :cond_1

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyro"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v7

    .line 930
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v7

    mul-float/2addr v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 932
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v5

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 933
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v6

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyro"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    .line 936
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getGyro"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 941
    :cond_2
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private getGyroExpansion(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 993
    if-ne p1, v5, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v2, :cond_4

    .line 998
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;

    invoke-direct {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;-><init>()V

    .line 1000
    .local v0, "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-eqz v2, :cond_1

    .line 1002
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v2, "10"

    aput-object v2, v1, v7

    .line 1003
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mReturnValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 1004
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 1005
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1006
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1007
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1008
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1009
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    aget-short v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1010
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1011
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v2, 0x9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1012
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v2, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1038
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getGyroExpansion"

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    .end local v0    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_0
    :goto_0
    return-object v1

    .line 1016
    .restart local v0    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_1
    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-nez v2, :cond_2

    .line 1017
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroExpansion"

    const-string v4, "Noise Bias null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mData:[S

    if-nez v2, :cond_3

    .line 1021
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroExpansion"

    const-string v4, "Data null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    :cond_3
    iget-object v2, v0, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-nez v2, :cond_0

    .line 1025
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroExpansion"

    const-string v4, "RMS null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1031
    .end local v0    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_4
    if-ne p1, v6, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private getGyroPower(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 958
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 986
    :cond_0
    :goto_0
    return-object v0

    .line 960
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 961
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 963
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyroPower"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 969
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 970
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 983
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyroPower"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 973
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getGyroPower"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getGyroSelf(I)[Ljava/lang/String;
    .locals 11
    .param p1, "target"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x6

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1083
    if-ne p1, v6, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v2, :cond_5

    .line 1087
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;-><init>()V

    .line 1089
    .local v1, "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    iget-object v2, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-eqz v2, :cond_2

    .line 1090
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "7"

    aput-object v3, v2, v7

    .line 1091
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mReturnValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 1092
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 1093
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    .line 1094
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    aget v5, v5, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1095
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1096
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    .line 1097
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    aget v5, v5, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1099
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v3, "STMICRO_SMARTPHONE"

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v3, "STMICRO_TABLET"

    if-ne v2, v3, :cond_1

    .line 1101
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "8"

    aput-object v3, v2, v7

    .line 1102
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v3, 0x8

    const-string v4, ""

    aput-object v4, v2, v3

    .line 1160
    .end local v1    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroSelf"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :goto_1
    return-object v2

    .line 1108
    .restart local v1    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_2
    iget-object v2, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mNoiseBias:[F

    if-nez v2, :cond_3

    .line 1109
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroSelf"

    const-string v4, "Noise Bias null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    :cond_3
    iget-object v2, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;->mRMSValue:[F

    if-nez v2, :cond_4

    .line 1113
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroSelf"

    const-string v4, "RMS null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 1119
    .end local v1    # "returnValue":Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$GyroExpansionData;
    :cond_5
    if-ne p1, v8, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v2, :cond_9

    .line 1120
    const-string v0, ""

    .line 1121
    .local v0, "resultValue":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v3, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1123
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 1125
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroTemperature"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1128
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v9

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1129
    const-string v0, "0"

    .line 1134
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "7"

    aput-object v3, v2, v7

    .line 1135
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 1136
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    .line 1138
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v5, v5, v8

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1139
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v5, v5, v10

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1140
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    .line 1141
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v3, 0x7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v3, "STMICRO_SMARTPHONE"

    if-eq v2, v3, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Gyroscope:Ljava/lang/String;

    const-string v3, "STMICRO_TABLET"

    if-ne v2, v3, :cond_1

    .line 1145
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "8"

    aput-object v3, v2, v7

    .line 1146
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/16 v3, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v5, v5, v9

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    goto/16 :goto_0

    .line 1131
    :cond_7
    const-string v0, "-1"

    goto/16 :goto_2

    .line 1150
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getGyroSelfTest"

    const-string v4, "null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1156
    .end local v0    # "resultValue":Ljava/lang/String;
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method private getGyroTemperature(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1048
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 1076
    :cond_0
    :goto_0
    return-object v0

    .line 1050
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 1051
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1053
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyroTemperature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1060
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 1073
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getGyroTemperature"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 1063
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getGyroTemperature"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getLight(I)[Ljava/lang/String;
    .locals 10
    .param p1, "target"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1170
    const/16 v4, 0x10

    new-array v1, v4, [Ljava/lang/String;

    .line 1172
    .local v1, "mReturnData":[Ljava/lang/String;
    if-ne p1, v8, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v4, :cond_1

    .line 1173
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnLight()[F

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 1175
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v4, :cond_0

    .line 1177
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLight"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 1181
    const-string v3, "None"

    aput-object v3, v1, v8

    .line 1182
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v9

    .line 1217
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLight"

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    .end local v1    # "mReturnData":[Ljava/lang/String;
    :goto_1
    return-object v1

    .line 1185
    .restart local v1    # "mReturnData":[Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getLight"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 1188
    goto :goto_1

    .line 1190
    :cond_1
    if-ne p1, v9, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v4, :cond_4

    .line 1191
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v5, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1193
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 1194
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v0, v3

    .line 1195
    .local v0, "length":I
    const-string v2, ""

    .line 1198
    .local v2, "resultstring":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLight"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    if-ne v0, v8, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v3, v7

    .line 1202
    :goto_2
    const-string v3, "2"

    aput-object v3, v1, v7

    .line 1203
    const-string v3, "None"

    aput-object v3, v1, v8

    .line 1204
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v9

    goto :goto_0

    .line 1201
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->computeForLux([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1207
    .end local v0    # "length":I
    .end local v2    # "resultstring":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getLight"

    const-string v6, "null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v3

    .line 1210
    goto :goto_1

    :cond_4
    move-object v1, v3

    .line 1213
    goto :goto_1
.end method

.method private getLightADC(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 1271
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 1299
    :cond_0
    :goto_0
    return-object v0

    .line 1273
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 1274
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1276
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getLightADC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 1296
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getLightADC"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 1286
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getLightADC"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getLightCCT(I)[Ljava/lang/String;
    .locals 9
    .param p1, "target"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1227
    if-ne p1, v6, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v3, :cond_1

    .line 1261
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getLight"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_1
    return-object v2

    .line 1234
    :cond_1
    if-ne p1, v8, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v3, :cond_0

    .line 1235
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1237
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1238
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v0, v2

    .line 1239
    .local v0, "length":I
    const-string v1, ""

    .line 1242
    .local v1, "resultstring":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getLight"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1245
    if-ne v0, v6, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v2, v7

    .line 1246
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "2"

    aput-object v3, v2, v7

    .line 1247
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v3, "None"

    aput-object v3, v2, v6

    .line 1248
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    goto :goto_0

    .line 1245
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->computeForCCT([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1251
    .end local v0    # "length":I
    .end local v1    # "resultstring":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLight"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getMagnetic(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1306
    if-ne p1, v4, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_2

    .line 1307
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagnetic()[F

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 1309
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v1, :cond_1

    .line 1311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagnetic"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v6

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v4

    .line 1316
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 1317
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1318
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1333
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagnetic"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    .line 1321
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagnetic"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1326
    :cond_2
    if-ne p1, v5, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private getMagneticADC(I)[Ljava/lang/String;
    .locals 12
    .param p1, "target"    # I

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 1864
    if-ne p1, v7, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v3, :cond_9

    .line 1865
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1876
    :cond_0
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1900
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 1902
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticADC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v6, v6, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1905
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v9

    .line 1906
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v7

    .line 1907
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v8

    .line 1908
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v10

    .line 1909
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mADC:[Ljava/lang/String;

    aget-object v4, v4, v10

    aput-object v4, v3, v11

    .line 1991
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticADC"

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1994
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :goto_2
    return-object v3

    .line 1877
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1885
    :cond_3
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1886
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1889
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1891
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BOSCH_BMC150"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1897
    :cond_7
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1912
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticADC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1915
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1917
    :cond_9
    if-ne p1, v8, :cond_12

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v3, :cond_12

    .line 1918
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1920
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v3, :cond_11

    .line 1922
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticADC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1925
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1929
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1930
    .local v0, "x":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1931
    .local v1, "y":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1932
    .local v2, "z":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v9

    .line 1934
    const/16 v3, -0x7d0

    if-gt v3, v0, :cond_b

    const/16 v3, 0x7d0

    if-gt v0, v3, :cond_b

    const/16 v3, -0x7d0

    if-gt v3, v1, :cond_b

    const/16 v3, 0x7d0

    if-gt v1, v3, :cond_b

    const/16 v3, -0x7d0

    if-gt v3, v2, :cond_b

    const/16 v3, 0x7d0

    if-gt v2, v3, :cond_b

    .line 1936
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "OK"

    aput-object v4, v3, v7

    .line 1941
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v8

    .line 1942
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v10

    .line 1943
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1938
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "NG"

    aput-object v4, v3, v7

    goto :goto_3

    .line 1944
    .end local v0    # "x":I
    .end local v1    # "y":I
    .end local v2    # "z":I
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1946
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1947
    .restart local v0    # "x":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1948
    .restart local v1    # "y":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1949
    .restart local v2    # "z":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v9

    .line 1950
    const/16 v3, -0x2000

    if-gt v3, v0, :cond_d

    const/16 v3, 0x2000

    if-gt v0, v3, :cond_d

    const/16 v3, -0x2000

    if-gt v3, v1, :cond_d

    const/16 v3, 0x2000

    if-gt v1, v3, :cond_d

    const/16 v3, -0x2000

    if-gt v3, v2, :cond_d

    const/16 v3, 0x2000

    if-gt v2, v3, :cond_d

    .line 1952
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "OK"

    aput-object v4, v3, v7

    .line 1955
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v8

    .line 1956
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v10

    .line 1957
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1954
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "NG"

    aput-object v4, v3, v7

    goto :goto_4

    .line 1958
    .end local v0    # "x":I
    .end local v1    # "y":I
    .end local v2    # "z":I
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "STMICRO_K303C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1960
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1961
    .restart local v0    # "x":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1962
    .restart local v1    # "y":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1963
    .restart local v2    # "z":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v9

    .line 1964
    const/16 v3, -0x4000

    if-gt v3, v0, :cond_f

    const/16 v3, 0x4000

    if-gt v0, v3, :cond_f

    const/16 v3, -0x4000

    if-gt v3, v1, :cond_f

    const/16 v3, 0x4000

    if-gt v1, v3, :cond_f

    const/16 v3, -0x4000

    if-gt v3, v2, :cond_f

    const/16 v3, 0x4000

    if-gt v2, v3, :cond_f

    .line 1966
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "OK"

    aput-object v4, v3, v7

    .line 1969
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v8

    .line 1970
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v10

    .line 1971
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1968
    :cond_f
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "NG"

    aput-object v4, v3, v7

    goto :goto_5

    .line 1973
    .end local v0    # "x":I
    .end local v1    # "y":I
    .end local v2    # "z":I
    :cond_10
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v9

    .line 1974
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v7

    .line 1975
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v8

    .line 1976
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v10

    .line 1977
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v10

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1981
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticADC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1984
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1987
    :cond_12
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private getMagneticDAC(I)[Ljava/lang/String;
    .locals 12
    .param p1, "target"    # I

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1734
    if-ne p1, v7, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v3, :cond_7

    .line 1735
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8973"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1746
    :cond_0
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1763
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 1765
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticDAC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v6, v6, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v8

    .line 1769
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v7

    .line 1770
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v9

    .line 1771
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v10

    .line 1772
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mDAC:[Ljava/lang/String;

    aget-object v4, v4, v10

    aput-object v4, v3, v11

    .line 1854
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticDAC"

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1857
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :goto_2
    return-object v3

    .line 1747
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS529"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS530C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "YAS532"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1755
    :cond_3
    new-instance v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1756
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1759
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v3, v11, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1775
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticDAC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1778
    const/4 v3, 0x0

    goto :goto_2

    .line 1780
    :cond_7
    if-ne p1, v9, :cond_11

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v3, :cond_11

    .line 1781
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v4, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1783
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v3, :cond_10

    .line 1785
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticDAC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8963C_MANAGER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK8975"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "AK09911C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1793
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v8

    .line 1794
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v7

    .line 1795
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    const-string v4, "OK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1796
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "1"

    aput-object v4, v3, v9

    .line 1800
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v10

    .line 1801
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1798
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v9

    goto :goto_3

    .line 1802
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD004A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD006A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1806
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1807
    .local v0, "x":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1808
    .local v1, "y":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1809
    .local v2, "z":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v8

    .line 1811
    const/16 v3, -0x7d0

    if-gt v3, v0, :cond_c

    const/16 v3, 0x7d0

    if-gt v0, v3, :cond_c

    const/16 v3, -0x7d0

    if-gt v3, v1, :cond_c

    const/16 v3, 0x7d0

    if-gt v1, v3, :cond_c

    const/16 v3, -0x7d0

    if-gt v3, v2, :cond_c

    const/16 v3, 0x7d0

    if-gt v2, v3, :cond_c

    .line 1813
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "OK"

    aput-object v4, v3, v7

    .line 1818
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v9

    .line 1819
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v10

    .line 1820
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1815
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "NG"

    aput-object v4, v3, v7

    goto :goto_4

    .line 1821
    .end local v0    # "x":I
    .end local v1    # "y":I
    .end local v2    # "z":I
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v4, "HSCDTD008A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1823
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1824
    .restart local v0    # "x":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v7

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1825
    .restart local v1    # "y":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1826
    .restart local v2    # "z":I
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v8

    .line 1827
    const/16 v3, -0x2000

    if-gt v3, v0, :cond_e

    const/16 v3, 0x2000

    if-gt v0, v3, :cond_e

    const/16 v3, -0x2000

    if-gt v3, v1, :cond_e

    const/16 v3, 0x2000

    if-gt v1, v3, :cond_e

    const/16 v3, -0x2000

    if-gt v3, v2, :cond_e

    const/16 v3, 0x2000

    if-gt v2, v3, :cond_e

    .line 1829
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "OK"

    aput-object v4, v3, v7

    .line 1832
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v9

    .line 1833
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v10

    .line 1834
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1831
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "NG"

    aput-object v4, v3, v7

    goto :goto_5

    .line 1836
    .end local v0    # "x":I
    .end local v1    # "y":I
    .end local v2    # "z":I
    :cond_f
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v4, "4"

    aput-object v4, v3, v8

    .line 1837
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v8

    aput-object v4, v3, v7

    .line 1838
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v7

    aput-object v4, v3, v9

    .line 1839
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v9

    aput-object v4, v3, v10

    .line 1840
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v4, v4, v10

    aput-object v4, v3, v11

    goto/16 :goto_1

    .line 1844
    :cond_10
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMagneticDAC"

    const-string v5, "null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1847
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 1850
    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private getMagneticOffsetH(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 2111
    if-ne p1, v4, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_4

    .line 2112
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2118
    :cond_0
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 2121
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2123
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticOffsetH"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2126
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v6

    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v1, v1, v6

    aput-object v1, v0, v4

    .line 2128
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v5

    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v7

    .line 2130
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mOffset_H:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v1

    .line 2145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticOffsetH"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_2
    :goto_0
    return-object v0

    .line 2133
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticOffsetH"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2138
    :cond_4
    if-ne p1, v5, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_2

    goto :goto_0
.end method

.method private getMagneticPowerOff(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1441
    if-ne p1, v5, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_8

    .line 1442
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8975"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1453
    :cond_0
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1470
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1472
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1476
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v5

    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOff:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    .line 1520
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOff"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_2
    :goto_2
    return-object v0

    .line 1454
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1462
    :cond_4
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1463
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1466
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v6, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1480
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticPowerOff"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1485
    :cond_8
    if-ne p1, v6, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_2

    .line 1486
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1488
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1490
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BMC150_COMBINATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1493
    const-string v0, "GEOMAGNETIC_SENSOR_POWER"

    const-string v1, "2"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1494
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const-string v1, "GEOMAGNETIC_SENSOR_POWER"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 1495
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1496
    const-string v0, "2"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1497
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "OK"

    aput-object v1, v0, v5

    .line 1498
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1500
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "NG"

    aput-object v1, v0, v5

    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "-1"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1504
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1510
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticPowerOff"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private getMagneticPowerOn(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1343
    if-ne p1, v4, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_b

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8975"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1355
    :cond_0
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1383
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1385
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1388
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 1389
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mPowerOn:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 1434
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOn"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_2
    :goto_2
    return-object v0

    .line 1356
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1364
    :cond_4
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1365
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD008A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1369
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1371
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BOSCH_BMC150"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1377
    :cond_8
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1378
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "STMICRO_K303C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1379
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_STMicro(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1393
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticPowerOn"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1398
    :cond_b
    if-ne p1, v6, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_2

    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1401
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 1403
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticPowerOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BMC150_COMBINATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1407
    const-string v0, "GEOMAGNETIC_SENSOR_POWER"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1408
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    const-string v1, "GEOMAGNETIC_SENSOR_POWER"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 1409
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 1410
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "OK"

    aput-object v1, v0, v4

    .line 1412
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1414
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "NG"

    aput-object v1, v0, v4

    .line 1415
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "-1"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1418
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v4

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1424
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticPowerOn"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private getMagneticSelf(I)[Ljava/lang/String;
    .locals 8
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2001
    if-ne p1, v4, :cond_c

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_c

    .line 2002
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8975"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2013
    :cond_0
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 2037
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 2039
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticSelf"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2043
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 2044
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 2045
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 2100
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticSelf"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_3
    :goto_2
    return-object v0

    .line 2014
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2022
    :cond_5
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 2023
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2026
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 2028
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BOSCH_BMC150"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2034
    :cond_9
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 2047
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v5

    .line 2048
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 2050
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v1, v1, v6

    aput-object v1, v0, v7

    .line 2051
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mSelf:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v1

    goto/16 :goto_1

    .line 2055
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticSelf"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2060
    :cond_c
    if-ne p1, v6, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_3

    .line 2061
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 2063
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_11

    .line 2064
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BOSCH_BMC150_POWER_NOISE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2066
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 2067
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 2068
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 2069
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BMC150_COMBINATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2070
    const-string v0, "GEOMAGNETIC_SENSOR_SELFTEST"

    const-string v1, "2"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2072
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2075
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 2076
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v5

    .line 2077
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 2078
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 2079
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "BMC150_NEWEST"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2080
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    goto/16 :goto_2

    .line 2082
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "4"

    aput-object v1, v0, v5

    .line 2083
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v4

    .line 2084
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 2085
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v6

    aput-object v1, v0, v7

    .line 2086
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v7

    aput-object v2, v0, v1

    goto/16 :goto_1

    .line 2090
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticSelf"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2073
    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method private getMagneticStatus(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1530
    if-ne p1, v5, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_8

    .line 1531
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8975"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8963C_MANAGER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK09911C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1542
    :cond_0
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1559
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1561
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticStatus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v5

    .line 1566
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mStatus:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    .line 1618
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticStatus"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_2
    :goto_2
    return-object v0

    .line 1543
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS530C"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS532"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1551
    :cond_4
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1552
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1555
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1569
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticStatus"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1574
    :cond_8
    if-ne p1, v6, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_2

    .line 1575
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1577
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 1579
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticStatus"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD004"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD004A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD006A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "HSCDTD008A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v1, "STMICRO_K303C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1586
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1588
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v0, v0, v4

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1589
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "OK"

    aput-object v1, v0, v5

    .line 1594
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1591
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "NG"

    aput-object v1, v0, v5

    goto :goto_3

    .line 1596
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1597
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v0, v0, v4

    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "1"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1602
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1608
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticStatus"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private getMagneticTemperature(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1629
    if-ne p1, v5, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_6

    .line 1630
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1635
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    .line 1664
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1666
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticTemperature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1670
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v5

    .line 1671
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;->mTemperature:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    .line 1724
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticTemperature"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_2
    return-object v0

    .line 1636
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1641
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    invoke-direct {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto :goto_0

    .line 1642
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD004A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "HSCDTD006A"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1645
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    invoke-virtual {v1, v6, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnMagneticExpansion_Alps(ILjava/lang/String;)Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Magnetic:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager$MagneticExpansionData;

    goto/16 :goto_0

    .line 1653
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1654
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "OK"

    aput-object v1, v0, v5

    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v6

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticTemperature"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_2

    .line 1674
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticTemperature"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1681
    :cond_6
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 1682
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "AK8973"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mFeature_Magnetic:Ljava/lang/String;

    const-string v2, "YAS529"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1684
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 1686
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 1688
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticTemperature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1691
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1692
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1693
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    goto/16 :goto_1

    .line 1696
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMagneticTemperature"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1707
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 1708
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 1709
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "0"

    aput-object v1, v0, v6

    .line 1712
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMagneticTemperature"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1715
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto/16 :goto_2
.end method

.method private getProximity(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 2155
    if-ne p1, v5, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_2

    .line 2156
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->returnProximity()[F

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    .line 2158
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    if-eqz v1, :cond_1

    .line 2160
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 2164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 2165
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_Float:[F

    aget v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    .line 2180
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximity"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    .line 2168
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getProximity"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2173
    :cond_2
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method private getProximityADC(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 2190
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 2218
    :cond_0
    :goto_0
    return-object v0

    .line 2192
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 2193
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 2195
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2197
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityADC"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 2201
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 2202
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 2215
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityADC"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2218
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 2205
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getProximityADC"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getProximityAVG(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 2225
    if-ne p1, v5, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 2253
    :cond_0
    :goto_0
    return-object v0

    .line 2227
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 2228
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 2230
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2232
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityAVG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2235
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v4

    .line 2236
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v5

    .line 2237
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v4

    aput-object v1, v0, v6

    .line 2250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityAVG"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2253
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 2240
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getProximityAVG"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getProximityOffset(I)[Ljava/lang/String;
    .locals 7
    .param p1, "target"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 2260
    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v1, :cond_1

    .line 2289
    :cond_0
    :goto_0
    return-object v0

    .line 2262
    :cond_1
    if-ne p1, v6, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v1, :cond_0

    .line 2263
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    sget v2, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->returnData(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    .line 2265
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityOffset"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2270
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "3"

    aput-object v1, v0, v5

    .line 2271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v4

    .line 2272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v1, v1, v5

    aput-object v1, v0, v6

    .line 2273
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mTemp_String:[Ljava/lang/String;

    aget-object v2, v2, v4

    aput-object v2, v0, v1

    .line 2286
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getProximityOffset"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->dataCheck([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2289
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mReturnData:[Ljava/lang/String;

    goto :goto_0

    .line 2276
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getProximityOffset"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getString_ID(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 2319
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v0, :cond_0

    .line 2320
    const-string v0, "ID_MANAGER_ACCELEROMETER"

    .line 2411
    :goto_0
    return-object v0

    .line 2321
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_1

    .line 2322
    const-string v0, "ID_MANAGER_ACCELEROMETER_N_ANGLE"

    goto :goto_0

    .line 2323
    :cond_1
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_2

    .line 2324
    const-string v0, "ID_MANAGER_ACCELEROMETER_SELF"

    goto :goto_0

    .line 2325
    :cond_2
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v0, :cond_3

    .line 2326
    const-string v0, "ID_MANAGER_BAROMETER"

    goto :goto_0

    .line 2327
    :cond_3
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p1, v0, :cond_4

    .line 2328
    const-string v0, "ID_MANAGER_GYRO"

    goto :goto_0

    .line 2329
    :cond_4
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_POWER:I

    if-ne p1, v0, :cond_5

    .line 2330
    const-string v0, "ID_MANAGER_GYRO_POWER"

    goto :goto_0

    .line 2331
    :cond_5
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p1, v0, :cond_6

    .line 2332
    const-string v0, "ID_MANAGER_GYRO_EXPANSION"

    goto :goto_0

    .line 2333
    :cond_6
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p1, v0, :cond_7

    .line 2334
    const-string v0, "ID_MANAGER_GYRO_SELF"

    goto :goto_0

    .line 2335
    :cond_7
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_8

    .line 2336
    const-string v0, "ID_MANAGER_GYRO_TEMPERATURE"

    goto :goto_0

    .line 2337
    :cond_8
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p1, v0, :cond_9

    .line 2338
    const-string v0, "ID_MANAGER_LIGHT"

    goto :goto_0

    .line 2339
    :cond_9
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p1, v0, :cond_a

    .line 2340
    const-string v0, "ID_MANAGER_MAGNETIC"

    goto :goto_0

    .line 2341
    :cond_a
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_b

    .line 2342
    const-string v0, "ID_MANAGER_MAGNETIC_POWER_ON"

    goto :goto_0

    .line 2343
    :cond_b
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_c

    .line 2344
    const-string v0, "ID_MANAGER_MAGNETIC_STATUS"

    goto :goto_0

    .line 2345
    :cond_c
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_d

    .line 2346
    const-string v0, "ID_MANAGER_MAGNETIC_TEMPERATURE"

    goto :goto_0

    .line 2347
    :cond_d
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p1, v0, :cond_e

    .line 2348
    const-string v0, "ID_MANAGER_MAGNETIC_DAC"

    goto :goto_0

    .line 2349
    :cond_e
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p1, v0, :cond_f

    .line 2350
    const-string v0, "ID_MANAGER_MAGNETIC_ADC"

    goto :goto_0

    .line 2351
    :cond_f
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p1, v0, :cond_10

    .line 2352
    const-string v0, "ID_MANAGER_MAGNETIC_SELF"

    goto :goto_0

    .line 2353
    :cond_10
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p1, v0, :cond_11

    .line 2354
    const-string v0, "ID_MANAGER_MAGNETIC_OFFSETH"

    goto :goto_0

    .line 2355
    :cond_11
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p1, v0, :cond_12

    .line 2356
    const-string v0, "ID_MANAGER_PROXIMITY"

    goto :goto_0

    .line 2359
    :cond_12
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne p1, v0, :cond_13

    .line 2360
    const-string v0, "ID_FILE____ACCELEROMETER"

    goto/16 :goto_0

    .line 2361
    :cond_13
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_14

    .line 2362
    const-string v0, "ID_FILE____ACCELEROMETER_N_ANGLE"

    goto/16 :goto_0

    .line 2363
    :cond_14
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_15

    .line 2364
    const-string v0, "ID_FILE____ACCELEROMETER_SELF"

    goto/16 :goto_0

    .line 2365
    :cond_15
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne p1, v0, :cond_16

    .line 2366
    const-string v0, "ID_FILE____ACCELEROMETER_CAL"

    goto/16 :goto_0

    .line 2367
    :cond_16
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne p1, v0, :cond_17

    .line 2368
    const-string v0, "ID_FILE____ACCELEROMETER_INTPIN"

    goto/16 :goto_0

    .line 2369
    :cond_17
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne p1, v0, :cond_18

    .line 2370
    const-string v0, "ID_FILE____BAROMETER_EEPROM"

    goto/16 :goto_0

    .line 2371
    :cond_18
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne p1, v0, :cond_19

    .line 2372
    const-string v0, "ID_FILE____GYRO_POWER"

    goto/16 :goto_0

    .line 2373
    :cond_19
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_1a

    .line 2374
    const-string v0, "ID_FILE____GYRO_TEMPERATURE"

    goto/16 :goto_0

    .line 2375
    :cond_1a
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne p1, v0, :cond_1b

    .line 2376
    const-string v0, "ID_FILE____GYRO_SELFTEST"

    goto/16 :goto_0

    .line 2377
    :cond_1b
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    if-ne p1, v0, :cond_1c

    .line 2378
    const-string v0, "ID_FILE____LIGHT"

    goto/16 :goto_0

    .line 2379
    :cond_1c
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne p1, v0, :cond_1d

    .line 2380
    const-string v0, "ID_FILE____LIGHT_ADC"

    goto/16 :goto_0

    .line 2381
    :cond_1d
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_1e

    .line 2382
    const-string v0, "ID_FILE____MAGNETIC_POWER_ON"

    goto/16 :goto_0

    .line 2383
    :cond_1e
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_1f

    .line 2384
    const-string v0, "ID_FILE____MAGNETIC_STATUS"

    goto/16 :goto_0

    .line 2385
    :cond_1f
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_20

    .line 2386
    const-string v0, "ID_FILE____MAGNETIC_TEMPERATURE"

    goto/16 :goto_0

    .line 2387
    :cond_20
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v0, :cond_21

    .line 2388
    const-string v0, "ID_FILE____MAGNETIC_DAC"

    goto/16 :goto_0

    .line 2389
    :cond_21
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v0, :cond_22

    .line 2390
    const-string v0, "ID_FILE____MAGNETIC_ADC"

    goto/16 :goto_0

    .line 2391
    :cond_22
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v0, :cond_23

    .line 2392
    const-string v0, "ID_FILE____MAGNETIC_SELF"

    goto/16 :goto_0

    .line 2393
    :cond_23
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne p1, v0, :cond_24

    .line 2394
    const-string v0, "ID_FILE____PROXIMITY_ADC"

    goto/16 :goto_0

    .line 2395
    :cond_24
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne p1, v0, :cond_25

    .line 2396
    const-string v0, "ID_FILE____PROXIMITY_AVG"

    goto/16 :goto_0

    .line 2397
    :cond_25
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne p1, v0, :cond_26

    .line 2398
    const-string v0, "ID_FILE____PROXIMITY_OFFSET"

    goto/16 :goto_0

    .line 2401
    :cond_26
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v0, :cond_27

    .line 2402
    const-string v0, "ID_INTENT__GRIP"

    goto/16 :goto_0

    .line 2405
    :cond_27
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne p1, v0, :cond_28

    .line 2406
    const-string v0, "ID_INTENT__CP_ACCELEROMETER"

    goto/16 :goto_0

    .line 2411
    :cond_28
    const-string v0, "Unknown"

    goto/16 :goto_0
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    .line 193
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    return-object v0
.end method


# virtual methods
.method public SensorOff()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->sensorOff()V

    .line 276
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "SensorOff"

    const-string v2, "Manager"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->sensorOff()V

    .line 282
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "SensorOff"

    const-string v2, "File"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "notiStop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mInterrupted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    .line 302
    :cond_2
    return-void
.end method

.method public SensorOff_Intent(I)V
    .locals 5
    .param p1, "sensorID"    # I

    .prologue
    const/4 v4, 0x0

    .line 306
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__COUNT:I

    if-gt p1, v0, :cond_1

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    if-eqz v0, :cond_1

    .line 308
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v0, :cond_3

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->sensorOff()V

    .line 314
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "SensorOff"

    const-string v2, "Intent"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 327
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "notiStop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mInterrupted : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    .line 335
    :cond_2
    return-void

    .line 310
    :cond_3
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne p1, v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->disableReceiver_CPsAccelerometer()V

    goto :goto_0
.end method

.method public SensorOn([I)V
    .locals 14
    .param p1, "sensorID"    # [I

    .prologue
    .line 199
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    new-array v4, v11, [I

    .line 200
    .local v4, "id_Manager":[I
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    new-array v2, v11, [I

    .line 201
    .local v2, "id_File":[I
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__COUNT:I

    new-array v3, v11, [I

    .line 202
    .local v3, "id_Intent":[I
    const/4 v9, 0x0

    .line 203
    .local v9, "index_Manager":I
    const/4 v5, 0x0

    .line 204
    .local v5, "index_File":I
    const/4 v7, 0x0

    .line 206
    .local v7, "index_Intent":I
    if-eqz p1, :cond_9

    .line 207
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v11, p1

    if-ge v0, v11, :cond_3

    .line 208
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    aget v12, p1, v0

    if-gt v11, v12, :cond_0

    aget v11, p1, v0

    sget v12, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    if-ge v11, v12, :cond_0

    .line 209
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "index_Manager":I
    .local v10, "index_Manager":I
    aget v11, p1, v0

    aput v11, v4, v9

    move v9, v10

    .line 207
    .end local v10    # "index_Manager":I
    .restart local v9    # "index_Manager":I
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    aget v12, p1, v0

    if-gt v11, v12, :cond_1

    aget v11, p1, v0

    sget v12, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sget v13, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    add-int/2addr v12, v13

    if-ge v11, v12, :cond_1

    .line 212
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index_File":I
    .local v6, "index_File":I
    aget v11, p1, v0

    aput v11, v2, v5

    move v5, v6

    .end local v6    # "index_File":I
    .restart local v5    # "index_File":I
    goto :goto_1

    .line 213
    :cond_1
    sget v11, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sget v12, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    add-int/2addr v11, v12

    aget v12, p1, v0

    if-gt v11, v12, :cond_2

    aget v11, p1, v0

    sget v12, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MAX:I

    if-gt v11, v12, :cond_2

    .line 215
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "index_Intent":I
    .local v8, "index_Intent":I
    aget v11, p1, v0

    aput v11, v3, v7

    move v7, v8

    .end local v8    # "index_Intent":I
    .restart local v7    # "index_Intent":I
    goto :goto_1

    .line 217
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "SensorOn"

    const-string v13, "ID : Unknown"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 221
    :cond_3
    if-lez v9, :cond_5

    .line 222
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "SensorOn"

    const-string v13, "Manager"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    new-array v1, v9, [I

    .line 225
    .local v1, "id":[I
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v9, :cond_4

    .line 226
    aget v11, v4, v0

    aput v11, v1, v0

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 229
    :cond_4
    new-instance v11, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    iget-object v12, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-direct {v11, v1, v12}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;-><init>([ILandroid/hardware/SensorManager;)V

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    .line 232
    .end local v1    # "id":[I
    :cond_5
    if-lez v5, :cond_7

    .line 233
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "SensorOn"

    const-string v13, "File"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    new-array v1, v5, [I

    .line 236
    .restart local v1    # "id":[I
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v5, :cond_6

    .line 237
    aget v11, v2, v0

    aput v11, v1, v0

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 240
    :cond_6
    new-instance v11, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    invoke-direct {v11, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;-><init>([I)V

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    .line 243
    .end local v1    # "id":[I
    :cond_7
    if-lez v7, :cond_9

    .line 244
    iget-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "SensorOn"

    const-string v13, "Intent"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-array v1, v7, [I

    .line 247
    .restart local v1    # "id":[I
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v7, :cond_8

    .line 248
    aget v11, v3, v0

    aput v11, v1, v0

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 251
    :cond_8
    new-instance v11, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    sget-object v12, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mContext:Landroid/content/Context;

    invoke-direct {v11, v12, v1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;-><init>(Landroid/content/Context;[I)V

    iput-object v11, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    .line 254
    .end local v0    # "i":I
    .end local v1    # "id":[I
    :cond_9
    return-void
.end method

.method public SensorOn([ILandroid/os/Handler;I)V
    .locals 2
    .param p1, "sensorID"    # [I
    .param p2, "notiHandler"    # Landroid/os/Handler;
    .param p3, "loopDelay_millisecond"    # I

    .prologue
    .line 257
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->SensorOn([I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->startLoop(I)V

    .line 263
    :cond_0
    if-eqz p1, :cond_1

    .line 264
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;-><init>(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->setLoopDelay(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->setHandler(Landroid/os/Handler;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->setDaemon(Z)V

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mNoti:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->start()V

    .line 271
    :cond_1
    return-void
.end method

.method public getData(I)[Ljava/lang/String;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getString_ID(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER:I

    if-ne p1, v0, :cond_0

    .line 396
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    .line 500
    :goto_0
    return-object v0

    .line 397
    :cond_0
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER:I

    if-ne p1, v0, :cond_1

    .line 398
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZ(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :cond_1
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_2

    .line 400
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 401
    :cond_2
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_N_ANGLE:I

    if-ne p1, v0, :cond_3

    .line 402
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 403
    :cond_3
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_4

    .line 404
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 405
    :cond_4
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_SELF:I

    if-ne p1, v0, :cond_5

    .line 406
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 407
    :cond_5
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_CAL:I

    if-ne p1, v0, :cond_6

    .line 408
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterCal(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 409
    :cond_6
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____ACCELEROMETER_INTPIN:I

    if-ne p1, v0, :cond_7

    .line 410
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterIntpin(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 413
    :cond_7
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__CP_ACCELEROMETER:I

    if-ne p1, v0, :cond_8

    .line 414
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getAccelermeterXYZnAngle(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 418
    :cond_8
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_BAROMETER:I

    if-ne p1, v0, :cond_9

    .line 419
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getBarometer(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 420
    :cond_9
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____BAROMETER_EEPROM:I

    if-ne p1, v0, :cond_a

    .line 421
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getBarometerEEPROM(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 424
    :cond_a
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_INTENT__GRIP:I

    if-ne p1, v0, :cond_b

    .line 425
    invoke-direct {p0, v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGrip(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 428
    :cond_b
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO:I

    if-ne p1, v0, :cond_c

    .line 429
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyro(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 430
    :cond_c
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_POWER:I

    if-ne p1, v0, :cond_d

    .line 431
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroPower(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 432
    :cond_d
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_EXPANSION:I

    if-ne p1, v0, :cond_e

    .line 433
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroExpansion(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 434
    :cond_e
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_f

    .line 435
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 436
    :cond_f
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_TEMPERATURE:I

    if-ne p1, v0, :cond_10

    .line 437
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 438
    :cond_10
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_GYRO_SELF:I

    if-ne p1, v0, :cond_11

    .line 439
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 440
    :cond_11
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____GYRO_SELFTEST:I

    if-ne p1, v0, :cond_12

    .line 441
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getGyroSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 444
    :cond_12
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_LIGHT:I

    if-ne p1, v0, :cond_13

    .line 445
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getLight(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 446
    :cond_13
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT:I

    if-ne p1, v0, :cond_14

    .line 447
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getLight(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 448
    :cond_14
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_CCT:I

    if-ne p1, v0, :cond_15

    .line 449
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getLightCCT(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 450
    :cond_15
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____LIGHT_ADC:I

    if-ne p1, v0, :cond_16

    .line 451
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getLightADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 454
    :cond_16
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC:I

    if-ne p1, v0, :cond_17

    .line 455
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagnetic(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 456
    :cond_17
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_18

    .line 457
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticPowerOn(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 458
    :cond_18
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_ON:I

    if-ne p1, v0, :cond_19

    .line 459
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticPowerOn(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 460
    :cond_19
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_POWER_OFF:I

    if-ne p1, v0, :cond_1a

    .line 461
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticPowerOff(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 462
    :cond_1a
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_POWER_OFF:I

    if-ne p1, v0, :cond_1b

    .line 463
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticPowerOff(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 464
    :cond_1b
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_1c

    .line 465
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticStatus(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 466
    :cond_1c
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_STATUS:I

    if-ne p1, v0, :cond_1d

    .line 467
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticStatus(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 468
    :cond_1d
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_1e

    .line 469
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 470
    :cond_1e
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_TEMPERATURE:I

    if-ne p1, v0, :cond_1f

    .line 471
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticTemperature(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 472
    :cond_1f
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_DAC:I

    if-ne p1, v0, :cond_20

    .line 473
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticDAC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 474
    :cond_20
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_DAC:I

    if-ne p1, v0, :cond_21

    .line 475
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticDAC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 476
    :cond_21
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_ADC:I

    if-ne p1, v0, :cond_22

    .line 477
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 478
    :cond_22
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_ADC:I

    if-ne p1, v0, :cond_23

    .line 479
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 480
    :cond_23
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_SELF:I

    if-ne p1, v0, :cond_24

    .line 481
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 482
    :cond_24
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____MAGNETIC_SELF:I

    if-ne p1, v0, :cond_25

    .line 483
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticSelf(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 484
    :cond_25
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_MAGNETIC_OFFSETH:I

    if-ne p1, v0, :cond_26

    .line 485
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getMagneticOffsetH(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 488
    :cond_26
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_PROXIMITY:I

    if-ne p1, v0, :cond_27

    .line 489
    invoke-direct {p0, v5}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getProximity(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 490
    :cond_27
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_ADC:I

    if-ne p1, v0, :cond_28

    .line 491
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getProximityADC(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 492
    :cond_28
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_AVG:I

    if-ne p1, v0, :cond_29

    .line 493
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getProximityAVG(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 494
    :cond_29
    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____PROXIMITY_OFFSET:I

    if-ne p1, v0, :cond_2a

    .line 495
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->getProximityOffset(I)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 499
    :cond_2a
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getData"

    const-string v2, "id : Unknown"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public isSensorOn(I)Z
    .locals 3
    .param p1, "sensorID"    # I

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MIN:I

    if-gt v0, p1, :cond_0

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    if-ge p1, v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadManager:Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadManager;->isSensorOn(I)Z

    move-result v0

    .line 350
    :goto_0
    return v0

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    if-eqz v0, :cond_1

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    if-gt v0, p1, :cond_1

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadFile:Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadFile;->isSensorOn(I)Z

    move-result v0

    goto :goto_0

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    if-eqz v0, :cond_2

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_MANAGER_COUNT:I

    sget v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_FILE____COUNT:I

    add-int/2addr v0, v1

    if-gt v0, p1, :cond_2

    sget v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->ID_SCOPE_MAX:I

    if-gt p1, v0, :cond_2

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mSensorReadIntent:Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/hwmoduletest/modules/SensorReadIntent;->isSensorOn(I)Z

    move-result v0

    goto :goto_0

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "SensorOn"

    const-string v2, "null / ID unknown"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const/4 v0, 0x0

    goto :goto_0
.end method
