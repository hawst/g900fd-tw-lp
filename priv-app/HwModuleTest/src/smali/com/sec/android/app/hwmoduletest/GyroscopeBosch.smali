.class public Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeBosch.java"


# instance fields
.field private mGyroSelfTestLayout:Landroid/widget/TableLayout;

.field private mHandler:Landroid/os/Handler;

.field private mPass:Z

.field private mSelfTestResults:[Ljava/lang/String;

.field private mSubType:Ljava/lang/String;

.field private txt_bist:Landroid/widget/TextView;

.field private txt_build_in_selftest:Landroid/widget/TextView;

.field private txt_zerox:Landroid/widget/TextView;

.field private txt_zeroy:Landroid/widget/TextView;

.field private txt_zeroz:Landroid/widget/TextView;

.field private txtresult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "GyroscopeBosch"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mPass:Z

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mHandler:Landroid/os/Handler;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Landroid/widget/TableLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSubType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSelfTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->showTestResults_BMG160([Ljava/lang/String;)V

    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 42
    const v0, 0x7f0b00ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_build_in_selftest:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0b00c9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_bist:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0b00d0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zerox:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0b00d1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zeroy:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f0b00d2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zeroz:Landroid/widget/TextView;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "subtype"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSubType:Ljava/lang/String;

    .line 49
    return-void
.end method

.method private showTestResults_BMG160([Ljava/lang/String;)V
    .locals 10
    .param p1, "results"    # [Ljava/lang/String;

    .prologue
    const v9, 0x7f08007b

    const v8, 0x7f08007a

    const/high16 v7, -0x10000

    const v6, -0xffff01

    .line 80
    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "result":Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "bist":Ljava/lang/String;
    const/4 v5, 0x2

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "zerorate_x":Ljava/lang/String;
    const/4 v5, 0x3

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "zerorate_y":Ljava/lang/String;
    const/4 v5, 0x4

    aget-object v5, p1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "zerorate_z":Ljava/lang/String;
    const-string v5, "1"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 87
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_bist:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    .line 88
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_bist:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zerox:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zeroy:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_zeroz:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    const-string v5, "1"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mPass:Z

    .line 100
    iget-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mPass:Z

    if-eqz v5, :cond_1

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(I)V

    .line 102
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 108
    :goto_1
    return-void

    .line 90
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_bist:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(I)V

    .line 91
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_bist:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 104
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(I)V

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->setContentView(I)V

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->init()V

    .line 39
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 52
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 53
    const v1, 0x7f0b00c7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableLayout;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    invoke-virtual {v1, v2}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const-string v1, "GYRO_SENSOR_SELFTEST"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "selfTestResult":Ljava/lang/String;
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mSelfTestResults:[Ljava/lang/String;

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txtresult:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->txt_build_in_selftest:Landroid/widget/TextView;

    const-string v2, "(Build in selftest)"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeBosch$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeBosch;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 74
    return-void
.end method
