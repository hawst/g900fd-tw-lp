.class public Lcom/sec/android/app/hwmoduletest/view/Circle;
.super Landroid/view/View;
.source "Circle.java"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private radius:F

.field private x:F

.field private y:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/Circle;->init(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/Circle;->init(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/Circle;->init(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->mPaint:Landroid/graphics/Paint;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->x:F

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->y:F

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->radius:F

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 53
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/Circle;->invalidate()V

    .line 49
    return-void
.end method

.method public setPoint(FF)V
    .locals 0
    .param p1, "px"    # F
    .param p2, "py"    # F

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->x:F

    .line 39
    iput p2, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->y:F

    .line 40
    return-void
.end method

.method public setRadius(F)V
    .locals 0
    .param p1, "pradius"    # F

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/view/Circle;->radius:F

    .line 44
    return-void
.end method
