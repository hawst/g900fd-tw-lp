.class public Lcom/sec/android/app/hwmoduletest/support/Support$Feature;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Feature"
.end annotation


# static fields
.field public static final DC_MOTER:Ljava/lang/String; = "DC_MOTER"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "DEVICE_TYPE"

.field public static final FACTORY_TEST_APO:Ljava/lang/String; = "FACTORY_TEST_APO"

.field public static final GRIPSENSOR_TYPE:Ljava/lang/String; = "GRIPSENSOR_TYPE"

.field public static final INBATT_SAVE_SOC:Ljava/lang/String; = "INBATT_SAVE_SOC"

.field public static final IRLED_CONCEPT:Ljava/lang/String; = "IRLED_CONCEPT"

.field public static final IRLED_REPEAT_MODE:Ljava/lang/String; = "IRLED_REPEAT_MODE"

.field public static final IS_AUTOBRIGHTNESS:Ljava/lang/String; = "IS_AUTOBRIGHTNESS"

.field public static final IS_DBLC_FUNCTION:Ljava/lang/String; = "IS_DBLC_FUNCTION"

.field public static final IS_DUALSTANDBY:Ljava/lang/String; = "IS_DUALSTANDBY"

.field public static final IS_DUALTD:Ljava/lang/String; = "IS_DUALTD"

.field public static final IS_DUMMYKEY_FACTORY_MODE:Ljava/lang/String; = "IS_DUMMYKEY_FACTORY_MODE"

.field public static final IS_ESCMODEM:Ljava/lang/String; = "IS_ESCMODEM"

.field public static final IS_LIGHT_SENSOR_LEVEL5:Ljava/lang/String; = "IS_LIGHT_SENSOR_LEVEL5"

.field public static final IS_PREVAIL2SPR:Ljava/lang/String; = "IS_PREVAIL2SPR"

.field public static final IS_SUPPORT_SENSOR_HUB_THERMS:Ljava/lang/String; = "IS_SUPPORT_SENSOR_HUB_THERMS"

.field public static final IS_TOUCHKEY_GRAPH:Ljava/lang/String; = "IS_TOUCHKEY_GRAPH"

.field public static final LCD_TYPE:Ljava/lang/String; = "LCD_TYPE"

.field public static final MAGNETIC_ROTATE_DEGREE:Ljava/lang/String; = "MAGNETIC_ROTATE_DEGREE"

.field public static final MODEL_COMMUNICATION_MODE:Ljava/lang/String; = "MODEL_COMMUNICATION_MODE"

.field public static final MODEL_NAME:Ljava/lang/String; = "MODEL_NAME"

.field public static final PANEL_TYPE:Ljava/lang/String; = "PANEL_TYPE"

.field public static final SELFTEST_MENU_LIST:Ljava/lang/String; = "SELFTEST_MENU_LIST"

.field public static final SENSOR_NAME_ACCELEROMETER:Ljava/lang/String; = "SENSOR_NAME_ACCELEROMETER"

.field public static final SENSOR_NAME_GESTURE:Ljava/lang/String; = "SENSOR_NAME_GESTURE"

.field public static final SENSOR_NAME_GYROSCOPE:Ljava/lang/String; = "SENSOR_NAME_GYROSCOPE"

.field public static final SENSOR_NAME_MAGNETIC:Ljava/lang/String; = "SENSOR_NAME_MAGNETIC"

.field public static final SENSOR_VENDOR_ECG:Ljava/lang/String; = "SENSOR_VENDOR_ECG"

.field public static final SPEN_HOVERING_DEL_LEFTTOP_RIGHTDOWN:Ljava/lang/String; = "SPEN_HOVERING_DEL_LEFTTOP_RIGHTDOWN"

.field public static final SUBKEY_FIVE_KEY:Ljava/lang/String; = "SUBKEY_FIVE_KEY"

.field public static final SUBKEY_FOUR_KEY:Ljava/lang/String; = "SUBKEY_FOUR_KEY"

.field public static final SUBKEY_SIX_KEY:Ljava/lang/String; = "SUBKEY_SIX_KEY"

.field public static final SUPPORT_BOOK_COVER:Ljava/lang/String; = "SUPPORT_BOOK_COVER"

.field public static final SUPPORT_DUAL_LCD_FOLDER:Ljava/lang/String; = "SUPPORT_DUAL_LCD_FOLDER"

.field public static final SUPPORT_DUAL_MOTOR:Ljava/lang/String; = "SUPPORT_DUAL_MOTOR"

.field public static final SUPPORT_EPEN:Ljava/lang/String; = "SUPPORT_EPEN"

.field public static final SUPPORT_GRIPSENSOR_CAL_UI:Ljava/lang/String; = "SUPPORT_GRIPSENSOR_CAL_UI"

.field public static final SUPPORT_HRM_SPO2:Ljava/lang/String; = "SUPPORT_HRM_SPO2"

.field public static final TAG:Ljava/lang/String; = "Feature"

.field public static final THRETHOLD_CHECK:Ljava/lang/String; = "THRETHOLD_CHECK"

.field public static final TWOSPEAKER:Ljava/lang/String; = "TWOSPEAKER"

.field public static final WAY_PATH_TSK_UPDATE:Ljava/lang/String; = "WAY_PATH_TSK_UPDATE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 269
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 273
    const-string v0, "value"

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z
    invoke-static {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$400(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getByte(Ljava/lang/String;)B
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 277
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getByte(Ljava/lang/String;Ljava/lang/String;)B
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$500(Ljava/lang/String;Ljava/lang/String;)B

    move-result v0

    return v0
.end method

.method public static getDouble(Ljava/lang/String;)D
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 293
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getDouble(Ljava/lang/String;Ljava/lang/String;)D
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$700(Ljava/lang/String;Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static getFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 289
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getFloat(Ljava/lang/String;Ljava/lang/String;)F
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$200(Ljava/lang/String;Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public static getInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 285
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getInt(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$600(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
