.class Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;
.super Landroid/widget/ArrayAdapter;
.source "GyroscopeDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryAdaptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 208
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 212
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;

    .line 215
    .local v4, "item":Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;
    if-nez p2, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 217
    .local v7, "li":Landroid/view/LayoutInflater;
    const v12, 0x7f030034

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v7, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 222
    .end local v7    # "li":Landroid/view/LayoutInflater;
    .local v6, "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->getCount()Ljava/lang/String;

    move-result-object v1

    .line 223
    .local v1, "countString":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->getValueX()Ljava/lang/String;

    move-result-object v9

    .line 224
    .local v9, "xVal":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->getValueY()Ljava/lang/String;

    move-result-object v10

    .line 225
    .local v10, "yVal":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;->getValueZ()Ljava/lang/String;

    move-result-object v11

    .line 226
    .local v11, "zVal":Ljava/lang/String;
    const v12, 0x7f0b00d3

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 227
    .local v2, "countView":Landroid/widget/TextView;
    const v12, 0x7f0b00d4

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 228
    .local v3, "idView":Landroid/widget/TextView;
    const v12, 0x7f0b00d5

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 229
    .local v5, "itemView":Landroid/widget/TextView;
    const v12, 0x7f0b00d6

    invoke-virtual {v6, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 230
    .local v8, "resultView":Landroid/widget/TextView;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    const/high16 v12, -0x10000

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 235
    const v12, -0xff0100

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 236
    const v12, -0xffff01

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 237
    return-object v6

    .end local v1    # "countString":Ljava/lang/String;
    .end local v2    # "countView":Landroid/widget/TextView;
    .end local v3    # "idView":Landroid/widget/TextView;
    .end local v5    # "itemView":Landroid/widget/TextView;
    .end local v6    # "layout":Landroid/widget/LinearLayout;
    .end local v8    # "resultView":Landroid/widget/TextView;
    .end local v9    # "xVal":Ljava/lang/String;
    .end local v10    # "yVal":Ljava/lang/String;
    .end local v11    # "zVal":Ljava/lang/String;
    :cond_0
    move-object/from16 v6, p2

    .line 219
    check-cast v6, Landroid/widget/LinearLayout;

    .restart local v6    # "layout":Landroid/widget/LinearLayout;
    goto :goto_0
.end method
