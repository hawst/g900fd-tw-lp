.class Lcom/sec/android/app/hwmoduletest/GestureSensor$5;
.super Ljava/lang/Object;
.source "GestureSensor.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GestureSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureSensor;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private GetGesturedata([F)V
    .locals 2
    .param p1, "values"    # [F

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    const/4 v1, 0x0

    aget v1, p1, v1

    # invokes: Lcom/sec/android/app/hwmoduletest/GestureSensor;->DirectionSetText(F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1600(Lcom/sec/android/app/hwmoduletest/GestureSensor;F)V

    .line 350
    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 309
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->GetGesturedata([F)V

    .line 311
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42880000    # 68.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1100(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1102(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 346
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkBlue:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1102(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42aa0000    # 85.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1300(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1302(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkGreen:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1302(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    goto :goto_0

    .line 327
    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42a40000    # 82.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1400(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1402(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 332
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkRed:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1402(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    goto :goto_0

    .line 335
    :cond_5
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42980000    # 76.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1500(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1502(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 340
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # setter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->checkYellow:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1502(Lcom/sec/android/app/hwmoduletest/GestureSensor;Z)Z

    goto/16 :goto_0

    .line 344
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureSensor$5;->this$0:Lcom/sec/android/app/hwmoduletest/GestureSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureSensor;->access$1200(Lcom/sec/android/app/hwmoduletest/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method
