.class public Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Kernel"
.end annotation


# static fields
.field public static final ACCEL_SENSOR_CAL:Ljava/lang/String; = "ACCEL_SENSOR_CAL"

.field public static final ACCEL_SENSOR_INTPIN:Ljava/lang/String; = "ACCEL_SENSOR_INTPIN"

.field public static final ACCEL_SENSOR_RAW:Ljava/lang/String; = "ACCEL_SENSOR_RAW"

.field public static final APCHIP_TEMP_ADC:Ljava/lang/String; = "APCHIP_TEMP_ADC"

.field public static final BARCODE_EMUL_FIRMWARE_VERSION:Ljava/lang/String; = "BARCODE_EMUL_FIRMWARE_VERSION"

.field public static final BAROMETE_VENDOR:Ljava/lang/String; = "BAROMETE_VENDOR"

.field public static final BAROME_EEPROM:Ljava/lang/String; = "BAROME_EEPROM"

.field public static final BATTERY_AUTH:Ljava/lang/String; = "BATTERY_AUTH"

.field public static final BATTERY_CAPACITY:Ljava/lang/String; = "BATTERY_CAPACITY"

.field public static final BATTERY_TEMP:Ljava/lang/String; = "BATTERY_TEMP"

.field public static final BATTERY_TEMP_ADC:Ljava/lang/String; = "BATTERY_TEMP_ADC"

.field public static final BATTERY_TEMP_ADC_AVERAGE:Ljava/lang/String; = "BATTERY_TEMP_ADC_AVERAGE"

.field public static final BATTERY_TEMP_ADC_CAL:Ljava/lang/String; = "BATTERY_TEMP_ADC_CAL"

.field public static final BATTERY_TEMP_AVERAGE:Ljava/lang/String; = "BATTERY_TEMP_AVERAGE"

.field public static final BATTERY_TEMP_VF_ADC:Ljava/lang/String; = "BATTERY_TEMP_VF_ADC"

.field public static final BATTERY_TEMP_VF_OCV:Ljava/lang/String; = "BATTERY_TEMP_VF_OCV"

.field public static final BATTERY_TEST_MODE:Ljava/lang/String; = "BATTERY_TEST_MODE"

.field public static final BATTERY_UPDATE_BEFORE_READ:Ljava/lang/String; = "BATTERY_UPDATE_BEFORE_READ"

.field public static final BATTERY_VOLT:Ljava/lang/String; = "BATTERY_VOLT"

.field public static final BATTERY_VOLT_ADC:Ljava/lang/String; = "BATTERY_VOLT_ADC"

.field public static final BATTERY_VOLT_ADC_AVERAGE:Ljava/lang/String; = "BATTERY_VOLT_ADC_AVERAGE"

.field public static final BATTERY_VOLT_ADC_CAL:Ljava/lang/String; = "BATTERY_VOLT_ADC_CAL"

.field public static final BATTERY_VOLT_AVERAGE:Ljava/lang/String; = "BATTERY_VOLT_AVERAGE"

.field public static final BROME_SENSOR_SEAR_LEVEL_PRESSURE:Ljava/lang/String; = "BROME_SENSOR_SEAR_LEVEL_PRESSURE"

.field public static final CHG_CURRENT_ADC:Ljava/lang/String; = "CHG_CURRENT_ADC"

.field public static final ECG_SENSOR_FW_VERSION:Ljava/lang/String; = "ECG_SENSOR_FW_VERSION"

.field public static final EPEN_FIRMWARE_UPDATE:Ljava/lang/String; = "EPEN_FIRMWARE_UPDATE"

.field public static final EPEN_FIRMWARE_UPDATE_STATUS:Ljava/lang/String; = "EPEN_FIRMWARE_UPDATE_STATUS"

.field public static final EPEN_FIRMWARE_VERSION:Ljava/lang/String; = "EPEN_FIRMWARE_VERSION"

.field public static final EPEN_TUNING_VERSION:Ljava/lang/String; = "EPEN_TUNING_VERSION"

.field public static final EXTERNAL_MEMORY_INSERTED:Ljava/lang/String; = "EXTERNAL_MEMORY_INSERTED"

.field public static final EXTRA_BUTTON_EVENT:Ljava/lang/String; = "EXTRA_BUTTON_EVENT"

.field public static final FACTORY_MODE:Ljava/lang/String; = "FACTORY_MODE"

.field public static final FUEL_GAUGE_RESET:Ljava/lang/String; = "FUEL_GAUGE_RESET"

.field public static final GEOMAGNETIC_SENSOR_ADC:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_ADC"

.field public static final GEOMAGNETIC_SENSOR_DAC:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_DAC"

.field public static final GEOMAGNETIC_SENSOR_POWER:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_POWER"

.field public static final GEOMAGNETIC_SENSOR_SELFTEST:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_SELFTEST"

.field public static final GEOMAGNETIC_SENSOR_STATUS:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_STATUS"

.field public static final GEOMAGNETIC_SENSOR_TEMP:Ljava/lang/String; = "GEOMAGNETIC_SENSOR_TEMP"

.field public static final GESTURE_SENSOR_VENDOR:Ljava/lang/String; = "GESTURE_SENSOR_VENDOR"

.field public static final GYRO_OIS_POWER:Ljava/lang/String; = "GYRO_OIS_POWER"

.field public static final GYRO_OIS_RAWDATA:Ljava/lang/String; = "GYRO_OIS_RAWDATA"

.field public static final GYRO_OIS_SELFTEST:Ljava/lang/String; = "GYRO_OIS_SELFTEST"

.field public static final GYRO_SENSOR_DPS_SELECT:Ljava/lang/String; = "GYRO_SENSOR_DPS_SELECT"

.field public static final GYRO_SENSOR_NAME:Ljava/lang/String; = "GYRO_SENSOR_NAME"

.field public static final GYRO_SENSOR_POWER_ON:Ljava/lang/String; = "GYRO_SENSOR_POWER_ON"

.field public static final GYRO_SENSOR_SELFTEST:Ljava/lang/String; = "GYRO_SENSOR_SELFTEST"

.field public static final GYRO_SENSOR_TEMP:Ljava/lang/String; = "GYRO_SENSOR_TEMP"

.field public static final HRM_DEVICE_ID:Ljava/lang/String; = "HRM_DEVICE_ID"

.field public static final HRM_EOL_TEST:Ljava/lang/String; = "HRM_EOL_TEST"

.field public static final HRM_EOL_TEST_RESULT:Ljava/lang/String; = "HRM_EOL_TEST_RESULT"

.field public static final HRM_EOL_TEST_STATUS:Ljava/lang/String; = "HRM_EOL_TEST_STATUS"

.field public static final HRM_HR_RANGE:Ljava/lang/String; = "HRM_HR_RANGE"

.field public static final HRM_HR_RANGE2:Ljava/lang/String; = "HRM_HR_RANGE2"

.field public static final HRM_IDAC_RANGE:Ljava/lang/String; = "HRM_IDAC_RANGE"

.field public static final HRM_LED_CURRENT:Ljava/lang/String; = "HRM_LED_CURRENT"

.field public static final HRM_LED_CURRENT2:Ljava/lang/String; = "HRM_LED_CURRENT2"

.field public static final HRM_LIBRARY_ELF_VER:Ljava/lang/String; = "HRM_LIBRARY_ELF_VER"

.field public static final HRM_LIBRARY_EOL_VER:Ljava/lang/String; = "HRM_LIBRARY_EOL_VER"

.field public static final HRM_LIBRARY_VER:Ljava/lang/String; = "HRM_LIBRARY_VER"

.field public static final HRM_MODE_IR:Ljava/lang/String; = "HRM_MODE_IR"

.field public static final HRM_MODE_R:Ljava/lang/String; = "HRM_MODE_R"

.field public static final HRM_SENSOR_NAME:Ljava/lang/String; = "HRM_SENSOR_NAME"

.field public static final HRM_VENDOR:Ljava/lang/String; = "HRM_VENDOR"

.field public static final HUMITEMP_THERMISTER_BATT_CELCIUS:Ljava/lang/String; = "HUMITEMP_THERMISTER_BATT_CELCIUS"

.field public static final HUMITEMP_THERMISTER_PAM:Ljava/lang/String; = "HUMITEMP_THERMISTER_PAM"

.field public static final HUMITEMP_THERMISTER_S_HUB_BATT_CELCIUS:Ljava/lang/String; = "HUMITEMP_THERMISTER_S_HUB_BATT_CELCIUS"

.field public static final IR_LED_RESULT_TX:Ljava/lang/String; = "IR_LED_RESULT_TX"

.field public static final IR_LED_SEND:Ljava/lang/String; = "IR_LED_SEND"

.field public static final IR_LED_SEND_TEST:Ljava/lang/String; = "IR_LED_SEND_TEST"

.field public static final IR_THERMOMETER_SENSOR_VENDOR:Ljava/lang/String; = "IR_THERMOMETER_SENSOR_VENDOR"

.field public static final JIG_CONNECTION_CHECK:Ljava/lang/String; = "JIG_CONNECTION_CHECK"

.field public static final LCD_AUTO_BRIGHTNESS:Ljava/lang/String; = "LCD_AUTO_BRIGHTNESS"

.field public static final LED_BLINK:Ljava/lang/String; = "LED_BLINK"

.field public static final LED_BLUE:Ljava/lang/String; = "LED_BLUE"

.field public static final LED_GREEN:Ljava/lang/String; = "LED_GREEN"

.field public static final LED_LOWPOWER:Ljava/lang/String; = "LED_LOWPOWER"

.field public static final LED_RED:Ljava/lang/String; = "LED_RED"

.field public static final LIGHT_SENSOR_ADC:Ljava/lang/String; = "LIGHT_SENSOR_ADC"

.field public static final LIGHT_SENSOR_LUX:Ljava/lang/String; = "LIGHT_SENSOR_LUX"

.field public static final LIGHT_SENSOR_NAME:Ljava/lang/String; = "LIGHT_SENSOR_NAME"

.field public static final LIGHT_SENSOR_RAW:Ljava/lang/String; = "LIGHT_SENSOR_RAW"

.field public static final LOG_LEVEL_NEVER_SHOW:I = 0x0

.field public static final MAGNT_SENSOR_RAW:Ljava/lang/String; = "MAGNT_SENSOR_RAW"

.field public static final MOTOR_LPF:Ljava/lang/String; = "MOTOR_LPF"

.field public static final PATH_BACKLIGHT_ON_FOR_GREEN_LCD:Ljava/lang/String; = "PATH_BACKLIGHT_ON_FOR_GREEN_LCD"

.field public static final PATH_HALLIC_STATE:Ljava/lang/String; = "PATH_HALLIC_STATE"

.field public static final PATH_HALLIC_STATE2:Ljava/lang/String; = "PATH_HALLIC_STATE2"

.field public static final PROXI_SENSOR_ADC:Ljava/lang/String; = "PROXI_SENSOR_ADC"

.field public static final PROXI_SENSOR_ADC_AVG:Ljava/lang/String; = "PROXI_SENSOR_ADC_AVG"

.field public static final PROXI_SENSOR_DEFAULT_TRIM:Ljava/lang/String; = "PROXI_SENSOR_DEFAULT_TRIM"

.field public static final PROXI_SENSOR_NAME:Ljava/lang/String; = "PROXI_SENSOR_NAME"

.field public static final PROXI_SENSOR_OFFSET:Ljava/lang/String; = "PROXI_SENSOR_OFFSET"

.field public static final PROXI_SENSOR_OFFSET_PASS:Ljava/lang/String; = "PROXI_SENSOR_OFFSET_PASS"

.field public static final PROXI_SENSOR_SET_HIGH_THRESHOLD:Ljava/lang/String; = "PROXI_SENSOR_SET_HIGH_THRESHOLD"

.field public static final PROXI_SENSOR_SET_LOW_THRESHOLD:Ljava/lang/String; = "PROXI_SENSOR_SET_LOW_THRESHOLD"

.field public static final RAM_VERSION:Ljava/lang/String; = "RAM_VERSION"

.field public static final SEC_EXT_THERMISTER_ADC:Ljava/lang/String; = "SEC_EXT_THERMISTER_ADC"

.field public static final SEC_EXT_THERMISTER_TEMP:Ljava/lang/String; = "SEC_EXT_THERMISTER_TEMP"

.field public static final SENSORHUB_CRASHED_FIRMWARE_UPDATE:Ljava/lang/String; = "SENSORHUB_CRASHED_FIRMWARE_UPDATE"

.field public static final SENSORHUB_FIRMWARE_UPDATE:Ljava/lang/String; = "SENSORHUB_FIRMWARE_UPDATE"

.field public static final SENSORHUB_FIRMWARE_VERSION:Ljava/lang/String; = "SENSORHUB_FIRMWARE_VERSION"

.field public static final SENSORHUB_MCU:Ljava/lang/String; = "SENSORHUB_MCU"

.field public static final SENSORHUB_SLEEP_TEST:Ljava/lang/String; = "SENSORHUB_SLEEP_TEST"

.field public static final TAG:Ljava/lang/String; = "Sysfs"

.field public static final TEMP_HUMID_ENGINE_VER:Ljava/lang/String; = "TEMP_HUMID_ENGINE_VER"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN1:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN1"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN2:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN2"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN3:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN3"

.field public static final TOUCH_KEY_SENSITIVITY_HIDDEN4:Ljava/lang/String; = "TOUCH_KEY_SENSITIVITY_HIDDEN4"

.field public static final TSK_FACTORY_MODE:Ljava/lang/String; = "TSK_FACTORY_MODE"

.field public static final TSK_FIRMWARE_UPDATE:Ljava/lang/String; = "TSK_FIRMWARE_UPDATE"

.field public static final TSK_FIRMWARE_UPDATE_STATUS:Ljava/lang/String; = "TSK_FIRMWARE_UPDATE_STATUS"

.field public static final TSK_FIRMWARE_VERSION_PANEL:Ljava/lang/String; = "TSK_FIRMWARE_VERSION_PANEL"

.field public static final TSK_FIRMWARE_VERSION_PHONE:Ljava/lang/String; = "TSK_FIRMWARE_VERSION_PHONE"

.field public static final TSP_COMMAND_CMD:Ljava/lang/String; = "TSP_COMMAND_CMD"

.field public static final TSP_COMMAND_RESULT:Ljava/lang/String; = "TSP_COMMAND_RESULT"

.field public static final TSP_COMMAND_STATUS:Ljava/lang/String; = "TSP_COMMAND_STATUS"

.field public static final UART_ON_OFF:Ljava/lang/String; = "UART_ON_OFF"

.field public static final ULTRASONIC_ADC_DISTANCE:Ljava/lang/String; = "ULTRASONIC_ADC_DISTANCE"

.field public static final ULTRASONIC_VER_CHECK:Ljava/lang/String; = "ULTRASONIC_VER_CHECK"

.field public static final UV_LIBRARY_VER:Ljava/lang/String; = "UV_LIBRARY_VER"

.field public static final UV_PROD_ID:Ljava/lang/String; = "UV_PROD_ID"

.field public static final UV_SENSOR_EOL_TEST:Ljava/lang/String; = "UV_SENSOR_EOL_TEST"

.field public static final WINDOW_TYPE:Ljava/lang/String; = "WINDOW_TYPE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 591
    const-string v0, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isExistFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 845
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 846
    .local v0, "f":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isExistFileID(Ljava/lang/String;)Z
    .locals 2
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 850
    new-instance v0, Ljava/io/File;

    const-string v1, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 851
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method public static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 595
    const-string v5, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 596
    .local v1, "path":Ljava/lang/String;
    const/4 v4, 0x0

    .line 597
    .local v4, "value":Ljava/lang/String;
    const/4 v2, 0x0

    .line 600
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 602
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 603
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 605
    if-eqz v4, :cond_0

    .line 606
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 612
    :cond_0
    if-eqz v3, :cond_3

    .line 614
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 622
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.read"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    return-object v4

    .line 615
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 616
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v2, v3

    .line 617
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 609
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 610
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 612
    if-eqz v2, :cond_1

    .line 614
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 615
    :catch_2
    move-exception v0

    .line 616
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 612
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_2

    .line 614
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 617
    :cond_2
    :goto_3
    throw v5

    .line 615
    :catch_3
    move-exception v0

    .line 616
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 612
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 609
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static read(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "logLevel"    # I

    .prologue
    .line 672
    const-string v5, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {p0, v5, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$1000(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 673
    .local v1, "path":Ljava/lang/String;
    const/4 v4, 0x0

    .line 674
    .local v4, "value":Ljava/lang/String;
    const/4 v2, 0x0

    .line 677
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 679
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 680
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 681
    if-eqz v4, :cond_0

    .line 682
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 688
    :cond_0
    if-eqz v3, :cond_4

    .line 690
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 697
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-lez p1, :cond_2

    .line 698
    const-string v5, "Support"

    const-string v6, "Kernel.read"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_2
    return-object v4

    .line 691
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 692
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v2, v3

    .line 693
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 685
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 686
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 688
    if-eqz v2, :cond_1

    .line 690
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 691
    :catch_2
    move-exception v0

    .line 692
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 688
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_3

    .line 690
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 693
    :cond_3
    :goto_3
    throw v5

    .line 691
    :catch_3
    move-exception v0

    .line 692
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 688
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 685
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_4
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static readFromPath(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 11
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "isLog"    # Z

    .prologue
    .line 627
    const/4 v5, 0x0

    .line 628
    .local v5, "value":Ljava/lang/String;
    const/4 v3, 0x0

    .line 629
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 631
    .local v1, "freader":Ljava/io/FileReader;
    if-eqz p0, :cond_0

    const-string v7, "Unknown"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move-object v6, v5

    .line 665
    .end local v5    # "value":Ljava/lang/String;
    .local v6, "value":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 636
    .end local v6    # "value":Ljava/lang/String;
    .restart local v5    # "value":Ljava/lang/String;
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 638
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_2

    .line 639
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    move-object v3, v4

    .line 642
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_2
    if-eqz v3, :cond_3

    .line 643
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 645
    if-eqz v5, :cond_3

    .line 646
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v5

    .line 652
    :cond_3
    if-eqz v3, :cond_7

    .line 654
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 661
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :cond_4
    :goto_1
    if-eqz p1, :cond_5

    .line 662
    const-string v7, "Support"

    const-string v8, "Kernel.readFromPath"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "path="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", value="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v6, v5

    .line 665
    .end local v5    # "value":Ljava/lang/String;
    .restart local v6    # "value":Ljava/lang/String;
    goto :goto_0

    .line 655
    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v6    # "value":Ljava/lang/String;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v5    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 656
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v1, v2

    .line 657
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_1

    .line 649
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 650
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 652
    if-eqz v3, :cond_4

    .line 654
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 655
    :catch_2
    move-exception v0

    .line 656
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 652
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v3, :cond_6

    .line 654
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 657
    :cond_6
    :goto_4
    throw v7

    .line 655
    :catch_3
    move-exception v0

    .line 656
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_4

    .line 652
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3

    .line 649
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :cond_7
    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_1
.end method

.method public static readVal(Ljava/lang/String;)I
    .locals 10
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 705
    const-string v6, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v6}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 706
    .local v1, "path":Ljava/lang/String;
    const/4 v5, 0x0

    .line 707
    .local v5, "value":Ljava/lang/String;
    const/4 v4, 0x0

    .line 708
    .local v4, "retVal":I
    const/4 v2, 0x0

    .line 711
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 713
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 714
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 715
    if-eqz v5, :cond_0

    .line 716
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 722
    :cond_0
    if-eqz v3, :cond_3

    .line 724
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 731
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    const-string v6, "Support"

    const-string v7, "Kernel.readVal"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "path="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", retVal="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    return v4

    .line 725
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 726
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    move-object v2, v3

    .line 727
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 719
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 720
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 722
    if-eqz v2, :cond_1

    .line 724
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 725
    :catch_2
    move-exception v0

    .line 726
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 722
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v2, :cond_2

    .line 724
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 727
    :cond_2
    :goto_3
    throw v6

    .line 725
    :catch_3
    move-exception v0

    .line 726
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 722
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 719
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static write(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 736
    const-string v5, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 737
    .local v1, "path":Ljava/lang/String;
    const/4 v2, 0x1

    .line 738
    .local v2, "res":Z
    const/4 v3, 0x0

    .line 741
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_0
    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, v1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742
    .end local v3    # "writer":Ljava/io/FileWriter;
    .local v4, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v4, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 743
    invoke-virtual {v4}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 749
    if-eqz v4, :cond_0

    .line 750
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v3, v4

    .line 758
    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.write"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    return v2

    .line 752
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 754
    const/4 v2, 0x0

    move-object v3, v4

    .line 756
    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_0

    .line 744
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 745
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 746
    const/4 v2, 0x0

    .line 749
    if-eqz v3, :cond_1

    .line 750
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 752
    :catch_2
    move-exception v0

    .line 753
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 754
    const/4 v2, 0x0

    .line 756
    goto :goto_0

    .line 748
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 749
    :goto_2
    if-eqz v3, :cond_2

    .line 750
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 755
    :cond_2
    :goto_3
    throw v5

    .line 752
    :catch_3
    move-exception v0

    .line 753
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 754
    const/4 v2, 0x0

    goto :goto_3

    .line 748
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 744
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v4    # "writer":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v3, v4

    .end local v4    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public static write(Ljava/lang/String;[B)Z
    .locals 9
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "value"    # [B

    .prologue
    .line 763
    const-string v5, "path"

    # invokes: Lcom/sec/android/app/hwmoduletest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v5}, Lcom/sec/android/app/hwmoduletest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 764
    .local v3, "path":Ljava/lang/String;
    const/4 v4, 0x1

    .line 765
    .local v4, "res":Z
    const/4 v1, 0x0

    .line 768
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 769
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 770
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 776
    if-eqz v2, :cond_0

    .line 777
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 785
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v6, "Support"

    const-string v7, "Kernel.write"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "path="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", value="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p1, :cond_3

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v7, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    return v4

    .line 779
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 780
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 781
    const/4 v4, 0x0

    move-object v1, v2

    .line 783
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 771
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 772
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 773
    const/4 v4, 0x0

    .line 776
    if-eqz v1, :cond_1

    .line 777
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 779
    :catch_2
    move-exception v0

    .line 780
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 781
    const/4 v4, 0x0

    .line 783
    goto :goto_0

    .line 775
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 776
    :goto_3
    if-eqz v1, :cond_2

    .line 777
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 782
    :cond_2
    :goto_4
    throw v5

    .line 779
    :catch_3
    move-exception v0

    .line 780
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 781
    const/4 v4, 0x0

    goto :goto_4

    .line 785
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    const-string v5, "null"

    goto :goto_1

    .line 775
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 771
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method

.method public static writeToPath(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 817
    const/4 v1, 0x1

    .line 818
    .local v1, "res":Z
    const/4 v2, 0x0

    .line 820
    .local v2, "writer":Ljava/io/FileWriter;
    const-string v4, "Support"

    const-string v5, "Kernel.writeToPath start"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 824
    .end local v2    # "writer":Ljava/io/FileWriter;
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 831
    if-eqz v3, :cond_0

    .line 832
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 840
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    const-string v4, "Support"

    const-string v5, "Kernel.writeToPath end"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    return v1

    .line 834
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 835
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 836
    const/4 v1, 0x0

    move-object v2, v3

    .line 838
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_0

    .line 826
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 827
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 828
    const/4 v1, 0x0

    .line 831
    if-eqz v2, :cond_1

    .line 832
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 834
    :catch_2
    move-exception v0

    .line 835
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 836
    const/4 v1, 0x0

    .line 838
    goto :goto_0

    .line 830
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 831
    :goto_2
    if-eqz v2, :cond_2

    .line 832
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 837
    :cond_2
    :goto_3
    throw v4

    .line 834
    :catch_3
    move-exception v0

    .line 835
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 836
    const/4 v1, 0x0

    goto :goto_3

    .line 830
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 826
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method public static writeToPath(Ljava/lang/String;[B)Z
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # [B

    .prologue
    .line 790
    const/4 v3, 0x1

    .line 791
    .local v3, "res":Z
    const/4 v1, 0x0

    .line 794
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 796
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 803
    if-eqz v2, :cond_0

    .line 804
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 812
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v5, "Support"

    const-string v6, "Kernel.writeToPath"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", value="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p1, :cond_3

    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    return v3

    .line 806
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 808
    const/4 v3, 0x0

    move-object v1, v2

    .line 810
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 798
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 799
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 800
    const/4 v3, 0x0

    .line 803
    if-eqz v1, :cond_1

    .line 804
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 806
    :catch_2
    move-exception v0

    .line 807
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 808
    const/4 v3, 0x0

    .line 810
    goto :goto_0

    .line 802
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 803
    :goto_3
    if-eqz v1, :cond_2

    .line 804
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 809
    :cond_2
    :goto_4
    throw v4

    .line 806
    :catch_3
    move-exception v0

    .line 807
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 808
    const/4 v3, 0x0

    goto :goto_4

    .line 812
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    const-string v4, "null"

    goto :goto_1

    .line 802
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 798
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2
.end method
