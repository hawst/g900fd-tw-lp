.class Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;
.super Ljava/lang/Object;
.source "UVDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/UVDisplay$1;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MAXIM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    new-array v3, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$004(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getADC()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVIndex()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/UVDisplay;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;[Ljava/lang/String;)V

    invoke-interface {v0, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 104
    const-string v0, "UVDisplay"

    const-string v1, "startDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ADC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getADC()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVIndex()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/UVDisplay;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mDisplayAdapter:Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$800(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay$DisplayAdapter;->notifyDataSetChanged()V

    .line 126
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ADI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$004(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVA()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVB()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVIndex()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/UVDisplay;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;[Ljava/lang/String;)V

    invoke-interface {v0, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 111
    const-string v0, "UVDisplay"

    const-string v1, "startDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV A:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVA()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV B:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVB()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mSensorUV:Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$300(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/hwmoduletest/sensors/SensorUV;->getUVIndex()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$400(Lcom/sec/android/app/hwmoduletest/UVDisplay;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mVendor:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$200(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "STMicroelectronics"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    new-array v3, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$004(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/UVDisplay;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v7

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v5, v5, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/UVDisplay;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v7

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(F)Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$700(Lcom/sec/android/app/hwmoduletest/UVDisplay;F)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;[Ljava/lang/String;)V

    invoke-interface {v0, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 118
    const-string v0, "UVDisplay"

    const-string v1, "startDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$000(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ADC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/UVDisplay;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UV calc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v3, v3, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mValue:[Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$600(Lcom/sec/android/app/hwmoduletest/UVDisplay;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v7

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    # invokes: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Calculate_UVcal(F)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$700(Lcom/sec/android/app/hwmoduletest/UVDisplay;F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # getter for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->mItemList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$500(Lcom/sec/android/app/hwmoduletest/UVDisplay;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v2, v2, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    new-array v3, v5, [Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/UVDisplay$1$1;->this$1:Lcom/sec/android/app/hwmoduletest/UVDisplay$1;

    iget-object v4, v4, Lcom/sec/android/app/hwmoduletest/UVDisplay$1;->this$0:Lcom/sec/android/app/hwmoduletest/UVDisplay;

    # ++operator for: Lcom/sec/android/app/hwmoduletest/UVDisplay;->Count:I
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/UVDisplay;->access$004(Lcom/sec/android/app/hwmoduletest/UVDisplay;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/UVDisplay$TestItem;-><init>(Lcom/sec/android/app/hwmoduletest/UVDisplay;[Ljava/lang/String;)V

    invoke-interface {v0, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method
