.class public Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;
.super Landroid/view/View;
.source "LcdFlameTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/LcdFlameTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private coordi_X:[F

.field private coordi_Y:[F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/LcdFlameTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/hwmoduletest/LcdFlameTest;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x3

    .line 144
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->this$0:Lcom/sec/android/app/hwmoduletest/LcdFlameTest;

    .line 145
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 141
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_X:[F

    .line 142
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_Y:[F

    .line 146
    invoke-virtual {p1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->mScreenWidth:I

    .line 147
    invoke-virtual {p1}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->mScreenHeight:I

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->init()V

    .line 149
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const/high16 v5, 0x40800000    # 4.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 174
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_X:[F

    add-int/lit8 v2, v0, -0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->mScreenWidth:I

    mul-int/2addr v3, v0

    int-to-float v3, v3

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    aput v3, v1, v2

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_Y:[F

    add-int/lit8 v2, v0, -0x1

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->mScreenHeight:I

    mul-int/2addr v3, v0

    int-to-float v3, v3

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    aput v3, v1, v2

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x3

    const/high16 v6, 0x40a00000    # 5.0f

    .line 152
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 154
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 155
    .local v2, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 156
    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 157
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 158
    const/high16 v4, -0x1000000

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    const/4 v4, 0x5

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v4, v6, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 163
    .local v3, "px":F
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v7, :cond_1

    .line 164
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v7, :cond_0

    .line 165
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_X:[F

    aget v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/LcdFlameTest$MyView;->coordi_Y:[F

    aget v5, v5, v1

    invoke-virtual {p1, v4, v5, v3, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 163
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 169
    .end local v0    # "i":I
    :cond_1
    return-void
.end method
