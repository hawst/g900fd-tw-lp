.class public Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
.super Ljava/lang/Object;
.source "FactoryTestPhone.java"


# static fields
.field private static final FACTORY_TEST_HISTORY_VIEW_FIRST_COMMAND:I = 0x12

.field private static final FACTORY_TEST_HISTORY_VIEW_SECOND_COMMAND:I = 0x3

.field private static final FACTORY_TEST_STATUS_FIRST_COMMAND:I = 0x12

.field private static final FACTORY_TEST_STATUS_SECOND_COMMAND:I = 0x4

.field private static final MSG_DATA_CP_ACCELEROMETER:I = 0xce

.field private static final MSG_REQUEST_GRIP_CONFIRM:I = 0xcd

.field private static final MSG_REQUEST_HISTORY_NV_VALUES:I = 0x65

.field private static final MSG_REQUEST_ITEM_NV_UPDATE:I = 0x66

.field private static final MSG_REQUEST_TEST_NV_VALUES:I = 0x64

.field private static mQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private IS_CAL_TEST_PASS:Z

.field private IS_FINAL_TEST_PASS:Z

.field private IS_LTECAL_TEST_PASS:Z

.field private IS_LTEFINAL_TEST_PASS:Z

.field private IS_PBA_TEST_PASS:Z

.field private IS_SMD_TEST_PASS:Z

.field private isConnected:Z

.field mContext:Landroid/content/Context;

.field private mData:[B

.field public mHandler:Landroid/os/Handler;

.field private final mNVKeyToItemIDHashmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private mResponse:Landroid/os/Message;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "FactoryTestPhone"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_SMD_TEST_PASS:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_PBA_TEST_PASS:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_CAL_TEST_PASS:Z

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_FINAL_TEST_PASS:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTECAL_TEST_PASS:Z

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTEFINAL_TEST_PASS:Z

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mNVKeyToItemIDHashmap:Ljava/util/HashMap;

    .line 157
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$1;-><init>(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 389
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone$2;-><init>(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mContext:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 78
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    .line 79
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mData:[B

    .line 80
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mResponse:Landroid/os/Message;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z

    return p1
.end method

.method static synthetic access$300()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mData:[B

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mResponse:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;[BLandroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Landroid/os/Message;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->sendMessageToRil([BLandroid/os/Message;)V

    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "invokeOemRilRequestRaw"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "invokeOemRilRequestRaw : failed offer a item"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "isConnected == true"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_1
    sget-object v0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 216
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->sendMessageToRil([BLandroid/os/Message;)V

    goto :goto_1

    .line 219
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "isConnected == false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->bindSecPhoneService()V

    goto :goto_0
.end method

.method private sendMessageToRil([BLandroid/os/Message;)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 175
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 177
    .local v0, "req":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessage() - current Queue size before : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    sget-object v1, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 180
    sget-object v1, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "data":[B
    check-cast p1, [B

    .line 181
    .restart local p1    # "data":[B
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 182
    new-instance p2, Landroid/os/Message;

    .end local p2    # "response":Landroid/os/Message;
    invoke-direct {p2}, Landroid/os/Message;-><init>()V

    .line 183
    .restart local p2    # "response":Landroid/os/Message;
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 187
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage() to RIL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->unbindSecPhoneService()V

    .line 201
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessage() - current Queue size after : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void

    .line 194
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage : mQueue is empty!!!"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 191
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public DestroySecPhoneService()V
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->unbindSecPhoneService()V

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 387
    return-void
.end method

.method public bindSecPhoneService()V
    .locals 4

    .prologue
    .line 84
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "bindSecPhoneService"

    const-string v3, "bindSecPhoneService()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 88
    return-void
.end method

.method public getResultForPGMItems(Ljava/lang/String;)Z
    .locals 3
    .param p1, "NVKey"    # Ljava/lang/String;

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "result":Z
    const/16 v2, 0x10

    invoke-static {p1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 104
    .local v0, "itemNv":I
    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    :pswitch_0
    return v1

    .line 106
    :pswitch_1
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_SMD_TEST_PASS:Z

    .line 107
    goto :goto_0

    .line 109
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_PBA_TEST_PASS:Z

    .line 110
    goto :goto_0

    .line 112
    :pswitch_3
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_CAL_TEST_PASS:Z

    .line 113
    goto :goto_0

    .line 115
    :pswitch_4
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_FINAL_TEST_PASS:Z

    .line 116
    goto :goto_0

    .line 118
    :pswitch_5
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTECAL_TEST_PASS:Z

    .line 119
    goto :goto_0

    .line 121
    :pswitch_6
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTEFINAL_TEST_PASS:Z

    .line 122
    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public requestGripSensorOn(Z)V
    .locals 12
    .param p1, "isOn"    # Z

    .prologue
    const/4 v11, 0x4

    .line 298
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "requestGripSensorOn"

    const-string v9, "requestGripSensorOn()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 300
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 301
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0x8

    .line 302
    .local v4, "fileSize":I
    const/16 v5, 0x12

    .line 303
    .local v5, "mainCmd":B
    const/16 v6, 0x9

    .line 304
    .local v6, "subCmd":B
    const/4 v1, 0x0

    .line 306
    .local v1, "data":[B
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "requestGripSensorOn"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RILSUPPORTBR_____ requestGripSensorOn() ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "MODEL_NAME"

    invoke-static {v10}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]_____RILSUPPORTBR"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    if-eqz p1, :cond_1

    .line 314
    new-array v1, v11, [B

    .end local v1    # "data":[B
    fill-array-data v1, :array_0

    .line 330
    .restart local v1    # "data":[B
    :goto_0
    const/16 v7, 0x12

    :try_start_0
    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 331
    const/16 v7, 0x9

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 332
    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 333
    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    if-eqz v2, :cond_0

    .line 339
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 346
    :cond_0
    :goto_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mData:[B

    .line 347
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x65

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mResponse:Landroid/os/Message;

    .line 348
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v9, 0xcd

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 349
    return-void

    .line 323
    :cond_1
    new-array v1, v11, [B

    .end local v1    # "data":[B
    fill-array-data v1, :array_1

    .restart local v1    # "data":[B
    goto :goto_0

    .line 340
    :catch_0
    move-exception v3

    .line 341
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 334
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 335
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 337
    if-eqz v2, :cond_0

    .line 339
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 340
    :catch_2
    move-exception v3

    .line 341
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 337
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v2, :cond_2

    .line 339
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 342
    :cond_2
    :goto_2
    throw v7

    .line 340
    :catch_3
    move-exception v3

    .line 341
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 314
    nop

    :array_0
    .array-data 1
        0x2t
        0x0t
        0xbt
        0x1t
    .end array-data

    .line 323
    :array_1
    .array-data 1
        0x2t
        0x0t
        0xbt
        0x0t
    .end array-data
.end method

.method public requestHistoryNvViewToRil()V
    .locals 7

    .prologue
    .line 247
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 248
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 249
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x7

    .line 252
    .local v3, "fileSize":I
    const/16 v4, 0x12

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 253
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 254
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 255
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 256
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 257
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x65

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 264
    return-void

    .line 258
    :catch_0
    move-exception v2

    .line 259
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestTestNvViewToRil()V
    .locals 7

    .prologue
    .line 227
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 228
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 229
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x7

    .line 232
    .local v3, "fileSize":I
    const/16 v4, 0x12

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 233
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 234
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 235
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 236
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 237
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 244
    return-void

    .line 238
    :catch_0
    move-exception v2

    .line 239
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendToRilCpAccelermeter([B)V
    .locals 9
    .param p1, "control"    # [B

    .prologue
    .line 355
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "sendToRilCpAccelermeter"

    const-string v8, "RILSUPPORTBR_____ sendToRilCpAccelermeter() _____RILSUPPORTBR"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 357
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 358
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x8

    .line 359
    .local v3, "fileSize":I
    const/16 v4, 0x12

    .line 360
    .local v4, "mainCmd":B
    const/16 v5, 0x9

    .line 363
    .local v5, "subCmd":B
    const/16 v6, 0x12

    :try_start_0
    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 364
    const/16 v6, 0x9

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 365
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 366
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    if-eqz v1, :cond_0

    .line 372
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 379
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xce

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 380
    return-void

    .line 373
    :catch_0
    move-exception v2

    .line 374
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 367
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 368
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 370
    if-eqz v1, :cond_0

    .line 372
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 373
    :catch_2
    move-exception v2

    .line 374
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 370
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_1

    .line 372
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 375
    :cond_1
    :goto_1
    throw v6

    .line 373
    :catch_3
    move-exception v2

    .line 374
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setResultForPGMItems(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "NVKey"    # Ljava/lang/String;
    .param p2, "result"    # Z

    .prologue
    .line 131
    const/16 v1, 0x10

    invoke-static {p1, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 133
    .local v0, "itemNv":I
    packed-switch v0, :pswitch_data_0

    .line 155
    :goto_0
    :pswitch_0
    return-void

    .line 135
    :pswitch_1
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_SMD_TEST_PASS:Z

    goto :goto_0

    .line 138
    :pswitch_2
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_PBA_TEST_PASS:Z

    goto :goto_0

    .line 141
    :pswitch_3
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_CAL_TEST_PASS:Z

    goto :goto_0

    .line 144
    :pswitch_4
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_FINAL_TEST_PASS:Z

    goto :goto_0

    .line 147
    :pswitch_5
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTECAL_TEST_PASS:Z

    goto :goto_0

    .line 150
    :pswitch_6
    iput-boolean p2, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->IS_LTEFINAL_TEST_PASS:Z

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public unbindSecPhoneService()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "unbindSecPhoneService"

    const-string v2, "unbindSecPhoneService()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->isConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public updateNVItem(BB)V
    .locals 7
    .param p1, "itemID"    # B
    .param p2, "result"    # B

    .prologue
    .line 267
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 268
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 269
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/16 v3, 0x9

    .line 272
    .local v3, "fileSize":I
    const/16 v4, 0x12

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 273
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 274
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 275
    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 276
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 277
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 278
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 279
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 284
    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v6, 0x66

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/hwmoduletest/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 286
    return-void

    .line 280
    :catch_0
    move-exception v2

    .line 281
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
