.class Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;
.super Landroid/os/Handler;
.source "SensorHubTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/SensorHubTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    .line 57
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 109
    :goto_0
    return-void

    .line 59
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$000(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mHandler()"

    const-string v8, "Display SensorHub Info()"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getMcuFirmware()V
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$100(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V

    .line 61
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getBinFirmware()V
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$200(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V

    .line 62
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$300(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mHandler()"

    const-string v8, "Start SensorHub SelfTest"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    iget-boolean v6, v6, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->isFactoryMode:Z

    if-eqz v6, :cond_0

    .line 65
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, "alarmManagerOffIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 67
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$400(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "handleCommand"

    const-string v8, "AlarmManager Enable"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .end local v0    # "alarmManagerOffIntent":Landroid/content/Intent;
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$500(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startSensrohubTest()V

    .line 71
    new-instance v4, Landroid/content/Intent;

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/sec/android/app/hwmoduletest/WakeUpService;

    invoke-direct {v4, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    .local v4, "intent":Landroid/content/Intent;
    const-string v6, "com.sec.factory.WakeUp"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v6, "isAcquireWakelock"

    invoke-virtual {v4, v6, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11, v4, v11}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 75
    .local v5, "sender":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "alarm"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 76
    .local v2, "am":Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x1388

    add-long/2addr v6, v8

    invoke-virtual {v2, v11, v6, v7, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 77
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mPM:Landroid/os/PowerManager;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$600(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/os/PowerManager;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/os/PowerManager;->goToSleep(J)V

    goto/16 :goto_0

    .line 80
    .end local v2    # "am":Landroid/app/AlarmManager;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "sender":Landroid/app/PendingIntent;
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$700(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mHandler()"

    const-string v8, "WakeUp device"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    iget-boolean v6, v6, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->isFactoryMode:Z

    if-eqz v6, :cond_1

    .line 83
    new-instance v1, Landroid/content/Intent;

    const-string v6, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    .local v1, "alarmManagerOnIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 85
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$800(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "handleCommand"

    const-string v8, "AlarmManager disable"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .end local v1    # "alarmManagerOnIntent":Landroid/content/Intent;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mModuleDevice:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$500(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->readSensrohubTest()Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$902(Lcom/sec/android/app/hwmoduletest/SensorHubTest;[Ljava/lang/String;)[Ljava/lang/String;

    .line 89
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$1000(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Result - "

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MCU name: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v11

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  /  Selftest: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  /  INT pin: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v12

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->IntCheckResult:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$1100(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "INT Check : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v12

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->McuChipName:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$1200(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MCU name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->result:[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$900(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->mPM:Landroid/os/PowerManager;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$600(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Landroid/os/PowerManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 96
    const-wide/16 v6, 0xc8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    move-result-object v6

    const/16 v7, 0xff

    invoke-virtual {v6, v7}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->setBrightness(I)V

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$1300(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mHandler()"

    const-string v8, "checkPassResult()"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/SensorHubTest;->checkPassFail()V
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->access$1400(Lcom/sec/android/app/hwmoduletest/SensorHubTest;)V

    goto/16 :goto_0

    .line 97
    :catch_0
    move-exception v3

    .line 98
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 106
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/SensorHubTest$1;->this$0:Lcom/sec/android/app/hwmoduletest/SensorHubTest;

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/SensorHubTest;->finish()V

    goto/16 :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
