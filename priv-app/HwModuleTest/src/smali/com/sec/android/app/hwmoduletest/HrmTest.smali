.class public Lcom/sec/android/app/hwmoduletest/HrmTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "HrmTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;,
        Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;
    }
.end annotation


# instance fields
.field private ITEM_NUM:I

.field private final MSG_UPDATE_BIO_SENSOR_VALUE:B

.field private final MSG_UPDATE_HRM_SENSOR_VALUE:B

.field public final MSG_UPDATE_UI:I

.field private final UpdateBio:I

.field private final UpdateHrm:I

.field private hrm_vendor:Ljava/lang/String;

.field private linearLayout:Landroid/widget/LinearLayout;

.field private mBIOSensor:Landroid/hardware/Sensor;

.field private mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mHRMHRAvg:F

.field private mHRMHRCount:I

.field private mHRMHRMax:F

.field private mHRMHRMin:F

.field private mHRMHRTotal:F

.field private mHRMHeartRate:F

.field private mHRMIRLevel:F

.field private mHRMREDLevel:F

.field private mHRMRvalue:F

.field private mHRMSensor:Landroid/hardware/Sensor;

.field private mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

.field private mHRMSignalQtyAvg:F

.field private mHRMSignalQtyCount:I

.field private mHRMSignalQtyMax:F

.field private mHRMSignalQtyMin:F

.field private mHRMSignalQtyTotal:F

.field private mHRMSignalQuality:F

.field private mHRMSpO2:F

.field private mHRMSpO2Avg:F

.field private mHRMSpO2Count:I

.field private mHRMSpO2Max:F

.field private mHRMSpO2Min:F

.field private mHRMSpO2Total:F

.field private mHRMTemp:F

.field private mHRM_RRI:F

.field private mHRM_RRI_Avg:F

.field private mHRM_RRI_Count:I

.field private mHRM_RRI_Max:F

.field private mHRM_RRI_Min:F

.field private mHRM_RRI_Total:F

.field private mHRM_SNR:F

.field private mHRM_SNR_Avg:F

.field private mHRM_SNR_Count:I

.field private mHRM_SNR_Max:F

.field private mHRM_SNR_Min:F

.field private mHRM_SNR_Total:F

.field private mHandler:Landroid/os/Handler;

.field private mHrmGraph:Lcom/sec/android/app/hwmoduletest/view/HrmGraph;

.field private mIsRvalue:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThreeFormat:Ljava/text/DecimalFormat;

.field private mTimer:Ljava/util/Timer;

.field private txt_item:[Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 106
    const-string v0, "HrmTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->ITEM_NUM:I

    .line 51
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mTimer:Ljava/util/Timer;

    .line 61
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->UpdateBio:I

    .line 62
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->UpdateHrm:I

    .line 64
    const/16 v0, 0xa

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->MSG_UPDATE_BIO_SENSOR_VALUE:B

    .line 65
    const/16 v0, 0xb

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->MSG_UPDATE_HRM_SENSOR_VALUE:B

    .line 67
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F

    .line 68
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F

    .line 69
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F

    .line 70
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F

    .line 71
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I

    .line 72
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F

    .line 73
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Avg:F

    .line 74
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F

    .line 75
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F

    .line 76
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I

    .line 77
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F

    .line 78
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F

    .line 79
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F

    .line 80
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F

    .line 81
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F

    .line 83
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Avg:F

    .line 84
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F

    .line 85
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F

    .line 86
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I

    .line 87
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Min:F

    .line 88
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Avg:F

    .line 89
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Max:F

    .line 90
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F

    .line 91
    iput v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I

    .line 102
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mIsRvalue:Z

    .line 104
    iput v4, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->MSG_UPDATE_UI:I

    .line 109
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HrmTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/HrmTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;

    .line 107
    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMTemp:F

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMTemp:F

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHeartRate:F

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/hwmoduletest/HrmTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mIsRvalue:Z

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMRvalue:F

    return v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMRvalue:F

    return p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2:F

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQuality:F

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI:F

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR:F

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F

    return v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMin:F

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRMax:F

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F

    return v0
.end method

.method static synthetic access$2916(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRTotal:F

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRAvg:F

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I

    return v0
.end method

.method static synthetic access$3108(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMHRCount:I

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mThreeFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/text/DecimalFormat;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Min:F

    return p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F

    return v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Max:F

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F

    return v0
.end method

.method static synthetic access$3616(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Total:F

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Avg:F

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Avg:F

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I

    return v0
.end method

.method static synthetic access$3808(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSpO2Count:I

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F

    return v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Min:F

    return p1
.end method

.method static synthetic access$4000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Max:F

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F

    return v0
.end method

.method static synthetic access$4116(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Total:F

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Avg:F

    return v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Avg:F

    return p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I

    return v0
.end method

.method static synthetic access$4308(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_SNR_Count:I

    return v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Min:F

    return v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Min:F

    return p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Max:F

    return v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Max:F

    return p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F

    return v0
.end method

.method static synthetic access$4616(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Total:F

    return v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Avg:F

    return p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I

    return v0
.end method

.method static synthetic access$4808(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRM_RRI_Count:I

    return v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F

    return v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMin:F

    return p1
.end method

.method static synthetic access$5000(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F

    return v0
.end method

.method static synthetic access$5002(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyAvg:F

    return p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyMax:F

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/hwmoduletest/HrmTest;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F

    return v0
.end method

.method static synthetic access$5216(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p1, "x1"    # F

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyTotal:F

    return v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I

    return v0
.end method

.method static synthetic access$5308(Lcom/sec/android/app/hwmoduletest/HrmTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSignalQtyCount:I

    return v0
.end method

.method private init()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    .line 191
    const-string v1, "HRM_VENDOR"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    .line 193
    const-string v1, "MAXIM"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 194
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->ITEM_NUM:I

    .line 199
    :cond_0
    :goto_0
    const v1, 0x7f0b0121

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->linearLayout:Landroid/widget/LinearLayout;

    .line 200
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->ITEM_NUM:I

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    .line 202
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->ITEM_NUM:I

    if-ge v0, v1, :cond_2

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    const/high16 v3, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->linearLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    .end local v0    # "i":I
    :cond_1
    const-string v1, "ADI"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->ITEM_NUM:I

    goto :goto_0

    .line 214
    .restart local v0    # "i":I
    :cond_2
    new-instance v1, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    invoke-direct {v1, p0, v5}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HrmTest;Lcom/sec/android/app/hwmoduletest/HrmTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .line 215
    new-instance v1, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    invoke-direct {v1, p0, v5}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HrmTest;Lcom/sec/android/app/hwmoduletest/HrmTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    .line 216
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10019

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensor:Landroid/hardware/Sensor;

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x1001a

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensor:Landroid/hardware/Sensor;

    .line 219
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x0

    .line 125
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 126
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->setContentView(I)V

    .line 127
    const v0, 0x7f0b0122

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHrmGraph:Lcom/sec/android/app/hwmoduletest/view/HrmGraph;

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->init()V

    .line 130
    new-instance v6, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v6}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 131
    .local v6, "paramDecimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    const/16 v0, 0x2e

    invoke-virtual {v6, v0}, Ljava/text/DecimalFormatSymbols;->setDecimalSeparator(C)V

    .line 132
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mFormat:Ljava/text/DecimalFormat;

    .line 133
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.###"

    invoke-direct {v0, v1, v6}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mThreeFormat:Ljava/text/DecimalFormat;

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 138
    const-string v0, "RVALUE_SPO2_IN_HRM_TEST"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "Display to R value"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mIsRvalue:Z

    .line 142
    :cond_0
    return-void
.end method

.method protected onDestory()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 186
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 187
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->access$900(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$1000(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 182
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 145
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->access$000(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$100(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)V

    .line 150
    return-void
.end method

.method public updateUI(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 153
    if-nez p1, :cond_3

    .line 154
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getTempValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->access$200(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHrmGraph:Lcom/sec/android/app/hwmoduletest/view/HrmGraph;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->addValueIR(F)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHrmGraph:Lcom/sec/android/app/hwmoduletest/view/HrmGraph;

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/view/HrmGraph;->addValueRED(F)V

    .line 173
    :cond_1
    :goto_1
    return-void

    .line 156
    :cond_2
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getIRValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->access$300(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mBIOSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getREDValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->access$400(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 162
    :cond_3
    if-ne p1, v2, :cond_5

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getHeartRateValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$500(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    const-string v0, "MAXIM"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getSignalQulityValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$600(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 166
    :cond_4
    const-string v0, "ADI"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getSNRValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$700(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->txt_item:[Landroid/widget/TextView;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMSensorTask:Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->getRRIValueString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;->access$800(Lcom/sec/android/app/hwmoduletest/HrmTest$HRMSensorTask;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 171
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "updateUI"

    const-string v2, "update type is invalid"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
