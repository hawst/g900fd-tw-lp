.class Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;
.super Ljava/lang/Object;
.source "BarometerWaterProofTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CalculatePressure"
.end annotation


# static fields
.field public static final L_LEAK_INITIAL_CHECK_SPEC:F = 3.0f

.field public static final L_LEAK_SPEC:F = 10.0f

.field public static final S_LEAK_SPEC:F = 10.0f


# instance fields
.field private mPressureValues:[F


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const/4 v0, 0x5

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    return-void

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$1;

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;-><init>()V

    return-void
.end method


# virtual methods
.method public checkInitialLLeak()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v1, v1, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v1, v1, v4

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v1, v1, v3

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v2, v2, v4

    sub-float/2addr v1, v2

    const/high16 v2, 0x40400000    # 3.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 150
    const/4 v0, 0x1

    .line 153
    :cond_0
    return v0
.end method

.method public getLLeak()F
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 118
    const/high16 v0, -0x40800000    # -1.0f

    .line 119
    .local v0, "result":F
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v7

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v8

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v8

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v5, v5, v6

    sub-float v1, v4, v5

    .line 121
    .local v1, "v1":F
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v5, v5, v6

    sub-float v2, v4, v5

    .line 122
    .local v2, "v2":F
    div-float v3, v1, v2

    .line 123
    .local v3, "v3":F
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float v0, v3, v4

    .line 124
    const-string v4, "BarometerWaterProofTest"

    const-string v5, "CalculatePressure:getLLeak"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v1="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", v2="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", v3="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .end local v1    # "v1":F
    .end local v2    # "v2":F
    .end local v3    # "v3":F
    :cond_0
    const-string v4, "BarometerWaterProofTest"

    const-string v5, "CalculatePressure:getLLeak"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return v0
.end method

.method public getSLeak()F
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 132
    const/high16 v0, -0x40800000    # -1.0f

    .line 133
    .local v0, "result":F
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v6

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v7

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v8

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v9

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v9

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v5, v5, v8

    sub-float v1, v4, v5

    .line 136
    .local v1, "v1":F
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v4, v4, v6

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aget v5, v5, v7

    sub-float v2, v4, v5

    .line 137
    .local v2, "v2":F
    div-float v3, v1, v2

    .line 138
    .local v3, "v3":F
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float v0, v3, v4

    .line 139
    const-string v4, "BarometerWaterProofTest"

    const-string v5, "CalculatePressure:getSLeak"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v1="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", v2="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", v3="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .end local v1    # "v1":F
    .end local v2    # "v2":F
    .end local v3    # "v3":F
    :cond_0
    const-string v4, "BarometerWaterProofTest"

    const-string v5, "CalculatePressure:getSLeak"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "value="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    return v0
.end method

.method public setValue(FI)V
    .locals 4
    .param p1, "f"    # F
    .param p2, "index"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/BarometerWaterProofTest$CalculatePressure;->mPressureValues:[F

    aput p1, v0, p2

    .line 113
    const-string v0, "BarometerWaterProofTest"

    const-string v1, "CalculatePressure:setValue"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    return-void
.end method
