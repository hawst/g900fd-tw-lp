.class public Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "FingerPrintTest.java"

# interfaces
.implements Lcom/validity/fingerprint/FingerprintCore$EventListener;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "FingerPrintTest"

.field private static final SENSOR_FW_TN:I = 0x0

.field private static final SENSOR_FW_TS:I = 0x1


# instance fields
.field private final OPERATION_CANCEL:I

.field private final OPERATION_CLEANUP:I

.field private final OPERATION_NOTIFY_PLACED_AND_WAIT:I

.field private final OPERATION_REMOVE_ENROLL:I

.field private final OPERATION_REQUEST_METHOD3_DATA:I

.field private final OPERATION_REQUEST_METHOD4_DATA:I

.field private final OPERATION_REQUEST_MRM_DATA:I

.field private final OPERATION_REQUEST_NORMALSCAN_DATA:I

.field private final OPERATION_REQUEST_SENSORINFO:I

.field private final OPERATION_REQUEST_SNR_DATA:I

.field private final OPERATION_SENSORSTATUS:I

.field private final OPERATION_START_ENROLL:I

.field private final OPERATION_START_INDENTIFY:I

.field private final TAG:Ljava/lang/String;

.field private final USER_ID:Ljava/lang/String;

.field handler:Landroid/os/Handler;

.field private iv_image:Landroid/widget/ImageView;

.field private mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

.field private mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

.field private mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

.field private mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

.field private mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

.field private mFingerprint:Lcom/validity/fingerprint/Fingerprint;

.field private mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;

.field private mFwVersion:I

.field private mLock:Ljava/util/concurrent/Semaphore;

.field private mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

.field private mShowPutFingerDialogRunnable:Ljava/lang/Runnable;

.field private mTest:Ljava/lang/String;

.field private mThread:Ljava/lang/Thread;

.field private mUpdateImageRunnable:Ljava/lang/Runnable;

.field private mUpdateMRMDataGraphRunnable:Ljava/lang/Runnable;

.field private mUpdateMessageRunnable:Ljava/lang/Runnable;

.field private mUpdateMethod3DataGraphRunnable:Ljava/lang/Runnable;

.field private mUpdateMethod4DataGraphRunnable:Ljava/lang/Runnable;

.field private mUpdateNormalDataGraphRunnable:Ljava/lang/Runnable;

.field private mUpdateResultMessageRunnable:Ljava/lang/Runnable;

.field private mVersion:Ljava/lang/String;

.field private m_CurrentOperation:I

.field private m_Message:Ljava/lang/String;

.field private m_PopupMessage:Ljava/lang/String;

.field private m_PreviousOperation:I

.field private m_ReadMRMDataString:Ljava/lang/String;

.field private m_ReadMRMPixelString:Ljava/lang/String;

.field private m_ReadMethod3PrimaryData:[Ljava/lang/String;

.field private m_ReadMethod3PrimaryPixel:Ljava/lang/String;

.field private m_ReadMethod3SecondaryData:[Ljava/lang/String;

.field private m_ReadMethod3SecondaryPixel:Ljava/lang/String;

.field private m_ReadMethod4BaundaryData:[Ljava/lang/String;

.field private m_ReadMethod4PrimaryData:[Ljava/lang/String;

.field private m_ReadMethod4SecondaryData:[Ljava/lang/String;

.field private m_ReadNoTermSignalString:Ljava/lang/String;

.field private m_ReadNormalScanAverageString:Ljava/lang/String;

.field private m_ReadNormalScanSerialNumberString:Ljava/lang/String;

.field private m_ReadNormalStdevSection1String:Ljava/lang/String;

.field private m_ReadNormalStdevSection2String:Ljava/lang/String;

.field private m_ReadNormalStdevString:Ljava/lang/String;

.field private m_ReadTermSignalLossString:Ljava/lang/String;

.field private m_ReadTermSignalString:Ljava/lang/String;

.field private m_ResultMessage:Ljava/lang/String;

.field private m_ScreenHeight:I

.field private m_ScreenWidth:I

.field private m_gotermbutton:Landroid/widget/Button;

.field private m_result:I

.field private tv_msg1:Landroid/widget/TextView;

.field private tv_msg2:Landroid/widget/TextView;

.field private tv_result:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    const-string v0, "FingerPrintTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 47
    const-string v0, "FingerPrintTest"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;

    .line 66
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I

    .line 67
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I

    .line 68
    const-string v0, "Test"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->USER_ID:Ljava/lang/String;

    .line 70
    iput v3, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REMOVE_ENROLL:I

    .line 71
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_START_ENROLL:I

    .line 72
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_CANCEL:I

    .line 73
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_START_INDENTIFY:I

    .line 74
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_SENSORSTATUS:I

    .line 75
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_CLEANUP:I

    .line 76
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_SENSORINFO:I

    .line 77
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_NOTIFY_PLACED_AND_WAIT:I

    .line 79
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_NORMALSCAN_DATA:I

    .line 80
    const/16 v0, 0x66

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_MRM_DATA:I

    .line 81
    const/16 v0, 0x67

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_SNR_DATA:I

    .line 82
    const/16 v0, 0x68

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_METHOD3_DATA:I

    .line 83
    const/16 v0, 0x69

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->OPERATION_REQUEST_METHOD4_DATA:I

    .line 85
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    .line 86
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    .line 87
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    .line 89
    const-string v0, "Ready"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PopupMessage:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanSerialNumberString:Ljava/lang/String;

    .line 93
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    .line 101
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    .line 102
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;

    .line 107
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    .line 108
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    .line 119
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    .line 120
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    .line 127
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$2;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateResultMessageRunnable:Ljava/lang/Runnable;

    .line 150
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$3;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateImageRunnable:Ljava/lang/Runnable;

    .line 165
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$4;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateNormalDataGraphRunnable:Ljava/lang/Runnable;

    .line 214
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMRMDataGraphRunnable:Ljava/lang/Runnable;

    .line 260
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$6;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMethod3DataGraphRunnable:Ljava/lang/Runnable;

    .line 286
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMethod4DataGraphRunnable:Ljava/lang/Runnable;

    .line 335
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$8;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mShowPutFingerDialogRunnable:Ljava/lang/Runnable;

    .line 346
    new-instance v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$9;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PopupMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    return-void
.end method

.method static synthetic access$3302(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/util/concurrent/Semaphore;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/Fingerprint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->iv_image:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->iv_image:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/validity/fingerprint/FingerprintBitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;

    return-object v0
.end method

.method private excuteOperation(I)V
    .locals 2
    .param p1, "operation"    # I

    .prologue
    .line 763
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$14;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mThread:Ljava/lang/Thread;

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1014
    return-void
.end method

.method private getSNRInfoString(Lcom/validity/fingerprint/SensorTestSNRInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "snrInfo"    # Lcom/validity/fingerprint/SensorTestSNRInfo;

    .prologue
    .line 1029
    const-string v0, ""

    .line 1031
    .local v0, "msg":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nName of the pixel group: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1033
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nFixed patter noise:\n\tStandard deviation of pixel baseline averages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->fixedPatNoise_baseAvgStdDev:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tRange of pixel baseline averages: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->fixedPatNoise_baseAvgRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1039
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSNR data-Median pixel values:\n\tPixel group number in test: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrMedianInfo_pxlCollPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal level used in SNR calculation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrMedianInfo_signal_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tNoise level used in SNR calculation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrMedianInfo_noise_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal to Noise Ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrMedianInfo_snr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1048
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSNR data-Average pixel values:\n\tPixel group number in test: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrAvgInfo_pxlCollPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrAvgInfo_sig_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tNoise level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrAvgInfo_noise_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal to Noise Ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrAvgInfo_snr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1055
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nUsing cummulative signal histogram:\n\tNumber"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrCummInfo_pxlCollPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrCummInfo_sig_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tNoise level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrCummInfo_noise_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal to Noise Ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->grpSnrCummInfo_snr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1061
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nNumber of pixels in group: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->nPxl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1063
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nSNR data-List of pixel results:\n\tCollection pixel location in data from sensor:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->pxlSnrListResults_pxlCollPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->pxlSnrListResults_sig_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tNoise level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->pxlSnrListResults_noise_level:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\tSignal to Noise Ratio: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/validity/fingerprint/SensorTestSNRInfo;->pxlSnrListResults_snr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1070
    return-object v0
.end method

.method private initLayout()V
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/4 v6, -0x1

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 407
    .local v2, "mDisplay":Landroid/view/Display;
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 408
    .local v3, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v2, v3}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 410
    const-string v4, "IS_TSP_SECOND_LCD_TEST"

    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 411
    iget v4, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 413
    :cond_0
    iget v4, v3, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I

    .line 414
    iget v4, v3, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I

    .line 416
    const v4, 0x7f0b0077

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    .line 417
    const v4, 0x7f0b0079

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod3Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod3Graph;

    .line 418
    const v4, 0x7f0b007a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    .line 421
    new-instance v4, Landroid/widget/Button;

    invoke-direct {v4, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    new-instance v5, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$10;

    invoke-direct {v5, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$10;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 433
    .local v1, "lContainerLayout":Landroid/widget/RelativeLayout;
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 435
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I

    div-int/lit8 v4, v4, 0x3

    iget v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I

    div-int/lit8 v5, v5, 0x8

    invoke-direct {v0, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 437
    .local v0, "lButtonParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 438
    const/16 v4, 0xe

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 439
    iget v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I

    div-int/lit8 v4, v4, 0x3

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 441
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    const/high16 v5, 0x41c80000    # 25.0f

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setTextSize(F)V

    .line 442
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 443
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 444
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setGravity(I)V

    .line 445
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 446
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 449
    const v4, 0x7f0b0074

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg1:Landroid/widget/TextView;

    .line 450
    const v4, 0x7f0b0075

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg2:Landroid/widget/TextView;

    .line 451
    const v4, 0x7f0b0031

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg2:Landroid/widget/TextView;

    const-string v5, "Ready"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    return-void
.end method

.method private readMRMLogFile()V
    .locals 12

    .prologue
    .line 1330
    const-string v7, "/data/validity/ValiditySnsrTest.log"

    .line 1331
    .local v7, "path":Ljava/lang/String;
    const-string v0, "Start of Selected Setting Pixel Data "

    .line 1333
    .local v0, "Header":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1335
    .local v6, "lfile":Ljava/io/File;
    const-string v9, ""

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;

    .line 1336
    const-string v9, ""

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;

    .line 1338
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1339
    const-string v9, "FingerPrintTest"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " file Exist"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1340
    const/4 v6, 0x0

    .line 1341
    const/4 v4, 0x0

    .line 1342
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 1345
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1346
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1347
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v8, ""

    .line 1349
    .local v8, "readStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 1350
    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1351
    const-string v9, "FingerPrintTest"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Found!! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1352
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1353
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1354
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1356
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1358
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1360
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;

    .line 1361
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1362
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1363
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1370
    :cond_1
    if-eqz v5, :cond_2

    .line 1372
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1379
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 1373
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1374
    .local v3, "e":Ljava/io/IOException;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1367
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "readStr":Ljava/lang/String;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 1368
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1370
    if-eqz v4, :cond_2

    .line 1372
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 1373
    :catch_2
    move-exception v3

    .line 1374
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1370
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_2
    if-eqz v4, :cond_3

    .line 1372
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1375
    :cond_3
    :goto_3
    throw v9

    .line 1373
    :catch_3
    move-exception v3

    .line 1374
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_3

    .line 1370
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v9

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 1367
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v3

    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    move-object v1, v2

    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private readMethod3LogFile()V
    .locals 23

    .prologue
    .line 1382
    const-string v12, "/data/validity/ValiditySnsrTest.log"

    .line 1383
    .local v12, "path":Ljava/lang/String;
    const-string v16, "Pri PGA Gain Selected For SNR"

    .line 1384
    .local v16, "priHeader":Ljava/lang/String;
    const-string v13, "Stimulus Av (x10)"

    .line 1385
    .local v13, "pixelHeader":Ljava/lang/String;
    const-string v14, "No Stimulus Av (x10)"

    .line 1386
    .local v14, "pixelHeader_NoStimulus":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1387
    .local v15, "pixelList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1389
    .local v7, "file":Ljava/io/File;
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 1390
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;

    .line 1391
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    .line 1392
    const/16 v18, 0x5

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    .line 1394
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1395
    const-string v18, "FingerPrintTest"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " file Exist"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    const/4 v7, 0x0

    .line 1397
    const/4 v8, 0x0

    .line 1398
    .local v8, "fis":Ljava/io/FileInputStream;
    const/4 v3, 0x0

    .line 1401
    .local v3, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v12}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1402
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v9, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/InputStreamReader;

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1403
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .local v4, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v17, ""

    .line 1405
    .local v17, "readStr":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_d

    .line 1406
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 1407
    move-object/from16 v0, v17

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1410
    :cond_1
    const-string v2, ""

    .local v2, "address":Ljava/lang/String;
    const-string v5, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1413
    .local v5, "data":Ljava/lang/String;
    :try_start_3
    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1414
    const-string v18, ","

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    .line 1422
    :goto_1
    :try_start_4
    const-string v18, "2760"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    .line 1425
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Gain 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 1472
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    .end local v17    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v6

    move-object v3, v4

    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    move-object v8, v9

    .line 1473
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/io/IOException;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1475
    if-eqz v8, :cond_2

    .line 1477
    :try_start_6
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1484
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    :cond_2
    :goto_3
    return-void

    .line 1418
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "data":Ljava/lang/String;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readStr":Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 1419
    .local v6, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_7
    invoke-virtual {v6}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 1475
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    .end local v6    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v17    # "readStr":Ljava/lang/String;
    :catchall_0
    move-exception v18

    move-object v3, v4

    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v8, :cond_3

    .line 1477
    :try_start_8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 1480
    :cond_3
    :goto_5
    throw v18

    .line 1426
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v4    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "data":Ljava/lang/String;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "readStr":Ljava/lang/String;
    :cond_4
    :try_start_9
    const-string v18, "2770"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 1428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aput-object v5, v18, v19

    .line 1429
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Signal 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1430
    :cond_5
    const-string v18, "2780"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 1432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aput-object v5, v18, v19

    .line 1433
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Noise 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1434
    :cond_6
    const-string v18, "2790"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aput-object v5, v18, v19

    .line 1437
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1438
    :cond_7
    const-string v18, "2870"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v5, v18, v19

    .line 1441
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Gain 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1442
    :cond_8
    const-string v18, "2880"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 1444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aput-object v5, v18, v19

    .line 1445
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Signal 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1446
    :cond_9
    const-string v18, "2890"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 1448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aput-object v5, v18, v19

    .line 1449
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Noise 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x2

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1450
    :cond_a
    const-string v18, "2900"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 1452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aput-object v5, v18, v19

    .line 1453
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "SNR 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1454
    :cond_b
    const-string v18, "3220"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 1456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aput-object v5, v18, v19

    .line 1457
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "N.Noise 1 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1458
    :cond_c
    const-string v18, "3310"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aput-object v5, v18, v19

    .line 1461
    const-string v18, "FingerPrintTest"

    const-string v19, "readMethod3LogFile"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "N.Noise 2 : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x4

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    .end local v2    # "address":Ljava/lang/String;
    .end local v5    # "data":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v10, v18, v19

    .line 1468
    .local v10, "gain1":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v11, v18, v19

    .line 1469
    .local v11, "gain2":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3PrimaryPixel:Ljava/lang/String;

    .line 1470
    invoke-static {v11}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod3SecondaryPixel:Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1475
    if-eqz v9, :cond_2

    .line 1477
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_3

    .line 1478
    :catch_2
    move-exception v6

    .line 1479
    .local v6, "e":Ljava/io/IOException;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1478
    .end local v4    # "bufferReader":Ljava/io/BufferedReader;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v10    # "gain1":Ljava/lang/String;
    .end local v11    # "gain2":Ljava/lang/String;
    .end local v17    # "readStr":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v6

    .line 1479
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1478
    .end local v6    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 1479
    .restart local v6    # "e":Ljava/io/IOException;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 1475
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v18

    goto/16 :goto_4

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v18

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 1472
    :catch_5
    move-exception v6

    goto/16 :goto_2

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v6

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method private readMethod4LogFile()V
    .locals 20

    .prologue
    .line 1150
    const-string v13, "/data/validity/ValiditySnsrTest.log"

    .line 1151
    .local v13, "path":Ljava/lang/String;
    const-string v9, "No Term Signal (x10)"

    .line 1152
    .local v9, "mNoTermSignalHeader":Ljava/lang/String;
    const-string v12, "Term Signal (x10)"

    .line 1153
    .local v12, "mTermSignalHeader":Ljava/lang/String;
    const-string v10, "Term Percent Signal Loss (x10)"

    .line 1154
    .local v10, "mSignalLossHeader":Ljava/lang/String;
    const-string v11, "Region"

    .line 1156
    .local v11, "mSkipHeader":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1157
    .local v6, "file":Ljava/io/File;
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;

    .line 1158
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;

    .line 1159
    const-string v15, ""

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;

    .line 1160
    const/16 v15, 0xc

    new-array v15, v15, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    .line 1161
    const/16 v15, 0xc

    new-array v15, v15, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    .line 1162
    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    .line 1164
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1165
    const-string v15, "FingerPrintTest"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " file Exist"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1166
    const/4 v6, 0x0

    .line 1167
    const/4 v7, 0x0

    .line 1168
    .local v7, "fis":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 1171
    .local v2, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v13}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1172
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .local v8, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v15, Ljava/io/InputStreamReader;

    invoke-direct {v15, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v15}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1173
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .local v3, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v14, ""

    .line 1175
    .local v14, "readStr":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_21

    .line 1176
    invoke-virtual {v14, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-virtual {v14, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 1177
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;

    .line 1180
    :cond_1
    invoke-virtual {v14, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-virtual {v14, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 1181
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;

    .line 1184
    :cond_2
    invoke-virtual {v14, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-virtual {v14, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 1185
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;

    .line 1188
    :cond_3
    const-string v1, ""

    .local v1, "address":Ljava/lang/String;
    const-string v4, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1191
    .local v4, "data":Ljava/lang/String;
    :try_start_3
    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1192
    const-string v15, ","

    invoke-virtual {v14, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    aget-object v15, v15, v16

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 1202
    :goto_1
    :try_start_4
    const-string v15, "USL (Id=1800)"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1203
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    .line 1204
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "USL (Id=1800) : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1315
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v14    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v2, v3

    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 1316
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v5, "e":Ljava/io/IOException;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :goto_2
    :try_start_5
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1318
    if-eqz v7, :cond_4

    .line 1320
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1327
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    :cond_4
    :goto_3
    return-void

    .line 1198
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "readStr":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 1199
    .local v5, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_7
    invoke-virtual {v5}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 1318
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    .end local v5    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v14    # "readStr":Ljava/lang/String;
    :catchall_0
    move-exception v15

    move-object v2, v3

    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v7, :cond_5

    .line 1320
    :try_start_8
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 1323
    :cond_5
    :goto_5
    throw v15

    .line 1205
    .end local v2    # "bufferReader":Ljava/io/BufferedReader;
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "address":Ljava/lang/String;
    .restart local v3    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "data":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v14    # "readStr":Ljava/lang/String;
    :cond_6
    :try_start_9
    const-string v15, "LSL (Id=1800)"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1206
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v4, v15, v16

    .line 1207
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "LSL (Id=1800) : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1208
    :cond_7
    const-string v15, "USL (Id=1801)"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 1209
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v4, v15, v16

    .line 1210
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "USL (Id=1801) : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1211
    :cond_8
    const-string v15, "LSL (Id=1801)"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 1212
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 1213
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "LSL (Id=1801) : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1214
    :cond_9
    const-string v15, "13550"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 1216
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    .line 1217
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Signal Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1218
    :cond_a
    const-string v15, "13670"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 1220
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v4, v15, v16

    .line 1221
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Noise Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1222
    :cond_b
    const-string v15, "13790"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 1224
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v4, v15, v16

    .line 1225
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1226
    :cond_c
    const-string v15, "13880"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 1228
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 1229
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Pixel Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1230
    :cond_d
    const-string v15, "13890"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 1232
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x4

    aput-object v4, v15, v16

    .line 1233
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term CSTV Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1234
    :cond_e
    const-string v15, "14390"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 1236
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x5

    aput-object v4, v15, v16

    .line 1237
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1238
    :cond_f
    const-string v15, "14510"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 1240
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x6

    aput-object v4, v15, v16

    .line 1241
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Noise Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x6

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1242
    :cond_10
    const-string v15, "14630"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 1244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x7

    aput-object v4, v15, v16

    .line 1245
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x7

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1246
    :cond_11
    const-string v15, "14750"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 1248
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x8

    aput-object v4, v15, v16

    .line 1249
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Rate Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1250
    :cond_12
    const-string v15, "14840"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_13

    .line 1252
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0x9

    aput-object v4, v15, v16

    .line 1253
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Pixel Fail Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x9

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1254
    :cond_13
    const-string v15, "14850"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_14

    .line 1256
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0xa

    aput-object v4, v15, v16

    .line 1257
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss CSTC FAIL Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xa

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1258
    :cond_14
    const-string v15, "14870"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 1260
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    const/16 v16, 0xb

    aput-object v4, v15, v16

    .line 1261
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Cal SNR Pri : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1264
    :cond_15
    const-string v15, "15470"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 1266
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x0

    aput-object v4, v15, v16

    .line 1267
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Signal Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1268
    :cond_16
    const-string v15, "15590"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 1270
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x1

    aput-object v4, v15, v16

    .line 1271
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Noise Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1272
    :cond_17
    const-string v15, "15710"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_18

    .line 1274
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x2

    aput-object v4, v15, v16

    .line 1275
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x2

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1276
    :cond_18
    const-string v15, "15800"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_19

    .line 1278
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x3

    aput-object v4, v15, v16

    .line 1279
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term Pixel Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x3

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1280
    :cond_19
    const-string v15, "15810"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1a

    .line 1282
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x4

    aput-object v4, v15, v16

    .line 1283
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "No Term CSTV Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1284
    :cond_1a
    const-string v15, "16310"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1b

    .line 1286
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x5

    aput-object v4, v15, v16

    .line 1287
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x5

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1288
    :cond_1b
    const-string v15, "16430"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1c

    .line 1290
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x6

    aput-object v4, v15, v16

    .line 1291
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal Noise Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x6

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1292
    :cond_1c
    const-string v15, "16550"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1d

    .line 1294
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x7

    aput-object v4, v15, v16

    .line 1295
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Term Signal SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x7

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1296
    :cond_1d
    const-string v15, "16670"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1e

    .line 1298
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x8

    aput-object v4, v15, v16

    .line 1299
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Rate Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1300
    :cond_1e
    const-string v15, "16760"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1f

    .line 1302
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0x9

    aput-object v4, v15, v16

    .line 1303
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Pixel Fail Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x9

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1304
    :cond_1f
    const-string v15, "16770"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_20

    .line 1306
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0xa

    aput-object v4, v15, v16

    .line 1307
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss CSTC FAIL Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xa

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1308
    :cond_20
    const-string v15, "16790"

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 1310
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    const/16 v16, 0xb

    aput-object v4, v15, v16

    .line 1311
    const-string v15, "FingerPrintTest"

    const-string v16, "readMethod4LogFile"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Signal Loss Cal SNR Sec : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0xb

    aget-object v18, v18, v19

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1318
    .end local v1    # "address":Ljava/lang/String;
    .end local v4    # "data":Ljava/lang/String;
    :cond_21
    if-eqz v8, :cond_4

    .line 1320
    :try_start_a
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_3

    .line 1321
    :catch_2
    move-exception v5

    .line 1322
    .local v5, "e":Ljava/io/IOException;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1321
    .end local v3    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .end local v14    # "readStr":Ljava/lang/String;
    .restart local v2    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_3
    move-exception v5

    .line 1322
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_3

    .line 1321
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 1322
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-static {v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_5

    .line 1318
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v15

    goto/16 :goto_4

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v15

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 1315
    :catch_5
    move-exception v5

    goto/16 :goto_2

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v5

    move-object v7, v8

    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method private readNormalScanLogFile()V
    .locals 18

    .prologue
    .line 1074
    const-string v12, "/data/validity/ValiditySnsrTest.log"

    .line 1075
    .local v12, "path":Ljava/lang/String;
    const-string v1, "Start of Normal Scan Waveform Data "

    .line 1076
    .local v1, "Header":Ljava/lang/String;
    const-string v2, "Used Line Index "

    .line 1077
    .local v2, "Header2":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1079
    .local v3, "ReadCount":I
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1080
    .local v11, "lfile":Ljava/io/File;
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanSerialNumberString:Ljava/lang/String;

    .line 1081
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;

    .line 1082
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;

    .line 1083
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;

    .line 1084
    const-string v14, ""

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;

    .line 1086
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1087
    const-string v14, "FingerPrintTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " file Exist"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    const/4 v11, 0x0

    .line 1089
    const/4 v9, 0x0

    .line 1090
    .local v9, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 1093
    .local v5, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v12}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1094
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .local v10, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-direct {v14, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1096
    .end local v5    # "bufferReader":Ljava/io/BufferedReader;
    .local v6, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    const-string v13, ""

    .line 1098
    .local v13, "readStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 1099
    const-string v4, ""

    .local v4, "address":Ljava/lang/String;
    const-string v7, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1102
    .local v7, "data":Ljava/lang/String;
    :try_start_3
    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    aget-object v14, v14, v15

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 1103
    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    aget-object v14, v14, v15

    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    .line 1108
    :goto_0
    :try_start_4
    const-string v14, "2230"

    invoke-virtual {v14, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1110
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanSerialNumberString:Ljava/lang/String;

    .line 1111
    const-string v14, "FingerPrintTest"

    const-string v15, "readNormalScanLogFile"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Serial Number : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanSerialNumberString:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanSerialNumberString:Ljava/lang/String;

    iput-object v15, v14, Lcom/sec/android/app/hwmoduletest/FingerPrintGraph;->mSerialNumber:Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1131
    :cond_1
    :goto_1
    const/4 v14, 0x2

    if-ne v3, v14, :cond_0

    .line 1138
    .end local v4    # "address":Ljava/lang/String;
    .end local v7    # "data":Ljava/lang/String;
    :cond_2
    if-eqz v10, :cond_3

    .line 1140
    :try_start_5
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1147
    .end local v6    # "bufferReader":Ljava/io/BufferedReader;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "readStr":Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 1104
    .restart local v4    # "address":Ljava/lang/String;
    .restart local v6    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "readStr":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1105
    .local v8, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_6
    invoke-virtual {v8}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 1135
    .end local v4    # "address":Ljava/lang/String;
    .end local v7    # "data":Ljava/lang/String;
    .end local v8    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v13    # "readStr":Ljava/lang/String;
    :catch_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufferReader":Ljava/io/BufferedReader;
    move-object v9, v10

    .line 1136
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .local v8, "e":Ljava/io/IOException;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :goto_3
    :try_start_7
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1138
    if-eqz v9, :cond_3

    .line 1140
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_2

    .line 1141
    :catch_2
    move-exception v8

    .line 1142
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2

    .line 1113
    .end local v5    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "address":Ljava/lang/String;
    .restart local v6    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "readStr":Ljava/lang/String;
    :cond_4
    :try_start_9
    invoke-virtual {v13, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1114
    const-string v14, "FingerPrintTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Found!! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    .line 1116
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalScanAverageString:Ljava/lang/String;

    .line 1117
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevString:Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    .line 1138
    .end local v4    # "address":Ljava/lang/String;
    .end local v7    # "data":Ljava/lang/String;
    .end local v13    # "readStr":Ljava/lang/String;
    :catchall_0
    move-exception v14

    move-object v5, v6

    .end local v6    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufferReader":Ljava/io/BufferedReader;
    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :goto_4
    if-eqz v9, :cond_5

    .line 1140
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    .line 1143
    :cond_5
    :goto_5
    throw v14

    .line 1120
    .end local v5    # "bufferReader":Ljava/io/BufferedReader;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "address":Ljava/lang/String;
    .restart local v6    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v7    # "data":Ljava/lang/String;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v13    # "readStr":Ljava/lang/String;
    :cond_6
    :try_start_b
    invoke-virtual {v13, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 1121
    const-string v14, "FingerPrintTest"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Found!! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    if-nez v3, :cond_8

    .line 1123
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection1String:Ljava/lang/String;

    .line 1127
    :cond_7
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 1124
    :cond_8
    const/4 v14, 0x1

    if-ne v3, v14, :cond_7

    .line 1125
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNormalStdevSection2String:Ljava/lang/String;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    .line 1141
    .end local v4    # "address":Ljava/lang/String;
    .end local v7    # "data":Ljava/lang/String;
    :catch_3
    move-exception v8

    .line 1142
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_2

    .line 1141
    .end local v6    # "bufferReader":Ljava/io/BufferedReader;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v10    # "fis":Ljava/io/FileInputStream;
    .end local v13    # "readStr":Ljava/lang/String;
    .restart local v5    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    :catch_4
    move-exception v8

    .line 1142
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_5

    .line 1138
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v14

    goto :goto_4

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v14

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 1135
    :catch_5
    move-exception v8

    goto/16 :goto_3

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v10    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v8

    move-object v9, v10

    .end local v10    # "fis":Ljava/io/FileInputStream;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method private sendSensorStatusLog()V
    .locals 3

    .prologue
    .line 1017
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    const/16 v1, 0x166

    if-ne v0, v1, :cond_1

    .line 1018
    const-string v0, "FingerPrintTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sensor status : VCS_SENSOR_STATUS_INITIALISING ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    :cond_0
    :goto_0
    return-void

    .line 1019
    :cond_1
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    const/16 v1, 0x168

    if-ne v0, v1, :cond_2

    .line 1020
    const-string v0, "FingerPrintTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sensor status : VCS_SENSOR_STATUS_OUT_OF_ORDER ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1021
    :cond_2
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_3

    .line 1022
    const-string v0, "FingerPrintTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sensor status : VCS_SENSOR_STATUS_WORKING ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1023
    :cond_3
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    if-nez v0, :cond_0

    .line 1024
    const-string v0, "FingerPrintTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sensor status : VCS_SENSOR_STATUS_OK ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x400

    .line 458
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 459
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->requestWindowFeature(I)Z

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 463
    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->setContentView(I)V

    .line 465
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->initLayout()V

    .line 466
    const-string v0, "FingerPrintTest"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    new-instance v0, Lcom/validity/fingerprint/Fingerprint;

    invoke-direct {v0, p0}, Lcom/validity/fingerprint/Fingerprint;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v0, p0}, Lcom/validity/fingerprint/Fingerprint;->registerListener(Lcom/validity/fingerprint/FingerprintCore$EventListener;)I

    .line 470
    const-string v0, "FingerPrintTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v2}, Lcom/validity/fingerprint/Fingerprint;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    .line 473
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    .line 474
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 1500
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    if-eqz v1, :cond_0

    .line 1501
    const-string v1, "FingerPrintTest"

    const-string v2, "onDestroy() - mFingerprint.cleanUp() "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1503
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V

    .line 1504
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v1}, Lcom/validity/fingerprint/Fingerprint;->cancel()I

    .line 1505
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    invoke-virtual {v1}, Lcom/validity/fingerprint/Fingerprint;->cleanUp()I

    .line 1506
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    .line 1507
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1512
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 1513
    return-void

    .line 1508
    :catch_0
    move-exception v0

    .line 1509
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEvent(Lcom/validity/fingerprint/FingerprintEvent;)V
    .locals 10
    .param p1, "event"    # Lcom/validity/fingerprint/FingerprintEvent;

    .prologue
    const-wide/16 v8, 0x1f4

    const/16 v7, 0x69

    const/16 v6, 0x68

    const/16 v5, 0x66

    const/16 v4, 0xb

    .line 480
    if-eqz p1, :cond_0

    .line 481
    const-string v1, "FingerPrintTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onEventsCB(): Event Id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iget v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventId:I

    sparse-switch v1, :sswitch_data_0

    .line 755
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : UNKNOWN_EVT_MESSAGE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 486
    :sswitch_0
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_DATALOG_DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 488
    const-string v0, "\n*********SNR Data******** "

    .line 489
    .local v0, "SNRData":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/SensorTestSNRInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getSNRInfoString(Lcom/validity/fingerprint/SensorTestSNRInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 490
    const-string v1, "FingerPrintTest"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 492
    .end local v0    # "SNRData":Ljava/lang/String;
    :cond_1
    const-string v1, "FingerPrintTest"

    const-string v2, "event.eventData == null!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 496
    :sswitch_1
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_PUT_STIMULUS_ON_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 499
    :sswitch_2
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_PUT_TERM_BLOCK_ON_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    const-string v1, "METHOD4"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 501
    iput v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    .line 502
    const-string v1, "Put stimulus on the sensor\nand press OK or HomeButton"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PopupMessage:Ljava/lang/String;

    .line 507
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mShowPutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 504
    :cond_2
    iput v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    .line 505
    const-string v1, "No term data scan finished!\n\nFor scan term data,\nPut stimulus on the sensor\npress OK or Homebutton"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PopupMessage:Ljava/lang/String;

    goto :goto_1

    .line 511
    :sswitch_3
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_PUT_STON_SSS_ON_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iput v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    .line 513
    const-string v1, "Put stimulus on the sensor\npress OK or Homebutton"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PopupMessage:Ljava/lang/String;

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mShowPutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 517
    :sswitch_4
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_REMOVE_STIMULUS_FROM_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 520
    :sswitch_5
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_REMOVE_TERM_BLOCK_FROM_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    goto/16 :goto_0

    .line 524
    :sswitch_6
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_REMOVE_STON_SSS_FROM_SENSOR"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    invoke-direct {p0, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    goto/16 :goto_0

    .line 528
    :sswitch_7
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_RESET_AFTER_TEST_RES"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 531
    :sswitch_8
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_3

    .line 532
    const-string v1, "Normal data scan finished"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 534
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->readNormalScanLogFile()V

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateNormalDataGraphRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 572
    :goto_2
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_SCRIPT_END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 536
    :cond_3
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    if-ne v1, v4, :cond_7

    .line 537
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    if-ne v1, v5, :cond_5

    .line 538
    const-string v1, "MRM data scan finished"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 540
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->readMRMLogFile()V

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMRMDataGraphRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 554
    :cond_4
    :goto_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    goto :goto_2

    .line 542
    :cond_5
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    if-ne v1, v6, :cond_6

    .line 543
    const-string v1, "SNR Test data scan finished"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 544
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 545
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->readMethod3LogFile()V

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMethod3DataGraphRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 547
    :cond_6
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_PreviousOperation:I

    if-ne v1, v7, :cond_4

    .line 549
    const-string v1, "SNR Test data scan finished"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 550
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->readMethod4LogFile()V

    .line 552
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMethod4DataGraphRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 555
    :cond_7
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    if-ne v1, v5, :cond_8

    .line 556
    const-string v1, "MRM data scan canceled\nDue to no action"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 557
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 559
    :cond_8
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    if-ne v1, v6, :cond_9

    .line 560
    const-string v1, "SNR Test data scan canceled\nDue to no action"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 562
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 563
    :cond_9
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_CurrentOperation:I

    if-ne v1, v7, :cond_a

    .line 564
    const-string v1, "Method4 Test data scan canceled\nDue to no action"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 565
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 566
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 568
    :cond_a
    const-string v1, "Script end"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateResultMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 575
    :sswitch_9
    const-string v1, "Script start\nPlease wait...\nDon\'t exit until finish\n\nIf not response in 5 SEC,\nPlease restart test"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 576
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 577
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_SCRIPT_START"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 580
    :sswitch_a
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_SECTION_END"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 583
    :sswitch_b
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_SECTION_START"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 586
    :sswitch_c
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SNSR_TEST_SNR_DATA"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 588
    const-string v0, "\n*********SNR Data******** "

    .line 589
    .restart local v0    # "SNRData":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/SensorTestSNRInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->getSNRInfoString(Lcom/validity/fingerprint/SensorTestSNRInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 590
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 591
    const-string v1, "FingerPrintTest"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 595
    .end local v0    # "SNRData":Ljava/lang/String;
    :sswitch_d
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ALL_SENSORS_INITIALIZED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 598
    :sswitch_e
    const-string v1, "FingerPrint captured"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 600
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_EIV_FINGERPRINT_CAPTURED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/FingerprintBitmap;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprintBitmap:Lcom/validity/fingerprint/FingerprintBitmap;

    if-nez v1, :cond_b

    .line 604
    const-string v1, "FingerPrintTest"

    const-string v2, "Invalid FingerprintBitmap object"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 607
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateImageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 608
    const-string v1, "FingerPrintTest"

    const-string v2, "FingerprintBitmap object received"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    goto/16 :goto_0

    .line 613
    :sswitch_f
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ENROLL_CAPTURE_STATUS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 616
    :sswitch_10
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ENROLL_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 619
    :sswitch_11
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ENROLL_FAILED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 622
    :sswitch_12
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ENROLL_NEXT_CAPTURE_START"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 625
    :sswitch_13
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_ENROLL_SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 628
    :sswitch_14
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_FINGER_DETECTED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 631
    :sswitch_15
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_FINGER_REMOVED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 634
    :sswitch_16
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_FINGER_SETTLED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 637
    :sswitch_17
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_GESTURE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 640
    :sswitch_18
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_IDENTIFY_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 643
    :sswitch_19
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_IDENTIFY_FAILED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 646
    :sswitch_1a
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_IDENTIFY_SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 649
    :sswitch_1b
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_DETECTED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 652
    :sswitch_1c
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_FAILED_INITIALIZATION"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 655
    :sswitch_1d
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_COMPLETE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 658
    :sswitch_1e
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_FAILED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 661
    :sswitch_1f
    const-string v1, "Swipe your finger to capture"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 662
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 663
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_FINGERPRINT_CAPTURE_START"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 666
    :sswitch_20
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_FINGERPRINT_FAILED_SWIPE_RETRY"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 669
    :sswitch_21
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_INFO"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-object v1, p1, Lcom/validity/fingerprint/FingerprintEvent;->eventData:Ljava/lang/Object;

    check-cast v1, Lcom/validity/fingerprint/SensorInfo;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    .line 671
    const-string v1, "FwVersion : "

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget-object v2, v2, Lcom/validity/fingerprint/SensorInfo;->fwVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 673
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nFlex Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 674
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x7f9

    if-ne v1, v2, :cond_c

    .line 675
    const-string v1, "60"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    .line 684
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget-object v1, v1, Lcom/validity/fingerprint/SensorInfo;->fwVersion:Ljava/lang/String;

    const-string v2, "04.52"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 685
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I

    .line 690
    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 691
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nProductId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 692
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->productId:I

    const/16 v2, 0xd0

    if-ne v1, v2, :cond_10

    .line 693
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    .line 700
    :goto_6
    const-string v1, "Sensor Infomation"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_Message:Ljava/lang/String;

    .line 701
    const-string v1, "NORMALDATA"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$11;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 676
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x52

    if-ne v1, v2, :cond_d

    .line 677
    const-string v1, "65"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    goto/16 :goto_4

    .line 678
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    const/16 v2, 0x59

    if-ne v1, v2, :cond_e

    .line 679
    const-string v1, "68A"

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    goto/16 :goto_4

    .line 681
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->flexId:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mVersion:Ljava/lang/String;

    goto/16 :goto_4

    .line 687
    :cond_f
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFwVersion:I

    goto/16 :goto_5

    .line 694
    :cond_10
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v1, v1, Lcom/validity/fingerprint/SensorInfo;->productId:I

    const/16 v2, 0xd1

    if-ne v1, v2, :cond_11

    .line 695
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    goto :goto_6

    .line 697
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mSensorInfo:Lcom/validity/fingerprint/SensorInfo;

    iget v2, v2, Lcom/validity/fingerprint/SensorInfo;->productId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;

    goto :goto_6

    .line 708
    :cond_12
    const-string v1, "METHOD3"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 709
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$12;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 715
    :cond_13
    const-string v1, "METHOD4"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$13;-><init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 722
    :cond_14
    const-string v1, "SENSORINFO"

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mTest:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 723
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mUpdateResultMessageRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 728
    :sswitch_22
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_RAW_FINGERPRINT_CAPTURE_COMPLETE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 731
    :sswitch_23
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_READY_FOR_USE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 734
    :sswitch_24
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SENSOR_REMOVED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 737
    :sswitch_25
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SET_IR_FLAGS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 740
    :sswitch_26
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SWIPE_DIRECTION"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 743
    :sswitch_27
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_SWIPE_SPEED_UPDATE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 746
    :sswitch_28
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_VERIFY_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 749
    :sswitch_29
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_VERIFY_FAILED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 752
    :sswitch_2a
    const-string v1, "FingerPrintTest"

    const-string v2, "Event is : VCS_EVT_VERIFY_SUCCESS"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 483
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_24
        0x2 -> :sswitch_1b
        0x3 -> :sswitch_23
        0x4 -> :sswitch_1c
        0x5 -> :sswitch_1d
        0x6 -> :sswitch_22
        0x7 -> :sswitch_1e
        0x8 -> :sswitch_1f
        0x9 -> :sswitch_d
        0xa -> :sswitch_20
        0xb -> :sswitch_14
        0xd -> :sswitch_10
        0xe -> :sswitch_28
        0xf -> :sswitch_18
        0x10 -> :sswitch_12
        0x11 -> :sswitch_e
        0x14 -> :sswitch_15
        0x20 -> :sswitch_f
        0x21 -> :sswitch_16
        0x25 -> :sswitch_25
        0x29 -> :sswitch_26
        0x2a -> :sswitch_27
        0x1a5 -> :sswitch_13
        0x1a6 -> :sswitch_2a
        0x1a7 -> :sswitch_1a
        0x1a8 -> :sswitch_11
        0x1a9 -> :sswitch_29
        0x1aa -> :sswitch_19
        0x1ab -> :sswitch_17
        0x1ac -> :sswitch_21
        0x7d1 -> :sswitch_9
        0x7d2 -> :sswitch_b
        0x7d3 -> :sswitch_a
        0x7d4 -> :sswitch_8
        0x7da -> :sswitch_0
        0x7e6 -> :sswitch_7
        0x7e7 -> :sswitch_2
        0x7e8 -> :sswitch_5
        0x7e9 -> :sswitch_1
        0x7ea -> :sswitch_4
        0x7eb -> :sswitch_c
        0x7ec -> :sswitch_3
        0x7ed -> :sswitch_6
    .end sparse-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 139
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const-string v0, "FingerPrintTest"

    const-string v1, "onKeyDown() - Homekey pressed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const-string v0, "FingerPrintTest"

    const-string v1, "onKeyDown() - excuteOperation(OPERATION_NOTIFY_PLACED_AND_WAIT) "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->excuteOperation(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mClosePutFingerDialogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 147
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 1487
    const-string v0, "FingerPrintTest"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1488
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 1489
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1490
    const-string v0, "FingerPrintTest"

    const-string v1, "onPause() - mFingerprint.notify() "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerprint:Lcom/validity/fingerprint/Fingerprint;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/validity/fingerprint/Fingerprint;->notify(ILjava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_result:I

    .line 1495
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v6, 0xff00

    const/4 v7, 0x0

    .line 360
    const/4 v2, 0x0

    .line 361
    .local v2, "x":F
    const/4 v3, 0x0

    .line 362
    .local v3, "y":F
    const/4 v1, 0x0

    .line 363
    .local v1, "index":I
    const/4 v0, 0x0

    .line 365
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 367
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_1

    .line 368
    and-int v4, v0, v6

    shr-int/lit8 v1, v4, 0x8

    .line 369
    const-string v4, "FingerPrintTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_POINTER_DOWN index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 371
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 373
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getX()F

    move-result v4

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getY()F

    move-result v4

    cmpl-float v4, v3, v4

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v3, v4

    if-gez v4, :cond_0

    .line 377
    const-string v4, "FingerPrintTest"

    const-string v5, "Event.ACTION_POINTER_2_DOWN pos is inside OK button "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setPressed(Z)V

    .line 401
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    return v4

    .line 381
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v4

    const/4 v5, 0x6

    if-ne v4, v5, :cond_0

    .line 382
    and-int v4, v0, v6

    shr-int/lit8 v1, v4, 0x8

    .line 383
    const-string v4, "FingerPrintTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ACTION_POINTER_UP index : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 385
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 387
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getX()F

    move-result v4

    cmpl-float v4, v2, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getWidth()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v2, v4

    if-gez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getY()F

    move-result v4

    cmpl-float v4, v3, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v5}, Landroid/widget/Button;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    cmpg-float v4, v3, v4

    if-gez v4, :cond_2

    .line 391
    const-string v4, "FingerPrintTest"

    const-string v5, "Event.ACTION_POINTER_2_UP pos is inside OK button "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setPressed(Z)V

    .line 393
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->callOnClick()Z

    goto :goto_0

    .line 395
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->isPressed()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 396
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_gotermbutton:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setPressed(Z)V

    goto/16 :goto_0
.end method
