.class public Lcom/sec/android/app/hwmoduletest/AccImageTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "AccImageTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;
    }
.end annotation


# instance fields
.field private final MSG_UPDATE_SENSOR_UI:B

.field private final TIMER_TASK_PERIOD:I

.field mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

.field mAccelerometerSensor:Landroid/hardware/Sensor;

.field private mAngleText:Landroid/widget/TextView;

.field private mAutoRotateState:I

.field private mAutoRotateState2:I

.field private mBackground:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mRawDataText:Landroid/widget/TextView;

.field mSensorManager:Landroid/hardware/SensorManager;

.field mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "AccImageTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 25
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->TIMER_TASK_PERIOD:I

    .line 30
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->MSG_UPDATE_SENSOR_UI:B

    .line 44
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$1;-><init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mHandler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mRawDataText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAngleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->setContentView(I)V

    .line 64
    const v0, 0x7f0b000c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mBackground:Landroid/view/View;

    .line 65
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mRawDataText:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0b000e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAngleText:Landroid/widget/TextView;

    .line 68
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mTimer:Ljava/util/Timer;

    .line 69
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAccelerometerSensor:Landroid/hardware/Sensor;

    .line 71
    new-instance v0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;Lcom/sec/android/app/hwmoduletest/AccImageTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x64

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mBackground:Landroid/view/View;

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 74
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 115
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->access$600(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAutoRotateState:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 105
    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation_second"

    iget v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAutoRotateState2:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 78
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAccelerometerSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAcceSensorTask:Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    # invokes: Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->access$500(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAutoRotateState:I

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 88
    const-string v0, "SUPPORT_DUAL_LCD_FOLDER"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation_second"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest;->mAutoRotateState2:I

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation_second"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 93
    :cond_0
    return-void
.end method
