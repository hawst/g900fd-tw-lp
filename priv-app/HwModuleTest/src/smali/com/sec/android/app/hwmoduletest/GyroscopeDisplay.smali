.class public Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;,
        Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;,
        Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;,
        Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;
    }
.end annotation


# static fields
.field private static final GYRO_DPS_VALUE_2000:Ljava/lang/String; = "2000"

.field private static final GYRO_DPS_VALUE_250:Ljava/lang/String; = "250"

.field private static final GYRO_DPS_VALUE_500:Ljava/lang/String; = "500"


# instance fields
.field private final TEXT_SCALING_COOR:F

.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

.field private mCount:I

.field private mGyroSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mX:F

.field private mY:F

.field private mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "GyroscopeDisplay"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 35
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->TEXT_SCALING_COOR:F

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mValueList:Ljava/util/List;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mCount:I

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mHandler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->writeDpsValue(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;FFF)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "x3"    # F

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->getValueFromSensor(FFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->startGyroDisplay()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$304(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mCount:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mX:F

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mY:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mZ:F

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private displayDialog()V
    .locals 3

    .prologue
    .line 104
    const v1, 0x7f08006e

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;->newInstance(I)Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;

    move-result-object v0

    .line 105
    .local v0, "dialog":Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$AlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method private displayValueFromSensor()V
    .locals 6

    .prologue
    .line 151
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 152
    .local v0, "mTimer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x7d0

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 167
    return-void
.end method

.method private getValueFromSensor(FFF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mX:F

    .line 145
    iput p2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mY:F

    .line 146
    iput p3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mZ:F

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getValueFromSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "x="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mX:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mY:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", z="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mZ:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

    const v1, 0x7f030034

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

    .line 171
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mListView:Landroid/widget/ListView;

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mAdaptor:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 173
    return-void
.end method

.method private startGyroDisplay()V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mGyroSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->displayValueFromSensor()V

    .line 91
    return-void
.end method

.method private writeDpsValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "dpsValue"    # Ljava/lang/String;

    .prologue
    .line 94
    const-string v0, "GYRO_SENSOR_DPS_SELECT"

    invoke-static {v0, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 95
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->setContentView(I)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->initTextView()V

    .line 60
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;

    .line 61
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mGyroSensor:Landroid/hardware/Sensor;

    .line 65
    const-string v0, "STMICRO_SMARTPHONE"

    const-string v1, "SENSOR_NAME_GYROSCOPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "STMICRO_TABLET"

    const-string v1, "SENSOR_NAME_GYROSCOPE"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->displayDialog()V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->startGyroDisplay()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 99
    const-string v0, "0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeDisplay;->writeDpsValue(Ljava/lang/String;)V

    .line 100
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 101
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 83
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 84
    return-void
.end method
