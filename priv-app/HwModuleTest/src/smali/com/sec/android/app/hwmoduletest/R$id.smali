.class public final Lcom/sec/android/app/hwmoduletest/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ADC:I = 0x7f0b01af

.field public static final Auto:I = 0x7f0b02e3

.field public static final Button01:I = 0x7f0b0053

.field public static final Button02:I = 0x7f0b0057

.field public static final Button03:I = 0x7f0b0058

.field public static final Button04:I = 0x7f0b0059

.field public static final Button05:I = 0x7f0b005a

.field public static final EXIT:I = 0x7f0b018e

.field public static final Gesture_Countdelta:I = 0x7f0b009d

.field public static final Gesture_Crosstalk:I = 0x7f0b009c

.field public static final Gesture_Data:I = 0x7f0b009a

.field public static final Gesture_Zmean:I = 0x7f0b009b

.field public static final Gesture_count_direction:I = 0x7f0b00a5

.field public static final Gesture_count_title:I = 0x7f0b00a4

.field public static final Gesture_vendor:I = 0x7f0b0099

.field public static final Headset_on:I = 0x7f0b018b

.field public static final LinearLayout01:I = 0x7f0b006d

.field public static final LinearLayout02:I = 0x7f0b006e

.field public static final MCUchip:I = 0x7f0b022c

.field public static final OFF:I = 0x7f0b018d

.field public static final Offset:I = 0x7f0b01c0

.field public static final Offset_btn:I = 0x7f0b01b8

.field public static final Receiver_Headset_on:I = 0x7f0b018c

.field public static final Receiver_on:I = 0x7f0b018a

.field public static final Reset_btn:I = 0x7f0b01b9

.field public static final ScrollView:I = 0x7f0b0112

.field public static final SeekBar01:I = 0x7f0b0056

.field public static final Speaker_on:I = 0x7f0b0189

.field public static final TspNodeMax:I = 0x7f0b02a6

.field public static final TspNodeMin:I = 0x7f0b02a5

.field public static final WorkView:I = 0x7f0b01b7

.field public static final acc_imageview:I = 0x7f0b000f

.field public static final acc_lpf_onoff:I = 0x7f0b000b

.field public static final acc_sensor_graph:I = 0x7f0b000a

.field public static final acce_separator:I = 0x7f0b01d1

.field public static final acce_title:I = 0x7f0b01d0

.field public static final accelerometer_sen:I = 0x7f0b008b

.field public static final adc_x:I = 0x7f0b0173

.field public static final adc_y:I = 0x7f0b0174

.field public static final adc_z:I = 0x7f0b0175

.field public static final additional:I = 0x7f0b0045

.field public static final ambi_temp:I = 0x7f0b0138

.field public static final angle:I = 0x7f0b000e

.field public static final ap:I = 0x7f0b0184

.field public static final auto_disable:I = 0x7f0b0029

.field public static final auto_enable:I = 0x7f0b0028

.field public static final auto_mode:I = 0x7f0b01a5

.field public static final autoanswer:I = 0x7f0b0027

.field public static final avg_data_value:I = 0x7f0b0090

.field public static final back_key:I = 0x7f0b0293

.field public static final back_title:I = 0x7f0b0279

.field public static final background:I = 0x7f0b000c

.field public static final background_layout:I = 0x7f0b015b

.field public static final background_layout_back:I = 0x7f0b00b4

.field public static final background_layout_side:I = 0x7f0b00b1

.field public static final backkey_max:I = 0x7f0b02a1

.field public static final backkey_min:I = 0x7f0b029c

.field public static final backkey_str:I = 0x7f0b0297

.field public static final backview:I = 0x7f0b01ae

.field public static final barcodefw:I = 0x7f0b002b

.field public static final barcodetitle:I = 0x7f0b002a

.field public static final barom_separator:I = 0x7f0b01e4

.field public static final barom_title:I = 0x7f0b01e3

.field public static final barometer_altitude:I = 0x7f0b0030

.field public static final barometer_pressure:I = 0x7f0b002f

.field public static final barometer_temperature:I = 0x7f0b002d

.field public static final barometer_temperature_result:I = 0x7f0b002e

.field public static final barometer_title:I = 0x7f0b002c

.field public static final binfirmversion:I = 0x7f0b022b

.field public static final bist_str:I = 0x7f0b00c9

.field public static final bluetooth_test_text:I = 0x7f0b0052

.field public static final bmz:I = 0x7f0b0181

.field public static final body_temp:I = 0x7f0b014d

.field public static final body_temp_title:I = 0x7f0b013d

.field public static final bodytemp_sensor_graph:I = 0x7f0b0152

.field public static final bottom:I = 0x7f0b0000

.field public static final btnGroup:I = 0x7f0b0013

.field public static final btn_acc_graph:I = 0x7f0b01d4

.field public static final btn_acc_image_test:I = 0x7f0b01d3

.field public static final btn_accuracy:I = 0x7f0b006b

.field public static final btn_back:I = 0x7f0b0072

.field public static final btn_barom_altitude:I = 0x7f0b01e6

.field public static final btn_barom_selftest:I = 0x7f0b01e9

.field public static final btn_calibration:I = 0x7f0b0008

.field public static final btn_calibration_erase:I = 0x7f0b0009

.field public static final btn_ecg_graph:I = 0x7f0b011f

.field public static final btn_ecg_test:I = 0x7f0b011e

.field public static final btn_fingerprint_method3:I = 0x7f0b007e

.field public static final btn_fingerprint_method4:I = 0x7f0b007f

.field public static final btn_fingerprint_normalscan:I = 0x7f0b007d

.field public static final btn_fingerprint_sensorinfo:I = 0x7f0b0080

.field public static final btn_gesture_crosstalk:I = 0x7f0b020b

.field public static final btn_gesture_direction:I = 0x7f0b020c

.field public static final btn_glucose_check:I = 0x7f0b011b

.field public static final btn_glucose_selftest:I = 0x7f0b011a

.field public static final btn_glucose_start:I = 0x7f0b00af

.field public static final btn_glucose_stop:I = 0x7f0b00b0

.field public static final btn_gyro_display:I = 0x7f0b01f6

.field public static final btn_gyro_graph:I = 0x7f0b01f7

.field public static final btn_gyro_selftest:I = 0x7f0b01f5

.field public static final btn_hrm_eoltest:I = 0x7f0b021b

.field public static final btn_hrm_start:I = 0x7f0b021a

.field public static final btn_light_test:I = 0x7f0b01f0

.field public static final btn_linearity:I = 0x7f0b006c

.field public static final btn_magn_power_noise_test:I = 0x7f0b0207

.field public static final btn_magn_self_test:I = 0x7f0b0206

.field public static final btn_ois_gyro_selftest:I = 0x7f0b01f9

.field public static final btn_sensorhub_test:I = 0x7f0b01cf

.field public static final btn_temp_humid:I = 0x7f0b0213

.field public static final btn_temphumid_display:I = 0x7f0b0214

.field public static final btn_temphumid_graph:I = 0x7f0b0215

.field public static final btn_temphumid_thermistor:I = 0x7f0b0216

.field public static final btn_thermo_display:I = 0x7f0b0117

.field public static final btn_thermo_display_off:I = 0x7f0b0133

.field public static final btn_thermo_display_on:I = 0x7f0b0134

.field public static final btn_thermo_test:I = 0x7f0b0116

.field public static final btn_thermo_test_onetest:I = 0x7f0b014a

.field public static final btn_thermo_test_onoff:I = 0x7f0b014b

.field public static final btn_uv_display:I = 0x7f0b0224

.field public static final btn_uv_ic_check:I = 0x7f0b0225

.field public static final btn_uv_start:I = 0x7f0b0223

.field public static final build_time:I = 0x7f0b005b

.field public static final buttonStart:I = 0x7f0b0014

.field public static final buttonStop:I = 0x7f0b0015

.field public static final button_cpchange:I = 0x7f0b01aa

.field public static final button_dualmodeoff:I = 0x7f0b01a9

.field public static final button_dualmodeon:I = 0x7f0b01a8

.field public static final button_exit:I = 0x7f0b0276

.field public static final button_fwupdate:I = 0x7f0b0275

.field public static final button_minus:I = 0x7f0b02e7

.field public static final button_off:I = 0x7f0b0131

.field public static final button_on:I = 0x7f0b0130

.field public static final button_plus:I = 0x7f0b02e8

.field public static final button_savereset:I = 0x7f0b019e

.field public static final button_usbsettings:I = 0x7f0b02c5

.field public static final cabc_off:I = 0x7f0b005e

.field public static final cabc_on:I = 0x7f0b005d

.field public static final cal_data_check:I = 0x7f0b008a

.field public static final calibration_str:I = 0x7f0b00ee

.field public static final camera_image_preview:I = 0x7f0b005f

.field public static final cancel_button:I = 0x7f0b02cf

.field public static final changelist:I = 0x7f0b005c

.field public static final channel_down:I = 0x7f0b012b

.field public static final channel_title:I = 0x7f0b0129

.field public static final channel_up:I = 0x7f0b012a

.field public static final checkbox_logging:I = 0x7f0b0019

.field public static final circle:I = 0x7f0b0032

.field public static final clr_button:I = 0x7f0b0127

.field public static final colorstate:I = 0x7f0b0155

.field public static final config_tsp_fw_version:I = 0x7f0b0259

.field public static final config_tsp_fw_version_str:I = 0x7f0b0258

.field public static final container:I = 0x7f0b0124

.field public static final control_button:I = 0x7f0b0034

.field public static final copy_to_sdcard:I = 0x7f0b023a

.field public static final count:I = 0x7f0b0230

.field public static final counter:I = 0x7f0b0128

.field public static final cp:I = 0x7f0b0185

.field public static final cp_acce_separator:I = 0x7f0b01d7

.field public static final cp_acce_title:I = 0x7f0b01d6

.field public static final cp_ril_log:I = 0x7f0b023f

.field public static final cp_tv_acc_value:I = 0x7f0b01d8

.field public static final cpu_version:I = 0x7f0b0060

.field public static final csc:I = 0x7f0b0186

.field public static final csfb_mode:I = 0x7f0b01a7

.field public static final current_Offset:I = 0x7f0b01b0

.field public static final current_data:I = 0x7f0b0033

.field public static final current_fuel_gauge:I = 0x7f0b0084

.field public static final current_high_Threshold:I = 0x7f0b01b2

.field public static final current_low_Threshold:I = 0x7f0b01b1

.field public static final dac_x:I = 0x7f0b016f

.field public static final dac_y:I = 0x7f0b0170

.field public static final dac_z:I = 0x7f0b0171

.field public static final data_list:I = 0x7f0b003a

.field public static final data_value:I = 0x7f0b008c

.field public static final dbg_state:I = 0x7f0b023b

.field public static final delete_dump:I = 0x7f0b0236

.field public static final diff_X:I = 0x7f0b00ff

.field public static final diff_Y:I = 0x7f0b0100

.field public static final diff_x_str:I = 0x7f0b00e7

.field public static final diff_y_str:I = 0x7f0b00e8

.field public static final diff_z_str:I = 0x7f0b00e9

.field public static final direction_data:I = 0x7f0b008e

.field public static final display_item_1:I = 0x7f0b001e

.field public static final display_item_2:I = 0x7f0b001b

.field public static final display_item_3:I = 0x7f0b001f

.field public static final display_item_4:I = 0x7f0b0020

.field public static final display_item_5:I = 0x7f0b0021

.field public static final display_item_6:I = 0x7f0b0022

.field public static final display_item_7:I = 0x7f0b0023

.field public static final display_item_8:I = 0x7f0b0024

.field public static final display_item_9:I = 0x7f0b0025

.field public static final dm:I = 0x7f0b01a2

.field public static final dm_modem_adb_mode:I = 0x7f0b02cd

.field public static final dps_X:I = 0x7f0b00fb

.field public static final dps_Y:I = 0x7f0b00fc

.field public static final dummy:I = 0x7f0b0070

.field public static final ear_on:I = 0x7f0b0162

.field public static final ecg_sensor_graph:I = 0x7f0b0068

.field public static final ecg_separator:I = 0x7f0b011d

.field public static final ecg_title:I = 0x7f0b011c

.field public static final edittext_threshold_high:I = 0x7f0b01b5

.field public static final edittext_threshold_low:I = 0x7f0b01b4

.field public static final et_barom_altitude:I = 0x7f0b01e7

.field public static final exit:I = 0x7f0b0164

.field public static final exit_button:I = 0x7f0b0066

.field public static final failhist_msg:I = 0x7f0b0071

.field public static final final_result:I = 0x7f0b0067

.field public static final fingerprint_graph:I = 0x7f0b0077

.field public static final fingerprint_image:I = 0x7f0b0076

.field public static final fingerprint_lib_version:I = 0x7f0b0081

.field public static final fingerprint_message1:I = 0x7f0b0074

.field public static final fingerprint_message2:I = 0x7f0b0075

.field public static final fingerprint_method3_graph:I = 0x7f0b0079

.field public static final fingerprint_method4_graph:I = 0x7f0b007a

.field public static final fingerprint_mrm_graph:I = 0x7f0b0078

.field public static final fingerprint_separator:I = 0x7f0b007c

.field public static final fingerprint_test_title:I = 0x7f0b007b

.field public static final fingerprint_title:I = 0x7f0b0073

.field public static final fragment_fingerprint_bottom:I = 0x7f0b0220

.field public static final freq_100:I = 0x7f0b0165

.field public static final freq_200:I = 0x7f0b0166

.field public static final freq_300:I = 0x7f0b0167

.field public static final freq_500:I = 0x7f0b01c6

.field public static final frequency_dc_ir:I = 0x7f0b02b8

.field public static final frequency_dc_red:I = 0x7f0b02b9

.field public static final frequency_dc_result:I = 0x7f0b02ba

.field public static final frequency_noise_ir:I = 0x7f0b02bb

.field public static final frequency_noise_red:I = 0x7f0b02bc

.field public static final frequency_noise_result:I = 0x7f0b02bd

.field public static final frequency_sample_no_ir:I = 0x7f0b02b5

.field public static final frequency_sample_no_red:I = 0x7f0b02b6

.field public static final frequency_sample_result:I = 0x7f0b02b7

.field public static final fta_swver:I = 0x7f0b0083

.field public static final fuel_gauge_read:I = 0x7f0b0085

.field public static final fuel_gauge_write1:I = 0x7f0b0087

.field public static final fuel_gauge_write2:I = 0x7f0b0089

.field public static final func:I = 0x7f0b02be

.field public static final funcBtn1:I = 0x7f0b0046

.field public static final funcBtn10:I = 0x7f0b004f

.field public static final funcBtn11:I = 0x7f0b0050

.field public static final funcBtn12:I = 0x7f0b0051

.field public static final funcBtn2:I = 0x7f0b0047

.field public static final funcBtn3:I = 0x7f0b0048

.field public static final funcBtn4:I = 0x7f0b0049

.field public static final funcBtn5:I = 0x7f0b004a

.field public static final funcBtn6:I = 0x7f0b004b

.field public static final funcBtn7:I = 0x7f0b004c

.field public static final funcBtn8:I = 0x7f0b004d

.field public static final funcBtn9:I = 0x7f0b004e

.field public static final func_disable:I = 0x7f0b02c0

.field public static final func_enable:I = 0x7f0b02bf

.field public static final gesture_count_direction:I = 0x7f0b001d

.field public static final gesture_count_title:I = 0x7f0b001c

.field public static final gesture_separator:I = 0x7f0b020a

.field public static final gesture_test_count_delta:I = 0x7f0b0091

.field public static final gesture_test_mode1:I = 0x7f0b009e

.field public static final gesture_test_mode1_fragment:I = 0x7f0b009f

.field public static final gesture_test_mode2:I = 0x7f0b00a3

.field public static final gesture_title:I = 0x7f0b0209

.field public static final gesture_value:I = 0x7f0b008d

.field public static final geture_sensor_test_mode1_list:I = 0x7f0b00a2

.field public static final glucose_separator:I = 0x7f0b0119

.field public static final glucose_title:I = 0x7f0b0118

.field public static final grip_str_sen1:I = 0x7f0b00b2

.field public static final grip_str_sen2:I = 0x7f0b00b5

.field public static final gripsensor_cal_btn:I = 0x7f0b00bf

.field public static final gripsensor_caldata:I = 0x7f0b00be

.field public static final gripsensor_caldata_str:I = 0x7f0b00bd

.field public static final gripsensor_calerase_btn:I = 0x7f0b00c0

.field public static final gripsensor_crcount:I = 0x7f0b00ba

.field public static final gripsensor_crcount_null:I = 0x7f0b00b9

.field public static final gripsensor_cspercent:I = 0x7f0b00b8

.field public static final gripsensor_cspercent_str:I = 0x7f0b00b7

.field public static final gripsensor_onoff_btn:I = 0x7f0b00c1

.field public static final gripsensor_proxpercent:I = 0x7f0b00bc

.field public static final gripsensor_proxpercent_str:I = 0x7f0b00bb

.field public static final gripsensor_skip_btn:I = 0x7f0b00c2

.field public static final group:I = 0x7f0b015f

.field public static final gyro_sensor_graph:I = 0x7f0b00d7

.field public static final gyro_separator:I = 0x7f0b01f2

.field public static final gyro_title:I = 0x7f0b01f1

.field public static final gyro_zero_rate:I = 0x7f0b00d9

.field public static final hallic_1st:I = 0x7f0b0104

.field public static final hallic_2nd:I = 0x7f0b010a

.field public static final hallic_test:I = 0x7f0b0105

.field public static final hallic_test2:I = 0x7f0b010b

.field public static final hallic_test_table:I = 0x7f0b0103

.field public static final hallicpoint:I = 0x7f0b0110

.field public static final hallicpoint2:I = 0x7f0b0111

.field public static final hist_nv_list:I = 0x7f0b0092

.field public static final history:I = 0x7f0b0093

.field public static final home_key:I = 0x7f0b0292

.field public static final home_title:I = 0x7f0b0278

.field public static final homekey_max:I = 0x7f0b02a0

.field public static final homekey_min:I = 0x7f0b029b

.field public static final homekey_str:I = 0x7f0b0296

.field public static final hr_value_txt:I = 0x7f0b0069

.field public static final hrm_device_id:I = 0x7f0b021d

.field public static final hrm_lib:I = 0x7f0b021c

.field public static final hrm_lib_elf:I = 0x7f0b021f

.field public static final hrm_lib_eol:I = 0x7f0b021e

.field public static final hrm_sensor_graph:I = 0x7f0b0122

.field public static final hrm_separator:I = 0x7f0b0219

.field public static final hrm_title:I = 0x7f0b0218

.field public static final humid_acc:I = 0x7f0b0243

.field public static final humid_comp:I = 0x7f0b0245

.field public static final humid_raw:I = 0x7f0b0244

.field public static final hw:I = 0x7f0b0188

.field public static final hw_id:I = 0x7f0b0123

.field public static final hw_self_x:I = 0x7f0b00f8

.field public static final hw_self_y:I = 0x7f0b00f9

.field public static final hw_self_z:I = 0x7f0b00fa

.field public static final hwversion:I = 0x7f0b0082

.field public static final hx:I = 0x7f0b017b

.field public static final hy:I = 0x7f0b017d

.field public static final hz:I = 0x7f0b017f

.field public static final id_start_button:I = 0x7f0b008f

.field public static final id_up_button:I = 0x7f0b01b6

.field public static final idac_back:I = 0x7f0b028d

.field public static final idac_home:I = 0x7f0b028c

.field public static final idac_label:I = 0x7f0b028a

.field public static final idac_menu:I = 0x7f0b028b

.field public static final imageBarcode:I = 0x7f0b0010

.field public static final info:I = 0x7f0b015c

.field public static final info1:I = 0x7f0b00b3

.field public static final info2:I = 0x7f0b00b6

.field public static final initialized:I = 0x7f0b00f0

.field public static final initialized_title:I = 0x7f0b0135

.field public static final intcheck:I = 0x7f0b0229

.field public static final intial_X:I = 0x7f0b00fd

.field public static final intial_Y:I = 0x7f0b00fe

.field public static final invisible:I = 0x7f0b00da

.field public static final ired_vendor_text:I = 0x7f0b0125

.field public static final irtemplist_body_temp:I = 0x7f0b0145

.field public static final irtemplist_count:I = 0x7f0b0144

.field public static final irtemplist_rawta_data:I = 0x7f0b0149

.field public static final irtemplist_rawto_data:I = 0x7f0b0148

.field public static final irtemplist_ta_data:I = 0x7f0b0147

.field public static final irtemplist_to_data:I = 0x7f0b0146

.field public static final kernel_log:I = 0x7f0b0238

.field public static final l_leak:I = 0x7f0b0037

.field public static final layout2:I = 0x7f0b0126

.field public static final lcd_self_test:I = 0x7f0b02a4

.field public static final lcdtype:I = 0x7f0b0154

.field public static final leakage_view:I = 0x7f0b0036

.field public static final left:I = 0x7f0b0001

.field public static final light_separator:I = 0x7f0b01ed

.field public static final light_title:I = 0x7f0b01ec

.field public static final linear:I = 0x7f0b0121

.field public static final linearLayout1:I = 0x7f0b0228

.field public static final linearLayout2:I = 0x7f0b022d

.field public static final linearLayout3:I = 0x7f0b02aa

.field public static final linear_layout:I = 0x7f0b0044

.field public static final list:I = 0x7f0b024e

.field public static final list_raw_data:I = 0x7f0b0143

.field public static final loopback_off:I = 0x7f0b0163

.field public static final lux_val:I = 0x7f0b015d

.field public static final lux_val2:I = 0x7f0b015e

.field public static final magn_separator:I = 0x7f0b01ff

.field public static final magn_title:I = 0x7f0b01fe

.field public static final magnetic2:I = 0x7f0b00c4

.field public static final magnetic3:I = 0x7f0b00d8

.field public static final magnetic4:I = 0x7f0b00c6

.field public static final magnetic5:I = 0x7f0b00cb

.field public static final mcufirmversion:I = 0x7f0b022a

.field public static final mdm_silent_reset:I = 0x7f0b023d

.field public static final measured_adc:I = 0x7f0b02d5

.field public static final measured_calc:I = 0x7f0b02d8

.field public static final measured_count:I = 0x7f0b00d3

.field public static final measured_gesture_test_count_delta_ChA:I = 0x7f0b0095

.field public static final measured_gesture_test_count_delta_ChB:I = 0x7f0b0096

.field public static final measured_gesture_test_count_delta_ChC:I = 0x7f0b0097

.field public static final measured_gesture_test_count_delta_ChD:I = 0x7f0b0098

.field public static final measured_gesture_test_count_delta_count:I = 0x7f0b0094

.field public static final measured_gesture_test_mode1_Zmean:I = 0x7f0b00a1

.field public static final measured_gesture_test_mode1_count:I = 0x7f0b00a0

.field public static final measured_gesture_test_mode2_count:I = 0x7f0b00a6

.field public static final measured_gesture_test_mode2_item1:I = 0x7f0b00a7

.field public static final measured_gesture_test_mode2_item2:I = 0x7f0b00a8

.field public static final measured_gesture_test_mode2_item3:I = 0x7f0b00a9

.field public static final measured_gesture_test_mode2_item4:I = 0x7f0b00aa

.field public static final measured_gesture_test_mode2_item5:I = 0x7f0b00ab

.field public static final measured_gesture_test_mode2_item6:I = 0x7f0b00ac

.field public static final measured_gesture_test_mode2_item7:I = 0x7f0b00ad

.field public static final measured_gesture_test_mode2_item8:I = 0x7f0b00ae

.field public static final measured_item:I = 0x7f0b003b

.field public static final measured_pressure:I = 0x7f0b003e

.field public static final measured_tag:I = 0x7f0b003d

.field public static final measured_time:I = 0x7f0b003c

.field public static final measured_time_stamp:I = 0x7f0b003f

.field public static final measured_uva:I = 0x7f0b02d6

.field public static final measured_uvb:I = 0x7f0b02d7

.field public static final measured_value_comp_check:I = 0x7f0b024a

.field public static final measured_value_humid_comp:I = 0x7f0b0249

.field public static final measured_value_humid_raw:I = 0x7f0b0247

.field public static final measured_value_temp_comp:I = 0x7f0b0248

.field public static final measured_value_temp_raw:I = 0x7f0b0246

.field public static final measured_value_thermistor_ap:I = 0x7f0b024f

.field public static final measured_value_thermistor_chg:I = 0x7f0b0251

.field public static final measured_value_thermistor_pam:I = 0x7f0b0250

.field public static final measured_value_thermistor_sensorhub_batt1:I = 0x7f0b024c

.field public static final measured_value_thermistor_sensorhub_batt2:I = 0x7f0b024d

.field public static final measured_value_x:I = 0x7f0b00d4

.field public static final measured_value_y:I = 0x7f0b00d5

.field public static final measured_value_z:I = 0x7f0b00d6

.field public static final menu:I = 0x7f0b00c7

.field public static final menu_key:I = 0x7f0b0291

.field public static final menu_setting1:I = 0x7f0b02e9

.field public static final menu_setting2:I = 0x7f0b02ea

.field public static final menu_title:I = 0x7f0b0277

.field public static final menukey_max:I = 0x7f0b029f

.field public static final menukey_min:I = 0x7f0b029a

.field public static final menukey_str:I = 0x7f0b0295

.field public static final message:I = 0x7f0b028e

.field public static final minMax:I = 0x7f0b0290

.field public static final mode1:I = 0x7f0b01bc

.field public static final mode2:I = 0x7f0b01bd

.field public static final mode_check:I = 0x7f0b01bf

.field public static final modem_log:I = 0x7f0b0239

.field public static final mtp_adb_mode:I = 0x7f0b02c8

.field public static final mtp_mode:I = 0x7f0b02c7

.field public static final music_volume:I = 0x7f0b0195

.field public static final name:I = 0x7f0b006a

.field public static final new_fuel_gauge1:I = 0x7f0b0086

.field public static final new_fuel_gauge2:I = 0x7f0b0088

.field public static final noise_bias_x:I = 0x7f0b00f2

.field public static final noise_bias_y:I = 0x7f0b00f3

.field public static final noise_bias_z:I = 0x7f0b00f4

.field public static final noise_power_x:I = 0x7f0b00f5

.field public static final noise_power_y:I = 0x7f0b00f6

.field public static final noise_power_z:I = 0x7f0b00f7

.field public static final normal_receiver:I = 0x7f0b01c4

.field public static final off:I = 0x7f0b02e2

.field public static final offset_h_x:I = 0x7f0b0177

.field public static final offset_h_y:I = 0x7f0b0178

.field public static final offset_h_z:I = 0x7f0b0179

.field public static final offset_result:I = 0x7f0b01b3

.field public static final offset_threshold_value:I = 0x7f0b01c1

.field public static final ois_gyro_value:I = 0x7f0b01f8

.field public static final ois_result:I = 0x7f0b0102

.field public static final ok_button:I = 0x7f0b02ce

.field public static final on:I = 0x7f0b02e1

.field public static final on_off_repeat:I = 0x7f0b0132

.field public static final onoff_title:I = 0x7f0b012f

.field public static final panel_vendor_name:I = 0x7f0b025b

.field public static final panel_vendor_str:I = 0x7f0b025a

.field public static final panelfirmversion:I = 0x7f0b02a8

.field public static final peak_dc_ir:I = 0x7f0b02af

.field public static final peak_dc_red:I = 0x7f0b02b0

.field public static final peak_dc_result:I = 0x7f0b02b1

.field public static final peak_peak_ir:I = 0x7f0b02ac

.field public static final peak_peak_red:I = 0x7f0b02ad

.field public static final peak_peak_result:I = 0x7f0b02ae

.field public static final peak_ratio_ir:I = 0x7f0b02b2

.field public static final peak_ratio_red:I = 0x7f0b02b3

.field public static final peak_ratio_result:I = 0x7f0b02b4

.field public static final phonefirmversion:I = 0x7f0b02a7

.field public static final piezo_receiver:I = 0x7f0b01c5

.field public static final power_noise_graph:I = 0x7f0b01ab

.field public static final prime_x_str:I = 0x7f0b00e2

.field public static final prime_y_str:I = 0x7f0b00e3

.field public static final prime_z_str:I = 0x7f0b00e4

.field public static final progress:I = 0x7f0b0194

.field public static final prox_separator:I = 0x7f0b01da

.field public static final prox_title:I = 0x7f0b01d9

.field public static final proximity:I = 0x7f0b01c2

.field public static final proximity_str:I = 0x7f0b01c3

.field public static final ptp_adb_mode:I = 0x7f0b02ca

.field public static final ptp_mode:I = 0x7f0b02c9

.field public static final quickstart:I = 0x7f0b0042

.field public static final ramdump_mode:I = 0x7f0b023c

.field public static final rawTa_title:I = 0x7f0b0141

.field public static final rawTa_value:I = 0x7f0b0151

.field public static final rawTo_title:I = 0x7f0b0140

.field public static final rawTo_value:I = 0x7f0b0150

.field public static final raw_data:I = 0x7f0b000d

.field public static final rawdata_back:I = 0x7f0b027e

.field public static final rawdata_home:I = 0x7f0b027d

.field public static final rawdata_label:I = 0x7f0b027b

.field public static final rawdata_max_back:I = 0x7f0b0288

.field public static final rawdata_max_home:I = 0x7f0b0287

.field public static final rawdata_max_label:I = 0x7f0b0285

.field public static final rawdata_max_menu:I = 0x7f0b0286

.field public static final rawdata_max_recent:I = 0x7f0b0289

.field public static final rawdata_menu:I = 0x7f0b027c

.field public static final rawdata_min_back:I = 0x7f0b0283

.field public static final rawdata_min_home:I = 0x7f0b0282

.field public static final rawdata_min_label:I = 0x7f0b0280

.field public static final rawdata_min_menu:I = 0x7f0b0281

.field public static final rawdata_min_recent:I = 0x7f0b0284

.field public static final rawdata_recent:I = 0x7f0b027f

.field public static final rcv_on:I = 0x7f0b0160

.field public static final recent_key:I = 0x7f0b0294

.field public static final recent_title:I = 0x7f0b027a

.field public static final recentkey_max:I = 0x7f0b02a2

.field public static final recentkey_min:I = 0x7f0b029d

.field public static final recentkey_str:I = 0x7f0b0298

.field public static final recording_mainmic_rcv:I = 0x7f0b0190

.field public static final recording_mainmic_spk:I = 0x7f0b018f

.field public static final recording_off:I = 0x7f0b0193

.field public static final recording_submic_rcv:I = 0x7f0b0192

.field public static final recording_submic_spk:I = 0x7f0b0191

.field public static final release_text:I = 0x7f0b0109

.field public static final release_text2:I = 0x7f0b010f

.field public static final release_title:I = 0x7f0b0108

.field public static final release_title2:I = 0x7f0b010e

.field public static final res_humi_raw:I = 0x7f0b0210

.field public static final res_humi_raw_sleep:I = 0x7f0b0212

.field public static final res_temp_raw:I = 0x7f0b020f

.field public static final res_temp_raw_sleep:I = 0x7f0b0211

.field public static final reset_button:I = 0x7f0b0035

.field public static final result:I = 0x7f0b00c3

.field public static final result_text:I = 0x7f0b0031

.field public static final rfcal:I = 0x7f0b0187

.field public static final right:I = 0x7f0b0002

.field public static final rmnet_dm_modem_mode:I = 0x7f0b02cc

.field public static final rndis_dm_modem_mode:I = 0x7f0b02cb

.field public static final rtc_msg:I = 0x7f0b01c7

.field public static final run_dump:I = 0x7f0b0237

.field public static final run_dump_all:I = 0x7f0b0235

.field public static final s_leak:I = 0x7f0b0038

.field public static final scroll_view:I = 0x7f0b0043

.field public static final seekbar_magnitude:I = 0x7f0b02e6

.field public static final seektext:I = 0x7f0b0055

.field public static final self_check_result:I = 0x7f0b0101

.field public static final self_test_list:I = 0x7f0b01cb

.field public static final self_test_listview_item:I = 0x7f0b01c9

.field public static final sensorHub_test:I = 0x7f0b0227

.field public static final sensorHubpassfail:I = 0x7f0b022e

.field public static final sensor_arrow:I = 0x7f0b0208

.field public static final sensorhub_crashed_update:I = 0x7f0b0270

.field public static final sensorhub_firmware_bin:I = 0x7f0b026f

.field public static final sensorhub_firmware_bin_str:I = 0x7f0b026e

.field public static final sensorhub_firmware_mcu:I = 0x7f0b026d

.field public static final sensorhub_firmware_mcu_str:I = 0x7f0b026c

.field public static final sensorhub_separator:I = 0x7f0b01cd

.field public static final sensorhub_text:I = 0x7f0b01ce

.field public static final sensorhub_title:I = 0x7f0b01cc

.field public static final sensorhub_update:I = 0x7f0b0271

.field public static final seperator:I = 0x7f0b0062

.field public static final sglte:I = 0x7f0b01a4

.field public static final sglte_mode:I = 0x7f0b01a6

.field public static final simple_version:I = 0x7f0b01c8

.field public static final spinner_mode:I = 0x7f0b0017

.field public static final spk_on:I = 0x7f0b0161

.field public static final start_button:I = 0x7f0b0065

.field public static final start_ril_log:I = 0x7f0b023e

.field public static final state:I = 0x7f0b022f

.field public static final status:I = 0x7f0b0169

.field public static final strX:I = 0x7f0b00db

.field public static final strY:I = 0x7f0b00dc

.field public static final strZ:I = 0x7f0b00dd

.field public static final str_X:I = 0x7f0b00cc

.field public static final str_Y:I = 0x7f0b00cd

.field public static final str_Z:I = 0x7f0b00ce

.field public static final sub1_str:I = 0x7f0b00c8

.field public static final sub2_str:I = 0x7f0b00ca

.field public static final sub3_str:I = 0x7f0b00e6

.field public static final sub4_str:I = 0x7f0b00ea

.field public static final sub5_str:I = 0x7f0b00cf

.field public static final svc_led_blue:I = 0x7f0b0233

.field public static final svc_led_magenta:I = 0x7f0b0234

.field public static final svc_led_off:I = 0x7f0b0231

.field public static final svc_led_red:I = 0x7f0b0232

.field public static final sx:I = 0x7f0b016b

.field public static final sy:I = 0x7f0b016d

.field public static final sz:I = 0x7f0b0183

.field public static final ta_title:I = 0x7f0b013f

.field public static final ta_value:I = 0x7f0b014f

.field public static final table:I = 0x7f0b0063

.field public static final tableRow1:I = 0x7f0b00c5

.field public static final table_button:I = 0x7f0b0064

.field public static final table_list_title:I = 0x7f0b0039

.field public static final table_title:I = 0x7f0b013b

.field public static final tablerow_adc:I = 0x7f0b0172

.field public static final tablerow_bmz:I = 0x7f0b0180

.field public static final tablerow_dac:I = 0x7f0b016e

.field public static final tablerow_hx:I = 0x7f0b017a

.field public static final tablerow_hy:I = 0x7f0b017c

.field public static final tablerow_hz:I = 0x7f0b017e

.field public static final tablerow_initialized:I = 0x7f0b00ef

.field public static final tablerow_offset_h:I = 0x7f0b0176

.field public static final tablerow_status:I = 0x7f0b0137

.field public static final tablerow_sx:I = 0x7f0b016a

.field public static final tablerow_sy:I = 0x7f0b016c

.field public static final tablerow_sz:I = 0x7f0b0182

.field public static final tablerow_temp:I = 0x7f0b0168

.field public static final tablerow_title:I = 0x7f0b013c

.field public static final tablerow_value:I = 0x7f0b014c

.field public static final temp:I = 0x7f0b0136

.field public static final temp_acc:I = 0x7f0b0240

.field public static final temp_comp:I = 0x7f0b0242

.field public static final temp_humid_engine:I = 0x7f0b0217

.field public static final temp_humid_separator:I = 0x7f0b020e

.field public static final temp_humid_title:I = 0x7f0b020d

.field public static final temp_raw:I = 0x7f0b0241

.field public static final temp_status:I = 0x7f0b0139

.field public static final temperature:I = 0x7f0b00f1

.field public static final temphumid_sensor_graph:I = 0x7f0b024b

.field public static final test_name:I = 0x7f0b01ca

.field public static final text:I = 0x7f0b0054

.field public static final textDigits:I = 0x7f0b0011

.field public static final textStatus:I = 0x7f0b0016

.field public static final textSymbology:I = 0x7f0b0012

.field public static final textView1:I = 0x7f0b0153

.field public static final text_dumpsys_power:I = 0x7f0b01ad

.field public static final text_magnitude:I = 0x7f0b02e4

.field public static final text_magnitude_range:I = 0x7f0b02e5

.field public static final text_process_status:I = 0x7f0b01ac

.field public static final thd_mode:I = 0x7f0b01ba

.field public static final thd_reset:I = 0x7f0b01be

.field public static final thd_result:I = 0x7f0b01bb

.field public static final thd_spec:I = 0x7f0b0272

.field public static final thermo_display_separator:I = 0x7f0b013a

.field public static final thermo_display_separator_2:I = 0x7f0b0142

.field public static final thermo_lib_version:I = 0x7f0b0115

.field public static final thermo_separator:I = 0x7f0b0114

.field public static final thermo_title:I = 0x7f0b0113

.field public static final title:I = 0x7f0b0061

.field public static final title_msg:I = 0x7f0b006f

.field public static final to_title:I = 0x7f0b013e

.field public static final to_value:I = 0x7f0b014e

.field public static final toggle_start:I = 0x7f0b0018

.field public static final top:I = 0x7f0b0003

.field public static final touch_key_graph:I = 0x7f0b02a3

.field public static final touchkey_currentversion:I = 0x7f0b0273

.field public static final touchkey_recommendvesion:I = 0x7f0b0274

.field public static final tsTitle:I = 0x7f0b028f

.field public static final ts_max_label:I = 0x7f0b029e

.field public static final ts_min_label:I = 0x7f0b0299

.field public static final tsk_part_firmware:I = 0x7f0b0262

.field public static final tsk_part_firmware_str:I = 0x7f0b0261

.field public static final tsk_phone_firmware:I = 0x7f0b0260

.field public static final tsk_phone_firmware_str:I = 0x7f0b025f

.field public static final tsk_update:I = 0x7f0b0263

.field public static final tsp_part_firmware:I = 0x7f0b0255

.field public static final tsp_part_firmware_str:I = 0x7f0b0254

.field public static final tsp_phone_firmware:I = 0x7f0b0253

.field public static final tsp_phone_firmware_str:I = 0x7f0b0252

.field public static final tsp_threshold:I = 0x7f0b0257

.field public static final tsp_threshold_str:I = 0x7f0b0256

.field public static final tsp_update:I = 0x7f0b025d

.field public static final tsp_update_factory:I = 0x7f0b025c

.field public static final tsp_update_ums:I = 0x7f0b025e

.field public static final tspchip:I = 0x7f0b02a9

.field public static final tsppassfail:I = 0x7f0b02ab

.field public static final tv_acc_sleep_value:I = 0x7f0b01d5

.field public static final tv_acc_value:I = 0x7f0b01d2

.field public static final tv_adc:I = 0x7f0b0158

.field public static final tv_adc_title:I = 0x7f0b0157

.field public static final tv_barom_altitude:I = 0x7f0b01ea

.field public static final tv_barom_altitude_setting:I = 0x7f0b01e5

.field public static final tv_barom_pressure:I = 0x7f0b01e8

.field public static final tv_barom_sleep_altitude:I = 0x7f0b01eb

.field public static final tv_calibration:I = 0x7f0b0007

.field public static final tv_gyro_sleep_value:I = 0x7f0b01f4

.field public static final tv_gyro_value:I = 0x7f0b01f3

.field public static final tv_level:I = 0x7f0b015a

.field public static final tv_light_adc:I = 0x7f0b01ef

.field public static final tv_light_lux:I = 0x7f0b01ee

.field public static final tv_logging_on:I = 0x7f0b001a

.field public static final tv_lux:I = 0x7f0b0156

.field public static final tv_magn_sensor_raw_value:I = 0x7f0b0200

.field public static final tv_magn_sensor_sleep_value:I = 0x7f0b0202

.field public static final tv_magn_sensor_value:I = 0x7f0b0201

.field public static final tv_orie_azimuth:I = 0x7f0b0203

.field public static final tv_orie_pitch:I = 0x7f0b0204

.field public static final tv_orie_roll:I = 0x7f0b0205

.field public static final tv_prox_adc_avg:I = 0x7f0b01dc

.field public static final tv_prox_adc_sleep_avg:I = 0x7f0b01df

.field public static final tv_prox_default_trim:I = 0x7f0b01de

.field public static final tv_prox_high_threshold:I = 0x7f0b01e1

.field public static final tv_prox_low_threshold:I = 0x7f0b01e2

.field public static final tv_prox_offset:I = 0x7f0b01e0

.field public static final tv_prox_status:I = 0x7f0b01db

.field public static final tv_prox_tsp_color_id:I = 0x7f0b01dd

.field public static final tv_ultra_sensor_adc_distance_value:I = 0x7f0b01fd

.field public static final tv_ultra_sensor_company_fw_value:I = 0x7f0b01fc

.field public static final tv_uv_lib:I = 0x7f0b0226

.field public static final tv_x:I = 0x7f0b0004

.field public static final tv_y:I = 0x7f0b0005

.field public static final tv_z:I = 0x7f0b0006

.field public static final txt_ecg_fw_version:I = 0x7f0b0120

.field public static final type:I = 0x7f0b02c1

.field public static final type_ap:I = 0x7f0b02c3

.field public static final type_apcp:I = 0x7f0b02c4

.field public static final type_cp:I = 0x7f0b02c2

.field public static final type_text_length:I = 0x7f0b0026

.field public static final type_text_length_usb:I = 0x7f0b01a1

.field public static final uart:I = 0x7f0b0197

.field public static final uart_esc:I = 0x7f0b019f

.field public static final uart_lte:I = 0x7f0b019a

.field public static final uart_modem:I = 0x7f0b0198

.field public static final uart_pda:I = 0x7f0b0199

.field public static final ultra_separator:I = 0x7f0b01fb

.field public static final ultra_title:I = 0x7f0b01fa

.field public static final usb:I = 0x7f0b019b

.field public static final usb_esc:I = 0x7f0b01a0

.field public static final usb_modem:I = 0x7f0b019c

.field public static final usb_name:I = 0x7f0b01a3

.field public static final usb_pda:I = 0x7f0b019d

.field public static final usbradio:I = 0x7f0b02c6

.field public static final uv_display:I = 0x7f0b02d4

.field public static final uv_display_listview:I = 0x7f0b02d3

.field public static final uv_display_off:I = 0x7f0b02d0

.field public static final uv_display_on:I = 0x7f0b02d1

.field public static final uv_display_upper:I = 0x7f0b02d2

.field public static final uv_hold_btn:I = 0x7f0b02de

.field public static final uv_ic_check_exit:I = 0x7f0b02dd

.field public static final uv_ic_check_id:I = 0x7f0b02d9

.field public static final uv_ic_check_result:I = 0x7f0b02db

.field public static final uv_ic_check_spec:I = 0x7f0b02da

.field public static final uv_ic_check_test:I = 0x7f0b02dc

.field public static final uv_sensor_graph:I = 0x7f0b02e0

.field public static final uv_separator:I = 0x7f0b0222

.field public static final uv_table:I = 0x7f0b02df

.field public static final uv_title:I = 0x7f0b0221

.field public static final values_rgbw:I = 0x7f0b0159

.field public static final view_help:I = 0x7f0b0041

.field public static final view_status:I = 0x7f0b0040

.field public static final voice_call_volume:I = 0x7f0b0196

.field public static final volume_down:I = 0x7f0b012e

.field public static final volume_title:I = 0x7f0b012c

.field public static final volume_up:I = 0x7f0b012d

.field public static final wacom_firmware:I = 0x7f0b0267

.field public static final wacom_firmware_str:I = 0x7f0b0266

.field public static final wacom_firmware_str_ums:I = 0x7f0b0264

.field public static final wacom_firmware_ums:I = 0x7f0b0265

.field public static final wacom_tuning_version:I = 0x7f0b0269

.field public static final wacom_tuning_version_str:I = 0x7f0b0268

.field public static final wacom_update_kernel:I = 0x7f0b026b

.field public static final wacom_update_ums:I = 0x7f0b026a

.field public static final working_text:I = 0x7f0b0107

.field public static final working_text2:I = 0x7f0b010d

.field public static final working_title:I = 0x7f0b0106

.field public static final working_title2:I = 0x7f0b010c

.field public static final x_value:I = 0x7f0b00d0

.field public static final xyz_add_str:I = 0x7f0b00e1

.field public static final xyz_x_str:I = 0x7f0b00de

.field public static final xyz_y_str:I = 0x7f0b00df

.field public static final xyz_z_str:I = 0x7f0b00e0

.field public static final xyzprime_add_str:I = 0x7f0b00e5

.field public static final y_value:I = 0x7f0b00d1

.field public static final z_value:I = 0x7f0b00d2

.field public static final zero_x_str:I = 0x7f0b00eb

.field public static final zero_y_str:I = 0x7f0b00ec

.field public static final zero_z_str:I = 0x7f0b00ed


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
