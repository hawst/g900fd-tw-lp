.class Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "ECGGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 60
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "action":Ljava/lang/String;
    const-string v7, "com.sec.factory.app.factorytest.ECG_ALL_DATA"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 64
    .local v2, "ecg_bundle":Landroid/os/Bundle;
    const-string v7, "ecg_float"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    .line 65
    .local v1, "ecg_all_data":[F
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # invokes: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->drawGraph([F)V
    invoke-static {v7, v1}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$000(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;[F)V

    .line 66
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$100(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onReceive"

    const-string v9, "Received ecg_bundle"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .end local v1    # "ecg_all_data":[F
    .end local v2    # "ecg_bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    const-string v7, "com.sec.factory.app.factorytest.ECG_DATA"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 70
    const-string v7, "ecg_delta"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "ecg_delta":Ljava/lang/String;
    const-string v7, "ecg_delta_rawData"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 72
    .local v4, "ecg_delta_rawData":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$200(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onReceive"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ecg_delta : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$300(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onReceive"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ecg_delta_rawData : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 76
    .end local v3    # "ecg_delta":Ljava/lang/String;
    .end local v4    # "ecg_delta_rawData":Ljava/lang/String;
    :cond_2
    const-string v7, "com.sec.factory.app.factorytest.HEART_RATE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 77
    const-string v7, "heart_rate"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .local v5, "heart_rate":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$400(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onReceive"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HR : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 81
    .local v6, "heat_rate_decimal":I
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->mTextView_HeartRate:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$500(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Heart rate : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 84
    .end local v5    # "heart_rate":Ljava/lang/String;
    .end local v6    # "heat_rate_decimal":I
    :cond_3
    const-string v7, "com.sec.factory.app.factorytest.DEVICE_DETACHED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 85
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity$1;->this$0:Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;->access$600(Lcom/sec/android/app/hwmoduletest/ECGGraphActivity;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "onReceive"

    const-string v9, "Device have beean detached!"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
