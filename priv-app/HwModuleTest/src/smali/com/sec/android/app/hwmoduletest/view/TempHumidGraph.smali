.class public Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;
.super Landroid/view/View;
.source "TempHumidGraph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private final GRAPH_SCALING_COOR:F

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private final TEXT_SCALING_COOR:F

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mDataPaint:Landroid/graphics/Paint;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mHumid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mHumidPaint:Landroid/graphics/Paint;

.field private mPathHumid:Landroid/graphics/Path;

.field private mPathTemp:Landroid/graphics/Path;

.field private mRawDataMax:[F

.field private mRawDataMin:[F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTemp:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mTempPaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x3

    .line 50
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INCREASING_COOR:I

    .line 22
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->GRAPH_SCALING_COOR:F

    .line 23
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->TEXT_SCALING_COOR:F

    .line 29
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    .line 40
    const-string v0, "TempHumidGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    .line 42
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMin:[F

    .line 45
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMax:[F

    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->init(Landroid/content/Context;)V

    .line 52
    return-void

    .line 42
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 45
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x3

    .line 60
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INCREASING_COOR:I

    .line 22
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->GRAPH_SCALING_COOR:F

    .line 23
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->TEXT_SCALING_COOR:F

    .line 29
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    .line 40
    const-string v0, "TempHumidGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    .line 42
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMin:[F

    .line 45
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMax:[F

    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->init(Landroid/content/Context;)V

    .line 62
    return-void

    .line 42
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 45
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x3

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_X:I

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INCREASING_COOR:I

    .line 22
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->GRAPH_SCALING_COOR:F

    .line 23
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->TEXT_SCALING_COOR:F

    .line 29
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    .line 40
    const-string v0, "TempHumidGraph"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    .line 42
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMin:[F

    .line 45
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mRawDataMax:[F

    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->init(Landroid/content/Context;)V

    .line 57
    return-void

    .line 42
    nop

    :array_0
    .array-data 4
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
        0x7f800000    # Float.POSITIVE_INFINITY
    .end array-data

    .line 45
    :array_1
    .array-data 4
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
        -0x800000    # Float.NEGATIVE_INFINITY
    .end array-data
.end method

.method private init(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v6, 0x41f00000    # 30.0f

    const/high16 v5, -0x1000000

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    .line 65
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mContext:Landroid/content/Context;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 68
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mScreenWidth:I

    .line 69
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mScreenHeight:I

    .line 70
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mScreenHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    .line 71
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    div-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->LIST_SIZE:I

    .line 72
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mEffects:Landroid/graphics/PathEffect;

    .line 73
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 78
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 88
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 93
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 98
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mDataPaint:Landroid/graphics/Paint;

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mDataPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mDataPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mDataPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->invalidate()V

    .line 104
    return-void
.end method

.method private setPathHumid()V
    .locals 7

    .prologue
    const/high16 v6, 0x41000000    # 8.0f

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 266
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v6

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 276
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v6

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->invalidate()V

    .line 286
    return-void
.end method

.method private setPathTemp()V
    .locals 7

    .prologue
    const/high16 v6, 0x41000000    # 8.0f

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 238
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    .line 243
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    const/high16 v3, 0x41200000    # 10.0f

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v6

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 250
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v6

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->invalidate()V

    .line 260
    return-void
.end method


# virtual methods
.method public addValueHumid(FF)V
    .locals 2
    .param p1, "comp"    # F
    .param p2, "raw"    # F

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 198
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->setPathHumid()V

    .line 199
    return-void
.end method

.method public addValueTemp(FF)V
    .locals 2
    .param p1, "comp"    # F
    .param p2, "raw"    # F

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->setPathTemp()V

    .line 185
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v11, 0x428c0000    # 70.0f

    const/high16 v10, 0x42480000    # 50.0f

    const/16 v5, 0xb4

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v9, 0x41200000    # 10.0f

    .line 108
    const/4 v8, 0x0

    .line 109
    .local v8, "testTemp":F
    const/4 v7, 0x0

    .line 113
    .local v7, "testHumid":F
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 126
    :goto_0
    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 127
    const-string v0, "Temp :"

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v10, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 128
    const/high16 v1, 0x42dc0000    # 110.0f

    const/high16 v3, 0x43700000    # 240.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 129
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x42dc0000    # 110.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 130
    const-string v0, "Humid :"

    const/high16 v1, 0x43820000    # 260.0f

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v10, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 131
    const/high16 v1, 0x43b90000    # 370.0f

    const/high16 v3, 0x43fa0000    # 500.0f

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 132
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43b90000    # 370.0f

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 136
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, -0xc8

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    add-int/lit16 v0, v0, 0xc8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    move v3, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 138
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathTemp:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTempPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mPathHumid:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumidPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 171
    return-void

    .line 116
    :catch_0
    move-exception v6

    .line 117
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    const-string v3, "get value fail"

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size of Temp, Humid : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mTemp:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->mHumid:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onDraw"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LIST_SIZE value : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/hwmoduletest/view/TempHumidGraph;->LIST_SIZE:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
