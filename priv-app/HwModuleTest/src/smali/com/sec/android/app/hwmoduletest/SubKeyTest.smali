.class public Lcom/sec/android/app/hwmoduletest/SubKeyTest;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "SubKeyTest.java"


# static fields
.field private static final SCANCODE_BEAM:I = 0x111

.field private static final SCANCODE_DUMMY_BACK1:I = 0xfa

.field private static final SCANCODE_DUMMY_BACK2:I = 0xfd

.field private static final SCANCODE_DUMMY_HOME:I = 0xfc

.field private static final SCANCODE_DUMMY_MENU1:I = 0xfb

.field private static final SCANCODE_DUMMY_MENU2:I = 0xf9


# instance fields
.field private IS_FOUR_KEY:Z

.field private mBackground:Landroid/view/View;

.field private final mIsDummyKeySameKeycode:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "SubKeyTest"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 18
    const-string v0, "IS_DUMMYKEY_SAME_KEYCODE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mIsDummyKeySameKeycode:Z

    .line 20
    const-string v0, "SUBKEY_FOUR_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->IS_FOUR_KEY:Z

    .line 31
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->setContentView(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 72
    const-string v0, "SUBKEY_FOUR_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUBKEY_FIVE_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUBKEY_SIX_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    const-string v0, "EXTRA_BUTTON_EVENT"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 79
    :cond_1
    const-string v0, "IS_DUMMYKEY_FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 80
    const-string v0, "TSK_FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 81
    const-string v0, "TSK_FACTORY_MODE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 84
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 85
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v8, 0x7f07000e

    const v7, 0x7f07000d

    const v6, 0x7f07000c

    const v5, 0x7f070016

    const v4, 0x7f070012

    .line 102
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mIsDummyKeySameKeycode:Z

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v0

    .line 105
    .local v0, "scancode":I
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "Same Keycode for dummy key: Convert"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    sparse-switch p1, :sswitch_data_0

    .line 127
    .end local v0    # "scancode":I
    :cond_0
    :goto_0
    sparse-switch p1, :sswitch_data_1

    .line 310
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    .line 109
    .restart local v0    # "scancode":I
    :sswitch_0
    const/16 v1, 0xfb

    if-ne v0, v1, :cond_2

    .line 110
    const/16 p1, 0x11a

    goto :goto_0

    .line 111
    :cond_2
    const/16 v1, 0xf9

    if-ne v0, v1, :cond_0

    .line 112
    const/16 p1, 0x11e

    goto :goto_0

    .line 116
    :sswitch_1
    const/16 v1, 0xfa

    if-ne v0, v1, :cond_3

    .line 117
    const/16 p1, 0x11f

    goto :goto_0

    .line 118
    :cond_3
    const/16 v1, 0xfd

    if-ne v0, v1, :cond_0

    .line 119
    const/16 p1, 0x11d

    goto :goto_0

    .line 129
    .end local v0    # "scancode":I
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "VOLUME_UP"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 133
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "VOLUME_DOWN"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 137
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "POWER"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    .line 141
    :sswitch_5
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "HOME"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 146
    :sswitch_6
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_MOVE_HOME"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 150
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "MENU"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 154
    :sswitch_8
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "CAMERA"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 158
    :sswitch_9
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "BACK"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 162
    :sswitch_a
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "APP_SWITCH"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 166
    :sswitch_b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_USER"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 170
    :sswitch_c
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_ACTIVE"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 174
    :sswitch_d
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_SEARCH"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 178
    :sswitch_e
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEY_TEST_FUNC1"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 182
    :sswitch_f
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEY_TEST_FUNC2"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 186
    :sswitch_10
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_0"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 190
    :sswitch_11
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_1"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 194
    :sswitch_12
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_2"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 198
    :sswitch_13
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_3"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 202
    :sswitch_14
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_4"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 206
    :sswitch_15
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_5"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 210
    :sswitch_16
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_6"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 214
    :sswitch_17
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_7"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 218
    :sswitch_18
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_8"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 222
    :sswitch_19
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_9"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 226
    :sswitch_1a
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_STAR"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 230
    :sswitch_1b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_POUND"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 234
    :sswitch_1c
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_CALL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 238
    :sswitch_1d
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_CLEAR"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 242
    :sswitch_1e
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DPAD_UP"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 246
    :sswitch_1f
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DPAD_DOWN"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 250
    :sswitch_20
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DPAD_LEFT"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 254
    :sswitch_21
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DPAD_RIGHT"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 258
    :sswitch_22
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_ENTER"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 262
    :sswitch_23
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DPAD_CENTER"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 266
    :sswitch_24
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_NETWORK_SEL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 270
    :sswitch_25
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_3G"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 274
    :sswitch_26
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DEL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 279
    :sswitch_27
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_ENDCALL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 285
    :sswitch_28
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DUMMY_MENU"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 290
    :sswitch_29
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DUMMY_HOME1"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->IS_FOUR_KEY:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 295
    :sswitch_2a
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DUMMY_HOME2"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->IS_FOUR_KEY:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 300
    :sswitch_2b
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_DUMMY_BACK"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 304
    :sswitch_2c
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_BEAM"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x52 -> :sswitch_0
    .end sparse-switch

    .line 127
    :sswitch_data_1
    .sparse-switch
        0x3 -> :sswitch_5
        0x4 -> :sswitch_9
        0x5 -> :sswitch_1c
        0x6 -> :sswitch_27
        0x7 -> :sswitch_10
        0x8 -> :sswitch_11
        0x9 -> :sswitch_12
        0xa -> :sswitch_13
        0xb -> :sswitch_14
        0xc -> :sswitch_15
        0xd -> :sswitch_16
        0xe -> :sswitch_17
        0xf -> :sswitch_18
        0x10 -> :sswitch_19
        0x11 -> :sswitch_1a
        0x12 -> :sswitch_1b
        0x13 -> :sswitch_1e
        0x14 -> :sswitch_1f
        0x15 -> :sswitch_20
        0x16 -> :sswitch_21
        0x17 -> :sswitch_23
        0x18 -> :sswitch_2
        0x19 -> :sswitch_3
        0x1a -> :sswitch_4
        0x1b -> :sswitch_8
        0x1c -> :sswitch_1d
        0x42 -> :sswitch_22
        0x43 -> :sswitch_26
        0x52 -> :sswitch_7
        0x54 -> :sswitch_d
        0x7a -> :sswitch_6
        0xbb -> :sswitch_a
        0xee -> :sswitch_b
        0x109 -> :sswitch_f
        0x10a -> :sswitch_e
        0x111 -> :sswitch_2c
        0x11a -> :sswitch_28
        0x11d -> :sswitch_2b
        0x11e -> :sswitch_29
        0x11f -> :sswitch_2a
        0x120 -> :sswitch_c
        0x128 -> :sswitch_25
        0x133 -> :sswitch_24
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->setBrightnessMode()V

    .line 66
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doWakeLock(Z)V

    .line 67
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "SUBKEY_FOUR_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUBKEY_FIVE_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SUBKEY_SIX_KEY"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    :cond_0
    const-string v0, "EXTRA_BUTTON_EVENT"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 51
    :cond_1
    const-string v0, "IS_DUMMYKEY_FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const-string v0, "TSK_FACTORY_MODE"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->isExistFileID(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    const-string v0, "TSK_FACTORY_MODE"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 57
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 58
    sget-object v0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->mModulePower:Lcom/sec/android/app/hwmoduletest/modules/ModulePower;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModulePower;->doWakeLock(Z)V

    .line 59
    return-void
.end method

.method public setBrightness()V
    .locals 3

    .prologue
    .line 89
    const-string v0, "IS_AUTOBRIGHTNESS"

    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->unSetBrightnessMode()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setBrightness"

    const-string v2, "Enable AutoBrightness"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->setBrightness()V

    .line 97
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/SubKeyTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setBrightness"

    const-string v2, "Do nothing"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
