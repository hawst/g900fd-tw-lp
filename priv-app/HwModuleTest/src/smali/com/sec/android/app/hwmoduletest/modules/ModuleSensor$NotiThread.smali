.class Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;
.super Ljava/lang/Thread;
.source "ModuleSensor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NotiThread"
.end annotation


# instance fields
.field private NOTI_LOOP_DELAY:I

.field private mNotiHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;)V
    .locals 1

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 357
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->NOTI_LOOP_DELAY:I

    .line 358
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->mNotiHandler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$1;

    .prologue
    .line 356
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;-><init>(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 363
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->access$100(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->mNotiHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 367
    :try_start_0
    iget v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->NOTI_LOOP_DELAY:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 377
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    iget-object v1, v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "NotiThread-run"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LoopStop-mInterrupted : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->this$0:Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;

    # getter for: Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->mInterrupted:Z
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;->access$100(Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor;)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "notiHandler"    # Landroid/os/Handler;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->mNotiHandler:Landroid/os/Handler;

    .line 386
    return-void
.end method

.method public setLoopDelay(I)V
    .locals 0
    .param p1, "loopDelay_millisecond"    # I

    .prologue
    .line 381
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleSensor$NotiThread;->NOTI_LOOP_DELAY:I

    .line 382
    return-void
.end method
