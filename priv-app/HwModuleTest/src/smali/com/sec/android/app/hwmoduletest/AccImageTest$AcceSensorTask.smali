.class Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;
.super Ljava/util/TimerTask;
.source "AccImageTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/AccImageTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AcceSensorTask"
.end annotation


# instance fields
.field private mAcceSensorValues:[F

.field private mAngle:[I

.field private mIsRunningTask:Z

.field private mRawData:[I

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 117
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mIsRunningTask:Z

    .line 121
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    .line 122
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    .line 123
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;Lcom/sec/android/app/hwmoduletest/AccImageTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/AccImageTest$1;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/AccImageTest;)V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->getRawdataString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->getAngleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->resume()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->pause()V

    return-void
.end method

.method private getAngleString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Angle - x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    mul-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", z: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    mul-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, "res":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$1200(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getAngleString"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-object v0
.end method

.method private getRawdataString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACC Raw Data - x: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", y: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", z: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "res":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$1100(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getRawdataString"

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-object v0
.end method

.method private pause()V
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mIsRunningTask:Z

    .line 158
    return-void
.end method

.method private readToAccelerometerSensor()V
    .locals 10

    .prologue
    .line 162
    :try_start_0
    const-string v3, "ACCEL_SENSOR_RAW"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 163
    .local v1, "rawDatas":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x2

    const/4 v5, 0x2

    aget-object v5, v1, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v3, v4

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$900(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToAccelerometerSensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "raw[0]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", raw[1]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", raw[2]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v6, 0x2

    aget v5, v5, v6

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    int-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v2, v4

    .line 170
    .local v2, "realg":F
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    mul-int/lit8 v5, v5, -0x1

    aput v5, v3, v4

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    mul-int/lit8 v5, v5, -0x1

    aput v5, v3, v4

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v6, 0x2

    aget v5, v5, v6

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v6

    double-to-float v5, v6

    const v6, 0x42652ee1

    mul-float/2addr v5, v6

    float-to-int v5, v5

    add-int/lit8 v5, v5, -0x5a

    aput v5, v3, v4

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$1000(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "readToAccelerometerSensor"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "angle[0]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", angle[1]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", angle[2]="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    .end local v1    # "rawDatas":[Ljava/lang/String;
    .end local v2    # "realg":F
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mRawData:[I

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    float-to-double v6, v5

    const-wide/high16 v8, 0x4090000000000000L    # 1024.0

    mul-double/2addr v6, v8

    const-wide v8, 0x40239eb851eb851fL    # 9.81

    div-double/2addr v6, v8

    double-to-int v5, v6

    aput v5, v3, v4

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAngle:[I

    const/4 v8, 0x2

    const/4 v9, 0x0

    aput v9, v7, v8

    aput v9, v5, v6

    aput v9, v3, v4

    goto :goto_0
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mIsRunningTask:Z

    .line 154
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 127
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 131
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 142
    :goto_0
    return-void

    .line 133
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$700(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value[0]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value[1]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value[2]="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mAcceSensorValues:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->readToAccelerometerSensor()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/AccImageTest$AcceSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/AccImageTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/AccImageTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/AccImageTest;->access$800(Lcom/sec/android/app/hwmoduletest/AccImageTest;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 150
    :cond_0
    return-void
.end method
