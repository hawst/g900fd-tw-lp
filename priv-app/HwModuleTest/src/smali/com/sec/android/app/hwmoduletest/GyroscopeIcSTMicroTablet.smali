.class public Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeIcSTMicroTablet.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "GyroscopeIcSTMicroTablet"

.field private static final RET_IDX_CALIBRATION:I = 0xf

.field private static final RET_IDX_FIFO_NUM:I = 0x9

.field private static final RET_IDX_FIFO_X:I = 0xa

.field private static final RET_IDX_FIFO_Y:I = 0xb

.field private static final RET_IDX_FIFO_Z:I = 0xc

.field private static final RET_IDX_MAX:I = 0xf

.field private static final RET_IDX_NOMAL_X:I = 0x0

.field private static final RET_IDX_NOMAL_Y:I = 0x1

.field private static final RET_IDX_NOMAL_Z:I = 0x2

.field private static final RET_IDX_SELFTEST:I = 0xd

.field private static final RET_IDX_SELFTEST_X:I = 0x3

.field private static final RET_IDX_SELFTEST_Y:I = 0x4

.field private static final RET_IDX_SELFTEST_Z:I = 0x5

.field private static final RET_IDX_ZERORATE:I = 0xe

.field private static final RET_IDX_ZERORATE_X:I = 0x6

.field private static final RET_IDX_ZERORATE_Y:I = 0x7

.field private static final RET_IDX_ZERORATE_Z:I = 0x8

.field private static final TEST_FAIL:Ljava/lang/String; = "0"

.field private static final TEST_SUCCESS:Ljava/lang/String; = "1"


# instance fields
.field private mCalibrationPass:Z

.field private mGyroSelfTestLayout:Landroid/widget/TableLayout;

.field private mGyroZeroRateText:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mSelfTestResults:[Ljava/lang/String;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mZeroRatePass:Z

.field private txt_calibration:Landroid/widget/TextView;

.field private txt_diff_x:Landroid/widget/TextView;

.field private txt_diff_y:Landroid/widget/TextView;

.field private txt_diff_z:Landroid/widget/TextView;

.field private txt_self_x:Landroid/widget/TextView;

.field private txt_self_y:Landroid/widget/TextView;

.field private txt_self_z:Landroid/widget/TextView;

.field private txt_x:Landroid/widget/TextView;

.field private txt_xyz_x:Landroid/widget/TextView;

.field private txt_xyz_y:Landroid/widget/TextView;

.field private txt_xyz_z:Landroid/widget/TextView;

.field private txt_y:Landroid/widget/TextView;

.field private txt_z:Landroid/widget/TextView;

.field private txt_zerorate_x:Landroid/widget/TextView;

.field private txt_zerorate_y:Landroid/widget/TextView;

.field private txt_zerorate_z:Landroid/widget/TextView;

.field private txtresult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    const-string v0, "GyroscopeIcSTMicroTablet"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 41
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mZeroRatePass:Z

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mCalibrationPass:Z

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mHandler:Landroid/os/Handler;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;)Landroid/widget/TableLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->showTestResults([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 81
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0b00db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_x:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0b00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_y:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_z:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0b00de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_x:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0b00df

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_y:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0b00e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_z:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0b00e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_x:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_y:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0b00e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_z:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0b00e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_x:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0b00e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_y:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0b00e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_z:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0b00eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_x:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0b00ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_y:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0b00ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_z:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0b00ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_calibration:Landroid/widget/TextView;

    .line 98
    return-void
.end method

.method private showTestResults([Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "results"    # [Ljava/lang/String;
    .param p2, "resultValue"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const v4, -0xffff01

    const/high16 v3, -0x10000

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_x:Landroid/widget/TextView;

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_y:Landroid/widget/TextView;

    aget-object v1, p1, v5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_xyz_z:Landroid/widget/TextView;

    aget-object v1, p1, v6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_x:Landroid/widget/TextView;

    const/4 v1, 0x3

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_self_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_x:Landroid/widget/TextView;

    const/4 v1, 0x6

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_y:Landroid/widget/TextView;

    const/4 v1, 0x7

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_zerorate_z:Landroid/widget/TextView;

    const/16 v1, 0x8

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_x:Landroid/widget/TextView;

    const/4 v1, 0x3

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_diff_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v6

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mCalibrationPass:Z

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_calibration:Landroid/widget/TextView;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_calibration:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    :goto_0
    if-eqz p2, :cond_0

    .line 187
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mZeroRatePass:Z

    if-eqz v0, :cond_2

    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    :cond_0
    :goto_1
    return-void

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_calibration:Landroid/widget/TextView;

    const v1, 0x7f08007b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txt_calibration:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 190
    :cond_2
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 195
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->setContentView(I)V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->init()V

    .line 78
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 101
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 102
    const/4 v1, 0x0

    .line 103
    .local v1, "resultValue":Ljava/lang/String;
    const/4 v2, 0x1

    .line 104
    .local v2, "resultValue_Int":I
    const v4, 0x7f0b00d9

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    .line 105
    const v4, 0x7f0b00c7

    invoke-virtual {p0, v4}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableLayout;

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    invoke-virtual {v4, v7}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    const-string v7, ""

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v7, "/sys/class/sensors/gyro_sensor/selftest"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x2000

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "selfTestResult":Ljava/lang/String;
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    .line 116
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xf

    aget-object v4, v4, v7

    if-eqz v4, :cond_2

    .line 117
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xe

    aget-object v4, v4, v7

    const-string v7, "1"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mZeroRatePass:Z

    .line 119
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xf

    aget-object v4, v4, v7

    const-string v7, "1"

    invoke-virtual {v4, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mCalibrationPass:Z

    .line 127
    iget-boolean v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mZeroRatePass:Z

    if-eqz v4, :cond_3

    .line 128
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Zero Rate Level Check:  PASS \n(Index of FIFO : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0x9

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / Data : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xb

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xc

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    const v5, -0xffff01

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->txtresult:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 159
    .end local v3    # "selfTestResult":Ljava/lang/String;
    :goto_2
    return-void

    .restart local v3    # "selfTestResult":Ljava/lang/String;
    :cond_0
    move v4, v6

    .line 117
    goto/16 :goto_0

    :cond_1
    move v4, v6

    .line 119
    goto :goto_1

    .line 122
    :cond_2
    const-string v4, "GyroscopeIcSTMicroTablet"

    const-string v5, "onResume"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " -- parse error: Gyro self-test -- ret selftest : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 156
    .end local v3    # "selfTestResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 143
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "selfTestResult":Ljava/lang/String;
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Zero Rate Level Check:  FAIL \n(Index of FIFO : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0x9

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / Data : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xb

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mSelfTestResults:[Ljava/lang/String;

    const/16 v7, 0xc

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicroTablet;->mGyroZeroRateText:Landroid/widget/TextView;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method
