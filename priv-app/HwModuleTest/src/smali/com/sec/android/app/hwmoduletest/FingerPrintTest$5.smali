.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 219
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setVisibility(I)V

    .line 220
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMPixelString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "pixelDataArray":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMRMDataString:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 223
    .local v1, "mrmDataArray":[Ljava/lang/String;
    aget-object v7, v2, v10

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v10

    .line 224
    aget-object v7, v2, v10

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 226
    .local v5, "startPixel":I
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v7

    add-int/lit8 v7, v7, -0x32

    int-to-float v7, v7

    array-length v8, v2

    add-int/lit8 v8, v8, -0x1

    add-int/2addr v8, v5

    add-int/lit8 v8, v8, 0x14

    int-to-float v8, v8

    div-float v3, v7, v8

    .line 228
    .local v3, "scaleX":F
    const-string v7, "FingerPrintTest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Graph X Scale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setGraphXScale(F)V

    .line 231
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v7

    int-to-float v7, v7

    const/high16 v8, 0x40400000    # 3.0f

    div-float/2addr v7, v8

    const/high16 v8, 0x42480000    # 50.0f

    div-float/2addr v7, v8

    const/high16 v8, 0x41200000    # 10.0f

    div-float v4, v7, v8

    .line 232
    .local v4, "scaleY":F
    const-string v7, "FingerPrintTest"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Graph Y Scale : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setGraphYScale(F)V

    .line 235
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->setStartPixel(I)V

    .line 238
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 239
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7, v10, v10, v10}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->addValue(III)V

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_0
    const/4 v0, 0x1

    :goto_1
    array-length v7, v2

    if-ge v0, v7, :cond_2

    .line 243
    aget-object v7, v1, v0

    const-string v8, "\\s"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v0

    .line 244
    aget-object v7, v1, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 245
    .local v6, "value":I
    if-gez v6, :cond_1

    .line 246
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    neg-int v8, v6

    invoke-virtual {v7, v8, v10, v10}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->addValue(III)V

    .line 242
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 248
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7, v6, v10, v10}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->addValue(III)V

    goto :goto_2

    .line 251
    .end local v6    # "value":I
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMRMGraph:Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintMRMGraph;->showGraph()V

    .line 257
    .end local v0    # "i":I
    .end local v1    # "mrmDataArray":[Ljava/lang/String;
    .end local v2    # "pixelDataArray":[Ljava/lang/String;
    .end local v3    # "scaleX":F
    .end local v4    # "scaleY":F
    .end local v5    # "startPixel":I
    :goto_3
    return-void

    .line 253
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    const-string v8, "Cannot found Data log!!"

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$202(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Ljava/lang/String;)Ljava/lang/String;

    .line 254
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$5;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
