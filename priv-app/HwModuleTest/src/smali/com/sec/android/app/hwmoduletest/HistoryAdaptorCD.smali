.class Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;
.super Landroid/widget/ArrayAdapter;
.source "GestureTestCountDeltaDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/HistoryItemCD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItemCD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/HistoryItemCD;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 203
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 206
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;

    .line 209
    .local v11, "item":Lcom/sec/android/app/hwmoduletest/HistoryItemCD;
    if-nez p2, :cond_0

    .line 210
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptorCD;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    .line 211
    .local v13, "li":Landroid/view/LayoutInflater;
    const v14, 0x7f030022

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v13, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 216
    .end local v13    # "li":Landroid/view/LayoutInflater;
    .local v12, "layout":Landroid/widget/LinearLayout;
    :goto_0
    invoke-virtual {v11}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->getCount()Ljava/lang/String;

    move-result-object v9

    .line 217
    .local v9, "countString":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->getValueRawDataA()Ljava/lang/String;

    move-result-object v1

    .line 218
    .local v1, "chA":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->getValueRawDataB()Ljava/lang/String;

    move-result-object v3

    .line 219
    .local v3, "chB":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->getValueRawDataC()Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "chC":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/sec/android/app/hwmoduletest/HistoryItemCD;->getValueRawDataD()Ljava/lang/String;

    move-result-object v7

    .line 221
    .local v7, "chD":Ljava/lang/String;
    const v14, 0x7f0b0094

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 222
    .local v10, "countView":Landroid/widget/TextView;
    const v14, 0x7f0b0095

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 223
    .local v2, "chAView":Landroid/widget/TextView;
    const v14, 0x7f0b0096

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 224
    .local v4, "chBView":Landroid/widget/TextView;
    const v14, 0x7f0b0097

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 225
    .local v6, "chCView":Landroid/widget/TextView;
    const v14, 0x7f0b0098

    invoke-virtual {v12, v14}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 226
    .local v8, "chDView":Landroid/widget/TextView;
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    return-object v12

    .end local v1    # "chA":Ljava/lang/String;
    .end local v2    # "chAView":Landroid/widget/TextView;
    .end local v3    # "chB":Ljava/lang/String;
    .end local v4    # "chBView":Landroid/widget/TextView;
    .end local v5    # "chC":Ljava/lang/String;
    .end local v6    # "chCView":Landroid/widget/TextView;
    .end local v7    # "chD":Ljava/lang/String;
    .end local v8    # "chDView":Landroid/widget/TextView;
    .end local v9    # "countString":Ljava/lang/String;
    .end local v10    # "countView":Landroid/widget/TextView;
    .end local v12    # "layout":Landroid/widget/LinearLayout;
    :cond_0
    move-object/from16 v12, p2

    .line 213
    check-cast v12, Landroid/widget/LinearLayout;

    .restart local v12    # "layout":Landroid/widget/LinearLayout;
    goto :goto_0
.end method
