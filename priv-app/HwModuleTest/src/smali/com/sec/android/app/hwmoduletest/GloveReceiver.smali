.class Lcom/sec/android/app/hwmoduletest/GloveReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TspPatternStyleX.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;
    }
.end annotation


# static fields
.field public static final CLASS_NAME:Ljava/lang/String; = "GloveReceiver"

.field public static final INTENT_GLOVE:Ljava/lang/String; = "com.samsung.glove.ENABLE"

.field public static final INTENT_GLOVE_EXTRA:Ljava/lang/String; = "gloveEnable"

.field private static mHandler:Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;

.field private static mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

.field private static mIsEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 718
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mIsEnable:Z

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 708
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 710
    return-void
.end method

.method public static getEnable()Z
    .locals 1

    .prologue
    .line 723
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mIsEnable:Z

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/hwmoduletest/GloveReceiver;
    .locals 1

    .prologue
    .line 743
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    if-nez v0, :cond_0

    .line 744
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    invoke-direct {v0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;-><init>()V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    .line 746
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    return-object v0
.end method

.method public static registerBroadcastReceiver(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 750
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 751
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.glove.ENABLE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 752
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->getInstance()Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 753
    return-void
.end method

.method public static setHandler(Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;)V
    .locals 0
    .param p0, "l"    # Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;

    .prologue
    .line 727
    sput-object p0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mHandler:Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;

    .line 728
    return-void
.end method

.method public static unregisterBroadcastReceiver(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 756
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    if-eqz v0, :cond_0

    .line 757
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mInstance:Lcom/sec/android/app/hwmoduletest/GloveReceiver;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 760
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 732
    const-string v1, "com.samsung.glove.ENABLE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 733
    const-string v1, "gloveEnable"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 734
    .local v0, "isEnable":Z
    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mIsEnable:Z

    .line 735
    const-string v1, "GloveReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive com.samsung.glove.ENABLE isEnable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mHandler:Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;

    if-eqz v1, :cond_0

    .line 737
    sget-object v1, Lcom/sec/android/app/hwmoduletest/GloveReceiver;->mHandler:Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;

    invoke-interface {v1, v0}, Lcom/sec/android/app/hwmoduletest/GloveReceiver$IGloverEventHandler;->onGloveEnableChanged(Z)V

    .line 740
    .end local v0    # "isEnable":Z
    :cond_0
    return-void
.end method
