.class public Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GripSensorTest2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;
    }
.end annotation


# static fields
.field private static IS_GRIP_COUNT:I = 0x0

.field protected static final KEY_TIMEOUT:I = 0x2

.field protected static final KEY_TIMER_EXPIRED:I = 0x1

.field private static final MASK_SENSING_GRIP_OFF:Ljava/lang/String; = "000000000000"

.field private static final MASK_SENSING_GRIP_SIDE_ON:Ljava/lang/String; = "000001000000"

.field private static final MASK_SENSING_GRIP_TOP_ON:Ljava/lang/String; = "010000000000"

.field private static final MASK_SENSING_GRIP_TOP_SIDE_ON:Ljava/lang/String; = "010001000000"

.field protected static final MILLIS_IN_SEC:I = 0x3e8

.field private static final VIB_STRONG:I = 0x1

.field private static final VIB_WEAK:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private GRIPSENSOR_DUAL_MODE:Z

.field private Grip_status_ON_1:Z

.field private Grip_status_ON_2:Z

.field private final VIBRATE_TIME:I

.field WHAT_EXIT:B

.field WHAT_NOTI_SENSOR_UPDATAE:B

.field private info1:Landroid/widget/TextView;

.field private info2:Landroid/widget/TextView;

.field private mBackgroudLayout1:Landroid/widget/LinearLayout;

.field private mBackgroudLayout2:Landroid/widget/LinearLayout;

.field private mCurrentTime:J

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mIsPressedBackkey:Z

.field private mSensorListener:Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mVibrator:Landroid/os/Vibrator;

.field mWl:Landroid/os/PowerManager$WakeLock;

.field private pass1:Z

.field private pass2:Z

.field temp:[F

.field private txtgripsensor1:Landroid/widget/TextView;

.field private txtgripsensor2:Landroid/widget/TextView;

.field private working:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->IS_GRIP_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    const-string v0, "GripSensorTest2"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->working:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->pass1:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->pass2:Z

    .line 70
    const v0, 0xffff

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->VIBRATE_TIME:I

    .line 72
    const-string v0, "GripSensorTest2"

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->CLASS_NAME:Ljava/lang/String;

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mIsPressedBackkey:Z

    .line 76
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mCurrentTime:J

    .line 96
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z

    .line 97
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z

    .line 98
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z

    .line 116
    iput-byte v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->WHAT_NOTI_SENSOR_UPDATAE:B

    .line 118
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->WHAT_EXIT:B

    .line 48
    return-void
.end method

.method private ActiveVibrate(I)V
    .locals 4
    .param p1, "intensity"    # I

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->startVibration(I)V

    .line 237
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 238
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$1;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 242
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_1:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->Grip_status_ON_2:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GripSensorTest2;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->stopVibration()V

    return-void
.end method

.method private init()V
    .locals 7

    .prologue
    const v4, 0x7f08008f

    const/16 v6, 0x8

    .line 168
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 169
    .local v1, "pm":Landroid/os/PowerManager;
    const/16 v2, 0x1a

    const-string v3, "My Lock Tag"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 170
    const v2, 0x7f0b00b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->txtgripsensor1:Landroid/widget/TextView;

    .line 171
    const v2, 0x7f0b00b5

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->txtgripsensor2:Landroid/widget/TextView;

    .line 172
    const v2, 0x7f0b00b3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    .line 173
    const v2, 0x7f0b00b6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 176
    const v2, 0x7f0b00b1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    .line 177
    const v2, 0x7f0b00b4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    .line 178
    const-string v2, "15"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$HwTestMenu;->getTestCase(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "gripCount":Ljava/lang/String;
    const-string v2, "GRIPSENSOR_DUAL_MODE"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z

    .line 180
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 181
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sput v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->IS_GRIP_COUNT:I

    .line 183
    :cond_0
    const-string v2, "GripSensorTest2"

    const-string v3, "init"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IS_GRIP_COUNT : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->IS_GRIP_COUNT:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "GRIPSENSOR_DUAL_MODE : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    sget v2, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->IS_GRIP_COUNT:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->GRIPSENSOR_DUAL_MODE:Z

    if-nez v2, :cond_1

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->txtgripsensor1:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->txtgripsensor2:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    :cond_1
    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    .line 194
    return-void
.end method

.method private startTest()V
    .locals 8

    .prologue
    const v7, 0x7f070021

    const/4 v6, 0x0

    const v5, 0x7f08008f

    const v4, 0x7f070002

    const/4 v3, 0x0

    .line 245
    sget v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->IS_GRIP_COUNT:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 246
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "IS_GRIP_COUNT == 1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->temp:[F

    aget v0, v0, v3

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 250
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V

    .line 251
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "________ 0000 ________"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    :goto_0
    return-void

    .line 253
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->stopVibration()V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 258
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "MASK_SENSING_GRIP_OFF"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->temp:[F

    aget v0, v0, v3

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->temp:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 267
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V

    .line 268
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "MASK_SENSING_GRIP_SIDE_BACK_ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->temp:[F

    aget v0, v0, v3

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_3

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 276
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V

    .line 277
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "MASK_SENSING_GRIP_SIDE_ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 278
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->temp:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_4

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    const v1, 0x7f080092

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 285
    invoke-direct {p0, v3}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->ActiveVibrate(I)V

    .line 286
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "MASK_SENSING_GRIP_BACK_ON"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 288
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->stopVibration()V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    .line 293
    const-string v0, "GripSensorTest2"

    const-string v1, "onReceive"

    const-string v2, "MASK_SENSING_GRIP_OFF else"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 214
    const-string v1, "GripSensorTest2"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 219
    .local v0, "pattern":[J
    if-nez p1, :cond_1

    .line 220
    aput-wide v6, v0, v4

    .line 221
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 227
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 228
    return-void

    .line 222
    :cond_1
    if-ne p1, v5, :cond_0

    .line 223
    aput-wide v6, v0, v4

    .line 224
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 215
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 232
    const-string v0, "GripSensorTest2"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    const v0, 0x7f030030

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setContentView(I)V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->init()V

    .line 124
    const-string v0, "GripSensorTest2"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 159
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onDestroy()V

    .line 165
    return-void
.end method

.method public onExit()V
    .locals 3

    .prologue
    .line 207
    const-string v0, "GripSensorTest2"

    const-string v1, "onExit"

    const-string v2, "finish"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->setResult(I)V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->finish()V

    .line 211
    return-void
.end method

.method public onFinish()V
    .locals 3

    .prologue
    .line 201
    const-string v0, "GripSensorTest2"

    const-string v1, "onFinish"

    const-string v2, "GripSensor Test finish"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->finish()V

    .line 204
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 148
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    .line 150
    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    .line 152
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onPause()V

    .line 153
    const-string v0, "GripSensorTest2"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 131
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 132
    const-string v0, "GripSensorTest2"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 135
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;-><init>(Lcom/sec/android/app/hwmoduletest/GripSensorTest2;Lcom/sec/android/app/hwmoduletest/GripSensorTest2$1;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

    .line 136
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x10018

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/hwmoduletest/GripSensorTest2$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 139
    return-void
.end method

.method public setBackgroundColor(Landroid/widget/LinearLayout;I)V
    .locals 1
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "id"    # I

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GripSensorTest2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 198
    return-void
.end method
