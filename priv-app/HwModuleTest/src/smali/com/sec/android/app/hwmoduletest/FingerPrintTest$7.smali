.class Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;
.super Ljava/lang/Object;
.source "FingerPrintTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/FingerPrintTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 289
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 293
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setVisibility(I)V

    .line 295
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4PrimaryData:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2400(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4SecondaryData:[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2500(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setDataResult([Ljava/lang/String;[Ljava/lang/String;)V

    .line 296
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadMethod4BaundaryData:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2700(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setSignalLossBoundary([Ljava/lang/String;)V

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadNoTermSignalString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "mNoTermSingalDataArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 299
    .local v3, "mTermSingalDataArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ReadTermSignalLossString:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 301
    .local v2, "mSignalLossRateDataArray":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenWidth:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v6

    add-int/lit8 v6, v6, -0x32

    int-to-float v6, v6

    array-length v7, v1

    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v7, v7, 0x14

    int-to-float v7, v7

    div-float v4, v6, v7

    .line 303
    .local v4, "scaleX":F
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Graph X Scale : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setGraphXScale(F)V

    .line 306
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ScreenHeight:I
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$1200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40400000    # 3.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x437f0000    # 255.0f

    div-float/2addr v6, v7

    const/high16 v7, 0x40c00000    # 6.0f

    div-float v5, v6, v7

    .line 307
    .local v5, "scaleY":F
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Graph Y Scale : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->setGraphYScale(F)V

    .line 309
    const-string v6, "FingerPrintTest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NoTermSignal Length : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " TermSignalLoss Length : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v10, :cond_0

    .line 312
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    invoke-virtual {v6, v9, v9, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->addValue(III)V

    .line 311
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    :cond_0
    const/4 v0, 0x1

    :goto_1
    array-length v6, v1

    if-ge v0, v6, :cond_1

    .line 316
    aget-object v6, v1, v0

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v0

    .line 317
    aget-object v6, v3, v0

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v0

    .line 318
    aget-object v6, v2, v0

    const-string v7, "\\s"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v0

    .line 319
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    aget-object v7, v1, v0

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aget-object v8, v3, v0

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aget-object v9, v2, v0

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->addValue(III)V

    .line 315
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 325
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_msg2:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$100(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 326
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->mFingerPrintMethod4Graph:Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$2600(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintMethod4Graph;->showGraph()V

    .line 332
    .end local v0    # "i":I
    .end local v1    # "mNoTermSingalDataArray":[Ljava/lang/String;
    .end local v2    # "mSignalLossRateDataArray":[Ljava/lang/String;
    .end local v3    # "mTermSingalDataArray":[Ljava/lang/String;
    .end local v4    # "scaleX":F
    .end local v5    # "scaleY":F
    :goto_2
    return-void

    .line 328
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    const-string v7, "Cannot found Data log!!"

    # setter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$202(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;Ljava/lang/String;)Ljava/lang/String;

    .line 329
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 330
    iget-object v6, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->tv_result:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$300(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Landroid/widget/TextView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/hwmoduletest/FingerPrintTest$7;->this$0:Lcom/sec/android/app/hwmoduletest/FingerPrintTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->m_ResultMessage:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/hwmoduletest/FingerPrintTest;->access$200(Lcom/sec/android/app/hwmoduletest/FingerPrintTest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
