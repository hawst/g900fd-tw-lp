.class public Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;
.super Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;
.source "ModuleDevice.java"


# static fields
.field public static final ACTION_EXTERNAL_STORAGE_MOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_MOUNTED"

.field public static final ACTION_EXTERNAL_STORAGE_UNMOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_EXTERNAL_STORAGE_UNMOUNTED"

.field public static final ACTION_OTG_RESPONSE:Ljava/lang/String; = "com.sec.factory.intent.ACTION_OTG_RESPONSE"

.field public static final ACTION_USB_STORAGE_MOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_USB_STORAGE_MOUNTED"

.field public static final ACTION_USB_STORAGE_UNMOUNTED:Ljava/lang/String; = "com.sec.factory.action.ACTION_USB_STORAGE_UNMOUNTED"

.field public static final EXTERNAL_MEMORY:I = 0x1

.field public static final INTERNAL_MEMORY:I = 0x0

.field private static final MSG_OTG_TEST_FINISH:B = 0x0t

.field private static final OTG_MUIC_OFF:[B

.field private static final OTG_MUIC_ON:[B

.field private static final OTG_TEST_OFF:[B

.field private static final OTG_TEST_ON:[B

.field private static final OTG_TEST_WAIT_DELAY:I = 0x7d0

.field public static final PATH_EXTERNAL_STORAGE:Ljava/lang/String; = "extSdCard"

.field public static final PATH_INTERNAL_STORAGE:Ljava/lang/String; = "sdcard"

.field public static final PATH_OTG_STORAGE:Ljava/lang/String; = "UsbStotageX"

.field private static final STORAGE_PATH:[Ljava/lang/String;

.field public static final SYSTEM_STORAGE:I = 0x2

.field public static final TSP_CMD_CHIP_NAME_READ:Ljava/lang/String; = "get_chip_name"

.field public static final TSP_CMD_CHIP_VENDOR_READ:Ljava/lang/String; = "get_chip_vendor"

.field public static final TSP_CMD_MODULE_OFF_MASTER:Ljava/lang/String; = "module_off_master"

.field public static final TSP_CMD_MODULE_OFF_SLAVE:Ljava/lang/String; = "module_off_slave"

.field public static final TSP_CMD_MODULE_ON_MASTER:Ljava/lang/String; = "module_on_master"

.field public static final TSP_CMD_MODULE_ON_SLAVE:Ljava/lang/String; = "module_on_slave"

.field public static final TSP_CMD_MODULE_TSP_CONNECTION:Ljava/lang/String; = "get_tsp_connection"

.field public static final TSP_CMD_READ1_CM_ABS:Ljava/lang/String; = "get_cm_abs"

.field public static final TSP_CMD_READ1_GET_REF:Ljava/lang/String; = "get_reference"

.field public static final TSP_CMD_READ1_RAWCAP:Ljava/lang/String; = "get_rawcap"

.field public static final TSP_CMD_READ1_RUN_CM_ABS:Ljava/lang/String; = "run_cm_abs_read"

.field public static final TSP_CMD_READ1_RUN_DELTA:Ljava/lang/String; = "run_delta_read"

.field public static final TSP_CMD_READ1_RUN_RAWCAP:Ljava/lang/String; = "run_rawcap_read"

.field public static final TSP_CMD_READ1_RUN_REF:Ljava/lang/String; = "run_reference_read"

.field public static final TSP_CMD_READ2_CM_DELFA:Ljava/lang/String; = "get_cm_delta"

.field public static final TSP_CMD_READ2_GET_DELTA:Ljava/lang/String; = "get_delta"

.field public static final TSP_CMD_READ2_GET_RAW:Ljava/lang/String; = "get_raw"

.field public static final TSP_CMD_READ2_RUN_CM_DELTA:Ljava/lang/String; = "run_cm_delta_read"

.field public static final TSP_CMD_READ2_RUN_RxToRx:Ljava/lang/String; = "run_rx_to_rx_read"

.field public static final TSP_CMD_READ2_RxToRx:Ljava/lang/String; = "get_rx_to_rx"

.field public static final TSP_CMD_READ3_GET_DELTA:Ljava/lang/String; = "get_delta"

.field public static final TSP_CMD_READ3_INTENSITY:Ljava/lang/String; = "get_intensity"

.field public static final TSP_CMD_READ3_RUN_DELTA:Ljava/lang/String; = "run_delta_read"

.field public static final TSP_CMD_READ3_RUN_INTENSITY:Ljava/lang/String; = "run_intensity_read"

.field public static final TSP_CMD_READ3_RUN_TxToTx:Ljava/lang/String; = "run_tx_to_tx_read"

.field public static final TSP_CMD_READ3_TxToTx:Ljava/lang/String; = "get_tx_to_tx"

.field public static final TSP_CMD_READ4_RUN_TxToGND:Ljava/lang/String; = "run_tx_to_gnd_read"

.field public static final TSP_CMD_READ4_TxToGND:Ljava/lang/String; = "get_tx_to_gnd"

.field public static final TSP_CMD_READ5_RUN_RAW:Ljava/lang/String; = "run_raw_read"

.field public static final TSP_CMD_READ_THRESHOLD:Ljava/lang/String; = "get_threshold"

.field public static final TSP_CMD_SELFTEST:Ljava/lang/String; = "run_selftest"

.field public static final TSP_CMD_X_COUNT:Ljava/lang/String; = "get_x_num"

.field public static final TSP_CMD_Y_COUNT:Ljava/lang/String; = "get_y_num"

.field public static final TSP_FIRMWARE_VERSION_PANEL:Ljava/lang/String; = "get_fw_ver_ic"

.field public static final TSP_FIRMWARE_VERSION_PHONE:Ljava/lang/String; = "get_fw_ver_bin"

.field public static final TSP_RETURN_VALUE_NA:Ljava/lang/String; = "NA"

.field public static final TSP_RETURN_VALUE_NG:Ljava/lang/String; = "NG"

.field public static final TSP_RETURN_VALUE_OK:Ljava/lang/String; = "OK"

.field public static final TYPE_EXTERNAL_STORAGE:I = 0x1

.field public static final TYPE_INTERNAL_STORAGE:I = 0x0

.field public static final TYPE_USB_STORAGE:I = 0x2

.field public static final TYPE_USB_STORAGE_B:I = 0x3

.field public static final UNIT_BYTE:I = 0x0

.field public static final UNIT_GB:I = 0x40000000

.field public static final UNIT_KB:I = 0x400

.field public static final UNIT_MB:I = 0x100000

.field public static final USER_STORAGE_1:I = 0x0

.field public static final USER_STORAGE_2:I = 0x1

.field private static mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;


# instance fields
.field private mIsMountedExternalStorage:Z

.field private mIsMountedUsbStorage:Z

.field private mIsMountedUsbStorageB:Z

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 71
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/mnt/sdcard"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/mnt/extSdCard"

    aput-object v2, v0, v1

    const-string v1, "/system"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    .line 231
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->OTG_TEST_ON:[B

    .line 234
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->OTG_TEST_OFF:[B

    .line 237
    new-array v0, v3, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->OTG_MUIC_ON:[B

    .line 240
    new-array v0, v3, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->OTG_MUIC_OFF:[B

    return-void

    .line 231
    nop

    :array_0
    .array-data 1
        0x4ft
        0x4et
        0x0t
    .end array-data

    .line 234
    :array_1
    .array-data 1
        0x4ft
        0x46t
        0x46t
        0x0t
    .end array-data

    .line 237
    :array_2
    .array-data 1
        0x30t
        0x0t
    .end array-data

    .line 240
    nop

    :array_3
    .array-data 1
        0x31t
        0x0t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 43
    const-string v1, "ModuleDevice"

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 185
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mIsMountedExternalStorage:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mIsMountedUsbStorage:Z

    .line 187
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mIsMountedUsbStorageB:Z

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "Create ModuleDevice"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mVibrator:Landroid/os/Vibrator;

    .line 46
    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 50
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "registerReceiver OK"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    .line 59
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;

    return-object v0
.end method

.method private startTSPTest([BI)Ljava/lang/String;
    .locals 6
    .param p1, "cmd"    # [B
    .param p2, "commandLength"    # I

    .prologue
    .line 302
    const-string v1, ""

    .line 303
    .local v1, "status":Ljava/lang/String;
    const-string v0, ""

    .line 305
    .local v0, "result":Ljava/lang/String;
    const-string v2, "TSP_COMMAND_CMD"

    invoke-static {v2, p1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 306
    const-string v2, "TSP_COMMAND_STATUS"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 307
    const-string v2, "TSP_COMMAND_RESULT"

    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 309
    if-nez v1, :cond_0

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Status Read => status == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v2, "NG"

    .line 339
    :goto_0
    return-object v2

    .line 312
    :cond_0
    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 313
    if-nez v0, :cond_1

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>1 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string v2, "NG"

    goto :goto_0

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 321
    :cond_2
    const-string v2, "NOT_APPLICABLE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A- Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    if-nez v0, :cond_3

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>2 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v2, "NG"

    goto/16 :goto_0

    .line 331
    :cond_3
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 333
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v2, "NG"

    goto/16 :goto_0

    .line 338
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Command Write"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v2, "NG"

    goto/16 :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleDevice"

    const-string v2, "finalize ModuleDevice"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 66
    return-void
.end method

.method public getAvailableSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 93
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 94
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getInnerMemorySize()J
    .locals 6

    .prologue
    .line 107
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 108
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x100000

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getSize(II)J
    .locals 6
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 88
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 89
    .local v0, "stat":Landroid/os/StatFs;
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long/2addr v2, v4

    int-to-long v4, p2

    div-long/2addr v2, v4

    return-wide v2
.end method

.method public getStoragePath(I)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 213
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v1

    .line 215
    .local v1, "storageVolumes":[Landroid/os/storage/StorageVolume;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->isMountedStorage(I)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 216
    aget-object v2, v1, p1

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getStorageParh"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Storage path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .end local v0    # "path":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 221
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getStorageParh"

    const-string v4, "Storage path : null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUseSize(II)J
    .locals 4
    .param p1, "storage"    # I
    .param p2, "unit"    # I

    .prologue
    .line 98
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->getSize(II)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->getAvailableSize(II)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public hoveringOnOFF(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 432
    const-string v0, "hover_enable"

    .line 433
    .local v0, "mainCmd":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 434
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onOffHovering"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mainCmd="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    const-string v1, "TSP_COMMAND_CMD"

    invoke-static {v1, v0}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 436
    return-void
.end method

.method public int_extMEMOSize(I)Ljava/lang/String;
    .locals 14
    .param p1, "type"    # I

    .prologue
    .line 142
    const/4 v3, 0x0

    .line 143
    .local v3, "resData":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v10}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 144
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    aget-object v10, v5, p1

    invoke-virtual {v10}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "path":Ljava/lang/String;
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 146
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x400

    div-long v6, v10, v12

    .line 147
    .local v6, "totalMass":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long/2addr v10, v12

    const-wide/16 v12, 0x400

    div-long v0, v10, v12

    .line 148
    .local v0, "freeMass":J
    sub-long v8, v6, v0

    .line 149
    .local v8, "usedMass":J
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[Main]TotalMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, FreeMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, UsedMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 152
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "resData="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-object v3
.end method

.method public int_extMEMOSize(IZ)Ljava/lang/String;
    .locals 14
    .param p1, "type"    # I
    .param p2, "isByte"    # Z

    .prologue
    .line 157
    const/4 v3, 0x0

    .line 158
    .local v3, "resData":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v10}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 159
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    aget-object v10, v5, p1

    invoke-virtual {v10}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 160
    .local v2, "path":Ljava/lang/String;
    new-instance v4, Landroid/os/StatFs;

    invoke-direct {v4, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 161
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockCount()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long v6, v10, v12

    .line 162
    .local v6, "totalMass":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v12

    int-to-long v12, v12

    mul-long v0, v10, v12

    .line 163
    .local v0, "freeMass":J
    sub-long v8, v6, v0

    .line 164
    .local v8, "usedMass":J
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "[Main]TotalMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, FreeMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes, UsedMass : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Bytes"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 167
    iget-object v10, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "externalMEMOSize"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "resData="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-object v3
.end method

.method public isDetectExternalMemory()Z
    .locals 2

    .prologue
    .line 112
    const-string v1, "EXTERNAL_MEMORY_INSERTED"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "state":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 115
    const-string v1, "INSERT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 117
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isExternalMemoryExist()Z
    .locals 2

    .prologue
    .line 122
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "state":Ljava/lang/String;
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "unmounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInnerMemoryExist()Z
    .locals 2

    .prologue
    .line 102
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMountedStorage(I)Z
    .locals 8
    .param p1, "type"    # I

    .prologue
    const/4 v3, 0x0

    .line 190
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v4}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 192
    .local v2, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v4, v2

    if-ge p1, v4, :cond_0

    aget-object v4, v2, p1

    if-nez v4, :cond_1

    .line 193
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    const-string v6, "StorageVolumes[type] is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :goto_0
    return v3

    .line 197
    :cond_1
    aget-object v4, v2, p1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    if-eqz v0, :cond_2

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v3, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "state":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isMountedStorage"

    const-string v5, "Environment.MEDIA_MOUNTED : mounted"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "isMountedStorage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "state : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v3, "mounted"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0

    .line 207
    .end local v1    # "state":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "isMountedStorage"

    const-string v6, "another error"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public mainMEMOSize()Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v12, 0x400

    .line 129
    const/4 v2, 0x0

    .line 130
    .local v2, "resData":Ljava/lang/String;
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 131
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v8, v10

    div-long v4, v8, v12

    .line 132
    .local v4, "totalMass":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long/2addr v8, v10

    div-long v0, v8, v12

    .line 133
    .local v0, "freeMass":J
    sub-long v6, v4, v0

    .line 134
    .local v6, "usedMass":J
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mainMEMOSize"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "[Main]TotalMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB, FreeMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB, UsedMass : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "KB"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 137
    iget-object v8, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "mainMEMOSize"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resData="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-object v2
.end method

.method public readSensrohubSleepTest()Ljava/lang/String;
    .locals 5

    .prologue
    .line 411
    const-string v0, ""

    .line 412
    .local v0, "result":Ljava/lang/String;
    const-string v1, "SENSORHUB_SLEEP_TEST"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readSensrohubTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    return-object v0
.end method

.method public readSensrohubTest()Ljava/lang/String;
    .locals 5

    .prologue
    .line 422
    const-string v0, ""

    .line 423
    .local v0, "result":Ljava/lang/String;
    const-string v1, "SENSORHUB_MCU"

    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "readSensrohubTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    return-object v0
.end method

.method public startSensrohubSleepTest()V
    .locals 2

    .prologue
    .line 407
    const-string v0, "SENSORHUB_SLEEP_TEST"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 408
    return-void
.end method

.method public startSensrohubTest()V
    .locals 2

    .prologue
    .line 418
    const-string v0, "SENSORHUB_MCU"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 419
    return-void
.end method

.method public startTSPReadTest(Ljava/lang/String;I)Ljava/lang/String;
    .locals 13
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "x"    # I

    .prologue
    const/16 v10, 0xa

    const/4 v12, 0x0

    .line 351
    const-string v9, "get_x_num"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 353
    .local v7, "temp_X":Ljava/lang/String;
    const-string v9, "NG"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 354
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "startTSPReadTest"

    const-string v11, "error - TSP_CMD_X_COUNT"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v9, "NG"

    .line 400
    :goto_0
    return-object v9

    .line 357
    :cond_0
    invoke-static {v7, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 361
    .local v0, "X_AXIS_MAX":I
    const-string v9, "get_y_num"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 363
    .local v8, "temp_Y":Ljava/lang/String;
    const-string v9, "NG"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 364
    iget-object v9, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "startTSPReadTest"

    const-string v11, "error - TSP_CMD_Y_COUNT"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v9, "NG"

    goto :goto_0

    .line 367
    :cond_1
    invoke-static {v8, v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 370
    .local v1, "Y_AXIS_MAX":I
    const/4 v6, 0x0

    .line 371
    .local v6, "strCMD":Ljava/lang/String;
    const-string v5, ""

    .line 372
    .local v5, "result":Ljava/lang/String;
    const-string v9, "get_chip_vendor"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "mTspManufacture":Ljava/lang/String;
    const-string v9, "SYNAPTICS"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 376
    const/4 v4, 0x0

    .local v4, "node_y":I
    :goto_1
    if-ge v4, v1, :cond_7

    .line 377
    const/4 v3, 0x0

    .local v3, "node_x":I
    :goto_2
    if-ge v3, v0, :cond_2

    .line 378
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 380
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 377
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 376
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 385
    .end local v3    # "node_x":I
    .end local v4    # "node_y":I
    :cond_3
    if-ge p2, v0, :cond_6

    .line 386
    const/4 v4, 0x0

    .restart local v4    # "node_y":I
    :goto_3
    if-ge v4, v1, :cond_5

    .line 387
    const/4 v3, 0x0

    .restart local v3    # "node_x":I
    :goto_4
    if-ge v3, v0, :cond_4

    .line 388
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 389
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v11

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 387
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 386
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 393
    .end local v3    # "node_x":I
    :cond_5
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 396
    .end local v4    # "node_y":I
    :cond_6
    const-string v9, "NG"

    goto/16 :goto_0

    .line 400
    .restart local v4    # "node_y":I
    :cond_7
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0
.end method

.method public startTSPTest(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 296
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startTSPTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cmd name => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 298
    .local v0, "byteCMD":[B
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/hwmoduletest/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
