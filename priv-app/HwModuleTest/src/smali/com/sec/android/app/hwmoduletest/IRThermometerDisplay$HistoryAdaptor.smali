.class Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;
.super Landroid/widget/ArrayAdapter;
.source "IRThermometerDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HistoryAdaptor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .local p3, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 318
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 322
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;

    .line 325
    .local v7, "item":Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;
    if-nez p2, :cond_1

    .line 326
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryAdaptor;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 327
    .local v9, "li":Landroid/view/LayoutInflater;
    const v18, 0x7f030049

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v9, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 332
    .end local v9    # "li":Landroid/view/LayoutInflater;
    .local v8, "layout":Landroid/widget/LinearLayout;
    :goto_0
    const v18, 0x7f0b0144

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 333
    .local v6, "countView":Landroid/widget/TextView;
    const v18, 0x7f0b0145

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 334
    .local v4, "bodytempView":Landroid/widget/TextView;
    const v18, 0x7f0b0146

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 335
    .local v17, "todataView":Landroid/widget/TextView;
    const v18, 0x7f0b0147

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 336
    .local v15, "tadataView":Landroid/widget/TextView;
    const v18, 0x7f0b0148

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 337
    .local v12, "rawtoView":Landroid/widget/TextView;
    const v18, 0x7f0b0149

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 339
    .local v10, "rawtaView":Landroid/widget/TextView;
    const-string v18, "PARTRON"

    sget-object v19, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay;->vendor:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 340
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getCount()Ljava/lang/String;

    move-result-object v5

    .line 341
    .local v5, "countString":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getValueBodyTemp()Ljava/lang/String;

    move-result-object v3

    .line 342
    .local v3, "bodytempStr":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getValueToData()Ljava/lang/String;

    move-result-object v16

    .line 343
    .local v16, "todataStr":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getValueTaData()Ljava/lang/String;

    move-result-object v14

    .line 344
    .local v14, "tadataStr":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getRawto_data()Ljava/lang/String;

    move-result-object v13

    .line 345
    .local v13, "rawtodataStr":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/hwmoduletest/IRThermometerDisplay$HistoryItem;->getRawta_data()Ljava/lang/String;

    move-result-object v11

    .line 347
    .local v11, "rawtadataStr":Ljava/lang/String;
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    invoke-virtual {v15, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    .end local v3    # "bodytempStr":Ljava/lang/String;
    .end local v5    # "countString":Ljava/lang/String;
    .end local v11    # "rawtadataStr":Ljava/lang/String;
    .end local v13    # "rawtodataStr":Ljava/lang/String;
    .end local v14    # "tadataStr":Ljava/lang/String;
    .end local v16    # "todataStr":Ljava/lang/String;
    :cond_0
    return-object v8

    .end local v4    # "bodytempView":Landroid/widget/TextView;
    .end local v6    # "countView":Landroid/widget/TextView;
    .end local v8    # "layout":Landroid/widget/LinearLayout;
    .end local v10    # "rawtaView":Landroid/widget/TextView;
    .end local v12    # "rawtoView":Landroid/widget/TextView;
    .end local v15    # "tadataView":Landroid/widget/TextView;
    .end local v17    # "todataView":Landroid/widget/TextView;
    :cond_1
    move-object/from16 v8, p2

    .line 329
    check-cast v8, Landroid/widget/LinearLayout;

    .restart local v8    # "layout":Landroid/widget/LinearLayout;
    goto :goto_0
.end method
