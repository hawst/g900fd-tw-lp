.class Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;
.super Landroid/os/Handler;
.source "GripSensorCalibration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 298
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 333
    :goto_0
    return-void

    .line 300
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$400(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$300(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "exception"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 303
    const-string v1, "GripSensorCalibration"

    const-string v2, "MSG_UPDATE_UI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCaldata_data : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "caldatas":[Ljava/lang/String;
    aget-object v1, v0, v5

    if-eqz v1, :cond_0

    .line 306
    const-string v1, "GripSensorCalibration"

    const-string v2, "MSG_UPDATE_UI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "caldatas[0] : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    aget-object v1, v0, v5

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " :  Y ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 320
    .end local v0    # "caldatas":[Ljava/lang/String;
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->ispass:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$700(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 312
    .restart local v0    # "caldatas":[Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " :  N ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 317
    .end local v0    # "caldatas":[Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$600(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$500(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 325
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "PASS"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->mText_Result:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;->access$800(Lcom/sec/android/app/hwmoduletest/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 298
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
