.class Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;
.super Ljava/util/TimerTask;
.source "HrmTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HrmTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BIOSensorTask"
.end annotation


# instance fields
.field private mBIOSensorValues:[F

.field private mIsRunningTask:Z

.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/hwmoduletest/HrmTest;)V
    .locals 1

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mIsRunningTask:Z

    .line 255
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/hwmoduletest/HrmTest;Lcom/sec/android/app/hwmoduletest/HrmTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest;
    .param p2, "x1"    # Lcom/sec/android/app/hwmoduletest/HrmTest$1;

    .prologue
    .line 221
    invoke-direct {p0, p1}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;-><init>(Lcom/sec/android/app/hwmoduletest/HrmTest;)V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->resume()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getTempValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getIRValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->getREDValueString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;

    .prologue
    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->pause()V

    return-void
.end method

.method private getIRValueString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "IR(ADC) : --"

    .line 272
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IR(ADC) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1500(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getREDValueString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "RED(ADC) : --"

    .line 277
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RED(ADC) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1600(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTempValueString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMTemp:F
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const-string v0, "Temp(\'C) : --"

    .line 268
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Temp(\'C) : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMTemp:F
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1800(Lcom/sec/android/app/hwmoduletest/HrmTest;)F

    move-result v1

    const/high16 v2, 0x41800000    # 16.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private pause()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mIsRunningTask:Z

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 253
    :cond_0
    return-void
.end method

.method private readToBIOSensor()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1400(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "readToBIOSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBIOSensorValues :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v1, v1, v4

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMIRLevel:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1502(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v1, v1, v5

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMREDLevel:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1602(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->hrm_vendor:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1700(Lcom/sec/android/app/hwmoduletest/HrmTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "MAXIM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    aget v1, v1, v6

    # setter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHRMTemp:F
    invoke-static {v0, v1}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1802(Lcom/sec/android/app/hwmoduletest/HrmTest;F)F

    .line 264
    :cond_0
    return-void
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mIsRunningTask:Z

    .line 245
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 225
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 228
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    return-void

    .line 230
    :pswitch_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mBIOSensorValues:[F

    goto :goto_0

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x10019
        :pswitch_0
    .end packed-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 238
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->readToBIOSensor()V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HrmTest$BIOSensorTask;->this$0:Lcom/sec/android/app/hwmoduletest/HrmTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HrmTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HrmTest;->access$1300(Lcom/sec/android/app/hwmoduletest/HrmTest;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 241
    :cond_0
    return-void
.end method
