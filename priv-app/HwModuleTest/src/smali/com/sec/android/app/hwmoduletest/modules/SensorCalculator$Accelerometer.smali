.class Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;
.super Ljava/lang/Object;
.source "SensorCalculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Accelerometer"
.end annotation


# instance fields
.field private mBitCount:I

.field private mFlatness_Max:I

.field private mFlatness_Min:I

.field private mIsSupport_INT_Pin:Z

.field private mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;


# direct methods
.method public constructor <init>(IIIIIIIIIZ)V
    .locals 8
    .param p1, "bitCount"    # I
    .param p2, "xMin"    # I
    .param p3, "xMax"    # I
    .param p4, "yMin"    # I
    .param p5, "yMax"    # I
    .param p6, "zMin"    # I
    .param p7, "zMax"    # I
    .param p8, "flatnessMin"    # I
    .param p9, "flatnessMax"    # I
    .param p10, "isSupport_INT_Pin"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 20
    iput p1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mBitCount:I

    .line 21
    new-instance v1, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;-><init>(IIIIII)V

    iput-object v1, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    .line 22
    move/from16 v0, p8

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mFlatness_Min:I

    .line 23
    move/from16 v0, p9

    iput v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mFlatness_Max:I

    .line 24
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mIsSupport_INT_Pin:Z

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;)Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Accelerometer;->mRange:Lcom/sec/android/app/hwmoduletest/modules/SensorCalculator$Range;

    return-object v0
.end method
