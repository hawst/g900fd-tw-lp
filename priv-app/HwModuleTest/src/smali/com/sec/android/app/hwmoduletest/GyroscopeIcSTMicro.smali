.class public Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;
.super Lcom/sec/android/app/hwmoduletest/support/BaseActivity;
.source "GyroscopeIcSTMicro.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String; = "GyroscopeSensorTest"


# instance fields
.field private mGyroSelfTestLayout:Landroid/widget/TableLayout;

.field private mGyroZeroRateText:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mSelfTestResults:[Ljava/lang/String;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mZeroRatePass:Z

.field private txt_X:Landroid/widget/TextView;

.field private txt_Y:Landroid/widget/TextView;

.field private txt_Z:Landroid/widget/TextView;

.field private txt_diff_x:Landroid/widget/TextView;

.field private txt_diff_y:Landroid/widget/TextView;

.field private txt_diff_z:Landroid/widget/TextView;

.field private txt_fifo_result:Landroid/widget/TextView;

.field private txt_fifo_subject:Landroid/widget/TextView;

.field private txt_prime_x:Landroid/widget/TextView;

.field private txt_prime_y:Landroid/widget/TextView;

.field private txt_prime_z:Landroid/widget/TextView;

.field private txt_sub1:Landroid/widget/TextView;

.field private txt_sub2:Landroid/widget/TextView;

.field private txt_sub3:Landroid/widget/TextView;

.field private txt_xyz_add:Landroid/widget/TextView;

.field private txt_xyz_x:Landroid/widget/TextView;

.field private txt_xyz_y:Landroid/widget/TextView;

.field private txt_xyz_z:Landroid/widget/TextView;

.field private txt_xyzprime_add:Landroid/widget/TextView;

.field private txtresult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "GyroscopeIcSTMicro"

    invoke-direct {p0, v0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;-><init>(Ljava/lang/String;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mZeroRatePass:Z

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mHandler:Landroid/os/Handler;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;)Landroid/widget/TableLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mSelfTestResults:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;
    .param p1, "x1"    # [Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->showTestResults([Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 63
    const v0, 0x7f0b00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0b00e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_add:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0b00e5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyzprime_add:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0b00db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_X:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0b00dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_Y:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0b00dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_Z:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0b00c8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_sub1:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0b00ca

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_sub2:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0b00e6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_sub3:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0b00de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_x:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0b00df

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_y:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0b00e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_z:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f0b00e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_x:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0b00e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_y:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0b00e4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_z:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0b00e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_x:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0b00e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_y:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0b00e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_z:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0b00d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_fifo_subject:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0b00c6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_fifo_result:Landroid/widget/TextView;

    .line 83
    return-void
.end method

.method private showTestResults([Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "results"    # [Ljava/lang/String;
    .param p2, "resultValue"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, -0x10000

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_x:Landroid/widget/TextView;

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_y:Landroid/widget/TextView;

    aget-object v1, p1, v4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_xyz_z:Landroid/widget/TextView;

    aget-object v1, p1, v5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_x:Landroid/widget/TextView;

    aget-object v1, p1, v6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_prime_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_x:Landroid/widget/TextView;

    aget-object v1, p1, v6

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_y:Landroid/widget/TextView;

    const/4 v1, 0x4

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txt_diff_z:Landroid/widget/TextView;

    const/4 v1, 0x5

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    aget-object v2, p1, v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    if-eqz p2, :cond_0

    .line 145
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mZeroRatePass:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const v1, 0x7f08007c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030036

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->setContentView(I)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->init()V

    .line 60
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 86
    invoke-super {p0}, Lcom/sec/android/app/hwmoduletest/support/BaseActivity;->onResume()V

    .line 89
    const v2, 0x7f0b00d9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    .line 90
    const v2, 0x7f0b00c7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableLayout;

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroSelfTestLayout:Landroid/widget/TableLayout;

    invoke-virtual {v2, v4}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v4, "/sys/class/sensors/gyro_sensor/selftest"

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x2000

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "selfTestResult":Ljava/lang/String;
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mSelfTestResults:[Ljava/lang/String;

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mSelfTestResults:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v2, v2, v4

    if-eqz v2, :cond_0

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mSelfTestResults:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v2, v2, v4

    const-string v4, "1"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mZeroRatePass:Z

    .line 106
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mZeroRatePass:Z

    if-eqz v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    const-string v3, "Zero Rate Level Check:  PASS"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->txtresult:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro$1;-><init>(Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 128
    .end local v1    # "selfTestResult":Ljava/lang/String;
    :goto_1
    return-void

    .restart local v1    # "selfTestResult":Ljava/lang/String;
    :cond_1
    move v2, v3

    .line 103
    goto :goto_0

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    const-string v3, "Zero Rate Level Check:  FAIL"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GyroscopeIcSTMicro;->mGyroZeroRateText:Landroid/widget/TextView;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 125
    .end local v1    # "selfTestResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
