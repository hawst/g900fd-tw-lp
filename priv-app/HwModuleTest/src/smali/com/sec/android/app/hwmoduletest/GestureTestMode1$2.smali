.class Lcom/sec/android/app/hwmoduletest/GestureTestMode1$2;
.super Ljava/lang/Object;
.source "GestureTestMode1.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$2;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode1$2;->this$0:Lcom/sec/android/app/hwmoduletest/GestureTestMode1;

    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mBtn_start:Landroid/widget/ToggleButton;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode1;)Landroid/widget/ToggleButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$402(Z)Z

    .line 100
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 101
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$200()Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->starGestureZmeanTest()V

    .line 107
    :goto_0
    return-void

    .line 103
    :cond_0
    # setter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mIsTesting:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$402(Z)Z

    .line 104
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$100()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 105
    # getter for: Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->GestureFragment1:Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1;->access$200()Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode1Fragment;->stopGestureZmeanTest()V

    goto :goto_0
.end method
