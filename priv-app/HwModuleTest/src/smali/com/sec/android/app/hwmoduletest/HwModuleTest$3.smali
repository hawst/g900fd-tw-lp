.class Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;
.super Landroid/os/Handler;
.source "HwModuleTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/hwmoduletest/HwModuleTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V
    .locals 0

    .prologue
    .line 780
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 782
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 801
    :cond_0
    :goto_0
    return-void

    .line 784
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$200(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "handleMessage"

    const-string v2, "START_SAVE_TSP_DATA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->makeTspDataReference()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$300(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->makeTspDataInspection()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$400(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    .line 787
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$500(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "StartTspdataTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PassSavingReference -1 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    iget-boolean v3, v3, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingReference:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,PassSavingInspection : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    iget-boolean v3, v3, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingInspection:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingReference:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    iget-boolean v0, v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->PassSavingInspection:Z

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # getter for: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$600(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "StartTspdataTest"

    const-string v2, "remove dialog"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    iget-object v0, v0, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/HwModuleTest$3;->this$0:Lcom/sec/android/app/hwmoduletest/HwModuleTest;

    # invokes: Lcom/sec/android/app/hwmoduletest/HwModuleTest;->hideProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/hwmoduletest/HwModuleTest;->access$700(Lcom/sec/android/app/hwmoduletest/HwModuleTest;)V

    goto :goto_0

    .line 782
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method
