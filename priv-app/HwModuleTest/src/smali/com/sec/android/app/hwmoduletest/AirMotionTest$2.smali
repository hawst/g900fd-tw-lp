.class Lcom/sec/android/app/hwmoduletest/AirMotionTest$2;
.super Ljava/lang/Object;
.source "AirMotionTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/AirMotionTest;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/AirMotionTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/AirMotionTest;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/AirMotionTest$2;->this$0:Lcom/sec/android/app/hwmoduletest/AirMotionTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 109
    check-cast p1, Landroid/widget/CheckBox;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    # setter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z
    invoke-static {v2}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$102(Z)Z

    .line 111
    # getter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$200()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 116
    :goto_0
    return-void

    .line 113
    :cond_0
    # setter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mLoggingOn:Z
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$102(Z)Z

    .line 114
    # getter for: Lcom/sec/android/app/hwmoduletest/AirMotionTest;->mCheckbox_logging:Landroid/widget/CheckBox;
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/AirMotionTest;->access$200()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
