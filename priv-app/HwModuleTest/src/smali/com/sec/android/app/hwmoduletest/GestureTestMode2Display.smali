.class public Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;
.super Landroid/app/Activity;
.source "GestureTestMode2Display.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "GestureTestMode2Display"

.field private static final TYPE_AIRMOTION:I = 0x2

.field private static mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

.field private static mLoggingOn:Z

.field private static mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private ISWVGA800:Z

.field private gesture_count_D:I

.field private gesture_count_L:I

.field private gesture_count_R:I

.field private gesture_count_U:I

.field private mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

.field private mCount:I

.field private mHandler:Landroid/os/Handler;

.field private mListView:Landroid/widget/ListView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mValueList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/hwmoduletest/HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private txt_Gesture_count_direction:Landroid/widget/TextView;

.field private txt_Gesture_count_title:Landroid/widget/TextView;

.field private txt_display_item_1:Landroid/widget/TextView;

.field private txt_display_item_2:Landroid/widget/TextView;

.field private txt_display_item_3:Landroid/widget/TextView;

.field private txt_display_item_4:Landroid/widget/TextView;

.field private txt_display_item_5:Landroid/widget/TextView;

.field private txt_display_item_6:Landroid/widget/TextView;

.field private txt_display_item_7:Landroid/widget/TextView;

.field private txt_display_item_8:Landroid/widget/TextView;

.field private vendor:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 63
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z

    .line 66
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 68
    sput-object v1, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;

    .line 60
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mHandler:Landroid/os/Handler;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mPowerManager:Landroid/os/PowerManager;

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->ISWVGA800:Z

    .line 75
    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I

    .line 196
    new-instance v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display$1;-><init>(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000()Lcom/sec/android/app/hwmoduletest/LogSensorData;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I

    return v0
.end method

.method static synthetic access$308(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_U:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I

    return v0
.end method

.method static synthetic access$408(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_D:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I

    return v0
.end method

.method static synthetic access$508(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_L:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I

    return v0
.end method

.method static synthetic access$608(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->gesture_count_R:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_direction:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;)Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    return-object v0
.end method

.method static synthetic access$900()Z
    .locals 1

    .prologue
    .line 55
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z

    return v0
.end method

.method private initTextView()V
    .locals 3

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->ISWVGA800:Z

    if-eqz v0, :cond_1

    .line 182
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    const v1, 0x7f03002d

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    .line 186
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    iget-boolean v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->ISWVGA800:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;->setISWVGA800(Z)V

    .line 187
    const v0, 0x7f0b0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mListView:Landroid/widget/ListView;

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 191
    sget-boolean v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z

    if-eqz v0, :cond_0

    .line 192
    new-instance v0, Lcom/sec/android/app/hwmoduletest/LogSensorData;

    const-string v1, "GestureData"

    invoke-direct {v0, v1}, Lcom/sec/android/app/hwmoduletest/LogSensorData;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 194
    :cond_0
    return-void

    .line 184
    :cond_1
    new-instance v0, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    const v1, 0x7f03002c

    iget-object v2, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mValueList:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mAdaptor:Lcom/sec/android/app/hwmoduletest/HistoryAdaptor;

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    .line 79
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const-string v3, "GestureTestMode2Display"

    const-string v4, "onCreate"

    const-string v5, "onCreate start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v3, "GESTURE_SENSOR_VENDOR"

    invoke-static {v3}, Lcom/sec/android/app/hwmoduletest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;

    .line 83
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 85
    .local v2, "displayMetrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 87
    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 88
    .local v1, "deviceWidth":I
    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 90
    .local v0, "deviceHeight":I
    const-string v3, "GestureTestMode2Display"

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deviceWidth = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", deviceHeight = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const/16 v3, 0x1e0

    if-ne v1, v3, :cond_0

    const/16 v3, 0x320

    if-ne v0, v3, :cond_0

    .line 93
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->ISWVGA800:Z

    .line 96
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->ISWVGA800:Z

    if-eqz v3, :cond_2

    .line 97
    const v3, 0x7f03002b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->setContentView(I)V

    .line 102
    :goto_0
    const v3, 0x7f0b001e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_1:Landroid/widget/TextView;

    .line 103
    const v3, 0x7f0b001b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_2:Landroid/widget/TextView;

    .line 104
    const v3, 0x7f0b001f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_3:Landroid/widget/TextView;

    .line 105
    const v3, 0x7f0b0020

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_4:Landroid/widget/TextView;

    .line 106
    const v3, 0x7f0b0021

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_5:Landroid/widget/TextView;

    .line 107
    const v3, 0x7f0b0022

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_6:Landroid/widget/TextView;

    .line 108
    const v3, 0x7f0b0023

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_7:Landroid/widget/TextView;

    .line 109
    const v3, 0x7f0b0024

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_8:Landroid/widget/TextView;

    .line 110
    const v3, 0x7f0b00a4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_title:Landroid/widget/TextView;

    .line 111
    const v3, 0x7f0b00a5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_direction:Landroid/widget/TextView;

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_direction:Landroid/widget/TextView;

    const-string v4, "R:    L:    U:    D:"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    const-string v3, "MAXIM"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_1:Landroid/widget/TextView;

    const-string v4, "Zdelta"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_2:Landroid/widget/TextView;

    const-string v4, "Peak to Peak"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_3:Landroid/widget/TextView;

    const-string v4, "Valid cnt"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_4:Landroid/widget/TextView;

    const-string v4, "Angle"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_5:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_6:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_7:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_8:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_title:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_Gesture_count_direction:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :cond_1
    :goto_1
    const-string v3, "sensorhub"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 139
    sget-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    .line 140
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    iput-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mPowerManager:Landroid/os/PowerManager;

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mPowerManager:Landroid/os/PowerManager;

    const/16 v4, 0x1a

    const-string v5, "GestureTestMode2Display"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "logging"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z

    .line 143
    const-string v3, "GestureTestMode2Display"

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "logging flag value = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mLoggingOn:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-direct {p0}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->initTextView()V

    .line 145
    return-void

    .line 99
    :cond_2
    const v3, 0x7f03002a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->setContentView(I)V

    goto/16 :goto_0

    .line 126
    :cond_3
    const-string v3, "AMS"

    iget-object v4, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->vendor:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_1:Landroid/widget/TextView;

    const-string v4, "D"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_2:Landroid/widget/TextView;

    const-string v4, "CA"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_3:Landroid/widget/TextView;

    const-string v4, "Cnt"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_4:Landroid/widget/TextView;

    const-string v4, "MSNW"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_5:Landroid/widget/TextView;

    const-string v4, "EnA"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_6:Landroid/widget/TextView;

    const-string v4, "ExA"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_7:Landroid/widget/TextView;

    const-string v4, "EnM"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->txt_display_item_8:Landroid/widget/TextView;

    const-string v4, "ExM"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 164
    const-string v0, "GestureTestMode2Display"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 167
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 168
    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 171
    :cond_0
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    invoke-virtual {v0}, Lcom/sec/android/app/hwmoduletest/LogSensorData;->stop()V

    .line 173
    sput-object v3, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mGestureLog:Lcom/sec/android/app/hwmoduletest/LogSensorData;

    .line 175
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 176
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 156
    const-string v0, "GestureTestMode2Display"

    const-string v1, "onPause"

    const-string v2, "onPause start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;)V

    .line 160
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 161
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 149
    const-string v0, "GestureTestMode2Display"

    const-string v1, "onResume"

    const-string v2, "onResume start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/hwmoduletest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    sget-object v2, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mSensorHub:Lcom/samsung/android/sensorhub/SensorHub;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 152
    sget-object v0, Lcom/sec/android/app/hwmoduletest/GestureTestMode2Display;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 153
    return-void
.end method
