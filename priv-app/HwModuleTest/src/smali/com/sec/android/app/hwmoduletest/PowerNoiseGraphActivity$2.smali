.class Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;
.super Ljava/util/TimerTask;
.source "PowerNoiseGraphActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    iput-object p2, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 109
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->run_num:I
    invoke-static {v1}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$400(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;)I

    move-result v1

    # getter for: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->sampling:I
    invoke-static {}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$500()I

    move-result v2

    if-le v1, v2, :cond_0

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->getRawData(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$600(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;Z)V

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 116
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    return-void

    .line 112
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity$2;->this$0:Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->getRawData(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;->access$600(Lcom/sec/android/app/hwmoduletest/PowerNoiseGraphActivity;Z)V

    goto :goto_0
.end method
